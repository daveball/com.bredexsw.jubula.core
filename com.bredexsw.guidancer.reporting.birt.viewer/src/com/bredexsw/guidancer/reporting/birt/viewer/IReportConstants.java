/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 11708 $
 *
 * $Date: 2010-08-02 16:58:22 +0200 (Mon, 02 Aug 2010) $
 *
 * $Author: al $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.reporting.birt.viewer;

/**
 * Constants regarding the GUIdancer BIRT reports
 * 
 * @author markus
 * @created Mar 2, 2010
 */
@SuppressWarnings("nls")
public interface IReportConstants {
    /**
     * <code>PLUGIN_REPORT_LOCATION</code>
     */
    public static final String PLUGIN_REPORT_LOCATION = "/reports";

    /**
     * <code>BIRT_REPORT_DESIGN_FILE_PATTERN</code>
     */
    public static final String BIRT_REPORT_DESIGN_FILE_PATTERN = ".*rptdesign";

    /**
     * <code>ABSOLUTE_FILE_URL_KEY</code>
     */
    public static final String ABSOLUTE_FILE_URL_KEY = "com.bredexsw.guidancer.client.gui.command.parameter.createBirtReport.reportToOpen"; //$NON-NLS-1$

    /**
     * <code>REPORT_PARAMETER_DB_PASSWD</code>
     */
    public static final String RP_DB_PASSWD = "DB_PASSWD";

    /**
     * <code>REPORT_PARAMETER_DB_USER</code>
     */
    public static final String RP_DB_USER = "DB_USER";

    /**
     * <code>REPORT_PARAMETER_DB_CONNECTION_STRING</code>
     */
    public static final String RP_DB_CONNECTION_STRING = "DB_CONNECTION_STRING";

    /**
     * <code>RP_DB_JDBC_DRIVER_CLASS</code>
     */
    public static final String RP_DB_JDBC_DRIVER_CLASS = "JDBC_Driver";
    
    /**
     * <code>RP_TEST_RUN_ID</code>
     */
    public static final String RP_TEST_RUN_ID = "P_ID";
}