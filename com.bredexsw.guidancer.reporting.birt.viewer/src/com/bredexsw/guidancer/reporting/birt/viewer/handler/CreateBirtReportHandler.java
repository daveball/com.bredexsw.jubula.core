/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13224 $
 *
 * $Date: 2010-11-22 13:52:11 +0100 (Mon, 22 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.reporting.birt.viewer.handler;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.birt.report.engine.emitter.html.URLEncoder;
import org.eclipse.birt.report.viewer.utilities.WebViewer;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jubula.client.core.model.ITestResultSummaryPO;
import org.eclipse.jubula.client.core.persistence.Persistor;
import org.eclipse.jubula.client.ui.handlers.AbstractTestResultViewHandler;

import com.bredexsw.guidancer.reporting.birt.viewer.IReportConstants;

/**
 * @author markus
 * @created Feb 18, 2010
 */
public class CreateBirtReportHandler extends AbstractTestResultViewHandler 
    implements IReportConstants {
    /** the ID of the "create Birt Report" command */
    public static final String RUN_BIRT_REPORT_COMMAND_ID = "com.bredexsw.guidancer.client.gui.commands.createBirtReport"; //$NON-NLS-1$

    /**
     * <code>WEB_APP_NAME</code>
     */
    private static final String WEB_APP_NAME = "viewer"; //$NON-NLS-1$

    /**
     * {@inheritDoc}
     */
    public Object executeImpl(ExecutionEvent event) {
        final Object reportToExecute = event
                .getParameter(ABSOLUTE_FILE_URL_KEY);

        final Map<String, String> addParams = new HashMap<String, String>();
        ITestResultSummaryPO selectedSummary = getSelectedSummary(event);
        if (selectedSummary != null) {
            addParams.put(RP_TEST_RUN_ID, selectedSummary.getId().toString());
        }

        if (reportToExecute instanceof String) {
            Map<String, Object> params = createGeneralParams(addParams);
            WebViewer.display(WEB_APP_NAME, (String)reportToExecute, params);
        }
        return null;
    }

    /**
     * @param additionalParams
     *            additional parameter to pass to the report
     * @return a map of viewer and report parameters
     */
    protected static Map<String, Object> createGeneralParams(
            Map<String, String> additionalParams) {
        Map<String, Object> webappParams = new HashMap<String, Object>();
        webappParams.put(WebViewer.SERVLET_NAME_KEY, WebViewer.VIEWER_FRAMESET);
        webappParams.put(WebViewer.FORMAT_KEY, WebViewer.HTML);

        Persistor h = Persistor.instance();
        Map<String, String> reportParams = new HashMap<String, String>();
        reportParams.put(RP_DB_JDBC_DRIVER_CLASS, h.getCurrentDBDriverClass());
        reportParams.put(RP_DB_CONNECTION_STRING, h.getCurrentDBUrl());
        reportParams.put(RP_DB_USER, h.getCurrentDBUser());
        //special characters in password are previously encoded
        reportParams.put(RP_DB_PASSWD, 
                URLEncoder.encode(h.getCurrentDBPw(), "UTF-8"));
        reportParams.putAll(additionalParams);

        webappParams.put(WebViewer.EMITTER_OPTIONS_KEY, reportParams);
        return webappParams;
    }
    
}
