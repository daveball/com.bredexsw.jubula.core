/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 12959 $
 *
 * $Date: 2010-11-11 13:53:23 +0100 (Thu, 11 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.reporting.birt.viewer;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jubula.client.ui.utils.CommandHelper;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;

import com.bredexsw.guidancer.reporting.birt.viewer.handler.CreateBirtReportHandler;


/**
 * Class to collect available reports in reports directory and contribute it to
 * the ui
 * 
 * @author markus
 * @created Feb 18, 2010
 */
public class CreateBirtReportContribution extends CompoundContributionItem
        implements IReportConstants {
    /**
     * {@inheritDoc}
     */
    protected IContributionItem[] getContributionItems() {
        List<IContributionItem> contributionItems = 
            new ArrayList<IContributionItem>();
        URL bundleRootURL = Activator.getDefault().getBundle().getEntry(
                PLUGIN_REPORT_LOCATION);
        try {
            URL pluginUrl = FileLocator.resolve(bundleRootURL);
            File reportDir = new File(pluginUrl.getFile());
            File[] availableReports = reportDir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    if (name != null && name.matches(
                                    BIRT_REPORT_DESIGN_FILE_PATTERN)) {
                        return true;
                    }
                    return false;
                }
            });
            List<File> reports = Arrays.asList(availableReports);
            Collections.sort(reports);
            if (reports.size() > 0) {
                for (File f : availableReports) {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put(ABSOLUTE_FILE_URL_KEY, f.getCanonicalPath());
                    String itemName = "Report: " + f.getName().replaceFirst(//$NON-NLS-1$
                            "\\.rptdesign", ""); //$NON-NLS-1$ //$NON-NLS-2$
                    contributionItems.add(CommandHelper.createContributionItem(
                            CreateBirtReportHandler.RUN_BIRT_REPORT_COMMAND_ID, 
                            params,
                            itemName, CommandContributionItem.STYLE_PUSH));
                }
            } else {
                contributionItems.add(CommandHelper.createContributionItem(
                        CreateBirtReportHandler.RUN_BIRT_REPORT_COMMAND_ID, 
                        null,
                        "No Reports found...", //$NON-NLS-1$
                        CommandContributionItem.STYLE_PUSH));
            }
        } catch (IOException e) {
            // empty
        }

        return contributionItems
                .toArray(new IContributionItem[contributionItems.size()]);
    }
}
