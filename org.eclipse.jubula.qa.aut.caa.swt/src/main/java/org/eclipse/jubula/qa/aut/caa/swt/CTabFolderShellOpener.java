package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;

/**
 * ButtonListener to open a shell with CTabFolders to test.
 * 
 * @author janw
 *
 */
public class CTabFolderShellOpener extends AbstractShellOpener {

    /**
     * @param owner owner
     */
    public CTabFolderShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getPageTitle() {
        return I18NUtils.getString("title_ctabfolder"); //$NON-NLS-1$
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
            ComponentNameConstants.TESTPAGE_TITLE_TABBEDPANES);

        // TabFolder 1
        Menu tabFolderContextMenu = getPopupMenu(getCurrentPageShell());
        CTabFolder ctabfolder1 = new CTabFolder(parent, SWT.LEFT);
        ctabfolder1.setLocation(10, 10);
        ctabfolder1.setSize(200, 100);
        CompUtils.setComponentName(ctabfolder1,
            ComponentNameConstants.TESTPAGE_CTABFOLDERS_CTF01);
        ctabfolder1.setVisible(true);
        ctabfolder1.setEnabled(true);
        ctabfolder1.setMenu(tabFolderContextMenu);
        
        CTabItem item1 = new CTabItem(ctabfolder1, SWT.LEFT);
        item1.setText(I18NUtils.getString("ctab1"));
        Label lbl1 = new Label(ctabfolder1, SWT.CENTER);
        lbl1.setText(I18NUtils.getString("img_carrot"));
        lbl1.setMenu(tabFolderContextMenu);
        item1.setControl(lbl1);
        
        CTabItem item2 = new CTabItem(ctabfolder1, SWT.LEFT);
        item2.setText(I18NUtils.getString("ctab2"));
        Label lbl2 = new Label(ctabfolder1, SWT.CENTER);
        lbl2.setText(I18NUtils.getString("img_banana"));
        lbl2.setMenu(tabFolderContextMenu);
        item2.setControl(lbl2);
        
        CTabItem item3 = new CTabItem(ctabfolder1, SWT.TOP);
        item3.setText(I18NUtils.getString("ctab3"));
        Label lbl3 = new Label(ctabfolder1, SWT.CENTER);
        lbl3.setEnabled(false);
        lbl3.setText(I18NUtils.getString("img_kiwi"));
        lbl3.setMenu(tabFolderContextMenu);
        item3.setControl(lbl3);
        
        // TabFolder 2
        CTabFolder ctabfolder2 = new CTabFolder(parent, SWT.LEFT);
        ctabfolder2.setLocation(10, 120);
        ctabfolder2.setSize(200, 100);
        CompUtils.setComponentName(ctabfolder2,
            ComponentNameConstants.TESTPAGE_CTABFOLDERS_CTF02);
        ctabfolder2.setVisible(true);
        ctabfolder2.setEnabled(false);
        
        CTabItem item21 = new CTabItem(ctabfolder2, SWT.RIGHT);
        item21.setText(I18NUtils.getString("ctab1"));
        Label lbl21 = new Label(ctabfolder2, SWT.CENTER);
        lbl21.setText(I18NUtils.getString("img_carrot"));
        item21.setControl(lbl21);
        
        CTabItem item22 = new CTabItem(ctabfolder2, SWT.LEFT);
        item22.setText(I18NUtils.getString("ctab2"));
        Label lbl22 = new Label(ctabfolder2, SWT.CENTER);
        lbl22.setText(I18NUtils.getString("img_banana"));
        item22.setControl(lbl22);
        
        CTabItem item23 = new CTabItem(ctabfolder2, SWT.LEFT);
        item23.setText(I18NUtils.getString("ctab3"));
        Label lbl23 = new Label(ctabfolder2, SWT.CENTER);
        lbl23.setText(I18NUtils.getString("img_kiwi"));
        item23.setControl(lbl23);
        
        createMoreTabFolders(parent);
    }
    
    /**
     * @param c the parent to use
     */
    private void createMoreTabFolders(Composite c) {
        // TabFolder 3
        CTabFolder ctabfolder3 = new CTabFolder(c, SWT.LEFT);
        ctabfolder3.setLocation(10, 230);
        ctabfolder3.setSize(200, 100);
        CompUtils.setComponentName(ctabfolder3,
                ComponentNameConstants.TESTPAGE_CTABFOLDERS_CTF03);
        ctabfolder3.setVisible(true);
        
        for (int i = 0; i < 50; i++) {
            CTabItem item = new CTabItem(ctabfolder3, SWT.RIGHT);
            item.setText(i + ""); //$NON-NLS-1$
            Label lbl = new Label(ctabfolder3, SWT.CENTER);
            lbl.setText(I18NUtils.getString("img_carrot"));
            item.setControl(lbl);
        }

        // TabFolder 4
        CTabFolder ctabfolder4 = new CTabFolder(c, SWT.LEFT);
        ctabfolder4.setLocation(10, 340);
        ctabfolder4.setSize(200, 100);
        CompUtils.setComponentName(ctabfolder4,
            ComponentNameConstants.TESTPAGE_CTABFOLDERS_CTF04);
        ctabfolder4.setTabPosition(SWT.BOTTOM);
        ctabfolder4.setSimple(false);
        ctabfolder4.setVisible(true);
        ctabfolder4.setEnabled(true);
        
        CTabItem item41 = new CTabItem(ctabfolder4, SWT.LEFT);
        item41.setText(I18NUtils.getString("ctab1"));
        Label lbl41 = new Label(ctabfolder4, SWT.CENTER);
        lbl41.setText(I18NUtils.getString("img_carrot"));
        item41.setControl(lbl41);
        
        CTabItem item42 = new CTabItem(ctabfolder4, SWT.LEFT);
        item42.setText(I18NUtils.getString("ctab2"));
        Label lbl42 = new Label(ctabfolder4, SWT.CENTER);
        lbl42.setText(I18NUtils.getString("img_banana"));
        item42.setControl(lbl42);
        
        CTabItem item43 = new CTabItem(ctabfolder4, SWT.TOP);
        item43.setText(I18NUtils.getString("ctab3"));
        Label lbl43 = new Label(ctabfolder4, SWT.CENTER);
        lbl43.setEnabled(false);
        lbl43.setText(I18NUtils.getString("img_kiwi"));
        item43.setControl(lbl43);

        // TabFolder 5
        CTabFolder ctabfolder5 = new CTabFolder(c, SWT.LEFT | SWT.CLOSE);
        ctabfolder5.setLocation(10, 450);
        ctabfolder5.setSize(200, 100);
        CompUtils.setComponentName(ctabfolder5,
            ComponentNameConstants.TESTPAGE_CTABFOLDERS_CTF05);
        ctabfolder5.setVisible(true);
        ctabfolder5.setEnabled(true);
        
        CTabItem item51 = new CTabItem(ctabfolder5, SWT.LEFT);
        item51.setText(I18NUtils.getString("ctab1"));
        Label lbl51 = new Label(ctabfolder5, SWT.CENTER);
        lbl51.setText(I18NUtils.getString("img_carrot"));
        item51.setControl(lbl51);
        
        CTabItem item52 = new CTabItem(ctabfolder5, SWT.LEFT);
        item52.setText(I18NUtils.getString("ctab2"));
        Label lbl52 = new Label(ctabfolder5, SWT.CENTER);
        lbl52.setText(I18NUtils.getString("img_banana"));
        item52.setControl(lbl52);
        
        CTabItem item53 = new CTabItem(ctabfolder5, SWT.TOP);
        item53.setText(I18NUtils.getString("ctab3"));
        Label lbl53 = new Label(ctabfolder5, SWT.CENTER);
        lbl53.setEnabled(false);
        lbl53.setText(I18NUtils.getString("img_kiwi"));
        item53.setControl(lbl53);
    }
}
