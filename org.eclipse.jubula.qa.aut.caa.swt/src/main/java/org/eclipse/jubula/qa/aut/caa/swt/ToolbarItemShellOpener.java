package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * ButtonListener to open a shell with Toolbar items to test.
 * 
 */
public class ToolbarItemShellOpener extends AbstractShellOpener {
    /**
     * ToolbarItemShellOpener
     * 
     * @param owner
     *            owner
     */
    public ToolbarItemShellOpener(Composite owner) {
        super(owner);
    }

    /** {@inheritDoc} */
    protected void fillContent(Composite parent) {
        parent.setLayout(new RowLayout());
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TOOLBAR_ITEM);

        ToolBar toolBar = new ToolBar(parent, SWT.HORIZONTAL);
        // Create push buttons
        ToolItem item = createToolItem(toolBar, SWT.PUSH,
                I18NUtils.getName("btn1"), null, null, "This is button one");
        new ToolItem(toolBar, SWT.SEPARATOR);

        // Create push buttons
        item = createToolItem(toolBar, SWT.PUSH, I18NUtils.getName("btn2"),
                null, null, "This is button two");
        item.setEnabled(false);

        new ToolItem(toolBar, SWT.SEPARATOR);

        // Create dropdowns
        createToolbarItemWithDropdownMenu(toolBar, getLabel());
    }

    /**
     * @param tb
     *            the toolbar
     * @param lbl the label to use
     */
    private void createToolbarItemWithDropdownMenu(final ToolBar tb, 
        Label lbl) {
        final ToolItem tItem = createToolItem(tb, SWT.DROP_DOWN,
                I18NUtils.getName("btn3"), null, null,
                "This is dropdown toolbar item.");

        final Menu menu = fillMenu(new Menu(tb.getShell(), SWT.POP_UP), lbl);

        tItem.addListener(SWT.Selection, new Listener() {
            public void handleEvent(Event event) {
                if (event.detail == SWT.ARROW) {
                    Rectangle rect = tItem.getBounds();
                    Point pt = new Point(rect.x, rect.y + rect.height);
                    pt = tb.toDisplay(pt);
                    menu.setLocation(pt.x, pt.y);
                    menu.setVisible(true);
                }
            }
        });
    }

    /**
     * Helper function to create tool item
     * 
     * @param parent
     *            the parent toolbar
     * @param type
     *            the type of tool item to create
     * @param text
     *            the text to display on the tool item
     * @param image
     *            the image to display on the tool item
     * @param hotImage
     *            the hot image to display on the tool item
     * @param toolTipText
     *            the tool tip text for the tool item
     * @return ToolItem
     */
    private ToolItem createToolItem(ToolBar parent, int type, String text,
            Image image, Image hotImage, String toolTipText) {
        ToolItem item = new ToolItem(parent, type);
        item.setText(text);
        item.setImage(image);
        item.setHotImage(hotImage);
        item.setToolTipText(toolTipText);
        return item;
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_toolbaritem"); //$NON-NLS-1$
    }
    
}
