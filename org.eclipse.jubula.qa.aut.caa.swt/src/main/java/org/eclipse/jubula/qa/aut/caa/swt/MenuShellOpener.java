package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

/**
 * ButtonListener to open a shell with Menu to test.
 * 
 * MenuItems: enabled = true /false radioButton = true checkBox = true separator
 * = true subMenu = true
 * 
 */
public class MenuShellOpener extends AbstractShellOpener {

    /**
     * MenuShellOpener
     * 
     * @param owner
     *            owner
     */
    public MenuShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_MENUS);

        Menu menubar = new Menu(getCurrentPageShell(), SWT.BAR);
        MenuItem menuItem = new MenuItem(menubar, SWT.CASCADE);
        menuItem.setData("name", I18NUtils.getName("mi6")); //$NON-NLS-1$
        
        MenuItem menuItem2 = new MenuItem(menubar, SWT.CASCADE);
        menuItem2.setData("name", I18NUtils.getName("mi7")); //$NON-NLS-1$

        Menu menu = fillMenu(new Menu(menubar), getLabel());
        menu.setData("name", I18NUtils.getName("mn1")); //$NON-NLS-1$
        
        Menu menu2 = fillMenu(new Menu(menubar), getLabel());
        menu2.setData("name", I18NUtils.getName("mn2")); //$NON-NLS-1$
        
        menuItem.setText(I18NUtils.getString("menu")); //$NON-NLS-1$
        menuItem.setMenu(menu);
        
        menuItem2.setText(I18NUtils.getString("menu2")); //$NON-NLS-1$
        menuItem2.setMenu(menu2);
        
        getCurrentPageShell().setMenuBar(menubar);
    }
        
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_menus"); //$NON-NLS-1$
    }
    
}
