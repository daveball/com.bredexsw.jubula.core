package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * ButtonListener to open a shell with Buttons to test.
 * 
 * Button 1: enabled = true tooltip = true contextmenu = true
 * 
 * Button 2: enabled = false
 * 
 */
public class ButtonShellOpener extends AbstractShellOpener {

    /**
     * ButtonShellOpener
     * 
     * @param owner
     *            owner
     */
    public ButtonShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * 
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent, 
                ComponentNameConstants.TESTPAGE_TITLE_BUTTONS);

        // button 1
        Button btn1 = new Button(parent, SWT.PUSH);
        btn1.setText(I18NUtils.getName("btn1"));
        CompUtils.setComponentName(btn1,
                ComponentNameConstants.TESTPAGE_BUTTONS_BTN01);
        btn1.setSize(70, 25);
        btn1.setLocation(10, 10);
        btn1.setToolTipText(I18NUtils.getString("tooltip"));
        btn1.setMenu(getPopupMenu(getCurrentPageShell()));

        // button 2
        Button btn2 = new Button(parent, SWT.PUSH);
        btn2.setText(I18NUtils.getName("btn2"));
        CompUtils.setComponentName(btn2,
                ComponentNameConstants.TESTPAGE_BUTTONS_BTN02);
        btn2.setSize(70, 25);
        btn2.setLocation(10, 50);
        btn2.setEnabled(false);
        
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_buttons"); //$NON-NLS-1$
    }

}
