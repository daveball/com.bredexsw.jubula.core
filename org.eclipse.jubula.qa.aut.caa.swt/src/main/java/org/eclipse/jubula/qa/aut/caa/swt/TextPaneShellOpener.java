/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 7711 $
 *
 * $Date: 2009-01-21 11:10:21 +0100 (Wed, 21 Jan 2009) $
 *
 * $Author: berndk $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.widgets.Composite;

/**
 * TextPane not supported in SWT
 *
 * @author berndk
 * @created 14.01.2009
 */
public class TextPaneShellOpener extends AbstractShellOpener {

    /**
     * TextPaneShellOpener
     * @param owner owner
     */
    public TextPaneShellOpener(Composite owner) {
        super(owner);
    }
  
    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TEXTPANES);
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_textPanes"); //$NON-NLS-1$
    }
}
