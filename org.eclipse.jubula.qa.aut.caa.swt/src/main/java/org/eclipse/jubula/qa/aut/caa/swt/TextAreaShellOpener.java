/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 7711 $
 *
 * $Date: 2009-01-21 11:10:21 +0100 (Wed, 21 Jan 2009) $
 *
 * $Author: berndk $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * @author berndk
 * @created 14.01.2009
 */
public class TextAreaShellOpener extends AbstractShellOpener {

    /**
     * TextAreaShellOpener 
     * 
     * TextArea not supported in SWT
     * 
     * @param owner
     *            owner
     */
    public TextAreaShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_TEXTAREAS);

        Text text1 = new Text(parent, SWT.MULTI | SWT.BORDER);
        text1.setSize(200, 60);
        text1.setLocation(10, 10);
        text1.setToolTipText(I18NUtils.getString("tooltip"));
        text1.setMenu(getPopupMenu(getCurrentPageShell()));
        
        Text text2 = new Text(parent, SWT.MULTI | SWT.BORDER);
        text2.setSize(200, 60);
        text2.setLocation(10, 80);
        text2.setEnabled(false);
        
        Text text3 = new Text(parent, SWT.MULTI | SWT.BORDER);
        text3.setSize(200, 60);
        text3.setLocation(10, 150);
        text3.setText(I18NUtils.getString("text1"));
        
        Text text4 = new Text(parent, SWT.MULTI | SWT.BORDER);
        text4.setSize(200, 60);
        text4.setLocation(10, 220);
        text4.setText(I18NUtils.getString("text2"));
        text4.setEnabled(false);

        Text text5 = new Text(parent, SWT.MULTI | SWT.BORDER);
        text5.setSize(200, 60);
        text5.setLocation(10, 290);
        text5.setText(I18NUtils.getString("text3"));
        text5.setEditable(false);
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_textAreas"); //$NON-NLS-1$
    }
}
