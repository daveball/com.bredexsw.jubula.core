package org.eclipse.jubula.qa.aut.caa;

import org.eclipse.jubula.qa.aut.caa.swt.StartShell;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


/**
 * Mainclass to start the SWT AUT.
 * 
 * @author oussama
 */
public class StartSWTAUT {

    /**
     * 
     */
    private static Display display;

    /**
     * 
     */
    private static Shell shell;

    /**
     * private Constructor
     */
    private StartSWTAUT() {
        // empty
    }

    /**
     * @param args
     *            args
     */
    public static void main(String[] args) {
        display = new Display();

        shell = StartShell.getShell(display);
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();
    }
}
