package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;

/**
 * ButtonListener to open a shell with Buttons to test.
 * 
 * Button 1: enabled = true tooltip = true contextmenu = true
 * 
 * Button 2: enabled = false
 * 
 */
public class ApplicationShellOpener extends AbstractShellOpener {
    /**
     * @author BREDEX GmbH
     */
    private static class ProperAUTTerminator implements SelectionListener  {
        /** {@inheritDoc} */
        public void widgetDefaultSelected(SelectionEvent arg0) {
            widgetSelected(arg0);
        }
        /** {@inheritDoc} */
        public void widgetSelected(SelectionEvent arg0) {
            System.exit(0);
        }
    }

    /**
     * listener to terminate the AUT properly
     */
    private SelectionListener m_autTerminator = new ProperAUTTerminator();
    
    /**
     * ButtonShellOpener
     * 
     * @param owner
     *            owner
     */
    public ApplicationShellOpener(Composite owner) {
        super(owner);
    }


    /** {@inheritDoc} */
    protected void fillContent(Composite parent) {
        // Aut --> Exit Menu
        Menu menubar = new Menu(getCurrentPageShell(), SWT.BAR);
        
        MenuItem autMenuItem = new MenuItem(menubar, SWT.CASCADE);
        autMenuItem.setData("name", I18NUtils.getName("aut")); //$NON-NLS-1$
        
        Menu autMenu = new Menu(getCurrentPageShell(), SWT.DROP_DOWN);
        
        MenuItem exitMenuItem = new MenuItem(autMenu, SWT.PUSH);
        exitMenuItem.setText(I18NUtils.getString("exit"));
        exitMenuItem.addSelectionListener(m_autTerminator);
        
        autMenuItem.setText(I18NUtils.getString("aut")); //$NON-NLS-1$
        autMenuItem.setMenu(autMenu);
        getCurrentPageShell().setMenuBar(menubar);
        
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_APPLICATION);

        // Input Text 
        Text tf1 = new Text(parent, SWT.BORDER);
        CompUtils.setComponentName(tf1,
                ComponentNameConstants.TESTPAGE_APPLICATION_TF01);
        tf1.setSize(70, 25);
        tf1.setLocation(10, 10);
        tf1.setToolTipText(I18NUtils.getString("tooltip"));
        tf1.setMenu(getPopupMenu(getCurrentPageShell()));
        
        // Exit Button
        Button exitBtn = new Button(parent, SWT.PUSH);
        exitBtn.setText(I18NUtils.getString("exit"));
        CompUtils.setComponentName(exitBtn,
                ComponentNameConstants.TESTPAGE_APPLICATION_EXIT_BTN);
        exitBtn.setSize(70, 25);
        exitBtn.setLocation(10, 45);
        exitBtn.addSelectionListener(m_autTerminator);
    }

    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_application"); //$NON-NLS-1$
    }
}
