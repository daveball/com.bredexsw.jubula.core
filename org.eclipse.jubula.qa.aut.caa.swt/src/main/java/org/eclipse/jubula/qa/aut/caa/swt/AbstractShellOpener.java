package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

/**
 * Abstract superclass for all shell openers.
 * 
 */
public abstract class AbstractShellOpener extends SelectionAdapter {
    /**
     * m_owner
     */
    private Composite m_owner;

    /**
     * m_mainShell
     */
    private Shell m_mainShell;

    /**
     * m_currentPageShell
     */
    private Shell m_currentPageShell;

    /**
     * m_label
     */
    private Label m_label;
    
    /**
     * constructor
     * 
     * @param owner
     *            owner
     */
    public AbstractShellOpener(Composite owner) {
        m_owner = owner;
        m_mainShell = owner.getShell();
    }

    /**
     * Do when clicked.
     * 
     * @param arg0
     *            arg0
     */
    public void widgetSelected(SelectionEvent arg0) {
        final Shell newPageShell = new Shell(arg0.display);
        newPageShell.setParent(m_owner);
        newPageShell.setLayout(new FormLayout());

        newPageShell.addShellListener(new ShellAdapter() {
            @Override
            public void shellClosed(ShellEvent e) {
                super.shellClosed(e);
                System.exit(0);
            }
        });
        
        final Composite closeAndLabelComposite = new Composite(
                newPageShell, SWT.BORDER);
        FormData formDataButtom = new FormData();
        formDataButtom.left = new FormAttachment(0, 5);
        formDataButtom.right = new FormAttachment(100, -5);
        formDataButtom.bottom = new FormAttachment(100, -1);
        closeAndLabelComposite.setLayoutData(formDataButtom);
        closeAndLabelComposite.setLayout(new GridLayout());

        m_label = new Label(closeAndLabelComposite, SWT.NULL);
        m_label.setText(I18NUtils.getString("lblBeforeAction")); //$NON-NLS-1$
        CompUtils.setComponentName(m_label,
                ComponentNameConstants.TESTPAGES_CONTENTLABEL);
        m_label.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));
        
        Button btnClose = new Button(closeAndLabelComposite, SWT.CLOSE);
        btnClose.setText(I18NUtils.getString("closeBtn")); //$NON-NLS-1$
        CompUtils.setComponentName(btnClose,
                ComponentNameConstants.TESTPAGES_CLOSEBUTTON);
        btnClose.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));
        btnClose.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                newPageShell.dispose();
                m_mainShell.setVisible(true);
                m_mainShell.setActive();
            }
        });
        
        ScrolledComposite scrollCom = new ScrolledComposite(newPageShell,
                SWT.V_SCROLL | SWT.H_SCROLL | SWT.CENTER);
        scrollCom.setExpandHorizontal(true);
        scrollCom.setExpandVertical(true);
        scrollCom.setMinSize(350, 600);

        FormData formDataMainContent = new FormData(200, 700);
        formDataMainContent.top = new FormAttachment(0, 1);
        formDataMainContent.left = new FormAttachment(0, 5);
        formDataMainContent.right = new FormAttachment(100, -5);
        formDataMainContent.bottom = new FormAttachment(closeAndLabelComposite,
                -5);
        scrollCom.setLayoutData(formDataMainContent);
        
        Composite scrollableContentComposite = new Composite(scrollCom,
                SWT.NONE);
        // FIXME-Begin: DON't movable setter call!
        setCurrentPageShell(newPageShell);
        fillContent(scrollableContentComposite);
        // FIXME-End: DON't movable setter call!
        newPageShell.setText(getPageTitle());
        scrollCom.setContent(scrollableContentComposite);

        m_mainShell.setVisible(false);
        newPageShell.pack();
        newPageShell.open();
        newPageShell.setMaximized(true);
    }

    /**
     * @return the current page title to display
     */
    protected abstract String getPageTitle();

    /**
     * Create the content for the composite.
     * 
     * @param parent
     *            parent
     */
    protected abstract void fillContent(Composite parent);

    /**
     * getPopupMenu
     * 
     * @param shell
     *            shell
     * @return PopupMenu
     */
    protected Menu getPopupMenu(Shell shell) {
        return fillMenu(new Menu(shell, SWT.POP_UP), m_label);
    }
    
    /**
     * getPopupMenu
     * 
     * @param menu
     *            menu the menu to use
     * @param output
     *            the label to use for output; may be null
     * @return PopupMenu
     */
    protected Menu fillMenu(Menu menu, final Label output) {
        MenuItem item1 = new MenuItem(menu, SWT.PUSH);
        item1.setText(I18NUtils.getString("menuitem_first"));
        item1.setData("name", I18NUtils.getName("mi1"));
        MenuItem itemMore = new MenuItem(menu, SWT.CASCADE);
        itemMore.setText(I18NUtils.getString("menu_more"));
        itemMore.setData("name", I18NUtils.getString("menu_more"));
        Menu subMenu = new Menu(menu);
        itemMore.setMenu(subMenu);
        subMenu.setData("name", I18NUtils.getName("mn2"));
        MenuItem item2 = new MenuItem(subMenu, SWT.PUSH);
        item2.setText(I18NUtils.getString("menuitem_second"));
        item2.setData("name", I18NUtils.getName("mi2"));
        MenuItem cbxItem = new MenuItem(subMenu, SWT.CHECK);
        cbxItem.setText(I18NUtils.getString("menuitem_cbx"));
        cbxItem.setData("name", I18NUtils.getName("mi3"));
        MenuItem rbxItem = new MenuItem(subMenu, SWT.RADIO);
        rbxItem.setText(I18NUtils.getString("menuitem_rbx"));
        rbxItem.setData("name", I18NUtils.getName("mi4"));
        new MenuItem(subMenu, SWT.SEPARATOR);
        MenuItem item3 = new MenuItem(subMenu, SWT.PUSH);
        item3.setText(I18NUtils.getString("menuitem_third"));
        item3.setEnabled(false);
        item3.setData("name", I18NUtils.getName("mi5"));
        menu.setData("name", I18NUtils.getName("popup"));
        MenuItem itemMore2 = new MenuItem(subMenu, SWT.CASCADE);
        itemMore2.setText(I18NUtils.getString("menu_more"));
        itemMore2.setData("name", I18NUtils.getString("menu_more"));
        Menu subMenu2 = new Menu(subMenu);
        itemMore2.setMenu(subMenu2);
        subMenu.setData("name", I18NUtils.getName("mn3"));
        MenuItem item4 = new MenuItem(subMenu2, SWT.PUSH);
        item4.setText(I18NUtils.getString("menuitem_level3"));
        item4.setData("name", I18NUtils.getName("mi6"));
        MenuItem itemMore3 = new MenuItem(subMenu2, SWT.CASCADE);
        itemMore3.setText(I18NUtils.getString("menu_more"));
        itemMore3.setData("name", I18NUtils.getString("menu_more"));
        Menu subMenu3 = new Menu(subMenu2);
        itemMore3.setMenu(subMenu3);
        subMenu.setData("name", I18NUtils.getName("mn4"));
        MenuItem item5 = new MenuItem(subMenu3, SWT.PUSH);
        item5.setText(I18NUtils.getString("menuitem_level4"));
        item5.setData("name", I18NUtils.getName("mi7"));

        if (output != null) {
            Listener selectionListener = new Listener() {
                public void handleEvent(Event event) {
                    MenuItem item = (MenuItem) event.widget;
                    output.setText(item.getText());
                }
            };
            
            item1.addListener(SWT.Selection, selectionListener);
            item2.addListener(SWT.Selection, selectionListener);
            item3.addListener(SWT.Selection, selectionListener);
            itemMore.addListener(SWT.Selection, selectionListener);
            cbxItem.addListener(SWT.Selection, selectionListener);
            rbxItem.addListener(SWT.Selection, selectionListener);
            item4.addListener(SWT.Selection, selectionListener);
            item5.addListener(SWT.Selection, selectionListener);
        }
        
        return menu;
    }

    /**
     * @return the currents page shell
     */
    protected Shell getCurrentPageShell() { 
        return m_currentPageShell;
    }

    /**
     * @param currentPageShell the current page shell
     */
    private void setCurrentPageShell(Shell currentPageShell) {
        m_currentPageShell = currentPageShell;
    }

    /**
     * @return the label
     */
    protected Label getLabel() {
        return m_label;
    }
}
