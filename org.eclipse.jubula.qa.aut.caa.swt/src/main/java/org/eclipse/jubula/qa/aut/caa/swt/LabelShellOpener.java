package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * ButtonListener to open a shell with Labels to test.
 * 
 * Label 1: tooltip = true contextmenu = true enabled = true text = true
 * 
 * JLabel 2: text = true enabled = false
 * 
 */
public class LabelShellOpener extends AbstractShellOpener {
    /**
     * LabelShellOpener
     * 
     * @param owner
     *            owner
     */
    public LabelShellOpener(Composite owner) {
        super(owner);
    }

    /**
     * {@inheritDoc}
     */
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_LABELS);

        // label 1
        Label lbl1 = new Label(parent, SWT.LEFT);
        lbl1.setText(I18NUtils.getName("lbl1"));
        CompUtils.setComponentName(lbl1,
                ComponentNameConstants.TESTPAGE_LABELS_LBL01);
        lbl1.setSize(70, 25);
        lbl1.setLocation(10, 10);
        lbl1.setToolTipText(I18NUtils.getString("tooltip"));
        lbl1.setMenu(getPopupMenu(getCurrentPageShell()));

        // label 2
        Label lbl2 = new Label(parent, SWT.LEFT);
        lbl2.setText(I18NUtils.getName("lbl2"));
        CompUtils.setComponentName(lbl2,
                ComponentNameConstants.TESTPAGE_LABELS_LBL02);
        lbl2.setSize(70, 25);
        lbl2.setLocation(10, 50);
        lbl2.setEnabled(false);
        // label 3
        final Label lbl3 = new Label(parent, SWT.LEFT);
        lbl3.setText(I18NUtils.getName("lbl3"));
        CompUtils.setComponentName(lbl3,
                ComponentNameConstants.TESTPAGE_LABELS_LBL03);
        lbl3.setSize(70, 25);
        lbl3.setLocation(10, 100);
        lbl3.addMouseListener(new MouseAdapter() {
            public void mouseDoubleClick(MouseEvent e) {
                
                lbl3.setText(I18NUtils.getName("twoClick"));
                
            }
            
            public void mouseDown(MouseEvent e) {
                lbl3.setText(I18NUtils.getName("oneClick"));
            }
        });
    }
    /** {@inheritDoc} */
    protected String getPageTitle() {
        return I18NUtils.getString("title_labels"); //$NON-NLS-1$
    }
}
