package org.eclipse.jubula.qa.aut.caa.swt;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ProgressBar;

/**
 * ButtonListener to open a shell with progressBar to test.
 * 
 * progressBar 1: progressBar 2: progressBar 3:
 */
public class ProgressBarShellOpener extends AbstractShellOpener {
    /**
     * this is the min value of ProgressBar
     */
    private static final int MIN = 0;
    /**
     * this is the max value of ProgressBar
     */
    private static final int MAX = 100;


    /**
     * ProgressBarShellOpener
     * 
     * @param owner
     *             owner
     */
    public ProgressBarShellOpener(Composite owner) {
        super(owner);
    }

  /**
  * {@inheritDoc}
  */
    @Override
    protected void fillContent(Composite parent) {
        CompUtils.setComponentName(parent,
                ComponentNameConstants.TESTPAGE_TITLE_PROGRESSBARS);
        
        //group 1 for progressBar 1 
        Group gr1 = new Group(parent, 0);
        gr1.setText("PB 1: HORIZONTAL");
        gr1.setLocation(20, 20);
        gr1.setSize(200, 40);
        // progressBar 1: horizontal
        ProgressBar pb1 = new ProgressBar(gr1, SWT.HORIZONTAL); 
        CompUtils.setComponentName(pb1,
                ComponentNameConstants.TESTPAGE_PROGRESSBAR_PB01);
        pb1.setToolTipText(I18NUtils.getString("tooltip"));
        pb1.setMenu(getPopupMenu(getCurrentPageShell()));
        pb1.setMinimum(MIN);
        pb1.setMaximum(MAX);
        pb1.setLocation (20, 25);
        pb1.setSize(70, 10);

        //group 2 for progressBar 2 
        Group gr2 = new Group(parent, 0);
        gr2.setText("PB 2: VERTICAL");
        gr2.setLocation(20, 70);
        gr2.setSize(200, 100);
        // progressBar 2: vertical
        ProgressBar pb2 = new ProgressBar(gr2, SWT.VERTICAL);  
        CompUtils.setComponentName(pb2,
                ComponentNameConstants.TESTPAGE_PROGRESSBAR_PB02);
        pb2.setToolTipText(I18NUtils.getString("tooltip"));
        pb2.setMenu(getPopupMenu(getCurrentPageShell()));
        pb2.setMinimum(MIN);
        pb2.setMaximum(MAX);
        pb2.setLocation (20, 25);
        pb2.setSize(10, 70);
        
        //group 3 for progressBar 3 
        Group gr3 = new Group(parent, 0);
        gr3.setText("PB 3: VERTICAL, DISABLED");
        gr3.setLocation(20, 180);
        gr3.setSize(200, 100);
        // progressBar 3: vertical disabled
        ProgressBar pb3 = new ProgressBar(gr3, SWT.VERTICAL);
        CompUtils.setComponentName(pb3,
                ComponentNameConstants.TESTPAGE_PROGRESSBAR_PB03);
        pb3.setToolTipText(I18NUtils.getString("tooltip"));
        pb3.setMinimum(MIN);
        pb3.setMaximum(MAX);
        pb3.setLocation (20, 25);
        pb3.setSize(10, 70);
        pb3.setEnabled(false);

    }
    
    /** {@inheritDoc} */
    @Override
    protected String getPageTitle() {
        return I18NUtils.getString("title_progressBar"); //$NON-NLS-1$
    }
}
