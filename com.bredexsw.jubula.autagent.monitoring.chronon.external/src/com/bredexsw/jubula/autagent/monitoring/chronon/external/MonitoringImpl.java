package com.bredexsw.jubula.autagent.monitoring.chronon.external;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.autagent.monitoring.AbstractMonitoring;
import org.eclipse.jubula.tools.internal.constants.AutConfigConstants;
import org.eclipse.jubula.tools.internal.constants.StringConstants;

/**
 * @author BREDEX GmbH
 * 
 */
public class MonitoringImpl extends AbstractMonitoring {
    /** Key for autConfigMap, this key was set in the extension point */
    private static final String JAVA_AGENT_JAR = "JAVA_AGENT_JAR"; //$NON-NLS-1$
    /** Key for autConfigMap, this key was set in the extension point */
    private static final String NATIVE_AGENT_FILE = "NATIVE_AGENT_FILE"; //$NON-NLS-1$
    /** Key for autConfigMap, this key was set in the extension point */
    private static final String CHRONON_CONFIG_FILE = "CHRONON_CONFIG_FILE"; //$NON-NLS-1$
    
    /** {@inheritDoc} */
    public String createAgent() {
        StringBuilder agentString = new StringBuilder();
        final File javaAgentJar = getAbsolutePathFor(
                getMonitoringAttribute(JAVA_AGENT_JAR));
        final File nativeAgentFile = getAbsolutePathFor(
                getMonitoringAttribute(NATIVE_AGENT_FILE));
        final File chrononConfigFile = getAbsolutePathFor(
                getMonitoringAttribute(CHRONON_CONFIG_FILE));
        
        // build the java agent configuration
        agentString.append(StringConstants.QUOTE);
        agentString.append("-javaagent:"); //$NON-NLS-1$
        agentString.append(javaAgentJar.getAbsolutePath());
        agentString.append(StringConstants.EQUALS_SIGN);
        agentString.append(chrononConfigFile.getAbsolutePath());
        agentString.append(StringConstants.QUOTE);

        // separate
        agentString.append(StringConstants.SPACE);

        // build the native agent configuration
        agentString.append(StringConstants.QUOTE);
        agentString.append("-agentpath:"); //$NON-NLS-1$
        agentString.append(nativeAgentFile.getAbsolutePath());
        agentString.append(StringConstants.QUOTE);
        return agentString.toString();
    }

    /**
     * @param relativeOrAbsoluteFilePath
     *            a relative or absolute path to a file
     * @return a file instance pointing to the given path;
     *         if a relative path was given it's set relative to the AUT working
     *         directory; the file pointing to might not exist!
     */
    private File getAbsolutePathFor(String relativeOrAbsoluteFilePath) {
        String workingDirPath = getMonitoringAttribute(
                AutConfigConstants.WORKING_DIR);
        File fileToUse = new File(relativeOrAbsoluteFilePath);
        if (!fileToUse.isAbsolute() && !StringUtils.isEmpty(workingDirPath)) {
            fileToUse = new File(workingDirPath, relativeOrAbsoluteFilePath);
        }
        return fileToUse;
    }
}
