﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ExceptionConstants
    {
        //Exit worth constants
        public const int EXIT_INVALID_ARGS = 1;

        public const int EXIT_AUT_NOT_FOUND = 3;        
        
        public const int AUT_START_ERROR = 25;

        public const int AUT_NOT_UIA_SUPPORTED = 26;

        //normal constants

        public const int AUT_STOP_FAILED = 5;

        public const int AUT_ACTIVE_CHECK_FAILED = 6;

        public const int SERVER_STOP_FAILED = 7;

        public const int REFLECTION_PARAMETER_NOT_EQUAL = 8;

        public const int REFLECTION_SIGNATURE_NOT_EQUAL = 9;

        public const int REFLECTION_ERROR = 10;


    }
}
