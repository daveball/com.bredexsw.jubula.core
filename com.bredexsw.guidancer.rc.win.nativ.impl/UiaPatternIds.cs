﻿
namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class UiaPatternIds
    {
        public const int UIA_DockPatternId = 10011;
        public const int UIA_ExpandCollapsePatternId = 10005;
        public const int UIA_GridItemPatternId = 10007;
        public const int UIA_GridPatternId = 10006;
        public const int UIA_InvokePatternId = 10000;
        public const int UIA_ItemContainerPatternId = 10019;
        public const int UIA_LegacyIAccessiblePatternId = 10018;
        public const int UIA_MultipleViewPatternId = 10008;
        public const int UIA_RangeValuePatternId = 10003;
        public const int UIA_ScrollItemPatternId = 10017;
        public const int UIA_ScrollPatternId = 10004;
        public const int UIA_SelectionItemPatternId = 10010;
        public const int UIA_SelectionPatternId = 10001;
        public const int UIA_SynchronizedInputPatternId = 10021;
        public const int UIA_TableItemPatternId = 10013;
        public const int UIA_TablePatternId = 10012;
        public const int UIA_TextPatternId = 10014;
        public const int UIA_TogglePatternId = 10015;
        public const int UIA_TransformPatternId = 10016;
        public const int UIA_ValuePatternId = 10002;
        public const int UIA_VirtualizedItemPatternId = 10020;
        public const int UIA_WindowPatternId = 10009;
    }
}
