﻿
namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Summary of all input IDs and Flags
    /// </summary>
    class InputConstants
    {
        /// <summary>
        /// Flag for pushing the left mouse button down
        /// </summary>
        public const int MOUSE_LEFTDOWN = 0x02;
        /// <summary>
        /// Flag for pushing the left mouse button up
        /// </summary>
        public const int MOUSE_LEFTUP = 0x04;
        /// <summary>
        /// Flag for pushing the right mouse button down
        /// </summary>
        public const int MOUSE_RIGHTDOWN = 0x08;
        /// <summary>
        /// Flag for pushing the right mouse button up
        /// </summary>
        public const int MOUSE_RIGHTUP = 0x10;
        /// <summary>
        /// Flag for pushing the wheel mouse button down
        /// </summary>
        public const int MOUSE_WHEELDOWN = 0x20;
                /// <summary>
        /// Flag for pushing the wheel mouse button up
        /// </summary>
        public const int MOUSE_WHEELUP = 0x40;
        /// <summary>
        /// Flag for pushing the num lock
        /// </summary>
        public const int VK_NUMLOCK = 0x90;
        /// <summary>
        /// Flag for pushing the scroll lock
        /// </summary>
        public const int VK_SCROLL = 0x91;
        /// <summary>
        /// Flag for pushing the caplock
        /// </summary>
        public const int VK_CAPITAL = 0x14;
        /// <summary>
        /// ID for clicking with the left mouse button
        /// </summary>
        public const int LEFT_MOUSE = 1;
        /// <summary>
        /// ID for clicking with the wheel mouse button
        /// </summary>
        public const int WHEEL_MOUSE = 2;
        /// <summary>
        /// ID for clicking with the right mouse button
        /// </summary>
        public const int RIGHT_MOUSE = 3;
        /// <summary>
        /// ID to toggle the num-lock
        /// </summary>
        public const int TOGGLE_NUMLOCK = 1;
        /// <summary>
        /// ID to toggle the cap-lock
        /// </summary>
        public const int TOGGLE_CAPLOCK = 2;
        /// <summary>
        /// ID to toggle the scroll-lock
        /// </summary>
        public const int TOGGLE_SCROLLLOCK = 3;
        /// <summary>
        /// The time interval between single clicks
        /// </summary>
        public const int INTERVAL_MS = 150;
        /// <summary>
        /// Flag for releasing a key
        /// </summary>
        public const int KEYEVENTF_KEYUP = 0x2;
        /// <summary>
        /// Flag for pressing a key down 
        /// </summary>
        public const int KEYEVENTF_KEYDOWN = 0x0;
        /// <summary>
        /// Flag for pressing the shift key
        /// </summary>
        public const int KEYFLAG_SHIFT = 0x10;
        /// <summary>
        /// Flag for pressing the control key
        /// </summary>
        public const int KEYFLAG_CONTROL = 0x11;
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send strg + end
        /// </summary>
        public const string VK_STRG_END = "^{END}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send strg + pos1
        /// </summary>
        public const string VK_STRG_POS1 = "^{HOME}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send strg + pos1
        /// </summary>
        public const string VK_POS1 = "{HOME}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send strg + page down
        /// </summary>
        public const string VK_STRG_PAGEDOWN = "^{PGDN}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send strg + a
        /// </summary>
        public const string VK_STRG_A = "^a";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send right
        /// </summary>
        public const string VK_RIGHT = "{RIGHT}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send strg + down
        /// </summary>
        public const string VK_STRG_DOWN = "^{DOWN}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send delete
        /// </summary>
        public const string VK_DEL = "{DEL}";
        /// <summary>
        /// Virtual Key ( for SendKeys ) to send esc
        /// </summary>
        public const string VK_ESC = "{ESC}";       

    }
}
