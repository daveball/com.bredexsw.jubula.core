﻿
/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;
using System.Collections.Generic;


namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Container class that contains a IUIAutomationElement and other container childs to represent the
    /// authierarchy.
    /// </summary>
    class NetHierarchyContainer
    {
        /// <summary>
        /// the parent container
        /// </summary>
        private NetHierarchyContainer m_parent;
        /// <summary>
        /// the IUIAutomationElement for this container
        /// </summary>
        private IUIAutomationElement m_component;
        /// <summary>
        /// list of child containers
        /// </summary>
        private List<NetHierarchyContainer> m_containerList = new List<NetHierarchyContainer>();
        /// <summary>
        /// the name for this container element
        /// </summary>
        private string m_name;
        /// <summary>
        /// the locator for this container element
        /// </summary>
        private string m_locatorPath;

        /// <summary>
        /// seperator for the locator
        /// </summary>
        public const char SEPERATOR = '/';
        /// <summary>
        /// begin of the index for the locator
        /// </summary>
        public const char INDEX_BEGIN = '[';
        /// <summary>
        /// end of the index for the locator
        /// </summary>
        public const char INDEX_END = ']';

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="ae">the automationElement</param>
        public NetHierarchyContainer(IUIAutomationElement ae)
        {
            this.m_component = ae;
            this.m_name = createName(null, ae);
            this.m_locatorPath = m_name + "[0]";
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="ae">name for the container</param>
        /// <param name="parent">parent container</param>
        /// <param name="index">index of the container</param>
        public NetHierarchyContainer(IUIAutomationElement ae, 
            NetHierarchyContainer parent, int index) {
            this.m_parent = parent;
            this.m_component = ae;
            this.m_name = createName(parent, ae);
            this.m_locatorPath = parent.getLocatorPath() 
                + SEPERATOR + m_name + INDEX_BEGIN + index + INDEX_END;
        }

        /// <summary>
        ///  gets the name
        /// </summary>
        /// <returns>the name</returns>
        public string getName()
        {
            return m_name;
        }

        /// <summary>
        /// Builds the name for this element. If the automation name is empty or represents 
        /// a generated ID, then we will use the short name for the control type. If the 
        /// name is already used by a prospective sibling, then an index will be added to 
        /// the end.
        /// </summary>
        /// <param name="plannedParent">
        /// The hierarchy container to which the element will be added. The returned name
        /// will be unique within the context of this parent at the time the method is 
        /// called.
        /// </param>
        /// <param name="ae">
        /// The element for which to generate a name.
        /// </param>
        /// <returns>
        /// a name for the given element that is unique within the context of the planned
        /// parent at the time this method is called.
        /// </returns>
        private string createName(NetHierarchyContainer plannedParent, IUIAutomationElement ae)
        {
            string name = ae.CurrentAutomationId;
            // Check whether we need to generate a name. See http://bugzilla.bredex.de/502 for 
            // reasons why we need to check whether the provided name is entirely digits.
            // Using TryParse for performance reasons. If this misses a corner case, then an
            // alternative solution would be regex 
            // (i.e. new Regex("^[0-9]+$").isMatch(name)
            ulong throwAwayTryParseResult;
            if (string.IsNullOrEmpty(name) || ulong.TryParse(name, out throwAwayTryParseResult))
            {
                name = ControlTypeUtils.getShortNameForId(m_component.CurrentControlType);
            }

            string nameWithSuffix = name;

            if (plannedParent != null)
            {
                NetHierarchyContainer[] siblings = plannedParent.m_containerList.ToArray();
                List<string> siblingNames = new List<string>();
                foreach (NetHierarchyContainer sibling in siblings)
                {
                    siblingNames.Add(sibling.getName());
                }

                int i = 0;
                while (siblingNames.Contains(nameWithSuffix))
                {
                    i++;
                    nameWithSuffix = name + "_" + i;
                }
            }

            return nameWithSuffix;
        }        

        /// <summary>
        /// Gets the locator path for the element
        /// </summary>
        /// <returns>the locator</returns>
        public string getLocatorPath()
        {
            return m_locatorPath;
        }       

        /// <summary>
        /// add a container as child
        /// </summary>
        /// <param name="component">the nethierarchycontainer</param>
        public void add(NetHierarchyContainer component)
        {
            m_containerList.Add(component);
        }

        /// <summary>
        /// Get the container element
        /// </summary>
        /// <returns>the automationElement</returns>
        public IUIAutomationElement getComponent()
        {
            return m_component;
        }

    }
}
