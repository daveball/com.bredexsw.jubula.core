﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using System.Windows.Forms;
namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    partial class InvisibleFormTouchMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // InvisibleFormTouchMode
            //    
            this.ClientSize = new System.Drawing.Size(10, 10);
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InvisibleForm";
            this.ShowInTaskbar = false;
            this.TransparencyKey = System.Drawing.SystemColors.AppWorkspace;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Shown += new System.EventHandler(this.InvisibleFormTouchMode_Shown);
            this.ResumeLayout(false);
        }
                

        #endregion

    }
}