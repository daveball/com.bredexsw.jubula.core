﻿using Interop.UIAutomationCore;
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ScrollbarValueScroller : IScroller
    {

        private IUIAutomationValuePattern m_scrollbarValuePattern;

        public ScrollbarValueScroller(
            IUIAutomationValuePattern scrollbarValuePattern)
        {
            m_scrollbarValuePattern = scrollbarValuePattern;
        }
        
        public void setScrollPercentage(double scrollPercentage)
        {
            m_scrollbarValuePattern.SetValue(
                Convert.ToString(scrollPercentage));
        }

        public double getScrollPercentage()
        {
            return Convert.ToDouble(m_scrollbarValuePattern.CurrentValue);
        }

    }
}
