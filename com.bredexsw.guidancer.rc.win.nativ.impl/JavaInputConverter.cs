﻿using System; // Convert
using System.Windows.Forms; // Keys

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    public class JavaInputConverter
    {
        /// <summary>Java AWT shift modifier mask</summary>
        private const int SHIFT_DOWN_MASK = 1 << 6;
        /// <summary>Java AWT control modifier mask</summary>
        private const int CTRL_DOWN_MASK = 1 << 7;
        /// <summary>Java AWT alt modifier mask</summary>
        private const int ALT_DOWN_MASK = 1 << 9;
        /// <summary>Java mouse button 1 (left mouse button)</summary>
        private const int MOUSE_BUTTON_1 = 1;
        /// <sumary>Java mouse button 2 (middle mouse button)</summary>
        private const int MOUSE_BUTTON_2 = 2;
        /// <summary>Java mouse button 3 (right mouse button)</summary>
        private const int MOUSE_BUTTON_3 = 3;

        /// <summary>
        /// Private constructor, because this is only an util class with static methods
        /// </summary>
        private JavaInputConverter()
        {
        }

        /// <summary>
        /// Converts a Java AWT key modifier with key code into windows key event arg class.
        /// </summary>
        /// <param name="keyModifierString">The Java key modifier as a string containing an integer value.</param>
        /// <param name="keyCodeString">The Java key code as a string containing an integer value.</param>
        /// <returns>The windows key event args, which represents the same given Java key modifier and key code.</returns>
        public static KeyEventArgs getWinKeyEventArgs(string keyModifierString, string keyCodeString)
        {
            int keyModifier = Convert.ToInt32(keyModifierString);
            int keyCode = Convert.ToInt32(keyCodeString);
            Keys winKeys = 0;
            // convert java AWT key modifier to windows key modifier
            if ((keyModifier & SHIFT_DOWN_MASK) > 0)
            {
                winKeys |= Keys.Shift;
            }
            if ((keyModifier & CTRL_DOWN_MASK) > 0)
            {
                winKeys |= Keys.Control;
            }
            if ((keyModifier & ALT_DOWN_MASK) > 0)
            {
                winKeys |= Keys.Alt;
            }
            // convert Java key code to windows keys, which have the same ASCII codes
            if (   (keyCode >= (int) Keys.D0     ) && (keyCode <= (int) Keys.D9)
                || (keyCode >= (int) Keys.A      ) && (keyCode <= (int) Keys.Z )
                || (keyCode >= (int) Keys.NumPad0) && (keyCode <= (int) Keys.F12))
            {
                winKeys |= (Keys) keyCode;
            }
            return new KeyEventArgs(winKeys);
        }

        /// <summary>
        /// Converts a Java mouse button into a windows mouse event args.
        /// </summary>
        /// <param name="button">The Java mouse button.</param>
        /// <returns>The windows mouse event args class, which represents the same Java mouse button with click count 1.</returns>
        public static MouseEventArgs getWinMouseEventArgs(string keyCodeString)
        {
            int keyCode = Convert.ToInt32(keyCodeString);
            MouseButtons mouseButtons = 0;
            switch (keyCode)
            {
                case MOUSE_BUTTON_1:
                    mouseButtons = MouseButtons.Left;
                    break;
                case MOUSE_BUTTON_2:
                    mouseButtons = MouseButtons.Middle;
                    break;
                case MOUSE_BUTTON_3:
                    mouseButtons = MouseButtons.Right;
                    break;
            }
            return new MouseEventArgs(mouseButtons, 1, 0, 0, 0);
        }
    }

}