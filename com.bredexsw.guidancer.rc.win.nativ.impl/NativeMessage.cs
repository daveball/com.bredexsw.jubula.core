﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// The message send by the java aut agent.
    /// </summary>
    class NativeMessage
    {
        /// <summary>
        /// name of the class which will be invoked
        /// </summary>
        public string className { get; set; }
        /// <summary>
        /// name of the method which will be invoked
        /// </summary>
        public string methodName { get; set; }
        /// <summary>
        /// the locator of a component
        /// </summary>
        public string locator { get; set; }
        /// <summary>
        /// list of parameters for the method
        /// </summary>
        public object[] parameters { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public NativeMessage()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="className">the class name</param>
        /// <param name="methodName">the method name</param>
        /// <param name="parameters">the parameters</param>
        public NativeMessage(string className, string methodName, object[] parameters)
        {
            this.className = className;
            this.methodName = methodName;
            this.parameters = parameters;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="className">the class name</param>
        /// <param name="methodName">the method name</param>
        /// <param name="locator">the locator of a component</param>
        /// <param name="parameters">the parameters</param>
        public NativeMessage(string className, string methodName, string locator, object[] parameters)
        {
            this.className = className;
            this.methodName = methodName;
            this.locator = locator;
            this.parameters = parameters;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>if the locator is set</returns>
        public bool isLocatorSet() 
        {
            return (this.locator != "");
        }
    }

}
