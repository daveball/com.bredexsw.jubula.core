﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Form which will be created over a supported element in the OM-Mode.
    /// Creates a green rectangle.
    /// </summary>
    public partial class InvisibleFormTouchMode : Form
    {
        /// <value>
        /// the rectangle which contains the sized and postition 
        /// from the supported element that lays under the invisibleform
        /// </value>
        private System.Drawing.Rectangle m_rec;
        /// <value>
        /// Thread that listens if the mouse left the area from the invsible form
        /// </value>
        delegate void closeDelegate();
        /// <value>
        /// Thread that waits x time before he closes the form, needed to highlight component
        /// </value>
        private Thread m_timerThread;
        /// <value>
        /// shows how long the window should be open, needed to highlight component
        /// </value>
        private static int time = 1000;
        /// <summary>
        /// The automationcondition
        /// </summary>
        private IUIAutomationCondition m_condition;
       
        /// <summary>
        /// Constructor for the normal OM.
        /// </summary>
        /// <param name="rec">
        /// Rectangle which contains the sized and postition 
        /// from the supported element that lays under the invisibleform
        /// </param>
        public InvisibleFormTouchMode(IUIAutomationCondition condition)
        {
            this.m_condition = condition;
            this.m_rec = getAUTBoundsRec();
            this.TopMost = true;
            this.Opacity = .01;
            this.BackColor = Color.Black;
            InitializeComponent();
        }
    
        /// <summary>
        /// Constructor for highlighting component.
        /// </summary>
        /// <param name="rec">position for the green rectangle</param>
        /// <param name="timer">how long the rectangle should be displayed</param>
        public InvisibleFormTouchMode(System.Drawing.Rectangle rec, int timer)
        {
            time = timer;
            m_rec = rec;
            this.TopMost = true;
            this.Opacity = 1;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            InitializeComponent();
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form_Paint);
            m_timerThread = new Thread(new ThreadStart(this.waitTime));
            m_timerThread.Start();
        }

        /// <summary>
        /// Get the parent AUT window bounds.
        /// </summary>
        /// <returns>Rectangle with the parent AUT window bounds</returns>
        private Rectangle getAUTBoundsRec()
        {
            IUIAutomationElement desktop = ComUtils.AUTOMATION.GetRootElement();
            IUIAutomationElement autWindow = desktop.FindFirst(TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ProcessIdPropertyId, WinRobotImpl.getProcessId()));
            return new Rectangle(autWindow.CurrentBoundingRectangle.left,
                autWindow.CurrentBoundingRectangle.top,
                autWindow.CurrentBoundingRectangle.right,
                autWindow.CurrentBoundingRectangle.bottom);
        }

        ///<summary>
        /// wait x time and then close the form
        /// </summary>
        private void waitTime()
        {
            Thread.Sleep(time);
            closeDelegate d1 = closeForm;
            try
            {
                this.Invoke(d1);
            }
            catch (InvalidOperationException)
            {

            }
        }

        public void delegateClose()
        {
            
            WinRobotImpl robo = new WinRobotImpl();
            robo.setAUTFocus();
            Thread.Sleep(2000);
            this.TopMost = false;
            this.Visible = false;
            closeDelegate d1 = closeForm;
            try
            {
                this.Invoke(d1);
            }
            catch (InvalidOperationException)
            {

            }       
        }

        /// <summary>
        /// draws the green rectangle as form border
        /// </summary>
        /// <param name="sender">the sender object</param>
        /// <param name="e">the paint event</param>
        private void Form_Paint(object sender, PaintEventArgs e)
        {            
            System.Drawing.Graphics graphicsObj;
            graphicsObj = this.CreateGraphics();
            Pen myPen = new Pen(System.Drawing.Color.Green, 7);
            graphicsObj.DrawRectangle(myPen, this.ClientRectangle);           
        }

        /// <summary>
        /// shown event where the invisible form is tabbed gets the bounds
        /// </summary
        /// <param name="sender">the sender</param>
        /// <param name="e">the event</param>
        private void InvisibleFormTouchMode_Shown(Object sender, EventArgs e)
        {
            this.Bounds = m_rec;
            IUIAutomationElement ae = 
                ComUtils.AUTOMATION.ElementFromHandle(this.Handle);
            ae.SetFocus();
        }
               
        /// <summary>
        /// Closes this form.
        /// </summary>
        private void closeForm()
        {
            while (this.Handle == IntPtr.Zero)
            {
                Thread.Sleep(100);
                this.Refresh();
            }
            this.Close();
        }
    }
}
