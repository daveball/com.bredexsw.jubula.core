﻿using Interop.UIAutomationCore;
using System;
using System.Runtime.InteropServices;
using System.Windows;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ComUtils
    {
        public static readonly IUIAutomation AUTOMATION =
            new CUIAutomation();

        // FIXME it would be nice to be able to use UIA_ScrollPatternNoScroll,
        //       as documented at: 
        //       http://msdn.microsoft.com/en-us/library/windows/desktop/ee671345%28v=vs.85%29.aspx
        //       but I cannot find the constant anywhere.
        public static readonly double NO_SCROLL = -1.0;

        public static readonly int UIA_E_ELEMENTNOTAVAILABLE = -2147220991;

        public static readonly int UIA_E_ELEMENTNOTENABLED = -2147220992;

        public static readonly int UIA_E_NOCLICKABLEPOINT = -2147220990;

        /// <summary>
        /// Converts a tagRECT object to a Rect object
        /// </summary>
        /// <param name="nativeRect">a tagRECT</param>
        /// <returns>the converted Rect</returns>
        public static Rect convertRect(tagRECT nativeRect)
        {
            return new Rect(nativeRect.left, nativeRect.top, 
                nativeRect.right - nativeRect.left, 
                nativeRect.bottom - nativeRect.top);
        }

        /// <summary>
        /// Intersects the rectangle bounds of a component with his parent bounds. 
        /// Used to get the "real" bounds of a component if his own bounds are relative to his parents.
        /// </summary>
        /// <param name="ae">the component</param>
        /// <returns>the absolute bounds of the component</returns>
        public static Rect getClippedBoundsRect(IUIAutomationElement ae)
        {
            IUIAutomationElement parent = ComUtils.AUTOMATION.RawViewWalker.GetParentElement(ae);
            if (parent != null)
            {
                Rect aeRec = convertRect(ae.CurrentBoundingRectangle);
                Rect rectangleBounds = Rect.Intersect(convertRect(parent.CurrentBoundingRectangle), aeRec);

                return rectangleBounds;
            }
            else
            {
                return convertRect(ae.CurrentBoundingRectangle);
            }
        }

        /// <summary>
        /// Checks if the component is visible on the screen.        /// 
        /// </summary>
        /// <param name="ae">the component to check</param>
        /// <returns>if the component and all his parents are visible</returns>
        public static bool isOffscreen(IUIAutomationElement ae)
        {
            if (Convert.ToBoolean(ae.CurrentIsOffscreen))
            {
                return true;
            }
            else
            {
                BXAutomationElement bxElement = new BXAutomationElement(ae);
                BXAutomationElement bxRoot = new BXAutomationElement(ComUtils.AUTOMATION.GetRootElement());
                while (bxElement != null && !bxElement.Equals(bxRoot))
                {
                    Rect rect = getClippedBoundsRect(bxElement.automationElement);
                    if (rect.IsEmpty)
                    {
                        return true;
                    }
                    else
                    {
                        bxElement = new BXAutomationElement(
                            ComUtils.AUTOMATION.RawViewWalker.GetParentElement(
                            bxElement.automationElement));            
                    }
                }                
                return false;
            }
        }


        public static bool isExceptionOfType(COMException e, int typeId)
        {
            return e.ErrorCode == typeId;
        }

        public static bool isEquals(IUIAutomationElement aeOne, IUIAutomationElement aeTwo)
        {
            if (aeOne == null || aeTwo == null)
            {
                // null-safe equality check will return true if both are null
                return aeOne == aeTwo;
            }
            try
            {
                return Convert.ToBoolean(AUTOMATION.CompareElements(aeOne, aeTwo));
            }
            catch (COMException)
            {
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }

        }
    }
}
