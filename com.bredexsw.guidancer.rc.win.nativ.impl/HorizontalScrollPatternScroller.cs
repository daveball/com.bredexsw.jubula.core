﻿using Interop.UIAutomationCore;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class HorizontalScrollPatternScroller : AbstractScrollPatternScroller
    {
        public HorizontalScrollPatternScroller(
            IUIAutomationScrollPattern scrollPattern)
                : base(scrollPattern)
        {

        }

        public override void setScrollPercentage(double scrollPercentage)
        {
            getScrollPattern().SetScrollPercent(
                scrollPercentage, ComUtils.NO_SCROLL);
        }

        public override double getScrollPercentage()
        {
            return getScrollPattern().CurrentHorizontalScrollPercent;
        }
    }
}
