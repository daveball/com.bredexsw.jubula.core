﻿using Interop.UIAutomationCore;
using log4net;
using System.Collections.Generic;


namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ControlTypeUtils
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ControlTypeUtils));

        private static readonly Dictionary<int, string> CONTROL_TYPE_DICT = new Dictionary<int, string>()
        {
            {UiaControlTypeIds.UIA_ButtonControlTypeId, "button"},
            {UiaControlTypeIds.UIA_CalendarControlTypeId, "calendar"},
            {UiaControlTypeIds.UIA_CheckBoxControlTypeId, "check box"},
            {UiaControlTypeIds.UIA_ComboBoxControlTypeId, "combo box"},
            {UiaControlTypeIds.UIA_CustomControlTypeId, "custom"},
            {UiaControlTypeIds.UIA_DataGridControlTypeId, "table"}, //we don't have any data grid controltype on java rc site only "table". To find there our implclasses we have to confirm it here as a table controltype
            {UiaControlTypeIds.UIA_DataItemControlTypeId, "data item"},
            {UiaControlTypeIds.UIA_DocumentControlTypeId, "document"},
            {UiaControlTypeIds.UIA_EditControlTypeId, "edit"},
            {UiaControlTypeIds.UIA_GroupControlTypeId, "group"},
            {UiaControlTypeIds.UIA_HeaderControlTypeId, "header"},
            {UiaControlTypeIds.UIA_HeaderItemControlTypeId, "header item"},
            {UiaControlTypeIds.UIA_HyperlinkControlTypeId, "hyperlink"},
            {UiaControlTypeIds.UIA_ImageControlTypeId, "image"},
            {UiaControlTypeIds.UIA_ListControlTypeId, "list"},
            {UiaControlTypeIds.UIA_ListItemControlTypeId, "list item"},
            {UiaControlTypeIds.UIA_MenuBarControlTypeId, "menu bar"},
            {UiaControlTypeIds.UIA_MenuControlTypeId, "menu"},
            {UiaControlTypeIds.UIA_MenuItemControlTypeId, "menu item"},
            {UiaControlTypeIds.UIA_PaneControlTypeId, "pane"},
            {UiaControlTypeIds.UIA_ProgressBarControlTypeId, "progress bar"},
            {UiaControlTypeIds.UIA_RadioButtonControlTypeId, "radio button"},
            {UiaControlTypeIds.UIA_ScrollBarControlTypeId, "scroll bar"},
            {UiaControlTypeIds.UIA_SeparatorControlTypeId, "separator"},
            {UiaControlTypeIds.UIA_SliderControlTypeId, "slider"},
            {UiaControlTypeIds.UIA_SpinnerControlTypeId, "spinner"},
            {UiaControlTypeIds.UIA_SplitButtonControlTypeId, "split button"},
            {UiaControlTypeIds.UIA_StatusBarControlTypeId, "status bar"},
            {UiaControlTypeIds.UIA_TabControlTypeId, "tab"},
            {UiaControlTypeIds.UIA_TabItemControlTypeId, "tab item"},
            {UiaControlTypeIds.UIA_TableControlTypeId, "table"},
            {UiaControlTypeIds.UIA_TextControlTypeId, "text"},
            {UiaControlTypeIds.UIA_ThumbControlTypeId, "thumb"},
            {UiaControlTypeIds.UIA_TitleBarControlTypeId, "title bar"},
            {UiaControlTypeIds.UIA_ToolBarControlTypeId, "tool bar"},
            {UiaControlTypeIds.UIA_ToolTipControlTypeId, "tool tip"},
            {UiaControlTypeIds.UIA_TreeControlTypeId, "tree"},
            {UiaControlTypeIds.UIA_TreeItemControlTypeId, "tree item"},
            {UiaControlTypeIds.UIA_WindowControlTypeId, "window"},
        };

        public static string getShortNameForId(int controlTypeId)
        {
            string retVal;
            if (CONTROL_TYPE_DICT.TryGetValue(controlTypeId, out retVal))
            {
                return retVal;
            }

            LOG.WarnFormat("No short name found for ID {0}. Returning 'unrecognized type'.", controlTypeId);
            return "unrecognized type";
        }

        /// <summary>
        /// Convert a controle type name from string to the Automation-ControlType
        /// </summary>
        public static int stringToControlType(string controlName)
        {
            switch (controlName)
            {             
                // WPF components
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ButtonWPF": return UiaControlTypeIds.UIA_ButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.EditWPF": return UiaControlTypeIds.UIA_EditControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.DocumentWPF": return UiaControlTypeIds.UIA_DocumentControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TextWPF": return UiaControlTypeIds.UIA_TextControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TreeWPF": return UiaControlTypeIds.UIA_TreeControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.RadioButtonWPF": return UiaControlTypeIds.UIA_RadioButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.CheckBoxWPF": return UiaControlTypeIds.UIA_CheckBoxControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TableWPF": return UiaControlTypeIds.UIA_DataGridControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TabWPF": return UiaControlTypeIds.UIA_TabControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ListWPF": return UiaControlTypeIds.UIA_ListControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBoxWPF": return UiaControlTypeIds.UIA_ComboBoxControlTypeId;

                // WinForms components
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ButtonWinForm": return UiaControlTypeIds.UIA_ButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.EditwinForm": return UiaControlTypeIds.UIA_EditControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.DocumentWinForm": return UiaControlTypeIds.UIA_DocumentControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TextWinForm": return UiaControlTypeIds.UIA_TextControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TreeWinForm": return UiaControlTypeIds.UIA_TreeControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.RadioButtonWinForm": return UiaControlTypeIds.UIA_RadioButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.CheckBoxWinForm": return UiaControlTypeIds.UIA_CheckBoxControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TableWinForm": return UiaControlTypeIds.UIA_TableControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TabWinForm": return UiaControlTypeIds.UIA_TabControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ListWinForm": return UiaControlTypeIds.UIA_ListControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBoxWinForm": return UiaControlTypeIds.UIA_ComboBoxControlTypeId;

                // Win32 components
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ButtonWin32": return UiaControlTypeIds.UIA_ButtonControlTypeId;

                // XAML components
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ButtonXAML": return UiaControlTypeIds.UIA_ButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TextXAML": return UiaControlTypeIds.UIA_TextControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.CheckBoxXAML": return UiaControlTypeIds.UIA_CheckBoxControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.EditXAML": return UiaControlTypeIds.UIA_EditControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.RadioButtonXAML": return UiaControlTypeIds.UIA_RadioButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ListXAML": return UiaControlTypeIds.UIA_ListControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBoxXaml": return UiaControlTypeIds.UIA_ComboBoxControlTypeId;

                // InternetExplorer components
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ButtonInternetExplorer": return UiaControlTypeIds.UIA_ButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.TextInternetExplorer": return UiaControlTypeIds.UIA_TextControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.CheckBoxInternetExplorer": return UiaControlTypeIds.UIA_CheckBoxControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.EditInternetExplorer": return UiaControlTypeIds.UIA_EditControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.RadioButtonInternetExplorer": return UiaControlTypeIds.UIA_RadioButtonControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ListInternetExplorer": return UiaControlTypeIds.UIA_ListControlTypeId;
                case "com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBoxInternetExplorer": return UiaControlTypeIds.UIA_ComboBoxControlTypeId;
                default: return 0;
            }
        }
    }
}
