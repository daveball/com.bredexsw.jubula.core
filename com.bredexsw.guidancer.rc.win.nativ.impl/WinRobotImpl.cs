﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;


namespace com.bredexsw.guidancer.rc.win.nativ.impl
{

    /// <summary>
    /// Robot for all actions that involes the AUT like starting and stoping
    /// </summary>
    class WinRobotImpl : IWinRobot
    {
        /// <summary>
        /// the process object which will be used to start / stop the AUT
        /// </summary>
        private static Process process;
        /// <summary>
        /// the process id from the started AUT.
        /// </summary>
        private static int processID;
        /// <summary>
        /// the index position of the automation element for the preAction method
        /// </summary>
        private const int AUTOMATION_ELEMENT_POSITION = 0;
        /// <summary>
        /// the index position of the method name which will be invokes in the preAction method
        /// </summary>
        private const int METHOD_NAME_POSITION = 0;
        /// <summary>
        /// the index position of the locator for the preAction method
        /// </summary>
        private const int LOCATOR_POSITION = 1;
        /// <summary>
        /// constant to check if the automation element is created with wpf
        /// </summary>
        private const string WPF_FRAMEWORK = "WPF";
        /// <summary>
        /// The maximum difference between a scrollable component's upper bound (x + width, or y + height)
        /// and its scrollbar's upper bound that will still allow that scrollbar to be positively identified
        /// as a vertical or horizontal scrollbar. So far, little effort has gone into determining this value,
        /// so any necessary changes are welcome. Just be careful not to set it so high that the incorrect
        /// scrollbar could be identified (i.e. horizontal scrollbar mistaken for vertical scrollbar, or 
        /// vice versa).
        /// </summary>
        private const int SCROLLBAR_LOCATION_TOLERANCE = 5;
        /// <summary>
        /// The current menu / menuItem / treeItem of the aut, neeed for name path selection
        /// </summary>
        private static IUIAutomationElement currentItem;
        /// <summary>
        /// Bool if AUT is a win app, else false
        /// </summary>
        public static bool win8App { get; set; }
        /// <summary>
        /// List of chars which need to be placed in a "{}" within the input text action
        /// </summary>
        private char[] exceptionChars = { '+', '^', '%', '~', '(', ')', '{', '}', '[', ']' };
        /// <summary>
        /// Name of the modernUI autstartet exe file
        /// </summary>
        private const string MODERNUI_AUTSTART_EXE = "com.bredexsw.guidancer.rc.win.nativ.modernui.autstart.exe";
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NETServer));

        /// <summary>
        /// Starts the AUT.
        /// </summary>
        /// <param name="autpath">
        /// The path to the AUT.
        /// </param>
        /// <param name="args">
        /// Arguments for the AUT-Start.
        /// </param>
        public void startAUT(string autPath, string workDir, string args)
        {
            process = new Process();
            try
            {
                if (workDir != null)
                {
                    process.StartInfo.WorkingDirectory = workDir;
                }
                process.StartInfo.FileName = autPath;
            }
            catch (ArgumentNullException)
            {
                throw new ServerException("AUT filename is null.",
                    ServerExceptionConstants.EXIT_AUT_NOT_FOUND);
            }

            if (!args.Equals(""))
            {
                try
                {
                    process.StartInfo.Arguments = args;
                }
                catch (ArgumentNullException)
                {
                    throw new ServerException("AUT arguments are null.",
                        ServerExceptionConstants.EXIT_INVALID_ARGS);
                }
            }

            try
            {
                process.Start();
                processID = process.Id;
            }
            catch (Win32Exception)
            {
                throw new ServerException("During the Start of the AUT, an error occured.",
                    ServerExceptionConstants.AUT_START_ERROR);
            }

            while (process.MainWindowHandle == IntPtr.Zero)
            {
                Thread.Sleep(100);
                process.Refresh();
            }

            IUIAutomationElement mainform =
                ComUtils.AUTOMATION.ElementFromHandle(process.MainWindowHandle);

            if (mainform == null)
            {
                throw new ServerException("Mainform for the UIA is null.",
                    ServerExceptionConstants.AUT_NOT_UIA_SUPPORTED);
            }
            win8App = false;
        }

        /// <summary>
        /// Launch an App and return its process id.
        /// </summary>
        /// <param name="autName">The app name</param>
        /// <param name="args">The arguments that are given to the AUT on start</param>
        public static void startAUT(string autName, string args)
        {
            RegistryNodeInfo appRegInfo = ModernUIRegistySearcher.GetLaunchIDByName(autName, true);
            if (appRegInfo == null)
            {
                appRegInfo = ModernUIRegistySearcher.GetLaunchIDByName(autName, false);
            }
            if (appRegInfo != null && appRegInfo.LaunchID != null && appRegInfo.DisplayName != null)
            {
                Process launchProcess = new Process();
                try
                {
                    launchProcess.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
                }
                catch (ArgumentNullException)
                {
                    throw new ServerException("AUT working directory is null.",
                        ServerExceptionConstants.EXIT_AUT_NOT_FOUND);
                }
                try
                {
                    launchProcess.StartInfo.FileName = MODERNUI_AUTSTART_EXE;
                }
                catch (ArgumentNullException)
                {
                    throw new ServerException("AUT name is null.",
                        ServerExceptionConstants.EXIT_AUT_NOT_FOUND);
                }

                try
                {
                    launchProcess.StartInfo.Arguments = appRegInfo.LaunchID;
                    if (args != null && !args.Equals(""))
                    {
                        launchProcess.StartInfo.Arguments += args;
                    }
                }
                catch (ArgumentNullException)
                {
                    throw new ServerException("AUT arguments are null.",
                        ServerExceptionConstants.EXIT_INVALID_ARGS);
                }
                launchProcess.StartInfo.UseShellExecute = false;
                launchProcess.Start();
                launchProcess.WaitForExit();
                try
                {
                    processID = launchProcess.ExitCode;
                    process = Process.GetProcessById(processID);

                }
                catch (Exception)
                {
                    throw new ServerException("Cannot get AUT process.",
                        ServerExceptionConstants.AUT_START_ERROR);
                }
                if (processID == 0)
                {
                    throw new ServerException("AUT not launched. Could not start " +
                    "AUT Process.",
                        ServerExceptionConstants.AUT_START_ERROR);
                }
                win8App = true;
            }
            else
            {
                throw new ServerException("Could not find the AUT",
                    ServerExceptionConstants.AUT_START_ERROR);
            }
        }

        /// <summary>
        /// Stops the AUT
        /// </summary>
        public void stopAUT()
        {
            if (!process.HasExited)
            {
                try
                {
                    process.Kill();
                }
                catch (Win32Exception)
                {
                    throw new ServerException("AUT could not be terminated.",
                        ServerExceptionConstants.AUT_STOP_FAILED);
                }
                catch (InvalidOperationException)
                {
                    throw new ServerException("The process has already exited.",
                        ServerExceptionConstants.AUT_STOP_FAILED);
                }
            }
        }
        /// <summary>
        /// Checks  if the AUT is started / active.
        /// </summary>
        public bool isAUTactive()
        {
            if (process == null)
            {
                return false;
            }
            else
            {
                process.Refresh();

                try
                {
                    if (process.HasExited)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (InvalidOperationException)
                {
                    throw new ServerException("There is no process associated.",
                        ServerExceptionConstants.AUT_ACTIVE_CHECK_FAILED);
                }
                catch (Win32Exception)
                {
                    throw new ServerException("The exit code for the process could not be retrieved.",
                        ServerExceptionConstants.AUT_ACTIVE_CHECK_FAILED);
                }
            }

        }

        /// <summary>
        /// Stops this Win server
        /// </summary>
        public void stopServer()
        {
            if (ObjectMapping.isMapping())
            {
                ObjectMapping.closeMapping();
            }

            Environment.Exit(0);
        }

        /// <summary>
        /// Sets the AUT in the focus so it will be the most front window on the desktop
        /// </summary>
        /// <param name="hWnd">the window handle id</param>
        /// <returns>if it was successfull</returns>
        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hWnd, uint Msg);
        private const uint SW_SHOW = 0x05;
        private const uint SW_RESTORE = 0x09;
        public void setAUTFocus()
        {
            try
            {
                IUIAutomationElement mainform = ComUtils.AUTOMATION.ElementFromHandle(process.MainWindowHandle);

                if (mainform == null)
                {
                    throw new ServerException("No AUT window is available.",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
                //restore minimzied window
                else if (ComUtils.isOffscreen(mainform))
                {
                    ShowWindow(process.MainWindowHandle, SW_RESTORE);
                }

                mainform.SetFocus();
                if (!mainform.CurrentName.Equals(ComUtils.AUTOMATION.GetFocusedElement().CurrentName))
                {
                    mainform.SetFocus();
                }
            }
            catch (Exception)
            {

            }

        }

        /// <summary>
        /// Method that will be called, if the action-method requires an IUIAutomationElement to work with.
        /// Fixme: This method should be removed; see also loop in method initConnection() of Communicator class.
        /// </summary>
        /// <param name="param">parameters that contains the action-method name, the locator of
        /// the IUIAutomationElement and other parameters.</param>
        /// <returns>the return value of the called action method</returns>
        public object preAction(object[] param)
        {
            //the param array must contain on the first place the actionMethod-name which will be invoked 
            //after the preAction and on the second place the locator string.
            // !! dont change the order and the indexes here !!
            Type type = this.GetType();
            MethodInfo methodinfo = type.GetMethod(param[METHOD_NAME_POSITION].ToString());

            object[] methodParams = new object[param.Length - 1];
            for (int i = 1; i < param.Length - 1; i++)
            {
                methodParams[i] = param[i + 1];
            }

            if (param[LOCATOR_POSITION] != null)
            {
                methodParams[AUTOMATION_ELEMENT_POSITION] =
                    AUTHierarchyRobotImpl.findComponent(param[LOCATOR_POSITION].ToString());
            }

            return methodinfo.Invoke(this, methodParams);
        }

        /// <summary>
        /// Inserts a text for e.g in a textfield in the first index without deleting the old text
        /// </summary>
        /// <param name="insertText">the text that will be inserted</param>
        public void inputText(string insertText)
        {
            char[] charArr = insertText.ToCharArray();

            for (int i = 0; i < charArr.Length; i++)
            {
                SendKeys.SendWait(this.checkInputChar(charArr[i])); // insert text    
            }

        }

        private string checkInputChar(char charInput)
        {
            if (this.exceptionChars.Contains(charInput))
            {
                return "{" + charInput + "}";
            }
            return charInput.ToString();
        }

        /// <summary>
        /// Inserts a text e.g in a textfield in the first index without
        /// first changing the text cursor position.
        /// </summary>
        /// <param name="insertText">the text that will be inserted</param>
        public void inputTextInCellEditor(string insertText)
        {
            SendKeys.SendWait(insertText);
        }

        public void insertTextAfterIndex(string insertText, int index)
        {
            SendKeys.SendWait(InputConstants.VK_STRG_POS1); // press strg + home / pos1

            for (int i = 0; i < index; i++)
            {
                SendKeys.SendWait(InputConstants.VK_RIGHT); // press right arrow button
            }
            SendKeys.SendWait(insertText); // insert text
        }

        /// <summary>
        /// Select all text from a component with text input
        /// </summary>
        /// <param name="ae">the component</param>
        public void selectAll()
        {
            SendKeys.SendWait(InputConstants.VK_STRG_A);    // select all text
        }

        /// <summary>
        /// Replaces the old text with a new one.
        /// </summary>
        /// <param name="locator">the locator</param>
        /// <param name="insertText">the new text</param>
        public void replaceText(string insertText)
        {
            SendKeys.SendWait(InputConstants.VK_STRG_A);    // select all text
            SendKeys.SendWait(InputConstants.VK_DEL); // delete selection
            SendKeys.SendWait(insertText); // insert text
        }

        /// <summary>
        /// Performs a click in the middel of the component.
        /// </summary>
        /// <param name="clickCount">count of the clicks</param>
        /// <param name="mouseButton">mouse button</param>        
        public static void click(int clickCount, int mouseButton)
        {
            if (mouseButton == InputConstants.LEFT_MOUSE)
            {
                clickLeftMouseKey(clickCount);
            }
            else if (mouseButton == InputConstants.WHEEL_MOUSE)
            {
                clickMouseWheel(clickCount);
            }
            else if (mouseButton == InputConstants.RIGHT_MOUSE)
            {
                clickRightMouseKey(clickCount);
            }
        }

        /// <summary>
        /// Opens the context menu and saves the current opend menu.
        /// Used for textpath actions.
        /// </summary>
        /// <param name="ae">The component with the context menu.</param>
        /// <param name="checkMouse">if the mouse position should be checked</param>
        public void initContextMenu(IUIAutomationElement ae, int button, int x, int y,
            bool checkMouse)
        {

            if (!checkMouse || !isMouseInComponent(ae))
            {
                moveMouseToPos(x, y);
            }

            click(1, button);

            currentItem = getCurrentItemFromComponent(ae);

            //performance problems could lead to the situation that the first click wasn't performed correctly, try it a second time
            if (currentItem == null)
            {
                click(1, button);
                currentItem = getCurrentItemFromComponent(ae);

                if (currentItem == null)
                {
                    throw new ServerException("Contextmenu not found.");
                }

            }
            MenuImplClass.currentItem = currentItem;
        }

        /// <summary>
        /// Seaches for the conext menu of a component.
        /// </summary>
        /// <param name="component">the component</param>
        /// <returns>the whole contextmenu as a single item</returns>
        private IUIAutomationElement getCurrentItemFromComponent(IUIAutomationElement component)
        {
            IUIAutomationCondition menuCond = ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ControlTypePropertyId,
                UiaControlTypeIds.UIA_MenuControlTypeId);

            IUIAutomationCondition idCondition = ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ProcessIdPropertyId, processID);

            IUIAutomationCondition andCond = ComUtils.AUTOMATION.CreateAndCondition(
                menuCond, idCondition);

            if (isWPFElement(component) || win8App)
            {
                Thread.Sleep(10);
                IUIAutomationElement window = getForegroundAutomationWindow();
                currentItem = window.FindFirst(TreeScope.TreeScope_Descendants, menuCond);
            }
            else
            {
                Thread.Sleep(10);
                currentItem = ComUtils.AUTOMATION.GetRootElement().FindFirst(
                    TreeScope.TreeScope_Children, andCond);
            }

            return currentItem;
        }

        /// <summary>
        /// Gets all menuItem entrys from a given menuItem.
        /// </summary>        
        /// <returns>All child menuItems</returns>
        public string[] getCurrentMenuItems()
        {
            List<string> itemNames = new List<string>();
            IUIAutomationElementArray menuItems = this.getCurrentMenuItemsAsUIA();

            //try a second time, the null could be a performance problem
            if (menuItems == null)
            {
                throw new ServerException("No contextmenu items found.");
            }

            for (int i = 0; i < menuItems.Length; i++)
            {
                itemNames.Add(menuItems.GetElement(i).CurrentName);
            }

            return itemNames.ToArray();
        }

        private IUIAutomationElementArray getCurrentMenuItemsAsUIA()
        {
            IUIAutomationElementArray menuItems = MenuImplClass.getMenuItemsFromElement(
                MenuImplClass.currentItem);

            if (menuItems == null || menuItems.Length == 0)
            {
                menuItems = MenuImplClass.getMenuItemsFromElement(MenuImplClass.getContextMenuMenuWithTimeout());
            }

            return menuItems;
        }

        /// <summary>
        /// Moves the cursor mouse position to the center of the component.
        /// </summary>
        /// <param name="ae">the automation element where the mouse should be moved</param>
        public static void moveMouseToElement(IUIAutomationElement ae)
        {
            tagPOINT clickablePoint = new tagPOINT();
            try
            {
                int result = ae.GetClickablePoint(out clickablePoint);
                // not sure whether it is necessary to check the result
                // value or catch and check exception code, so we're
                // doing both
                if (result == ComUtils.UIA_E_NOCLICKABLEPOINT)
                {
                    throw new ServerException("No clickable point.");
                }
                Cursor.Position = new System.Drawing.Point(
                    clickablePoint.x,
                    clickablePoint.y);
            }
            catch (COMException ce)
            {
                // not sure whether it is necessary to check the result
                // value or catch and check exception code, so we're
                // doing both
                if (ce.ErrorCode == ComUtils.UIA_E_NOCLICKABLEPOINT)
                {
                    throw new ServerException("No clickable point.");
                }

                throw ce;
            }
        }

        /// <summary>
        /// move the mouse to a positon
        /// </summary>
        /// <param name="x">x position</param>
        /// <param name="y">y position</param>
        public static void moveMouseToPos(int x, int y)
        {
            Cursor.Position = new System.Drawing.Point(x, y);
        }

        [DllImport("user32.dll")]
        public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);

        /// <summary>
        /// Clicks the real right mouse key
        /// </summary>
        /// <param name="clickCount">count of clicks</param>
        public static void clickRightMouseKey(int clickCount)
        {
            for (int i = 0; i < clickCount; i++)
            {
                mouse_event(InputConstants.MOUSE_RIGHTDOWN | InputConstants.MOUSE_RIGHTUP, 0, 0, 0, 0);
                Thread.Sleep(InputConstants.INTERVAL_MS);
            }
        }

        /// <summary>
        /// Clicks the real left mouse key
        /// </summary>
        /// <param name="clickCount">count of clicks</param>
        public static void clickLeftMouseKey(int clickCount)
        {
            for (int i = 0; i < clickCount; i++)
            {
                mouse_event(InputConstants.MOUSE_LEFTDOWN | InputConstants.MOUSE_LEFTUP, 0, 0, 0, 0);
                Thread.Sleep(InputConstants.INTERVAL_MS);
            }
        }

        /// <summary>
        /// Clicks the real mouse wheel
        /// </summary>
        /// <param name="clickCount">count of clicks</param>
        public static void clickMouseWheel(int clickCount)
        {
            for (int i = 0; i < clickCount; i++)
            {
                mouse_event(InputConstants.MOUSE_WHEELDOWN | InputConstants.MOUSE_WHEELUP, 0, 0, 0, 0);
                Thread.Sleep(InputConstants.INTERVAL_MS);
            }
        }

        /// <summary>
        /// Press a mouse button and hold it down
        /// </summary>
        /// <param name="mouseButton">the mouse button id</param>
        public void pressMouse(int mouseButton)
        {
            if (mouseButton == InputConstants.LEFT_MOUSE)
            {
                mouse_event(InputConstants.MOUSE_LEFTDOWN, 0, 0, 0, 0);
            }
            else if (mouseButton == InputConstants.WHEEL_MOUSE)
            {
                mouse_event(InputConstants.MOUSE_WHEELDOWN, 0, 0, 0, 0);
            }
            else if (mouseButton == InputConstants.RIGHT_MOUSE)
            {
                mouse_event(InputConstants.MOUSE_RIGHTDOWN, 0, 0, 0, 0);
            }
        }

        /// <summary>
        /// Release a mouse button
        /// </summary>
        /// <param name="mouseButton">the mouse button id</param>
        public void releaseMouse(int mouseButton)
        {
            if (mouseButton == InputConstants.LEFT_MOUSE)
            {
                mouse_event(InputConstants.MOUSE_LEFTUP, 0, 0, 0, 0);
            }
            else if (mouseButton == InputConstants.WHEEL_MOUSE)
            {
                mouse_event(InputConstants.MOUSE_WHEELUP, 0, 0, 0, 0);
            }
            else if (mouseButton == InputConstants.RIGHT_MOUSE)
            {
                mouse_event(InputConstants.MOUSE_RIGHTUP, 0, 0, 0, 0);
            }
        }

        [DllImport("user32.dll", EntryPoint = "keybd_event", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern void keybd_event(byte vk, byte bscan, uint flags, int extrainfo);
        /// <summary>
        /// Press a button and hold it down
        /// </summary>
        /// <param name="button">the button id</param>
        public static void pressButton(int button)
        {
            keybd_event((byte)button, 0, InputConstants.KEYEVENTF_KEYDOWN, 0);
        }

        /// <summary>
        /// Release a button which was held down
        /// </summary>
        /// <param name="button">the button id</param>
        public static void releaseButton(int button)
        {
            keybd_event((byte)button, 0, InputConstants.KEYEVENTF_KEYUP, 0);
        }

        /// <summary>
        /// Opens the context menu for a automation element.
        /// </summary>
        /// <param name="ae">The automation element </param>
        /// <param name="button">The mouse button.</param>
        /// <param name="checkMouse">if the mouse position should be checked</param>
        private IUIAutomationElementArray openContextMenu(IUIAutomationElement ae,
            int button, int x, int y, bool checkMouse)
        {
            if (!checkMouse)
            {
                moveMouseToPos(x, y);
            }
            else
            {
                if (!isMouseInComponent(ae))
                {
                    moveMouseToPos(x, y);
                }
            }
            click(1, button);

            IUIAutomationElement contextMenu = MenuImplClass.getFirstMenu(ae);
            if (contextMenu == null)
            {
                throw new ServerException("Context menu does not exist",
                                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
            IUIAutomationCondition menuItemsCond =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_MenuItemControlTypeId);
            IUIAutomationElementArray menuItems = contextMenu.FindAll(TreeScope.TreeScope_Descendants, menuItemsCond);
            return menuItems;
        }

        /// <summary>
        /// Opens the context menu and performs the selection.
        /// </summary>
        /// <param name="ae">the component with the context menu.</param>
        /// <param name="iPath">The index path for selection.</param>
        /// <param name="button">the mouse button.</param>
        /// <param name="checkMouse">if the mouse pos should be checked</param>
        [Obsolete]
        public bool selectContextMenuItemByIndexpath(IUIAutomationElement ae, object[] iPath, int button
            , bool onlyCheck, string checkProperty, int x, int y, bool checkMouse)
        {
            int[] indexPath = iPath.Cast<int>().ToArray();
            IUIAutomationElementArray menuItems = openContextMenu(ae, button, x, y,
                checkMouse);

            bool checkResult = onlyCheck;
            int level = 0;
            try
            {
                MenuImplClass.menuItemIndexSelect(menuItems, ref level, indexPath, ref checkResult, checkProperty);
            }
            catch (IndexOutOfRangeException)
            {
                throw new ServerException("MenuItem does not exist.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
            catch (COMException ce)
            {
                if (ComUtils.isExceptionOfType(ce, ComUtils.UIA_E_ELEMENTNOTENABLED))
                {
                    throw new ServerException("MenuItem is disabled.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
                throw ce;
            }
            return checkResult;
        }

        public string getProperty(IUIAutomationElement ae, string propertyName)
        {
            return typeof(IUIAutomationElement).GetProperty("Current" + propertyName).
                GetValue(ae, null).ToString();
        }

        private static tagRECT getClientArea(IUIAutomationElement ae)
        {
            tagRECT clientArea = ae.CurrentBoundingRectangle;
            IUIAutomationElement horizontalScrollbar =
                getHorizontalScrollbar(ae);

            if (horizontalScrollbar != null)
            {
                tagRECT scrollbarBounds =
                    horizontalScrollbar.CurrentBoundingRectangle;
                bool isScrollbarAtBottom =
                    clientArea.bottom - scrollbarBounds.bottom < scrollbarBounds.top - clientArea.top;
                if (isScrollbarAtBottom)
                {
                    clientArea.bottom = scrollbarBounds.top;
                }
                else
                {
                    clientArea.top = scrollbarBounds.bottom;
                }
            }

            IUIAutomationElement verticalScrollbar = getVerticalScrollbar(ae);
            if (verticalScrollbar != null)
            {
                tagRECT scrollbarBounds =
                    verticalScrollbar.CurrentBoundingRectangle;
                bool isScrollbarAtRight =
                    clientArea.right - scrollbarBounds.right < scrollbarBounds.left - clientArea.left;
                if (isScrollbarAtRight)
                {
                    clientArea.right = scrollbarBounds.left;
                }
                else
                {
                    clientArea.left = scrollbarBounds.right;
                }
            }

            return clientArea;
        }

        public static IUIAutomationElement getVerticalScrollbar(
            IUIAutomationElement scrollable)
        {
            // ASSUMPTION: vertical scrollbar is on right side
            IUIAutomationElementArray scrollbars = getScrollbars(scrollable);
            tagRECT scrollableBounds = scrollable.CurrentBoundingRectangle;
            for (int i = 0; i < scrollbars.Length; i++)
            {
                IUIAutomationElement scrollbar =
                    scrollbars.GetElement(i);
                tagRECT bounds = scrollbar.CurrentBoundingRectangle;
                if (Math.Abs(bounds.right - scrollableBounds.right) < SCROLLBAR_LOCATION_TOLERANCE)
                {
                    return scrollbar;
                }
            }

            return null;
        }

        public static IUIAutomationElementArray getScrollbars(
            IUIAutomationElement scrollable)
        {
            return scrollable.FindAll(
                TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_ScrollBarControlTypeId));
        }

        public static IUIAutomationElement getHorizontalScrollbar(
          IUIAutomationElement scrollable)
        {
            // ASSUMPTION: horizontal scrollbar is on bottom
            IUIAutomationElementArray scrollbars = getScrollbars(scrollable);
            tagRECT scrollableBounds = scrollable.CurrentBoundingRectangle;
            for (int i = 0; i < scrollbars.Length; i++)
            {
                IUIAutomationElement scrollbar =
                    scrollbars.GetElement(i);
                tagRECT bounds = scrollbar.CurrentBoundingRectangle;
                if (Math.Abs(bounds.bottom - scrollableBounds.bottom) < SCROLLBAR_LOCATION_TOLERANCE)
                {
                    return scrollbar;
                }
            }

            return null;
        }

        /// <summary>
        /// Scrolls the given component's parent to maximize the given 
        /// component's visibility. The parent will be scrolled such that the
        /// component's origin is as close to the parent's client area's origin
        /// as possible. Due to the granularity of scrolling (percentage-based,
        /// rather than pixel-based), it is not guaranteed that the two origins
        /// will exactly match when this method returns. This method does not
        /// take non-scrolling visibility factors (non-visible components,
        /// windows that are positioned such that they are only half-visible on
        /// the desktop, etc) into account.
        /// </summary>
        /// 
        /// <param name="ae">The component to scroll into view</param>
        /// <exception cref="ServerException">if the given element's parent
        /// does not support scrolling via the UI Automation Framework
        /// or if a control referenced by the given IUIAutomationElement or its
        /// parent is no longer available</exception>
        public static void scrollToVisible(IUIAutomationElement ae)
        {

            if (ComUtils.isOffscreen(ae))
            {
                IUIAutomationTreeWalker walker =
                    ComUtils.AUTOMATION.ControlViewWalker;
                IUIAutomationElement parent = walker.GetParentElement(ae);

                try
                {
                    IUIAutomationScrollPattern pattern =
                        parent.GetCurrentPattern(UiaPatternIds.UIA_ScrollPatternId)
                            as IUIAutomationScrollPattern;

                    Rect bounds = ComUtils.convertRect(
                        ae.CurrentBoundingRectangle);
                    Rect parentClientArea = ComUtils.convertRect(getClientArea(parent));

                    double scrollPercentX = ComUtils.NO_SCROLL;
                    double scrollPercentY = ComUtils.NO_SCROLL;

                    if (Convert.ToBoolean(pattern.CurrentHorizontallyScrollable))
                    {
                        scrollPercentX = calculateTargetScrollPercentage(
                            bounds.X, bounds.Width, parentClientArea.X,
                            parentClientArea.Width, pattern.CurrentHorizontalViewSize,
                            pattern.CurrentHorizontalScrollPercent);
                    }

                    if (Convert.ToBoolean(pattern.CurrentVerticallyScrollable))
                    {
                        scrollPercentY = calculateTargetScrollPercentage(
                            bounds.Y, bounds.Height, parentClientArea.Y,
                            parentClientArea.Height, pattern.CurrentVerticalViewSize,
                            pattern.CurrentVerticalScrollPercent);
                    }

                    pattern.SetScrollPercent(scrollPercentX, scrollPercentY);
                }
                catch (InvalidOperationException)
                {
                    throw new ServerException("Scrolling not supported by parent component.");
                }
                catch (COMException ce)
                {
                    throw new ServerException(ce.Message);
                }
            }
        }

        /// <summary>
        /// Calculates the target scroll percentage for a control's parent in
        /// order to scroll that control into view on a single axis.
        /// </summary>
        /// 
        /// <param name="position">The absolute position of the component on the 
        /// screen on a single axis.</param>
        /// <param name="length">The size of the component on a single axis.
        /// </param>
        /// <param name="parentPosition">The absolute position of the component's 
        /// parent on the screen on a single axis.</param>
        /// <param name="parentLength">The size of the component's parent's 
        /// client area ("viewport" area where content is displayed) on a 
        /// single axis.</param>
        /// <param name="parentViewSize">The percentage of the component's 
        /// parent's total size that is visible in its client area. This value
        /// must be in the "percent" format (ex. 97.6% must be given as 97.6, 
        /// *not* .976).</param>
        /// <param name="currentScrollPercentage">The current value for the percentage of the component's 
        /// parent's total size that is visible in its client area. This value
        /// must be in the "percent" format (ex. 97.6% must be given as 97.6, 
        /// *not* .976).</param>
        /// 
        /// <returns>the percentage to which the component's parent should be
        /// scrolled in order to best reveal the component. This value is in the 
        /// "percent" format (ex. 97.6% is represented as 97.6, *not* .976).
        /// </returns>
        private static double calculateTargetScrollPercentage(
            double position, double length, double parentPosition,
            double parentLength, double parentViewSize,
            double currentScrollPercentage)
        {
            // calculate the total parent length (not just the client
            // area represented by its reported bounds)
            double totalParentLength =
                (parentLength) / (parentViewSize / 100);

            double currentClientAreaOrigin =
                (totalParentLength - parentLength)
                    * (currentScrollPercentage / 100);

            double maxScrollPosition = totalParentLength - parentLength;
            double relativePosition = position - parentPosition;
            double targetClientAreaOrigin = currentClientAreaOrigin + relativePosition;

            double targetScrollPercentage =
                targetClientAreaOrigin / maxScrollPosition;

            // scale the percentage for usage in Automation Framework's
            // Scrolling Pattern (ex. 96.2 instead of .962).
            // cover the corner cases to prevent attempts to scroll to
            // unreachable locations.
            return Math.Max(Math.Min((targetScrollPercentage * 100), 100), 0);
        }

        /// <summary>
        /// Scrolls the control item corresponding to the given AutomationElement
        /// into view with respect to its container's viewport.
        /// </summary>
        /// 
        /// <param name="ae">The IUIAutomationElement referencing the item to scroll
        /// into view.</param>
        /// 
        /// <exception cref="ServerException">if the given element
        /// cannot be scrolled into view via the UI Automation Framework
        /// (this will happen if, for example, the IUIAutomationElement does not 
        /// represent an item) or if the item referenced by the given 
        /// IUIAutomationElement is no longer available</exception>
        public static void scrollItemIntoView(IUIAutomationElement ae)
        {
            try
            {
                IUIAutomationScrollItemPattern pattern =
                    ae.GetCurrentPattern(UiaPatternIds.UIA_ScrollItemPatternId)
                        as IUIAutomationScrollItemPattern;
                if (pattern == null)
                {
                    throw new ServerException("Component does not support scrolling.");
                }
                pattern.ScrollIntoView();
            }
            catch (COMException ce)
            {
                throw new ServerException(ce.Message);
            }
        }




        /// <summary>
        /// Get the automation element under the cursor position.
        /// </summary>
        /// <returns>the automation element.</returns>
        public static IUIAutomationElement getElementAtCursorPosition()
        {
            tagPOINT cursorLocation = new tagPOINT();
            cursorLocation.x = Cursor.Position.X;
            cursorLocation.y = Cursor.Position.Y;
            IUIAutomationElement elementAtCursorLocation =
            ComUtils.AUTOMATION.ElementFromPoint(cursorLocation);

            //Check if the component belongs to the aut, if not throw a exception
            if (elementAtCursorLocation.CurrentProcessId.Equals(processID))
            {
                return elementAtCursorLocation;
            }
            else
            {
                throw new ServerException("Component does not belong to the AUT.");
            }
        }

        public static void scrollIntoView(
            IScroller scrollDelegate, IUIAutomationElement toReveal,
            int viewPortLowerBound, int viewPortUpperBound, bool isVertical)
        {

            IUIAutomationTreeWalker visibleWalker =
                ComUtils.AUTOMATION.CreateTreeWalker(
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_IsOffscreenPropertyId,
                        false));

            bool scrollPerformed = true;
            double scrollFloor = 0;
            double scrollCeiling = 100;
            double newScrollValue = scrollDelegate.getScrollPercentage();
            while (ComUtils.isOffscreen(toReveal)
                    && scrollPerformed)
            {
                scrollPerformed = false;
                double scrollValue = scrollDelegate.getScrollPercentage();

                IUIAutomationElement nextVisible =
                    visibleWalker.GetNextSiblingElement(toReveal);

                if (nextVisible != null)
                {
                    scrollCeiling = newScrollValue;
                    newScrollValue = (scrollValue + scrollFloor) / 2.0;
                    scrollDelegate.setScrollPercentage(newScrollValue);
                    scrollPerformed = scrollValue
                        != scrollDelegate.getScrollPercentage();
                }
                else
                {
                    IUIAutomationElement previousVisible =
                        visibleWalker.GetPreviousSiblingElement(toReveal);
                    if (previousVisible != null)
                    {
                        scrollFloor = newScrollValue;
                        newScrollValue =
                            (scrollValue + scrollCeiling) / 2.0;
                        scrollDelegate.setScrollPercentage(newScrollValue);
                        scrollPerformed = scrollValue
                            != scrollDelegate.getScrollPercentage();
                    }
                }
            }

            if (ComUtils.isOffscreen(toReveal))
            {
                // Coarse scrolling was not enough to get the desired element
                // on-screen. Assume that it's one of the last or first elements.
                IUIAutomationElement nextVisible =
                    visibleWalker.GetNextSiblingElement(toReveal);

                if (nextVisible != null)
                {
                    scrollDelegate.setScrollPercentage(0);
                }
                else
                {
                    IUIAutomationElement previousVisible =
                        visibleWalker.GetPreviousSiblingElement(toReveal);
                    if (previousVisible != null)
                    {
                        scrollDelegate.setScrollPercentage(100);
                    }
                }
            }

            if (!ComUtils.isOffscreen(toReveal))
            {
                // We've scrolled the element into the viewport, but it may still
                // be only partially visible. We now need to perform fine-grained
                // scrolling to expose as much of the element's bounds as 
                // possible.
                tagRECT elementBounds = toReveal.CurrentBoundingRectangle;
                int elementLowerBound =
                    isVertical ? elementBounds.top : elementBounds.left;
                int elementUpperBound =
                    isVertical ? elementBounds.bottom : elementBounds.right;
                if (elementLowerBound < viewPortLowerBound)
                {
                    // Discovering the minimal scroll increment looks to be
                    // impossible because a scrollbar's Value Pattern's 
                    // CurrentValue always returns an integer (which seems to be 
                    // a truncated version of the actual internal value). We 
                    // just use 0.5, which should at least prevent accidentally
                    // scrolling backward due to truncation.
                    newScrollValue =
                        scrollDelegate.getScrollPercentage() - 0.5;
                    bool isNewValueValid = 0 <= newScrollValue
                        && newScrollValue <= 100;
                    while (elementLowerBound < viewPortLowerBound
                        && isNewValueValid)
                    {
                        scrollDelegate.setScrollPercentage(newScrollValue);
                        elementBounds = toReveal.CurrentBoundingRectangle;
                        elementLowerBound =
                            isVertical ? elementBounds.top : elementBounds.left;
                        newScrollValue -= 0.5;
                        isNewValueValid = 0 <= newScrollValue
                            && newScrollValue <= 100;
                    }
                }
                else if (elementUpperBound > viewPortUpperBound)
                {
                    // Discovering the minimal scroll increment looks to be
                    // impossible because a scrollbar's Value Pattern's 
                    // CurrentValue always returns an integer (which seems to be 
                    // a truncated version of the actual internal value). We 
                    // just use 0.5, which should at least prevent accidentally
                    // scrolling backward due to truncation.
                    newScrollValue =
                        scrollDelegate.getScrollPercentage() + 0.5;
                    bool isNewValueValid = 0 <= newScrollValue
                        && newScrollValue <= 100;
                    while (elementUpperBound > viewPortUpperBound
                        && isNewValueValid)
                    {
                        scrollDelegate.setScrollPercentage(newScrollValue);
                        elementBounds = toReveal.CurrentBoundingRectangle;
                        elementUpperBound =
                            isVertical ? elementBounds.bottom : elementBounds.right;
                        newScrollValue += 0.5;
                        isNewValueValid = 0 <= newScrollValue
                            && newScrollValue <= 100;
                    }
                }
            }

            // This seems to be necessary because the scrolling is otherwise
            // occasionally reported as finished although it actually is not.
            Thread.Sleep(500);
        }

        /// <summary>
        /// Check if the mouse curser is over the given component.
        /// </summary>
        /// <param name="ae">the component</param>
        /// <returns>if the mouse is over the element</returns>
        public bool isMouseInComponent(IUIAutomationElement ae)
        {
            tagPOINT point = new tagPOINT();
            point.x = Cursor.Position.X;
            point.y = Cursor.Position.Y;
            IUIAutomationElement element =
                ComUtils.AUTOMATION.ElementFromPoint(point);
            if (ComUtils.isEquals(ae, element))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the current time in milliseconds.
        /// </summary>
        /// <returns>The current time in milliseconds.</returns>
        public static long getCurrentTimeInMilliseconds() {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        /// <summary>
        /// Press ESC button to close context menus or menus of menu bar.
        /// </summary>
        /// <param name="ae">The component the context menu has previously opened at with right click or null for closing menu bar menus.</param>
        /// <param name="count">The number how often the ESC button should be pressed.</param>
        public void closeMenu(IUIAutomationElement ae, int count)
        {
            for (int i = 0; i < count; i++)
            {
                SendKeys.SendWait(InputConstants.VK_ESC);
            }
            IUIAutomationElement menu = MenuImplClass.getFirstMenu(ae);
            if (menu != null)
            {
                long startMS = getCurrentTimeInMilliseconds();
                while (menu != null && getCurrentTimeInMilliseconds() - startMS < 1000)
                {
                    menu = MenuImplClass.getFirstMenu(ae);
                }
                if (menu != null)
                {
                    throw new ServerException("Closing menu failed",
                                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
            }
        }

        /// <summary>
        /// Selects a pattern from the begin index to the end index
        /// </summary>
        /// <param name="begin">where the pattern begins</param>
        /// <param name="end">where the pattern ends</param>
        public void selectPattern(int begin, int end)
        {
            SendKeys.SendWait(InputConstants.VK_STRG_POS1); // press strg + home / pos1

            for (int i = 0; i < begin; i++)
            {
                SendKeys.SendWait(InputConstants.VK_RIGHT); // press right arrow button
            }

            //press shift for text selection
            pressButton(InputConstants.KEYFLAG_SHIFT);

            for (int i = 0; i < end - begin; i++)
            {
                SendKeys.SendWait(InputConstants.VK_RIGHT); // press right arrow button
            }

            releaseButton(InputConstants.KEYFLAG_SHIFT);
        }

        /// <summary>
        /// Sets he position of the caret to a specific index
        /// </summary>
        /// <param name="index">where the caret should be placed</param>
        public void setCaretPosition(int index)
        {
            SendKeys.SendWait(InputConstants.VK_STRG_POS1); // press strg + home / pos1

            for (int i = 0; i < index; i++)
            {
                SendKeys.SendWait(InputConstants.VK_RIGHT); // press right arrow button
            }
        }

        /// <summary>
        /// Toggle a key
        /// </summary>
        /// <param name="key">the key to toggle</param>
        /// <param name="activate">if the key should be activ or inactiv</param>
        public void toggle(int key, bool activate)
        {
            bool activated = false;

            if (InputConstants.TOGGLE_NUMLOCK == key)
            {
                activated = Control.IsKeyLocked(Keys.NumLock);
                toggleKey(InputConstants.VK_NUMLOCK, activate, activated);
            }
            else if (InputConstants.TOGGLE_CAPLOCK == key)
            {
                activated = Control.IsKeyLocked(Keys.CapsLock);
                toggleKey(InputConstants.VK_CAPITAL, activate, activated);
            }
            else if (InputConstants.TOGGLE_SCROLLLOCK == key)
            {
                activated = Control.IsKeyLocked(Keys.Scroll);
                toggleKey(InputConstants.VK_SCROLL, activate, activated);
            }
            else
            {
                throw new ServerException("Key not supported",
                    ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
        }

        /// <summary>
        /// Checks if the key should be toggled and performs the toggle
        /// </summary>
        /// <param name="key">the key to toggle</param>
        /// <param name="activate">if the key should be activ or inactiv</param>
        /// <param name="activated">the current activation state </param>
        private void toggleKey(int key, bool activate, bool activated)
        {
            if (activate != activated)
            {
                pressButton(key);
                releaseButton(key);
            }
        }

        /// <summary>
        /// Check if the given element is a part of the wpf framework
        /// </summary>
        /// <param name="element">the element to check</param>
        /// <returns>is the element is a part of the wpf framework</returns>
        private bool isWPFElement(IUIAutomationElement element)
        {
            if (element != null && element.CurrentFrameworkId.Equals(WPF_FRAMEWORK))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Clicks to the given direction in the foreground window
        /// </summary>
        /// <param name="method">the direction</param>
        public void appActivationClick(string method)
        {
            IUIAutomationElement window = getForegroundAutomationWindow();

            IUIAutomationWindowPattern windowPattern = window.GetCurrentPattern(
                UiaPatternIds.UIA_WindowPatternId) as IUIAutomationWindowPattern;

            int[] bounds = AbstractElementImplClass.getBounds(window);
            int x = bounds[0];
            int y = bounds[1];
            int width = bounds[2];
            int height = bounds[3];
            int posx = 0;
            int posy = 0;
            bool doClick = false;

            //change the width and height so what we don't hit the taskbar in max. window state
            if (windowPattern.CurrentWindowVisualState.Equals(WindowVisualState.WindowVisualState_Maximized))
            {
                width = width - 10;
                height = height - 10;
            }

            switch (method)
            {
                case "TITLEBAR":
                    posx = (width / 2) + x;
                    posy = 3 + y;
                    doClick = true;
                    break;
                case "NW":
                    posx = x;
                    posy = y;
                    doClick = true;
                    break;
                case "NE":
                    posx = (width - 1) + x;
                    posy = y;
                    doClick = true;
                    break;
                case "SW":
                    posx = x;
                    posy = (height - 1) + y;
                    doClick = true;
                    break;
                case "SE":
                    posx = (width - 1) + x;
                    posy = (height - 1) + y;
                    doClick = true;
                    break;
                case "CENTER":
                    posx = (width / 2) + x;
                    posy = (height / 2) + y;
                    doClick = true;
                    break;
            }

            if (doClick)
            {
                moveMouseToPos(posx, posy);
                click(1, 1);
            }
        }

        /// <summary>
        /// Uses the ForegroundWindow handle to create a IUIAutomationElement that represents the ForegroundWindow
        /// of our AUT.
        /// </summary>
        /// <returns>ForegroundWindow as a AutomationElement</returns>
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        public static IUIAutomationElement getForegroundAutomationWindow()
        {
            IUIAutomationCondition idCondition =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ProcessIdPropertyId, WinRobotImpl.processID);

            IUIAutomationElement window =
                ComUtils.AUTOMATION.ElementFromHandle(GetForegroundWindow());

            //return only the window if it is a part of the AUT
            if (AutomationConditionMatcher.matches(window, idCondition))
            {
                return window;
            }
            else //performance problems could lead to the failure, better check it a second time
            {
                WinRobotImpl robot = new WinRobotImpl();
                robot.setAUTFocus();
                Thread.Sleep(200);
                window = ComUtils.AUTOMATION.ElementFromHandle(GetForegroundWindow());
                if (AutomationConditionMatcher.matches(window, idCondition))
                {
                    return window;
                }
                else
                {
                    throw new ServerException("No active AUT Window",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
            }
        }

        public static int getProcessId()
        {
            return processID;
        }
    }
}
