﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Robot Interface.
    /// </summary>
    interface IWinRobot
    {
        /// <summary>
        /// Starts the AUT.
        /// </summary>
        /// <param name="autpath">
        /// The path to the AUT.
        /// </param>
        /// /// <param name="autWorkDir">
        /// The aut working directory
        /// </param>
        /// <param name="args">
        /// Arguments for the AUT-Start.
        /// </param>
        void startAUT(string autPath, string autWorkDir, string args);

        /// <summary>
        /// Stops the AUT.
        /// </summary>
        void stopAUT();

        /// <summary>
        /// Checks  if the AUT is started / active.
        /// </summary>
        bool isAUTactive();

        /// <summary>
        /// Stops this Win server
        /// </summary>
        void stopServer();

    }
}
