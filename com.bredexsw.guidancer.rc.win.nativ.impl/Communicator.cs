﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Web.Script.Serialization;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Communication class which is waiting for messages from the AUTAgent to excecute these
    /// and to send a respone.
    /// </summary>
    class Communicator
    {        
        /// <summary>
        /// the tcp port for the communication
        /// </summary>
        private int m_port;        
        /// <summary>
        /// the thread for reading / writing messages to the autagent
        /// </summary>
        private Thread m_mainThread;
        /// <summary>
        /// the json serializer
        /// </summary>
        private JavaScriptSerializer m_jss = new JavaScriptSerializer();       
        /// <summary>
        /// the stream reder
        /// </summary>
        private StreamReader m_sr;
        /// <summary>
        /// the stream writer
        /// </summary>
        private StreamWriter m_sw;
        /// <summary>
        /// the name of the namespace
        /// </summary>
        private const string namespaceName = "com.bredexsw.guidancer.rc.win.nativ.impl.";
        /// <summary>
        /// Method name of the set element method from the abstractElementImpl class
        /// </summary>
        private const string setElementMethod = "setElement";

        /// <summary>
        /// Constructor to create the waiting connection to the AUTAgent.
        /// </summary>
        /// <param name="port">
        /// The TCP-Port.
        /// </param>
        public Communicator(int port)
        {
            m_port = port;
            m_mainThread = new Thread(new ThreadStart(this.initConnection));
            m_mainThread.Start();
        }

        /// <summary>
        /// Creates the Connection, waits for the AUTAgent message and sends respones.
        /// </summary>
        public void initConnection()
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Loopback, m_port);
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            try
            {
                server.Connect(ip);
                NetworkStream ns = new NetworkStream(server);
                m_sr = new StreamReader(ns,System.Text.Encoding.UTF8);
                m_sw = new StreamWriter(ns);                
                Type type = null;

                string data;

                while (true)
                {
                    //get msg
                    data = m_sr.ReadLine();
                    //create obj from json
                    NativeMessage msgObj = m_jss.Deserialize<NativeMessage>(data);
                    
                    //reflection - check if the method contain a return value, if not resend a "void"
                    NativeAnswer returnMsgObj = new NativeAnswer();
                    try
                    {
                        MethodInfo methodinfo; 
                        object returnvalue = null;
                        string className = namespaceName + msgObj.className;                        
                        var classObject = System.Activator.CreateInstance(Type.GetType(className));
                        type = classObject.GetType();

                        //set automation element for impl class
                        if (msgObj.isLocatorSet())
                        {
                            // FIXME RB: Currently this part is not used, but should be instead of using preAction method like in WinRobotImpl class.
                            //           My suggestion is to convert the locator here and to invoke the target method directly.
                            //           In this way, the setElementMethod attribute can be removed also.
                            object[] locatorParam = { msgObj.locator };
                            type.GetMethod(setElementMethod, new[] { typeof(string) }).Invoke(classObject, locatorParam);
                        }

                        //perform action
                        Type[] types = new Type[msgObj.parameters.Length];
                        for (int i = 0; i < msgObj.parameters.Length; i++)
                        {
                            if (msgObj.parameters[i] != null)
                            {
                                types[i] = msgObj.parameters[i].GetType();
                            }
                            else
                            {
                                types[i] = typeof(string); // we assume that null means always a string type
                            }
                        }

                        methodinfo = type.GetMethod(msgObj.methodName, types);
                        returnvalue = methodinfo.Invoke(classObject, msgObj.parameters); 

                        //returnvalue is always null if the reflected method has no return values
                        if (returnvalue == null)
                        {
                            returnvalue = "null";
                        }

                        returnMsgObj = new NativeAnswer(returnvalue);
                    }
                    /**
                     * Catch exception which occurred during the robot method or the reflection and pass
                     * the exception to the Win Java server 
                     **/
                    //Method private / not found - no inner
                    catch (NullReferenceException)
                    {
                        ServerException se = new ServerException("Method not found: " + getSignatureFromMessage(msgObj),
                            ServerExceptionConstants.REFLECTION_ERROR);                        
                        returnMsgObj = gatherExceptionInfos(se, type, msgObj);
                    }
                    //Method signature do not match - no inner
                    catch (ArgumentException)
                    {
                        ServerException se = new ServerException("Method signature do not match.",
                            ServerExceptionConstants.REFLECTION_SIGNATURE_NOT_EQUAL);
                        returnMsgObj = gatherExceptionInfos(se, type, msgObj);
                    }
                    //Parameter count is unequal - no inner
                    catch (TargetParameterCountException)
                    {
                        ServerException se = new ServerException("Parameter count mismatch.",
                            ServerExceptionConstants.REFLECTION_PARAMETER_NOT_EQUAL);
                        returnMsgObj = gatherExceptionInfos(se, type, msgObj);
                    }
                    //exception thrown through the invoked method - inner
                    catch (TargetInvocationException e)
                    {
                        Exception exception = e;

                        while (exception.InnerException != null)
                        {
                            exception = exception.InnerException;
                        }

                        if (exception != null && typeof(ServerException) == exception.GetType())
                        {
                            ServerException innerException = (ServerException)exception;
                            returnMsgObj = gatherExceptionInfos(innerException, type, msgObj);
                        }                                                                 
                    }
                    finally
                    {
                        sendAnswer(returnMsgObj);
                    }
                }

            }
            catch (SocketException)
            {
                Environment.Exit(0);
            }
            catch (IOException)
            {
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Extract the signature of the method as a string, which the given message represents.
        /// </summary>
        /// <param name="msgObj">The native message representing a method signature.</param>
        /// <returns>The signature as a string from the given message.</returns>
        private String getSignatureFromMessage(NativeMessage msgObj)
        {
            string msg = msgObj.className + "." + msgObj.methodName + "(";
            for (int i = 0; i < msgObj.parameters.Length; i++)
            {
                if (i > 0)
                {
                    msg += ",";
                }
                if (msgObj.parameters[i] != null)
                {
                    msg += msgObj.parameters[i].GetType().Name;
                }
                else
                {
                    msg += "null";
                }
            }
            msg += ')';
            return msg;
        }

        /// <summary>
        /// Constructor to create the waiting connection to the AUTAgent.
        /// </summary>
        /// <param name="rmo">
        /// The Message that will be returned to the AUTAgent.
        /// </param>
        private void sendAnswer(NativeAnswer rmo)
        {
            string jsonString = m_jss.Serialize(rmo);
            m_sw.WriteLine(jsonString);
            m_sw.Flush();
        }

        /// <summary>
        /// Gathers all information from a reflection exception and convert these to
        /// a message that can be returned to the AUTAgent.
        /// </summary>
        /// <param name="e">
        /// The ServerException
        /// </param>
        /// <param name="type">
        /// The invoked class
        /// </param>
        /// <param name="msgObj">
        /// The message
        /// </param>        
        private NativeAnswer gatherExceptionInfos(ServerException e, Type type, NativeMessage msgObj)
        {
            Type exceptionType = e.GetType();
            int id = e.exceptionID;
            string name = ServerExceptionConstants.getStringToID(id);
            string msg = e.Message;
            string invokeMethodh = msgObj.methodName;
            string invokeClass = type.Name;
            
            return new NativeAnswer(id,name, msg, invokeMethodh, invokeClass);
        }
    }
}