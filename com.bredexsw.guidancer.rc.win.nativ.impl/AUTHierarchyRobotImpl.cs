﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Robot class for all AUTHierarchy actions
    /// </summary>
    class AUTHierarchyRobotImpl
    {
        /// <summary>
        /// Element that represents the "root" of the autHierarchy.
        /// </summary>
        private static NetHierarchyContainer autHierarchy;
        /// <summary>
        /// The real map, used if a container is searched for a automation element.
        /// </summary>
        private static Dictionary<BXAutomationElement, NetHierarchyContainer> realMap =
            new Dictionary<BXAutomationElement, NetHierarchyContainer>();
        /// <summary>
        /// The hierarchy map, used to find a container with the automation element for a given locator string.
        /// </summary>
        private static Dictionary<String, NetHierarchyContainer> hierarchyMap = new Dictionary<String, NetHierarchyContainer>();


        /// <summary>
        /// Create the AUTHierarchy in the form of a tree with all
        /// elements that are supported.
        /// </summary>
        /// <returns>returns the AUTHierarchy to the Java Win server</returns>
        public List<AUTComponentInformations> getHierarchy()
        {            
            refreshHierarchy();
            List<AUTComponentInformations> m_componentList = new List<AUTComponentInformations>();
            
            foreach (var pair in realMap)
            {
                NetHierarchyContainer nhc = pair.Value;
                m_componentList.Add(new AUTComponentInformations(nhc.getName(), nhc.getLocatorPath(),
                    ControlTypeUtils.getShortNameForId(nhc.getComponent().CurrentControlType),nhc.getComponent().CurrentFrameworkId));
            }

            return m_componentList;
        }

        private static void getHierarchyElements(NetHierarchyContainer hc)
        {            
            IUIAutomationElement ae = hc.getComponent();
            BXAutomationElement bxElement = new BXAutomationElement(ae);

            realMap[bxElement] = hc;
            hierarchyMap[hc.getLocatorPath()] = hc;

            // Short-circuit for ignoring all children of Tables (i.e. 
            // TableItems) as well as other complex components. This is currently 
            // necessary in order to maintain acceptable performance levels for 
            // complex controls with *many* entries.
            // FIXME make this configurable (i.e. not hard-coded)
            // FIXME verify that this does not prevent mapping of controls
            //       that are embedded in table cells (cell editors, 
            //       combo boxes, etc.) and other "contained" controls
            int controlTypeId = ae.CurrentControlType;
            if (controlTypeId == UiaControlTypeIds.UIA_TableControlTypeId                
                || controlTypeId == UiaControlTypeIds.UIA_DataGridControlTypeId                
                || controlTypeId == UiaControlTypeIds.UIA_ListControlTypeId
                || controlTypeId == UiaControlTypeIds.UIA_MenuControlTypeId
                || controlTypeId == UiaControlTypeIds.UIA_MenuBarControlTypeId
                || controlTypeId == UiaControlTypeIds.UIA_TreeControlTypeId)
            {
                return;
            }

            IUIAutomationElementArray children =
                ae.FindAll(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.ControlViewCondition);
            for (int i = 0; i < children.Length; i++)
            {
                IUIAutomationElement child = children.GetElement(i);
                NetHierarchyContainer nhc = new NetHierarchyContainer(child, hc, i);
                hc.add(nhc);

                getHierarchyElements(nhc);
            }
        }
        /// <summary>
        /// Builds the autHierarchy by searching all elements in the windows automation tree root node
        /// that belongs to our AUT.
        /// </summary>
        /// <param name="hc">the parent container where all childrens are searched</param>
        private static void getRootHierarchyElements(NetHierarchyContainer desktop)
        {
            IUIAutomationElement ae = desktop.getComponent();
            BXAutomationElement bxElement = new BXAutomationElement(ae);

            realMap[bxElement] = desktop;
            hierarchyMap[desktop.getLocatorPath()] = desktop;

            IUIAutomationElementArray children =
                ae.FindAll(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.CreateAndCondition(
                    ComUtils.AUTOMATION.ContentViewCondition, 
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ProcessIdPropertyId, WinRobotImpl.getProcessId())));
            for (int i = 0; i < children.Length; i++)
            {
                IUIAutomationElement child = children.GetElement(i);
                NetHierarchyContainer nhc = new NetHierarchyContainer(child, desktop, i);
                desktop.add(nhc);

                getHierarchyElements(nhc);
            }
            
        }

        /// <summary>
        /// Clears all the Maps and Names.
        /// </summary>
        private static void clear()
        {
            realMap.Clear();
            hierarchyMap.Clear();
        }

        /// <summary>
        /// Rebuilds the AUTHierarchy
        /// </summary>
        private static void refreshHierarchy()
        {
            clear();
            IUIAutomationElement rootDesktop = 
                ComUtils.AUTOMATION.GetRootElement();
            autHierarchy = new NetHierarchyContainer(rootDesktop);
            getRootHierarchyElements(autHierarchy);
        }

        /// <summary>
        /// Finds the automation element to the given AUTHieraray path
        /// </summary>
        /// <param name="path">the path to the aut component</param>
        /// <returns>the automation element</returns>
        public static IUIAutomationElement findComponent(string path)
        {
            if (hierarchyMap.ContainsKey(path))
            {
                return hierarchyMap[path].getComponent();
            }
            else
            {                
                refreshHierarchy();
                if (hierarchyMap.ContainsKey(path))
                {
                    return hierarchyMap[path].getComponent();
                }
                else
                {
                    throw new ServerException("Component not Found",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
            }     
        }

        /// <summary>
        /// Finds the locator to a given automation element in our autHierarchy
        /// </summary>
        /// <param name="ae">the automation element</param>
        /// <returns>the locator path</returns>
        public static string findLocator(IUIAutomationElement ae)
        {
            BXAutomationElement bxElement = new BXAutomationElement(ae);

            if (realMap.ContainsKey(bxElement))
            {
                return realMap[bxElement].getLocatorPath();
            }
            else
            {
                refreshHierarchy();
                if (realMap.ContainsKey(bxElement))
                {
                    return realMap[bxElement].getLocatorPath();
                }
                return null;
            }            
        }        
    }
}
