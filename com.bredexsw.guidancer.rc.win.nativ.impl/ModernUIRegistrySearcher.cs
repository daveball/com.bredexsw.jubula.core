﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Class to launch modernUI apps and search for all startable apps.
    /// </summary>
    class ModernUIRegistySearcher
    {  
        [DllImport("shlwapi.dll", BestFitMapping = false, CharSet = CharSet.Unicode, ExactSpelling = true, SetLastError = false, ThrowOnUnmappableChar = true)]
        private static extern int SHLoadIndirectString(string pszSource, StringBuilder pszOutBuf, int cchOutBuf, IntPtr ppvReserved);
    
        /// <summary>
        /// Get all startable modern ui apps.
        /// </summary>
        /// <returns>a string array that contains all app names</returns>
        public static string[] GetAllAppNames()
        {
            List<RegistryNodeInfo> allInstalledApps = GetAppDisplayNames();
            List<string> launchableApps = new List<string>();
            foreach (RegistryNodeInfo regNode in allInstalledApps)
            { // true, if the app has a name and is startable over the registry
                if (regNode.LaunchID != null && regNode.DisplayName != null)
                {
                    launchableApps.Add(regNode.DisplayName);
                }
            }
            return launchableApps.ToArray();
        }

        /// <summary>
        /// Traverse app names from the Win registry and store some meta data to find the
        /// app launchID to start the AUT.
        /// </summary>
        /// <param name="appName">the app name</param>
        /// <param name="searchLocation">true if localMachine tree, false if user tree</param>
        /// <returns>true if the app was found</returns>
        public static RegistryNodeInfo GetLaunchIDByName(string appName, bool searchLocation)
        {
            RegistryNodeInfo appRegInfo = new RegistryNodeInfo();
            try
            {
                RegistryKey pkgRootNode = searchLocation ? Registry.LocalMachine.OpenSubKey(
                    "Software\\Classes\\Local Settings\\Software\\Microsoft\\"
                    + "Windows\\CurrentVersion\\AppModel\\Repository\\Packages") :
                    Registry.CurrentUser.OpenSubKey(
                    "Software\\Classes\\Local Settings\\Software\\Microsoft\\"
                    + "Windows\\CurrentVersion\\AppModel\\Repository\\Packages");
                // subKeyName == packageID
                string[] subKeyNames = pkgRootNode.GetSubKeyNames();
                string displayName = "";
                // for each application pkg (may contain multiple apps)
                for (int i = 0; i < subKeyNames.Length; i++)
                {
                    RegistryKey appRoot = pkgRootNode.OpenSubKey(subKeyNames[i]);
                    string[] subNodes = appRoot.GetSubKeyNames();
                    // for each application pkg node, look if the "Applications" node exists
                    for (int j = 0; j < subNodes.Length; j++)
                    {
                        RegistryKey childKey = appRoot.OpenSubKey(subNodes[j]);
                        if (subNodes[j].Equals("Applications"))
                        {
                            if (childKey != null)
                            {
                                string[] subSubNodes = childKey.GetSubKeyNames();
                                // for each launch ID in the node
                                for (int k = 0; k < subSubNodes.Length; k++)
                                {
                                    // if contains "!", then we found a launchID
                                    if (subSubNodes[k].Contains("!"))
                                    {
                                        displayName = GetDisplayName(childKey.OpenSubKey(subSubNodes[k]));
                                        // if the appname matches the input name
                                        if (displayName != null && displayName.ToLower().Equals(appName.ToLower()))
                                        {
                                            appRegInfo.LaunchID = subSubNodes[k];
                                            appRegInfo.RegistryKey = childKey.OpenSubKey(subSubNodes[k]);
                                            appRegInfo.DisplayName = displayName;
                                            // all required infos found
                                            return appRegInfo;
                                        }
                                        else
                                        {
                                            // not the right name, try the next node
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // if the application root node has no child, the name information
                    // is directly in the node.
                    appRegInfo.DisplayName = GetDisplayName(appRoot);
                    appRegInfo.RegNodeID = subKeyNames[i];
                    if (appRegInfo.DisplayName.ToLower().Equals(appName.ToLower()))
                    {
                        appRegInfo.LaunchID = GetLaunchID(appRegInfo);
                        return appRegInfo;
                    }
                }
                // app not found
                return null;
            }
            catch (NullReferenceException)
            {
                throw new ServerException("AUT not found", ServerExceptionConstants.EXIT_AUT_NOT_FOUND);
            }
        }

        /// <summary>
        /// Get the display name of the given registry key.
        /// </summary>
        /// <param name="key">The registry key</param>
        /// <returns>the app name or null if not found</returns>
        private static string GetDisplayName(RegistryKey key)
        {
            string displayName = key.GetValue("DisplayName") as string;
            if (displayName == null)
            {
                displayName = key.GetValue("ApplicationName") as string;
            }
            if (displayName != null)
            {
                // name in clear text
                if (!displayName.StartsWith("@{"))
                {
                    return displayName;
                }
                // name as resource
                else
                {
                    StringBuilder outBuff = new StringBuilder(1024);
                    int result = SHLoadIndirectString(displayName, outBuff, -1, IntPtr.Zero);
                    if (result == 0) //SHLoadIndirectString returns 0 if successful
                    {
                        return outBuff.ToString();
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Get the registry launchID from a package node.
        /// </summary>
        /// <param name="regRoot">The key where to get the data from</param>
        /// <returns>The launchID to the key or null</returns>
        private static string GetLaunchID(RegistryNodeInfo regRoot)
        {
            // expand the app registry>>Server node
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\Classes\\ActivatableClasses\\Package\\"
                + regRoot.RegNodeID + "\\Server");
            if (regKey != null)
            {
                string[] appSubKeys = regKey.GetSubKeyNames();
                for (int i = 0; i < appSubKeys.Length; i++)
                {
                    RegistryKey innerAppKey = regKey.OpenSubKey(appSubKeys[i]);
                    string launchID = innerAppKey.GetValue("AppUserModelId") as string;
                    if (launchID != null)
                    {
                        return launchID;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Find app names from the Win registry and store some meta data to find the
        /// app launchID to validate (later), that the app is startable.
        /// </summary>
        /// <returns>a list of registry nodes</returns>
        private static List<RegistryNodeInfo> GetAppDisplayNames()
        {
            List<RegistryNodeInfo> allInstalledApps = new List<RegistryNodeInfo>();
            RegistryKey pkgRootNode = Registry.CurrentUser.OpenSubKey(
                "Software\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\Repository\\Packages");
            // subKeyName == packageID
            string[] subKeyNames = pkgRootNode.GetSubKeyNames();
            string displayName = "";
            RegistryNodeInfo regNode;
            bool continueSearch = true;
            // for each application pkg (may contain multiple apps)
            for (int i = 0; i < subKeyNames.Length; i++)
            {
                RegistryKey appRoot = pkgRootNode.OpenSubKey(subKeyNames[i]);
                string[] subNodes = appRoot.GetSubKeyNames();
                // for each application node, look if the "Applications" node is expandable and exists
                for (int j = 0; j < subNodes.Length; j++)
                {
                    regNode = new RegistryNodeInfo();
                    RegistryKey childKey = appRoot.OpenSubKey(subNodes[j]);
                    if (subNodes[j].Equals("Applications"))
                    {
                        if (childKey != null)
                        {
                            string[] subSubNodes = childKey.GetSubKeyNames();
                            // for each launch ID in the node
                            for (int k = 0; k < subSubNodes.Length; k++)
                            {
                                // if contains "!", then we found a launchID
                                if (subSubNodes[k].Contains("!"))
                                {
                                    displayName = GetDisplayName(childKey.OpenSubKey(subSubNodes[k]));
                                    if (displayName != null)
                                    {
                                        regNode.DisplayName = displayName;
                                        regNode.LaunchID = subSubNodes[k];
                                        regNode.RegistryKey = childKey.OpenSubKey(subSubNodes[k]);
                                        // all required infos found
                                        allInstalledApps.Add(RegistryNodeInfo.Clone(regNode));
                                        continueSearch = false;
                                    }
                                }
                            }
                        }
                    }
                }
                // if the application root node has no child, the name information
                // is directly in the node.
                if (continueSearch)
                {
                    regNode = new RegistryNodeInfo();
                    regNode.DisplayName = GetDisplayName(appRoot);
                    regNode.RegNodeID = subKeyNames[i];
                    allInstalledApps.Add(regNode);
                    regNode.LaunchID = GetLaunchID(regNode);
                }
                else
                {
                    continueSearch = true;
                }
            }
            return allInstalledApps;
        }
    }
}
