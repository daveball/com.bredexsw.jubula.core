﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// List of all exception codes
    /// </summary>
    class ServerExceptionConstants
    {
        //Exit worth id constants
        public const int EXIT_INVALID_ARGS = 1;
        public const string EXIT_INVALID_ARGS_NAME = "EXIT_INVALID_ARGS";

        public const int EXIT_AUT_NOT_FOUND = 3;
        public const string EXIT_AUT_NOT_FOUND_NAME = "EXIT_AUT_NOT_FOUND";

        public const int AUT_STOP_FAILED = 5;
        public const string AUT_STOP_FAILED_NAME = "AUT_STOP_FAILED";

        public const int AUT_START_ERROR = 25;
        public const string AUT_START_ERROR_NAME = "AUT_START_ERROR";

        public const int AUT_NOT_UIA_SUPPORTED = 26;
        public const string AUT_NOT_UIA_SUPPORTED_NAME = "AUT_NOT_UIA_SUPPORTED";

        //normal id constants

        public const int AUT_ACTIVE_CHECK_FAILED = 6;
        public const string AUT_ACTIVE_CHECK_FAILED_NAME = "AUT_ACTIVE_CHECK";

        public const int SERVER_STOP_FAILED = 7;
        public const string SERVER_STOP_FAILED_NAME = "SERVER_STOP_FAILED";

        public const int REFLECTION_PARAMETER_NOT_EQUAL = 8;
        public const string REFLECTION_PARAMETER_NOT_EQUAL_NAME = "REFLECTION_PARAMETER_NOT_EQUAL";

        public const int REFLECTION_SIGNATURE_NOT_EQUAL = 9;
        public const string REFLECTION_SIGNATURE_NOT_EQUAL_NAME = "REFLECTION_SIGNATURE_NOT_EQUAL";

        public const int REFLECTION_ERROR = 10;
        public const string REFLECTION_ERROR_NAME = "REFLECTION_ERROR";

        public const int STEP_EXECUTION_EXCEPTION = 11;
        public const string STEP_EXECUTION_EXCEPTION_NAME = "STEP_EXECUTION_EXCEPTION";

        // This constant is also defined on the Java side 
        // (c.b.g.rc.win.communication.Communicator).
        public const int TIMEOUT = 12;
        // This constant is also defined on the Java side 
        // (c.b.g.rc.win.communication.Communicator).
        public const string TIMEOUT_NAME = "TIMEOUT";

        public const string UNKNOWN_ERROR = "UNKNOWN_ERROR";

        public static string getStringToID(int id) {
            switch (id)
            {
                case AUT_STOP_FAILED:
                    return AUT_STOP_FAILED_NAME;
                case AUT_ACTIVE_CHECK_FAILED:
                    return AUT_ACTIVE_CHECK_FAILED_NAME;
                case SERVER_STOP_FAILED:
                    return SERVER_STOP_FAILED_NAME;
                case REFLECTION_PARAMETER_NOT_EQUAL:
                    return REFLECTION_PARAMETER_NOT_EQUAL_NAME;
                case REFLECTION_SIGNATURE_NOT_EQUAL:
                    return REFLECTION_SIGNATURE_NOT_EQUAL_NAME;
                case REFLECTION_ERROR:
                    return REFLECTION_ERROR_NAME;
                case STEP_EXECUTION_EXCEPTION:
                    return STEP_EXECUTION_EXCEPTION_NAME;
                case TIMEOUT:
                    return TIMEOUT_NAME;
                default:
                    return UNKNOWN_ERROR;
            }
        }
        
    }
}
