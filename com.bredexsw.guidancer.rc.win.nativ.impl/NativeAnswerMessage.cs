﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Message which will be send back to the AUTAgent, contains the return value
    /// of the invoked method or exception informations.
    /// </summary>
    public class NativeAnswerMessage
    {
        /// <summary>
        /// Return value of the invoked method
        /// </summary>
        public object answer { get; set; }
        /// <summary>
        /// Indicator if a exception occurred
        /// </summary>
        public bool exceptionFlag { get; set; }
        /// <summary>
        /// The ID from the exception which occurred on the Win server side
        /// </summary>
        public int exceptionID { get; set; }
        /// <summary>
        /// The name from the exception which occurred
        /// </summary>
        public string exceptionName { get; set; }
        /// <summary>
        /// The message from the exception which occurred
        /// </summary>
        public string exceptionMsg { get; set; }
        /// <summary>
        /// The class name where the exception occurred
        /// </summary>
        public string exceptionClass { get; set; }
        /// <summary>
        /// The method name where the exception occurred
        /// </summary>
        public string exceptionMethod { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public NativeAnswerMessage()
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serverAnswer">
        /// the return value from the invoked method
        /// </param>
        public NativeAnswerMessage(object serverAnswer)
        {
            this.exceptionFlag = false;
            this.answer = serverAnswer;
        }

        /// <summary>
        /// Constructor for a return message with exception informations
        /// </summary>
        /// <param name="e_class">
        /// the class where the exception occured
        /// </param>
        /// <param name="e_id">
        /// the exception id
        /// </param>
        /// <param name="e_method">
        /// the method where the exception occured
        /// </param>
        /// <param name="e_msg">
        /// the message of the exception
        /// </param>
        /// <param name="e_name">
        /// the name of the exception
        /// </param>
        public NativeAnswerMessage(int e_id, string e_name, string e_msg, string e_method, string e_class)
        {
            this.exceptionFlag = true;
            this.exceptionID = e_id;
            this.exceptionName = e_name;
            this.exceptionMsg = e_msg;
            this.exceptionMethod = e_method;
            this.exceptionClass = e_class;
        }        
    }

}
