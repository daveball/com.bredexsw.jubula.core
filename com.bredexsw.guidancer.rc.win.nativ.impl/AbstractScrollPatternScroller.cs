﻿using Interop.UIAutomationCore;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    abstract class AbstractScrollPatternScroller : IScroller
    {

        private IUIAutomationScrollPattern m_scrollPattern;

        public AbstractScrollPatternScroller(
            IUIAutomationScrollPattern scrollPattern)
        {
            m_scrollPattern = scrollPattern;
        }

        abstract public void setScrollPercentage(double scrollPercentage);

        abstract public double getScrollPercentage();

        protected IUIAutomationScrollPattern getScrollPattern()
        {
            return m_scrollPattern;
        }
    }
}
