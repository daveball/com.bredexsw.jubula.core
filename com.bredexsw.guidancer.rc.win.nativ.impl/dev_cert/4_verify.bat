: Verify a signed program.exe
:  /v verbose (success, fail, warning)
:  /pa Specifies that the Default Authentication Verification Policy is used.
:  FILE_NAME The already signed executable file.
: see http://msdn.microsoft.com/en-us/library/windows/desktop/aa387764%28v=vs.85%29.aspx

set SIGNTOOL_DIR="C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin"
%SIGNTOOL_DIR%\signtool verify /v /pa "program.exe"
pause
