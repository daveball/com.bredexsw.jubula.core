: A developer must install root_ca.crt (Local Machine) and ca_signed_with_root_ca.crt_and_ca.key.pfx (Personal)
:   1) Install root_ca.crt at "Local Machine"
:     0. Login with administrator account
:     1. Double click on root_ca.crt
:     2. Click "Install Certificate..."
:     3. Select "Local Machine", click Next->Yes
:     4. Select "Automatically select the certificate store based on the type of the certificate", click Next->Finish
:   2) Move bredex.de Zertifcate from "Intermediate Certification Authorities" (ICA) to "Thrusted Root Certification Authorities" (TRCA)
:     0. Login with user account
:     1. start certmgr
:     2. Drag & Drop the bredex.de certificate from ICA to TRCA
:
:   3) Install ca_signed_with_root_ca.crt_and_ca.key.pfx at "Current User"
:     0. Login with user account
:     1. Double click on ca_signed_with_root_ca.crt_and_ca.key.pfx
:     2. Click "Install Certificate..."
:     3. Select "Current User", click Next
:     4. Select "Mark this key as exportable..." and "Include all extended properties", click Next
:     4. Select "Automatically select the certificate store based on the type of the certificate", click Next->Finish

: A user installs root_ca.crt described by 1) and 2)


: Sign your program.exe
:  /v verbose (success, fail, warning)
:  /s STORE_NAME The store name, where the cert is installed. Default is MY (Is MY the local user store???)
:  /n SUBJECT_NAME Part of the subject name (company name???)
:  FILE_NAME The executable file, which will be signed.
: see http://msdn.microsoft.com/en-us/library/windows/desktop/aa387764%28v=vs.85%29.aspx

set SIGNTOOL_DIR="C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin"
%SIGNTOOL_DIR%\signtool sign /v /s MY /n "bredex.de" "com.bredexsw.guidancer.rc.win.nativ.impl.exe"

:pause
