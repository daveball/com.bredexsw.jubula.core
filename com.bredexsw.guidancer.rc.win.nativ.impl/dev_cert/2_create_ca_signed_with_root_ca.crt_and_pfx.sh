# create ca certificate signed with the root ca
# The file ca_signed_with_root_ca.crt must be installed by user for starting the signed program (otherwise the signed certificate from a thrusted authority must be bought, so that the user do not have to install our certificate).
openssl x509 -req -days 365 -in ca.crt -CA root_ca.crt -CAkey root_ca.key -set_serial 01 -out ca_signed_with_root_ca.crt

# create a pfx file containing ca.key and ca_signed_with_root_ca.crt
# The file ca_signed_with_root_ca.crt_and_ca.key.pfx is used by developers to sign a program.
openssl pkcs12 -export -out ca_signed_with_root_ca.crt_and_ca.key.pfx -inkey ca.key -in ca_signed_with_root_ca.crt
