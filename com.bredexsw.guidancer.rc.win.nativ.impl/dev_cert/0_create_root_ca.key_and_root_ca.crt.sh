# see http://www.top20toolbar.com/misc/codesigncert.htm

# create root CA key
openssl genrsa -out root_ca.key 4096

# create root CA certificate
openssl req -new -x509 -days 365 -key root_ca.key -out root_ca.crt
# Country Name: DE
# State: Niedersachsen
# Locality: Braunschweig
# Organisation: BREDEX GmbH
# Organisation unit (Development or Support): []
# Common name use your domain name: bredex.de
# Email address: info@bredex.de 