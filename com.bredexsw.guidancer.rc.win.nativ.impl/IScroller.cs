﻿
namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    interface IScroller
    {
        void setScrollPercentage(double scrollPercentage);

        double getScrollPercentage();
    }
}
