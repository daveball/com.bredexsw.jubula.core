﻿using Interop.UIAutomationCore;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class VerticalScrollPatternScroller : AbstractScrollPatternScroller
    {
        public VerticalScrollPatternScroller(
            IUIAutomationScrollPattern scrollPattern)
                : base(scrollPattern)
        {

        }

        public override void setScrollPercentage(double scrollPercentage)
        {
            getScrollPattern().SetScrollPercent(
                ComUtils.NO_SCROLL, scrollPercentage);
        }

        public override double getScrollPercentage()
        {
            return getScrollPattern().CurrentVerticalScrollPercent;
        }
    }
}
