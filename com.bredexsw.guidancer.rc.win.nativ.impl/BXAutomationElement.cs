﻿using Interop.UIAutomationCore;
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class BXAutomationElement
    {
        public IUIAutomationElement automationElement { get; set; }

        public BXAutomationElement(IUIAutomationElement element)
        {
            automationElement = element;
        }
        
        public override bool Equals(Object obj)
        {
            if (obj is BXAutomationElement)
            {
                BXAutomationElement BXElement = obj as BXAutomationElement;
                return ComUtils.isEquals(automationElement, BXElement.automationElement);
            }
            else
            {
                return false;
            }
            
        }

        public override int GetHashCode()
        {
            return 20091988;
        }
    }
}
