﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Constants class for all GD modes
    /// </summary>
    class ModeConstants
    {

    /// <summary>
    /// Constant for mode, in which the tests are performed 
    /// </summary>
    public const int TESTING = 1;
    /// <summary>
    /// Constant for object mapping operations
    /// </summary>
    public const int OBJECT_MAPPING = 2;
    /// <summary>
    /// Constant for recording operations
    /// </summary>
    public const int RECORD_MODE = 3;
    /// <summary>
    /// Constant for checking operations
    /// </summary>
    public const int CHECK_MODE = 4;
    /// <summary>
    /// Constant for authierarchy operations
    /// </summary>
    public const int AUT_HIERARCHY = 5;

    }
}
