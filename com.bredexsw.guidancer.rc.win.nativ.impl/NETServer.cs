﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using log4net;
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Main class of the Win server, starts the Communicator with the committed port argument.
    /// </summary>    
    class NETServer
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NETServer));

        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                try
                {
                    int port = Convert.ToInt32(args[0]);
                    LOG.InfoFormat("Starting communicator on port: {0}", port);
                    Communicator com = new Communicator(port);
                }
                catch (FormatException fe)
                {
                    LOG.Error("Error occurred while parsing port number argument.", fe);
                }
            }
            else
            {
                LOG.Warn("Win RC server was executed with no arguments.");
                //Communicator com = new Communicator(11475);
            }
        }
    }
}
