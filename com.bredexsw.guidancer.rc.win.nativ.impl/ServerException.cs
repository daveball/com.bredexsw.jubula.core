﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Exception Class for all exceptions which occurred on the Win server and should
    /// be send back to the AUTAgent.
    /// </summary>
    class ServerException : Exception
    {
        /// <value>
        /// ID of the exception
        /// </value>
        public int exceptionID { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ServerException() : base() { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">
        /// the exception message
        /// </param>
        public ServerException(string message) : base(message) { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">
        /// the exception message
        /// </param>
        /// <param name="id">
        /// the exception id
        /// </param>
        public ServerException(string message, int id) : base(message)
        {
            this.exceptionID = id;
        }

    }
}
