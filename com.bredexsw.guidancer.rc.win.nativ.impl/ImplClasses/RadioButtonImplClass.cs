﻿using Interop.UIAutomationCore;
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class RadioButtonImplClass : AbstractButtonImplClass
    {
        /// <summary>
        /// Check if a radioButton is selected
        /// </summary>
        /// <param name="ae">the radioButton</param>
        /// <returns>the check result</returns>
        [Obsolete]
        public bool checkRadioButtonSelection()
        {
            IUIAutomationSelectionItemPattern radioCheck = element.GetCurrentPattern(
                UiaPatternIds.UIA_SelectionItemPatternId) as IUIAutomationSelectionItemPattern;
            return Convert.ToBoolean(radioCheck.CurrentIsSelected);
        }

        /// <summary>
        /// Check if a radioButton is selected
        /// </summary>
        /// <param name="ae">the radioButton</param>
        /// <returns>the check result</returns>
        public bool checkButtonSelection()
        {
            IUIAutomationSelectionItemPattern radioCheck = element.GetCurrentPattern(
                UiaPatternIds.UIA_SelectionItemPatternId) as IUIAutomationSelectionItemPattern;
            return Convert.ToBoolean(radioCheck.CurrentIsSelected);
        }
    }
}
