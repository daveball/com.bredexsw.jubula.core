﻿using Interop.UIAutomationCore;
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ListImplClass : AbstractElementImplClass
    {
        protected IUIAutomationElementArray getListItems()
        {
            return element.FindAll(
                TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_ListItemControlTypeId));
        }

        protected IUIAutomationElement getFocusListItem(IUIAutomationElementArray currentListItems)
        {
            IUIAutomationCondition focusCondition =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_HasKeyboardFocusPropertyId, true);

            for (int i = 0; i < currentListItems.Length; i++)
            {
                IUIAutomationElement item = currentListItems.GetElement(i);
                 
                if (AutomationConditionMatcher.matches(item, focusCondition))
                {
                    return item;
                }
            }
           
            return null;
        }

        protected bool isListItemSelected(IUIAutomationElement listitem)
        {
            IUIAutomationSelectionItemPattern selectionPattern =
                            listitem.GetCurrentPattern(
                             UiaPatternIds.UIA_SelectionItemPatternId)
                                as IUIAutomationSelectionItemPattern;

            if (Convert.ToBoolean(selectionPattern.CurrentIsSelected))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
