﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ApplicationImplClass
    {
        /// <summary>
        /// Get all window titles of visible windows
        /// </summary>
        /// <returns>the window titles</returns>
        public string[] getAllWindowTitles()
        {
            IUIAutomationCondition isWindowCond =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_WindowControlTypeId);
            IUIAutomationCondition isOffscreenCond =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_IsOffscreenPropertyId,
                    false);
            IUIAutomationCondition andCond =
                ComUtils.AUTOMATION.CreateAndCondition(
                    isWindowCond, isOffscreenCond);

            IUIAutomationElementArray windows =
                ComUtils.AUTOMATION.GetRootElement().FindAll(
                    TreeScope.TreeScope_Children,
                    andCond);

            List<String> names = new List<string>();
            for (int i = 0; i < windows.Length; i++)
            {
                names.Add(windows.GetElement(i).CurrentName);
            }
            return names.ToArray();
        }

        /// <summary>
        /// Get the active AUT window bounds
        /// </summary>
        /// <returns>The bound values</returns>
        public int[] getActiveWindowBounds()
        {
            return AbstractElementImplClass.getBounds(WinRobotImpl.getForegroundAutomationWindow());
        }

        /// <summary>
        /// Get the name of the current active window.
        /// </summary>
        /// <returns>The name of the current active window</returns>
        public string getActiveWindowTitle()
        {
            return WinRobotImpl.getForegroundAutomationWindow().CurrentName;
        }       
    }
}
