﻿using Interop.UIAutomationCore;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class MenuImplClass : AbstractElementImplClass
    {
        /// <summary>
        /// Check Selection ID String
        /// </summary>
        private const string checkSelectionID = "selection";
        /// <summary>
        /// Check Existence ID String
        /// </summary>
        private const string checkExistenceID = "existence";
        /// <summary>
        /// Check Enablement ID String
        /// </summary>
        private const string checkEnablementID = "enablement";
        /// <summary>
        /// Check Expandable ID String (e.g. hasSubMenu)
        /// </summary>
        private const string checkExpandableID = "expandable";
        
        /// <summary>
        /// The current menu / menuItem / of the aut, neeed for name path selection
        /// </summary>
        public static IUIAutomationElement currentItem;
        
        
        /// <summary>
        /// Action method for selecting a menu item in the menu bar by the index path.
        /// </summary>
        /// <param name="iPath">the array with the item indexes<</param>
        [Obsolete]
        public bool selectMenuItemByIndexpath(object[] iPath, bool checkOnly, string checkProperty)
        {
            int[] indexPath = iPath.Cast<int>().ToArray();
            IUIAutomationElement foregroundElement = WinRobotImpl.getForegroundAutomationWindow();
            IUIAutomationElementArray menuItems = getMenuItemsFromElement(foregroundElement);
            bool checkResult = checkOnly;
            int level = 0;

            try
            {
                menuItemIndexSelect(menuItems, ref level, indexPath, ref checkResult, checkProperty);
            }
            catch (IndexOutOfRangeException)
            {
                throw new ServerException("MenuItem does not exist.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
            catch (COMException)
            {
                throw new ServerException("MenuItem is disabled.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
            finally
            {
                if (checkOnly)
                {
                    for (int i = 0; i <= level; i++)
                    {
                        SendKeys.SendWait(InputConstants.VK_ESC);
                    }
                }
            }
            return checkResult;
        }

        /// <summary>
        /// Gives the menuItems from a given Element.
        /// </summary>
        /// <param name="element">Element which contains the menuItems</param>
        /// <returns>the menuItems</returns>
        public static IUIAutomationElementArray getMenuItemsFromElement(
            IUIAutomationElement element)
        {
            //condition to get the menu item
            IUIAutomationCondition menuItemCond =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_MenuItemControlTypeId);
            //condition to get the menu element
            IUIAutomationCondition menuCond =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_MenuControlTypeId);
            //condition to get the menu bar
            IUIAutomationCondition menuBarCond =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_MenuBarControlTypeId);

            //we want menuItems from a wpf window
            if (element != null && isWPFElement(element))
            {
                IUIAutomationElementArray menuItems = null;
                //select menu bar items from wpf window
                if (element.CurrentControlType.Equals(
                    UiaControlTypeIds.UIA_WindowControlTypeId))
                {
                    //window menu bar
                    IUIAutomationElement menuBar = element.FindFirst(TreeScope.TreeScope_Children, menuCond);
                    //select all menu items from the bar
                    menuItems = menuBar.FindAll(TreeScope.TreeScope_Children, menuItemCond);
                }
                //else if (element.CurrentControlType.Equals(UiaControlTypeIds.UIA_MenuControlTypeId))
                //{
                //}
                //get all 2nd and deeper menu items from a menu bar window
                else if (element.CurrentControlType.Equals(
                    UiaControlTypeIds.UIA_MenuItemControlTypeId) ||
                    element.CurrentControlType.Equals(
                    UiaControlTypeIds.UIA_MenuControlTypeId))
                {
                    menuItems = element.FindAll(
                        TreeScope.TreeScope_Descendants,
                        ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_ControlTypePropertyId,
                            UiaControlTypeIds.UIA_MenuItemControlTypeId));

                }

                return menuItems;
            }
            else if(element!=null)
            {
                //we want the menuItems from a menuItem object
                if (element.CurrentControlType.Equals(
                    UiaControlTypeIds.UIA_MenuItemControlTypeId))
                {
                    IUIAutomationElementArray items = element.FindAll(
                        TreeScope.TreeScope_Descendants,
                        ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_ControlTypePropertyId,
                            UiaControlTypeIds.UIA_MenuItemControlTypeId));
                    return items;
                }
                //we want the menuItems from a menu object
                else if (element.CurrentControlType.Equals(UiaControlTypeIds.UIA_MenuControlTypeId))
                {
                    IUIAutomationElementArray items = element.FindAll(TreeScope.TreeScope_Children,
                            ComUtils.AUTOMATION.CreatePropertyCondition(UiaPropertyIds.UIA_ControlTypePropertyId,
                            UiaControlTypeIds.UIA_MenuItemControlTypeId));
                    return items;
                }
                //we want the menuItems from a menubar which is in a window located
                else
                {
                    //select only the user menuBar which does not have the name "System Menu Bar"
                    IUIAutomationCondition noSysMenuCond =
                        ComUtils.AUTOMATION.CreateNotCondition(
                            ComUtils.AUTOMATION.CreatePropertyCondition(
                                UiaPropertyIds.UIA_NamePropertyId,
                                "System"));
                    //marge the menuBarCond and noSysMenuCond to one IUIAutomationCondition which must be true for a result
                    IUIAutomationCondition andCon =
                        ComUtils.AUTOMATION.CreateAndCondition(menuBarCond, noSysMenuCond);

                    IUIAutomationElement menuBar = element.FindFirst(TreeScope.TreeScope_Descendants, andCon);

                    if (menuBar == null)
                    {
                        throw new ServerException("No menu found.",
                                ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                    }

                    return menuBar.FindAll(TreeScope.TreeScope_Children, menuItemCond);
                }
            }
            return null;
        }

        /// <summary>
        /// Searches in the given menuItem Collection for the given index and selects or expands the element.
        /// </summary>
        /// <param name="menuItems">collection of menuItems</param>
        /// <param name="index">the current index position in the path array</param>
        /// <param name="path">the path array filled with menuItem indexes</param>
        [Obsolete]
        public static void menuItemIndexSelect(IUIAutomationElementArray menuItems, ref int index, int[] path
            , ref bool onlyCheck, string checkProperty)
        {
            if (index < path.Count() && menuItems.Length > 0)
            {
                IUIAutomationElement menuItem = menuItems.GetElement(path[index] - 1);
                IUIAutomationExpandCollapsePattern expandPattern =
                    menuItem.GetCurrentPattern(UiaPatternIds.UIA_ExpandCollapsePatternId)
                        as IUIAutomationExpandCollapsePattern;

                // last element in the path, do not expand or invoke
                if (index + 1 == path.Count() && onlyCheck == true)
                {
                    onlyCheck = checkPropertys(menuItem, checkProperty);
                }
                // expand the menuItem
                else if (expandPattern != null)
                {
                    WinRobotImpl.moveMouseToElement(menuItem);
                    WinRobotImpl.click(1, 1);
                    ++index;

                    if (index < path.Count())
                    {
                        IUIAutomationElementArray items = null;
                        if (!isWPFElement(menuItem))
                        {
                            items = getMenuItemsFromElement(getContextMenuMenuWithTimeout());
                        }
                        if (items == null || items.Length == 0)
                        {
                            items = getMenuItemsFromElement(menuItem);
                        }                    
                        menuItemIndexSelect(items, ref index, path, ref onlyCheck, checkProperty);
                    }
                    
                }
                // invoke the menuItem
                else
                {
                    WinRobotImpl.moveMouseToElement(menuItem);

                    IUIAutomationInvokePattern invokePattern =
                        menuItem.GetCurrentPattern(UiaPatternIds.UIA_InvokePatternId)
                            as IUIAutomationInvokePattern;
                    invokePattern.Invoke();
                    ++index;

                    if (index < path.Count())
                    {
                        IUIAutomationElementArray items = null;
                        if (!isWPFElement(menuItem))
                        {
                            items = getMenuItemsFromElement(getContextMenuMenuWithTimeout());
                        }
                        if (items == null || items.Length == 0)
                        {
                            items = getMenuItemsFromElement(menuItem);
                        }

                        menuItemIndexSelect(items, ref index, path, ref onlyCheck, checkProperty);
                    }
                }
            }
            else if (index < path.Count() && menuItems.Length == 0)
            {
                //path indexes left but no menu items left to select
                throw new ServerException("MenuItem does not exist.",
                    ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
        }

        /// <summary>
        /// Searches for a open second level contextMenu which is a child from the current open window.
        /// At least needed if we want to get the items from the second context menu level.
        /// </summary>
        /// <returns>the menu object</returns>    
        private static IUIAutomationElement getContextMenuMenu()
        {
            IUIAutomationTreeWalker walker = ComUtils.AUTOMATION.CreateTreeWalker(
                            ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_ControlTypePropertyId,
                                UiaControlTypeIds.UIA_MenuControlTypeId));
            IUIAutomationElement window = WinRobotImpl.getForegroundAutomationWindow();
            return walker.GetFirstChildElement(window);
        }

        /// <summary>
        /// Searches for a open second level contextMenu which is a child from the current open window.
        /// At least needed if we want to get the items from the second context menu level.
        /// </summary>
        /// <returns>the menu object</returns>
        public static IUIAutomationElement getContextMenuMenuWithTimeout()
        {
            IUIAutomationElement menu = getContextMenuMenu();
            if (menu == null)
            {
                long startMS = WinRobotImpl.getCurrentTimeInMilliseconds();
                while (menu == null && WinRobotImpl.getCurrentTimeInMilliseconds() - startMS < 1000)
                {
                    menu = getContextMenuMenu();
                }
            }
            return menu;
        }


        public static bool checkPropertys(IUIAutomationElement ae, string propertyName)
        {
            if (checkSelectionID.Equals(propertyName))
            {
                IUIAutomationSelectionItemPattern selectionItemPattern =
                    ae.GetCurrentPattern(UiaPatternIds.UIA_SelectionItemPatternId)
                        as IUIAutomationSelectionItemPattern;
                if (selectionItemPattern != null)
                {
                    RadioButtonImplClass radioBoxItem = new RadioButtonImplClass();
                    radioBoxItem.setElement(ae);
                    return radioBoxItem.checkRadioButtonSelection();
                }
                else if (selectionItemPattern == null)
                {
                    IUIAutomationLegacyIAccessiblePattern legacyPattern =
                        ae.GetCurrentPattern(UiaPatternIds.UIA_LegacyIAccessiblePatternId)
                        as IUIAutomationLegacyIAccessiblePattern;

                    uint state = legacyPattern.CurrentState;
                    bool isChecked = (uint)AccessibleStates.Checked == ((uint)AccessibleStates.Checked & state);

                    if (isChecked)
                    {
                        return true;
                    }
                    return false;
                }

                CheckBoxImplClass checkBoxItem = new CheckBoxImplClass();
                checkBoxItem.setElement(ae);
                return checkBoxItem.checkCheckBoxSelection();
            }
            else if (checkExistenceID.Equals(propertyName))
            {
                return (ae != null);
            }
            else if (checkEnablementID.Equals(propertyName))
            {
                return checkEnablement(ae);

            }
            else if (checkExpandableID.Equals(propertyName))
            {
                IUIAutomationExpandCollapsePattern expandPattern = null;
                try
                {
                    expandPattern =
                     ae.GetCurrentPattern(UiaPatternIds.UIA_ExpandCollapsePatternId)
                         as IUIAutomationExpandCollapsePattern;
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
                if (expandPattern != null)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Check if the given element ( menuItems ) are selected, exists or enable.
        /// </summary>
        /// <param name="name">the name of the menuItem to check</param>
        /// <param name="propertyName">name of the check id</param>
        /// <returns>the check result</returns>
        public static bool checkPropertys(String name, string propertyName)
        {
            IUIAutomationElement ae = getMenu(name);
            return checkPropertys(ae, propertyName);
        }

        /// <summary>
        /// Inits the main menu of the aut foreground window
        /// </summary>
        public void initWindowMenu()
        {
            IUIAutomationElement foregroundElement = WinRobotImpl.getForegroundAutomationWindow();
            currentItem = foregroundElement;
        }

        /// <summary>
        /// Gets the menu bar of the foreground window.
        /// </summary>
        /// <returns>The menu bar or null, if the foreground window does not have a menu bar.</returns>
        private static IUIAutomationElement getMenuBar()
        {
            IUIAutomationElement window = WinRobotImpl.getForegroundAutomationWindow();
            IUIAutomationCondition condition = null;
            if (isWPFElement(window))
            {
                condition = ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_MenuControlTypeId);
            }
            else
            {
                condition = ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_MenuBarControlTypeId);
            }
            return window.FindFirst(TreeScope.TreeScope_Children, condition);
        }

        /// <summary>
        /// Check if for the open window a menubar exist
        /// </summary>
        /// <returns>menubar existence</returns>
        public static bool existsMenuBar()
        {
            return getMenuBar() != null;
        }

        /// <summary>
        /// Selects a menu item from the current menu.
        /// </summary>
        /// <param name="name">The name of the menu item which will be selected</param>
        [Obsolete]
        public bool selectMenuItemByName(string name, bool check, string checkProperty)
        {
            try
            {
                //search for children in the menuItem
                IUIAutomationElement ae = currentItem.FindFirst(
                    TreeScope.TreeScope_Descendants,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_NamePropertyId, name));
                //if no item found it could be that we have to search for a menu with the childs
                if (ae == null)
                {
                    IUIAutomationElementArray items = null;

                    if (!isWPFElement(currentItem))
                    {
                        items = getMenuItemsFromElement(getContextMenuMenuWithTimeout());
                    }
                    else
                    {
                        items = getMenuItemsFromElement(currentItem);
                    }

                    for (int i = 0; i < items.Length; i++)
                    {
                        IUIAutomationElement item = items.GetElement(i);
                        if (item.CurrentName.Equals(name))
                        {
                            ae = item;
                            break;
                        }
                    }
                }
                //still not item found means that the menuItem realy does not exist
                if (ae == null)
                {
                    throw new ServerException("MenuItem does not exist.",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
                //check the item
                else if (check == true)
                {
                    return checkPropertys(ae, checkProperty);
                }
                //perform selection
                else
                {
                    IUIAutomationExpandCollapsePattern expandPattern =
                        ae.GetCurrentPattern(UiaPatternIds.UIA_ExpandCollapsePatternId)
                            as IUIAutomationExpandCollapsePattern;
                    if (expandPattern != null)
                    {
                        WinRobotImpl.moveMouseToElement(ae);
                        WinRobotImpl.click(1, 1);
                    }
                    else
                    {
                        IUIAutomationInvokePattern invokePattern =
                            ae.GetCurrentPattern(UiaPatternIds.UIA_InvokePatternId)
                                as IUIAutomationInvokePattern;
                        WinRobotImpl.moveMouseToElement(ae);
                        invokePattern.Invoke();
                    }
                    currentItem = ae;
                }
            }
            catch (COMException ce)
            {
                // ElementNotEnabledException
                if (ComUtils.isExceptionOfType(ce, ComUtils.UIA_E_ELEMENTNOTENABLED))
                {
                    throw new ServerException("MenuItem is disabled.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }

                throw ce;
            }
            return false;
        }

        /// <summary>
        /// Gets the first menu from the opened context menu or opened menu bar.
        /// </summary>
        /// <param name="ae">The parent automation element right clicked for context menu or null for getting the first opened menu of the menu bar.</param>
        /// <returns>The menu with the given parent automation element or null, if it does not exist.</returns>
        public static IUIAutomationElement getFirstMenu(IUIAutomationElement ae)
        {
            IUIAutomationCondition menuCondition =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_MenuControlTypeId);
            IUIAutomationCondition idCondition =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ProcessIdPropertyId,
                    WinRobotImpl.getProcessId());

            Thread.Sleep(20);
            IUIAutomationElement contextMenu = null;
            if (ae == null)
            { // menu of menu bar
                // FIXME: Currently we only wait 300ms and assume the menu of the menu bar has been closed.
                //        The method by getMenuItemsFromElement(ae) throws "no menu found", so currently
                //        we can not verify, if the menu of a menu bar has really been closed.
                Thread.Sleep(300);
            }
            else
            { // context menu
                if (isWPFElement(ae))
                {
                    IUIAutomationElement activeWindow = WinRobotImpl.getForegroundAutomationWindow();
                    contextMenu = activeWindow.FindFirst(TreeScope.TreeScope_Descendants,
                        menuCondition);
                }
                else
                {
                    IUIAutomationCondition and =
                        ComUtils.AUTOMATION.CreateAndCondition(menuCondition, idCondition);
                    contextMenu =
                    ComUtils.AUTOMATION.GetRootElement().FindFirst(
                        TreeScope.TreeScope_Children, and);
                }
            }
            return contextMenu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static IUIAutomationElement getMenu(string name)
        {
            //search for children in the menuItem
            IUIAutomationElement ae = currentItem.FindFirst(
                TreeScope.TreeScope_Descendants,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_NamePropertyId, name));
            //if no item found it could be that we have to search for a menu with the childs
            if (ae == null)
            {
                IUIAutomationElementArray items = null;

                if (!isWPFElement(currentItem))
                {
                    items = getMenuItemsFromElement(getContextMenuMenuWithTimeout());
                }
                else
                {
                    items = getMenuItemsFromElement(currentItem);
                }

                for (int i = 0; i < items.Length; i++)
                {
                    IUIAutomationElement item = items.GetElement(i);
                    if (item.CurrentName.Equals(name))
                    {
                        ae = item;
                        break;
                    }
                }
            }
            //still not item found means that the menuItem realy does not exist
            if (ae == null)
            {
                throw new ServerException("MenuItem does not exist.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
            return ae;
        }
        /// <summary>
        /// selects a menu with a given name
        /// </summary>
        public bool selectMenuItemByName(string name)
        {
            try
            {
                //search for children in the menuItem
                IUIAutomationElement ae = getMenu(name);
                //perform selection
                
                
                    IUIAutomationExpandCollapsePattern expandPattern =
                        ae.GetCurrentPattern(UiaPatternIds.UIA_ExpandCollapsePatternId)
                            as IUIAutomationExpandCollapsePattern;
                    if (expandPattern != null)
                    {
                        WinRobotImpl.moveMouseToElement(ae);
                        WinRobotImpl.click(1, 1);
                    }
                    else
                    {
                        IUIAutomationInvokePattern invokePattern =
                            ae.GetCurrentPattern(UiaPatternIds.UIA_InvokePatternId)
                                as IUIAutomationInvokePattern;
                        WinRobotImpl.moveMouseToElement(ae);
                        invokePattern.Invoke();
                    }
                    currentItem = ae;
                
            }
            catch (COMException ce)
            {
                // ElementNotEnabledException
                if (ComUtils.isExceptionOfType(ce, ComUtils.UIA_E_ELEMENTNOTENABLED))
                {
                    throw new ServerException("MenuItem is disabled.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }

                throw ce;
            }
            return false;
        }
    
    }
}
