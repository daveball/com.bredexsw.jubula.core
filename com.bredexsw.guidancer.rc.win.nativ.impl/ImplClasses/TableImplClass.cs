﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    abstract class TableImplClass : AbstractElementImplClass
    {
        /// <summary>
        /// the observed value of DataGridView cells with empty contents
        /// </summary>
        private const string EMPTY_CELL_WORKAROUND_VALUE = "(null)";

        /// <summary>
        /// The maximum difference between a scrollable component's upper bound (x + width, or y + height)
        /// and its scrollbar's upper bound that will still allow that scrollbar to be positively identified
        /// as a vertical or horizontal scrollbar. So far, little effort has gone into determining this value,
        /// so any necessary changes are welcome. Just be careful not to set it so high that the incorrect
        /// scrollbar could be identified (i.e. horizontal scrollbar mistaken for vertical scrollbar, or 
        /// vice versa).
        /// </summary>
        private const int SCROLLBAR_LOCATION_TOLERANCE = 5;

        public abstract bool isCellAtCursorPositionEditable();

        public bool isCellAtCursorPositionEditable(int controlType)
        {
            IUIAutomationElement elementToCheck = WinRobotImpl.getElementAtCursorPosition();

            IUIAutomationElement parent = ComUtils.AUTOMATION.ControlViewWalker.
            GetParentElement(elementToCheck);
            //check if one of the parents is from type table, if not then its not a cell
            while (true)
            {
                if (ComUtils.isEquals(parent, ComUtils.AUTOMATION.GetRootElement()) || parent == null)
                {
                    throw new ServerException("Invalid row / column value");
                }
                else if (parent.CurrentControlType == controlType)
                {
                    break;
                }

                parent = ComUtils.AUTOMATION.ControlViewWalker.
                GetParentElement(parent);
            }

            //must be enabled, keyboardfocusable and from type edit to be a editable cell
            if (Convert.ToBoolean(elementToCheck.CurrentIsEnabled) &&
            Convert.ToBoolean(elementToCheck.CurrentIsKeyboardFocusable) &&
            elementToCheck.CurrentControlType == UiaControlTypeIds.UIA_EditControlTypeId)
            {
                return true;
            }
            return false;
        }

        public IUIAutomationElement[] getTableRowHeaderElements(
            IUIAutomationElement tableElement)
        {
            List<IUIAutomationElement> headerElements =
                new List<IUIAutomationElement>();

            foreach (IUIAutomationElement row in getRows(tableElement))
            {
                IUIAutomationElement rowHeader = row.FindFirst(TreeScope.TreeScope_Children,
                    ComUtils.AUTOMATION.CreatePropertyCondition(UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_HeaderControlTypeId));

                headerElements.Add(rowHeader);
            }

            return headerElements.ToArray();
        }


        protected abstract IUIAutomationElement[] getRows(IUIAutomationElement tableElement);


        public string[][] getTableContents()
        {
            List<string[]> tableContents = new List<string[]>();
            IUIAutomationElementArray[] contentCells = getCells(this.getElement());

            try
            {
                foreach (IUIAutomationElementArray row in contentCells)
                {
                    List<string> rowContents = new List<string>();

                    for (int i = 0; i < row.Length; i++)
                    {
                        IUIAutomationElement cell = row.GetElement(i);
                        IUIAutomationLegacyIAccessiblePattern pattern =
                            cell.GetCurrentPattern(UiaPatternIds.UIA_LegacyIAccessiblePatternId) as IUIAutomationLegacyIAccessiblePattern;
                        string cellValue = pattern.CurrentValue;
                        if (EMPTY_CELL_WORKAROUND_VALUE.Equals(cellValue))
                        {
                            cellValue = string.Empty;
                        }
                        rowContents.Add(cellValue);
                    }

                    tableContents.Add(rowContents.ToArray());
                }

                return tableContents.ToArray();
            }
            catch (COMException ce)
            {
                throw new ServerException(ce.Message);
            }
        }

        protected IUIAutomationElementArray[] getCells(
            IUIAutomationElement tableElement)
        {
            List<IUIAutomationElementArray> rowList =
                new List<IUIAutomationElementArray>();

            IUIAutomationElement[] rows = getRows(tableElement);

            foreach (IUIAutomationElement row in rows)
            {
                IUIAutomationElementArray contentCells = getCellsinRow(row);
                if (contentCells.Length > 0)
                {
                    rowList.Add(contentCells);
                }
            }

            return rowList.ToArray();
        }

        protected abstract IUIAutomationElementArray getCellsinRow(IUIAutomationElement row);

        public string[] getTableRowHeaders()
        {
            List<string> headerLabels = new List<string>();
            try
            {
                foreach (IUIAutomationElement rowHeader
                    in getTableRowHeaderElements(this.getElement()))
                {
                    IUIAutomationValuePattern pattern =
                        rowHeader.GetCurrentPattern(UiaPatternIds.UIA_ValuePatternId)
                            as IUIAutomationValuePattern;
                    // If both row and column headers are present, then the 
                    // "header cell" in the absolute top-left corner does
                    // not support the Value Pattern. This is fine with us, as
                    // we would not want to include that value in the 
                    // header column values anyway.
                    if (pattern != null)
                    {
                        headerLabels.Add(pattern.CurrentValue);
                    }
                }

                return headerLabels.ToArray();
            }
            catch (COMException ce)
            {
                throw new ServerException(ce.Message);
            }
        }

        public bool isCellEditorFocused()
        {
            IUIAutomationElement cellEditorPane = this.getElement().FindFirst(
                TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_PaneControlTypeId));
            if (cellEditorPane != null)
            {
                IUIAutomationElement cellEditor = cellEditorPane.FindFirst(
                    TreeScope.TreeScope_Children,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_EditControlTypeId));
                return cellEditor != null
                    && Convert.ToBoolean(cellEditor.CurrentHasKeyboardFocus);
            }

            return false;
        }

        protected abstract IUIAutomationElementArray getTableColumnHeaderElements(
            IUIAutomationElement tableElement);

        /// <summary>
        /// Get the size and position of the table cell at the given
        /// index coordinates
        /// </summary>
        /// <param name="table">the table in which to find the cell</param>
        /// <param name="row">the row index of the cell</param>
        /// <param name="column">the column index of the cell</param>
        /// <returns>the bounds of the cell at the given index coordinates</returns>
        public int[] getCellBounds(int row, int col)
        {
            IUIAutomationElement table = this.getElement();
            WinRobotImpl.scrollToVisible(table);

            int[] boundValues = new int[4];
            IUIAutomationElement cell = null;

            if (row == -1)
            {
                // get column header bounds
                cell = getTableColumnHeaderElements(table).GetElement(col);
            }
            else if (col == -1)
            {
                // get row header bounds
                cell = getTableRowHeaderElements(table)[row];
            }
            else
            {
                // get content cell bounds
                cell = getCells(table)[row].GetElement(col);
            }

            if (ComUtils.isOffscreen(cell))
            {
                scrollToRow(table,
                    ComUtils.AUTOMATION.RawViewWalker.GetParentElement(cell));

                scrollToCell(table, cell);

            }

            Rect cellBounds =
                ComUtils.convertRect(cell.CurrentBoundingRectangle);

            // position
            boundValues[0] = (int)cellBounds.X;
            boundValues[1] = (int)cellBounds.Y;
            // size
            boundValues[2] = (int)cellBounds.Width;
            boundValues[3] = (int)cellBounds.Height;

            return boundValues;
        }


        protected virtual void scrollToRow(IUIAutomationElement table,
            IUIAutomationElement row)
        {
            IUIAutomationElement verticalScrollbar = WinRobotImpl.getVerticalScrollbar(table);
                        
            if (verticalScrollbar != null)
            {
                IUIAutomationValuePattern scrollValuePattern =
                    verticalScrollbar.GetCurrentPattern(
                        UiaPatternIds.UIA_ValuePatternId) as IUIAutomationValuePattern;

                if (scrollValuePattern != null)
                {
                    // ASSUMPTION: horizontal scrollbar is at bottom of the table
                    tagRECT tableClientArea = table.CurrentBoundingRectangle;
                    int viewPortUpperBound = tableClientArea.bottom;
                    IUIAutomationElement horizontalScrollbar =
                        WinRobotImpl.getHorizontalScrollbar(table);
                    if (horizontalScrollbar != null)
                    {
                        tagRECT horizontalScrollbarBounds =
                            horizontalScrollbar.CurrentBoundingRectangle;
                        viewPortUpperBound -= horizontalScrollbarBounds.bottom
                            - horizontalScrollbarBounds.top;

                    }

                    WinRobotImpl.scrollIntoView(
                        new ScrollbarValueScroller(scrollValuePattern), row,
                        tableClientArea.top, viewPortUpperBound, true);
                }
            }

        }

        protected virtual void scrollToCell(IUIAutomationElement table,
            IUIAutomationElement cell)
        {
            IUIAutomationElement horizontalScrollbar =
                WinRobotImpl.getHorizontalScrollbar(table);

            if (horizontalScrollbar != null)
            {
                IUIAutomationValuePattern scrollValuePattern =
                    horizontalScrollbar.GetCurrentPattern(
                        UiaPatternIds.UIA_ValuePatternId) as IUIAutomationValuePattern;
                if (scrollValuePattern != null)
                {
                    tagRECT tableClientArea = table.CurrentBoundingRectangle;
                    IUIAutomationElement verticalScrollbar =
                        WinRobotImpl.getVerticalScrollbar(table);
                    int viewPortUpperBound = tableClientArea.right;
                    if (verticalScrollbar != null)
                    {
                        tagRECT verticalScrollbarBounds =
                            verticalScrollbar.CurrentBoundingRectangle;
                        // ASSUMPTION: vertical scrollbar is on right-hand side of 
                        //             the table
                        viewPortUpperBound -= verticalScrollbarBounds.right
                            - verticalScrollbarBounds.left;
                    }
                    WinRobotImpl.scrollIntoView(
                        new ScrollbarValueScroller(scrollValuePattern), cell,
                        tableClientArea.left, viewPortUpperBound, false);
                }
            }
        }


        public int[] getCellAtMouseCursor()
        {
            IUIAutomationElement tableElement = this.getElement();

            // AutomationElement.FromPoint does not work here (it throws an 
            // inexplicable ElementNotAvailableException). So we need to do 
            // this "by hand".
            System.Windows.Point cursorPosition = new System.Windows.Point(
                    Cursor.Position.X, Cursor.Position.Y);

            if (ComUtils.convertRect(tableElement.CurrentBoundingRectangle)
                .Contains(cursorPosition))
            {

                IUIAutomationCondition cellTypeCondition = ComUtils.AUTOMATION.CreateOrCondition(
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_DataItemControlTypeId),
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_CustomControlTypeId)
                );

                // only look through the visible CustomControlCells or DataItemControlCells
                IUIAutomationElementArray visibleCustomElements =
                    tableElement.FindAll(TreeScope.TreeScope_Descendants,
                        ComUtils.AUTOMATION.CreateAndCondition(
                            ComUtils.AUTOMATION.CreatePropertyCondition(
                                UiaPropertyIds.UIA_IsOffscreenPropertyId,
                                false), cellTypeCondition));

                for (int i = 0; i < visibleCustomElements.Length; i++)
                {
                    IUIAutomationElement element = visibleCustomElements.GetElement(i);
                    IUIAutomationElement parent =
                        ComUtils.AUTOMATION.ControlViewWalker.GetParentElement(element);
                    // if the element is a cell and the cursor is within its 
                    // bounds, then we've found the cell we're looking for
                    if (!ComUtils.isEquals(tableElement, parent)
                        && ComUtils.convertRect(element.CurrentBoundingRectangle).Contains(cursorPosition))
                    {
                        IUIAutomationElement[] rows = getRows(tableElement);
                        int rowIndex = getIndex(getRows(tableElement), parent);
                        int columnIndex =
                            getIndex(getCellsinRow(parent), element);

                        return new int[] { rowIndex, columnIndex };
                    }
                }
            }

            // no cell belonging to desired table exists at mouse cursor
            // position
            return new int[] { -1, -1 };
        }



        private int getIndex(IUIAutomationElement[] collection, IUIAutomationElement element)
        {
            for (int i = 0; i < collection.Length; i++)
            {
                if (ComUtils.isEquals(collection[i], element))
                {
                    return i;
                }
            }

            return -1;
        }

        private int getIndex(IUIAutomationElementArray collection, IUIAutomationElement element)
        {
            for (int i = 0; i < collection.Length; i++)
            {
                if (ComUtils.isEquals(element, collection.GetElement(i)))
                {
                    return i;
                }
            }

            return -1;
        }

        public int[] getSelectedCell()
        {
            return getSelectedCell(this.getElement());
        }

        public int[] getSelectedCell(IUIAutomationElement element)
        {
            // find the first descendant element that "looks" like a 
            // cell and has focus
            IUIAutomationCondition cellTypeCondition = ComUtils.AUTOMATION.CreateOrCondition(
               ComUtils.AUTOMATION.CreatePropertyCondition(
               UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_DataItemControlTypeId),
               ComUtils.AUTOMATION.CreatePropertyCondition(
               UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_CustomControlTypeId)
               );

            IUIAutomationElement cell =
                this.getElement().FindFirst(TreeScope.TreeScope_Descendants,
                    ComUtils.AUTOMATION.CreateAndCondition(cellTypeCondition,
                        ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_HasKeyboardFocusPropertyId,
                            true)));

            IUIAutomationElement row =
                ComUtils.AUTOMATION.ControlViewWalker.GetParentElement(cell);

            int rowIndex = getIndex(getRows(this.getElement()), row);
            int columnIndex = getIndex(getCellsinRow(row), cell);

            return new int[] { rowIndex, columnIndex };
        }

        
    }
}
