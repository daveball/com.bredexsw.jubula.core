﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIAutomationClient;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class AbstractElementImpl
    {
        public static IUIAutomationElement element;

        public void setElement(String locator)
        {
            element = AUTHierarchyRobotImpl.findComponent(locator);
        }

        public IUIAutomationElement getElement()
        {            
            return element;
        }

        /// <summary>
        /// Gets the name of a automationElement
        /// </summary>
        /// <param name="locator">the locator</param>
        /// <returns>the name</returns>
        public string getName()
        {
            return element.CurrentName;
        }

        /// <summary>
        /// Checks if the IUIAutomationElement is enabled
        /// </summary>
        /// <param name="locator">the locator</param>
        /// <returns>the enablement status</returns>
        public bool checkEnablement()
        {
            return Convert.ToBoolean(element.CurrentIsEnabled);
        }
    }
}
