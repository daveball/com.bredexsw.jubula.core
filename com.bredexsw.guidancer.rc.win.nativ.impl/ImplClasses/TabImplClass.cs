﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class TabImplClass : AbstractElementImplClass
    {
        public string[] getTabItemLabels()
        {
            List<string> labels = new List<string>();

            IUIAutomationElementArray tabItems = getTabItems(this.getElement());

            for (int i = 0; i < tabItems.Length; i++)
            {
                labels.Add(tabItems.GetElement(i).CurrentName);
            }

            return labels.ToArray();
        }

        private IUIAutomationElementArray getTabItems(IUIAutomationElement tab)
        {
            return tab.FindAll(
                TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_TabItemControlTypeId));
        }

        public int[] getTabItemBounds(int index)
        {
            IUIAutomationElement item = getTabItems(this.getElement()).GetElement(index);
            return getBounds(item);
        }

        public bool isTabItemEnabled( int index)
        {
            IUIAutomationElement item = getTabItems(this.getElement()).GetElement(index);
            return Convert.ToBoolean(item.CurrentIsEnabled);
        }

        public int getSelectedTabItemIndex()
        {
            IUIAutomationElementArray tabItems = getTabItems(this.getElement());
            for (int i = 0; i < tabItems.Length; i++)
            {
                IUIAutomationElement item = tabItems.GetElement(i);                
                IUIAutomationSelectionItemPattern pattern =
                    item.GetCurrentPattern(
                        UiaPatternIds.UIA_SelectionItemPatternId)
                            as IUIAutomationSelectionItemPattern;
                if (Convert.ToBoolean(pattern.CurrentIsSelected))
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
