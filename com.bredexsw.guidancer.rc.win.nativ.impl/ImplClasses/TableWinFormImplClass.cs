﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class TableWinFormImplClass : TableImplClass
    {
        public string[] getTableColumnHeaders()
        {

            List<string> headerLabels = new List<string>();

            try
            {
                IUIAutomationElementArray columnHeaders =
                    getTableColumnHeaderElements(this.getElement());

                for (int i = 0; i < columnHeaders.Length; i++)
                {

                    IUIAutomationElement headerCell = columnHeaders.GetElement(i);
                    IUIAutomationValuePattern pattern =
                        headerCell.GetCurrentPattern(UiaPatternIds.UIA_ValuePatternId)
                            as IUIAutomationValuePattern;
                    // If both row and column headers are present, then the 
                    // "header cell" in the absolute top-left corner does
                    // not support the Value Pattern. This is fine with us, as
                    // we would not want to include that value in the 
                    // header column values anyway.
                    if (pattern != null)
                    {
                        headerLabels.Add(pattern.CurrentValue);
                    }
                }

                return headerLabels.ToArray();
            }
            catch (COMException ce)
            {
                throw new ServerException(ce.Message);
            }
        }

        protected override IUIAutomationElementArray getTableColumnHeaderElements(IUIAutomationElement tableElement)
        {
            IUIAutomationElement firstRow =
                tableElement.FindFirst(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_CustomControlTypeId));
            return firstRow.FindAll(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.CreatePropertyCondition(UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_HeaderControlTypeId));
        }

        public bool isSelectedCellEditable()
        {
            IUIAutomationElement table = this.getElement();
            //find the edit cell, if null, it means there is no editable cell in the table open
            IUIAutomationElement editCell = table.FindFirst(TreeScope.TreeScope_Descendants,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_EditControlTypeId));

            if (editCell == null)
            {
                return false;
            }

            //check if the editCell is a nasted component of the selectedCell
            int[] selectedCell = getSelectedCell(table);
            int[] selectedCellBounds = getCellBounds(selectedCell[0], selectedCell[1]);

            Rect editRect = ComUtils.convertRect(editCell.CurrentBoundingRectangle);
            Rect selectedRect = new Rect(selectedCellBounds[0], selectedCellBounds[1],
                selectedCellBounds[2], selectedCellBounds[3]);

            if (selectedRect.Contains(editRect))
            {
                //must be enabled, keyboardfocusable and own the keyboardfocus to be a current selected editable cell
                if (editCell != null &&
                    Convert.ToBoolean(editCell.CurrentIsEnabled) &&
                    Convert.ToBoolean(editCell.CurrentIsKeyboardFocusable) &&
                     Convert.ToBoolean(editCell.CurrentHasKeyboardFocus) &&
                    editCell.CurrentControlType == UiaControlTypeIds.UIA_EditControlTypeId)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

         public bool isCellEditable(int row, int col)
         {
             IUIAutomationElement table = this.getElement();
             //find the edit cell, if null, it means there is no editable cell in the table open
             IUIAutomationElement editCell = table.FindFirst(TreeScope.TreeScope_Descendants,
                 ComUtils.AUTOMATION.CreatePropertyCondition(
                     UiaPropertyIds.UIA_ControlTypePropertyId,
                     UiaControlTypeIds.UIA_EditControlTypeId));

             if (editCell == null)
             {
                 return false;
             }

             //get bounds to check if the editor is inside the cell            
             int[] selectedCellBounds = getCellBounds(row, col);
             Rect selectedRect = new Rect(selectedCellBounds[0], selectedCellBounds[1],
                 selectedCellBounds[2], selectedCellBounds[3]);
             Rect editRect = ComUtils.convertRect(editCell.CurrentBoundingRectangle);

             if (selectedRect.Contains(editRect))
             {
                 //must be enabled, keyboardfocusable and own the keyboardfocus to be a current selected editable cell
                 if (editCell != null &&
                     Convert.ToBoolean(editCell.CurrentIsEnabled) &&
                     Convert.ToBoolean(editCell.CurrentIsKeyboardFocusable) &&
                      Convert.ToBoolean(editCell.CurrentHasKeyboardFocus) &&
                     editCell.CurrentControlType == UiaControlTypeIds.UIA_EditControlTypeId)
                 {
                     return true;
                 }
                 return false;
             }
             return false;
         }

         /// <summary>
         /// Check if the cell at the current mouse position is editable.
         /// </summary>
         /// <returns>the cell editability</returns>
         public override bool isCellAtCursorPositionEditable()
         {
             return base.isCellAtCursorPositionEditable(UiaControlTypeIds.UIA_TableControlTypeId);
         }

         /// <summary>
         /// Finds and returns all rows in the given table element.
         /// </summary>
         /// <param name="tableElement">The table for which to find all rows.
         /// There is no type checking to make sure that this element represents
         /// a table. If the element does not represent a table, then the results
         /// are undefined.</param>
         /// <returns>all child elements that may be classified as a row in the
         /// table. This does not include the column header row even if it is 
         /// present. May be empty. Never null.</returns>
         protected override IUIAutomationElement[] getRows(IUIAutomationElement tableElement)
         {
             List<IUIAutomationElement> rows = new List<IUIAutomationElement>();

             IUIAutomationElementArray rowElements =
                 tableElement.FindAll(TreeScope.TreeScope_Children,
                     ComUtils.AUTOMATION.CreatePropertyCondition(
                             UiaPropertyIds.UIA_ControlTypePropertyId,
                             UiaControlTypeIds.UIA_CustomControlTypeId));

             IUIAutomationCondition cellTypeCondition = ComUtils.AUTOMATION.CreateOrCondition(
                 ComUtils.AUTOMATION.CreatePropertyCondition(
                 UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_DataItemControlTypeId),
                 ComUtils.AUTOMATION.CreatePropertyCondition(
                 UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_CustomControlTypeId)
                 );

             for (int i = 0; i < rowElements.Length; i++)
             {
                 IUIAutomationElement row = rowElements.GetElement(i);
                 IUIAutomationElement firstNonHeaderCell =
                     row.FindFirst(TreeScope.TreeScope_Children, cellTypeCondition);
                 if (firstNonHeaderCell != null)
                 {
                     rows.Add(row);
                 }
             }

             return rows.ToArray();
         }

         protected override IUIAutomationElementArray getCellsinRow(IUIAutomationElement row)
         {
             // find all non-header cells
             return row.FindAll(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.CreateNotCondition(
                 ComUtils.AUTOMATION.CreatePropertyCondition(
                     UiaPropertyIds.UIA_ControlTypePropertyId,
                     UiaControlTypeIds.UIA_HeaderControlTypeId)));

         }

    }  
}
