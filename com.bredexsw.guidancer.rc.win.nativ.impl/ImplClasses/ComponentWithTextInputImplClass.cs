﻿using Interop.UIAutomationCore;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ComponentWithTextInputImplClass : AbstractElementImplClass
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(NETServer));

        /// <summary>
        /// Gets e.g for the textfield the text
        /// </summary>
        /// <param name="locator">the locator</param>
        /// <returns>the text that is saved as the value or text pattern</returns>
        public string getText()
        {
            if (element.GetCurrentPropertyValue(UiaPropertyIds.UIA_IsValuePatternAvailablePropertyId)) {
                IUIAutomationValuePattern value = element.
                    GetCurrentPattern(UiaPatternIds.UIA_ValuePatternId)
                                as IUIAutomationValuePattern;
                return value.CurrentValue.Trim();
            }
            else {
                IUIAutomationTextPattern text = element.
                    GetCurrentPattern(UiaPatternIds.UIA_TextPatternId)
                            as IUIAutomationTextPattern;
                return text.DocumentRange.GetText(9999999).Trim();
            }

        }

        /// <summary>
        /// Checks if the component is editable
        /// </summary>        
        /// <returns>if its editable</returns>
        public bool isEditable()
        {
            IUIAutomationLegacyIAccessiblePattern legacyPattern =
                       element.GetCurrentPattern(UiaPatternIds.UIA_LegacyIAccessiblePatternId)
                       as IUIAutomationLegacyIAccessiblePattern;

            uint state = legacyPattern.CurrentState;
            bool isReadOnly = (uint)AccessibleStates.ReadOnly == ((uint)AccessibleStates.ReadOnly & state);

            if (!checkEnablement() || isReadOnly)
            {
                return false;
            }
            return true;
        }
    }
}
