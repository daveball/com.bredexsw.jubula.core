﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class TableWPFImplClass : TableImplClass
    {
        public string[] getTableColumnHeaders()
        {

            List<string> headerLabels = new List<string>();

            try
            {
                IUIAutomationElementArray columnHeaders =
                    getTableColumnHeaderElements(this.getElement());

                if (columnHeaders == null)
                {
                    //return a empty array
                    return headerLabels.ToArray();
                }

                for (int i = 0; i < columnHeaders.Length; i++)
                {

                    IUIAutomationElement headerCell = columnHeaders.GetElement(i);
                    //name of the headerCell is also the text / value of the cell
                    headerLabels.Add(headerCell.CurrentName);                    
                }

                return headerLabels.ToArray();
            }
            catch (COMException ce)
            {
                throw new ServerException(ce.Message);
            }
        }

        protected override IUIAutomationElementArray getTableColumnHeaderElements(
            IUIAutomationElement tableElement)
        {
            try
            {
                IUIAutomationElement firstRow =
                tableElement.FindFirst(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_HeaderControlTypeId));
                return firstRow.FindAll(TreeScope.TreeScope_Children, ComUtils.AUTOMATION.CreatePropertyCondition(UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_HeaderItemControlTypeId));
            }
            catch (Exception)
            {
                return null;
            }            
        }

        protected override void scrollToRow(IUIAutomationElement table,
            IUIAutomationElement row)
        {
            scrollToWPFGridItem(row);
        }

        protected override void scrollToCell(IUIAutomationElement table,
           IUIAutomationElement cell)
        {
            scrollToWPFGridItem(cell);
        }

        private void scrollToWPFGridItem(IUIAutomationElement gridItem)
        {
            IUIAutomationScrollItemPattern scrollItem = gridItem.GetCurrentPattern(UiaPatternIds.UIA_ScrollItemPatternId)
                as IUIAutomationScrollItemPattern;
            scrollItem.ScrollIntoView();
        }

        /// <summary>
        /// Finds and returns all rows in the given table element.
        /// </summary>
        /// <param name="tableElement">The table for which to find all rows.
        /// There is no type checking to make sure that this element represents
        /// a table. If the element does not represent a table, then the results
        /// are undefined.</param>
        /// <returns>all child elements that may be classified as a row in the
        /// table. This does not include the column header row even if it is 
        /// present. May be empty. Never null.</returns>
        protected override IUIAutomationElement[] getRows(IUIAutomationElement tableElement)
        {
            List<IUIAutomationElement> rows = new List<IUIAutomationElement>();

            IUIAutomationElementArray rowElements =
                tableElement.FindAll(TreeScope.TreeScope_Children,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_ControlTypePropertyId,
                            UiaControlTypeIds.UIA_DataItemControlTypeId));           

            for (int i = 0; i < rowElements.Length; i++)
            {              
                rows.Add(rowElements.GetElement(i));
            }

            return rows.ToArray();
        }

        protected override IUIAutomationElementArray getCellsinRow(IUIAutomationElement row)
        {
            // find all non-header cells
            IUIAutomationCondition cellTypeCondition = ComUtils.AUTOMATION.CreateAndCondition(
                ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ControlTypePropertyId, UiaControlTypeIds.UIA_CustomControlTypeId),
                ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ClassNamePropertyId, "DataGridCell")
                );

            return row.FindAll(TreeScope.TreeScope_Children, cellTypeCondition);

        }        

         public bool isSelectedCellEditable()
        {
            IUIAutomationElement table = this.getElement();
            //find the edit cell, if null, it means there is no editable cell in the table open
            IUIAutomationElement editCell = table.FindFirst(TreeScope.TreeScope_Descendants,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_EditControlTypeId));

            if (editCell == null)
            {
                return false;
            }

            //check if the editCell is a nasted component of the selectedCell
            int[] selectedCell = getSelectedCell(table);
            int[] selectedCellBounds = getCellBounds(selectedCell[0], selectedCell[1]);

            Rect editRect = ComUtils.convertRect(editCell.CurrentBoundingRectangle);
            Rect selectedRect = new Rect(selectedCellBounds[0], selectedCellBounds[1],
                selectedCellBounds[2], selectedCellBounds[3]);

            if (selectedRect.Contains(editRect))
            {
                //must be enabled, keyboardfocusable and own the keyboardfocus to be a current selected editable cell
                if (editCell != null &&
                    Convert.ToBoolean(editCell.CurrentIsEnabled) &&
                    Convert.ToBoolean(editCell.CurrentIsKeyboardFocusable) &&
                     Convert.ToBoolean(editCell.CurrentHasKeyboardFocus) &&
                    editCell.CurrentControlType == UiaControlTypeIds.UIA_EditControlTypeId)
                {
                    return true;
                }
                return false;
            }
            return false;
        }


         public bool isCellEditable(int row, int col)
         {
             IUIAutomationElement table = this.getElement();
             //find the edit cell, if null, it means there is no editable cell in the table open
             IUIAutomationElement editCell = table.FindFirst(TreeScope.TreeScope_Descendants,
                 ComUtils.AUTOMATION.CreatePropertyCondition(
                     UiaPropertyIds.UIA_ControlTypePropertyId,
                     UiaControlTypeIds.UIA_EditControlTypeId));

             if (editCell == null)
             {
                 return false;
             }

             //get bounds to check if the editor is inside the cell            
             int[] selectedCellBounds = getCellBounds(row, col);
             Rect selectedRect = new Rect(selectedCellBounds[0], selectedCellBounds[1],
                 selectedCellBounds[2], selectedCellBounds[3]);
             Rect editRect = ComUtils.convertRect(editCell.CurrentBoundingRectangle);

             if (selectedRect.Contains(editRect))
             {
                 //must be enabled, keyboardfocusable and own the keyboardfocus to be a current selected editable cell
                 if (editCell != null &&
                     Convert.ToBoolean(editCell.CurrentIsEnabled) &&
                     Convert.ToBoolean(editCell.CurrentIsKeyboardFocusable) &&
                      Convert.ToBoolean(editCell.CurrentHasKeyboardFocus) &&
                     editCell.CurrentControlType == UiaControlTypeIds.UIA_EditControlTypeId)
                 {
                     return true;
                 }
                 return false;
             }
             return false;
         }

         /// <summary>
         /// Check if the cell at the current mouse position is editable.
         /// </summary>
         /// <returns>the cell editability</returns>
         public override bool isCellAtCursorPositionEditable()
         {
             return base.isCellAtCursorPositionEditable(UiaControlTypeIds.UIA_DataGridControlTypeId);
         }             

    }    
}
