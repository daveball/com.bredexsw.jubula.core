﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ListWPFImplClass : ListImplClass
    {
        /// <summary>
        /// Array with all listItem Labels, filled while checking the selected items
        /// </summary>
        private static List<String> listItemLabels = new List<string>();

        public string[] getListItemLabels()
        {
            List<string> labels = new List<string>();
            actionAllowed();

            IUIAutomationScrollPattern pattern =
                 element.GetCurrentPattern(
                     UiaPatternIds.UIA_ScrollPatternId)
                         as IUIAutomationScrollPattern;
            element.SetFocus();
            SendKeys.SendWait(InputConstants.VK_STRG_POS1);

            if (pattern.CurrentHorizontalViewSize == 100
                && pattern.CurrentVerticalViewSize == 100)
            {
                IUIAutomationElementArray currentListItems = getListItems();
                for (int i = 0; i < currentListItems.Length; i++)
                {
                    labels.Add(currentListItems.GetElement(i).CurrentName);
                }
            }
            else
            {
                while (true)
                {
                    if (pattern.CurrentVerticalScrollPercent == 100)
                    {
                        labels.Add(getFocusListItem(getListItems()).CurrentName);
                        break;
                    }
                    else
                    {
                        labels.Add(getFocusListItem(getListItems()).CurrentName);
                        SendKeys.SendWait(InputConstants.VK_STRG_DOWN);
                        Thread.Sleep(30);
                    }
                }
            }
            return labels.ToArray();
        }

        public string[] getListItemLabelsAfterSelectionCheck()
        {
            return listItemLabels.ToArray();
        }

        public int[] getListItemBounds(int index)
        {
            actionAllowed();
            element.SetFocus();
            bool extended = false;
            if (Control.ModifierKeys != 0)
            {
                SendKeys.SendWait(InputConstants.VK_POS1);
                extended = true;
            }
            else
            {
                SendKeys.SendWait(InputConstants.VK_STRG_POS1);
            }

            for (int i = 0; i < index; i++)
            {
                SendKeys.SendWait(InputConstants.VK_STRG_DOWN);
            }
            Thread.Sleep(30);
            IUIAutomationElement item = getFocusListItem(getListItems());

            if (item == null)
            {                
                for (int i = 0; i < 100; i++)
                {
                    Thread.Sleep(10);
                    item = getFocusListItem(getListItems());
                    if (item != null)
                    {                        
                        break;
                    }              
                }
            }

            if (extended)
            {
                WinRobotImpl.pressButton(InputConstants.KEYFLAG_CONTROL);
            }
            return getBounds(item);
        }

        public int[] getListSelectedIndices() {

            List<int> indices = new List<int>();
            listItemLabels.Clear();
            actionAllowed();

            IUIAutomationScrollPattern Scrollpattern =
                 element.GetCurrentPattern(
                     UiaPatternIds.UIA_ScrollPatternId)
                         as IUIAutomationScrollPattern;
            element.SetFocus();
            SendKeys.SendWait(InputConstants.VK_STRG_POS1);


            if (Scrollpattern.CurrentHorizontalViewSize == 100
                && Scrollpattern.CurrentVerticalViewSize == 100)
            {
                IUIAutomationElementArray currentListItems = getListItems();
                for (int i = 0; i < currentListItems.Length; i++)
                {
                    IUIAutomationElement item = currentListItems.GetElement(i);
                    listItemLabels.Add(item.CurrentName);
                    if (isListItemSelected(item))
                    {
                        indices.Add(i);
                    }
                }
            }
            else
            {
                    int currentIndex = 0;
                    while (true)
                    {
                        if (Scrollpattern.CurrentVerticalScrollPercent == 100)
                        {
                            IUIAutomationElement item = getFocusListItem(getListItems());
                            listItemLabels.Add(item.CurrentName);
                            if (isListItemSelected(item))
                            {
                                indices.Add(currentIndex);
                            }              
                            break;
                        }
                        else
                        {
                            IUIAutomationElement item = getFocusListItem(getListItems());

                            if (item == null)
                            {                               
                                for (int i = 0; i < 100; i++)
                                {
                                    Thread.Sleep(10);
                                    item = getFocusListItem(getListItems());
                                    if (item != null)
                                    {                                       
                                        break;
                                    }
                                }
                            }

                            listItemLabels.Add(item.CurrentName);
                            if (isListItemSelected(item))
                            {
                                indices.Add(currentIndex);
                            }             
                            SendKeys.SendWait(InputConstants.VK_STRG_DOWN);
                            Thread.Sleep(30);
                        }
                        currentIndex++;
                    }
                }   
             return indices.ToArray(); 
            }

        /// <summary>
        /// Checks if the List is enabled and thefore the action can be performed.
        /// </summary>
        private void actionAllowed()
        {
            if (!checkEnablement())
            {
                throw new ServerException("Action can't be performed on disabled WPF-List",
                ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
        }        
    }
}
