﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ListWinFormImplClass : ListImplClass
    {
        public string[] getListItemLabels()
        {
            List<string> labels = new List<string>();

            IUIAutomationElementArray listItems = getListItems();

            for (int i = 0; i < listItems.Length; i++)
            {
                labels.Add(listItems.GetElement(i).CurrentName);
            }

            return labels.ToArray();
        }

        public string[] getListItemLabelsAfterSelectionCheck()
        {
            return getListItemLabels();
        }

        public int[] getListItemBounds(int index)
        {
            IUIAutomationElement item = getListItems().GetElement(index);

            IUIAutomationScrollPattern scrollPattern =
                element.GetCurrentPattern(UiaPatternIds.UIA_ScrollPatternId)
                as IUIAutomationScrollPattern;

            if (scrollPattern != null)
            {
                tagRECT listBounds = element.CurrentBoundingRectangle;
                WinRobotImpl.scrollIntoView(
                    new VerticalScrollPatternScroller(scrollPattern),
                    item, listBounds.top, listBounds.bottom, true);
            }
            return getBounds(item);
        }

        public int[] getListSelectedIndices()
        {
            List<int> indices = new List<int>();

            IUIAutomationCacheRequest cacheRequest =
                ComUtils.AUTOMATION.CreateCacheRequest();
                cacheRequest.AddPattern(UiaPatternIds.UIA_SelectionItemPatternId);
                cacheRequest.AddProperty(
                    UiaPropertyIds.UIA_SelectionItemIsSelectedPropertyId);
                cacheRequest.AutomationElementMode =
                    AutomationElementMode.AutomationElementMode_None;
                IUIAutomationElementArray listItems =
                    getListItemsBuildCache(element, cacheRequest);

                for (int i = 0; i < listItems.Length; i++)
                {
                    IUIAutomationSelectionItemPattern pattern =
                        listItems.GetElement(i).GetCachedPattern(
                            UiaPatternIds.UIA_SelectionItemPatternId)
                                as IUIAutomationSelectionItemPattern;

                    if (Convert.ToBoolean(pattern.CachedIsSelected))
                    {
                        indices.Add(i);
                    }
                }
                return indices.ToArray(); 
         }

        private IUIAutomationElementArray getListItemsBuildCache(
            IUIAutomationElement list, IUIAutomationCacheRequest cacheRequest)
        {
            return list.FindAllBuildCache(
                TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_ListItemControlTypeId),
                cacheRequest);
        }
    }
}
