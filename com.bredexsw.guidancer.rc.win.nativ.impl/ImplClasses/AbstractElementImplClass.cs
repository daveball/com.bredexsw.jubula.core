﻿using Interop.UIAutomationCore;
using System;
using System.Windows;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class AbstractElementImplClass
    {
        /// <summary>
        /// the automation element which represents the current impl class
        /// </summary>
        public static IUIAutomationElement element;

        /// <summary>
        /// constant to check if the automation element is created with wpf
        /// </summary>
        private const string WPF_FRAMEWORK = "WPF";

        public void setElement(String locator)
        {
            element = AUTHierarchyRobotImpl.findComponent(locator);
        }

        public void setElement(IUIAutomationElement component)
        {
            element = component;
        }

        public IUIAutomationElement getElement()
        {            
            return element;
        }

        /// <summary>
        /// Gets the name of a automationElement
        /// </summary>
        /// <param name="locator">the locator</param>
        /// <returns>the name</returns>
        public string getName()
        {
            return element.CurrentName;
        }

        /// <summary>
        /// Checks if the IUIAutomationElement is enabled
        /// </summary>
        /// <param name="locator">the locator</param>
        /// <returns>the enablement status</returns>
        public bool checkEnablement()
        {
            return checkEnablement(element);
        }

        public static bool checkEnablement(IUIAutomationElement component)
        {
            return Convert.ToBoolean(component.CurrentIsEnabled);
        }

        /// <summary>
        /// Check if the component has the focus
        /// </summary>
        /// <param name="ae">The component</param>
        /// <returns>The check result</returns>
        public bool hasFocus()
        {
            return Convert.ToBoolean(element.CurrentHasKeyboardFocus)
                || isDescendantOf(ComUtils.AUTOMATION.GetFocusedElement(), element);
        }

        /// <summary>
        /// Checks whether one Automation Element is a descendant of another
        /// Automation Element in the automation tree.
        /// </summary>
        /// <param name="element">the element to check</param>
        /// <param name="potentialAncestor">
        /// the potential ancestor element to check
        /// </param>
        /// <returns>
        /// true if element is a descendant of potentialAncestor. 
        /// Otherwise, false.
        /// </returns>
        protected bool isDescendantOf(IUIAutomationElement element,
            IUIAutomationElement potentialAncestor)
        {
            IUIAutomationElement parent = element;
            while (parent != null)
            {
                if (ComUtils.isEquals(parent, potentialAncestor))
                {
                    return true;
                }
                parent =
                    ComUtils.AUTOMATION.RawViewWalker.GetParentElement(parent);
            }

            return false;
        }

        /// <summary>
        /// Get the size and position of the component
        /// </summary>

        /// <returns>the bounds</returns>
        public int[] getBounds()
        {
            return getBounds(element);
        }

        public static int[] getBounds(IUIAutomationElement component)
        {
            WinRobotImpl.scrollToVisible(component);
            int[] boundValues = new int[4];
            Rect clippedBounds = ComUtils.getClippedBoundsRect(component);

            // position
            boundValues[0] = (int)clippedBounds.X;
            boundValues[1] = (int)clippedBounds.Y;
            // size
            boundValues[2] = (int)clippedBounds.Width;
            boundValues[3] = (int)clippedBounds.Height;

            return boundValues;
        }

        /// <summary>
        /// Check if the given element is a part of the wpf framework
        /// </summary>
        /// <param name="element">the element to check</param>
        /// <returns>is the element is a part of the wpf framework</returns>
        protected static bool isWPFElement(IUIAutomationElement element)
        {
            if (element != null && element.CurrentFrameworkId.Equals(WPF_FRAMEWORK))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
