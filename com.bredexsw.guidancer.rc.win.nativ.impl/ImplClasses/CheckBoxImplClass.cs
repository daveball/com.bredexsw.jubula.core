﻿using Interop.UIAutomationCore;
using System;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class CheckBoxImplClass : AbstractButtonImplClass
    {
        /// <summary>
        /// Check if a menuItem or a checkBox is selected
        /// </summary>
        /// <returns>the check result</returns>
        [Obsolete]
        public bool checkCheckBoxSelection()
        {
            IUIAutomationCondition conditionOn =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ToggleToggleStatePropertyId,
                    ToggleState.ToggleState_On);
            return AutomationConditionMatcher.matches(element, conditionOn);
        }

        /// <summary>
        /// Check if a menuItem or a checkBox is selected
        /// </summary>
        /// <returns>the check result</returns>
        public bool checkButtonSelection(){
            IUIAutomationCondition conditionOn =
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ToggleToggleStatePropertyId,
                    ToggleState.ToggleState_On);
            return AutomationConditionMatcher.matches(element, conditionOn);
        }
    }
}
