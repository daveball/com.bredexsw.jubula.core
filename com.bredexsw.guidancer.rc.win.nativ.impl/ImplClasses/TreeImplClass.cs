﻿using Interop.UIAutomationCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class TreeImplClass : AbstractElementImplClass
    {
        /// <summary>
        /// The current treeItem of the aut, neeed for name path selection
        /// </summary>
        private static IUIAutomationElement currentItem;

        private delegate IUIAutomationElement TreeMoveDelegate(
            IUIAutomationElement treeItem, IUIAutomationTreeWalker moveWalker);

        private static readonly Dictionary<string, TreeMoveDelegate> TREE_MOVE_DICT = new Dictionary<string, TreeMoveDelegate>()
        {
            {"up", new TreeMoveDelegate(MoveUp)},
            {"down", new TreeMoveDelegate(MoveDown)},
            {"next", new TreeMoveDelegate(MoveNext)},
            {"previous", new TreeMoveDelegate(MovePrevious)}
        };

        private static IUIAutomationElement MoveUp(
            IUIAutomationElement treeItem, IUIAutomationTreeWalker moveWalker)
        {
            return moveWalker.GetParentElement(treeItem);
        }

        private static IUIAutomationElement MoveDown(
            IUIAutomationElement treeItem, IUIAutomationTreeWalker moveWalker)
        {
            IUIAutomationExpandCollapsePattern expandPattern =
                treeItem.GetCurrentPattern(UiaPatternIds.UIA_ExpandCollapsePatternId)
                    as IUIAutomationExpandCollapsePattern;

            if (expandPattern != null)
            {
                expandPattern.Expand();
            }

            return moveWalker.GetFirstChildElement(treeItem);
        }

        private static IUIAutomationElement MoveNext(
            IUIAutomationElement treeItem, IUIAutomationTreeWalker moveWalker)
        {
            return moveWalker.GetNextSiblingElement(treeItem);
        }

        private static IUIAutomationElement MovePrevious(
            IUIAutomationElement treeItem, IUIAutomationTreeWalker moveWalker)
        {
            return moveWalker.GetPreviousSiblingElement(treeItem);
        }

        public string getTextOfTreeItemAtCursorPosition()
        {
            IUIAutomationElement itemAtCursorPosition =
                getTreeItemAtMouseCursor(this.getElement());

            if (itemAtCursorPosition == null)
            {
                throw new ServerException("No tree node found at mouse position.");
            }

            return itemAtCursorPosition.CurrentName;
        }

        /// <summary>
        /// </summary>
        /// <param name="tree">
        /// The tree element from which to acquire the element under the mouse
        /// cursor.
        /// </param>
        /// <returns>
        /// an Automation Element representing the item (belonging to the given
        /// tree) currently under the mouse cursor, or null if the mouse cursor
        /// is not positioned directly over an item within the given tree.
        /// </returns>
        private IUIAutomationElement getTreeItemAtMouseCursor(
            IUIAutomationElement tree)
        {
            tagPOINT cursorLocation = new tagPOINT();
            cursorLocation.x = Cursor.Position.X;
            cursorLocation.y = Cursor.Position.Y;
            IUIAutomationElement elementAtCursorLocation =
                ComUtils.AUTOMATION.ElementFromPoint(cursorLocation);
            if (UiaControlTypeIds.UIA_TreeItemControlTypeId.Equals(
                    elementAtCursorLocation.CurrentControlType)
                && isDescendantOf(elementAtCursorLocation, tree))
            {
                return elementAtCursorLocation;
            }

            return null;
        }

        /// <summary>
        /// Gets the text of one of the selected tree nodes
        /// </summary>
        /// <returns></returns>
        public string getTextOfSelectedTreeItem()
        {
            IUIAutomationElementArray selectedItems = getSelectedTreeItems(this.getElement());

            if (selectedItems == null)
            {
                throw new ServerException("No tree node selected.");
            }
            IUIAutomationElement firstItem = selectedItems.GetElement(0);
            return firstItem.CurrentName;
        }

        /// <summary>
        /// Gets the text of one of the selected tree nodes
        /// </summary>
        /// <returns></returns>
        public string[] getTextOfSelectedTreeItems()
        {
            IUIAutomationElementArray selectedItems = getSelectedTreeItems(this.getElement());

            if (selectedItems == null)
            {
                throw new ServerException("No tree node selected.");
            }
            String[] selectedItemsText = new String[selectedItems.Length];
            for (int i = 0; i < selectedItems.Length; i++)
            {
                selectedItemsText[i] = selectedItems.GetElement(i).CurrentName;
            }
            return selectedItemsText;
        }

        /// <summary>
        /// </summary>
        /// <param name="tree">
        /// The tree element from which to acquire the selection.
        /// </param>
        /// <returns>
        /// an Automation Element representing the first selected item in the
        /// given tree, or null if no items are selected in the given tree.
        /// </returns>
        private IUIAutomationElementArray getSelectedTreeItems(
            IUIAutomationElement tree)
        {
            IUIAutomationSelectionPattern pattern =
                tree.GetCurrentPattern(UiaPatternIds.UIA_SelectionPatternId)
                    as IUIAutomationSelectionPattern;
            IUIAutomationElementArray selection = pattern.GetCurrentSelection();

            if (selection.Length > 0)
            {
                return selection;
            }

            return null;
        }

        /// <summary>
        /// Initials the tree view.
        /// </summary>
        public void initTreeMenu(bool absolute, int preAscend)
        {
            IUIAutomationElement tree = this.getElement();
            IUIAutomationElementArray treeItems;
            IUIAutomationCondition treeItemCond = ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_TreeItemControlTypeId);
            currentItem = null;
            if (absolute)
            {
                currentItem = tree;
            }
            else
            {
                treeItems = tree.FindAll(TreeScope.TreeScope_Descendants, treeItemCond);
                IUIAutomationElement relativNode = getSelectedTreeNodeFromTree(tree);

                if (preAscend > 0)
                {
                    for (int i = 0; i < preAscend; i++)
                    {
                        relativNode = ComUtils.AUTOMATION.RawViewWalker.GetParentElement(relativNode);
                        if (relativNode == null || relativNode.CurrentControlType
                            != UiaControlTypeIds.UIA_TreeItemControlTypeId)
                        {
                            throw new ServerException("The tree node cannot be found.",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                        }
                    }
                }
                currentItem = relativNode;
            }
        }

        /// <summary>
        /// Get the current selected tree item from the given tree
        /// </summary>
        /// <param name="tree">the tree</param>
        /// <returns>the selected node</returns>
        private IUIAutomationElement getSelectedTreeNodeFromTree(IUIAutomationElement tree)
        {
            IUIAutomationElementArray treeItems;
            IUIAutomationCondition treeItemCond = ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_TreeItemControlTypeId);
            treeItems = tree.FindAll(TreeScope.TreeScope_Descendants, treeItemCond);

            IUIAutomationElement relativNode = null;
            for (int i = 0; i < treeItems.Length; i++)
            {
                if (treeItems.GetElement(i).CurrentHasKeyboardFocus == 1)
                {
                    relativNode = treeItems.GetElement(i);
                    break;
                }
            }

            if (relativNode == null)
            {
                throw new ServerException("No selected TreeItem found.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }

            return relativNode;
        }

        /// <summary>
        /// Gets all treeItems entrys from a given treeItem.
        /// </summary>        
        /// <returns>All child treeItems</returns>
        public string[] getCurrentTreeItems()
        {
            List<string> itemNames = new List<string>();
            IUIAutomationElementArray treeItems = currentItem.FindAll(
                TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_TreeItemControlTypeId));

            for (int i = 0; i < treeItems.Length; i++)
            {
                itemNames.Add(treeItems.GetElement(i).CurrentName);
            }

            return itemNames.ToArray();
        }

        /// <summary>
        /// Select a single tree item
        /// </summary>
        /// <param name="name">the name of the tree item</param>
        /// <param name="clickCount">click count</param>
        /// <param name="button">the button to click</param>
        /// <param name="collapse">Whether the node should be collapsed</param>
        public bool selectTreeItemByName(string name, int clickCount,
            int button, bool collapse)
        {
            try
            {
                currentItem = currentItem.FindFirst(
                    TreeScope.TreeScope_Children,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_NamePropertyId, name));

                if (currentItem == null)
                {
                    throw new ServerException("TreeItem does not exist.",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }

                if (ComUtils.isOffscreen(currentItem))
                {
                    scrollToTreeItem(currentItem);
                }

                IUIAutomationExpandCollapsePattern expandPattern =
                    (IUIAutomationExpandCollapsePattern)currentItem.GetCurrentPattern(
                        UiaPatternIds.UIA_ExpandCollapsePatternId);
                ExpandCollapseState nodeState = expandPattern.CurrentExpandCollapseState;

                if (collapse)
                {
                    expandPattern.Collapse();
                    moveMouseToTreeItem(currentItem);                    
                }
                else if (!nodeState.Equals(ExpandCollapseState.ExpandCollapseState_LeafNode))
                {
                    expandPattern.Expand();
                    moveMouseToTreeItem(currentItem);
                }
                else
                {
                    moveMouseToTreeItem(currentItem);
                    WinRobotImpl.click(clickCount, button);
                }

            }
            catch (COMException ce)
            {
                if (ComUtils.isExceptionOfType(ce, ComUtils.UIA_E_ELEMENTNOTENABLED))
                {
                    throw new ServerException("TreeItem is disabled.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }

                throw ce;
            }
            return false;
        }

        /// <summary>
        /// Scroll to a treeItem
        /// </summary>
        /// <param name="treeItem">the tree item</param>
        private void scrollToTreeItem(IUIAutomationElement treeItem)
        {
            IUIAutomationScrollItemPattern scrollPattern =
                        (IUIAutomationScrollItemPattern)treeItem.GetCurrentPattern(
                        UiaPatternIds.UIA_ScrollItemPatternId);
            scrollPattern.ScrollIntoView();
        }

        public void moveInTree(string direction,
                int distance, int clickCount)
        {
            IUIAutomationTreeWalker moveWalker = ComUtils.AUTOMATION.CreateTreeWalker(
                ComUtils.AUTOMATION.CreatePropertyCondition(
                    UiaPropertyIds.UIA_ControlTypePropertyId,
                    UiaControlTypeIds.UIA_TreeItemControlTypeId));

            TreeMoveDelegate moveOperation;
            if (TREE_MOVE_DICT.TryGetValue(direction.ToLower(), out moveOperation))
            {

                IUIAutomationElementArray items = getSelectedTreeItems(this.getElement());
                if (items == null)
                {
                    throw new ServerException("No selected TreeItem found.",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
                IUIAutomationElement item = items.GetElement(0);
                for (int i = 0; i < distance && item != null; i++)
                {
                    item = moveOperation.Invoke(item, moveWalker);
                }

                if (item == null)
                {
                    throw new ServerException("TreeItem does not exist.",
                            ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }

                if (ComUtils.isOffscreen(item))
                {
                    scrollToTreeItem(item);
                }

                WinRobotImpl.moveMouseToElement(item);
                WinRobotImpl.click(clickCount, 1);
            }
            else
            {
                throw new ServerException("Direction not supported: " + direction,
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }

        }

        /// <summary>
        /// Select a tree item with the index path
        /// </summary>
        /// <param name="ae">the tree</param>
        /// <param name="path">the index path</param>
        /// <param name="absolute">is the path absolute</param>
        /// <param name="preAscend">pre ascend value for relative path</param>
        /// <param name="clickCount">click count</param>
        /// <param name="button">click button</param>
        /// <param name="collapse">if the last node should be collapsed</param>
        public void selectTreeItemByIndexpath(object[] path,
            bool absolute, int preAscend, int clickCount, int button, bool collapse)
        {
            int[] indexPath = path.Cast<int>().ToArray();
            initTreeMenu(absolute, preAscend);
            try
            {
                IUIAutomationExpandCollapsePattern expandPattern =
                    currentItem.GetCurrentPattern(UiaPatternIds.UIA_ExpandCollapsePatternId)
                        as IUIAutomationExpandCollapsePattern;
                if (expandPattern != null)
                {
                    ExpandCollapseState nodeState = expandPattern.CurrentExpandCollapseState;

                    if (!nodeState.Equals(ExpandCollapseState.ExpandCollapseState_LeafNode))
                    {
                        expandPattern.Expand();
                    }
                }

                IUIAutomationElementArray treeItems;

                treeItems = currentItem.FindAll(TreeScope.TreeScope_Children,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_TreeItemControlTypeId));

                treeItemIndexSelect(treeItems, 0, indexPath, clickCount, button, collapse);
            }
            catch (IndexOutOfRangeException)
            {
                throw new ServerException("TreeItem does not exist.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
            catch (COMException ce)
            {
                if (ComUtils.isExceptionOfType(ce, ComUtils.UIA_E_ELEMENTNOTENABLED))
                {
                    throw new ServerException("TreeItem is disabled.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }
                throw ce;
            }
        }

        /// <summary>
        /// Searches in the given menuItem Collection for the given index and selects or expands the element.
        /// </summary>
        /// <param name="menuItems">collection of menuItems</param>
        /// <param name="index">the current index position in the path array</param>
        /// <param name="path">the path array filled with menuItem indexes</param>
        /// <param name="button">the mouse button</param>
        /// <param name="clickCount">click count</param>
        /// <param name="treeItems">the tree items</param>
        /// <param name="collapse">if the last node should be collapsed</param>
        private void treeItemIndexSelect(IUIAutomationElementArray treeItems, int index, int[] path,
            int clickCount, int button, bool collapse)
        {
            if (index < path.Count() && treeItems.Length > 0)
            {
                IUIAutomationElement treeItem;
                try
                {
                    treeItem = treeItems.GetElement(path[index] - 1);
                }
                catch (Exception)
                {
                    throw new ServerException("TreeItem does not exist.",
                        ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
                }

                if (ComUtils.isOffscreen(treeItem))
                {
                    scrollToTreeItem(treeItem);
                }

                IUIAutomationExpandCollapsePattern expandPattern =
                    (IUIAutomationExpandCollapsePattern)treeItem.GetCurrentPattern(
                        UiaPatternIds.UIA_ExpandCollapsePatternId);
                ExpandCollapseState nodeState = expandPattern.CurrentExpandCollapseState;

                if (index == path.Length - 1 && collapse)
                {
                    expandPattern.Collapse();                    
                    moveMouseToTreeItem(treeItem);
                }
                else if (!nodeState.Equals(ExpandCollapseState.ExpandCollapseState_LeafNode))
                {
                    expandPattern.Expand();                    
                    moveMouseToTreeItem(treeItem);
                }
                else
                {                    
                    moveMouseToTreeItem(treeItem);
                    WinRobotImpl.click(clickCount, button);
                }

                IUIAutomationElementArray items = treeItem.FindAll(
                        TreeScope.TreeScope_Children,
                        ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_ControlTypePropertyId,
                            UiaControlTypeIds.UIA_TreeItemControlTypeId));

                treeItemIndexSelect(items, ++index, path, clickCount, button, collapse);
            }
            else if (index < path.Count() && treeItems.Length == 0)
            {
                //path indexes left but no menu items left to select
                throw new ServerException("TreeItem does not exist.",
                    ServerExceptionConstants.STEP_EXECUTION_EXCEPTION);
            }
        }

        /// <summary>
        /// Moves the mouse to the tree item.         
        /// </summary>
        /// <param name="treeItem">The tree item</param>
        private void moveMouseToTreeItem(IUIAutomationElement treeItem) 
        {
            /// In some cases it's possible that the tree item contains a own text element, if yes we have to move the mouse to the center
            /// of the text element and not of the tree item.
            IUIAutomationElement textChild = ComUtils.AUTOMATION.RawViewWalker.GetLastChildElement(treeItem);
           
            if (textChild != null && textChild.CurrentControlType.Equals(UiaControlTypeIds.UIA_TextControlTypeId))
            {
                Rect rec = ComUtils.getClippedBoundsRect(textChild);                
                WinRobotImpl.moveMouseToPos((int) rec.X, (int) rec.Y);
            }
            else
            {
                WinRobotImpl.moveMouseToElement(treeItem);
            }            
        }

    }
}
