﻿using System;
using System.Collections.Generic;
using Interop.UIAutomationCore;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    class ComboBoxImplClass : ComponentWithTextInputImplClass
    {
        public string[] getComboBoxItemLabels()
        {
            List<string> labels = new List<string>();

            IUIAutomationCacheRequest cacheRequest =
                ComUtils.AUTOMATION.CreateCacheRequest();
            cacheRequest.AddProperty(UiaPropertyIds.UIA_NamePropertyId);
            cacheRequest.AutomationElementMode =
                AutomationElementMode.AutomationElementMode_None;

            ensureComboBoxDropDownListIsOpen(element);
            try
            {
                IUIAutomationElementArray items =
                    getComboBoxItemsBuildCache(element, cacheRequest);

                for (int i = 0; i < items.Length; i++)
                {
                    labels.Add(items.GetElement(i).CachedName);
                }
            }
            finally
            {
                ensureComboBoxDropDownListIsClosed(element);
            }

            return labels.ToArray();
        }
        
        /// <returns>true if the Combo Box's text field is editable. Otherwise, false.</returns>
        [Obsolete]
        public bool isComboBoxEditable()
        {
            return isEditable();
        }

        /// <returns>true if the Combo Box's text field is editable. Otherwise, false.</returns>
        public bool isEditable()
        {
            // Only Combo Box fields of control type "edit" can be editable. Otherwise, the field 
            // will be of type "text".
            IUIAutomationTreeWalker editorFieldWalker = ComUtils.AUTOMATION.CreateTreeWalker(
                ComUtils.AUTOMATION.CreateAndCondition(
                    ComUtils.AUTOMATION.RawViewCondition,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_EditControlTypeId)));
            IUIAutomationElement editorField = editorFieldWalker.GetFirstChildElement(element);
            if (editorField != null)
            {
                return Convert.ToBoolean(editorField.CurrentIsEnabled);
            }

            return false;
        }


        /// <summary>
        /// Selects the item at the given index within the given combo box's 
        /// dropdown list. This method opens the combo box's dropdown list 
        /// if necessary. It also closes the dropdown list if it is still open 
        /// after the operation is finished.
        /// </summary>        
        /// <param name="index">the index of the item to select</param>
        public void selectComboBoxItem(int index)
        {
            ensureComboBoxDropDownListIsOpen(element);
            try
            {
                IUIAutomationElement item =
                    getComboBoxItemsBuildCache(
                        element, ComUtils.AUTOMATION.CreateCacheRequest())
                    .GetElement(index);
                WinRobotImpl.scrollItemIntoView(item);
                WinRobotImpl.moveMouseToElement(item);
                WinRobotImpl.clickLeftMouseKey(1);
            }
            finally
            {
                ensureComboBoxDropDownListIsClosed(element);
            }
        }

        private void ensureComboBoxDropDownListIsOpen(
            IUIAutomationElement comboBox)
        {
            if (!isComboBoxDropDownListVisible(comboBox))
            {
                toggleComboBoxDropDownList(comboBox);
            }
        }

        /// <summary>
        /// Finds the dropdown list for the given combo box. This method does
        /// *not* make any attempt to reveal or dismiss the dropdown list.
        /// </summary>
        /// <param name="comboBox">the combo box on which to operate</param>
        /// <param name="itemListOperation">the operation to perform</param>
        /// <returns>the dropdown list automation element for the given combo 
        ///          box, or null if no such list is currently 
        ///          available</returns>
        private bool isComboBoxDropDownListVisible(
            IUIAutomationElement comboBox)
        {
            IUIAutomationTreeWalker rawListWalker =
                ComUtils.AUTOMATION.CreateTreeWalker(
                    ComUtils.AUTOMATION.CreateAndCondition(
                        ComUtils.AUTOMATION.RawViewCondition,
                        ComUtils.AUTOMATION.CreatePropertyCondition(
                            UiaPropertyIds.UIA_ControlTypePropertyId,
                            UiaControlTypeIds.UIA_ListItemControlTypeId)));
            IUIAutomationElement dropdownList =
                rawListWalker.GetFirstChildElement(comboBox);
            return dropdownList != null
                && !ComUtils.isOffscreen(dropdownList);
        }

        private IUIAutomationElementArray getComboBoxItemsBuildCache(
            IUIAutomationElement comboBox, IUIAutomationCacheRequest cacheRequest)
        {
            return comboBox.FindAllBuildCache(
                    TreeScope.TreeScope_Descendants,
                    ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_ListItemControlTypeId),
                    cacheRequest);
        }

        private void ensureComboBoxDropDownListIsClosed(
            IUIAutomationElement comboBox)
        {
            if (isComboBoxDropDownListVisible(comboBox))
            {
                toggleComboBoxDropDownList(comboBox);
            }
        }

        private void toggleComboBoxDropDownList(IUIAutomationElement comboBox)
        {
            IUIAutomationElement toggleButton = getComboBoxArrowButton(comboBox);
            WinRobotImpl.moveMouseToElement(toggleButton);
            WinRobotImpl.clickLeftMouseKey(1);
        }

        private IUIAutomationElement getComboBoxArrowButton(
            IUIAutomationElement comboBox)
        {
            return comboBox.FindFirst(TreeScope.TreeScope_Children,
                ComUtils.AUTOMATION.CreatePropertyCondition(
                        UiaPropertyIds.UIA_ControlTypePropertyId,
                        UiaControlTypeIds.UIA_ButtonControlTypeId));
        }
    }
}
