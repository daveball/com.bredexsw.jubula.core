﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// The AutomationConditionMatcher checks if the given AutomationElement
    /// matches the given conditions.
    /// </summary>
    class AutomationConditionMatcher
    {

        /// <summary>
        /// Checks if the IUIAutomationElement matches the condition.
        /// </summary>
        /// <param name="ele">
        /// The IUIAutomationElement to check.
        /// </param>
        /// /// <param name="condition">
        /// The condition.
        /// </param>
        public static bool matches(
            IUIAutomationElement ele, IUIAutomationCondition condition)
        {
            // TreeWalker.Normalize(AutomationElement) was used here, but was 
            // causing errors when used with UIA's COM interface. 
            return ele.FindFirst(TreeScope.TreeScope_Element, condition) != null;
        }

        /// <summary>
        /// Checks if an ancestor of the IUIAutomationElement matches the condition.
        /// </summary>
        /// <param name="ele">
        /// The IUIAutomationElement to check.
        /// </param>
        /// /// <param name="condition">
        /// The condition.
        /// </param>
        public static IUIAutomationElement findMatchingAncestor(
            IUIAutomationElement ele, IUIAutomationCondition condition)
        {
            IUIAutomationElement parent = ele;
            while (parent != null)
            {
                IUIAutomationElement match = parent.FindFirst(TreeScope.TreeScope_Element, condition);
                if (match != null)
                {
                    return match;
                }
                parent =
                    ComUtils.AUTOMATION.RawViewWalker.GetParentElement(parent);
            }

            return null;
        }
    }
}
