﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Class that represents all information that we send to the .java server over a automationElement
    /// </summary>
    class AUTComponentInformations
    {
        /// <summary>
        /// the name of the component
        /// </summary>
        public string m_name { get; set; }
        /// <summary>
        /// the authierarchy locator of the component
        /// </summary>
        public string m_locatorPath { get; set; }
        /// <summary>
        /// the control type of the component
        /// </summary>
        public string m_control { get; set; }
        /// <summary>
        /// the framework which is used for this component
        /// </summary>
        public string m_frameworkID { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">the name</param>
        /// <param name="path">the locator</param>
        /// <param name="controlType">the control type</param>
        public AUTComponentInformations(string name, string path, string controlType, string frameworkID)
        {
            this.m_name = name;
            this.m_locatorPath = path;
            this.m_control = controlType;
            this.m_frameworkID = (frameworkID == "") && WinRobotImpl.win8App ? "InternetExplorer" : frameworkID;
        }        
    }
}
