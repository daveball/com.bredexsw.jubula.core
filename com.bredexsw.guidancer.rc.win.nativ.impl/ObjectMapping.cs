﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Interop.UIAutomationCore;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using log4net;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Class that starts / ends the OM and performs other OM actions.
    /// </summary>
    class ObjectMapping
    {
        /// <summary>
        /// The logger for logging errors and infos.
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ObjectMapping));
        /// <value>
        /// The Mouse / Keyboard Listener for the OM-Mode
        /// </value>
        private static MappingListener m_mappingListener;
        /// <value>
        /// Condition with the process id from the AUT
        /// </value>
        private static IUIAutomationCondition m_idCondition;
        /// <value>
        /// Condition with all supported typed from GD
        /// </value>
        private static IUIAutomationCondition m_supportedCondition;
        /// <value>
        /// List of selected IUIAutomationElement from the OM-Mode
        /// </value>
        private static List<IUIAutomationElement> m_selectedElements = new List<IUIAutomationElement>();
        /// <value>
        /// True, if object mapping is currently running, otherwise False.
        /// </value>
        private static bool m_isMapping = false;
        /// <value>
        /// True, if OM is in touch mode, otherwise False.
        /// </value>
        private static bool m_isTouchOMM = false;
        /// <summary>The expected key event for collecting components while in object mapping mode.</summary>
        private static KeyEventArgs mappingKeyEventArgs;
        /// <summary>The expected mouse event for collecting components while in object mapping mode.</summary>
        private static MouseEventArgs mappingMouseEventArgs;
        /// <returns>True, if object mapping is currently running, otherwise False.</returns>
        public static bool isMapping()
        {
            return m_isMapping;
        }
        /// <returns>True, if OM is in touch mode, otherwise False.</returns>
        public static bool isTouchOMM()
        {
            return m_isTouchOMM;
        }
        /// <returns>The expected key event for collecting components while in object mapping mode.</returns>
        public static KeyEventArgs getMappingKeyEventArgs()
        {
            return mappingKeyEventArgs;
        }
        /// <returns>The expected mouse event for collecting components while in object mapping mode.</returns>
        public static MouseEventArgs getMappingMouseEventArgs()
        {
            return mappingMouseEventArgs;
        }

        /// <summary>
        /// Starts the OMM. It is started in touch object mapping mode, if the key code specifies a mouse button (left, middle, right).
        /// In this case, the modifier is ignored. Called by reflection.
        /// </summary>
        /// <param name="keyModifierString">the modifier string that is used to map</param>
        /// <param name="keyCodeString">the key that is used to map</param>
        public static void startMapping(string keyModifierString, string keyCodeString)
        {
            mappingKeyEventArgs = JavaInputConverter.getWinKeyEventArgs(keyModifierString, keyCodeString);
            mappingMouseEventArgs = JavaInputConverter.getWinMouseEventArgs(keyCodeString);
            m_isTouchOMM = (mappingMouseEventArgs.Button != MouseButtons.None);
            LOG.InfoFormat("isTouchOMM={0}", m_isTouchOMM);
            m_idCondition = ComUtils.AUTOMATION.CreatePropertyCondition(
                UiaPropertyIds.UIA_ProcessIdPropertyId, WinRobotImpl.getProcessId());
            m_mappingListener = new MappingListener(m_idCondition, m_supportedCondition);
            m_mappingListener.startListener();
            m_isMapping = true;
        }

        /// <summary>
        /// Gets a list of supported components names and converts them to
        /// PropertyConditions.
        /// </summary>
        public void setAUTListOfSupportedComponents(object[] components)
        {
            IUIAutomationCondition[] typeProperties =
                new IUIAutomationCondition[components.Length];
            for (int i = 0; i < components.Length; i++)
            {
                typeProperties[i] = ComUtils.AUTOMATION.CreatePropertyCondition(UiaPropertyIds.UIA_ControlTypePropertyId,
                    ControlTypeUtils.stringToControlType(components[i].ToString()));
            }
            //at least two propertys needed to created the orCondition
            m_supportedCondition = 
                ComUtils.AUTOMATION.CreateOrConditionFromArray(typeProperties);
        }

        /// <summary>
        /// Highlights a Component. Called by reflection.
        /// </summary>
        /// <param name="locator">the locator of the component</param>
        /// <returns>return if the component could be highlighted</returns>
        public static bool highlightComponent(string locator)
        {
            LOG.InfoFormat("highLightComponent {0}", locator);
            bool rtn = false;
            IUIAutomationElement ae = AUTHierarchyRobotImpl.findComponent(locator);
            
            if (ae != null)
            {
                WinRobotImpl robot = new WinRobotImpl();
                robot.setAUTFocus();
                rtn = true;
                System.Drawing.Rectangle rec = new System.Drawing.Rectangle();
                System.Windows.Rect bounds = ComUtils.convertRect(ae.CurrentBoundingRectangle);
                    
                rec.X = (int)bounds.X;
                rec.Y = (int)bounds.Y;
                rec.Height = (int)bounds.Height;
                rec.Width = (int)bounds.Width;
                Application.Run(new InvisibleForm(rec, 1000));
            }
            return rtn;
        }

        /// <summary>
        /// Stops the OM. Called by reflection.
        /// </summary>
        public static void stopMapping()
        {
            m_mappingListener.stopListener();
            m_selectedElements.Clear();
            m_isMapping = false;
        }

        /// <summary>
        /// Stops the OM and closed the selection window.
        /// </summary>
        public static void closeMapping()
        {
            InvisibleForm.isAUTClosed = true;
            m_mappingListener.stopListener();
            m_selectedElements.Clear();
            m_isMapping = false;
        }

        /// <summary>
        /// Returns a list of selected AutomationElements which where gathered in the OM-Mode. Called by reflection.
        /// </summary>
        /// <returns>
        /// A string array with all automationID values from the selected elements
        /// </returns>
        public static string[] getSelectedElements()
        {
            try
            {
                List<IUIAutomationElement> tmplist = m_selectedElements.Distinct().ToList();
                string[] tmparr = new string[tmplist.Count];

                for (int i = 0; i < tmplist.Count; i++)
                {
                    tmparr[i] = AUTHierarchyRobotImpl.findLocator(tmplist[i]);                    
                }

                m_selectedElements.Clear();
                
                return tmparr;                
            }
            catch (COMException)
            {
                return null;
            }
            
        }

        /// <summary>
        /// Adds a IUIAutomationElement to the selected list.
        /// </summary>
        /// <param name="ae">
        /// The IUIAutomationElement that should be added to the list.
        /// </param> 
        public static void addSelectedElement(IUIAutomationElement ae)
        {
            m_selectedElements.Add(ae);
        }
    }
}
