﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;


namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Coordinates the mouseover-, shortcut-listeners and tab listeners
    /// for the OM-Mode in keyboard and in touch mode.
    /// </summary>
    class MappingListener
    {        
        /// <summary>
        /// Thread that listens where the mouse is
        /// </summary>
        private Thread m_mouseListenerThread;
        /// <summary>
        /// Thread that opens the invisible form and listens for the user shortcuts
        /// </summary>
        private Thread m_shortCutListenerThread;
        /// <summary>
        /// Thread that opens the highlighting invisible form in touchOMM
        /// </summary>
        private Thread m_highlightMappingThread;
        /// <summary>
        /// Thread that starts an invisible Form for touchOMM
        /// </summary>
        private Thread m_tabListenerThread;
        /// <summary>
        /// Condition for a collectable, mappable Control.
        /// </summary>
        private IUIAutomationCondition m_condition;
        /// <summary>
        /// The selected AutomationElement under the mouse.
        /// Required for single and doubleTab, so the singleTab
        /// performs the selection of the current element.
        /// </summary>
        private IUIAutomationElement m_selectedElement;
        /// <summary>
        /// The invisible form for the touch OMM
        /// </summary>
        private static InvisibleFormTouchMode m_touchForm;
        /// <summary>
        /// The supported IUIAutomationElement that is under the mouse
        /// </summary>
        public static IUIAutomationElement m_overElement;
        
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="idConditon">
        /// IUIAutomationCondition that is used to get only the elements from the same AUT process
        /// </param> 
        /// <param name="supportedCondition">
        /// OrCondition which contains a list of types which are supported and will be gatherd
        /// </param> 
        public MappingListener(IUIAutomationCondition idConditon, IUIAutomationCondition supportedCondition)
        {
            m_condition = ComUtils.AUTOMATION.CreateAndCondition(
                idConditon, supportedCondition);
        }

        /// <summary>
        /// Start the Mappinglisteners
        /// </summary>
        public void startListener()
        {
            if (!ObjectMapping.isTouchOMM())
            {
                m_mouseListenerThread = new Thread(new ThreadStart(this.startMouseOverListener));
                m_shortCutListenerThread = new Thread(new ThreadStart(this.startShortCutListener));
                m_mouseListenerThread.Start();
            }
            else
            {
                m_tabListenerThread = new Thread(new ThreadStart(this.startTabListener));
                m_tabListenerThread.Start();
            }
        }

        /// <summary>
        /// Start the tab listener in touch OMM.
        /// </summary>
        private void startTabListener()
        {
            m_touchForm = new InvisibleFormTouchMode(m_condition);
            m_touchForm.MouseClick += InvisibleFormTouchMode_MouseClick;
            m_touchForm.MouseDoubleClick += InvisibleFormTouchMode_MouseDoubleClick;
            Application.Run(m_touchForm);
        }

        /// <summary>
        /// Doubleclick delegate collects the parent hierarchy of the selected element,
        /// that is set in the singleclick delegate.
        /// </summary>
        /// <param name="sender">sender obj</param>
        /// <param name="e">The event</param>
        private void InvisibleFormTouchMode_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            collectParentHierarchy(m_selectedElement);
        }

        /// <summary>
        ///Ends the mapping listeners
        /// </summary>        
        public void stopListener()
        {
            if (!ObjectMapping.isTouchOMM())
            {
                m_mouseListenerThread.Abort();
                m_shortCutListenerThread.Abort();
            }
            else
            {
                m_touchForm.delegateClose();
                m_tabListenerThread.Abort();
            }
        }

        /// <summary>
        /// Click delegate of the touchListening form. Get the element and highlight
        /// the selection. On Rightclick / longtab collect and highlight all elements.
        /// Leftclick is also triggered on doubleclick so it also performes the selection
        /// of the AutomationElement.
        /// </summary>
        /// <param name="sender">sender obj</param>
        /// <param name="e">the event</param>
        private void InvisibleFormTouchMode_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    m_selectedElement = getUIAEleFromPoint(e);
                    getSelectedElement(m_selectedElement);
                    break;
                case MouseButtons.Right:
                    collectAllVisibleUIAE();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Get an IUIAutomationElement from a point.
        /// </summary>
        /// <param name="e">Mouseposition event</param>
        /// <returns>The IUIAutomationElement unter the position</returns>
        private IUIAutomationElement getUIAEleFromPoint(MouseEventArgs e)
        {
            tagPOINT point = new tagPOINT();
            point.x = e.X + m_touchForm.DesktopBounds.X;
            point.y = e.Y + m_touchForm.DesktopBounds.Y;
            // hide the listening form, grap the underlying element
            // and make it topmost and visible it again
            m_touchForm.TopMost = false;
            m_touchForm.Visible = false;
            IUIAutomationElement element =
                    ComUtils.AUTOMATION.ElementFromPoint(point);
            m_touchForm.TopMost = true;
            m_touchForm.Visible = true;
            return element;
        }

        /// <summary>
        /// Collect all IUIAutomationElements that match the condition.
        /// </summary>
        private void collectAllVisibleUIAE()
        {
            IUIAutomationElement desktop = ComUtils.AUTOMATION.GetRootElement();
            IUIAutomationElementArray allElememts = desktop.FindAll(
                TreeScope.TreeScope_Descendants, m_condition);
            for (int i = 0; i < allElememts.Length; i++)
            {
                getSelectedElement(allElememts.GetElement(i));
            }
        }

        /// <summary>
        /// Collect the element's parent hierarchy.
        /// </summary>
        /// <param name="element">The selected element</param>
        private void collectParentHierarchy(IUIAutomationElement element)
        {
            IUIAutomationElement parent = element;
            while (parent != null)
            {
                if (AutomationConditionMatcher.matches(parent, m_condition))
                {
                    getSelectedElement(parent);
                }
                parent =
                    ComUtils.AUTOMATION.RawViewWalker.GetParentElement(parent);
            }
        }

        /// <summary>
        /// Check if the found AutomationElement is usable, decides between
        /// tabs, comboboxes etc and set the correct one.
        /// </summary>
        /// <param name="element"></param>
        private void getSelectedElement(IUIAutomationElement element)
        {
            try
            {
                if (!ComUtils.isEquals(element, m_overElement))
                {
                    IUIAutomationElement match =
                        AutomationConditionMatcher.findMatchingAncestor(element, m_condition);

                    if (match != null)
                    {
                        if (isElementTab(match))
                        {
                            if (isTabInRange(match))
                            {
                                m_overElement = match;
                                startHighlighter();
                            }
                        }
                        else if (isComboBoxElement(match))
                        {
                            m_overElement = ComUtils.AUTOMATION.RawViewWalker.GetParentElement(match);
                            startHighlighter();
                        }
                        else
                        {
                            m_overElement = match;
                            startHighlighter();
                        }
                    }
                }
            }
            catch (COMException)
            {

            }
        }

        /// <summary>
        /// Start the highlight (mouseover highlight or onTab highlight) 
        /// depending on the selected Object Mapping Mode.
        /// </summary>
        private void startHighlighter()
        {
            if (!ObjectMapping.isTouchOMM())
            {
                startShortCutListener();
            }
            else
            {
                ObjectMapping.addSelectedElement(m_overElement);
                m_highlightMappingThread = new Thread(new ThreadStart(this.paintHighlight));
                m_highlightMappingThread.Start();
            }
        }

        /// <summary>
        /// Highlight the tabbed element
        /// </summary>
        private void paintHighlight()
        {
            Rect rec = ComUtils.getClippedBoundsRect(m_overElement);
            InvisibleFormTouchMode tm = new InvisibleFormTouchMode(new Rectangle(
                (int)rec.X, (int)rec.Y, (int)rec.Width, (int)rec.Height), 1000);
            Application.Run(tm);
        }


        /// <summary>
        ///Listen where the mouse is and checks if a automation 
        ///element is found under the position
        /// </summary>        
        private void startMouseOverListener()
        {            
            while (true)
            {
                try
                {
                    tagPOINT point = new tagPOINT();
                    point.x = Cursor.Position.X;
                    point.y = Cursor.Position.Y;
                    IUIAutomationElement element = 
                        ComUtils.AUTOMATION.ElementFromPoint(point);
                    getSelectedElement(element);
                    Thread.Sleep(200);
                }
                catch (COMException)
                {
                    
                }
            }            
        }

        public static bool isElementTab(IUIAutomationElement element)
        {
            try
            {
                return element.CurrentControlType == UiaControlTypeIds.UIA_TabControlTypeId;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the tab component is still in our border range
        /// </summary>
        /// <param name="element">the tab component</param>
        /// <returns>if its inside of our green border range</returns>
        public static bool isTabInRange(IUIAutomationElement element)
        {
            System.Windows.Point point = new System.Windows.Point(
                Cursor.Position.X, Cursor.Position.Y);
            Rect bounds = ComUtils.getClippedBoundsRect(element);

            if (bounds.Contains(point))
            {
                bounds.Inflate(-20, -20);
                if (!bounds.Contains(point))
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public static bool isComboBoxElement(IUIAutomationElement element) {
            return ComUtils.AUTOMATION.RawViewWalker.GetParentElement(element).CurrentControlType
                == UiaControlTypeIds.UIA_ComboBoxControlTypeId;            
        }

        /// <summary>
        /// Starts the form that listen to the user shortcut to collect the elements
        /// and to draw the green rectangle.
        /// </summary>        
        private void startShortCutListener()
        {
            System.Drawing.Rectangle rec = new System.Drawing.Rectangle();            
            System.Windows.Rect bounds =
                ComUtils.getClippedBoundsRect(m_overElement);
            rec.X = (int)bounds.X;
            rec.Y = (int)bounds.Y;
            rec.Height = (int)bounds.Height;
            rec.Width = (int)bounds.Width;
           
            //opens the green border window
            Application.Run(new InvisibleForm(rec, m_overElement));
        }
                        
    }
}
