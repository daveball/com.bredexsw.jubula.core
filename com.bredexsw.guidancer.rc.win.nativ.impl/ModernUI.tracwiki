== Things to know about Windows 8 ==
 * Detailed and up to date [http://blog.frogslayer.com/tag/automated-testing/ blog post] about required certificates, signing, the ui flag, remote debugging.
There exist two versions of Windows 8, the ''normal'' Windows 8 and Windows RT. The most importent thing about the normal Windows 8 Version is, that there are two different desktops. The normal one for Win-applications and the new ''Modern UI'' Desktop. The Modern UI desktop is made for ''Windows Store Apps'' that run in fullscreen-mode or in the splitted-mode, where two apps or one app and the plain old desktop can be simultaniously active. When a Windows Store App is minimized / not visible on the screen, it is suspended and not visible for inspection tools anymore. That behavior occurs on most mobile operating systems to save energy, so goes Windows 8. Windows 8 can run all ''old'' Windows applications and all Modern UI apps on x86 CPUs.

Windows RT is a special version of Windows 8 for ARM CPUs, made for touch and hybrid devices. Apart from some Microsoft applications like Office, it is not possible to make normal Win32 applications run on Windows RT. So Windows RT is a limited Windows 8 version, that can only install / run Windows Store Apps. That's why we can't inject touch events and can't use the UI Automation Framework on Windows RT, because our .NET Server is a Windows console application.

=== Required environment ===
In order to build a Windows Store App there are some necessary tools

 * Build target .NET 4.5
 * Visual Studio 2012 or higher, some new code samples from Microsoft only run on VS 2013 with Windows 8.1
 * Windows 8 (not RT) or higher, some new code samples from Microsoft only run on VS 2013 with Windows 8.1
 * A developer licence is required to build and sign Windows Store Apps. We can avoid that when the specific setting is enabled in the Group Policy Editor
 * Optional: Blend GUI editor for Windows Store Apps

== Facts about the Windows Store Apps ==
 * Windows Store Apps run in the ''App Container'', like a sandbox on iOS. Because of that, we can't access a Windows Store App from another.
 * When the running app is sent to the background, it does not exist (for inspection tools / UIA) anymore, it is completely suspended and its state is stored.
 * When the plain old desktop is active in fullscreen mode, the Modern UI desktop and all apps that are running on it are also not visible and suspended.
 * The development goes like normal .NET applications. In addition there is the GUI editor called ''Blend''.

== Automating Windows Store Apps ==
In order to automate Modern UI apps, there are some general things to know: The UI Automation Framework (UIA) allows us to automate all elements, that implement the automation interface. We can't automate a Modern UI app from another Modern UI app, because we don't have access to it (from one sandbox into another). But we can access a Modern UI app from a normal C#-application when we keep some things in mind: In order to gain access to a Modern UI app, we need to modify the manifest file with the tag <uiaccess=true>, then the .NET-Server has to be signed with a code signing certificate that must be trusted by the system. The application's target is .NET 4.

=== Rights and the manifest ===
There are some security mechanisms that we have to understand to gain access to the UI of a Modern UI app. A normal .NET application (WPF, !WinForms, Win32, ..) can be seen by the IUA immediately. But to see Modern UI apps, we have to create a manifest-file (right click on project >> add >> new item >> application manifest file) and toggle the ui-access flag in the manifest file of the .NET automation client. More detailed: Inside of the app.mainfest.xml, the security tags must be inside of the assembly tag. Here is the xml block that has to be added to our UIA application:

{{{
<trustInfo xmlns="urn:schemas-microsoft-com:asm.v2">
    <security>
        <requestedPrivileges xmlns="urn:schemas-microsoft-com:asm.v3">
            <requestedExecutionLevel level="asInvoker" uiAccess="true" />
        </requestedPrivileges>
    </security>
</trustInfo>
}}}
After that's done, we are not able to launch our UIA application anymore because of missing rights, but we are still able to built it, when we started Visual Studio as administrator. After building our application, we have to sign the .exe file which is in the output /bin folder. For more information how to create a trusted certificate and the signing commands please see [#Howtocreateatrustedcertificate How to create a trusted certificate] and [#Howtosignyourapplication How to sign your application]. Administration rights: We need to move/copy the .exe file of our automation client to a file path, where only the administrator has write access (e.g. "C:\Program Files\").

**Important:** Only when all the steps are successfully completed, our .NET application is able to see Modern UI apps. When the ui-access flag is set to true, we are not able to launch or debug the application from Visual Studio or via .exe anymore.

=== Remote Debugging with VS2013 ===
The remote machine needs a running VS2013 remote monitor and the executable. Unfortunately we can't debug an executable that has the uiaccess=true flag set in the manifest. But that is only used for object mapping.
To ensure that remote debugging is possible on every build version, we should deliver the pdb (=project debug database) file as an artefact. The pdb file contains the source code and symbols that are necassary to debug trough the code of an application. It is also possible to open the .exe file (with the pdb file in the same folder) as a solution in Visual Studio. If we select preferences from the context menu in the solution explorer, we can select how to debug this executable (select remotedebugging, etc).
[http://msdn.microsoft.com/en-us/library/y7f5zaaa.aspx Here] are some tutorials from msdn that explain the different steps.

Steps for the remote machine:
 1. Install the VS2013 remote monitor (once)
 1. Remove or rename the .manifest file that is in the .NET-Server bundle
 1. Run the VS2013 remote monitor
 1. Start testing / start GD / start the AUT Agent,...

Steps for the debugger machine:
 1. Start VS2013
 1. Click on Debug > Attach to Process and select the remote PC from the dropdown menu
 1. When the .NET-Server is running (by starting the AUT from GD), we can attach our debugger to that process

We can build a release version and a debug version. Currently we only use release versions.

=== Signing and verifying ===
In order to get the the automation client to run, we have to create a trusted windows certificate and sign our app with it. Trusting the certificate must be done only once per machine where the UIA application runs. How to do that is descriped in [#Howtocreateatrustedcertificate How to create a trusted certificate] and in [#Howtosignyourapplication How to sign your application]. Only when the application is trusted and launched from an administration path like ''C:\Program Files\'' it can see Modern UI applications with the UIA.

=== Events ===
Events in .NET are bound to the class where they can be fired. That's why the UIA and any other application cannot fire events for another application. We'd need to have the object from the class, where the event occurs and that is not possible.

=== Gestures ===
In Windows 8 there two ways to inject user-touchs or gestures.

 * Using C++ there is a header file where we can inject touch events (Sample code from MS exists).
 * Using C# with the User32.dll

In general, we can only inject a touch down and a touch up event. To perform a multi-touch-gesture, we have to touch down with one more coordinates, then move the individual positions step by step like the gesture and finally fire the two touch up events.

  I've ported the C++ sample to C#, because it is more comfortable to use C# in that case. The User32.dll provides two useful methods for us: !InitializeTouchInjection (..) to initialize and !InjectTouchInput (..) to perform the action. Therefore there are some enums and structs required in C# to use that methods. The touch injector class is a .NET console application so it will not work on Windows RT. Source code of [https://cgi-int.bredex.de/bredex/trac/p21306/attachment/wiki/ModernUI/TouchInjector.cs TouchInjector.cs] .

=== Object mapping ===
We use an invisible window with a painted border to perform object mapping. The window is above the UI compont and listens for the mapping keys. In the touch object mapping mode, there is a 99% invisible form over the complete screen. It is technically 1% visible because then we can detect click events in that window. The click event is consumed from our fullscreen window, so in touch object mapping mode the user cannot interact with the AUT.
The invisible windows always have to be the topmost window in both object mapping modes. To achieve that the property ''TopMost'' must be set to true. That requires the uiaccess flag set to true in the manifest.

 * the correct UI access permission is set,
 * the application is signed and trusted, and
 * it is being executed in a path, where only the system administrator has write access (= the same procedure as the UIA client application).

=== Compile / Embed (interop) dll files into the executable file ===
We want to compile the dll file into the exe file with VS2013, because then we just have one executable. If we don't embed the interop types, external referenced librarys / dlls have to be in the same folder as the executable, else the application will crash. It seems to be easy but there are some things to notice.
In Visual Studio, follow up these steps:
 * Expand References from the Solution explorer and open your referenced file you want to embed in the Properties View.
 * Change "Embed Interop Types" from False to True.
That seem to be the steps but then the VS Solution is not compilable anymore. We have to make sure, that we _never_ create an instance from any class of the embedded interop, only use public COM interfaces else the error "Interop type 'Class' cannot be embedded. Use the applicable interface instead." on compile time. That occurs because no executable code is embedded, also all constants get lost while embedding the interop file. For details look at the walkthrough at [http://msdn.microsoft.com/en-us/library/dd409610.aspx msdn].
Like in the MS !UiAutomation Framework we need the lost constants (ControlTypeIDs, !PropertyIds, PatternIDs). We can copy the required constants into out project and reference them instead of the missing ones from the embedded interop dll. That is version independent due to the comment on a bug from the MS Developer Misha Shneerson: [http://connect.microsoft.com/VisualStudio/feedback/details/508465/interop-type-onenote-application-cannot-be-embedded >>Source]
> For these you will need manually embed the values of the constant into your project - - since those are constant fields in the abstract classes and will never change - this is a safe thing to do.

=== !AppBar and !CharmBar ===

__AppBar__
 * Technically there are two kinds of app bars, the !CommandBar and the !AppBar [http://msdn.microsoft.com/en-us/library/windows/apps/hh781232.aspx#choose_an_appbar_or_commandbar -->msdn]. 
 * Both app bars are XAML components and can be on the bottom, top or bottom and top of the screen. 
 * They are context-free in their page, e.g. on right click of a button would open the bar and a context menu (if it exists) with focus on the context menu. 
 * The app developer should create one app bar per application and on page change the content may be swapped. App bars can be opened with a right click, with Windows+Z keys or with a swipe (in touch mode). 
 * !CommandBar
   * A simple bar that can contain only few components (like buttons, toggle buttons). Has a default layout / style
 * !AppBar
   * Can contain any XAML component. (Layouts, Popups, !ContextMenus, !ComboBoxes , Lists, ...) Has no default layout / style

Because !AppBars can contain all XAML components, The content of !AppBars should be mappable. The !CommandBar and the !AppBar look the same for the user. The "Select from ..." TC is only useful for !CommandBars that contain some buttons. In my opinion we should have a TC for "Open !AppBar" and then just map the components inside. Additionally a TC for a simple selection from a !AppBar.

__CharmBar__

 * Technically DirectUI and XAML.
 * The !CharmBar can be opened with the shortcut Windows+C. Windows+I opens the Settings !CharmBar directly.
 * The !CharmBar can contain Buttons, Labels, !ComboBoxes, !CheckBoxes, !TextInputs etc. It is also possible to navigate to other pages from the !CharmBar.

The only useful TC would imho be open !CharmBar and then support common !DirectUI components as GRC.

== Useful tools ==
All the tools are located in "C:\Program Files(x86)\Windows Kits\8.0\bin\x86\" inspect.exe, accevent.exe, makecert.exe and sign.exe.

 * The Inspector tool allows us to inspect every existing UI element on the screen (that is accessible with the UIA).
 * The !AccEvent tool allows us to see fired events (like click events).
 * The makecert tool allows us to create trusted Windows certificates.
 * The certmgr tool allows us to trust a already created windows certificate.
 * The signing tool allows us to sign and verify .exe files and bind it to a trusted certificate.

== FAQs ==
=== What are the requirements to build modern UI apps? ===
 * MS Visual Studio 2012 (or greater) and one of the following OS:
   * Windows 7 SP1 (x86 und x64)
   * Windows 8 (x86 und x64)
   * Windows Server 2008 R2 (x64)
   * Windows Server 2012 (x64)
 * See http://www.microsoft.com/visualstudio/deu/products/compatibility

=== How to package your application locally? ===
In Visual Studio:
 1. Use PROJECT->Store->Create App Packages...
 1. Select No for not uploading to the Windows Store
 1. Click Next
 1. Click Create
Projects can also be built wich ant / the .cmd file in each .NET project.

=== How to install your application locally? ===
 1. Package your application locally by using the steps above.
 1. If you are installing the application the first time, start powershell as administrator.
 1. If you packaged your application in a network folder, add execution rights with `Set-ExectionPolicy RemoteSigned`.
 1. Call the script `Add-AppDevPackage.ps1` located in a the first subfolder of your packaged application.
A signed .appx package can also be deployed from the powershell with the ''Add-AppxPackage'' command.
Hints:

 * With a developer license certificate, a user must reinstall the app every 30 days or a group policy must be set. See [http://stackoverflow.com/questions/13486860/can-i-distribute-a-windows-8-modern-ui-app-without-using-the-store here].
 * Enterprise Modern UI Apps can be installed only in a Windows Domain without using the Windows Store. See [http://superuser.com/questions/499340/install-a-windows-8-modern-ui-app-without-the-windows-store here].
 * With a store license certificate, an App can be uploaded to the Microsoft App Store.
 * [http://nsis.sourceforge.net/Docs/Modern%20UI/Readme.html NSIS] seam to have an installation method to install modern UI apps. (Not tested yet. Which installation method is used?)

=== How to build and install a modern UI app from command line with MS Visual Studio ===
 1. If the source code is located on a network drive, copy it on a local drive. Otherwise the installation fails on signing the application. Alternatively add a property to the .csproj file described in [#creatingamodernuiapp Creating a Modern UI App].
 1. Call MS Visual Studio with parameters like in the following example:
{{{
devenv.exe MySolutionFileName.sln /deploy "Release|x86" /project MyProjectFileName.csproj
}}}

=== How to create an in-officially trusted certificate? ===
 1. Locate the required [#Usefultools tools] and open a cmd terminal as administrator.
 1. create a certificate: we create an individual certificate. Params: the store name, the certificate name, output file.
{{{
makecert.exe -$individual -r -pe -ss "PrivateCertStore" -n CN="TestAppSign" "C:\Users\soeren\Desktop\testapp_certificate.cer"
}}}
 1. Let the system trust the created certificate. Params: path to the certificate.cer
{{{
certmgr.exe -add "C:\Users\soeren\Desktop\testapp_certificate.cer" -s -r localMachine root
}}}

=== How to sign your application? ===
 1. Install Windows SDK, if the signtool is not installed. See [#Usefultools tools].
 1. Sign your application with the trusted certificate. Params: the store name, the certificate name, the .exe to be signed
{{{
signtool.exe sign -v -s "PrivateCertStore" -n "TestAppSign" bin\Release\Automation.exe
}}}
 1. Verify your signed application.
{{{
signtool.exe verify -pa -v bin\Release\Automation.exe
}}}

Hints:

 * For creating an installer with signing the App see [http://www.eulanda.de/inside/entwicklerhandbuch/api/codesigning/Default.htm here] for an example.
 * See also [http://stackoverflow.com/questions/4739407/exporting-a-certificate-as-base-64-encoded-cer here] for extracting the public certificate from a pxf file.

=== How to start a modern UI app from normal desktop ===
 1. Calling the explorer with `%windir%\explorer.exe shell:::{4234d49b-0245-4df3-b780-3893943456e1} ` opens a folder with all installed modern UI apps.
   * [http://superuser.com/questions/478975/how-to-create-a-desktop-shortcut-to-a-windows-8-modern-ui-app]
   * [http://forums.mydigitallife.info/threads/32777-Launch-Metro-Apps-Directly-from-Desktop]
 1. Programmatically by using the App-ID or App-Name.

=== Creating a Modern UI App ===
 * See jubula_qs/org.eclipse.jubula.qa.aut.caa.dotnet.modernui/readme.wikitrac

=== How to install programms in CI with administrator rights? ===
The default setting since Windows VISTA is, that the UAC warning dialog is shown, if a program wants administrator rights. There exist two ways for a script to avoid popping up the UAC warning dialog:

 1. Disable the [http://www.eightforums.com/tutorials/5509-user-account-control-uac-change-settings-windows-8-a.html UAC warning dialog] by choosing the lowest level in windows settings.
 1. Let the UAC warning dialog enabled and use instead the [http://www.techsupportalert.com/content/how-create-program-shortcut-run-without-uac-prompt-windows-7.htm windows task scheduler] with checked option '''Run with higest privileges'''. Tasks can be created also by command line, but setting the highest level worked not for me with error "Access is denied".
{{{
: Remove the previously created task
SchTasks /delete /TN InstallITE /F
: Create a new task
SchTasks /create /SC ONCE /ST 00:00 /TN InstallITE /RU GDWIN8\bxtest /RL HIGHEST /TR "C:\hudson\test\installer-guidancer_win32-win32-x86.exe"
: Run the created task manually
SchTasks /run /TN InstallITE
}}}

=== The installation of the CaA AUT is broken (This app can't be opened - Dialog) ===
In ordern to install an .appx package we use the Sideload mechanism. That requires a valid Windows Developer license. Unfortunately a license has to be renewed every 30 days. To fix the broken installation, open a powershell with administrator rights (right >> run as admininstrator) and type the command:

{{{
: Open a propt to renew the developer license.
Show-WindowsDeveloperLicenseRegistration
: See details and how long the developer license is valid
Get-WindowsDeveloperLicense
}}}
Navigate through the pages and fill the email and password field of a Microsoft Developer account. After that, the app should start fine.

See

 * http://www.eightforums.com/tutorials/5509-user-account-control-uac-change-settings-windows-8-a.html Change UAC warning dialog settings on Windows 8.
 * http://www.techsupportalert.com/content/how-create-program-shortcut-run-without-uac-prompt-windows-7.htm Create a scheduled task with highest previleges on Windows 7.
 * http://www.geeksinphoenix.com/blog/post/2012/11/16/Using-Task-Scheduler-in-Windows-8.aspx Create a scheduled task on Windows 7.
 * http://www.pretentiousname.com/misc/win7_uac_whitelist2.html Other hacky ways to avoid UAC dialog warning.
