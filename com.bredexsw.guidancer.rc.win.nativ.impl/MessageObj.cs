﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// The message send by the java aut agent.
    /// </summary>
    class MessageObj
    {
        /// <summary>
        /// name of the class which will be invoked
        /// </summary>
        public string className { get; set; }
        /// <summary>
        /// name of the method which will be invoked
        /// </summary>
        public string methodName { get; set; }
        /// <summary>
        /// the ITE mode id, says which robot should be used
        /// </summary>
        public int mode { get; set; }
        /// <summary>
        /// list of parameters for the method
        /// </summary>
        public object[] parameters { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public MessageObj()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="className">the class name</param>
        /// <param name="methodName">the method name</param>
        /// <param name="parameters">the parameters</param>
        public MessageObj(string className, string methodName, object[] parameters)
        {
            this.className = className;
            this.methodName = methodName;
            this.parameters = parameters;
        }
    }

}
