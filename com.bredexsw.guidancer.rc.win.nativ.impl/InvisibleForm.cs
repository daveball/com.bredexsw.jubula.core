﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Interop.UIAutomationCore;
using log4net;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Form which will be created over a supported element in the OM-Mode.
    /// Creates a green rectangle.
    /// </summary>
    public partial class InvisibleForm : Form
    {
        /// <summary>
        /// The logger for logging errors and infos.
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(InvisibleForm));
        /// <value>
        /// the rectangle which contains the sized and postition 
        /// from the supported element that lays under the invisibleform
        /// </value>
        private System.Drawing.Rectangle m_rec;
        /// <value>
        /// Thread that listens if the mouse left the area from the invsible form
        /// </value>
        private Thread m_mouseListenerThread;
        /// <value>
        /// Thread that waits x time before he closes the form, needed to highlight component
        /// </value>
        private Thread m_timerThread;
        /// <value>
        /// delegate for thread safty closing
        /// </value>
        delegate void closeDelegate();
        /// <value>
        /// shows how long the window should be open, needed to highlight component
        /// </value>
        private static int time = 1000;

        public static bool isAUTClosed = false;

        private IUIAutomationElement m_element;

        /// <summary>
        /// Constructor.
        /// </summary>
        public InvisibleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor for the normal OM.
        /// </summary>
        /// <param name="rec">
        /// Rectangle which contains the sized and postition 
        /// from the supported element that lays under the invisibleform
        /// </param>
        public InvisibleForm(System.Drawing.Rectangle rec, IUIAutomationElement element)
        {
            isAUTClosed = false;
            m_element = element;
            this.m_rec = rec;
            this.TopMost = true;
            InitializeComponent();
            m_mouseListenerThread = new Thread(new ThreadStart(this.checkPos));
            m_mouseListenerThread.Start();            
        }
           
        /// <summary>
        /// Constructor for highlighting component.
        /// </summary>
        /// <param name="rec">position for the green rectangle</param>
        /// <param name="timer">how long the rectangle should be displayed</param>
        public InvisibleForm(System.Drawing.Rectangle rec, int timer)
        {
            isAUTClosed = false;
            time = timer;
            this.m_rec = rec;
            InitializeComponent();
            m_timerThread = new Thread(new ThreadStart(this.waitTime));
            m_timerThread.Start();
        }

        /// <summary>
        /// wait x time and then close the form
        /// </summary>
        private void waitTime()
        {
            Thread.Sleep(time);
            closeDelegate d1 = closeForm;
            try
            {
                this.Invoke(d1);
            }
            catch (InvalidOperationException)
            {

            }
        }

        private void delegateClose()
        {
            closeDelegate d1 = closeForm;
            try
            {
                this.Invoke(d1);
            }
            catch (InvalidOperationException)
            {

            }       
        }

        /// <summary>
        /// Check if the mouse is still inside the invisible form, if not then close
        /// the form.
        /// </summary>
        private void checkPos()
        {
            while (true)
            {
                if (isAUTClosed)
                {
                    delegateClose();                                 
                }

                System.Drawing.Point point = new System.Drawing.Point(Cursor.Position.X, Cursor.Position.Y);

                if (MappingListener.isElementTab(this.m_element))
                {
                    if (!MappingListener.isTabInRange(this.m_element))
                    {
                        delegateClose();                                              
                    }
                }                
                else if (!m_rec.Contains(point))
                {
                    delegateClose();                                 
                }
                Thread.Sleep(200);
            }
        }

        /// <summary>
        /// Key Pressed Event Handler, for the OM-Mapping.
        /// </summary>
        /// <param name="s">
        /// sender object
        /// </param>
        /// <param name="e">
        /// The key event containing the modifier and the key code.
        /// </param>
        public void onKeyPress(object s, KeyEventArgs e)
        {
            KeyEventArgs expected = ObjectMapping.getMappingKeyEventArgs();
            LOG.InfoFormat("key e={0} {1} | expected={2} {3}",
                    e.KeyCode, e.Modifiers, expected.KeyCode, expected.Modifiers);
            if (e.KeyCode == expected.KeyCode && e.Modifiers == expected.Modifiers)
            {
                ObjectMapping.addSelectedElement(MappingListener.m_overElement);
            }
        }

        /// <summary>
        /// draws the green rectangle as form border
        /// </summary>
        /// <param name="sender">the sender object</param>
        /// <param name="e">the paint event</param>
        private void Form_Paint(object sender, PaintEventArgs e)
        {            
            System.Drawing.Graphics graphicsObj;
            graphicsObj = this.CreateGraphics();
            Pen myPen = new Pen(System.Drawing.Color.Green, 7);
            graphicsObj.DrawRectangle(myPen, this.ClientRectangle);           
        }

        /// <summary>
        /// shown event where the invisible form gets focus and gets the bounds
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">the event</param>
        private void InvisibleForm_Shown(Object sender, EventArgs e)
        {
            this.Bounds = m_rec;
            IUIAutomationElement ae = 
                ComUtils.AUTOMATION.ElementFromHandle(this.Handle);
            ae.SetFocus();
        }
               
        /// <summary>
        /// Closes this form.
        /// </summary>
        private void closeForm()
        {
            while (this.Handle == IntPtr.Zero)
            {
                Thread.Sleep(100);
                this.Refresh();
            }
            this.Close();
        }       
    }
}
