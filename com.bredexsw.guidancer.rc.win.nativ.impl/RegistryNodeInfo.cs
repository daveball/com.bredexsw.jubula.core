﻿/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
using Microsoft.Win32;

namespace com.bredexsw.guidancer.rc.win.nativ.impl
{
    /// <summary>
    /// Registry node information.
    /// </summary>
    class RegistryNodeInfo
    {
        /// <summary>
        /// The registry key
        /// </summary>
        public RegistryKey RegistryKey { get; set; }
        /// <summary>
        /// The Application name
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// The registry launch id
        /// </summary>
        public string LaunchID { get; set; }
        /// <summary>
        /// The App pkg id to find a name - launchID pair
        /// </summary>
        public string RegNodeID { get; set; }

        /// <summary>
        /// Clone the obj in order to assign not just a reference (e.g. in a collection)
        /// </summary>
        /// <param name="input">the RegistryNodeInfo to clone</param>
        /// <returns>the cloned object</returns>
        public static RegistryNodeInfo Clone(RegistryNodeInfo input)
        {
            RegistryNodeInfo cloned = new RegistryNodeInfo();
            cloned.RegistryKey = input.RegistryKey;
            cloned.DisplayName = input.DisplayName;
            cloned.LaunchID = input.LaunchID;
            cloned.RegNodeID = input.RegNodeID;
            return cloned;
        }
    }
}
