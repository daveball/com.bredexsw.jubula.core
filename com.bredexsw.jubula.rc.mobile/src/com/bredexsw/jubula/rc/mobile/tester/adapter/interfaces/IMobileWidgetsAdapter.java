package com.bredexsw.jubula.rc.mobile.tester.adapter.interfaces;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IWidgetComponent;

/**
 * @author BREDEX GmbH
 */
public interface IMobileWidgetsAdapter extends IWidgetComponent {
    /**
     * Perform a swipe gesture on the current adapted widget
     * 
     * @param direction
     *            the direction in which to swipe
     */
    public void swipe(String direction);
}
