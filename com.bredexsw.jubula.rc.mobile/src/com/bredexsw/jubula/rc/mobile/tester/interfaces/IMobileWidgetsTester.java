package com.bredexsw.jubula.rc.mobile.tester.interfaces;

/**
 * @author BREDEX GmbH
 */
public interface IMobileWidgetsTester {
    /**
     * Perform a swipe gesture
     * 
     * @param direction
     *            the direction in which to swipe
     */
    public void rcSwipe(String direction);
}
