/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rp.view;

import org.eclipse.jubula.qa.aut.caa.swt.StartShell;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

/**
 * @author BREDEX GmbH
 * @created 08.03.2010
 */
public class AutSwtWrapperView extends ViewPart {
    /** view ID */
    public static final String ID = "org.eclipse.jubula.qa.aut.caa.rp.CaA_AUT_SWTWrapperView"; //$NON-NLS-1$

    /** storage for dialog content */
    private Composite m_content;

    /**
     * This is a callback that will allow us to create the viewer and initialize
     * it.
     * @param parent parent control
     */
    public void createPartControl(Composite parent) {
        m_content = StartShell.createContent(parent);
    }

    /**
     * Passing the focus request to the viewer's control.
     */
    public void setFocus() {
        m_content.setFocus();
    }
}