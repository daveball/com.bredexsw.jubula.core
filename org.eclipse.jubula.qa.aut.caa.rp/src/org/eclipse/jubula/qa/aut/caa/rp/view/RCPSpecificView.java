/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rp.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

/**
 * @author BREDEX GmbH
 * @created 08.03.2010
 */
public class RCPSpecificView extends ViewPart {

    /**
     * 
     */
    public RCPSpecificView() {
        //empty
    }

    /**
     * {@inheritDoc}
     */
    public void createPartControl(Composite parent) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText("2nd View"); //$NON-NLS-1$
    }

    /**
     * {@inheritDoc}
     */
    public void setFocus() {
        //empty
    }

}
