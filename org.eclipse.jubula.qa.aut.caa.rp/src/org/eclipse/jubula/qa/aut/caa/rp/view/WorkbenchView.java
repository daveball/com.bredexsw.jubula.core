/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rp.view;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.rp.utils.WorkbenchUtils;
import org.eclipse.jubula.qa.aut.caa.utils.CompUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.ViewPart;

/**
 * @author BREDEX GmbH
 */
public class WorkbenchView extends ViewPart {
    /**
     * @author BREDEX GmbH
     */
    private static class ProperAUTTerminator implements SelectionListener {
        /** {@inheritDoc} */
        public void widgetDefaultSelected(SelectionEvent arg0) {
            widgetSelected(arg0);
        }

        /** {@inheritDoc} */
        public void widgetSelected(SelectionEvent arg0) {
            WorkbenchUtils.executeCommand(ActionFactory.QUIT.getCommandId());
        }
    }

    /**
     * listener to terminate the AUT properly
     */
    private SelectionListener m_autTerminator = new ProperAUTTerminator();

    /** Constructor */
    public WorkbenchView() {
        // empty
    }

    /** {@inheritDoc} */
    public void createPartControl(Composite parent) {
        Composite contentComposite = new Composite(parent, SWT.NONE);
        contentComposite.setLayout(new FormLayout());

        // Exit Button
        Button exitBtn = new Button(contentComposite, SWT.PUSH);
        exitBtn.setText(I18NUtils.getString("exit"));
        CompUtils.setComponentName(exitBtn,
                ComponentNameConstants.TESTPAGE_APPLICATION_EXIT_BTN);
        exitBtn.addSelectionListener(m_autTerminator);
    }

    /** {@inheritDoc} */
    public void setFocus() {
        // empty
    }
}
