/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rp.perspective;

import org.eclipse.jubula.qa.aut.caa.rp.view.AutSwtWrapperView;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * @author BREDEX GmbH
 * @created 08.03.2010
 */
public class Perspective implements IPerspectiveFactory {
    /**
     * The ID
     */
    public static final String ID = "org.eclipse.jubula.qa.aut.caa.rp.perspective"; //$NON-NLS-1$

    /**
     * {@inheritDoc}
     */
    public void createInitialLayout(IPageLayout layout) {
        layout.setEditorAreaVisible(false);
        layout.setFixed(true);
        layout.addView(AutSwtWrapperView.ID, IPageLayout.LEFT, 0.5f, 
                layout.getEditorArea());
    }

}
