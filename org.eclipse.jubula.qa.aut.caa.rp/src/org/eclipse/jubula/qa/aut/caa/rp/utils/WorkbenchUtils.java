/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rp.utils;

import org.eclipse.core.commands.common.CommandException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;

/**
 * @author BREDEX GmbH
 */
public final class WorkbenchUtils {
    /** hide constructor */
    private WorkbenchUtils() {
        // hide
    }

    /**
     * Execute the given commmandId
     * 
     * @param commandID
     *            the command to execute
     * @return The return value from the execution; may be null.
     */
    public static Object executeCommand(String commandID) {
        IHandlerService handlerService = getHandlerService();
        try {
            return handlerService.executeCommand(commandID, null);
        } catch (CommandException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return the handler service
     */
    public static IHandlerService getHandlerService() {
        return (IHandlerService) PlatformUI.getWorkbench().getService(
                IHandlerService.class);
    }
}
