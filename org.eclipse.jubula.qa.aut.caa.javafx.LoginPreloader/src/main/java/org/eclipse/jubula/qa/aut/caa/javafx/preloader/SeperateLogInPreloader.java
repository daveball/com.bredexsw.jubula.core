/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.eclipse.jubula.qa.aut.caa.javafx.preloader;

import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Preloader with a login button. It will only start the Application if the
 * Login Button was pressed.
 * 
 * @author BREDEX GmbH
 */
public class SeperateLogInPreloader extends Preloader {

    /**
     * The Stage
     */
    private Stage m_stage;

    /**
     * The Application
     */
    private Confirm m_app;

    /**
     * Creates the Scene for this Preloader.
     * 
     * @return the Scene
     */
    private Scene createPreloaderScene() {
        TextField tf1 = new TextField();
        tf1.setPromptText("Username");
        PasswordField pf1 = new PasswordField();
        BorderPane p = new BorderPane();
        VBox loginBox = new VBox(tf1, pf1);
        p.setCenter(loginBox);
        Button login = new Button("Login");
        login.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                startApp();
            }
        });
        p.setBottom(login);
        return new Scene(p, 300, 150);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.m_stage = stage;
        stage.setScene(createPreloaderScene());
        stage.show();
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification scn) {
        if (scn.getType() == StateChangeNotification.Type.BEFORE_START) {
            m_app = (Confirm) scn.getApplication();
        }
    }

    /**
     * Signals the Application to start.
     */
    public void startApp() {
        Platform.runLater(new Runnable() {
            public void run() {
                m_stage.hide();
                m_app.confirm();
            }
        });
    }
}
