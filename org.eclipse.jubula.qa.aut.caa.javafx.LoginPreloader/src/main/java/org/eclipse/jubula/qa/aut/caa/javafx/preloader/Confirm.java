package org.eclipse.jubula.qa.aut.caa.javafx.preloader;

/**
 * 
 * @author BREDEX GmbH
 */
public interface Confirm {

    /**
     * Confirm if the button in the Preloader was pressed. This is used as a
     * shared interface between the preloader and the application
     */
    public void confirm();

}
