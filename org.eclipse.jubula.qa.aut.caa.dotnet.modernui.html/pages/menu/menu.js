﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/menu/menu.html", {
        ready: function (element, options) {
            document.querySelector(".titlearea").addEventListener("click", showHeaderMenu, false);
            document.getElementById("FirstItem").addEventListener("click", handleMenuSelection1, false);
            document.getElementById("More").addEventListener("click", handleMenuSelection2, false);

            WinJS.log && WinJS.log("Click or tap the title to show the header menu.", "sample", "status");
        }
    });

    // Place the menu under the title and aligned to the left of it
    function showHeaderMenu() {
        var title = document.querySelector("header .titlearea");
        var menu = document.getElementById("headerMenu").winControl;
        menu.anchor = title;
        menu.placement = "bottom";
        menu.alignment = "left";

        menu.show();
    }

    function handleMenuSelection1(event) {
        document.getElementById("MenuAction").innerText = "FirstItem";
    }

    function handleMenuSelection2(event) {
        document.getElementById("MenuAction").innerText = "More";
    }

})();
