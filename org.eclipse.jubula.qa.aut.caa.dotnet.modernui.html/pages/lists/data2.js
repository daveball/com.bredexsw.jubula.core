﻿(function () {
    "use strict";
    
    var dataArray = [];

    for (var i = 1; i <= 200; i++) {
        dataArray.push({ title: i});
    }

    var dataList = new WinJS.Binding.List(dataArray);

    // Create a namespace to make the data publicly
    // accessible. 
    var publicMembers =
        {
            itemList: dataList
        };
    WinJS.Namespace.define("DataExample2", publicMembers);
})();
