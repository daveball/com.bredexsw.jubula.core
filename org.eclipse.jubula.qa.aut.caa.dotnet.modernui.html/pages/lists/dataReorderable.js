﻿(function () {
    "use strict";

    var dataArray = [
        { title: ""},
        { title: "abcde"},
        { title: "aBcDe"},
        { title: "a B c D e"}, 
        { title: "a1b2c3"},
        { title: "0+*\./*+0"},
    ];

    var dataList = new WinJS.Binding.List(dataArray);

    // Create a namespace to make the data publicly
    // accessible. 
    var publicMembers =
        {
            itemList: dataList
        };
    WinJS.Namespace.define("DataExampleReorderable", publicMembers);
})();
