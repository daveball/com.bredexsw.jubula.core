﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/timePicker/timePicker.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {


            WinJS.UI.processAll();

            var timePicker1 = document.querySelector("#TimePicker1")
            var timePicker1_ = new WinJS.UI.TimePicker(timePicker1, { current: '17:19:00' });
            timePicker1_.clock = "24HourClock";

            var timePicker2 = document.querySelector("#TimePicker2")
            var timePicker2_ = new WinJS.UI.TimePicker(timePicker2, { current: '17:19:00' });
            timePicker2_.clock = "24HourClock";
            timePicker2_.disabled = true;

            var timePicker3 = document.querySelector("#TimePicker3")
            var timePicker3_ = new WinJS.UI.TimePicker(timePicker3, { current: '17:19:00' });
            timePicker3_.clock = "12HourClock";
            timePicker3_.addEventListener("contextmenu", this.contextMenuHandler, false);
            
            var timePicker4 = document.querySelector("#TimePicker4")
            var timePicker4_ = new WinJS.UI.TimePicker(timePicker4, { current: '17:19:00' });
            timePicker4_.clock = "12HourClock";
            timePicker4_.disabled = true;
            timePicker4_.addEventListener("contextmenu", this.contextMenuHandler, false);

            var datePicker1 = document.querySelector("#DatePicker1")
            var datePicker1_ = new WinJS.UI.DatePicker(datePicker1, { current: '5/23/2009' });
            datePicker1_.addEventListener("contextmenu", this.contextMenuHandler, false);

            var datePicker2 = document.querySelector("#DatePicker2")
            var datePicker2_ = new WinJS.UI.DatePicker(datePicker2, { current: '5/23/2009' });
            datePicker2_.disabled = true;
            datePicker2_.addEventListener("contextmenu", this.contextMenuHandler, false);
        },

        contextMenuHandler: function (eventInfo) {
            eventInfo.preventDefault();

            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("TimePickerAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("TimePickerAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("TimePickerAction").innerText = "Context menu dismissed";
                }
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
