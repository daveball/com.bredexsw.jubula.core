﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/checkBoxes/checkBoxes.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {


            // Retrieve the button and register our event handler. 
            var checkBox = document.querySelector("#CheckBox");
            checkBox.addEventListener("click", this.checkBoxClickHandler, false);
            checkBox.addEventListener("contextmenu", this.contextMenuHandler, false);
            var checkBox1 = document.querySelector("#CheckBox1");
            checkBox1.addEventListener("click", this.checkBoxClickHandler, false);
            checkBox1.addEventListener("contextmenu", this.contextMenuHandler, false);
        },

        checkBoxClickHandler: function (eventInfo) {
            var name = eventInfo.srcElement.id;
            document.getElementById("CheckBoxAction").innerText = name + " checked = " + eventInfo.srcElement.checked;
        },

        contextMenuHandler: function (eventInfo) {
            eventInfo.preventDefault();

            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("CheckBoxAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("CheckBoxAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("CheckBoxAction").innerText = "Context menu dismissed";
                }
            });
        },


        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
