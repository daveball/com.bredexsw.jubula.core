﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/comboBoxes/comboBoxes.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {


            var comboBox1 = document.querySelector("#ComboBox1");
            comboBox1.addEventListener("contextmenu", this.contextMenuHandler, false);

            var comboBox2 = document.querySelector("#ComboBox2");
            comboBox2.addEventListener("contextmenu", this.contextMenuHandler, false);
            
            var comboBox4 = document.querySelector("#ComboBox4");
            comboBox4.addEventListener("contextmenu", this.contextMenuHandler, false);
        },

        contextMenuHandler: function (eventInfo) {
            eventInfo.preventDefault();

            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("ComboAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("ComboAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("ComboAction").innerText = "Context menu dismissed";
                }
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
