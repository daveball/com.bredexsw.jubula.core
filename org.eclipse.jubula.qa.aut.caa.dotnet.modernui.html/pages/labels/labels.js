﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/labels/labels.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {

            // Retrieve the button and register our event handler. 
            var label = document.querySelector("#Label");
            label.addEventListener("contextmenu", this.contextMenuHandler, false);

            var label1 = document.querySelector("#Label1");
            label1.addEventListener("contextmenu", this.contextMenuHandler, false);
            label1.disabled = true;

            var label3= document.querySelector("#Label3");
            label3.addEventListener("click", this.labelClickHandler, false);
            label3.addEventListener("dblclick", this.labelDoubleClickHandler, false);
        },

        labelClickHandler: function (eventInfo) {
            eventInfo.srcElement.innerText = "1 Click";
        },

        labelDoubleClickHandler: function (eventInfo) {
            eventInfo.srcElement.innerText = "2 Clicks";
        },

        contextMenuHandler : function(eventInfo) {
            eventInfo.preventDefault();

            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("LabelAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("LabelAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("LabelAction").innerText = "Context menu dismissed";
                }
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
