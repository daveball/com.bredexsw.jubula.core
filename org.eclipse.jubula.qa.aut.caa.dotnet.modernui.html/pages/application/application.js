﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/application/application.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            
            // Retrieve the button and register our event handler. 
            var button = document.querySelector("#TerminateButton");
            button.addEventListener("click", this.applicationButtonListener, false);

            var textArea = document.querySelector("#ExitTextField");
            textArea.addEventListener("contextmenu", this.contextMenuHandler, false);
        },

        contextMenuHandler: function (eventInfo) {
            eventInfo.preventDefault();
            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("ApplicationAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("ApplicationAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("ApplicationAction").innerText = "Context menu dismissed";
                }
            });
        },

        applicationButtonListener: function (eventInfo) {
            window.close();
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
