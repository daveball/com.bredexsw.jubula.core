﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/appBar/appBar.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {

            var appBarFlyoutButton1 = document.querySelector("#AppBarFlyoutButton1");
            appBarFlyoutButton1.addEventListener("click", this.buttonClickHandler, false);
            var appBarFlyoutButton2 = document.querySelector("#AppBarFlyoutButton2");
            appBarFlyoutButton2.addEventListener("click", this.buttonClickHandler, false);

            var appBarButton1 = document.querySelector("#AppBarButton1");
            appBarButton1.addEventListener("click", this.buttonClickHandler, false);
            var appBarButton2 = document.querySelector("#AppBarButton2");
            appBarButton2.addEventListener("click", this.buttonClickHandler, false);

            var appBarToggleButton1 = document.querySelector("#AppBarToggleButton1");
            appBarToggleButton1.addEventListener("click", this.toggleButtonClickHandler, false);
            var appBarToggleButton2 = document.querySelector("#AppBarToggleButton2");
            appBarToggleButton2.addEventListener("click", this.toggleButtonClickHandler, false);

        },

        buttonClickHandler: function (eventInfo) {
            var buttonName = eventInfo.srcElement.id;
            document.getElementById("AppBarAction").innerText = buttonName.substring(6);
        },

        toggleButtonClickHandler: function (eventInfo) {
            var buttonName = eventInfo.srcElement.id;
            document.getElementById("AppBarAction").innerText = buttonName.substring(6) + " " + eventInfo.target.winControl.selected;
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
