﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/toggleSwitches/toggleSwitches.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {

            var toggleSwitch1 = document.querySelector("#ToggleSwitch1");
            toggleSwitch1.addEventListener("contextmenu", this.contextMenuHandler, false);
            toggleSwitch1.addEventListener("change", this.toggleSwitchChangeHandler, false);
            var toggleSwitch2 = document.querySelector("#ToggleSwitch2");
            toggleSwitch2.addEventListener("contextmenu", this.contextMenuHandler, false);
            toggleSwitch2.addEventListener("change", this.toggleSwitchChangeHandler, false);
        },

        toggleSwitchChangeHandler: function (eventInfo) {
            var name = eventInfo.srcElement.id;
            document.getElementById("ToggleSwitchAction").innerText = name + " " + eventInfo.target.winControl.checked;
        },

        contextMenuHandler: function (eventInfo) {
            eventInfo.preventDefault();

            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("ToggleSwitchAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("ToggleSwitchAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("ToggleSwitchAction").innerText = "Context menu dismissed";
                }
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
