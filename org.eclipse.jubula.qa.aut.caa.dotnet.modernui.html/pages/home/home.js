﻿(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/home/home.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {

            document.querySelector("#AppBarButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/appBar/appBar.html");
            };

            document.querySelector("#ApplicationButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/application/application.html");
            };

            document.querySelector("#ButtonsButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/buttons/buttons.html");
            };

            document.querySelector("#CheckBoxesButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/checkBoxes/checkBoxes.html");
            };

            document.querySelector("#ComboBoxesButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/comboBoxes/comboBoxes.html");
            };

            document.querySelector("#LabelsButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/labels/labels.html");
            };

            document.querySelector("#ListsButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/lists/lists.html");
            };

            document.querySelector("#MenuButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/menu/menu.html");
            };

            document.querySelector("#PasswordFieldsButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/passwordFields/passwordFields.html");
            };

            document.querySelector("#RadioButtonsButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/radioButtons/radioButtons.html");
            };

            document.querySelector("#TextAreasButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/textAreas/textAreas.html");
            };

            document.querySelector("#TextFieldsButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/textFields/textFields.html");
            };

            document.querySelector("#TimePickerButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/timePicker/timePicker.html");
            };

            document.querySelector("#ToggleSwitchesButton").onclick = function (args) {
                WinJS.Navigation.navigate("/pages/toggleSwitches/toggleSwitches.html");
            };
        }
    });
})();
