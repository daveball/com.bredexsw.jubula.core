﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/buttons/buttons.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {


            var button = document.querySelector("#Button");
            button.addEventListener("click", this.buttonClickHandler, false);
            button.addEventListener("contextmenu", this.contextMenuHandler, false);

            var button1 = document.querySelector("#Button1");
            button1.addEventListener("click", this.buttonClickHandler, false);
            button1.addEventListener("contextmenu", this.contextMenuHandler, false);

            var resetButton = document.querySelector("#ResetButton");
            resetButton.addEventListener("click", this.buttonClickHandler, false);
            resetButton.addEventListener("contextmenu", this.contextMenuHandler, false);

            var resetButton1 = document.querySelector("#ResetButton1");
            resetButton1.addEventListener("click", this.buttonClickHandler, false);
            resetButton1.addEventListener("contextmenu", this.contextMenuHandler, false);

            var submitButton = document.querySelector("#SubmitButton");
            submitButton.addEventListener("click", this.buttonClickHandler, false);
            submitButton.addEventListener("contextmenu", this.contextMenuHandler, false);

            var submitButton1 = document.querySelector("#SubmitButton1");
            submitButton1.addEventListener("click", this.buttonClickHandler, false);
            submitButton1.addEventListener("contextmenu", this.contextMenuHandler, false);

        },

        buttonClickHandler: function (eventInfo) {
            var buttonName = eventInfo.srcElement.id;
            document.getElementById("ButtonAction").innerText = buttonName;
        },
        
        contextMenuHandler: function (eventInfo) {
            eventInfo.preventDefault();

            var menu = new Windows.UI.Popups.PopupMenu();
            menu.commands.append(new Windows.UI.Popups.UICommand("FirstItem"));
            menu.commands.append(new Windows.UI.Popups.UICommandSeparator);
            menu.commands.append(new Windows.UI.Popups.UICommand("More"));

            document.getElementById("ButtonAction").innerText = "Context menu shown";

            menu.showAsync(pageToWinRT(eventInfo.pageX, eventInfo.pageY)).done(function (invokedCommand) {
                if (invokedCommand !== null) {
                    document.getElementById("ButtonAction").innerText = invokedCommand.label;
                } else {
                    // The command is null if no command was invoked.
                    document.getElementById("ButtonAction").innerText = "Context menu dismissed";
                }
            });
        },



        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });

})();
