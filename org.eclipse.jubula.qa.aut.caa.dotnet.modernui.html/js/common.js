﻿// Converts from page to WinRT coordinates, which take scale factor into consideration.
function pageToWinRT(pageX, pageY) {
    var zoomFactor = document.documentElement.msContentZoomFactor;
    return {
        x: (pageX - window.pageXOffset) * zoomFactor,
        y: (pageY - window.pageYOffset) * zoomFactor
    };
}