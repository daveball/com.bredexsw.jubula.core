package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.JTextField;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JTextFields to test.
 * 
 * JTextField 1: enabled = true tooltip = true contextmenu = true
 * 
 * JTextField 2: enabled = false text = false
 * 
 * JTextField 3: enabled = true editable = true text = true
 * 
 * JTextField 4: enabled = false text = true
 * 
 * JTextField 5: editable = false text = true
 * 
 */
public class TextFieldFrameOpener extends AbstractFrameOpener {

    /**
     * the preferred size for text fields
     */
    private static final Dimension PREFERRED_SIZE = new Dimension(115, 20);

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_textFields"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_TEXTFIELDS);
        // add components to test

        // textField 1
        JTextField tf1 = new JTextField();
        tf1.setName(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF01);
        tf1.setColumns(10);
        tf1.setPreferredSize(PREFERRED_SIZE);
        tf1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils
                .addContextListenerToComponent(tf1, frame.getlabel());
        frame.getPnlContent()
                .add(tf1, SwingComponentUtils.getConstraints(0, 0));

        // textField 2
        JTextField tf2 = new JTextField();
        tf2.setName(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF02);
        tf2.setColumns(10);
        tf2.setPreferredSize(PREFERRED_SIZE);
        tf2.setEnabled(false);
        frame.getPnlContent()
                .add(tf2, SwingComponentUtils.getConstraints(0, 1));

        // textField 3
        JTextField tf3 = new JTextField();
        tf3.setName(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF03);
        tf3.setColumns(10);
        tf3.setPreferredSize(PREFERRED_SIZE);
        tf3.setText(I18NUtils.getString("text1")); //$NON-NLS-1$
        frame.getPnlContent()
                .add(tf3, SwingComponentUtils.getConstraints(0, 2));

        // textField 4
        JTextField tf4 = new JTextField();
        tf4.setName(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF04);
        tf4.setColumns(10);
        tf4.setPreferredSize(PREFERRED_SIZE);
        tf4.setEnabled(false);
        tf4.setText(I18NUtils.getString("text2")); //$NON-NLS-1$
        frame.getPnlContent()
                .add(tf4, SwingComponentUtils.getConstraints(0, 3));

        // textField 5
        JTextField tf5 = new JTextField();
        tf5.setName(ComponentNameConstants.TESTPAGE_TEXTFIELDS_TF05);
        tf5.setColumns(10);
        tf5.setPreferredSize(PREFERRED_SIZE);
        tf5.setEditable(false);
        tf5.setText(I18NUtils.getString("text3")); //$NON-NLS-1$
        frame.getPnlContent()
                .add(tf5, SwingComponentUtils.getConstraints(0, 4));

        return frame;
    }

}
