package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;



/**
 * Opener for JFrame with JLabels to test.
 * 
 * JLabel 1: tooltip = true contextmenu = true enabled = true image = true text
 * = true
 * 
 * JLabel 2: text = true enabled = false
 * 
 */
public class LabelFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        final AbstractTestFrame frame = new AbstractTestFrame("title_labels"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_LABELS);
        // add components to test

        // label 1
        JLabel lbl1 = new JLabel(I18NUtils.getName("lbl1"), SwingComponentUtils //$NON-NLS-1$
                .getIcon("carrot"), SwingConstants.CENTER); //$NON-NLS-1$
        lbl1.setName(ComponentNameConstants.TESTPAGE_LABELS_LBL01);
        lbl1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils.addContextListenerToComponent(lbl1,
                frame.getlabel());
        frame.getPnlContent().add(lbl1,
                SwingComponentUtils.getConstraints(0, 0));

        // label 2
        JLabel lbl2 = new JLabel(I18NUtils.getName("lbl2")); //$NON-NLS-1$
        lbl2.setName(ComponentNameConstants.TESTPAGE_LABELS_LBL02);
        lbl2.setEnabled(false);
        frame.getPnlContent().add(lbl2,
                SwingComponentUtils.getConstraints(0, 1));

        // label 3
        final JLabel lbl3 = new JLabel(I18NUtils.getName("lbl3")); //$NON-NLS-1$
        lbl3.setName(ComponentNameConstants.TESTPAGE_LABELS_LBL03);
        frame.getPnlContent().add(lbl3,
                SwingComponentUtils.getConstraints(0, 2));
        lbl3.addMouseListener(new MouseListener() {

            public void mouseClicked(MouseEvent e) {

                int clickCount = e.getClickCount();
                switch (clickCount) {
                    case 1:
                        lbl3.setText(I18NUtils.getName("oneClick"));
                        break;
                    case 2:
                        lbl3.setText(I18NUtils.getName("twoClick"));
                        break;
                    default:
                        lbl3.setText(e.getClickCount() + " Click");
                }

            }

            public void mouseEntered(MouseEvent e) {
              

            }

            public void mouseExited(MouseEvent e) {
                

            }

            public void mousePressed(MouseEvent e) {
            

            }

            public void mouseReleased(MouseEvent e) {
               
            }

        });

        return frame;
    }

}
