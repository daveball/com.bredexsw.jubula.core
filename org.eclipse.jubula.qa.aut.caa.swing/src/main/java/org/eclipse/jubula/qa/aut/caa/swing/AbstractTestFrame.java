package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.eclipse.jubula.qa.aut.caa.StartSwingAUT;
import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import javax.swing.BoxLayout;
import java.awt.Component;


/**
 * AbstractTestFrame
 * 
 * 
 * @author markus
 * @created 19.03.2008
 */
public class AbstractTestFrame extends JFrame {

    /**
     * m_name
     */
    private String m_name;

    /**
     * m_pnlContent
     */
    private JPanel m_pnlContent;

    /**
     * m_label
     */
    private JLabel m_label;

    /**
     * constructor
     * 
     * @param name
     *            name
     */
    public AbstractTestFrame(String name) {
        m_name = name;
        init();
    }

    /**
     * @return m_label
     */
    public JLabel getlabel() {
        return m_label;
    }

    /**
     * Initializes the startFrame.
     * 
     */
    private void init() {
        this.setTitle(I18NUtils.getString(m_name));
        this.setName(m_name);

        m_pnlContent = new JPanel();
        m_pnlContent.setLayout(new GridBagLayout());
        m_pnlContent.setName(m_name + I18NUtils.getName("cp")); //$NON-NLS-1$
        initContent(m_pnlContent);
        
        JButton closeBtn = new JButton(I18NUtils.getString("closeBtn")); //$NON-NLS-1$
        closeBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        closeBtn.setName(ComponentNameConstants.TESTPAGES_CLOSEBUTTON);
        closeBtn.addMouseListener(new MouseListener() {

            public void mouseClicked(MouseEvent e) {
                dispose();
                StartSwingAUT.autStart();
            }

            public void mouseEntered(MouseEvent e) {
            // do nothing
            }

            public void mouseExited(MouseEvent e) {
            // do nothing
            }

            public void mousePressed(MouseEvent e) {
            // do nothing
            }

            public void mouseReleased(MouseEvent e) {
            // do nothing
            }

        });
        
        JScrollPane scrollableContentPane = new JScrollPane(m_pnlContent);

        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
        
        m_label = new JLabel();
        m_label.setAlignmentX(Component.CENTER_ALIGNMENT);
        m_label.setText(I18NUtils.getString("lblBeforeAction")); //$NON-NLS-1$
        m_label.setName(ComponentNameConstants.TESTPAGES_CONTENTLABEL);
        controlPanel.add(m_label);
        controlPanel.add(closeBtn);
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(scrollableContentPane, BorderLayout.CENTER);
        mainPanel.add(controlPanel, BorderLayout.SOUTH);
        
        this.getContentPane().add(mainPanel);
    }

    /**
     * Returns a JPanel with the components to test.
     * 
     * @param pnlContent
     *            pnlContent
     */
    protected void initContent(JPanel pnlContent) {
        // empty
    }

    /**
     * getPnlContent
     * 
     * @return m_pnlContent
     */
    public JPanel getPnlContent() {
        return m_pnlContent;
    }
}
