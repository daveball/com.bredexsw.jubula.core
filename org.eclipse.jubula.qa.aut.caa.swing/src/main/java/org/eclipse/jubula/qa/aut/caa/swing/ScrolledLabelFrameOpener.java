/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Time;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * 
 *
 * @author tobias
 * @created 05.08.2011
 * @version $Revision: 1.1 $
 */
public class ScrolledLabelFrameOpener extends AbstractFrameOpener {

    /**
     * <code>m_textarea</code>
     * for logs
     */
    private JTextArea m_textarea;
    
    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_buttons"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_BUTTONS);

        m_textarea = new JTextArea();
        JScrollPane logscrolledPane = new JScrollPane(m_textarea);
        logscrolledPane.setPreferredSize(new Dimension(300, 300));
        
        JScrollPane scrolledPane1 = new JScrollPane(createPanel(1));
        scrolledPane1.setPreferredSize(new Dimension(100, 100));

        JScrollPane scrolledPane2 = new JScrollPane(createPanel(2));
        scrolledPane2.setPreferredSize(new Dimension(100, 100));
        scrolledPane2.getViewport().setViewPosition(new Point(300, 300));

        JScrollPane scrolledPane31 = new JScrollPane(createPanel(3));
        scrolledPane31.setPreferredSize(new Dimension(100, 100));
        scrolledPane31.getViewport().setViewPosition(new Point(300, 300));
        JScrollPane scrolledPane3 = new JScrollPane(scrolledPane31);
        scrolledPane3.setPreferredSize(new Dimension(100, 100));

        JPanel panel1 = new JPanel();
        panel1.add(scrolledPane1);
        panel1.add(scrolledPane2);
        panel1.add(scrolledPane3);
        
        frame.getPnlContent().add(panel1, 
                SwingComponentUtils.getConstraints(0, 0));
        frame.getPnlContent().add(logscrolledPane,
                SwingComponentUtils.getConstraints(0, 1));
        
        return frame;
    }
    
    /**
     * @param number of Panel
     * @return Panel with Textfield and Button
     */
    private JPanel createPanel(int number) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        String name = "Button" + number; //$NON-NLS-1$
        JButton button = new JButton(name);
        button.setName(name);
        button.setPreferredSize(new Dimension(300, 25));
        name = "Textfield" + number; //$NON-NLS-1$
        JTextField textfield = new JTextField(name);
        textfield.setName(name);
        textfield.setPreferredSize(new Dimension(300, 25));
        addActionListener(button);
        addActionListener(textfield);
        JPanel emptypanel = new JPanel();
        emptypanel.setPreferredSize(new Dimension(300, 200));
        panel.add(button, SwingComponentUtils.getConstraints(0, 0));
        panel.add(textfield, SwingComponentUtils.getConstraints(0, 1));
        panel.add(emptypanel, SwingComponentUtils.getConstraints(0, 2));
        return panel;
    }
    
    /**
     * @param comp to add a mouse listener
     */
    private void addActionListener(final JComponent comp) {
        comp.addMouseListener(new MouseListener() {
            
            public void mouseReleased(MouseEvent e) {
                //empty
            }
            
            public void mousePressed(MouseEvent e) {
                Date today = new Date();
                Time time = new Time(today.getTime());
                m_textarea.append(time + " " + comp.getName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            
            public void mouseExited(MouseEvent e) {
                //empty
            }
            
            public void mouseEntered(MouseEvent e) {
                //empty
            }
            
            public void mouseClicked(MouseEvent e) {
                //empty
            }
        });
    }

}
