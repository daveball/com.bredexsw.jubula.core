package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.JTextPane;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JTextPanes to test.
 * 
 * JTextPane 1: enabled = true tooltip = true contextmenu = true
 * 
 * JTextPane 2: enabled = false text = false
 * 
 * JTextPane 3: enabled = true editable = true text = true
 * 
 * JTextPane 4: enabled = false text = true
 * 
 * JTextPane 5: editable = false text = true
 * 
 */
public class TextPaneFrameOpener extends AbstractFrameOpener {

    /**
     * the preferred size for text panes
     */
    private static final Dimension PREFERRED_SIZE = new Dimension(105, 25);

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_textPanes"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_TEXTPANES);
        // add components to test

        // textPane 1
        JTextPane tp1 = new JTextPane();
        tp1.setName(ComponentNameConstants.TESTPAGE_TEXTPANES_TP01);
        tp1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils
                .addContextListenerToComponent(tp1, frame.getlabel());
        tp1.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(tp1, SwingComponentUtils.getConstraints(0, 0));

        // editorPane 2
        JTextPane tp2 = new JTextPane();
        tp2.setName(ComponentNameConstants.TESTPAGE_TEXTPANES_TP02);
        tp2.setEnabled(false);
        tp2.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(tp2, SwingComponentUtils.getConstraints(0, 1));

        // editorPane 3
        JTextPane tp3 = new JTextPane();
        tp3.setName(ComponentNameConstants.TESTPAGE_TEXTPANES_TP03);
        tp3.setText(I18NUtils.getString("text1")); //$NON-NLS-1$
        tp3.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(tp3, SwingComponentUtils.getConstraints(0, 2));

        // editorPane 4
        JTextPane tp4 = new JTextPane();
        tp4.setName(ComponentNameConstants.TESTPAGE_TEXTPANES_TP04);
        tp4.setEnabled(false);
        tp4.setText(I18NUtils.getString("text2")); //$NON-NLS-1$
        tp4.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(tp4, SwingComponentUtils.getConstraints(0, 3));

        // editorPane 5
        JTextPane tp5 = new JTextPane();
        tp5.setName(ComponentNameConstants.TESTPAGE_TEXTPANES_TP05);
        tp5.setEditable(false);
        tp5.setText(I18NUtils.getString("text3")); //$NON-NLS-1$
        tp5.setPreferredSize(PREFERRED_SIZE);
        frame.getPnlContent()
                .add(tp5, SwingComponentUtils.getConstraints(0, 4));

        return frame;
    }

}
