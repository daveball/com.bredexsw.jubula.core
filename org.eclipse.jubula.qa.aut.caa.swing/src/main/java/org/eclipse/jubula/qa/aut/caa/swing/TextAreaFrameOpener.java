package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;
import javax.swing.JTextArea;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JTextAreas to test.
 * 
 * JTextArea 1: enabled = true tooltip = true contextmenu = true
 * 
 * JTextArea 2: enabled = false text = false
 * 
 * JTextArea 3: enabled = true editable = true text = true
 * 
 * JTextArea 4: enabled = false text = true
 * 
 * JTextArea 5: editable = false text = true
 * 
 */
public class TextAreaFrameOpener extends AbstractFrameOpener {

    /**
     * the preferred size for editor panes
     */
    private static final Dimension PREFERRED_SIZE = new Dimension(110, 32);

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_textAreas"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_TEXTAREAS);
        // add components to test

        // textArea 1
        JTextArea ta1 = new JTextArea();
        ta1.setName(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA01);
        ta1.setColumns(10);
        ta1.setRows(2);
        ta1.setPreferredSize(PREFERRED_SIZE);
        ta1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils
                .addContextListenerToComponent(ta1, frame.getlabel());
        frame.getPnlContent()
                .add(ta1, SwingComponentUtils.getConstraints(0, 0));

        // textArea 2
        JTextArea ta2 = new JTextArea();
        ta2.setName(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA02);
        ta2.setColumns(10);
        ta2.setRows(2);
        ta2.setEnabled(false);
        frame.getPnlContent()
                .add(ta2, SwingComponentUtils.getConstraints(0, 1));

        // textArea 3
        JTextArea ta3 = new JTextArea();
        ta3.setName(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA03);
        ta3.setColumns(10);
        ta3.setRows(2);
        ta3.setPreferredSize(PREFERRED_SIZE);
        ta3.setText(I18NUtils.getString("text1")); //$NON-NLS-1$
        frame.getPnlContent()
                .add(ta3, SwingComponentUtils.getConstraints(0, 2));

        // textArea 4
        JTextArea ta4 = new JTextArea();
        ta4.setName(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA04);
        ta4.setColumns(10);
        ta4.setRows(2);
        ta4.setEnabled(false);
        ta4.setText(I18NUtils.getString("text2")); //$NON-NLS-1$
        frame.getPnlContent()
                .add(ta4, SwingComponentUtils.getConstraints(0, 3));

        // textArea 5
        JTextArea ta5 = new JTextArea();
        ta5.setName(ComponentNameConstants.TESTPAGE_TEXTAREAS_TA05);
        ta5.setColumns(10);
        ta5.setRows(2);
        ta5.setEditable(false);
        ta5.setText(I18NUtils.getString("text3")); //$NON-NLS-1$
        frame.getPnlContent()
                .add(ta5, SwingComponentUtils.getConstraints(0, 4));

        return frame;
    }

}
