package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;


/**
 * Opener for JFrame with JMenuBar to test.
 * 
 * MenuItems: enabled = true /false radioButton = true checkBox = true separator
 * = true subMenu = true
 * 
 */
public class MenuFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_menus"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_MENUS);
        // add components to test
        JMenuBar menuBar = new JMenuBar();
        final JLabel frameLabel = frame.getlabel();
        menuBar.add(createMenu(frameLabel, 
            I18NUtils.getString("menu")));  //$NON-NLS-1$
        menuBar.add(createMenu(frameLabel, 
            I18NUtils.getString("menu2")));  //$NON-NLS-1$
        
        frame.setJMenuBar(menuBar);

        return frame;
    }

    /**
     * Returns a Menu to use in a menuBar.
     * 
     * @param output
     *            The label which shows the selected menu entry
     * @param label
     *            the label to use for the created menu
     * 
     * @return menu
     */
    private JMenu createMenu(final JLabel output, String label) {
        JMenu menu = new JMenu(label);
        JMenuItem item1 = new JMenuItem(I18NUtils.getString("menuitem_first")); //$NON-NLS-1$
        JMenuItem itemNotVisible = 
                new JMenuItem(I18NUtils.getString("menuitem_nonvisible"));
        itemNotVisible.setVisible(false);
        JMenu submenu = new JMenu(I18NUtils.getString("menu_more")); //$NON-NLS-1$
        JMenuItem item2 = new JMenuItem(I18NUtils.getString("menuitem_second")); //$NON-NLS-1$
        JCheckBoxMenuItem cbxItem = new JCheckBoxMenuItem(I18NUtils
                .getString("menuitem_cbx")); //$NON-NLS-1$
        JRadioButtonMenuItem rbxItem = new JRadioButtonMenuItem(I18NUtils
                .getString("menuitem_rbx")); //$NON-NLS-1$
        JMenuItem item3 = new JMenuItem(I18NUtils.getString("menuitem_third")); //$NON-NLS-1$
        item3.setEnabled(false);
        JMenu submenu2 = new JMenu(I18NUtils.getString("menu_more")); //$NON-NLS-1$
        JMenuItem item4 = new JMenuItem(I18NUtils.getString("menuitem_level3")); //$NON-NLS-1$
        JMenu submenu3 = new JMenu(I18NUtils.getString("menu_more")); //$NON-NLS-1$
        JMenuItem item5 = new JMenuItem(I18NUtils.getString("menuitem_level4")); //$NON-NLS-1$
        submenu.add(item2);
        submenu.add(cbxItem);
        submenu.add(rbxItem);
        submenu.addSeparator();
        submenu.add(item3);
        submenu.add(submenu2);
        submenu2.add(item4);
        submenu2.add(submenu3);
        submenu3.add(item5);
        menu.add(item1);
        menu.add(itemNotVisible);
        menu.add(submenu);        
        
        if (output != null) {
            ActionListener listener = new ActionListener() {
                
                public void actionPerformed(ActionEvent event) {
                    output.setText(event.getActionCommand());
                }
            };
            item1.addActionListener(listener);
            item2.addActionListener(listener);
            item3.addActionListener(listener);
            cbxItem.addActionListener(listener);
            rbxItem.addActionListener(listener);
            item4.addActionListener(listener);
            item5.addActionListener(listener);
        }
        
        return menu;
    }
}
