package org.eclipse.jubula.qa.aut.caa.swing;

import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JTables to test.
 * 
 * JTable 1: enabled = true tooltip = true contextmenu = true
 * 
 * JTable 2: enabled = true header = false
 * 
 */
public class TableFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_tables"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_TABLES);

        // table 1
        JTable tbl1 = new JTable(getHeaderTableData(), getColumnNames()) {
            public boolean isCellEditable(int x, int y) {
                return false;
            }
        };
        tbl1.setName(ComponentNameConstants.TESTPAGE_TABLES_TBL01);
        tbl1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        JScrollPane scrollPane = new JScrollPane(tbl1);
        scrollPane.setPreferredSize(new Dimension(250, 60));
        frame.getPnlContent().add(scrollPane,
                SwingComponentUtils.getConstraints(0, 0));

        // table 2
        JTable tbl2 = new JTable(getTableData(), getColumnNames());
        tbl2.setName(ComponentNameConstants.TESTPAGE_TABLES_TBL02);
        tbl2.setTableHeader(null);
        SwingComponentUtils.addContextListenerToComponent(tbl2,
                frame.getlabel());
        JScrollPane scrollPane2 = new JScrollPane(tbl2);
        scrollPane2.setPreferredSize(new Dimension(250, 60));
        frame.getPnlContent().add(scrollPane2, 
                SwingComponentUtils.getConstraints(0, 1));
        
        // table 3
        JTable tbl3 = new JTable(getHeaderTableData(), getColumnNames());
        tbl3.setName(ComponentNameConstants.TESTPAGE_TABLES_TBL03);
        tbl3.setMinimumSize(new Dimension(300, 65));
        for (int i = 0; i < tbl3.getColumnCount(); i++) {
            tbl3.getColumnModel().getColumn(i).setMinWidth(110);
        }
        tbl3.setAutoscrolls(true);
        tbl3.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane scrollPane3 = new JScrollPane(tbl3);
        scrollPane3.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane3.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane3.setAutoscrolls(true);
        scrollPane3.setPreferredSize(new Dimension(90, 60));
        frame.getPnlContent().add(scrollPane3, 
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 2));
        
        JTable tbl4 = createTable();
        addAlignedColumn(tbl4);
        
        tbl4.setName(ComponentNameConstants.TESTPAGE_TABLES_TBL04);
        tbl4.setMinimumSize(new Dimension(300, 65));
        tbl4.setAutoscrolls(true);
        tbl4.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane scrollPane4 = new JScrollPane(tbl4);
        scrollPane4.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane4.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane4.setAutoscrolls(true);
        scrollPane4.setPreferredSize(new Dimension(300, 150));
        frame.getPnlContent().add(scrollPane4, 
                SwingComponentUtils.getConstraintsWithoutHorizontalFill(0, 3));
        return frame;
    }

    /**
     * @param table table 
     */
    private void addAlignedColumn(JTable table) {
        DefaultTableModel model = new DefaultTableModel();
        model = (DefaultTableModel) table.getModel();
        String[] columnData = new String[20];
        String rl = I18NUtils.getString("textRightAlign"); //$NON-NLS-1$
        for (int j = 0; j < 20; j++) {
            columnData[j] = rl + String.valueOf(j + 1);
        }
        String columnName = I18NUtils.getString("rightAlignColName"); //$NON-NLS-1$
        model.addColumn(columnName, columnData);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        table.getColumn(columnName).setCellRenderer(rightRenderer);
    }

    /**
     * getColumnNames
     * 
     * @return ColumnNames
     */
    private String[] getColumnNames() {
        return new String[] { I18NUtils.getString("col1"), //$NON-NLS-1$
                I18NUtils.getString("col2"), I18NUtils.getString("col3"), //$NON-NLS-1$ //$NON-NLS-2$
                I18NUtils.getString("col4") }; //$NON-NLS-1$
    }

    /**
     * getHeaderTableData
     * 
     * @return TableData
     */
    private Object[][] getHeaderTableData() {
        return new Object[][] {
            { I18NUtils.getString("val1"), I18NUtils.getString("val2"), //$NON-NLS-1$ //$NON-NLS-2$
              new Integer(3), new Boolean(true) },
            { I18NUtils.getString("val4"), I18NUtils.getString("val5"), //$NON-NLS-1$ //$NON-NLS-2$
              new Integer(6), new Boolean(false) } };
    }
    
    /**
     * getTableData
     * 
     * @return TableData
     */
    private Object[][] getTableData() {
        return new Object[][] {
            { I18NUtils.getString("text1"), I18NUtils.getString("text7"), //$NON-NLS-1$ //$NON-NLS-2$
              I18NUtils.getString("text3"), I18NUtils.getString("text5") }, //$NON-NLS-1$ //$NON-NLS-2$
            { I18NUtils.getString("text2"), I18NUtils.getString("text4"), //$NON-NLS-1$ //$NON-NLS-2$
              I18NUtils.getString("text5"), I18NUtils.getString("text1") }, //$NON-NLS-1$ //$NON-NLS-2$
            { I18NUtils.getString("text3"), I18NUtils.getString("text1"), //$NON-NLS-1$ //$NON-NLS-2$
              I18NUtils.getString("text4"), I18NUtils.getString("text7") } }; //$NON-NLS-1$ //$NON-NLS-2$
 
    }

    /**
     * @return filled JTable
     */
    private JTable createTable() {
        JTable table;
        DefaultTableModel model = new DefaultTableModel();
        table = new JTable(model);
        char letter = 'A';
        String[] columnData = new String[20];
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                columnData[j] = letter + String.valueOf(j + 1);
            }
            model.addColumn(new Character(letter), columnData);
            letter++;
        }
        return table;
    }
}

