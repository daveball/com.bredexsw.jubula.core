/**
 * 
 */
package org.eclipse.jubula.qa.aut.caa.swing;

import javax.swing.JProgressBar;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;

/**
 * jProgressBar
 * 
 * @author mai
 * 
 */
public class JProgressBarFrameOpener extends AbstractFrameOpener {
    /**
     * this is the min value
     */
    private static final int MIN = 0;
    /**
     * this is the max value
     */
    private static final int MAX = 100;

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_jProgressBar");
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_JPROGRESSBARS);
        // add components to test

        // progressbar1: vertical
        final JProgressBar pb1 = new JProgressBar(JProgressBar.VERTICAL, MIN,
                MAX);
        pb1.setName(ComponentNameConstants.TESTPAGE_JPROGRESSBAR_JPB01);

        pb1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$
        SwingComponentUtils
                .addContextListenerToComponent(pb1, frame.getlabel());
        frame.getPnlContent()
                .add(pb1, SwingComponentUtils.getConstraints(MIN, MIN));

        // progressbar2: horizontal
        final JProgressBar pb2 = new JProgressBar(JProgressBar.HORIZONTAL, MIN,
                MAX);
        pb2.setName(ComponentNameConstants.TESTPAGE_JPROGRESSBAR_JPB02);

        pb2.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$  
        SwingComponentUtils
                .addContextListenerToComponent(pb2, frame.getlabel());
        frame.getPnlContent()
                .add(pb2, SwingComponentUtils.getConstraints(1, 1));

        // progressbar3: disabled
        // the disabled appearance is controlled by the UI delegate for a given
        // Look & Feel
        final JProgressBar pb3 = new JProgressBar(JProgressBar.HORIZONTAL, MIN,
                MAX);
        pb3.setName(ComponentNameConstants.TESTPAGE_JPROGRESSBAR_JPB03);
        pb3.setEnabled(false);
        pb3.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$  
        frame.getPnlContent()
                .add(pb3, SwingComponentUtils.getConstraints(1, 2));

        return frame;
    }

}