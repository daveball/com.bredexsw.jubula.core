package org.eclipse.jubula.qa.aut.caa.swing;

import javax.swing.JButton;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;


/**
 * Opener for JFrame with JButtons to test.
 * 
 * JButton 1: enabled = true tooltip = true contextmenu = true
 * 
 * JButton 2: enabled = false
 * 
 */
public class ButtonFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_buttons"); //$NON-NLS-1$
        frame.setName(ComponentNameConstants.TESTPAGE_TITLE_BUTTONS);
        // add components to test

        // button 1
        JButton btn1 = new JButton(I18NUtils.getName("btn1")); //$NON-NLS-1$
        btn1.setName(ComponentNameConstants.TESTPAGE_BUTTONS_BTN01);
        btn1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-1$
        SwingComponentUtils.addContextListenerToComponent(btn1,
                frame.getlabel());
        frame.getPnlContent().add(btn1,
                SwingComponentUtils.getConstraints(0, 0));

        // button 2
        JButton btn2 = new JButton(I18NUtils.getName("btn2")); //$NON-NLS-1$
        btn2.setName(ComponentNameConstants.TESTPAGE_BUTTONS_BTN02);
        btn2.setEnabled(false);
        frame.getPnlContent().add(btn2,
                SwingComponentUtils.getConstraints(0, 1));

        return frame;
    }

}
