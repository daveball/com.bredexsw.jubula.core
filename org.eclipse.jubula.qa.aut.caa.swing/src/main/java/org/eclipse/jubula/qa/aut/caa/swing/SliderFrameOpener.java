package org.eclipse.jubula.qa.aut.caa.swing;

import javax.swing.JSlider;
import javax.swing.SwingConstants;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.utils.SwingComponentUtils;

/**
 * slider
 * 
 * @author ann
 * 
 */
public class SliderFrameOpener extends AbstractFrameOpener {

    /**
     * {@inheritDoc}
     */
    protected AbstractTestFrame getFrame() {
        AbstractTestFrame frame = new AbstractTestFrame("title_slider");
        frame.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL01);
        // add components to test

        // slider1
        JSlider sl1 = new JSlider(SwingConstants.HORIZONTAL);
        sl1.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL01);
        sl1.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$
        SwingComponentUtils
                .addContextListenerToComponent(sl1, frame.getlabel());
        frame.getPnlContent()
                .add(sl1, SwingComponentUtils.getConstraints(0, 0));

        // slider2
        JSlider sl2 = new JSlider(SwingConstants.HORIZONTAL);
        sl2.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL02);
        sl2.setEnabled(false);
        frame.getPnlContent()
                .add(sl2, SwingComponentUtils.getConstraints(0, 1));
        
        // slider3
        JSlider sl3 = new JSlider(SwingConstants.VERTICAL);
        sl3.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL03);
        sl3.setToolTipText(I18NUtils.getString("tooltip")); //$NON-NLS-N$
        SwingComponentUtils
                .addContextListenerToComponent(sl3, frame.getlabel());
        frame.getPnlContent()
                .add(sl3, SwingComponentUtils.getConstraints(0, 2));

        // slider4
        JSlider sl4 = new JSlider(SwingConstants.VERTICAL);
        sl4.setName(ComponentNameConstants.TESTPAGE_SLIDER_SL04);
        sl4.setEnabled(false);
        frame.getPnlContent()
                .add(sl4, SwingComponentUtils.getConstraints(1, 2));

        return frame;
    }
}
