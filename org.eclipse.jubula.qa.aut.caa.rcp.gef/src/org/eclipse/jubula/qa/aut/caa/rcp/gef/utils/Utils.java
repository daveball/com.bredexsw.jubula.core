/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2012 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rcp.gef.utils;

/**
 * 
 *
 * @author tobias
 * @created 09.02.2012
 * @version $Revision: 1.1 $
 */
public class Utils {
    
    /** COMMAND_ID */
    public static final String COMMAND_ID = "org.eclipse.jubula.qa.aut.caa.rcp.gef.openShapeEditorCommand"; //$NON-NLS-1$
    
    /** PARAM_OPEN_EDITOR parameter-value for menu contribution */
    public static final String PARAM_OPEN_EDITOR = "org.eclipse.jubula.qa.aut.caa.rcp.gef.openEditorContribution";  //$NON-NLS-1$
    
    /** SHAPES_DIAGRAM */
    public static final String SHAPES_DIAGRAM = "/shapesExample.shapes"; //$NON-NLS-1$
    
    /** SHAPES_PROJECT */
    public static final String SHAPES_PROJECT = "/example"; //$NON-NLS-1$
    
    /** RESOURCES */
    public static final String RESOURCES = "/resources/"; //$NON-NLS-1$
}
