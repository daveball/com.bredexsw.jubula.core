package org.eclipse.jubula.qa.aut.caa.rcp.gef.perspective;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * @author tobias
 * @created 16.09.2011
 * @version $Revision: 1.1 $
 */
public class Perspective implements IPerspectiveFactory {

    /**
     * {@inheritDoc}
     */
    public void createInitialLayout(IPageLayout layout) {
        layout.setEditorAreaVisible(true);
    }

}
