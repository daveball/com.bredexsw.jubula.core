/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rcp.gef;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jubula.qa.aut.caa.rcp.gef.activator.Activator;
import org.eclipse.jubula.qa.aut.caa.rcp.gef.utils.Utils;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;

/**
 * 
 *
 * @author tobias
 * @created 28.09.2011
 * @version $Revision: 1.1 $
 */
public class OpenEditor extends AbstractHandler {
    
    /**
     * {@inheritDoc}
     */
    public Object execute(ExecutionEvent event) {
        String param = event.getParameter(Utils.PARAM_OPEN_EDITOR);
        IWorkbenchWindow window = 
                HandlerUtil.getActiveWorkbenchWindow(event);
        IWorkbenchPage page = window.getActivePage();
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IProject caaProject = root.getProject(Utils.SHAPES_PROJECT);
        IProgressMonitor progressMonitor = new NullProgressMonitor();
        
        if (!caaProject.exists()) {
            try {
                caaProject.create(progressMonitor);
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }

        try {
            caaProject.open(progressMonitor);
            IFile shapesFile = caaProject.getFile(param);
            if (shapesFile.exists()) {
                shapesFile.delete(true, null);
            }
            shapesFile.create(
//                    Activator.getDefault().getBundle().getResource("abc").openStream();
                    Activator.class.getResourceAsStream(
                            Utils.RESOURCES + param),
                            true, null);
            try {
                page.openEditor(new FileEditorInput(shapesFile),
                        "GEF Shapes Editor"); //$NON-NLS-1$
            } catch (PartInitException e) {
                e.printStackTrace();
            }
        } catch (CoreException e1) {
            e1.printStackTrace();
        }
        return null;
    }


}
