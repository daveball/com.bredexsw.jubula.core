/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2012 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rcp.gef;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jubula.qa.aut.caa.rcp.gef.activator.Activator;
import org.eclipse.jubula.qa.aut.caa.rcp.gef.utils.Utils;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;

/**
 * 
 *
 * @author tobias
 * @created 08.02.2012
 * @version $Revision: 1.1 $
 */
public class CreateGefOpenerContribution extends CompoundContributionItem {

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected IContributionItem[] getContributionItems() {
        List<IContributionItem> contributionItems = 
                new ArrayList<IContributionItem>();
        URL bundleRootURL = Activator.getDefault().getBundle().getEntry(
                "/resources/"); //$NON-NLS-1$
        try {
            URL pluginUrl = FileLocator.resolve(bundleRootURL);
            File resourceDir = new File(pluginUrl.getFile());
            File[] availableShapes = 
                    resourceDir.listFiles(new FilenameFilter() {
                        public boolean accept(File dir, String name) {
                            if (name != null && name.matches(
                                            ".*shapes")) { //$NON-NLS-1$
                                return true;
                            }
                            return false;
                        }
                    });
            for (File f : availableShapes) {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put(Utils.PARAM_OPEN_EDITOR, f.getName());
                String itemName = f.getName().replaceFirst("\\.shapes", ""); //$NON-NLS-1$ //$NON-NLS-2$
                CommandContributionItemParameter itemParameter = 
                        new CommandContributionItemParameter(
                            PlatformUI.getWorkbench(), null, Utils.COMMAND_ID,
                            CommandContributionItem.STYLE_PUSH);
                itemParameter.label = itemName;
                if (params != null) {
                    if (itemParameter.parameters == null) {
                        itemParameter.parameters = new HashMap(params);
                    } else {
                        itemParameter.parameters.putAll(params);
                    }
                }
                contributionItems.add(
                        new CommandContributionItem(itemParameter));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contributionItems
                .toArray(new IContributionItem[contributionItems.size()]);
    }

}
