/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rcp.swt.examples.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.examples.controlexample.ControlExample;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

/**
 * 
 *
 * @author tobias
 * @created Feb 11, 2011
 * @version $Revision: 1.1 $
 */
public class SWTExampleWrapperView extends ViewPart {

    /** storage for dialog content */
    private ControlExample m_controlExample;

    /**
     * {@inheritDoc}
     */
    public void createPartControl(Composite parent) {
        ScrolledComposite scrollComposite = new ScrolledComposite(parent, 
                SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        Composite composite = new Composite(scrollComposite, SWT.NONE);
        composite.setLayout(new FillLayout());
        m_controlExample = new ControlExample(composite);
        composite.setSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        scrollComposite.setContent(composite);
    }

    /**
     * {@inheritDoc}
     */
    public void setFocus() {
        m_controlExample.setFocus();
    }

}
