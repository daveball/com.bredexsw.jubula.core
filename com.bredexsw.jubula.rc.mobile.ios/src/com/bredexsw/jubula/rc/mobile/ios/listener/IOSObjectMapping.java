package com.bredexsw.jubula.rc.mobile.ios.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.communication.internal.message.ObjectMappedMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.rc.common.AUTServerConfiguration;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.rc.common.listener.AUTEventListener;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.exception.CommunicationException;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.eclipse.jubula.tools.internal.xml.businessmodell.ComponentClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.components.TechnicalComponentHandler;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 **/
public class IOSObjectMapping implements AUTEventListener {
    /**
     * the class prefix for the pseudo iOS / Objective-C classes
     */
    public static final String IOS_PSEUDO_CLASS_PACKAGE = "ios."; //$NON-NLS-1$

    /** <code>log</code>: the logger */
    private static Logger log = LoggerFactory.getLogger(IOSObjectMapping.class);

    /**
     * the time to use for collecting iOS elements
     */
    private Timer m_timer = null;

    /** {@inheritDoc} */
    public void start() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_OMM,
                IOSComConst.METHOD_SIGNATURE_START_OMM);
        Set<ComponentClass> st = AUTServerConfiguration.getInstance()
                .getSupportedTypes();
        List<String> supportedComponentClasses = new ArrayList<String>();

        for (ComponentClass cc : st) {
            supportedComponentClasses.add(cc.getName().replaceFirst(
                    IOS_PSEUDO_CLASS_PACKAGE, StringConstants.EMPTY));
        }

        message.addParameter(supportedComponentClasses.toArray());
        IOSAUTServer.sendMessage(message);
        m_timer = new Timer("Collect tapped iOS widgets", true); //$NON-NLS-1$
        m_timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                NativeAnswer answer = IOSAUTServer
                    .sendMessage(new NativeMessage(
                        IOSComConst.CLASS_NAME_OMM,
                        IOSComConst.METHOD_SIGNATURE_GET_TAPPED_WIDGETS));

                String jsonSerializedIndexPaths = answer.getString();
                Collection<IComponentIdentifier> identifierList = 
                        TechnicalComponentHandler
                            .getInstance().getIdentifier(
                                    jsonSerializedIndexPaths);
                if (!identifierList.isEmpty()) {
                    try {
                        // send a message with the identifier of the selected component
                        ObjectMappedMessage message = new ObjectMappedMessage();
                        message.setComponentIdentifiers(identifierList
                                .toArray(new IComponentIdentifier[identifierList
                                        .size()]));
                        AUTServer.getInstance().getCommunicator().send(message);
                    } catch (CommunicationException ce) {
                        log.error(ce.getLocalizedMessage(), ce);
                        // do nothing here: a closed connection is handled by the AUTServer
                    }
                }
            }
        }, 1000, 1000);
    }

    /** {@inheritDoc} */
    public void stop() {
        if (m_timer != null) {
            m_timer.cancel();
            m_timer = null;
        }
        NativeMessage message = new NativeMessage(
            IOSComConst.CLASS_NAME_OMM,
            IOSComConst.METHOD_SIGNATURE_STOP_OMM);
        IOSAUTServer.sendMessage(message);
    }

    /** {@inheritDoc} */
    public boolean highlightComponent(IComponentIdentifier compId) {
        AbstractIOSComponent component = null;
        try {
            component = TechnicalComponentHandler.getInstance()
                .findComponent(compId, 0);
            if (component != null) {
                NativeMessage message = new NativeMessage(
                    IOSComConst.CLASS_NAME_OMM,
                    IOSComConst.METHOD_SIGNATURE_HIGHLIGHT);
                message.addParameter(StringUtils.split(
                        component.getLocatorPath(), 
                        AbstractIOSComponent.SEPARATOR_CHAR));
                IOSAUTServer.sendMessage(message);
                return true;
            }
        } catch (ComponentNotFoundException e) {
            log.warn(e.getLocalizedMessage(), e);
        } catch (IllegalArgumentException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    /** {@inheritDoc} */
    public long[] getEventMask() {
        return null;
    }

    /** {@inheritDoc} */
    public void cleanUp() {
        // not necessary
    }

    /** {@inheritDoc} */
    public void update() {
        // not necessary
    }
}
