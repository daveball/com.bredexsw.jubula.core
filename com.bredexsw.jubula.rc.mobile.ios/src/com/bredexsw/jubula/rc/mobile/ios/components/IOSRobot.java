/**
 * 
 */
package com.bredexsw.jubula.rc.mobile.ios.components;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.exception.OsNotSupportedException;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public class IOSRobot implements IRobot<Rectangle> {
    /** the logger */
    private static final Logger LOG = 
        LoggerFactory.getLogger(IOSRobot.class);
    
    /** {@inheritDoc} */
    public void clickAtCurrentPosition(Object graphicsComponent,
            int clickCount, int button) throws RobotException {
        // not-yet supported
    }

    /** {@inheritDoc} */
    public void click(Object graphicsComponent, Rectangle constraints)
        throws RobotException {
        click(graphicsComponent, constraints, null);
    }

    /** {@inheritDoc} */
    public void click(Object graphicsComponent, Rectangle constraints,
            ClickOptions clickOptions, int xPos, boolean xAbsolute, int yPos,
            boolean yAbsolute) throws RobotException {

        AbstractIOSComponent comp = (AbstractIOSComponent) graphicsComponent;
        NativeMessage message;
        
        if (constraints == null && xPos != -1) {
            message = messageForClickPosition(graphicsComponent, 
                     xPos, xAbsolute, yPos, yAbsolute);
        } else if (constraints instanceof Rectangle) {
            message = messageForClickRectangle(graphicsComponent, constraints);
        } else {
            message = new NativeMessage(
                    IOSComConst.CLASS_NAME_CAPS_KIF,
                    IOSComConst.METHOD_SIGNATURE_TAP,
                    comp.getLocatorPath());
        }
        
        if (clickOptions != null) {
            for (int i = 0; i < clickOptions.getClickCount(); i++) {
                IOSAUTServer.sendMessage(message);
            }
        } else {
            IOSAUTServer.sendMessage(message);
        }
    }
    
    /**
     * Create native message with click position params
     * 
     * @param graphicsComponent The graphics component to click on
     * @param xPos x position
     * @param xAbsolute if true the unit is pixel, if false the unit is percent
     * @param yPos y position
     * @param yAbsolute if true the unit is pixel, if false the unit is percent
     * @return native message
     */
    private NativeMessage messageForClickPosition(Object graphicsComponent,
            int xPos, boolean xAbsolute, int yPos,
            boolean yAbsolute) {
        AbstractIOSComponent comp = (AbstractIOSComponent) graphicsComponent;
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF,
                IOSComConst.METHOD_SIGNATURE_TAP_AT_POSITION,
                comp.getLocatorPath());
        
        message.addParameter(String.valueOf(xPos));
        String xUnit = xAbsolute ? "PIXEL" : "PERCENT";
        message.addParameter(xUnit);
        message.addParameter(yPos);
        String yUnit = yAbsolute ? "PIXEL" : "PERCENT";
        message.addParameter(yUnit);
        return message;
    }
    
    /**
     * Create native message with Rectangle param
     * 
     * @param graphicsComponent The graphics component to click on
     * @param constraints A constraints object used by the Robot implementation
     * @return native message
     */
    private NativeMessage messageForClickRectangle(Object graphicsComponent,
            Rectangle constraints) {
        AbstractIOSComponent comp = (AbstractIOSComponent) graphicsComponent;
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF,
                IOSComConst.METHOD_SIGNATURE_TAP_AT,
                comp.getLocatorPath());
        message.addParameter(String.valueOf(constraints.x));
        message.addParameter(String.valueOf(constraints.y));
        message.addParameter(String.valueOf(constraints.width));
        message.addParameter(String.valueOf(constraints.height));
        return message;
    }

    /** {@inheritDoc} */
    public void click(Object graphicsComponent, Rectangle constraints,
            ClickOptions clickOptions) throws RobotException {
        click(graphicsComponent, constraints, clickOptions, -1, true, -1, true);
    }

    /** {@inheritDoc} */
    public void move(Object graphicsComponent, Rectangle constraints)
        throws RobotException {
        // not-yet supported
    }

    /** {@inheritDoc} */
    public void type(Object graphicsComponent, char character)
        throws RobotException {
        type(graphicsComponent, String.valueOf(character));
    }

    /** {@inheritDoc} */
    public void type(Object graphicsComponent, String text)
        throws RobotException {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF,
                IOSComConst.METHOD_SIGNATURE_TYPE_TEXT);
        message.addParameter(text);
        IOSAUTServer.sendMessage(message);
    }

    /** {@inheritDoc} */
    public void keyType(Object graphicsComponent, int keycode)
        throws RobotException {
        // not supported on iOS
    }

    /** {@inheritDoc} */
    public void keyPress(Object graphicsComponent, int keycode)
        throws RobotException {
        // not supported on iOS
    }

    /** {@inheritDoc} */
    public void keyRelease(Object graphicsComponent, int keycode)
        throws RobotException {
        // not supported on iOS
    }

    /** {@inheritDoc} */
    public void keyStroke(String keyStrokeSpecification) throws RobotException {
        if (ValueSets.KeyStroke.delete.rcValue()
                .equals(keyStrokeSpecification)) {
            NativeMessage message = new NativeMessage(
                    IOSComConst.CLASS_NAME_CAPS_KIF,
                    IOSComConst.METHOD_SIGNATURE_PRESS_DELETE);
            IOSAUTServer.sendMessage(message);
        }
    }

    /** {@inheritDoc} */
    public void scrollToVisible(Object graphicsComponent, Rectangle constraints)
        throws RobotException {
        // not-yet supported
    }

    /** {@inheritDoc} */
    public void keyToggle(Object obj, int key, boolean activated)
        throws OsNotSupportedException {
        // not supported on iOS
    }

    /** {@inheritDoc} */
    public void activateApplication(String method) throws RobotException {
        // not supported on iOS - the application is always active 
    }

    /** {@inheritDoc} */
    public Point getCurrentMousePosition() {
        // not supported on iOS
        return null;
    }

    /** {@inheritDoc} */
    public void mousePress(Object graphicsComponent, Rectangle constraints,
            int button) throws RobotException {
        // not-yet supported
    }

    /** {@inheritDoc} */
    public void mouseRelease(Object graphicsComponent, Rectangle constraints,
            int button) throws RobotException {
        // not-yet supported
    }

    /** {@inheritDoc} */
    public boolean isMouseInComponent(Object graphicsComponent) {
        // not supported on iOS
        return false;
    }

    /** {@inheritDoc} */
    public String getSystemModifierSpec() {
        // not supported on iOS
        return null;
    }

    /** {@inheritDoc} */
    public String getPropertyValue(Object graphicsComponent, 
        String propertyName)
        throws RobotException {
        // not-yet supported
        return null;
    }

    /** {@inheritDoc} */
    public BufferedImage createFullScreenCapture() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_AUT,
                IOSComConst.METHOD_SIGNATURE_GET_SCREENSHOT);
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        
        return stringToImage(answer.getString());
    }
    
    /**
     * @param imageString
     *            the image string
     * @return the buffered image
     */
    public BufferedImage stringToImage(String imageString) {
        BufferedImage bImage = null;
        ByteArrayInputStream bais;
        try {
            bais = new ByteArrayInputStream(Base64.decodeBase64(
                    imageString.getBytes()));
            bImage = ImageIO.read(bais);
        } catch (IOException ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
        }

        return bImage;
    }
}