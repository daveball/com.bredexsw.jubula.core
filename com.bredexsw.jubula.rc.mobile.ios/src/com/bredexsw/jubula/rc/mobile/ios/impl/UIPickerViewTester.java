package com.bredexsw.jubula.rc.mobile.ios.impl;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.ComboBoxTester;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.AbstractUIPickerAdapter;
import com.bredexsw.jubula.rc.mobile.tester.interfaces.IMobileWidgetsTester;

/**
 * @author BREDEX GmbH
 */
public class UIPickerViewTester extends ComboBoxTester 
    implements IMobileWidgetsTester {
    /**
     * @return the <code>AbstractUIPickerAdapter</code>
     */
    private AbstractUIPickerAdapter getCBAdapter() {
        return (AbstractUIPickerAdapter) getComponent();
    }
    
    /** {@inheritDoc} */
    public void rcSwipe(String direction) {
        getCBAdapter().swipe(direction);
    }
    
    /**
     * Selects a value from the list of the picker
     * 
     * @param value
     *            the value to select
     * @param operator
     *            if regular expressions are used
     * @param searchType
     *            Determines where the search begins ("relative" or "absolute")
     * @param columnIdx
     *            the index of the column to use
     */
    public void rcSelectValue(String value, String operator,
        final String searchType, final int columnIdx) {
        verifyColumnIdxExists(columnIdx);
        selectValue(value, String.valueOf(VALUE_SEPARATOR), operator,
            searchType, columnIdx);
    }
    
    /**
     * Selects a value from the list of the picker
     * 
     * @param value
     *            the item which should be selected.
     * @param separator
     *            The separator if <code>text</code> is an enumeration of
     *            values. Not supported by this implementation class
     * @param operator
     *            if regular expressions are used
     * @param searchType
     *            Determines where the search begins ("relative" or "absolute")
     * @param columnIdx
     *            the index of the column to use
     */
    private void selectValue(String value, String separator,
        String operator, final String searchType, final int columnIdx) {
        verifyColumnIdxExists(columnIdx);
        int implColumnIdx = IndexConverter.toImplementationIndex(columnIdx);
        String[] comboValues = getCBAdapter().getValues(implColumnIdx);
        Validate.notNull(value, "text must not be null"); //$NON-NLS-1$
        
        int index = -1;

        for (int i = 0;
                i < comboValues.length; ++i) {
            String str = comboValues[i];
            if (MatchUtil.getInstance().match(str, value, operator)) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            throw new StepExecutionException("Text '" + value //$NON-NLS-1$
                + "' not found", //$NON-NLS-1$
                EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }

        getCBAdapter().select(index, implColumnIdx);
    }
    
    /**
     * Selects <code>index</code> in the picker.
     *
     * @param index
     *            The index to select
     * @param columnIdx
     *            the index of the column to use
     */
    public void rcSelectIndex(String index, final int columnIdx) {
        verifyColumnIdxExists(columnIdx);
        int implIdx = IndexConverter.toImplementationIndex(
                IndexConverter.intValue(index));
        int implColumnIdx = IndexConverter.toImplementationIndex(columnIdx);
        getCBAdapter().select(implIdx, implColumnIdx);
    }
    
    /**
     * Verifies if the picker has <code>index</code> selected.
     * 
     * @param index
     *            The index to verify
     * @param isSelected
     *            If the index should be selected or not.
     * @param columnIdx
     *            the index of the column to use
     */
    public void rcVerifySelectedIndex(String index, boolean isSelected,
        final int columnIdx) {
        verifyColumnIdxExists(columnIdx);
        int implIdx = IndexConverter.toImplementationIndex(IndexConverter
                .intValue(index));
        int implColumnIdx = IndexConverter.toImplementationIndex(columnIdx);
        int actual = getCBAdapter().getSelectedIndex(implColumnIdx);
        Verifier.equals(implIdx, actual, isSelected);
    }
    
    /**
     * Verifies if the picker contains an element that renders
     * <code>value</code>.
     * 
     * @param value
     *            The text to verify
     * @param operator
     *            The operator used to verify
     * @param exists
     *            If the value should exist or not.
     * @param columnIdx
     *            the index of the column to use
     */
    public void rcVerifyContainsValue(String value, String operator,
        boolean exists, final int columnIdx) {
        verifyColumnIdxExists(columnIdx);
        int implColumnIdx = IndexConverter.toImplementationIndex(columnIdx);
        final boolean contains = containsValue(value, operator,
                implColumnIdx);
        Verifier.equals(exists, contains);
    }
    
    /**
     * verify that the given column index exist
     * 
     * @param colIndex
     *            the column index to check
     */
    protected void verifyColumnIdxExists(final int colIndex) {
        boolean exists = (colIndex > 0)
                && (colIndex <= getCBAdapter().getNoOfColumns());

        if (!exists) {
            throw new StepExecutionException(
                    "The picker column index doesn't exist: " + colIndex, EventFactory //$NON-NLS-1$
                            .createActionError(TestErrorEvent.INVALID_INDEX));
        }
    }
    
    /**
     * @param value
     *            The value to check
     * @param operator
     *            The operator used to verify
     * @param columnIdx
     *            the index of the column to use
     * @return <code>true</code> if the combobox contains an element rendered
     *         with the passed value
    */
    private boolean containsValue(String value, String operator,
            int columnIdx) {
        String[] comboValues = getCBAdapter().getValues(columnIdx);
        for (int i = 0; i < comboValues.length; i++) {
            boolean contains = MatchUtil.getInstance()
                   .match(comboValues[i], value, operator);
            if (contains) {
                return contains;
            }
        }
        return false;
    }

}
