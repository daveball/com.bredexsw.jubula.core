package com.bredexsw.jubula.rc.mobile.ios.impl;

import org.eclipse.jubula.rc.common.tester.LabelTester;

import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.AbstractUIViewAdapter;
import com.bredexsw.jubula.rc.mobile.tester.interfaces.IMobileWidgetsTester;

/**
 * @author BREDEX GmbH
 */
public class UITextVerifiableTester extends LabelTester 
    implements IMobileWidgetsTester {
    /**
     * @return the <code>AbstractUIViewAdapter</code>
     */
    private AbstractUIViewAdapter getAdapter() {
        return (AbstractUIViewAdapter) getComponent();
    }
    
    /** {@inheritDoc} */
    public void rcSwipe(String direction) {
        getAdapter().swipe(direction);
    }
}
