package com.bredexsw.jubula.rc.mobile.ios.constants;

/**
 * Constants for native iOS RC communication
 */
public interface IOSComConst {
    /** iOS RC AUT class name*/
    public static final String CLASS_NAME_AUT = "AUT";
    
    /** iOS RC OMM class name*/
    public static final String CLASS_NAME_OMM = "OMM";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UIVIEW = "UIViewCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UIAPPLICATION =
         "UIApplicationCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_KIF = "KIFCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UICONTROL = "UIControlCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UIBUTTON = "UIButtonCAPs";

    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UISWITCH = "UISwitchCAPs";

    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UISEGMENTEDCONTROL = 
            "UISegmentedControlCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITABBAR = 
            "UITabBarCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITEXTFIELD = "UITextFieldCAPs";

    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITEXTINPUT = 
            "AbstractUITextInputCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITEXTVIEW = "UITextViewCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UILABEL = "UILabelCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UIPICKERVIEW = 
            "UIPickerViewCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITABBARBUTTON = 
            "UITabBarButtonCAPs";
    
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITABLEVIEWCELL = 
            "UITableViewCellCAPs";
   
    /** iOS RC class name*/
    public static final String CLASS_NAME_CAPS_UITABLEVIEW = 
            "UITableViewCAPs";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_BADGE_VALUE = 
        "badgeValue:atIndex:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_IS_SHOWING = "isShowing:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_IS_SELECTED = "isSelected:";

    /** method signature */
    public static final String METHOD_SIGNATURE_GET_TAB_COUNT = "getTabCount:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_TITLE_OF_TAB_AT_IDX = 
            "getTitleOfTab:atIndex:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_BOUNDS_OF_TAB_AT_IDX = 
            "getBoundsOfTab:atIndex:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_MAIN_SCREEN_BOUNDS = 
            "getMainScreenBounds";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TAP_VIEW_WITH_ACCESS_LABEL = 
            "tapViewWithAccessibilityLabel:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_IS_TAB_ENABLED_AT_IDX = 
            "isTabEnabled:atIndex:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_HAS_FOCUS = "hasFocus:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_IS_ENABLED = "isEnabled:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_IS_EDITABLE = "isEditable:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_TEXT = "getText:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_NO_OF_COLUMNS = 
            "getNoOfColumns:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_SELECTED_IDX_IN_COLUMN = 
            "getSelectedIndex:inColumn:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_SELECT_IDX_IN_COLUMN = 
            "select:index:inColumn:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_VALUES_IN_COLUMN = 
            "getValues:inColumn:";    
    
    /** method signature */
    public static final String METHOD_SIGNATURE_SWIPE = 
            "swipe:inDirection:";

    /** method signature */
    public static final String METHOD_SIGNATURE_GET_VALUES = "getValues:";

    /** method signature */
    public static final String METHOD_SIGNATURE_GET_SELECTED_VALUES = 
            "getSelectedValues:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_SELECTED_INDICES = 
            "getSelectedIndices:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_SELECTED_INDEX = 
            "getSelectedIndex:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TAP_ON_INDEX = 
            "tap:count:atIndex:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_SELECT_ALL = "selectAll:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TAP = 
            "tap:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TAP_AT = 
            "tap:x:y:width:height:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TAP_AT_POSITION = 
            "tap:x:xUnit:y:yUnit:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TAP_AT_POINT = 
            "tapScreenAtPointX:Y:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_TYPE_TEXT = "type:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_PRESS_DELETE = "pressDelete";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_SCREENSHOT = 
            "getScreenshot";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_IS_ALIVE = "isAlive:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_HIERARCHY = "getHierarchy";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_START_OMM = "start:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_STOP_OMM = "stop";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_HIGHLIGHT = "highlight:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_INPUT_TEXT = 
            "rcInputText:";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_GET_TAPPED_WIDGETS = 
            "getTappedUIWidgetIndices";
    
    /** method signature */
    public static final String METHOD_SIGNATURE_KEY_INPUT_POST_DELAY =
            "keyInputPostDelay:";
}
