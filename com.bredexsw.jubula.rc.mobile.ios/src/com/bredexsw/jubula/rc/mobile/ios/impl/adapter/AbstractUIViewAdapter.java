package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jubula.rc.common.driver.IRobotFactory;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.AbstractMenuTester;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.ITextComponent;
import org.eclipse.jubula.rc.common.util.MatchUtil;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;
import com.bredexsw.jubula.rc.mobile.tester.adapter.interfaces.IMobileWidgetsAdapter;

/**
 * @author BREDEX GmbH
 */
public abstract class AbstractUIViewAdapter implements IMobileWidgetsAdapter,
    ITextComponent {
    /**
     * the iOS object
     */
    private final AbstractIOSComponent m_component;
    
    /**
     * Constructor
     * 
     * @param comp
     *            the object to adapt
     */
    protected AbstractUIViewAdapter(AbstractIOSComponent comp) {
        m_component = comp;
    }
    
    /** {@inheritDoc} */
    public Object getRealComponent() {
        return getComponent();
    }

    /** {@inheritDoc} */
    public boolean isShowing() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIVIEW,
                IOSComConst.METHOD_SIGNATURE_IS_SHOWING,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }

    /** {@inheritDoc} */
    public boolean hasFocus() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIVIEW,
                IOSComConst.METHOD_SIGNATURE_HAS_FOCUS,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }

    /** {@inheritDoc} */
    public String getPropteryValue(String propertyname) {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /** {@inheritDoc} */
    public AbstractMenuTester showPopup(int xPos, String xUnits, int yPos,
            String yUnits, int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /** {@inheritDoc} */
    public AbstractMenuTester showPopup(int button) {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /** {@inheritDoc} */
    public void showToolTip(String text, int textSize, int timePerWord,
            int windowWidth) {
        StepExecutionException.throwUnsupportedAction();
    }

    /** {@inheritDoc} */
    public void rcDrag(int mouseButton, String modifier, int xPos,
            String xUnits, int yPos, String yUnits) {
        StepExecutionException.throwUnsupportedAction();
    }

    /** {@inheritDoc} */
    public void rcDrop(int xPos, String xUnits, int yPos, String yUnits,
            int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /** {@inheritDoc} */
    public int getKeyCode(String mod) {
        StepExecutionException.throwUnsupportedAction();
        return 0;
    }

    /** {@inheritDoc} */
    public IRobotFactory getRobotFactory() {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /**
     * @return the component
     */
    protected AbstractIOSComponent getComponent() {
        return m_component;
    }
    
    /** {@inheritDoc} */
    public String getText() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_TEXT,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getString();
    }
    
    /**
     * @param values
     *            the values to search for
     * @param operator
     *            the operator to use
     * @param listValues
     *            the values to search in
     * @return the indices of the matching values
     */
    protected Integer[] findIndicesOfValues(String[] values, String operator,
            String[] listValues) {
        final Set<Integer> indexSet = new HashSet<Integer>();
        for (int i = 0; i < listValues.length; i++) {
            if (MatchUtil.getInstance().match(
                    listValues[i], values, operator)) {
                indexSet.add(new Integer(i));
            }
        }

        Integer[] indices = new Integer[indexSet.size()];
        indexSet.toArray(indices);
        return indices;
    }
    
    /** {@inheritDoc} */
    public void swipe(String direction) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF,
                IOSComConst.METHOD_SIGNATURE_SWIPE,
                getComponent().getLocatorPath());
        message.addParameter(direction);
        IOSAUTServer.sendMessage(message);
    }
}
