/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.mobile.ios.model;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.components.AUTComponent;
import org.eclipse.jubula.rc.common.components.HierarchyContainer;


/**
 * specific Hierarchy Container for iOS
 * 
 * @author BREDEX GmbH
 * @created 23.11.2009
 *
 */
public class IOSHierarchyContainer extends HierarchyContainer {
    /**
     * @param component
     *            the AUT component
     */
    public IOSHierarchyContainer(AUTComponent component) {
        super(component);
    }

    /** {@inheritDoc} */
    public void add(HierarchyContainer component) {
        Validate.notNull(component);
        super.add(component);
        if (component.getPrnt() != this) {
            component.setPrnt(this);
        }
    }

    /** {@inheritDoc} */
    public AUTComponent getCompID() {
        return super.getCompID();
    }
    
    /**
     * @return IOSHierarchyContainer[]
     */
    public IOSHierarchyContainer[] getComponents() {
        if (super.getComps().length == 0) {
            return new IOSHierarchyContainer[0];
        }
        HierarchyContainer[] containerArray = super.getComps();
        IOSHierarchyContainer[] iosContainerArray = 
            new IOSHierarchyContainer[containerArray.length];
        for (int i = 0; i < containerArray.length; i++) {
            iosContainerArray[i] = 
                (IOSHierarchyContainer)containerArray[i]; 
        }
        return iosContainerArray;
    }
}
