package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IButtonComponent;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * The CAP implementation for iOS UIButton actions
 */
public class UIButtonAdapter extends AbstractUIControlAdapter implements
    IButtonComponent {
    /**
     * Constructor
     * @param objectToAdapt
     *            the object to adapt
     */
    public UIButtonAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }

    /** {@inheritDoc} */
    public String getText() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIBUTTON,
                IOSComConst.METHOD_SIGNATURE_GET_TEXT,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getString();
    }

    /** {@inheritDoc} */
    public boolean isSelected() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIBUTTON,
                IOSComConst.METHOD_SIGNATURE_IS_SELECTED,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }
}
