package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.ITextComponent;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public class UILabelAdapter extends AbstractUIViewAdapter implements
        ITextComponent {
    /**
     * @param label
     *            the component to adapt
     */
    public UILabelAdapter(AbstractIOSComponent label) {
        super(label);
    }

    /** {@inheritDoc} */
    public String getText() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UILABEL,
                IOSComConst.METHOD_SIGNATURE_GET_TEXT,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getString();
    }
    
    /** {@inheritDoc} */
    public boolean isEnabled() {
        NativeMessage message = new NativeMessage(
            IOSComConst.CLASS_NAME_CAPS_UILABEL,
            IOSComConst.METHOD_SIGNATURE_IS_ENABLED, 
            getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }
}
