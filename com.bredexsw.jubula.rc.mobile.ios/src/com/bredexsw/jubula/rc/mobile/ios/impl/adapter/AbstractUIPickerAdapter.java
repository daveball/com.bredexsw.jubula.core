package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IComboComponent;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public abstract class AbstractUIPickerAdapter extends AbstractUIViewAdapter 
    implements IComboComponent {
    /**
     * Constructor
     * @param objectToAdapt
     *            the object to adapt
     */
    public AbstractUIPickerAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }
    
    /** {@inheritDoc} */
    public boolean isEditable() {
        return false;
    }

    /** {@inheritDoc} */
    public void selectAll() {
        StepExecutionException.throwUnsupportedAction();
    }

    /** {@inheritDoc} */
    public void input(String text, boolean replace)
        throws StepExecutionException, IllegalArgumentException {
        StepExecutionException.throwUnsupportedAction();
    }
    
    /** {@inheritDoc} */
    public int getSelectedIndex() {
        return getSelectedIndex(0);
    }

    /** {@inheritDoc} */
    public void select(int index) {
        select(index, 0);
    }
        
    @Override
    /** {@inheritDoc} */
    public String getText() {
        return getTextOfAllColumns();
    }
    
    /**
     * @return the concatenated string of all picker columns
     */
    protected abstract String getTextOfAllColumns();

    /**
     * @return the number of available columns
     */
    public int getNoOfColumns() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIPICKERVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_NO_OF_COLUMNS,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getInt();
    }
    
    /**
     * {@inheritDoc}
     * 
     * @param columnIdx
     *            the index of the column to use
     */
    public int getSelectedIndex(int columnIdx) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIPICKERVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_SELECTED_IDX_IN_COLUMN,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(columnIdx));
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getInt();
    }

    /**
     * Selects the picker element with the passed index.
     * Error Description    Execution Error: "+[NSInvocation _invocationWithMethodSignature:frame:]: method signature argument cannot be nil"
     * @param index
     *            The index to select
     * @param columnIdx
     *            the index of the column to use
     */
    public void select(int index, int columnIdx) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIPICKERVIEW,
                IOSComConst.METHOD_SIGNATURE_SELECT_IDX_IN_COLUMN,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(index));
        message.addParameter(String.valueOf(columnIdx));
        IOSAUTServer.sendMessage(message);
    }
        
    /**
     * {@inheritDoc}
     */
    public String[] getValues(int columnIdx) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIPICKERVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_VALUES_IN_COLUMN,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(columnIdx));
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getStringArray();
    }
    
    /**
     * {@inheritDoc}
     */
    public String[] getValues() {
        return getValues(0);
    }
}
