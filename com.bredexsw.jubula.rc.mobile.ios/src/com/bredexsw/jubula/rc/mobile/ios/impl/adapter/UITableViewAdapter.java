package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IListComponent;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public class UITableViewAdapter extends AbstractUIViewAdapter implements
        IListComponent {
    /**
     * @param comp the component to adapt
     */
    public UITableViewAdapter(AbstractIOSComponent comp) {
        super(comp);
    }

    /** {@inheritDoc} */
    public String getText() {
        String[] selected = getSelectedValues();
        if (selected.length > 0) {
            return selected[0];
        }
        throw new StepExecutionException("No list item selected", //$NON-NLS-1$
            EventFactory.createActionError(TestErrorEvent.NO_SELECTION));
    }

    /** {@inheritDoc} */
    public boolean isEnabled() {
        return true;
    }

    /** {@inheritDoc} */
    public int[] getSelectedIndices() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UITABLEVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_SELECTED_INDICES,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getIntArray();
    }

    /** {@inheritDoc} */
    public void clickOnIndex(Integer i, ClickOptions co) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UITABLEVIEW,
                IOSComConst.METHOD_SIGNATURE_TAP_ON_INDEX,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(co.getClickCount()));
        message.addParameter(String.valueOf(i));
        IOSAUTServer.sendMessage(message);
    }

    /** {@inheritDoc} */
    public String[] getSelectedValues() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UITABLEVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_SELECTED_VALUES,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getStringArray();
    }
    
    /** {@inheritDoc} */
    public String[] getValues() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UITABLEVIEW,
                IOSComConst.METHOD_SIGNATURE_GET_VALUES,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        String[] listValues = answer.getStringArray();
        return listValues;
    }
}
