package com.bredexsw.jubula.rc.mobile.ios.components;

import ios.UIResponder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.components.AUTHierarchy;
import org.eclipse.jubula.rc.common.components.HierarchyContainer;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.messagehandling.MessageIDs;
import org.eclipse.jubula.tools.internal.objects.ComponentIdentifier;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.listener.IOSObjectMapping;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;
import com.bredexsw.jubula.rc.mobile.ios.model.IOSHierarchyContainer;

/**
 * The iOS AUT Hierarchy implementation
 */
public class IOSAUTHierarchy extends AUTHierarchy {
    /** keys used for JSON deserialization */
    private static final String JSON_ACCESSIBILITY_ID_KEY = "accessibilityIdentifier"; //$NON-NLS-1$
    /** keys used for JSON deserialization */
    private static final String JSON_CLASS_NAME_KEY = "className";
    /** keys used for JSON deserialization */
    private static final String JSON_SUPER_CLASSES_KEY = "superClasses";
    /** keys used for JSON deserialization */
    private static final String JSON_CHILDREN_KEY = "children";

    /** the logger */
    private static final Logger LOG = 
        LoggerFactory.getLogger(IOSAUTHierarchy.class);

    /** the bp to find components */
    private static FindIOSComponentsBP findBP = new FindIOSComponentsBP();
    
    /**
     * Constructor
     */
    public IOSAUTHierarchy() {
        // currently empty
    }

    /**
     * add the component to hierarchy
     * 
     * @param jsonObjects
     *            ALL serialized iOS components
     */
    private void add(String jsonObjects) {
        JSONArray iosComponents = JSONArray.fromObject(jsonObjects);
        
        for (int i = 0; i < iosComponents.size(); i++) {
            IOSHierarchyContainer topLevelContainer = 
                    createAndAddHierarchyContainer(
                            iosComponents.getJSONObject(i), 
                                StringConstants.SLASH + i);
            topLevelContainer.setName(
                    topLevelContainer.getCompID().getName() 
                    + StringConstants.UNDERSCORE
                    + i, true);
        }
    }
    
    /**
     * @param object
     *            the JSONObject the object to add recursively
     * @param indexPath
     *            the index path - this is used as a transient unique identifier
     * @return the hierarchy container for the given object
     */
    private IOSHierarchyContainer createAndAddHierarchyContainer(
        JSONObject object, String indexPath) {
        AbstractIOSComponent component = getSyntheticModelIOSComponet(object);
        component.setComp(indexPath);
        IOSHierarchyContainer container = new IOSHierarchyContainer(component);
        if (object.has(JSON_CHILDREN_KEY)) {
            JSONArray jsonArray = object.getJSONArray(JSON_CHILDREN_KEY);
            for (int i = 0; i < jsonArray.size(); i++) {
                IOSHierarchyContainer childHierarchyContainer = 
                        createAndAddHierarchyContainer(
                            jsonArray.getJSONObject(i), indexPath
                                    + StringConstants.SLASH + i);
                childHierarchyContainer.setName(
                        childHierarchyContainer.getCompID().getName() 
                        + StringConstants.UNDERSCORE
                        + i, true);
                container.add(childHierarchyContainer);
            }
        }
        addToHierachyMap(container);
        return container;
    }

    /** clean the complete hierarchy */
    public void clean () {
        getRealMap().clear();
        getHierarchyMap().clear();
    }

    /**
     * @param jo the serialized iOS component as a JSON object
     * @return a synthetic model iOS component instance 
     */
    protected AbstractIOSComponent getSyntheticModelIOSComponet(JSONObject jo) {
        Class<?> componentClass = getComponentClass(jo);
        try {
            Constructor<?> constructor = componentClass.getConstructor();
            Object componentObj = constructor.newInstance();
            if (componentObj instanceof AbstractIOSComponent) {
                AbstractIOSComponent wc = (AbstractIOSComponent) componentObj;
                String componentName;
                if (jo.has(JSON_ACCESSIBILITY_ID_KEY)) {
                    componentName = jo.getString(JSON_ACCESSIBILITY_ID_KEY);
                } else {
                    componentName = componentClass.getName();
                }
                wc.setName(componentName);
                wc.setComponentClassName(jo.getString(JSON_CLASS_NAME_KEY));
                return wc;
            }
        } catch (SecurityException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (NoSuchMethodException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (IllegalArgumentException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (InstantiationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (IllegalAccessException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (InvocationTargetException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    /**
     * this class determines the type of a serialized DOM object
     * 
     * @param jo
     *            the serialized web component (DOM component)
     * @return the fully qualified component class name for the given web
     *         component
     */
    protected Class<?> getComponentClass(JSONObject jo) {
        List<String> possibleObjectTypes = new ArrayList<String>();
        possibleObjectTypes.add(jo.getString(JSON_CLASS_NAME_KEY));
        if (jo.has(JSON_SUPER_CLASSES_KEY)) {
            JSONArray superClassnames = 
                    jo.getJSONArray(JSON_SUPER_CLASSES_KEY);
            for (int i = 0; i < superClassnames.size(); i++) {
                possibleObjectTypes.add(superClassnames.getString(i));
            }   
        }
        
        for (String possibleType : possibleObjectTypes) {
            try {
                return Class.forName(IOSObjectMapping.IOS_PSEUDO_CLASS_PACKAGE
                        + possibleType);
            } catch (ClassNotFoundException e) {
                // is ok - use fall back type for all unknown UIResponder
            }
        }
        return UIResponder.class;
    }

    /**
     * @param widgetIndexPath
     *            the fully qualified index PATH of the iOS component to get an identifier for
     * @return a component identifier for the given iOS component; may return
     *         null if no component identifier could be created
     */
    public IComponentIdentifier getComponentIdentifier(String widgetIndexPath) {
        AbstractIOSComponent comp = findComponent(widgetIndexPath);
        if (comp != null) {
            IComponentIdentifier compID = new ComponentIdentifier();
            compID.setComponentClassName(comp.getComponentClassName());
            compID.setSupportedClassName(comp.getClass().getName());
            compID.setHierarchyNames(getPathToRoot(comp));
            compID.setNeighbours(getComponentContext(comp));
            // try to get some descriptive text
            HierarchyContainer container = getHierarchyContainer(
                    widgetIndexPath);
            setAlternativeDisplayName(container, comp, compID);
            try {
                // initially run a find component to ensure hierarchy is up-to-date
                findComponent(compID);
                // now search in up-to-date hierarchy
                if (widgetIndexPath.equals(
                        findBP.findComponent(compID, this))) {
                    compID.setEqualOriginalFound(true);
                }
            } catch (ComponentNotFoundException e) {
                // ignore - is ok here
            }
            return compID;
        }
        return null;
    }

    /**
     * Returns the hierarchy container for <code>component</code>. 
     * @param component the component from the AUT, must no be null
     * @throws IllegalArgumentException if component is null
     * @return the hierarchy container or null if the component is not yet managed
     */
    public IOSHierarchyContainer getHierarchyContainer(String component) 
        throws IllegalArgumentException {
        
        Validate.notNull(component, "The component must not be null"); //$NON-NLS-1$
        IOSHierarchyContainer result = null;
        try {
            AbstractIOSComponent compID = (AbstractIOSComponent)getRealMap()
                    .get(component);
            if (compID != null) {
                result = (IOSHierarchyContainer)getHierarchyMap().get(compID);
            }
        } catch (ClassCastException cce) {
            LOG.error(cce.getLocalizedMessage(), cce);
        } catch (NullPointerException npe) {
            LOG.error(npe.getLocalizedMessage(), npe);
        }
        return result;
    }
    
    /**
     * recollect all technical components from the web-page
     */
    private void rebuildHierarchy() {
        clean();
        try {
            NativeAnswer answer;
            answer = IOSAUTServer.getInstance().getiOSCommunicator()
                .sendMsg(new NativeMessage(
                        IOSComConst.CLASS_NAME_AUT,
                        IOSComConst.METHOD_SIGNATURE_GET_HIERARCHY));
            String technicalComponents = answer.getString();
            add(technicalComponents);
        } catch (RemoteServerException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

    /**
     * @param component
     *            the component to search the path to the root
     * @return a string list
     */
    private List<String> getPathToRoot(AbstractIOSComponent component) {
        List<String> pathToRoot = new ArrayList<String>();
        HierarchyContainer hc = getHierarchyContainer(component);
        do {
            pathToRoot.add(hc.getName());
            hc = hc.getPrnt();
        } while (hc != null);
        Collections.reverse(pathToRoot);
        return pathToRoot;
    }

    /**
     * @param indexPath
     *            the indexPath to search for
     * @return the AbstractWebComponent from the AUT hierarchy
     */
    private AbstractIOSComponent findComponent(String indexPath) {
        Object o = getRealMap().get(indexPath);
        if (o == null) {
            rebuildHierarchy();
            o = getRealMap().get(indexPath);
        }
        if (o == null) {
            LOG.error("Even after rebuilding the hierarchy the indexPath: \"" + indexPath //$NON-NLS-1$
                    + "\" could not be found in the hierarchy."); //$NON-NLS-1$
        }
        return (AbstractIOSComponent)o;
    }
    
    /**
     * @param compId the compId
     * @return the web component
     */
    public AbstractIOSComponent findComponent(IComponentIdentifier compId)
        throws ComponentNotFoundException {
        rebuildHierarchy();
        String indexPath = (String)findBP.findComponent(compId, this);

        if (indexPath == null) {
            throw new ComponentNotFoundException(
                    "component not found: " + compId, //$NON-NLS-1$
                    MessageIDs.E_COMPONENT_NOT_FOUND);
        }
        return (AbstractIOSComponent)getRealMap().get(indexPath);
    }
    
    /**
     * {@inheritDoc}
     */
    protected List<String> getComponentContext(AbstractIOSComponent component, 
            List<String> context) {
        IOSHierarchyContainer hc = getHierarchyContainer(component);
        IOSHierarchyContainer parent = (IOSHierarchyContainer)hc.getPrnt();
        if (parent != null) {
            IOSHierarchyContainer[] comps = parent.getComponents();
            for (int i = 0; i < comps.length; i++) {
                AbstractIOSComponent child = (AbstractIOSComponent)
                    comps[i].getCompID();
                if (!child.equals(component)) {
                    final String childClassName = 
                            child.getClass().getName();
                    String toAdd = childClassName 
                        + StringConstants.UNDERSCORE + 1; 
                    while (context.contains(toAdd)) {
                        int lastCount = Integer.valueOf(
                            toAdd.substring(toAdd.lastIndexOf(
                                    StringConstants.UNDERSCORE) + 1)).
                                intValue(); 
                        toAdd = childClassName 
                            + StringConstants.UNDERSCORE 
                            + (lastCount + 1);
                    }
                    context.add(toAdd);
                }
            }
        }
        return context;
    }
    
    /**
     * Returns the hierarchy container for <code>component</code>.
     * @param component the component from the AUT, must no be null
     * @throws IllegalArgumentException if component is null
     * @return the hierarchy container or null if the component is not yet managed
     */
    private IOSHierarchyContainer getHierarchyContainer(
            AbstractIOSComponent component) 
        throws IllegalArgumentException {
        Validate.notNull(component, "The component must not be null"); //$NON-NLS-1$
        IOSHierarchyContainer result = null;
        try {
            result = (IOSHierarchyContainer)getHierarchyMap().get(component);
        } catch (ClassCastException cce) {
            LOG.error(cce.getLocalizedMessage(), cce);
        } catch (NullPointerException npe) {
            LOG.error(npe.getLocalizedMessage(), npe);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public IComponentIdentifier[] getAllComponentId() {
        // no need to implement for HTML
        return new ComponentIdentifier[1];
    }

    /**
     * {@inheritDoc}
     */
    protected List<String> getComponentContext(Object component) {
        List<String> context = new ArrayList<String>();
        if (component instanceof AbstractIOSComponent) {
            getComponentContext((AbstractIOSComponent)component, context);
        }
        return context;
    }
}
