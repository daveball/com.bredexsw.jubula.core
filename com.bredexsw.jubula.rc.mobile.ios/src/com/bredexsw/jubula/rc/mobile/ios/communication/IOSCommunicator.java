/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.mobile.ios.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.eclipse.jubula.tools.internal.utils.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Communication class between the Java-.RC Server and
 * the native RC Server.
 */
public class IOSCommunicator {
    /** <code>log</code>: the logger */
    private static Logger log = LoggerFactory.getLogger(IOSCommunicator.class);
    /** 
     * ID for timeout in communication with native RC.
     * This constant is also defined on the native RC side
     * (c.b.g.rc.XYZ.nativ.impl.ServerExceptionConstants)
     */
    private static final int TIMEOUT_ERROR_ID = 12;
    /** 
     * Name for timeout in communication with native RC.
     * This constant is also defined on the native RC side
     * (c.b.g.rc.XYZ.nativ.impl.ServerExceptionConstants)
     */
    private static final String TIMEOUT_ERROR_NAME = "TIMEOUT";
    
    /** the host to connect to */
    private final String m_hostname;
    /** the port. */
    private int m_port;
    /** the communication socket. */
    private Socket m_socket;
    /** the print writer. */
    private PrintWriter m_writer;
    /** the buffered reader. */
    private BufferedReader m_br;

    
    /**
     * Constructor
     * 
     * @param hostname
     *            the host to connect to
     * @param port
     *            the TCP port.
     */
    public IOSCommunicator(String hostname, int port) {
        m_hostname = hostname;
        m_port = port;
    }
    
    /**
     * Method to initialize the connection to the native RC part
     */
    public void initialize() throws IOException {
        m_socket = new Socket(m_hostname, m_port);
        m_writer = new PrintWriter(new OutputStreamWriter(
                m_socket.getOutputStream(), "UTF8"), true);
        m_br = new BufferedReader(new InputStreamReader(
                m_socket.getInputStream(), "UTF8"));
    }

    /**
     * Sends a message to the native RC-Server and returns the server answer.
     * Contains a 30sec. timeout implementation for the answer.
     * 
     * @param msgObj the message to send
     * @return return the server answer
     * @throws RemoteServerException 
     * 
     */
    public synchronized NativeAnswer sendMsg(NativeMessage msgObj)
        throws RemoteServerException {
        final StringBuilder answer = new StringBuilder();
        // Thread waiting for the server answer
        final Thread answerThread = new Thread(new Runnable() { 
            public void run() {
                try {
                    answer.append(m_br.readLine());
                } catch (IOException e) {
                    log.error("IOException during waiting for server answer", e); //$NON-NLS-1$
                }
            }
        });
        answerThread.start();

        // Sending and serializing the MessageObj via JSON to native iOS     
        JSON json = JSONSerializer.toJSON(msgObj);
        m_writer.println(json.toString());
        m_writer.flush();

        //timeout - waiting 12 seconds for server answer
        int waittime = 0;
        while (waittime < 2400) {
            
            if (!answerThread.isAlive()) {
                if (answer.length() == 0) {
                    return new NativeAnswer("Server Closed");
                }
                JSONObject jsonAnswer = (JSONObject) JSONSerializer.
                        toJSON(answer.toString());
                boolean exceptionFlag = jsonAnswer.
                        getBoolean("exceptionFlag");
                
                if (!exceptionFlag) {
                    String responeString = jsonAnswer.
                            getString("methodReturnString");
                    return new NativeAnswer(responeString);
                } 
                int id = jsonAnswer.getInt("exceptionID");
                String name = jsonAnswer.getString("exceptionName");
                String msg = jsonAnswer.getString("exceptionMsg");
                String methodname = jsonAnswer.
                        getString("exceptionMethod");
                String classname = jsonAnswer.
                        getString("exceptionClass");
                
                NativeAnswer rmo = new NativeAnswer(id, name,
                        msg, methodname, classname);
                log.error("Server Exception occured: ", 
                        rmo.getServerException());
                throw rmo.getServerException();
            } 
            waittime++;
            TimeUtil.delay(10);
        }
        //timeout occurred
        log.error("Timeout error during waiting for server answer"); //$NON-NLS-1$
        answerThread.interrupt();
        throw new RemoteServerException(TIMEOUT_ERROR_ID, TIMEOUT_ERROR_NAME, 
                "Timeout occurred during communication with native native RC RC",  //$NON-NLS-1$
                "sendMsg", IOSCommunicator.class.getName()); //$NON-NLS-1$
    }
}