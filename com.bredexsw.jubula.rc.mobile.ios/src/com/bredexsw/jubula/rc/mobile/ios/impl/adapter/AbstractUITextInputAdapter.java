package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.ITextInputComponent;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public abstract class AbstractUITextInputAdapter 
    extends  AbstractUIControlAdapter 
    implements ITextInputComponent {
    /**
     * @param comp
     *            the component to adapt
     */
    public AbstractUITextInputAdapter(AbstractIOSComponent comp) {
        super(comp);
    }

    /** {@inheritDoc} */
    public void setSelection(int start) {
        StepExecutionException.throwUnsupportedAction();
    }

    /** {@inheritDoc} */
    public void setSelection(int start, int end) {
        StepExecutionException.throwUnsupportedAction();
    }

    /** {@inheritDoc} */
    public String getSelectionText() {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /** {@inheritDoc} */
    public void selectAll() {
        NativeMessage message = new NativeMessage(
            IOSComConst.CLASS_NAME_CAPS_UITEXTINPUT,
            IOSComConst.METHOD_SIGNATURE_SELECT_ALL,
            getComponent().getLocatorPath());
        IOSAUTServer.sendMessage(message);
    }

}