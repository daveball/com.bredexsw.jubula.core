package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public abstract class AbstractUIControlAdapter extends AbstractUIViewAdapter {

    /**
     * @param comp
     *            the component to adapt
     */
    protected AbstractUIControlAdapter(AbstractIOSComponent comp) {
        super(comp);
    }

    /** {@inheritDoc} */
    public boolean isEnabled() {
        return isEnabled(getComponent());
    }

    /**
     * @param component
     *            the component
     * @return whether the component is enabled or not
     */
    public static boolean isEnabled(final AbstractIOSComponent component) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UICONTROL,
                IOSComConst.METHOD_SIGNATURE_IS_ENABLED,
                component.getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }
}
