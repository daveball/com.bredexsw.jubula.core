/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.mobile.ios.components;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.json.JSONArray;

import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.tools.internal.messagehandling.MessageIDs;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;


/**
 * Helper class to handle technical components
 * 
 * @author BREDEX GmbH
 *
 */
public class TechnicalComponentHandler {
    /** <code>instance</code> singleton */
    private static TechnicalComponentHandler instance = null;    
    /**  <code>log</code>: the logger */
    private static Logger log = 
        LoggerFactory.getLogger(TechnicalComponentHandler.class);
    
    /** the hierarchy of the AUT */
    private static IOSAUTHierarchy autHierarchy = new IOSAUTHierarchy();
    
    /** Hide Constructor */
    private TechnicalComponentHandler() {
    // hide
    }

    /** @return the singleton instance */
    public static TechnicalComponentHandler getInstance() {
        if (instance == null) {
            instance = new TechnicalComponentHandler();
        }
        return instance;
    }
    
    /**
     * Investigates the given <code>component</code> for an identifier. It must
     * be distinct for the whole AUT.
     * 
     * @param jSONComponents
     *            the JSON components to get an identifier for
     * @return a list of identifier, containing the identification
     */
    public Collection<IComponentIdentifier> 
            getIdentifier(String jSONComponents) {
        JSONArray jsonArrays = JSONArray.fromObject(jSONComponents);
        Collection<IComponentIdentifier> a = 
            new ArrayList<IComponentIdentifier>();
        for (Object o : jsonArrays) {
            if (o instanceof JSONArray) {
                JSONArray ja = (JSONArray)o;
                StringBuilder indexPath = new StringBuilder();
                for (int i = (ja.size() - 1); i >= 0; i--) {
                    indexPath.append(AbstractIOSComponent.SEPARATOR_CHAR);
                    indexPath.append(ja.get(i));
                }
                IComponentIdentifier ci = autHierarchy
                        .getComponentIdentifier(indexPath.toString());
                if (ci != null) {
                    a.add(ci);
                }
            }
        }
        return a;
    }

    
    /**
     * @param compId The component id for which to find the web component.
     * @param timeout
     *      timeout for retries, in milliseconds
     * @return The web component best matching the given id, or 
     *         <code>null</code> if no such component could be found within
     *         the given time.
     */
    public AbstractIOSComponent findComponent(IComponentIdentifier compId,
            int timeout) throws ComponentNotFoundException,
            IllegalArgumentException {
        long start = System.currentTimeMillis();
        AbstractIOSComponent iosWidget = null;
        boolean timoutExpired = false;
        do {
            try {
                iosWidget = autHierarchy.findComponent(compId);
                TimeUtil.delay(100);

                timoutExpired = System.currentTimeMillis() - start > timeout;
                if (timoutExpired && iosWidget == null) {
                    iosWidget = autHierarchy.findComponent(compId);
                }
            } catch (ComponentNotFoundException cnf) {
                // ignore during search
            } catch (IllegalArgumentException iae) {
                log.error(iae.getLocalizedMessage(), iae);
                throw iae;
            } finally {
                timoutExpired = System.currentTimeMillis() - start > timeout;
            }
        } while (iosWidget == null && !timoutExpired);

        if (iosWidget == null) {
            throw new ComponentNotFoundException(
                    "component not found: " + compId, //$NON-NLS-1$
                    MessageIDs.E_COMPONENT_NOT_FOUND);
        }
        return iosWidget;
    }
    
    /**
     * clean the hierarchy
     */
    public void cleanHierarchy() {
        autHierarchy.clean();
    }
}
