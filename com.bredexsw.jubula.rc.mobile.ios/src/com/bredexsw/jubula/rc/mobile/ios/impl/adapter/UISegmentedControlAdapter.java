package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * The CAP implementation for iOS UISegmentedControl actions
 */
public class UISegmentedControlAdapter extends AbstractUITabControlAdapter {
    /**
     * Constructor
     * @param objectToAdapt
     *            the object to adapt
     */
    public UISegmentedControlAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }

    /** {@inheritDoc} */
    public boolean isEnabled() {
        return AbstractUIControlAdapter.isEnabled(getComponent());
    }

    /** {@inheritDoc} */
    protected String getNativeRCClassName() {
        return IOSComConst.CLASS_NAME_CAPS_UISEGMENTEDCONTROL;
    }
}
