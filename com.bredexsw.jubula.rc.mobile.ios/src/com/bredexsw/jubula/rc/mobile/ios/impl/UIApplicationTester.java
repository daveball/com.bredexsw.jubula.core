/**
 * 
 */
package com.bredexsw.jubula.rc.mobile.ios.impl;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.logger.AutServerLogger;
import org.eclipse.jubula.rc.common.tester.AbstractApplicationTester;
import org.eclipse.jubula.rc.common.util.PointUtil;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;

/**
 * @author BREDEX GmbH
 */
public class UIApplicationTester extends AbstractApplicationTester {

    /**
     * The logging.
     */
    private static AutServerLogger log = 
        new AutServerLogger(AbstractApplicationTester.class);

    /** {@inheritDoc} */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public Rectangle getActiveWindowBounds() {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /** {@inheritDoc} */
    @Override
    protected IRobot getRobot() {
        return IOSAUTServer.getInstance().getRobot();
    }

    /** {@inheritDoc} */
    @Override
    public void rcKeyStroke(String modifierSpec, String keySpec) {
        StepExecutionException.throwUnsupportedAction();
    }
    
    @Override
    public void rcInputText(String text) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UIAPPLICATION, 
                IOSComConst.METHOD_SIGNATURE_INPUT_TEXT);
        message.addParameter(text);
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        Boolean isSuccess = answer.getBoolean();
        if (!isSuccess) {
            throw new StepExecutionException(
                    TestErrorEvent.ID.ACTION_ERROR,
                    EventFactory.createActionError("Component for text"
                             + " input is not selected"));
        }
    }

    /** {@inheritDoc} */
    @Override
    protected Object getFocusOwner() {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /** {@inheritDoc} */
    @Override
    protected int getEventCode(int key) {
        StepExecutionException.throwUnsupportedAction();
        return 0;
    }

    /** {@inheritDoc} */
    @Override
    protected Object getActiveWindow() {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }
    
    
    /** {@inheritDoc} */
    public void rcClickDirect(int count, int button, int xPos, String xUnits,
            int yPos, String yUnits) throws StepExecutionException {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_AUT,
                IOSComConst.METHOD_SIGNATURE_GET_MAIN_SCREEN_BOUNDS);
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        Rectangle mainBounds = PointUtil.stringAsRectangle(answer.getString());

        Point pointToTap = PointUtil.calculateAwtPointToGo(xPos,
                xUnits.equalsIgnoreCase(ValueSets.Unit.pixel.rcValue()),
                yPos,
                yUnits.equalsIgnoreCase(ValueSets.Unit.pixel.rcValue()),
                mainBounds);
        
        message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF,
                IOSComConst.METHOD_SIGNATURE_TAP_AT_POINT);
        message.addParameter(String.valueOf(pointToTap.x));
        message.addParameter(String.valueOf(pointToTap.y));
        for (int i = 0; i < count; i++) {
            IOSAUTServer.sendMessage(message);
        }
    }
    
    /**
     * tap view with accessibility label
     * 
     * @param accessibilityLabel
     *            the label to use
     * @throws StepExecutionException
     *             in case of errors
     */
    public void rcTapViewWithAccessibilityLabel(final String accessibilityLabel)
        throws StepExecutionException {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF,
                IOSComConst.METHOD_SIGNATURE_TAP_VIEW_WITH_ACCESS_LABEL);
        message.addParameter(accessibilityLabel);
        IOSAUTServer.sendMessage(message);
    }
     
    @Override
    public void takeScreenshot(Rectangle captureRect, double scaleFactor,
           File outputFile) {

        File out = outputFile;
        try {    
            BufferedImage image = getRobot().createFullScreenCapture();
            int scaledWidth = (int) Math.floor(image.getWidth() * scaleFactor);
            int scaledHeight = 
                (int) Math.floor(image.getHeight() * scaleFactor);
            BufferedImage imageOut = 
                new BufferedImage(scaledWidth,
                    scaledHeight, BufferedImage.TYPE_INT_RGB);
            // Scale it to the new size on-the-fly
            Graphics2D graphics2D = imageOut.createGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);
            graphics2D.drawImage(image, 0, 0, scaledWidth, scaledHeight, null);

            // Save captured image using given format, if supported.
            String extension = getExtension(out.getName());
            if (extension.length() == 0
                    || !ImageIO.getImageWritersBySuffix(extension).hasNext()
                    || !ImageIO.write(imageOut, extension, out)) {
                
                // Otherwise, save using default format
                out = new File(outputFile.getPath() 
                        + EXTENSION_SEPARATOR + DEFAULT_IMAGE_FORMAT);
                if (!ImageIO.write(imageOut, DEFAULT_IMAGE_FORMAT, out)) {
                    
                    // This should never happen, so log as error if it does.
                    // In this situation, the screenshot will not be saved, but
                    // the test step will still be marked as successful.
                    log.error("Screenshot could not be saved. " + //$NON-NLS-1$
                            "Default image format (" + DEFAULT_IMAGE_FORMAT  //$NON-NLS-1$
                            + ") is not supported."); //$NON-NLS-1$
                }
            }
            
        } catch (IOException e) {
            throw new StepExecutionException(
                    "Screenshot could not be saved", //$NON-NLS-1$
                    EventFactory.createActionError(
                            TestErrorEvent.FILE_IO_ERROR));
        }

    }
}
