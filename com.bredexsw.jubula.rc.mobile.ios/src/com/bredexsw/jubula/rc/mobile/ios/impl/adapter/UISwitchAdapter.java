package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IButtonComponent;
import org.eclipse.jubula.tools.internal.constants.StringConstants;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * The CAP implementation for iOS UISwitch actions
 */
public class UISwitchAdapter extends AbstractUIControlAdapter implements
    IButtonComponent {
    /**
     * Constructor
     * @param objectToAdapt
     *            the object to adapt
     */
    public UISwitchAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }

    /** {@inheritDoc} */
    public String getText() {
        StepExecutionException.throwUnsupportedAction();
        return StringConstants.EMPTY;
    }

    /** {@inheritDoc} */
    public boolean isSelected() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UISWITCH,
                IOSComConst.METHOD_SIGNATURE_IS_SELECTED,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }
}
