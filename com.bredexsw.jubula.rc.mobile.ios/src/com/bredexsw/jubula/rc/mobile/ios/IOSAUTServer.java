package com.bredexsw.jubula.rc.mobile.ios;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.jubula.communication.internal.Communicator;
import org.eclipse.jubula.communication.internal.message.AUTServerStateMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.rc.common.adaptable.AdapterFactoryRegistry;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.driver.RobotTiming;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.listener.BaseAUTListener;
import org.eclipse.jubula.rc.common.listener.DisabledCheckListener;
import org.eclipse.jubula.rc.common.listener.DisabledRecordListener;
import org.eclipse.jubula.rc.common.registration.IRegisterAut;
import org.eclipse.jubula.tools.internal.constants.AUTServerExitConstants;
import org.eclipse.jubula.tools.internal.exception.CommunicationException;
import org.eclipse.jubula.tools.internal.exception.JBVersionException;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;
import com.bredexsw.jubula.rc.mobile.ios.communication.IOSCommunicator;
import com.bredexsw.jubula.rc.mobile.ios.components.IOSRobot;
import com.bredexsw.jubula.rc.mobile.ios.components.TechnicalComponentHandler;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.listener.IOSObjectMapping;

/**
 * @author BREDEX GmbH
 */
public class IOSAUTServer extends AUTServer {
    /** <code>instance</code>: the singleton instance */
    private static IOSAUTServer instance = null;
    /** <code>log</code>: the logger */
    private static Logger log = LoggerFactory.getLogger(IOSAUTServer.class);
    /** Delay between key input */
    private static final int KEY_INPUT_POST_DELAY =
            RobotTiming.getKeyInputPostDelay();
    /** the device's host name the AUT runs on */
    private String m_deviceHostName = null;
    /** the device's port the AUT runs on */
    private int m_devicePort = -1;
    /** the communicator */
    private IOSCommunicator m_communicator = null;
    /** watchdog thread to monitor native iOS AUT status */
    private IOSNativWatchdog m_watchdog;
    /** the robot to use */
    private IRobot m_robot;
    
    /** Constructor */
    protected IOSAUTServer() {
        super(new IOSObjectMapping(), 
                new DisabledRecordListener(), 
                new DisabledCheckListener());
    }
    
    /**
     * the Watchdog thread of the native iOS AUT
     */
    private class IOSNativWatchdog extends Thread {
        /** the communicator to use */
        private final IOSCommunicator m_com;

        /**
         * Constructor
         * 
         * @param communicator
         *            the communicator to use
         */
        public IOSNativWatchdog(IOSCommunicator communicator) {
            m_com = communicator;
        }
        
        /** {@inheritDoc} */
        public void run() {
            try {
                while (!isInterrupted() && isConnectedAndAlive()) {
                    TimeUtil.delay(1000);
                }
            } finally {
                shutdown();
            }
        }

        /**
         * @return whether the communicator is still connected and the AUT still
         *         alive
         */
        private boolean isConnectedAndAlive() {
            boolean alive = false;
            
            if (m_com != null) {
                String cT = String.valueOf(System.currentTimeMillis());
                NativeMessage isAlive = new NativeMessage(
                        IOSComConst.CLASS_NAME_AUT,
                        IOSComConst.METHOD_SIGNATURE_IS_ALIVE);
                isAlive.addParameter(cT);
                try {
                    NativeAnswer answer = m_com.sendMsg(isAlive);
                    if (answer != null) {
                        String rCT = answer.getString();
                        alive = cT.equals(rCT);
                    }
                } catch (RemoteServerException e) {
                    log.error(e.getLocalizedMessage(), e);
                }
            }
            
            return alive;
        }
    }
        
    /**
     * Method to get the single instance of this class.
     * 
     * @return the instance of this Singleton
     */
    public static IOSAUTServer getInstance() {
        if (instance == null) {
            instance = new IOSAUTServer();
            setInstance(instance);
        }

        return instance;
    }
    
    /**
     * @param args
     *            the main arguments
     */
    public static void main(String[] args) {
        validateAndLogMainArgsCount(args, 5);
        IOSAUTServer.getInstance().setArgs(args);
        IOSAUTServer.getInstance().start(false);
    }
    
    /**
     * @param args
     *            the arguments to set
     */
    public void setArgs(String args[]) {
        // this sequence is determined by the StartIOSAutServerCommand class
        int pos = 1;
        setDeviceHostName(args[pos++]);
        setDevicePort(Integer.valueOf(args[pos++]));
        setAutAgentHost(args[pos++]);
        setAutAgentPort(args[pos++]);
        setAutID(args[pos++]);
    }
    
    /**
     * {@inheritDoc}
     */
    public void start(boolean isRcpAccessible) {
        try {
            IRegisterAut autReg = parseAutReg();
            if (autReg == null) {
                String errorMessage = "Unable to initialize connection to AUT Agent: No connection information provided."; //$NON-NLS-1$
                log.error(errorMessage);
                sendExitReason(errorMessage, 
                        AUTServerExitConstants.EXIT_MISSING_AGENT_INFO);
            } else {
                try {
                    AdapterFactoryRegistry.initRegistration();
                    setiOSCommunicator(new IOSCommunicator(
                            getDeviceHostName(), 
                            getDevicePort()));
                    getiOSCommunicator().initialize();
                    
                    m_robot = new IOSRobot();
                    
                    m_watchdog = new IOSNativWatchdog(getiOSCommunicator());
                    m_watchdog.setName("Watchdog for native iOS App"); //$NON-NLS-1$
                    m_watchdog.start();

                    autReg.register();
                    setInputKeyPostDelayInAUT();
                } catch (IOException ioe) {
                    log.error("Exception during AUT device communication and registration", ioe); //$NON-NLS-1$
                    System.exit(AUTServerExitConstants.AUT_START_ERROR);
                }
            }
        } catch (IllegalArgumentException iae) {
            log.error("Exception in start()", iae); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.EXIT_INVALID_ARGS);
        } catch (SecurityException se) {
            log.error("Exception in start()", se); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.
                    EXIT_SECURITY_VIOLATION_REFLECTION);
        } catch (UnsupportedClassVersionError ucve) {
            log.error("Exception in start()", ucve); //$NON-NLS-1$
            try {
                getCommunicator().send(new AUTServerStateMessage(
                    AUTServerStateMessage.EXIT_AUT_WRONG_CLASS_VERSION,
                        ucve.getMessage()));
            } catch (CommunicationException ce) {
                log.error("Exception in start()", ce); //$NON-NLS-1$
            }
            System.exit(AUTServerExitConstants.EXIT_AUT_WRONG_CLASS_VERSION);
        } catch (JBVersionException ve) {
            log.error("Exception in start()", ve); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.EXIT_UNKNOWN_ITE_CLIENT);
        }
    }
    
    /** {@inheritDoc} */
    protected void addToolkitEventListener(BaseAUTListener listener) {
        if (listener instanceof IOSObjectMapping) {
            IOSObjectMapping mappingListener = 
                    (IOSObjectMapping) listener;
            mappingListener.start();
        }
    }

    /** {@inheritDoc} */
    protected void removeToolkitEventListener(BaseAUTListener listener) {
        if (listener instanceof IOSObjectMapping) {
            IOSObjectMapping mappingListener = 
                    (IOSObjectMapping) listener;
            mappingListener.stop();
        }
    }

    /** {@inheritDoc} */
    protected void startTasks() throws ExceptionInInitializerError,
            InvocationTargetException, NoSuchMethodException {
        // empty
    }

    /** {@inheritDoc} */
    public IRobot getRobot() {
        return m_robot;
    }

    /**
     * @return the deviceHostName
     */
    private String getDeviceHostName() {
        return m_deviceHostName;
    }

    /**
     * @param deviceHostName the deviceHostName to set
     */
    private void setDeviceHostName(String deviceHostName) {
        m_deviceHostName = deviceHostName;
    }

    /**
     * @return the devicePort
     */
    private int getDevicePort() {
        return m_devicePort;
    }

    /**
     * @param devicePort the devicePort to set
     */
    private void setDevicePort(int devicePort) {
        m_devicePort = devicePort;
    }
    
    @Override
    public void shutdown() {
        super.shutdown();
        Communicator agentCommunicator = getServerCommunicator();
        if (agentCommunicator != null) {
            agentCommunicator.clearListeners();
            agentCommunicator.close();
        }
        if (m_watchdog != null) {
            m_watchdog.interrupt();
        }
    }

    /**
     * @return the communicator
     */
    public IOSCommunicator getiOSCommunicator() {
        return m_communicator;
    }

    /**
     * @param communicator the communicator to set
     */
    private void setiOSCommunicator(IOSCommunicator communicator) {
        m_communicator = communicator;
    }
    
//    @Override
//    public void setMode(int newMode) {
//        if (newMode == ChangeAUTModeMessage.TESTING) {
//            this.setInputKeyPostDelayInAUT();
//        }
//        super.setMode(newMode);
//    }
//    
    /**
     * Change the input key delay in AUT with environment 
     * variable TEST_KEY_INPUT_POST_DELAY
     */
    private void setInputKeyPostDelayInAUT() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_KIF, 
                IOSComConst.METHOD_SIGNATURE_KEY_INPUT_POST_DELAY);
        message.addParameter(KEY_INPUT_POST_DELAY);
        IOSAUTServer.sendMessage(message);
    }
    
    /**
     * @param message
     *            the message to send to native iOS side
     * @return the answer - may be null in case of remote server problems
     * @throws StepExecutionException
     *             in case of remote RC server problems
     */
    public static NativeAnswer sendMessage(NativeMessage message) 
        throws StepExecutionException {
        try {
            return getInstance().getiOSCommunicator().sendMsg(message);
        } catch (RemoteServerException e) {
            String errorMessage = e.getMessage();
            log.error(errorMessage, e);
            throw new StepExecutionException(errorMessage,
                    EventFactory.createActionError(TestErrorEvent
                        .EXECUTION_ERROR, new String[] {errorMessage}));
        }
    }
    
    /** {@inheritDoc} */
    public Object findComponent(IComponentIdentifier ci, int timeout) 
        throws ComponentNotFoundException, IllegalArgumentException {
        return TechnicalComponentHandler
                    .getInstance().findComponent(ci, timeout);
    }
}