package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public class UINavigationItemViewAdapter extends AbstractUIViewAdapter {
    /**
     * @param view
     *            the component to adapt
     */
    public UINavigationItemViewAdapter(AbstractIOSComponent view) {
        super(view);
    }

    /** {@inheritDoc} */
    public boolean isEnabled() {
        return true;
    }
}
