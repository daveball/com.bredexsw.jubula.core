package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.ITabbedComponent;
import org.eclipse.jubula.rc.common.util.PointUtil;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * The base CAP implementation for iOS UITabControl actions
 */
public abstract class AbstractUITabControlAdapter 
    extends AbstractUIViewAdapter implements ITabbedComponent {
    /**
     * Constructor
     * @param objectToAdapt
     *            the object to adapt
     */
    public AbstractUITabControlAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }

    /**
     * @return the name of the native class name
     */
    protected abstract String getNativeRCClassName();
    
    /** {@inheritDoc} */
    public int getTabCount() {
        NativeMessage message = new NativeMessage(
                getNativeRCClassName(),
                IOSComConst.METHOD_SIGNATURE_GET_TAB_COUNT,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getInt();
    }

    /** {@inheritDoc} */
    public String getTitleofTab(int index) {
        NativeMessage message = new NativeMessage(
                getNativeRCClassName(),
                IOSComConst.METHOD_SIGNATURE_GET_TITLE_OF_TAB_AT_IDX,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(index));
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getString();
    }

    /** {@inheritDoc} */
    public Object getBoundsAt(int index) {
        NativeMessage message = new NativeMessage(
                getNativeRCClassName(),
                IOSComConst.METHOD_SIGNATURE_GET_BOUNDS_OF_TAB_AT_IDX,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(index));
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return PointUtil.stringAsRectangle(answer.getString());
    }
    
    /** {@inheritDoc} */
    public boolean isEnabledAt(int index) {
        NativeMessage message = new NativeMessage(
                getNativeRCClassName(),
                IOSComConst.METHOD_SIGNATURE_IS_TAB_ENABLED_AT_IDX,
                getComponent().getLocatorPath());
        message.addParameter(String.valueOf(index));
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getBoolean();
    }
    
    /** {@inheritDoc} */
    public int getSelectedIndex() {
        NativeMessage message = new NativeMessage(
                getNativeRCClassName(),
                IOSComConst.METHOD_SIGNATURE_GET_SELECTED_INDEX,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getInt();
    }
}
