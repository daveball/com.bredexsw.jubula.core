package com.bredexsw.jubula.rc.mobile.ios.impl;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.TabbedPaneTester;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITabBarAdapter;
import com.bredexsw.jubula.rc.mobile.tester.interfaces.IMobileWidgetsTester;

/**
 * @author BREDEX GmbH
 */
public class UITabComponentTester extends TabbedPaneTester 
    implements IMobileWidgetsTester {
    /**
     * @return the <code>AbstractUIViewAdapter</code>
     */
    private UITabBarAdapter getAdapter() {
        return (UITabBarAdapter) getComponent();
    }
    
    /** {@inheritDoc} */
    public void rcSwipe(String direction) {
        getAdapter().swipe(direction);
    }
    
    /**
     * @param text
     *            the text to check
     * @param tabIndex
     *            the index to check
     *            
     * @param operator operator
     */
    public void rcCheckBadgeValue(String text, Integer tabIndex, 
             String operator) {
        verifyTabIndexExists(tabIndex);
        int implIdx = IndexConverter.toImplementationIndex(tabIndex);
        String badgeValue = getAdapter().getBadgeValue(implIdx);
        if (badgeValue.equals("-1")) {
            throw new StepExecutionException("Badge value doesn't exist",
                EventFactory.createActionError("Badge value doesn't exist"));
        }
        Verifier.match(badgeValue, text, operator);
    }
    
    /**
     * 
     * @param tabIndex the index to check
     */
    protected void verifyTabIndexExists(Integer tabIndex) {
        int tabCount = getAdapter().getTabCount();
        boolean exists = (tabIndex > 0)
                && (tabIndex <= tabCount);

        if (!exists) {
            throw new StepExecutionException(
                    "The tab index doesn't exist: " + tabIndex, EventFactory //$NON-NLS-1$
                            .createActionError(TestErrorEvent.INVALID_INDEX));
        }
    }
    
}
