package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * @author BREDEX GmbH
 */
public class UITextFieldAdapter extends AbstractUITextInputAdapter {
    /**
     * Constructor
     * 
     * @param objectToAdapt
     *            the object to adapt
     */
    public UITextFieldAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }

    /** {@inheritDoc} */
    public String getText() {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UITEXTFIELD,
                IOSComConst.METHOD_SIGNATURE_GET_TEXT,
                getComponent().getLocatorPath());
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getString();
    }

    /** {@inheritDoc} */
    public boolean isEditable() {
        return true;
    }
}
