/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.mobile.ios.model;

import org.eclipse.jubula.rc.common.components.AUTComponent;

/**
 * Base class for our representation of iOS UI widgets.
 * 
 * @author BREDEX GmbH
 */
public class AbstractIOSComponent extends AUTComponent {
    /** the index path separator */
    public static final char SEPARATOR_CHAR = '/';
    /** the actual component class name on native iOS side */
    private String m_componentClassName;
    
    /**
     * Constructor
     */
    public AbstractIOSComponent() {
        super(new Object());
    }

    /**{@inheritDoc} */
    public void setComp(Object comp) {
        super.setComponent(comp);
    }
    
    /**
     * @return the locator index path
     */
    public String getLocatorPath() {
        return (String) getComponent();
    }

    /**
     * @return the componentClassName
     */
    public String getComponentClassName() {
        return m_componentClassName;
    }

    /**
     * @param componentClassName the componentClassName to set
     */
    public void setComponentClassName(String componentClassName) {
        m_componentClassName = componentClassName;
    }
}
