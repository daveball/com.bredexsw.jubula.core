package com.bredexsw.jubula.rc.mobile.ios.impl.adapter;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer;
import com.bredexsw.jubula.rc.mobile.ios.constants.IOSComConst;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * The CAP implementation for iOS UISegmentedControl actions
 */
public class UITabBarAdapter extends AbstractUITabControlAdapter {
    /**
     * Constructor
     * @param objectToAdapt
     *            the object to adapt
     */
    public UITabBarAdapter(AbstractIOSComponent objectToAdapt) {
        super(objectToAdapt);
    }

    /** {@inheritDoc} */
    public boolean isEnabled() {
        return true;
    }

    /** {@inheritDoc} */
    protected String getNativeRCClassName() {
        return IOSComConst.CLASS_NAME_CAPS_UITABBAR;
    }
    
    /**
     * @param tabIndex
     *            the index of the tab item to check
     * @return the badge value
     */
    public String getBadgeValue(Integer tabIndex) {
        NativeMessage message = new NativeMessage(
                IOSComConst.CLASS_NAME_CAPS_UITABBAR,
                IOSComConst.METHOD_SIGNATURE_GET_BADGE_VALUE,
                getComponent().getLocatorPath());
        message.addParameter(tabIndex);
        NativeAnswer answer = IOSAUTServer.sendMessage(message);
        return answer.getString();
    }
}
