package org.eclipse.jubula.rc.common.adapter;

import ios.UIButton;
import ios.UIImageView;
import ios.UILabel;
import ios.UINavigationItemView;
import ios.UIPickerView;
import ios.UISegmentedControl;
import ios.UISwitch;
import ios.UITabBar;
import ios.UITabBarButton;
import ios.UITableView;
import ios.UITableViewCell;
import ios.UITextField;
import ios.UITextView;

import org.eclipse.jubula.rc.common.adaptable.IAdapterFactory;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IComponent;

import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UIButtonAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UIImageViewAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UILabelAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UINavigationItemViewAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UIPickerViewAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UISegmentedControlAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UISwitchAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITabBarAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITabBarButtonAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITableViewAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITableViewCellAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITextFieldAdapter;
import com.bredexsw.jubula.rc.mobile.ios.impl.adapter.UITextViewAdapter;
import com.bredexsw.jubula.rc.mobile.ios.model.AbstractIOSComponent;

/**
 * This is the adapter factory for all iOS components. It is creating the
 * specific adapter for an iOS component.
 * 
 * Since we are using adapter here, it is a adapter factory. But this must not
 * be the case. It is only relevant that the object is implementing the specific
 * interface.
 * 
 * @author BREDEX GmbH
 * 
 */
public class IOSAdapterFactory implements IAdapterFactory {
    /** a list of supported adaptable types */
    private static final Class[] SUPPORTED_CLASSES = new Class[] {
        UIButton.class, UITextField.class, UILabel.class,
        UITableViewCell.class, UITableView.class, UISwitch.class,
        UISegmentedControl.class, UINavigationItemView.class,
        UIImageView.class, UITextView.class, UITabBarButton.class,
        UIPickerView.class, UITabBar.class };

    /** {@inheritDoc} */
    public Class[] getSupportedClasses() {
        return SUPPORTED_CLASSES;
    }

    /** {@inheritDoc} */
    public Object getAdapter(Class targetedClass, Object objectToAdapt) {
        if (targetedClass.isAssignableFrom(IComponent.class)) {
            IComponent adapter = null;
            if (objectToAdapt instanceof AbstractIOSComponent) {
                AbstractIOSComponent comp = 
                        (AbstractIOSComponent) objectToAdapt;
                if (objectToAdapt instanceof UIButton) {
                    adapter = new UIButtonAdapter(comp);
                } else if (objectToAdapt instanceof UITextField) {
                    adapter = new UITextFieldAdapter(comp);
                } else if (objectToAdapt instanceof UITextView) {
                    adapter = new UITextViewAdapter(comp);
                } else if (objectToAdapt instanceof UILabel) {
                    adapter = new UILabelAdapter(comp);
                } else if (objectToAdapt instanceof UITableViewCell) {
                    adapter = new UITableViewCellAdapter(comp);
                } else if (objectToAdapt instanceof UITableView) {
                    adapter = new UITableViewAdapter(comp);
                } else if (objectToAdapt instanceof UISwitch) {
                    adapter = new UISwitchAdapter(comp);
                } else if (objectToAdapt instanceof UISegmentedControl) {
                    adapter = new UISegmentedControlAdapter(comp);
                } else if (objectToAdapt instanceof UINavigationItemView) {
                    adapter = new UINavigationItemViewAdapter(comp);
                } else if (objectToAdapt instanceof UIImageView) {
                    adapter = new UIImageViewAdapter(comp);
                } else if (objectToAdapt instanceof UITabBarButton) {
                    adapter = new UITabBarButtonAdapter(comp);
                } else if (objectToAdapt instanceof UIPickerView) {
                    adapter = new UIPickerViewAdapter(comp);
                } else if (objectToAdapt instanceof UITabBar) {
                    adapter = new UITabBarAdapter(comp);
                }
            }
            return adapter;
        }
        return null;
    }
}
