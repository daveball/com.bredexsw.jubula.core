/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.activator;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 */
public class Activator implements BundleActivator {

    /** The shared context of this bundle. */
    private static BundleContext context;

    /**
     * @return The context of the bundle or null, if it is not running.
     */
    public static BundleContext getContext() {
        return context;
    }

    /**
     * {@inheritDoc}
     * @see BundleActivator#start(BundleContext)
     */
    public final void start(final BundleContext bundleContext)
        throws Exception {
        context = bundleContext;
    }

    /**
     * {@inheritDoc}
     * @see BundleActivator#stop(BundleContext)
     */
    public final void stop(final BundleContext bundleContext) throws Exception {
        context = null;
    }

}
