package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.commands;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.jubula.qa.aut.caa.rp.skeleton.ApplicationActionBarAdvisor;

/**
 * Handler for showing the selected menu item.
 */
public class ButtonHandler extends AbstractHandler {

    /** The ID of the parameter given by the command. */
    private static final String PARAMETER_ID =
        "org.eclipse.jubula.qa.aut.caa.rcp.toolbars.commands.button.parameter";

    /**
     * @param event The event.
     * @return null
     * @throws ExecutionException An exception...
     */
    public Object execute(final ExecutionEvent event)
        throws ExecutionException {
        updateStatusText(event);
        return null;
    }

    /**
     * Call this method to update the status text with the description given by
     * {@link #getStatusText(ExecutionEvent)}.
     * @param event The event.
     */
    protected final void updateStatusText(final ExecutionEvent event) {
        setStatusText(event, getStatusText(event));
    }

    /**
     * @param event The event.
     * @param text The message to show in the CaA status line.
     */
    private void setStatusText(final ExecutionEvent event,
            final String text) {
//      @SuppressWarnings("restriction")
//      IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
//      if (window instanceof WorkbenchWindow) { // internal API !!!
//          WorkbenchWindow workbenchWindow = (WorkbenchWindow) window;
//          workbenchWindow.getStatusLineManager().setMessage(text);
//      }
        ApplicationActionBarAdvisor app =
              ApplicationActionBarAdvisor.getInstance();
        if (app != null) {
            app.setStatusText(text);
        }
    }

    /**
     * Overload this method to modify the description of the button.
     * @param event The event.
     * @return The name defined by the parameter.
     */
    protected String getStatusText(final ExecutionEvent event) {
        return event.getParameter(PARAMETER_ID);
    }

}
