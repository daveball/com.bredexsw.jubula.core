package org.eclipse.jubula.qa.aut.caa.rcp.toolbars.view;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

/**
 * View part with a tool bar.
 */
public class ViewPartWithToolbar extends ViewPart {

    /**
     * Constructor.
     */
    public ViewPartWithToolbar() {
        super();
    }

    @Override
    public void createPartControl(Composite parent) {
        // nothing
    }

    @Override
    public void setFocus() {
        // nothing
    }

}
