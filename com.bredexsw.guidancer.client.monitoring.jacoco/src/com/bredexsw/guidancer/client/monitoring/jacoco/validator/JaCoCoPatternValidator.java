/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13473 $
 *
 * $Date: 2010-12-02 18:00:11 +0100 (Thu, 02 Dec 2010) $
 *
 * $Author: marc $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.client.monitoring.jacoco.validator;

import com.bredexsw.jubula.client.monitoring.validator.PatternValidator;
/** 
 * A validator to verify that the given pattern is valid for JaCoCo 
 * @author marc
 * @created 02.12.2010
 */
public class JaCoCoPatternValidator extends PatternValidator {      
    // empty
}
