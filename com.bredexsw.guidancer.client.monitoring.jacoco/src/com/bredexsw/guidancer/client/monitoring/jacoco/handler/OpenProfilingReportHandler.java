/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13473 $
 *
 * $Date: 2010-12-02 18:00:11 +0100 (Thu, 02 Dec 2010) $
 *
 * $Author: marc $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.client.monitoring.jacoco.handler;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipFile;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jubula.client.core.model.ITestResultSummaryPO;
import org.eclipse.jubula.tools.internal.constants.MonitoringConstants;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.IOverwriteQuery;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;
import org.eclipse.ui.wizards.datatransfer.ZipFileStructureProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This handler class will load the JaCoCo HTML report from the database and import it into
 * the GUIdancer workspace. The report is a ZIP file stored as BLOB in the database. 
 * After the report is imported into the eclipse workspace, it will be shown via the default browser.
 * 
 * @author marc
 * @created 28.09.2010
 */
public class OpenProfilingReportHandler extends AbstractHandler {
    
    /** the logger */
    private static final Logger LOG = 
        LoggerFactory.getLogger(OpenProfilingReportHandler.class);     
    /**
     * @param file the index.html which should be open by the browser
     * 
     */  
    private void openBrowser(IFile file) {
        IWorkbenchPage currentPage = 
            PlatformUI.getWorkbench().getActiveWorkbenchWindow()
            .getActivePage();             
        try {
            IDE.openEditor(currentPage, file);
        } catch (PartInitException e) {
            LOG.error(e.toString());            
        }
    }     
    /**
     * @param selectedObjects A list of structured selections. The selections
     * must be am instance of ITestResultSummaryPO.
     * @return A list of created Projects. This list is used to set the selections in the
     * navigator view
     */
    private List<IProject> createAndOpenSummarys(List<?> selectedObjects) {
        
        IWorkspace workspace = ResourcesPlugin.getWorkspace();  
        IWorkspaceRoot theRoot = ResourcesPlugin.getWorkspace().getRoot();
        Iterator<?> it = selectedObjects.iterator();   
        List<IProject> createdProjectList = new ArrayList<IProject>();
        
        while (it.hasNext()) {            
            Object selectedObject = it.next();
            final ITestResultSummaryPO testResultSummary = 
                (ITestResultSummaryPO) selectedObject;
            String theProjName = testResultSummary.getTestsuiteName() + "-" //$NON-NLS-1$
                    + testResultSummary.getInternalMonitoringId() + "-" //$NON-NLS-1$
                    + testResultSummary.getId().toString();  
            BufferedOutputStream bos = null;
            File tmpZip = null;
            try {
                IProject project = theRoot.getProject(theProjName); 
                createdProjectList.add(project);
                if (!project.exists() 
                        && testResultSummary.isReportWritten() 
                        && testResultSummary.getMonitoringReport() != null) {
                    byte[] byteArray = 
                            testResultSummary.getMonitoringReport().getReport();
                    if (!byteArray.equals(MonitoringConstants.EMPTY_REPORT) 
                            && byteArray != null) {
                        IProjectDescription theDesc = 
                            project.getWorkspace().newProjectDescription(
                                    theProjName);
                        project.create(theDesc, new NullProgressMonitor());
                        project.open(new NullProgressMonitor());
                        tmpZip = File.createTempFile("tmpReport", ".zip");
                        bos = new BufferedOutputStream(
                                new FileOutputStream(tmpZip));  
                        bos.write(byteArray);
                        ZipFile zipfile = new ZipFile(tmpZip);   
                        importReport(zipfile, workspace, theProjName);
                    }
                }                 
                IFile indexFile = project.getFile("index.html"); //$NON-NLS-1$               
                if (indexFile.exists()) {                   
                    openBrowser(indexFile); 
                }               
            } catch (CoreException err) {
                LOG.error("Error while importing monitoring report into workspace", err); //$NON-NLS-1$
             
            } catch (IOException e) {
                LOG.error("Error while importing monitoring report into workspace", e); //$NON-NLS-1$                
            } finally {
                try {
                    if (bos != null) {
                        bos.close();
                    }
                    if (tmpZip != null) {
                        tmpZip.delete();
                    }
                } catch (IOException e) {
                    LOG.error("Error while writing zip file to tmp dir", e); //$NON-NLS-1$                        
                }
            }
        }   
        return createdProjectList;
    }
   
    /**
     * Imports ZipFiles form local file system into workspace.
     * @param zip A ZipFile which will be imported into workspace
     * @param workspace The workspace
     * @param theProjName The name of project in which the zipFile will be 
     * extracted 
     */    
    private void importReport(ZipFile zip, IWorkspace workspace,
            String theProjName) {
        /** overwrite rules for import*/
        class OverwriteQuery implements IOverwriteQuery {
            /** 
             * {@inheritDoc}
             */
            public String queryOverwrite(String pathString) {
                return ALL;
            }
        }
        ZipFileStructureProvider zipStPr = 
            new ZipFileStructureProvider(zip);
        ImportOperation op = new ImportOperation(
                workspace.getRoot().getProject(theProjName).getFullPath(),     
                zipStPr.getRoot(),
                zipStPr,
                new OverwriteQuery());
        try { 
            
            ProgressMonitorDialog pmd = new ProgressMonitorDialog(null);
            pmd.setCancelable(true);            
            pmd.open();           
            IProgressMonitor monitor = pmd.getProgressMonitor();            
            op.run(monitor); 
            pmd.close();
            
        } catch (InvocationTargetException e) {
            LOG.error(e.toString());           
        } catch (InterruptedException e) {
            LOG.error(e.toString());           
        }
    } 
    /**
     * 
     * {@inheritDoc}
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
       
        ISelection selection = HandlerUtil.getCurrentSelection(event);
        if (selection instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = 
                (IStructuredSelection) selection;
            
            List<?> selectedObjects = structuredSelection.toList();            
            List<IProject> createdSummarys = 
                createAndOpenSummarys(selectedObjects);     
            try {
                IViewPart p = PlatformUI.getWorkbench()
                        .getActiveWorkbenchWindow().getActivePage()
                        .showView("org.eclipse.ui.views.ResourceNavigator"); //$NON-NLS-1$
                if (p.getSite().getSelectionProvider() instanceof TreeViewer) {
                    TreeViewer s = (TreeViewer)p.getSite()
                            .getSelectionProvider();
                    s.setSelection(new StructuredSelection(createdSummarys));
                }
            } catch (PartInitException e) {
                LOG.error("Error while opening navigator view", e); //$NON-NLS-1$  
            }
        }
        return null;
    }
      
}

