/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.opencv;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import com.bredexsw.jubula.ir.core.ImageMatch;
import com.bredexsw.jubula.ir.core.SearchOptions;
import com.bredexsw.jubula.ir.core.exceptions.ImageTimeoutException;
import com.bredexsw.jubula.ir.core.exceptions.IncompatibleImageException;
import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;

/** 
 * finds images using openCV
 * @author BREDEX GmbH
 *
 */
public class OpenCVFinder implements IRegionFinder {

    /** constructor to load the library only once */
    public OpenCVFinder() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    /** {@inheritDoc} 
     * @throws ImageTimeoutException */
    public List<ImageMatch> findRegions(BufferedImage templateImage,
            BufferedImage searchImage, SearchOptions options)
        throws IncompatibleImageException, ImageTimeoutException {
        double starttime = System.currentTimeMillis();
        boolean timeoutIsSet = false;
        if (options != null && options.getTimeout() > 0) {
            timeoutIsSet = true;
        }
        
        List<ImageMatch> imgRes = new ArrayList<ImageMatch>();
        boolean isTooClose = false;
        writeImagesToFiles(templateImage, searchImage);
        
        ArrayList<Rectangle> locArray = new ArrayList<Rectangle>();
       
        Mat img = Highgui.imread("image.PNG");
        Mat templ = Highgui.imread("template.PNG");

        if (img.cols() < templ.cols() || img.rows() < templ.rows()) {
            throw new IncompatibleImageException();
        }
        int resultCols = img.cols() - templ.cols() + 1;
        int resultRows = img.rows() - templ.rows() + 1;
        Mat result = new Mat(resultRows, resultCols, CvType.CV_32FC1);
        Imgproc.matchTemplate(img, templ, result, Imgproc.TM_CCOEFF_NORMED);
//        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());        
       
        double threshold = 0.85;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int closeDistanceX = (int) screenSize.getWidth() / 200;
        int closeDistanceY = (int) screenSize.getHeight() / 200;
        for (int rows = 0; rows < resultRows; rows++) {
            for (int cols = 0; cols < resultCols; cols++) { 
                if (result.get(rows, cols)[0] > threshold) { 
                    for (int g = 0; g < locArray.size(); g++) {
                    //alternative to isTooClose: if a match is found, increment rows
                    //by templ.rows() and cols by templ.cols()
                        if ((Math.abs(locArray.get(g).getX() - cols) 
                                <= closeDistanceX) && (Math.abs(locArray.get(g).
                                        getY() - rows) <= closeDistanceY)) {
                    // possible extra feature: do not add if similarity of new element is also smaller
                            isTooClose = true;
                        }
                    }
                    if (!isTooClose) {
                        locArray.add(new Rectangle(cols, rows,
                                templateImage.getWidth(),
                                templateImage.getHeight()));
                    }                    
                }
                isTooClose = false;
                if (timeoutIsSet) {
                    if ((System.currentTimeMillis() - starttime)
                            > options.getTimeout()) {
                        throw new ImageTimeoutException();
                    }
                }
            }
        }
        for (int i = 0; i < locArray.size(); i++) {
            imgRes.add(new ImageMatch(new Rectangle(locArray.get(i).x,
                    locArray.get(i).y, templateImage.getWidth(),
                    templateImage.getHeight()), result.get(
                            locArray.get(i).y, locArray.get(i).x)[0]));
        }    
        deleteFiles();
        return imgRes;
    }
    

    /** 
     * writes two BufferedImages to Files, required for openCV to work
     * @param templateImage saved to template.PNG
     * @param searchImage saved to image.PNG
     */
    private void writeImagesToFiles(BufferedImage templateImage,
            BufferedImage searchImage) {
        File outTemplate = new File("template.PNG");
       
        File outImage = new File("image.PNG");
        try {
            ImageIO.write(templateImage, "PNG", outTemplate);
            ImageIO.write(searchImage, "PNG", outImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** delete the files that were created by writeImagesToFiles */
    private void deleteFiles() {
        File template = new File("template.PNG");
        File image = new File("image.PNG");
        template.delete();
        image.delete();
    }


}
