/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.opencv.test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;


/**
 * Class for testing different approaches on square difference
 * @author BREDEX GmbH
 */
public class DiffTests {
    
    /** system dependent part of paths */
//    private static final String SYSTEMDEPENDENTPART = "/projects/";
    private static final String SYSTEMDEPENDENTPART = "K:/";

    
    /** path to 2x2 images */
    private static final String PATH = SYSTEMDEPENDENTPART + "guidancer/"
            + "Workspace/users/OliverHoffmann/images/2x2Tests/";
    
    /** path to 100x100 images */
    private static final String PATHBIG = SYSTEMDEPENDENTPART + "guidancer/"
            + "Workspace/users/OliverHoffmann/images/100x100Tests/";
    
    /** path to images of screenshots */
    private static final String PATHREAL = SYSTEMDEPENDENTPART + "guidancer/"
            + "Workspace/users/OliverHoffmann/images/realImages/";
    
    /** ending of file */
    private static final String ENDING = ".png";
    
    /** images used for testing */
    private static final String[] IMAGES = new String[] {
        "0000", "0001", "0010", "0011", "0100", "0101", "0110",
        "0111", "1000", "1001", "1010", "1011", "1100", "1101",
        "1110", "1111"};
    
    /** images used for testing */
    private static final String[] IMAGESBIG = new String[] {
        "0", "1", "2", "4", "5", "14",
        "line1", "line2", "11line2", "alot", 
        "blackAlot", "black3", "black1", "black"};
    
    /** variations of the equals button in linux */
    private static final String[] IMAGESLIN = new String[] {
        "equalsButton_win",
//        "equalsButton_lin_pixels", 
//        "equalsButton_lin_big", "equalsButton_lin_small",
//        "equalsButton_lin_convert", "equalsButton_lin_red",
//        "equalsButton_lin_inverted", "equalsButton_lin_black"
////        "55,140", "57,140", "58,138", "60,140", "1370,708"
        "equalsButton_win_snipped", "equalsButton_win_wrong"
    };
//        "equalsButton_lin100", "equalsButton_lin100_inverted"};
        
    
    /** amount of possible values for two digits of a hexadecimal number */
    private static final int TWOHEXADIGIT = 256;
    
    /** the output */
    private StringBuilder m_output;

    /** test values of selfWritten euclidean distance on 100x100 images */
    @Test
    public void testEuclidReal() {
        testCcoeff(false);
        FileWriter out = null;
        m_output = new StringBuilder();

      
        try {
            out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                    + "OliverHoffmann/testresults/CcoeffTestResultsReal.txt");
            m_output.append("Test started on: " + new Date().toString() 
                    + System.lineSeparator() + System.lineSeparator());
            for (int i = 0; i < IMAGESLIN.length; i++) {
                for (int j = 0; j < IMAGESLIN.length;
                        j++) {                       
                    m_output.append("Comparing " + IMAGESLIN[i]
                            + " with " + IMAGESLIN[j] 
                                    + ":" + System.lineSeparator());
                    double result = calcCcoeff(i, j, true);
                    m_output.append("Result: " + result
                            + System.lineSeparator());
                } m_output.append(System.lineSeparator());
            }
            out.write(m_output.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    /** test values of selfWritten euclidean distance on 100x100 images */
    @Test
    public void testEuclid() {
        FileWriter out = null;
        m_output = new StringBuilder();

      
        try {
            out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                    + "OliverHoffmann/testresults/CcoeffTestResults.txt");
            m_output.append("Test started on: " + new Date().toString() 
                    + System.lineSeparator() + System.lineSeparator());
            for (int i = 0; i < IMAGESBIG.length; i++) {
                for (int j = 0; j < IMAGESBIG.length;
                        j++) {                       
                    m_output.append("Comparing " + IMAGESBIG[i]
                            + " with " + IMAGESBIG[j] 
                                    + ":" + System.lineSeparator());
                    double result = calcCcoeff(i, j, false);
                    m_output.append("Result: " + result
                            + System.lineSeparator());
                } m_output.append(System.lineSeparator());
            }
            out.write(m_output.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    /** test values of functions of opencv compared to a self written approach */
    @Test
    public void testDiffs() {
        testSquareDiff(false);
        testSquareDiff(true);
        testCcorr(false);
        testCcorr(true);
        testCcoeff(false);
        testCcoeff(true);
    }
    
    /**
     * calculates the euclidean distance on an rgb image
     * @param index1 index to first image
     * @param index2 index to second image
     * @param real true if real images are used instead of black-white, else false
     * @return value of similarity
     */
    private double calcEuclid(int index1, int index2, boolean real) {
        double numerator = 0;
        BufferedImage image1;
        BufferedImage image2;
        int currentValue1;
        int currentValue2;
        try {
            if (real) {
                image1 = ImageIO.read(
                        new File(PATHREAL + IMAGESLIN[index1] + ENDING));
                image2 = ImageIO.read(
                        new File(PATHREAL + IMAGESLIN[index2] + ENDING));
            } else {
                image1 = ImageIO.read(
                        new File(PATHBIG + IMAGESBIG[index1] + ENDING));
                image2 = ImageIO.read(
                        new File(PATHBIG + IMAGESBIG[index2] + ENDING));
            }

            for (int i = 0; i < image1.getWidth(); i++) {
                for (int j = 0; j < image1.getHeight(); j++) {
                    currentValue1 = image1.getRGB(i, j);
                    currentValue2 = image2.getRGB(i, j);
                    
                    int r1 = (currentValue1 >> 16) & 0xff;
                    int g1 = (currentValue1 >> 8) & 0xff;
                    int b1 = (currentValue1) & 0xff;
                    
                    int r2 = (currentValue2 >> 16) & 0xff;
                    int g2 = (currentValue2 >> 8) & 0xff;
                    int b2 = (currentValue2) & 0xff;
                                      
//                    int b1 = currentValue1 % TWOHEXADIGIT;
//                    currentValue1 = currentValue1 / TWOHEXADIGIT;
//                    int g1 = currentValue1 % TWOHEXADIGIT;
//                    int r1 = currentValue1 / TWOHEXADIGIT;
//                    
//                    int b2 = currentValue2 % TWOHEXADIGIT;
//                    currentValue2 = currentValue2 / TWOHEXADIGIT;
//                    int g2 = currentValue2 % TWOHEXADIGIT;
//                    int r2 = currentValue2 / TWOHEXADIGIT;
                    
                    double currentDiff = 0;
                    currentDiff += Math.pow(r1 - r2, 2);
                    currentDiff += Math.pow(g1 - g2, 2);
                    currentDiff += Math.pow(b1 - b2, 2);                    
                    numerator += currentDiff;
//                    numerator += Math.pow(currentValue1 - currentValue2, 2);
                  
                }
            }
            return normalizeEuclid(image1, image2, numerator);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;

    }
    
    /**
     * calculates the cross-correlation distance on an rgb image
     * @param index1 index to first image
     * @param index2 index to second image
     * @param real true if real images are used instead of black-white, else false
     * @return value of similarity
     */
    private double calcCcorr(int index1, int index2, boolean real) {
        double numerator = 0;
        BufferedImage image1;
        BufferedImage image2;
        int currentValue1;
        int currentValue2;
        try {
            if (real) {
                image1 = ImageIO.read(
                        new File(PATHREAL + IMAGESLIN[index1] + ENDING));
                image2 = ImageIO.read(
                        new File(PATHREAL + IMAGESLIN[index2] + ENDING));
            } else {
                image1 = ImageIO.read(
                        new File(PATHBIG + IMAGESBIG[index1] + ENDING));
                image2 = ImageIO.read(
                        new File(PATHBIG + IMAGESBIG[index2] + ENDING));
            }

            for (int i = 0; i < image1.getWidth(); i++) {
                for (int j = 0; j < image1.getHeight(); j++) {
                    currentValue1 = image1.getRGB(i, j);
                    currentValue2 = image2.getRGB(i, j);
                    
                    int r1 = (currentValue1 >> 16) & 0xff;
                    int g1 = (currentValue1 >> 8) & 0xff;
                    int b1 = (currentValue1) & 0xff;
                    
                    int r2 = (currentValue2 >> 16) & 0xff;
                    int g2 = (currentValue2 >> 8) & 0xff;
                    int b2 = (currentValue2) & 0xff;
                                      
                    double currentDiff = 0;
                    currentDiff += r1 * r2;
                    currentDiff += g1 * g2;
                    currentDiff += b1 * b2;                    
                    numerator += currentDiff;
                }
            }
            return normalizeEuclid(image1, image2, numerator);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * calculates the cross-correlation-coefficient distance on an rgb image
     * @param index1 index to first image
     * @param index2 index to second image
     * @param real true if real images are used instead of black-white, else false
     * @return value of similarity
     */
    private double calcCcoeff(int index1, int index2, boolean real) {
        double numerator = 0;
        BufferedImage image1;
        BufferedImage image2;
        int currentValue1;
        int currentValue2;
        double averageR1 = 0;
        double averageG1 = 0;
        double averageB1 = 0;
        double averageR2 = 0;
        double averageG2 = 0;
        double averageB2 = 0;

        try {
            if (real) {
                image1 = ImageIO.read(
                        new File(PATHREAL + IMAGESLIN[index1] + ENDING));
                image2 = ImageIO.read(
                        new File(PATHREAL + IMAGESLIN[index2] + ENDING));
                
            } else {
                image1 = ImageIO.read(
                        new File(PATHBIG + IMAGESBIG[index1] + ENDING));
                image2 = ImageIO.read(
                        new File(PATHBIG + IMAGESBIG[index2] + ENDING));

            }

            // problem when template matching: for searchImage we have to calculate the average
            // of only the area where we are currently searching
            averageR1 = calcCcoeffUtil(image1, 0);
            averageG1 = calcCcoeffUtil(image1, 1);
            averageB1 = calcCcoeffUtil(image1, 2);
            averageR2 = calcCcoeffUtil(image2, 0);
            averageG2 = calcCcoeffUtil(image2, 1);
            averageB2 = calcCcoeffUtil(image2, 2);
            for (int i = 0; i < image1.getWidth(); i++) {
                for (int j = 0; j < image1.getHeight(); j++) {
                    currentValue1 = image1.getRGB(i, j);
                    currentValue2 = image2.getRGB(i, j);
                    
                    int r1 = (currentValue1 >> 16) & 0xff;
                    int g1 = (currentValue1 >> 8) & 0xff;
                    int b1 = (currentValue1) & 0xff;
                    
                    int r2 = (currentValue2 >> 16) & 0xff;
                    int g2 = (currentValue2 >> 8) & 0xff;
                    int b2 = (currentValue2) & 0xff;
                                      
                    double currentDiff = 0;
                    currentDiff += (r1 - averageR1) * (r2 - averageR2);
                    currentDiff += (g1 - averageG1) * (g2 - averageG2);
                    currentDiff += (b1 - averageB1) 
                            * (b2 - averageB2);                    
                    numerator += currentDiff;
                }
            }
            return normalizeCcoeff(image1, image2, numerator);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    /**
     * Utility method for ccoeff to alter the currentValues
     * (meaning to substract the average of the image from it)
     * @param image image to base alteration on
     * @param index 0 for red, 1 for green, 2 for blue
     * @return altered Value
     */
    private double calcCcoeffUtil(BufferedImage image, int index) {
        double average = 0;
        int currentValue;
        
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                currentValue = image.getRGB(i, j);
                int r1 = (currentValue >> 16) & 0xff;
                int g1 = (currentValue >> 8) & 0xff;
                int b1 = (currentValue) & 0xff;

                if (index == 0) {
                    average += r1;
                } else {
                    if (index == 1) {
                        average += g1;
                    } else {
                        average += b1;
                    }
                }
            }
        }
        average = average * (1.0 / (double)
                (image.getWidth() * image.getHeight()));
        return average;
    }
    
    /**
     * normalizes the value of the euclidean distance for each color channel
     * @param image1 first image
     * @param image2 second image
     * @param numerator value of distance
     * @return normalized value of distance
     */
    private double normalizeCcoeff(BufferedImage image1, BufferedImage image2,
            double numerator) {
        double denominator = 0;
        double pixelValue1 = 0;
        double pixelValue2 = 0;
        
        double averageR1 = calcCcoeffUtil(image1, 0);
        double averageG1 = calcCcoeffUtil(image1, 1);
        double averageB1 = calcCcoeffUtil(image1, 2);
        double averageR2 = calcCcoeffUtil(image2, 0);
        double averageG2 = calcCcoeffUtil(image2, 1);
        double averageB2 = calcCcoeffUtil(image2, 2);
        
        int currentValue1;
        int currentValue2;
        for (int i = 0; i < image1.getWidth(); i++) {
            for (int j = 0; j < image1.getHeight(); j++) {
                currentValue1 = image1.getRGB(i, j);
                currentValue2 = image2.getRGB(i, j);
                
                int r1 = (currentValue1 >> 16) & 0xff;
                int g1 = (currentValue1 >> 8) & 0xff;
                int b1 = (currentValue1) & 0xff;
                
                int r2 = (currentValue2 >> 16) & 0xff;
                int g2 = (currentValue2 >> 8) & 0xff;
                int b2 = (currentValue2) & 0xff;
                                
                double pixelValueCurr1 = 0;
                pixelValueCurr1 += Math.pow(r1 - averageR1, 2);
                pixelValueCurr1 += Math.pow(g1 - averageG1, 2);
                pixelValueCurr1 += Math.pow(b1 - averageB1, 2);
                
                double pixelValueCurr2 = 0;
                pixelValueCurr2 += Math.pow(r2 - averageR2, 2);
                pixelValueCurr2 += Math.pow(g2 - averageG2, 2);
                pixelValueCurr2 += Math.pow(b2 - averageB2, 2);
                
                pixelValue1 += pixelValueCurr1;
                pixelValue2 += pixelValueCurr2;
                
            }
        }
        
        denominator = Math.sqrt(pixelValue1 * pixelValue2);
        
        return (numerator / denominator);
    }
    
    /**
     * normalizes the value of the euclidean distance for each color channel
     * @param image1 first image
     * @param image2 second image
     * @param numerator value of distance
     * @return normalized value of distance
     */
    private double normalizeEuclid(BufferedImage image1, BufferedImage image2,
            double numerator) {
        double denominator = 0;
        double pixelValue1 = 0;
        double pixelValue2 = 0;
        
        int currentValue1;
        int currentValue2;
        for (int i = 0; i < image1.getWidth(); i++) {
            for (int j = 0; j < image1.getHeight(); j++) {
                currentValue1 = image1.getRGB(i, j);
                currentValue2 = image2.getRGB(i, j);
                
                int r1 = (currentValue1 >> 16) & 0xff;
                int g1 = (currentValue1 >> 8) & 0xff;
                int b1 = (currentValue1) & 0xff;
                
                int r2 = (currentValue2 >> 16) & 0xff;
                int g2 = (currentValue2 >> 8) & 0xff;
                int b2 = (currentValue2) & 0xff;
                                
                double pixelValueCurr1 = 0;
                pixelValueCurr1 += Math.pow(r1, 2);
                pixelValueCurr1 += Math.pow(g1, 2);
                pixelValueCurr1 += Math.pow(b1, 2);
                
                double pixelValueCurr2 = 0;
                pixelValueCurr2 += Math.pow(r2, 2);
                pixelValueCurr2 += Math.pow(g2, 2);
                pixelValueCurr2 += Math.pow(b2, 2);
                
                pixelValue1 += pixelValueCurr1;
                pixelValue2 += pixelValueCurr2;
                
//                pixelValue1 += currentValue1;
//                pixelValue2 += currentValue2;
            }
        }
        
        denominator = Math.sqrt(pixelValue1 * pixelValue2);
        
        // if one of the images is completely black we would divide by zero, so instead
        // we make the difference very high
//        if (denominator == 0) {
//            denominator = 0.0001;
//        }
        return (numerator / denominator);
    }

    /** 
     * test results between openCV matchTemplate with normalized square diff
     * and a self-written approach of normalized square diff
     * @param isBlackWhite true if images are black-white
     */
    private void testSquareDiff(boolean isBlackWhite) {
        FileWriter out = null;
        m_output = new StringBuilder();
        try {
            if (isBlackWhite) {
                out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/OliverHo"
                            + "ffmann/testresults/testResultsSquareDiffBW.txt");
            } else {
                out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/OliverHo"
                            + "ffmann/testresults/testResultsSquareDiff.txt");
            }
            m_output.append("Test started on: " + new Date().toString() 
                    + System.lineSeparator() + System.lineSeparator());
           
            for (int i = 0; i < IMAGES.length; i++) {
                for (int j = i; j < IMAGES.length; j++) {
                       
                    m_output.append("Comparing " + IMAGES[i]
                            + " with " + IMAGES[j] 
                                    + ":" + System.lineSeparator());
                    double resultManual = squareDiffManual(i, j, isBlackWhite);
                    double resultCV = squareDiffCV(i, j);
                    m_output.append("Result Manual: " + resultManual
                            + " Result openCV: " + resultCV 
                            + System.lineSeparator());
                }
                m_output.append(System.lineSeparator());
            }
            out.write(m_output.toString());
            out.close();
        } catch (IOException e1) {
            e1.printStackTrace();        
        }
    }
    
    
    /** 
     * test results between openCV matchTemplate with normalized cross-correlation
     * and a self-written approach of normalized cross-correlation
     * @param isBlackWhite true if images are black-white
     */
    private void testCcorr(boolean isBlackWhite) {
        FileWriter out = null;
        m_output = new StringBuilder();
        try {
            if (isBlackWhite) {
                out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/OliverHo"
                                + "ffmann/testresults/testResultsCcorrBW.txt");
            } else {
                out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/OliverHo"
                                + "ffmann/testresults/testResultsCcorr.txt");
            }
            
            m_output.append("Test started on: " + new Date().toString() 
                    + System.lineSeparator() + System.lineSeparator());
           
            for (int i = 0; i < IMAGES.length; i++) {
                for (int j = i; j < IMAGES.length; j++) {
                       
                    m_output.append("Comparing " + IMAGES[i]
                            + " with " + IMAGES[j] 
                                    + ":" + System.lineSeparator());
                    double resultManual = ccorrManual(i, j, isBlackWhite);
                    double resultCV = ccorrCV(i, j);
                    m_output.append("Result Manual: " + resultManual
                            + " Result openCV: " + resultCV 
                            + System.lineSeparator());
                }
                m_output.append(System.lineSeparator());
            }
            out.write(m_output.toString());
            out.close();
        } catch (IOException e1) {
            e1.printStackTrace();        
        }
    }
    
    /** 
     * test results between openCV matchTemplate with normalized correlation coefficient
     * and a self-written approach of normalized cross-coefficient
     * @param isBlackWhite true if images are black-white
     */
    private void testCcoeff(boolean isBlackWhite) {
        FileWriter out = null;
        m_output = new StringBuilder();
        try {
            if (isBlackWhite) {
                out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/OliverHo"
                            + "ffmann/testresults/testResultsCcoeffBW.txt");
            } else {
                out = new FileWriter(
                        SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                            + "OliverHoffmann/testresults/"
                            + "CcoeffTestResultsRealCV.txt");
            }
            
            m_output.append("Test started on: " + new Date().toString() 
                    + System.lineSeparator() + System.lineSeparator());
           
            for (int i = 0; i < IMAGESLIN.length; i++) {
                for (int j = 0; j < IMAGESLIN.length; j++) {
                       
                    m_output.append("Comparing " + IMAGESLIN[i]
                            + " with " + IMAGESLIN[j] 
                                    + ":" + System.lineSeparator());
//                    double resultManual = ccoeffManual(i, j, isBlackWhite);
                    double resultCV = ccoeffCV(i, j);
                    m_output.append(
//                            "Result Manual: " + resultManual
                             " Result openCV: " + resultCV 
                            + System.lineSeparator());
                }
                m_output.append(System.lineSeparator());
            }
            out.write(m_output.toString());
            out.close();
        } catch (IOException e1) {
            e1.printStackTrace();        
        }
    }
    
    /** 
     * manual approach of calculating sum of square differences between two images
     * @param index1 index of first image
     * @param index2 index of second image
     * @param isBlackWhite true if images are black-white
     * @return normalized double value for similarity
     */
    private double squareDiffManual(
            int index1, int index2, boolean isBlackWhite) {
        double numerator = 0;
        BufferedImage image1;
        BufferedImage image2;        
        int currentValue1;
        int currentValue2;        
        try {
            
            image1 = ImageIO.read(
                    new File(PATH + IMAGES[index1] + ENDING));
            image2 = ImageIO.read(
                    new File(PATH + IMAGES[index2] + ENDING));

            for (int i = 0; i < image1.getWidth(); i++) {
                for (int j = 0; j < image1.getHeight(); j++) {
                    currentValue1 = translateValue(image1, isBlackWhite, i, j);
                    currentValue2 = translateValue(image2, isBlackWhite, i, j);
                    numerator += Math.pow(
                            currentValue1 - currentValue2, 2);
                  
                }
            }
            return normalizeManual(image1, image2, numerator, isBlackWhite);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /** 
     * manual approach of calculating cross correlation between two images
     * @param index1 index of first image
     * @param index2 index of second image
     * @param isBlackWhite true if images are black-white 
     * @return normalized double value for similarity
     */
    private double ccorrManual(
            int index1, int index2, boolean isBlackWhite) {
        double numerator = 0;
        BufferedImage image1;
        BufferedImage image2;        
        int currentValue1;
        int currentValue2;        
        try {
            
            image1 = ImageIO.read(
                    new File(PATH + IMAGES[index1] + ENDING));
            image2 = ImageIO.read(
                    new File(PATH + IMAGES[index2] + ENDING));

            for (int i = 0; i < image1.getWidth(); i++) {
                for (int j = 0; j < image1.getHeight(); j++) {
                    currentValue1 = translateValue(image1, isBlackWhite, i, j);
                    currentValue2 = translateValue(image2, isBlackWhite, i, j);
                    
                    numerator += currentValue1 * currentValue2;
                  
                }
            }
            return normalizeManual(image1, image2, numerator, isBlackWhite);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }
    
   
    
    /** 
     * manual approach of calculating correlation coefficient between two images
     * @param index1 index of first image
     * @param index2 index of second image
     * @param isBlackWhite true if images are black-white 
     * @return normalized double value for similarity
     */
    private double ccoeffManual(
            int index1, int index2, boolean isBlackWhite) {
        double numerator = 0;
        BufferedImage image1;
        BufferedImage image2;        
        int currentValue1;
        int currentValue2;        
        try {
            
            image1 = ImageIO.read(
                    new File(PATH + IMAGES[index1] + ENDING));
            image2 = ImageIO.read(
                    new File(PATH + IMAGES[index2] + ENDING));

            for (int i = 0; i < image1.getWidth(); i++) {
                for (int j = 0; j < image1.getHeight(); j++) {
                    currentValue1 = translateValue(image1, isBlackWhite, i, j);
                    currentValue2 = translateValue(image2, isBlackWhite, i, j);
                    
                    numerator += ccoeffUtil(image1, currentValue1)
                            * ccoeffUtil(image2, currentValue2);
                  
                }
            }
            return normalizeManual(image1, image2, numerator, isBlackWhite);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    /**
     * method to normalize the result of a manual difference
     * @param image1 first image
     * @param image2 second image
     * @param numerator numerator to normalize
     * @param isBlackWhite true if images are black-white
     * @return normalized numerator
     */
    private double normalizeManual(
            BufferedImage image1, BufferedImage image2, 
            double numerator, boolean isBlackWhite) {
        double denominator = 0;
        double pixelValue1 = 0;
        double pixelValue2 = 0;
        
        int currentValue1;
        int currentValue2;
        for (int i = 0; i < image1.getWidth(); i++) {
            for (int j = 0; j < image1.getHeight(); j++) {
                currentValue1 = translateValue(image1, isBlackWhite, i, j);
                currentValue2 = translateValue(image2, isBlackWhite, i, j);
                
                pixelValue1 += Math.pow(currentValue1, 2);
                pixelValue2 += Math.pow(currentValue2, 2);
            }
        }
        
        denominator = Math.sqrt(pixelValue1 * pixelValue2);
        return numerator / denominator;
    }
    
    /** 
     * changes type of value of image if it is blackwhite 
     * @param image image to change
     * @param isBlackWhite ture if black-white
     * @param x x-position of value to change
     * @param y y-position of value to change
     * @return value of image
     */
    private int  translateValue(BufferedImage image,
            boolean isBlackWhite, int x, int y) {
        int result;
        if (isBlackWhite) {
            if (image.getRGB(x, y) == -1) {
                result = 1;
            } else {
                result = 0;
            }
            
        } else {
            result = image.getRGB(x, y);
        }
        return result;
    }
    
    /**
     * Utility method for ccoeff to alter the currentValues
     * (meaning to substract the average of the image from it)
     * @param image image to base alteration on
     * @param valueToAlterate value to alter
     * @return altered Value
     */
    private double ccoeffUtil(BufferedImage image, int valueToAlterate) {
        double average = 0;
        int currentValue;
        
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                if (image.getRGB(i, j) == -1) {
                    currentValue = 1;
                } else {
                    currentValue = 0;
                }
                average += currentValue;
            }
        }
        average = average * (1 / (image.getWidth() * image.getHeight()));
        return valueToAlterate - average;
    }
    /** 
     * openCV approach of calculating sum of square differences between two images 
     * @param i index of first image
     * @param j index of second image
     * @return normalized double value for similarity between 0 and 1
     */
    private double squareDiffCV(int i, int j) {
      
        String path1 = PATH + IMAGES[i] + ENDING;
        String path2 = PATH + IMAGES[j] + ENDING;

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
        Mat img = Highgui.imread(path1);
        Mat templ = Highgui.imread(path2);
        
        int resultCols = img.cols() - templ.cols() + 1;
        int resultRows = img.rows() - templ.rows() + 1;

        Mat result = new Mat(resultRows, resultCols, CvType.CV_32FC1);
        Imgproc.matchTemplate(img, templ, result, Imgproc.TM_SQDIFF_NORMED);
        
        return result.get(0, 0)[0];
    }
    
    /** 
     * openCV approach of calculating cross-correlation between two images 
     * @param i index of first image
     * @param j index of second image
     * @return normalized double value for similarity between 0 and 1
     */
    private double ccorrCV(int i, int j) {
      
        String path1 = PATH + IMAGES[i] + ENDING;
        String path2 = PATH + IMAGES[j] + ENDING;

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
        Mat img = Highgui.imread(path1);
        Mat templ = Highgui.imread(path2);
        
        int resultCols = img.cols() - templ.cols() + 1;
        int resultRows = img.rows() - templ.rows() + 1;

        Mat result = new Mat(resultRows, resultCols, CvType.CV_32FC1);
        Imgproc.matchTemplate(img, templ, result, Imgproc.TM_CCORR_NORMED);
        
        return result.get(0, 0)[0];
    }
    
    /** 
     * openCV approach of calculating correlation coefficient between two images 
     * @param i index of first image
     * @param j index of second image
     * @return normalized double value for similarity between 0 and 1
     */
    private double ccoeffCV(int i, int j) {
      
        String path1 = PATHREAL + IMAGESLIN[i] + ENDING;
        String path2 = PATHREAL + IMAGESLIN[j] + ENDING;

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
        Mat img = Highgui.imread(path1);
        Mat templ = Highgui.imread(path2);
        
        int resultCols = img.cols() - templ.cols() + 1;
        int resultRows = img.rows() - templ.rows() + 1;

        Mat result = new Mat(resultRows, resultCols, CvType.CV_32FC1);
        Imgproc.matchTemplate(img, templ, result, Imgproc.TM_CCOEFF_NORMED);
        
        return result.get(0, 0)[0];
    }
}
