package org.eclipse.jubula.qa.aut.caa.rcp.e4.swt.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.jubula.qa.aut.caa.swt.StartShell;
import org.eclipse.swt.widgets.Composite;

/**
 * A part wrapping SWT content
 * 
 * @author janw
 */
public class SWTPart {

    /** storage for dialog content */
    private Composite m_content;

    /**
     * empty constructor
     */
    @Inject
    public SWTPart() {
    }

    /**
     * Creates the control for the SWT-wrapping View
     * 
     * @param parent the parent for the SWT content
     */
    @PostConstruct
    public void createControl(Composite parent) {
        m_content = StartShell.createContent(parent);
        m_content.setVisible(true);
    }
}