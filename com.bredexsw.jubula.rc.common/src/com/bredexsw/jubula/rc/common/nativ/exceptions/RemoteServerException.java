/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.common.nativ.exceptions;

/**
 * Exception which occurred on the native RC-Server side
 */
public class RemoteServerException extends Exception {
    /**
     * ID of the native RC exception
     */
    private int m_exceptionID;

    /**
     * Message of the native RC exception
     */
    private String m_exceptionMsg;

    /**
     * Class from the invoked Method
     */
    private String m_exceptionClass;

    /**
     * Name of the invoked Method
     */
    private String m_exceptionMethod;

    /**
     * Constructor
     * 
     * @param id
     *            the exception id
     * @param name
     *            the exception name
     * @param msg
     *            the exception message
     * @param method
     *            the method name
     * @param className
     *            the class name
     */
    public RemoteServerException(int id, String name, String msg,
            String method, String className) {
        super(msg);
        this.m_exceptionID = id;
        this.m_exceptionMsg = msg;
        this.m_exceptionMethod = method;
        this.m_exceptionClass = className;

    }

    /**
     * 
     * @return m_exceptionName
     */
    public int getExceptionID() {
        return m_exceptionID;
    }

    /**
     * 
     * @return m_exceptionMsg
     */
    public String getExceptionMsg() {
        return m_exceptionMsg;
    }

    /**
     * 
     * @return m_exceptionClass
     */
    public String getExceptionClass() {
        return m_exceptionClass;
    }

    /**
     * 
     * @return m_exceptionMethod
     */
    public String getExceptionMethod() {
        return m_exceptionMethod;
    }
}
