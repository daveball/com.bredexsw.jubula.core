package com.bredexsw.jubula.rc.common;

/**
 * @author BREDEX GmbH
 */
public interface Constants {
    /** the bundle ID */
    public static final String BUNDLE_ID = "com.bredexsw.jubula.rc.common"; //$NON-NLS-1$
}
