/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 * set JQuery to no conflict mode to avoid namespace problems
 */
jQuery.noConflict();

/**
 * this is the main function which holds all necessary information for the
 * implementation of the object mapping in java script;
 * 
 * IMPORTANT - java keys can be found in class JSONConstantsObjectMapping.java
 * 
 * @param technicalSupportedTypes
 *            we do require a list of all supported technical component types
 *            for object mapping
 */
function omm(technicalSupportedTypes) {
	/** flag to enable / disable debug information / functions */
	var debugMode = false;

	/** enable or disable the debug mode */
	this.setDebugMode = function setDebugMode(isEnabled) {
		debugMode = isEnabled;
		return "successfully setDebugMode=" + isEnabled;
	};
	
	/** helper function to centrally enable / disable / configure logging */
	function log(stringToLog) {
		if (debugMode) {
			try {
				console.log(stringToLog);
			} catch (e) {
				/** 
				 * ignore, e.g. this does not work in IE as there is no console;
				 * works fine in FF when firebug is present
				 */
			}
		}
	}
	
	/** attribute name to store the enablement state in */
	var enablementStateAttr = 'uiAutomationEnablementState';
	
	/** attribute name to store the parent frame path in */
	var parentFramePathAttr = 'uiAutomationParentFramePath';
	
	/**
	 * this is a list of supported component types for object mapping; as we
	 * make use of the jQuery() selector function these types are the string
	 * value of the tagName of DOM nodes
	 */
	var supportedTypes = JSON.parse(technicalSupportedTypes);

	/** function to build the jQuery selector */
	function getSelector(supportedType, selectorPropertyName) {
		log("computing selector for: " + supportedType);
		var selectorPropertyArray = jQuery.grep(supportedType.properties, function(element, idx) {
			return element.name == selectorPropertyName;
		});
		
		if (selectorPropertyArray.length > 0) {
			log("selector is: " + selectorPropertyArray[0].value);
			return selectorPropertyArray[0].value;
		}

		return null;
	}
	
	/** attach to the given document */
	function attachToDocument(supportedTypes, targetDocument, info) {
		log("attaching to document: " + targetDocument.URL);
		try {
			for (var i = 0; i < supportedTypes.length; i++) {
				var selector = getSelector(supportedTypes[i], "selector");
				
				log("binding objects using computed selector: " + selector);
				var query = jQuery(selector, targetDocument);
				if (debugMode) {
					query.effect("highlight", { color :"blue" }, 1000);
				}
				query.live("mousedown", collect);
				query.live("mouseup", blockEvents);
				query.live("click", blockEvents);
				query.attr(parentFramePathAttr, info);
				log("finished live");
				
				var containsQuery = getSelector(supportedTypes[i], "contains-selector");
				log("created containsQuery var");
				if (containsQuery != null) {
					containsQuery.live("mousedown", blockEvents);
					containsQuery.live("mouseup", blockEvents);
					containsQuery.live("click", blockEvents);
					containsQuery.attr(parentFramePathAttr, info);
				}
				
				log("binding objects finished.");
				
				/** 
				 * enable all disabled components to make object mapping work
				 * and store disabled attribute in attribute: enablementStateKEY
				 */
				log("enabling all components...");
				var disabledSelector = selector + "[disabled]";
				var queryForDisabled = jQuery(disabledSelector, 
						targetDocument);
				queryForDisabled.attr(enablementStateAttr, "true");
				queryForDisabled.removeAttr("disabled");
				log("enabling finished.");
			}
		} catch (e) {
			log("attaching to document failed: " + e);
		}
		log("attaching to document finished.");
	}
	
	/** recursively attach to the given window and all nested frames */
	function recursiveAttach(supportedTypes, window, info) {
		var doc = window.document;
		attachToDocument(supportedTypes, doc, info);

		var innerFrames = window.frames;
		if( innerFrames.length > 0 ) {
			for( var i = 0; i < innerFrames.length; i++ ) {
				// !! 1-based index !!
				var index = i + 1;
				var parentInfo = info + '/uiframe[' + index + ']';
			    recursiveAttach( supportedTypes, innerFrames[i], parentInfo);
			}
		}
	}
	
	/**
	 * this functions starts the object mapping mode by "binding" the collect()
	 * function to all supported elements on the current page and its nested frames
	 */
	this.start = function start() {
		recursiveAttach(supportedTypes, window, '');
		return "successfully started...";
	};

	/** detach from the given document */
	function detachFromDocument(supportedTypes, targetDocument) {
		log("detaching from document: " + targetDocument.URL);
		try {
			for (var i = 0; i < supportedTypes.length; i++) {
				var selector = getSelector(supportedTypes[i], "selector");
				
				log("unbinding objects using computed selector: " + selector);
				var query = jQuery(selector, targetDocument);
				if (debugMode) {
					query.effect("highlight", { color :"green" }, 1000);
				}
				query.die("mousedown", collect);
				query.die("mouseup", blockEvents);
				query.die("click", blockEvents);
				query.removeAttr(parentFramePathAttr);
				
				log("unbinding objects finished.");
				
				log("disabling previously disabled components again...");
				var previousDisabledSelector = selector + "["
						+ enablementStateAttr + "]";
				var queryPrevDisabled = jQuery(previousDisabledSelector, 
						targetDocument);
				queryPrevDisabled.attr("disabled", "disabled");
				queryPrevDisabled.removeAttr(enablementStateAttr);
				log("disabling finished.");
			}
		} catch (e) {
			log("detaching from document failed: " + e);
		}
		log("detaching from document finished.");
	}
	
	/** recursively detach from the given window and all nested frames */
	function recursiveDetach(supportedTypes, window) {
		var doc = window.document;
		detachFromDocument(supportedTypes, doc);

		var innerFrames = window.frames;
		if( innerFrames.length > 0 ) {
			for(var i = 0; i < innerFrames.length; i++ ) {
			    recursiveDetach( supportedTypes, innerFrames[i]);
			}
		}
	}
	
	/**
	 * this functions stops the object mapping mode by unbinding the collect()
	 * function from all supported elements on the current page
	 */
	this.stop = function stop() {
		recursiveDetach(supportedTypes, window);
		return "successfully stopped...";
	};

	/**
	 * during object mapping this list of component identifier is filled on each
	 * "click" event
	 */
	var componentIdentifiers = new ComponentIdentifierList();

	/**
	 * this function is designed to be called periodically by the client to get
	 * a JSON stringified string of all technical component identifier which
	 * have been collected since calling this function the last time; after
	 * calling this function the list of already collected components is cleared
	 */
	this.getTechnicalComponents = function getTechnicalComponents(dummy) {
		try {
			var i = 0;
			var jsonFilter = new Array();
			jsonFilter[i++] = "ComponentIdentifierList";
			jsonFilter[i++] = "ComponentIdentifier";
			jsonFilter[i++] = "techCompXPath";
			var _uiJson = JSON.stringify(componentIdentifiers.get(), jsonFilter);
			componentIdentifiers.clear();
			log(_uiJson);
			return _uiJson;
		} catch (err) {
			return err;
		}
	};

	/** 
	 * This inner function is bound on the "click" event of each contained 
	 * element of each supported element. Containment in this case is used to
	 * indicate composition (e.g. a tree contains its tree items).
	 */
	function collectContainer(containerSelector) {
		var container = jQuery(this).parentsUntil(containerSelector).parent();
		componentIdentifiers.add(new ComponentIdentifier(container));
		container.effect("highlight", {
			color :"red"
		}, 250);
		// return false to prevent click from executing registered action
		return false;
	}
	/** this inner function is bound on the "click" event of each supported element */
	function collect() {
		componentIdentifiers.add(new ComponentIdentifier(jQuery(this)));
		jQuery(this).effect("highlight", {
			color :"red"
		}, 250);
		// return true so that you are e.g. able to still click into a textfield
		return false;
	}
	/** function to block the JS events during object mapping mode */
	function blockEvents() {
		// return false so that the event is not propagated any longer
		return false;
	}
	
	/** this is the inner object model for a technical collected component */
	function ComponentIdentifier(technicalComponentJQuery) {
		this.techCompXPath = getHierarchy(technicalComponentJQuery.context);

		function getHierarchy(technicalComponent) {
			return get_XPath(technicalComponent);
		}

		/** retrieve the XPATH for the current node */ 
		function get_XPath(cN) {
			var path = '';
			var startNode = cN;
			for (; cN && cN.nodeType == 1; cN = cN.parentNode) {
				var idx = jQuery(cN.parentNode).children(cN.tagName)
					.index(cN) + 1;
				path = '/' + cN.tagName.toLowerCase() + '[' + idx + ']' + path;
			}
			var fqPath = jQuery(startNode).attr(parentFramePathAttr) + path;
			return fqPath;
		}
	}

	/** inner list to temporarily store all collected technical components */
	function ComponentIdentifierList() {
		var defaultSize = 1;
		var cIdentifier = new Array(defaultSize);
		var cIndex = 0;

		this.add = function add(ComponentIdentifier) {
			cIdentifier[cIndex] = ComponentIdentifier;
			cIndex++;
		};

		this.get = function get() {
			return cIdentifier;
		};

		this.clear = function clear() {
			cIdentifier = new Array(defaultSize);
			cIndex = 0;
		};
	}
}