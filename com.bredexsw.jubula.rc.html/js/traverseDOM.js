/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
var UIDOMelements;

/**
 * IMPORTANT - Java keys can be found in class JSONConstantsTraverseDOM
 *
 * container for JSON communication
 **/
function domContainer(node, idAttribute) {
    var attributes = new Object();
    this.attributes = attributes;
    fillAttributes = function fillAttributes(domElement, idAttribute) {
        // store the relevant attributes
        try {
            attributes.nodeName = domElement.nodeName.toLowerCase();
        } catch (e) {/**ignore*/}

        try {
            if (idAttribute != 'id') {
                customAttributeValue = domElement.attributes
                        .getNamedItem(idAttribute).value;
                if (customAttributeValue != '' || customAttributeValue != null) {
                    attributes.id = customAttributeValue;
                }
            } else {
                if (domElement.id) {
                    attributes.id = domElement.id;
                }
            }
        } catch (e) {/**ignore*/}
        
        try {
            if (domElement.type) {
                attributes.type = domElement.type.toLowerCase();
            }
        } catch (e) {/**ignore*/}
        
        try {
            if (domElement.size) {
                attributes.size = domElement.size;
            }
        } catch (e) {/**ignore*/}
        
        try {
            if (domElement.className) {
                attributes.className = domElement.className;
            }
        } catch (e) {/**ignore*/}
        
        try {
            role = domElement.attributes.role.value.toLowerCase();
            if (role != '' || role != null) {
                attributes.role = role;
            }
        } catch (e) {/**ignore*/}
    };

    var children = new Array();
    this.children = children;
    
    fillAttributes(node, idAttribute);
}

/** recursive function to traverse the DOM tree */
function traverseDOMTree(domNode, idAttribute) {
    switch (domNode.nodeType) {
    case 2:  //   Node.ATTRIBUTE_NODE
    case 3:  //   Node.TEXT_NODE
    case 4:  //   Node.CDATA_SECTION_NODE
    case 5:  //   Node.ENTITY_REFERENCE_NODE
    case 6:  //   Node.ENTITY_NODE
    case 7:  //   Node.PROCESSING_INSTRUCTION_NODE
    case 8:  //   Node.COMMENT_NODE
    case 10: //   Node.DOCUMENT_TYPE_NODE
    case 12: //   Node.NOTATION_NODE
        break;
    
    case 9:  //   Node.DOCUMENT_NODE
	         /**
			  * completely skip document nodes when traversing as the
			  * document itself is not part of a valid XPath
			  */
        var currentElementChild = null;
        var childContainer = new Array();
        try {
            currentElementChild = domNode.firstChild;
        } catch (e) {/**ignore*/}

        while (currentElementChild) {
            // !!! recursion !!!
            var child = traverseDOMTree(currentElementChild, idAttribute);
            if (child != null) {
                childContainer.push(child);
            }
            // sibling traversal is quicker than node.childNodes 
            currentElementChild = currentElementChild.nextSibling;
        }
        return childContainer;
    case 1:  //   Node.ELEMENT_NODE
    case 11: //   Node.DOCUMENT_FRAGMENT_NODE
        var nodeContainer = new domContainer(domNode, idAttribute);
        
        var currentElementChild = null;
        try {
            currentElementChild = domNode.firstChild;
        } catch (e) {/**ignore*/}

        while (currentElementChild) {
            // !!! recursion !!!
            var child = traverseDOMTree(currentElementChild, idAttribute);
            if (child != null) {
                // setting the parent child index position
                nodeContainer.children.push(child.position);
            }
            
            // sibling traversal is quicker than node.childNodes 
            currentElementChild = currentElementChild.nextSibling;
        }
        
        // delete children if empty
        if (nodeContainer.children.length == 0) {
            nodeContainer.children = 0;
            delete nodeContainer.children;
        }
        nodeContainer.position = UIDOMelements.length; 
        UIDOMelements.push(nodeContainer);
        return nodeContainer;
    }
    return null;
}

/** traverse all available DOMs e.g. also embedded frames and iFrames */
function traverseAllDOMs(passedWindow, idAttribute) {
    var doc = passedWindow.document;
    // this is used to build the document parent <-> child hierarchy
    var children = new Array();
    
    // traverse the inner document hierarchy
    var traversedDOM = traverseDOMTree(doc, idAttribute);

    if (traversedDOM) {
        children.push(traversedDOM);
    }

    var innerFrames = passedWindow.frames;
    if (innerFrames.length > 0) {
        for (var i = 0; i < innerFrames.length; i++) {
            var frameContainer = new domContainer();
            frameContainer.attributes.nodeName = 'uiframe';
            // traverse the cross document hierarchy
            // !!! recursion
            var innerDocuments = traverseAllDOMs(innerFrames[i], idAttribute);
            if (innerDocuments) {
                for (var j = 0; j < innerDocuments.length; j++) {
	                var innerElements = innerDocuments[j];
		            if (innerElements) {
		                if (innerElements instanceof Array) {
		                	// add inner document hierarchies
		                	for (var k = 0; k < innerElements.length; k++) {
		                        frameContainer.children.push(innerElements[k].position);
	                    	}
		            	} else { 
		            		// add cross document hierarchies
			                frameContainer.children.push(innerElements.position);
		            	}
                	}
                }
            }
            // delete children if empty
            if (frameContainer.children.length == 0) {
                frameContainer.children = 0;
                delete frameContainer.children;
            }
            frameContainer.position = UIDOMelements.length; 
            UIDOMelements.push(frameContainer);
            children.push(frameContainer);
        }
    }
    return children;
}

/** returns the DOM as a cascaded JSON string */
function getDomAsJSON(passedWindow, idAttribute) {
    UIDOMelements = new Array();
    traverseAllDOMs(passedWindow, idAttribute);
    var jsonFilter = new Array();
    /** properties to filter during serialization */
    jsonFilter.push("domContainer");
    
    /** attributes + the collected attributes themselves */
    jsonFilter.push("attributes");
    jsonFilter.push("nodeName");
    jsonFilter.push("id");
    jsonFilter.push("type");
    jsonFilter.push("size");
    jsonFilter.push("role");
    jsonFilter.push("className");
    
    /** the children property of each domContainer */
    jsonFilter.push("children");
    
    return JSON.stringify(UIDOMelements, jsonFilter);
}