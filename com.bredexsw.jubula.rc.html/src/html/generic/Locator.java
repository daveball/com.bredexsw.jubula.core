/*******************************************************************************
 * Copyright (c) 2015 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package html.generic;

import html.Element;


/**
 * Synthetic class for pseudo locator element 
 *
 * @author BREDEX GmbH
 *
 */
public class Locator extends Element {
 // empty
}
