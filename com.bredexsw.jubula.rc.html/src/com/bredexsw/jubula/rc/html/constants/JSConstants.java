/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.constants;

import java.lang.reflect.InvocationTargetException;

import com.bredexsw.jubula.rc.html.utils.IOUtil;

/**
 * @author BREDEX GmbH
 * @created 07.09.2009
 *
 */
public class JSConstants {

    /**
     * <code>AUT_CONTEXT_PREFIX</code> prefix to run the in AUT java script
     * context
     */
    public static final String AUT_CONTEXT_PREFIX = "window."; //$NON-NLS-1$

    /** <code>JS_OMM_START</code> js call to start omm; */
    public static final String JS_OMM_START = AUT_CONTEXT_PREFIX
            + "omm.start();"; //$NON-NLS-1$

    /** <code>JS_OMM_STOP</code> js call to stop omm; */
    public static final String JS_OMM_STOP = AUT_CONTEXT_PREFIX + "omm.stop();"; //$NON-NLS-1$

    private static final String JS_PATH = "/lib/gen-js/";

    /** <code>OBJECT_MAPPING_SCRIPT</code> */
    private static final String OBJECT_MAPPING_SCRIPT = JS_PATH
            + "objectMapping.js"; //$NON-NLS-1$

    /** <code>TRAVERSE_DOM_SCRIPT</code> */
    private static final String TRAVERSE_DOM_SCRIPT = JS_PATH
            + "traverseDOM.js"; //$NON-NLS-1$

    /** <code>HIGHLIGHT_BORDER_SCRIPT</code> */
    private static final String HIGHLIGHT_BORDER_SCRIPT = JS_PATH
            + "highlightElementBorder.js";

    /** <code>RESTORE_BORDER_SCRIPT</code> */
    private static final String RESTORE_BORDER_SCRIPT = JS_PATH
            + "restoreElementBorder.js";

    /**
     * hide default constructor
     */
    private JSConstants() {
        // hide
    }

    /**
     * call from <b>SELENIUM CONTEXT</b>
     * 
     * @return js to get a json string of component identifier;
     */
    public static String getJSOMMGetCollectedTechnicalComponents() {
        /*
         * add system current millisecs to avoid blocking of js execution
         * calling the same command too often --> Selenium blocks execution
         */
        return "return " + AUT_CONTEXT_PREFIX + "omm.getTechnicalComponents(" //$NON-NLS-1$
                + System.currentTimeMillis() + ");"; //$NON-NLS-1$
    }

    /**
     * @param idAttributeName the name of the attribute to regard as the ID =
     *            technical name of the component
     * @return js to get a json string of component identifier;
     */
    public static String getJSAUTHierarchyGetAllTechnicalComponents(
        String idAttributeName) {
        return "getDomAsJSON(window, '" + idAttributeName + "')"; //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * @param jsonSupportedTypes a JSON serialized string of supported Type
     *            identifier
     * @return js to create a new omm object
     */
    @SuppressWarnings("nls")
    public static String getJSCreateOMM(String jsonSupportedTypes) {
        return AUT_CONTEXT_PREFIX + "omm = new omm('" + jsonSupportedTypes
                + "');";
    }

    /**
     * @return js which implements the omm in the browser
     */
    public static String getJSGetOMMScript() throws InvocationTargetException {
        return IOUtil.readResourceAsString(OBJECT_MAPPING_SCRIPT);
    }

    /**
     * @return js which fetches the full DOM hierarchy from the browser
     */
    public static String getJSGetTraverseDOMScript()
        throws InvocationTargetException {
        return IOUtil.readResourceAsString(TRAVERSE_DOM_SCRIPT);
    }

    /**
     * @return js which highlights an elements border.
     */
    public static String getJSGetHighlightBorderScript()
        throws InvocationTargetException {
        return IOUtil.readResourceAsString(HIGHLIGHT_BORDER_SCRIPT);
    }

    /**
     * @return js which restores an elements border to it's previous state.
     */
    public static String getJSGetRestoreBorderScript()
        throws InvocationTargetException {
        return IOUtil.readResourceAsString(RESTORE_BORDER_SCRIPT);
    }
}
