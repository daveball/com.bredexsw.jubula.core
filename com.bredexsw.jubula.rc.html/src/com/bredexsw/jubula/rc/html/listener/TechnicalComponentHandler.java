/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.listener;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.tools.internal.constants.TimingConstantsServer;
import org.eclipse.jubula.tools.internal.messagehandling.MessageIDs;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.components.AUTHtmlHierarchy;
import com.bredexsw.jubula.rc.html.components.AbstractAUTHtmlHierarchy;
import com.bredexsw.jubula.rc.html.components.json.JSONConstantsObjectMapping;
import com.bredexsw.jubula.rc.html.web.driver.AbstractRobotUser;
import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;


/**
 * Helper class to handle techincal components
 * 
 * @author BREDEX GmbH
 * @created 07.09.2009
 *
 */
public class TechnicalComponentHandler extends AbstractRobotUser {
    /**
     * <code>instance</code> singleton
     */
    private static TechnicalComponentHandler instance = null;    
    
    /**
     * <code>log</code>: the logger
     */
    private static Logger log = 
        LoggerFactory.getLogger(TechnicalComponentHandler.class);
    
    /** the Container hierarchy of the AUT */
    private static AbstractAUTHtmlHierarchy autHierarchy =
            new AUTHtmlHierarchy();
    
    /**
     * Hide Constructor
     */
    private TechnicalComponentHandler() {
    // hide
    }

    /**
     * @return the singleton instance
     */
    public static TechnicalComponentHandler getInstance() {
        if (instance == null) {
            instance = new TechnicalComponentHandler();
        }
        return instance;
    }
    
    /**
     * Investigates the given <code>component</code> for an identifier. It must
     * be distinct for the whole AUT.
     * 
     * @param jSONComponents
     *            the JSON components to get an identifier for
     * @return a list of identifier, containing the identification
     */
    public Collection<IComponentIdentifier> 
        getIdentifier(String jSONComponents) {
        Collection<IComponentIdentifier> a = 
                new ArrayList<IComponentIdentifier>();
        try {
            JSONArray jsonArray = JSONArray.fromObject(jSONComponents);
            for (Object o : jsonArray) {
                if (o instanceof JSONObject) {
                    JSONObject jo = (JSONObject)o;
                    String fqPath = jo.getString(
                                    JSONConstantsObjectMapping
                                        .TECH_COMP_X_PATH_KEY);
                    IComponentIdentifier ci = autHierarchy
                            .getComponentIdentifier(fqPath);
                    if (ci != null) {
                        a.add(ci);
                    }
                }
            }
        } catch (JSONException e) {
            log.error("Failed to collect components returned from the browser [" + jSONComponents + "]", e);
        }
        return a;
    }

    
    /**
     * @param compId The component id for which to find the web component.
     * @param timeout
     *      timeout for retries, in milliseconds
     * @return The web component best matching the given id, or 
     *         <code>null</code> if no such component could be found within
     *         the given time.
     */
    public AbstractWebComponent findComponent(IComponentIdentifier compId,
            int timeout) throws ComponentNotFoundException,
            IllegalArgumentException {
        long start = System.currentTimeMillis();
        AbstractWebComponent webComp = null;
        boolean timoutExpired = false;
        do {
            try {
                webComp = autHierarchy.findComponent(compId);
                try {
                    Thread.sleep(TimingConstantsServer.POLLING_DELAY_FIND_COMPONENT);
                } catch (InterruptedException e) {
                    // ok
                }
            } catch (ComponentNotFoundException cnf) {
                // ignore during search
            } catch (IllegalArgumentException iae) {
                log.error(iae.getLocalizedMessage(), iae);
                throw iae;
            } finally {
                timoutExpired = System.currentTimeMillis() - start > timeout;
            }
        } while (webComp == null && !timoutExpired);

        if (webComp == null) {
            throw new ComponentNotFoundException(
                    "component not found: " + compId, //$NON-NLS-1$
                    MessageIDs.E_COMPONENT_NOT_FOUND);
        }
        return webComp;
    }
    
    /**
     * clean the hierarchy
     */
    public void cleanHierarchy() {
        autHierarchy.clean();
    }
}
