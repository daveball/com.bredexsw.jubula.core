/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.listener;


import org.eclipse.jubula.rc.common.AUTServerConfiguration;
import org.eclipse.jubula.rc.common.businessprocess.ReflectionBP;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.rc.common.exception.UnsupportedComponentException;
import org.eclipse.jubula.rc.common.listener.AUTEventListener;
import org.eclipse.jubula.rc.common.logger.AutServerLogger;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;

import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;


/**
 * @author BREDEX GmbH
 * @created 03.09.2009
 *
 */
public class MappingListener implements AUTEventListener {
    /** the logger */
    private static AutServerLogger log = new AutServerLogger(
            MappingListener.class);

    /**
     * {@inheritDoc}
     */
    public void cleanUp() {
    // empty
    }

    /**
     * {@inheritDoc}
     */
    public boolean highlightComponent(IComponentIdentifier compId) {
        Object implClass;
        AbstractWebComponent component = null;
        try {
            component = TechnicalComponentHandler.getInstance()
                .findComponent(compId, 0);
            if (component != null) {
                implClass = AUTServerConfiguration.getInstance()
                    .getImplementationClass(component.getClass());
                ReflectionBP.invokeMethod("highLight", //$NON-NLS-1$
                        implClass, new Class[] {AbstractWebComponent.class}, 
                        new Object[] {component});
                return true;
            }
        } catch (ComponentNotFoundException e) {
            log.warn(e);
        } catch (UnsupportedComponentException e) {
            log.warn(e);
        } catch (IllegalArgumentException e) {
            log.warn(e);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void update() {
    // empty
    }

    /**
     * {@inheritDoc}
     */
    public long[] getEventMask() {
        return null;
    }
}
