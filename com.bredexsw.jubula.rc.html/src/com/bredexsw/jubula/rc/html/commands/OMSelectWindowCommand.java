/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.commands;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jubula.communication.internal.ICommand;
import org.eclipse.jubula.communication.internal.message.Message;
import org.eclipse.jubula.communication.internal.message.html.OMSelectWindowMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;
import com.bredexsw.jubula.rc.html.web.driver.omm.ObjectCollectionMode;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class OMSelectWindowCommand implements ICommand {
    
    /** the logger */
    private static Logger log = LoggerFactory.getLogger(
        OMSelectWindowCommand.class);
    
    /** the selection message, with the window title */
    private OMSelectWindowMessage m_message;
    /**
     * {@inheritDoc}
     */
    public Message getMessage() {
        return m_message;
    }

    /**
     * {@inheritDoc}
     */
    public void setMessage(Message message) {
        m_message = (OMSelectWindowMessage) message;

    }

    /**
     * {@inheritDoc}
     */
    public Message execute() {
        RobotWebImpl robot = WebAUTServer.getInstance().getWebRobot();
        
        try {  
            ObjectCollectionMode.getInstance().stop();

        } catch (InvocationTargetException ite) {
            // the old window is already closed
        } finally {
            robot.selectWindow(m_message.getWindowTitle());
            try {
                ObjectCollectionMode.getInstance().start();
            } catch (InvocationTargetException ite) {
                robot.selectWindow(null);
                // FIXME MMue send a Error to the Client
            }

        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void timeout() {
        log.error(this.getClass().getName() + ".timeout() called");
    }

}
