/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.KeyStroke;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.logger.AutServerLogger;
import org.eclipse.jubula.rc.common.util.LocalScreenshotUtil;
import org.eclipse.jubula.rc.common.util.PointUtil;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.toolkit.enums.ValueSets.InteractionMode;
import org.eclipse.jubula.toolkit.html.Browser;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.EnvironmentUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.constants.JSConstants;
import com.bredexsw.jubula.rc.html.listener.TechnicalComponentHandler;
import com.bredexsw.jubula.rc.html.utils.BrowserEnvironmentUtil;
import com.bredexsw.jubula.rc.html.web.driver.omm.ObjectCollectionMode;
import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;

/**
 * The web robot implementation
 * 
 * @author BREDEX GmbH
 * @created 13.08.2009
 */
public class RobotWebImpl implements IRobot<Rectangle> {

    private static final int INITIAL_URL_TIMEOUT = 5000;

    /** set to enable */
    public static final String TEST_HTML_TRUST_ALL_SSL_CERTIFICATES = "TEST_HTML_TRUST_ALL_SSL_CERTIFICATES"; //$NON-NLS-1$

    /** set FF template profile */
    public static final String TEST_HTML_FF_PROFILE_TEMPLATE = "TEST_HTML_FF_PROFILE_TEMPLATE"; //$NON-NLS-1$

    /** the logger */
    private static final AutServerLogger LOG = new AutServerLogger(
            RobotWebImpl.class);
    /**
     * <code>JS_INTRO</code> the javascript intro string
     */
    private static final String JS_INTRO = "{javascript:"; //$NON-NLS-1$

    /**
     * <code>JS_OUTRO</code> the javascript outro string
     */
    private static final String JS_OUTRO = "}"; //$NON-NLS-1$

    /**
     * <code>ERROR</code> string that indicates that an error occurred during
     * javascript evaluation
     */
    private static final String ERROR = "ERROR: "; //$NON-NLS-1$

    /**
     * <code>SAFETY_OUTRO</code>
     */
    private static final String SAFETY_OUTRO = "}catch(err){return \"" + ERROR //$NON-NLS-1$
            + "\" + err}"; //$NON-NLS-1$

    /**
     * <code>SAFETY_INTRO</code>
     */
    private static final String SAFETY_INTRO = "try{"; //$NON-NLS-1$

    /**
     * <code>idAttribute</code> the attribute to take as the id of the technical
     * component
     */
    private String m_idAttribute;

    /**
     * the keytyper to use (default or replace behaviour)
     */
    private IKeyTyper m_keytyper;

    /**
     * the WebDriver instance
     */
    private WebDriver m_webdriver = null;

    /**
     * indicates whether the browser and it's session has been launched
     */
    private boolean m_launched = false;

    /**
     * indicates whether the robot has currently a frame selected
     */
    private boolean m_frameSelected = false;

    private WebElement lastElem = null;
    private String lastBorder = null;
    private String SCRIPT_RESTORE_ELEMENT_BORDER;
    private String SCRIPT_GET_ELEMENT_BORDER;

    /**
     * @param browser the type of browser to start
     * @param browserExecutablePath the path to the browser executable, or null
     *            if the default path should be used.
     * @param browserURL the browser start URL
     * @param idAttribute the attribute to regard as the id / technical naming
     *            attribute
     * @param browserSize Dimensions to use for the browser window, or null for
     *            fullscreen.
     */
    public RobotWebImpl(Browser broswer, String browserExecutablePath,
            String browserURL, String idAttribute, Dimension browserSize)
            throws InvocationTargetException {

        ObjectCollectionMode.getInstance().init();
        TechnicalComponentHandler.getInstance();

        SCRIPT_GET_ELEMENT_BORDER = JSConstants.getJSGetHighlightBorderScript();
        SCRIPT_RESTORE_ELEMENT_BORDER = JSConstants
                .getJSGetRestoreBorderScript();

        setIdAttribute(idAttribute);
        startWebdriver(broswer, browserExecutablePath, browserSize);
        
        long startMillis = System.currentTimeMillis();
        do { 
            try {
                m_webdriver.navigate().to(browserURL);
                continue;
            } catch (Exception e) {
                // Browser might still be initialising, so retry...
            }
        } while ((System.currentTimeMillis() - startMillis) < INITIAL_URL_TIMEOUT);
    }

    /**
     * use this method to safely run javascript snippets in the context of the
     * selenium robot
     * 
     * @param js the java script
     * @throws InvocationTargetException in case that the specified java script
     *             is invalid
     * @return the correct evaluation of the given java script snippet
     */
    public String js(String js, Object... arguments)
        throws InvocationTargetException {
        String javaScript = JS_INTRO + SAFETY_INTRO + js + SAFETY_OUTRO
                + JS_OUTRO;

        try {
            Object returnedObject = executeScript(javaScript, arguments);
            if (returnedObject != null) {
                String returnValue = returnedObject.toString();
                if (returnValue.startsWith(ERROR)) {
                    throw new InvocationTargetException(new Exception(),
                            returnValue);
                } else {
                    return returnValue;
                }
            }
        } catch (WebDriverException e) {
            throw new InvocationTargetException(e);
        }
        return "";
    }

    private Object executeScript(String javaScript, Object... arguments) {
        if (!(m_webdriver instanceof JavascriptExecutor)) {
            abort("Cannot run script, as driver doesn't support Javascript.");
        }
        JavascriptExecutor jsDriver = (JavascriptExecutor) m_webdriver;
        return jsDriver.executeScript(javaScript, arguments);
    }

    public void setAttribute(WebElement element, String attribute, Object value) {
        try {
            executeScript(";arguments[0].setAttribute('" + attribute
                    + "', arguments[1]);", element, value);
        } catch (Exception e) {
            throw new StepExecutionException(e);
        }
    }

    public void setSelected(WebElement element, boolean value) {
        try {
            executeScript(";arguments[0].selected = arguments[1];", element,
                    value);
        } catch (Exception e) {
            throw new StepExecutionException(e);
        }
    }

    /**
     * start the WebDriver instance
     * @param browser the type of browser to start
     * @param browserExecutablePath the path to the browser executable, or null
     *            if the default executable should be used.
     * @param browserSize Dimensions to use for the browser window, or null for
     *            fullscreen.
     * 
     * @throws InvocationTargetException If an error occurs when starting
     *             WebDriver.
     */
    private void startWebdriver(Browser broswer, String browserExecutablePath,
        Dimension browserSize) throws InvocationTargetException {
        LOG.info("Starting webdriver for  " + broswer); //$NON-NLS-1$
        try {
            m_webdriver = webdriverFor(broswer, browserExecutablePath);

            if (browserSize == null) {
                m_webdriver.manage().window().maximize();
            } else {
                m_webdriver.manage().window().setSize(browserSize);
            }
            m_webdriver.manage().timeouts()
                    .implicitlyWait(50, TimeUnit.MILLISECONDS);

        } catch (Exception e) {
            throw new InvocationTargetException(e, e.getMessage());
        }
    }

    private WebDriver webdriverFor(Browser broswer, String browserExecutablePath) {
        // TODO - currently different browsers use browserExecutablePath
        // differently.
        // FF assumes it's the path to the browser.
        // Chrome and IE use it as the path to the DRIVER, not the broser
        switch (broswer) {
        case Chrome:
            if (browserExecutablePath != null) {
                System.setProperty("webdriver.chrome.driver",
                        browserExecutablePath);
            }
            DesiredCapabilities chromeCapabilities = DesiredCapabilities
                    .chrome();
            return new ChromeDriver(chromeCapabilities);
        case Firefox:
            if (browserExecutablePath != null) {
                FirefoxBinary ffBinary = new FirefoxBinary(new File(
                        browserExecutablePath));
                FirefoxProfile ffProfile = new FirefoxProfile();
                ffProfile.setEnableNativeEvents(true);
                return new FirefoxDriver(ffBinary, ffProfile);
            } else {
                return new FirefoxDriver();
            }
        case InternetExplorer:
            return launchIeDriver(browserExecutablePath);
        case Safari:
            DesiredCapabilities safariCapabilities = DesiredCapabilities
                    .safari();
            return new SafariDriver(safariCapabilities);

            // case IPhone:
            // String remoteBrowserUri = "http://157.59.109.235:8080";
            // return new RemoteWebDriver(remoteBrowserUri,
            // DesiredCapabilities.iphone());
        default:
            throw new IllegalArgumentException("Browser type [" + broswer //$NON-NLS-1$
                    + "] not supported."); //$NON-NLS-1$
        }
    }

    private WebDriver launchIeDriver(String browserExecutablePath) {
        if (browserExecutablePath == null ) {
            throw new IllegalArgumentException("IE Driver requires the path to IEDriverServer.exe");
        }
        System.setProperty("webdriver.ie.driver", browserExecutablePath);
        // System.setProperty("webdriver.ie.driver.loglevel", "DEBUG");
        // System.setProperty("webdriver.ie.driver.logfile", "C:\IEDriverServer.log");
        DesiredCapabilities ieCapabilities = DesiredCapabilities
                .internetExplorer();
        ieCapabilities
                .setCapability(
                        InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                        true);
        try{
            // IE Driver is not threadsafe. See https://github.com/SeleniumHQ/selenium/issues/422
            return new SychronisedInternetExplorerDriver(ieCapabilities);
        }catch(Exception e){
            // https://groups.google.com/forum/#!msg/selenium-users/q7rlXVQn8Q0/WV-NpBHkR3UJ
            throw new IllegalArgumentException("Unable to start IE Driver. Please make sure 'localhost' resolves to 127.0.0.1 (not ipv6 localhost)", e);
        }
    }

    /** {@inheritDoc} */
    public void activateApplication(String method) throws RobotException {
        // null as target for main window
        selectWindow(null);
    }

    public void click(String locator) {
        WebElement element = getElement(locator);
        focus(element);
        element.click();
    }

    /** {@inheritDoc} */
    public void click(Object graphicsComponent, Rectangle constraints)
        throws RobotException {
        click(graphicsComponent, constraints, ClickOptions.create()
                .setClickCount(1));
    }

    /** {@inheritDoc} */
    public void click(Object graphicsComponent, Rectangle constraints,
        ClickOptions clickOptions, int xPos, boolean xAbsolute, int yPos,
        boolean yAbsolute) throws RobotException {
        Validate.isTrue(
                graphicsComponent instanceof AbstractWebComponent,
                "graphics component must be an instance of " + AbstractWebComponent.class.getName()); //$NON-NLS-1$
        AbstractWebComponent webComponent = (AbstractWebComponent) graphicsComponent;
        String locator = webComponent.getLocator();
        click(locator, constraints, clickOptions, xPos, xAbsolute, yPos,
                yAbsolute);
    }

    /**
     * Performs a mouse click with the specified mouse button on the given
     * Selenium locator component. The click is configured by
     * <code>clickOptions</code>.
     * 
     * @param locator a valid Selenium locator to use for clicking
     * @param constraints A constraints object used by the Robot implementation.
     *            Any coordinates contained in the constraints must be
     *            <em>relative</em> to <code>graphicsComponent</code>'s
     *            coordinate system. May be <code>null</code>.
     * @param clickOptions clickOptions The click options
     * @param xPos xPos in component
     * @param xAbsolute absolute true if position should be absolute
     * @param yPos yPos in component
     * @param yAbsolute absolute true if position should be absolute
     */
    public void click(String locator, Rectangle constraints,
        ClickOptions clickOptions, int xPos, boolean xAbsolute, int yPos,
        boolean yAbsolute) {

        WebElement element = getElement(locator);
        waitUntilClickable(element);

        Rectangle constraintsToUse = constraints;
        if (constraintsToUse == null) {
            constraintsToUse = new Rectangle(0, 0, element.getSize().height,
                    element.getSize().width);
        }
        Point p = PointUtil.calculateAwtPointToGo(xPos, xAbsolute, yPos,
                yAbsolute, constraintsToUse);

        for (int i = 0; i < clickOptions.getClickCount(); i++) {
            Actions action = new Actions(m_webdriver);
            action.moveToElement(element, p.y, p.x);
            if (clickOptions.getMouseButton() == InteractionMode.primary
                    .rcIntValue()) {
                if (i % 2 == 1) {
                    action.doubleClick();
                } else {
                    action.click();
                }
            } else if (clickOptions.getMouseButton() == InteractionMode.secondary
                    .rcIntValue()) {
                action.contextClick();
            }
            action.perform();

            // FIXME zeb what should we do for middle mouse button?
            // FIXME zeb what should we do for invalid
            // (i.e. not right, left, or middle) mouse button?
        }
    }

    /**
     * gets the keytyper, default for firefox, special typer for other browsers
     * (replace behaviour)
     * 
     * @return keytyper
     */
    private IKeyTyper getKeyTyper() {
        if (m_keytyper == null) {
            m_keytyper = new KeyTyperReplaceImpl();
        }
        return m_keytyper;
    }

    /** {@inheritDoc} */
    public void click(Object graphicsComponent, Rectangle constraints,
        ClickOptions clickOptions) throws RobotException {

        if (clickOptions.getMouseButton() == InteractionMode.primary
                .rcIntValue()) {

            WebElement element = getElement(graphicsComponent);
            waitUntilClickable(element);

            for (int i = 0; i < clickOptions.getClickCount(); i++) {
                element.click();
            }
        } else {
            click(graphicsComponent, constraints, clickOptions, 50, false, 50,
                    false);
        }
    }

    /**
     * Simulate a mouseOver a web element, and then wait until the element can
     * be clicked. This improves support for complex web toolkits, where element
     * state changes using JavaScript.
     */
    private void waitUntilClickable(WebElement element) {
        // mouse over our element.
        new Actions(m_webdriver).moveToElement(element).perform();
        try {
            new WebDriverWait(m_webdriver, 5).until(ExpectedConditions
                    .elementToBeClickable(element));
        } catch (WebDriverException e) {
            throw new RobotException("Component was not clickable.", //$NON-NLS-1$
                    new TestErrorEvent(TestErrorEvent.COMP_NOT_FOUND));
        }
    }

    /** {@inheritDoc} */
    public void clickAtCurrentPosition(Object graphicsComponent,
        int clickCount, int button) throws RobotException {

        for (int i = 0; i < clickCount; i++) {
            Actions action = new Actions(m_webdriver);
            if (button == InteractionMode.primary.rcIntValue()) {
                action.click();
            } else if (button == InteractionMode.secondary.rcIntValue()) {
                action.contextClick();
            }
            action.perform();
        }
    }

    /** {@inheritDoc} */
    public Point getCurrentMousePosition() {
        LOG.warn("Unimplimented getCurrentMousePosition........................... ");

        org.openqa.selenium.Point position = m_webdriver.manage().window().getPosition();
        int x = position.x;
        int y = position.y;

        try{
            Dimension size = m_webdriver.manage().window().getSize();
            x = x + (size.width / 2);
            y = y + (size.height / 2);
        } catch (Exception e){
            // work around defect in IEDriver that casts a string to a map.
        }

        return new Point(x, y);
    }

    /** {@inheritDoc} */
    public String getSystemModifierSpec() {
        String keyStrokeSpec = ValueSets.Modifier.control.rcValue();
        if (EnvironmentUtils.isMacOS()) {
            keyStrokeSpec = ValueSets.Modifier.meta.rcValue();
        }
        return keyStrokeSpec;
    }

    /** {@inheritDoc} */
    public boolean isMouseInComponent(Object graphicsComponent) {
        WebElement element = getElement(graphicsComponent);
        Point point = getCurrentMousePosition();
        Rectangle bounds = new Rectangle(element.getLocation().x,
                element.getLocation().y, element.getLocation().x
                        + element.getSize().height, element.getLocation().y
                        + element.getSize().width);
        return bounds.contains(point);
    }

    /** {@inheritDoc} */
    public void keyPress(Object graphicsComponent, int keycode)
        throws RobotException {
        Actions action = new Actions(m_webdriver);
        if (graphicsComponent != null) {
            WebElement element = getElement(graphicsComponent);
            action.keyDown(element, Keys.getKeyFromUnicode((char) keycode));
        } else {
            action.keyDown(Keys.getKeyFromUnicode((char) keycode));
        }
        action.perform();
    }

    /** {@inheritDoc} */
    public void keyRelease(Object graphicsComponent, int keycode)
        throws RobotException {
        Actions action = new Actions(m_webdriver);
        if (graphicsComponent != null) {
            WebElement element = getElement(graphicsComponent);
            action.keyUp(element, Keys.getKeyFromUnicode((char) keycode));
        } else {
            action.keyUp(Keys.getKeyFromUnicode((char) keycode));
        }
        action.perform();
    }

    /** {@inheritDoc} */
    public void keyStroke(String keyStrokeSpecification) throws RobotException {

        KeyStroke keyStroke = KeyStroke.getKeyStroke(keyStrokeSpecification);

        if (keyStroke.getKeyCode() == KeyEvent.VK_F5
                || (keyStroke.getKeyCode() == KeyEvent.VK_ESCAPE && BrowserEnvironmentUtil
                        .isFirefox(this))) {
            return;
        }

        Actions action = new Actions(m_webdriver);
        String chord = chordFromKeyStroke(keyStroke);
        action.sendKeys(chord);
        action.perform();
    }

    /**
     * @param keyStroke KeyStroke whose chord is requested
     * @return a chord (hopefully) realising the KeyStroke
     */
    public static String chordFromKeyStroke(KeyStroke keyStroke) {
        StringBuilder chorde = new StringBuilder();
        int modifiers = keyStroke.getModifiers();
        if ((modifiers & InputEvent.ALT_MASK) != 0) {
            chorde.append(Keys.ALT);
        }
        if ((modifiers & InputEvent.ALT_GRAPH_MASK) != 0) {
            chorde.append(Keys.ALT);
            chorde.append(Keys.CONTROL);
        }
        if ((modifiers & InputEvent.CTRL_MASK) != 0) {
            chorde.append(Keys.CONTROL);
        }
        if ((modifiers & InputEvent.SHIFT_MASK) != 0) {
            chorde.append(Keys.SHIFT);
        }
        if ((modifiers & InputEvent.META_MASK) != 0) {
            chorde.append(Keys.META);
        }
        if (keyStroke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            chorde.append(Keys.ESCAPE);
        } else if (keyStroke.getKeyCode() == KeyEvent.VK_END) {
            chorde.append(Keys.END);
        } else if (keyStroke.getKeyCode() == KeyEvent.VK_HOME) {
            chorde.append(Keys.HOME);
        } else {
            chorde.append((char) keyStroke.getKeyCode());
        }
        return Keys.chord(chorde.toString());
    }

    /** {@inheritDoc} */
    public void keyToggle(Object graphicsComponent, int key, boolean activated) {
        if (activated) {
            keyPress(graphicsComponent, key);
        } else {
            keyRelease(graphicsComponent, key);
        }
    }

    /** {@inheritDoc} */
    public void keyType(Object graphicsComponent, int keycode)
        throws RobotException {
        Actions action = new Actions(m_webdriver);
        if (graphicsComponent != null) {
            WebElement element = getElement(graphicsComponent);
            action.sendKeys(element, Keys.getKeyFromUnicode((char) keycode));
        } else {
            action.sendKeys(Keys.getKeyFromUnicode((char) keycode));
        }
        action.perform();
    }

    public void sendText(AbstractWebComponent comp, String newText) {
        WebElement element = getElement(comp.getLocator());
        element.sendKeys(newText);
    }

    public void setText(AbstractWebComponent comp, String newText) {
        WebElement element = getElement(comp.getLocator());
        element.clear();
        element.sendKeys(newText);
    }

    /** {@inheritDoc} */
    public void mousePress(Object graphicsComponent, Rectangle constraints,
        int button) throws RobotException {
        if (button != InteractionMode.primary.rcIntValue()) {
            throw new RobotException(
                    "HTML AUT only supports mouse hold/release for the Primary mouse button.",
                    new TestErrorEvent(
                            TestErrorEvent.UNSUPPORTED_OPERATION_ERROR));
        }
        move(graphicsComponent, constraints);
        new Actions(m_webdriver).clickAndHold().perform();
    }

    /** {@inheritDoc} */
    public void mouseRelease(Object graphicsComponent, Rectangle constraints,
        int button) throws RobotException {
        if (button != InteractionMode.primary.rcIntValue()) {
            throw new RobotException(
                    "HTML AUT only supports mouse hold/release for the Primary mouse button.",
                    new TestErrorEvent(
                            TestErrorEvent.UNSUPPORTED_OPERATION_ERROR));
        }
        move(graphicsComponent, constraints);
        new Actions(m_webdriver).release().perform();
    }

    /** {@inheritDoc} */
    public void move(Object graphicsComponent, Rectangle constraints)
        throws RobotException {
        WebElement element = getElement(graphicsComponent);
        // TODO - use constraints for something?
        new Actions(m_webdriver).moveToElement(element).perform();
    }

    /** {@inheritDoc} */
    public void scrollToVisible(Object graphicsComponent, Rectangle constraints)
        throws RobotException {
        LOG.info("Unimplimented scrollToVisible..................... ");
        // no need to implement
    }

    /** {@inheritDoc} */
    public void type(Object graphicsComponent, char character)
        throws RobotException {
        type(graphicsComponent, "" + character, 0);
    }

    /** {@inheritDoc} */
    public void type(Object graphicsComponent, String text)
        throws RobotException {
        type(graphicsComponent, text, 0);
    }

    /**
     * insert text at specific cursor position
     * 
     * @param graphicsComponent AbstractWebComponent
     * @param text Text to enter
     * @param index cursor position
     */
    public void type(Object graphicsComponent, String text, int index)
        throws RobotException {
        getKeyTyper().type(graphicsComponent, text, index);
    }

    /**
     * @param idAttribute the idAttribute to set
     */
    private void setIdAttribute(String idAttribute) {
        m_idAttribute = idAttribute;
    }

    /**
     * @return the idAttribute for naming technical components
     */
    public String getIdAttribute() {
        return m_idAttribute;
    }

    /** {@inheritDoc} */
    public String getPropertyValue(Object graphicsComponent, String propertyName)
        throws RobotException {
        try {
            return getAttribute(
                    ((AbstractWebComponent) graphicsComponent).getLocator(),
                    propertyName);
        } catch (WebDriverException se) {
            throw new RobotException(se);
        }
    }

    /** {@inheritDoc} */
    public void stop() {
        setLaunched(false);
        if (m_webdriver != null) {
            m_webdriver.quit();
        }
    }

    /**
     * @return the launched
     */
    public boolean isLaunched() {
        return m_launched;
    }

    /**
     * @param launched the launched to set
     */
    public void setLaunched(boolean launched) {
        m_launched = launched;
    }

    /** {@inheritDoc} */
    public void selectFrame(String locator) {
        try {
            if (locator == null
                    || WebAUTServer.ROOT_WINDOW_PATH.equals(locator)) {
                m_webdriver.switchTo().defaultContent();
                setFrameSelected(false);
            } else if (locator.startsWith("index=")) {
                int frameIdx = Integer.valueOf(locator.substring("index="
                        .length()));
                List<WebElement> frames = m_webdriver.findElements(By
                        .xpath("//frame | //iframe"));
                if (frames == null || frames.size() < frameIdx + 1) {
                    throw new StepExecutionException(
                            "Page does not contain an expected frame.",
                            new TestErrorEvent(TestErrorEvent.COMP_NOT_FOUND));
                }
                m_webdriver.switchTo().frame(frames.get(frameIdx));
                setFrameSelected(!WebAUTServer.ROOT_WINDOW_PATH.equals(locator));
            } else {
                WebElement frameElement = getElement(locator);
                m_webdriver.switchTo().frame(frameElement);
                setFrameSelected(true);
            }
        } catch (Exception e) {
            throw new StepExecutionException(e);
        }
    }

    /**
     * @return the frameSelected
     */
    public boolean isFrameSelected() {
        return m_frameSelected;
    }

    /**
     * @param frameSelected the frameSelected to set
     */
    private void setFrameSelected(boolean frameSelected) {
        m_frameSelected = frameSelected;
    }

    /** {@inheritDoc} */
    public BufferedImage createFullScreenCapture() {
        try {
            // TODO - window seems preferable, but is this unexpected
            // behaviour?.
            return createWindowCapture();
        } catch (Exception e) {
            LOG.info(
                    "Failed to fetch screenshot from webdriver - faalingback to fullscreen.",
                    e);
            return LocalScreenshotUtil.createFullScreenCapture();
        }
    }

    public BufferedImage createWindowCapture() throws IOException {
        byte[] screenshot = ((TakesScreenshot) m_webdriver)
                .getScreenshotAs(OutputType.BYTES);
        InputStream in = new ByteArrayInputStream(screenshot);
        return ImageIO.read(in);
    }

    public String getAttribute(String locator, String attribute) {
        try {
            return getElement(locator).getAttribute(attribute);
        } catch (Exception e) {
            throw new StepExecutionException(e);
        }
    }

    private WebElement getElement(Object graphicsComponent) {
        Validate.isTrue(
                graphicsComponent instanceof AbstractWebComponent,
                "graphics component must be an instance of " + AbstractWebComponent.class.getName()); //$NON-NLS-1$
        AbstractWebComponent webComponent = (AbstractWebComponent) graphicsComponent;
        String locator = webComponent.getLocator();
        WebElement element = getElement(locator);
        return element;
    }

    public WebElement getElement(String locator) {
        try {
            locator = stripXPathHeader(locator);
            WebElement foundElement = m_webdriver
                    .findElement(By.xpath(locator));
            if (lastElem != null) {
                try {
                    js(SCRIPT_RESTORE_ELEMENT_BORDER, lastElem, lastBorder);
                } catch (InvocationTargetException ite) {
                    // lastElem might have been removed from the page, so lets
                    // ignore it.
                    lastElem = null;
                }
            }
            lastElem = foundElement;
            lastBorder = (js(SCRIPT_GET_ELEMENT_BORDER, foundElement));
            return foundElement;
        } catch (InvalidSelectorException ise) {
            throw ise;
        } catch (InvocationTargetException e) {
            throw new InvalidSelectorException(e.getMessage(), e);
        }
    }

    private String stripXPathHeader(String locator) {
        if (locator != null && locator.startsWith("xpath=")) {
            locator = locator.substring("xpath=".length());
        }
        return locator;
    }

    public Integer getXpathCount(String locator) {
        try {
            locator = stripXPathHeader(locator);
            List<WebElement> elements = m_webdriver.findElements(By
                    .xpath(locator));
            return elements == null ? 0 : elements.size();
        } catch (InvalidSelectorException ise) {
            throw ise;
        }
    }

    private String stripCssHeader(String locator) {
        if (locator.startsWith("css=")) {
            locator = locator.substring("css=".length());
        }
        return locator;
    }

    public Integer getCssCount(String locator) {
        try {
            locator = stripCssHeader(locator);
            List<WebElement> elements = m_webdriver.findElements(By
                    .cssSelector(locator));
            return elements == null ? 0 : elements.size();
        } catch (InvalidSelectorException ise) {
            throw ise;
        }
    }

    public boolean isElementPresent(String locator) {
        return getXpathCount(locator) > 0;
    }

    public boolean isVisible(String locator) {
        return getElement(locator).isDisplayed();
    }

    public void open(String URL) {
        m_webdriver.navigate().to(URL);
    }

    public String getLocation() {
        return m_webdriver.getCurrentUrl();
    }

    public void goBack() {
        m_webdriver.navigate().back();
    }

    public void refresh() {
        m_webdriver.navigate().refresh();
    }

    public String getWindowTitle() {
        return m_webdriver.getTitle();
    }

    public String[] getAllWindowTitles() {
        String currentWindow = m_webdriver.getWindowHandle();

        Set<String> windowTitles = new HashSet<String>();
        for (String handle : m_webdriver.getWindowHandles()) {
            m_webdriver.switchTo().window(handle);
            windowTitles.add(m_webdriver.getTitle());
        }

        m_webdriver.switchTo().window(currentWindow);
        return windowTitles.toArray(new String[windowTitles.size()]);
    }

    public void selectWindow(String selection) {
        if (selection == null) {
            m_webdriver.switchTo().defaultContent();
        } else {
            // TODO daveb: support "name=" or "title=" prefixes?
            m_webdriver.switchTo().frame(selection);
        }
    }

    public String getText(String locator) {
        // getText returns just the text, with "colapsed" spacing - Jubula
        // expects tabs to be intact
        String text = getElement(locator).getAttribute("innerHTML");
        // More oddly, jubula expects spaces to have been collapsed, but not
        // tabs...
        return text.replaceAll("[ ]+", " ");
    }

    public String getValue(String locator) {
        return getElement(locator).getAttribute("value");
    }

    public void select(AbstractWebComponent comp, int index) {
        Select select = new Select(getElement(comp));
        select.selectByIndex(index);
    }

    public String[] getSelectOptions(String locator) {
        WebElement element = getElement(locator);
        if (!"select".equals(element.getTagName())) {
            return new String[0];
        }

        // TODO daveb - investigate:
        // Select select = new Select(element);
        // SelectElement select = new SelectElement(element);
        // List<WebElement> options = select.Options;

        List<WebElement> optionElements = element.findElements(By
                .tagName("option"));
        String[] optionValues = new String[optionElements.size()];
        for (int i = 0; i < optionElements.size(); i++) {
            WebElement option = optionElements.get(i);
            optionValues[i] = option.getText();
        }
        return optionValues;
    }

    private void abort(String message) {
        LOG.warn(message); //$NON-NLS-1$
        throw new RobotException(message, new TestErrorEvent(
                TestErrorEvent.DROPDOWN_NOT_FOUND));
    }

    public Object addScript(String jsGetTraverseDOMScript,
        String domTraversalScriptId) {

        WebElement head = m_webdriver.findElement(By.tagName("head"));
        if (domTraversalScriptId != null) {
            for (WebElement scriptElement : head.findElements(By
                    .tagName("script"))) {
                if (domTraversalScriptId.equals(scriptElement
                        .getAttribute("id"))) {
                    abort("Cannot insert script, as an element with the same title already exists.");
                }
            }
        }
        StringBuilder insertionScript = new StringBuilder(";");
        insertionScript
                .append("var s=window.document.createElement('script');");
        insertionScript.append(" s.id='" + domTraversalScriptId + "'; ");
        insertionScript.append(" s.innerHTML=arguments[0]; ");
        insertionScript.append(" window.document.head.appendChild(s);");
        Object result = executeScript(insertionScript.toString(),
                jsGetTraverseDOMScript);
        return result;
    }

    public void removeScript(String domTraversalScriptId) {
        if (domTraversalScriptId == null) {
            abort("No script specified for removal.");
        }
        WebElement head = m_webdriver.findElement(By.tagName("head"));
        for (WebElement scriptElement : head.findElements(By.tagName("script"))) {
            if (domTraversalScriptId.equals(scriptElement.getAttribute("id"))) {
                executeScript(
                        "; window.document.head.removeChild(arguments[0]);",
                        scriptElement);
            }
        }
    }

    public boolean isEditable(String locator) {
        return getElement(locator).isEnabled();
    }

    public void attachFile(String locator, String filepath) {
        WebElement element = getElement(locator);
        if (!"input".equals(element.getTagName())
                || !"file".equals(element.getAttribute("type"))) {
            abort("Element identified by [" + locator
                    + "] is not an <input type='file'> tag.");
        }
        element.sendKeys(filepath);
    }

    public void highlight(String locator) {
        WebElement element = getElement(locator);
        focus(element);
    }

    public Number getCursorPosition(String locator) {
        WebElement element = getElement(locator);
        if (!"input".equals(element.getTagName())
                || !"textarea".equals(element.getTagName())) {
            abort("Element identified by [" + locator
                    + "] does not have a cursor.");
        }
        focus(element);

        LOG.warn("getCursorPosition not implemented."); //$NON-NLS-1$

        return 0;
    }

    public void focus(String locator) {
        focus(getElement(locator));
    }

    private void focus(WebElement element) {
        if ("input".equals(element.getTagName())) {
            element.sendKeys("");
        } else {
            new Actions(m_webdriver).moveToElement(element).perform();
        }
    }

    public boolean isAlertPresent() {
        try {
            m_webdriver.findElement(By.xpath("//input[@value = 'alert']"))
                    .click();
            m_webdriver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public String getAlert() {
        m_webdriver.findElement(By.xpath("//input[@value = 'alert']")).click();
        return m_webdriver.switchTo().alert().getText();
    }

    public boolean isConfirmationPresent() {
        try {
            m_webdriver.findElement(By.xpath("//input[@value = 'confirm']"))
                    .click();
            m_webdriver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public String getConfirmation() {
        m_webdriver.findElement(By.xpath("//input[@value = 'confirm']"))
                .click();
        return m_webdriver.switchTo().alert().getText();
    }

    public boolean isPromptPresent() {
        try {
            m_webdriver.findElement(By.xpath("//input[@value = 'prompt']"))
                    .click();
            m_webdriver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public String getPrompt() {
        m_webdriver.findElement(By.xpath("//input[@value = 'prompt']")).click();
        return m_webdriver.switchTo().alert().getText();
    }

    public void chooseOkOnNextConfirmation() {
        m_webdriver.switchTo().alert().accept();
    }

    public void chooseCancelOnNextConfirmation() {
        m_webdriver.switchTo().alert().dismiss();
    }

    public void answerOnNextPrompt(String text) {
        m_webdriver.switchTo().alert().sendKeys(text);
    }

    public Rectangle getWindowBounds() {
        org.openqa.selenium.Point position = m_webdriver.manage().window()
                .getPosition();
        Dimension size = m_webdriver.manage().window().getSize();
        return new Rectangle(position.x, position.y, size.width, size.height);
    }

    public void checkDriverStatus() {
        if (m_webdriver instanceof FirefoxDriver
                || m_webdriver instanceof ChromeDriver
                || m_webdriver instanceof InternetExplorerDriver
                || m_webdriver instanceof SafariDriver) {
            if (((RemoteWebDriver) m_webdriver).getSessionId() == null) {
                throw new SessionNotFoundException(
                        "Browser session has gone away.");
            }
            try {
                m_webdriver.manage().window().getSize();
            } catch (Exception e) {
                throw new SessionNotFoundException(
                        "Browser window has gone away.");
            }
        } else {
            String title = getWindowTitle();
            LOG.warn("Heartbeat : " + title);
        }
    }

    public void waitForElement(String xPath, int timeoutSeconds) {
        new WebDriverWait(m_webdriver, timeoutSeconds).until(ExpectedConditions
                .elementToBeClickable(By.xpath(xPath)));
    }

    public boolean isElementExists(String xPath) {
        List<WebElement> foundElements = m_webdriver.findElements(By.xpath(xPath));
        return !foundElements.isEmpty();
    }
}