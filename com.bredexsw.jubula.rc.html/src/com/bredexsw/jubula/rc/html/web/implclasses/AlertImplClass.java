/*******************************************************************************
 * Copyright (c) 2015 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;

import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;

/** @author BREDEX GmbH */
public class AlertImplClass extends PseudoComponentImplClass {
    /** Constructor */
    public AlertImplClass() {
    }

    /**
     * Writes the passed text into the currently component.
     * 
     * @param text The text.
     * @throws StepExecutionException
     */
    public void rcInputText(final String text) throws StepExecutionException {
        getWebRobot().answerOnNextPrompt(text);
    }

    /**
     * Verifies the rendered text inside the current component.<br>
     * If it is a complex component, it is always the selected object.
     * 
     * @param text The text to verify.
     * @param operator The operation used to verify
     * @throws StepExecutionException
     */
    public void rcCheckText(String text, String operator)
        throws StepExecutionException {
        String currentText = null;

        RobotWebImpl robot = getWebRobot();
        boolean alertPresent = robot.isAlertPresent();
        if (alertPresent) {
            currentText = robot.getAlert();
        }

        boolean confirmationPresent = robot.isConfirmationPresent();
        if (confirmationPresent) {
            currentText = robot.getConfirmation();
        }

        boolean promptPresent = robot.isPromptPresent();
        if (promptPresent) {
            currentText = robot.getPrompt();
        }
        if (!(alertPresent || promptPresent || confirmationPresent)) {
            throw new StepExecutionException("No component found.", //$NON-NLS-1$
                    EventFactory.createComponentNotFoundErrorEvent());
        }

        Verifier.match(currentText, text, operator);
    }

    /**
     * Verifies that the component exists and is visible.
     *
     * @param exists <code>True</code> if the component is expected to exist and
     *            be visible, otherwise <code>false</code>.
     */
    public void rcVerifyExists(boolean exists) {
        boolean alertPresent = getWebRobot().isAlertPresent();
        boolean confirmationPresent = getWebRobot().isConfirmationPresent();
        boolean promptPresent = getWebRobot().isPromptPresent();

        Verifier.equals(exists, alertPresent || confirmationPresent
                || promptPresent);
    }

    /**
     * @param type the interaction type
     * @throws StepExecutionException
     */
    public void rcInteract(final String type) throws StepExecutionException {
        if ("ACCEPT".equals(type)) { //$NON-NLS-1$
            getWebRobot().chooseOkOnNextConfirmation();
        } else if ("DISMISS".equals(type)) { //$NON-NLS-1$
            getWebRobot().chooseCancelOnNextConfirmation();
        } else {
            throw new StepExecutionException("Invalid interaction type.", //$NON-NLS-1$
                    EventFactory.createActionError());
        }
    }

    /**
     * Consumes the opened dialog and therefore closes it. This is using the
     * same method as <code>rcCheckText</code> since the method which gets the
     * text from the dialog also consumes it.
     *
     * This action will never throw a Exception so it could be used after each
     * action (including <code>rcCheckText</code>)
     */
    public void rcCloseDialog() {
        getTextAndConsumeDialog();
    }

    /**
     * Gets the text from a dialog and also consumes it. The method will throw a
     * {@link StepExecutionException} if there is no dialog present
     * @return the text of a dialog if one is present
     */
    private String getTextAndConsumeDialog() {
        String currentText = null;
        RobotWebImpl robot = getWebRobot();
        boolean alertPresent = robot.isAlertPresent();
        if (alertPresent) {
            currentText = robot.getAlert();
        }

        boolean confirmationPresent = robot.isConfirmationPresent();
        if (confirmationPresent) {
            currentText = robot.getConfirmation();
        }

        boolean promptPresent = robot.isPromptPresent();
        if (promptPresent) {
            currentText = robot.getPrompt();
        }
        if (!(alertPresent || promptPresent || confirmationPresent)) {
            throw new StepExecutionException("No component found.", //$NON-NLS-1$
                    EventFactory.createComponentNotFoundErrorEvent());
        }
        return currentText;
    }
}
