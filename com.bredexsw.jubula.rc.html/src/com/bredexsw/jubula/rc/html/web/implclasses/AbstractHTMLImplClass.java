/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.rc.common.Constants;
import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.openqa.selenium.WebDriverException;

/**
 * The abstract base class for all web implementation classes
 * 
 * @author BREDEX GmbH
 * @created 15.09.2009
 *
 */
public abstract class AbstractHTMLImplClass extends AbstractWebImplClass {
    /**
     * {@inheritDoc}
     */
    public void rcClick(int count, int button) {
        if (isEnabled(getComponent().getLocator())) {
            getRobot().click(getComponent(), null, 
                    ClickOptions.create()
                        .setClickCount(count)
                        .setMouseButton(button));
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public void rcClickDirect(int count, int button,
        int xPos, String xUnits, int yPos, String yUnits)
        throws StepExecutionException {
        if (isEnabled(getComponent().getLocator())) {
            getRobot().click(getComponent(), null, 
                    ClickOptions.create()
                        .setClickCount(count)
                        .setMouseButton(button),
                    xPos, xUnits.equalsIgnoreCase(POS_UNIT_PIXEL),
                    yPos, yUnits.equalsIgnoreCase(POS_UNIT_PIXEL));
        }
    }

    /**
     * Verifies the <code>enabled</code> property.
     *
     * @param enabled The <code>enabled</code> property value to verify
     */
    public void rcVerifyEnabled(boolean enabled) {
        Verifier.equals(enabled, isEnabled(getComponent().getLocator()));
    }

    /**
     * Verifies if the component has the focus.
     *
     * @param hasFocus The hasFocus property to verify.
     */
    public void rcVerifyFocus(boolean hasFocus) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * @param locator The locator
     * @return true if element is enabled, false otherwise
     */
    public boolean isEnabled(String locator) {
        boolean isDisabled = true;
        try {
            String attr = getRobot().getAttribute(locator, "disabled"); //$NON-NLS-1$
            String ariaAttr = getRobot().getAttribute(locator, "aria-disabled"); //$NON-NLS-1$
            if ((StringUtils.isEmpty(attr) && StringUtils.isEmpty(ariaAttr)) || (!"true".equalsIgnoreCase(attr) && !"true".equalsIgnoreCase(ariaAttr))) {
                isDisabled = false;
            }
        } catch (WebDriverException se) {
            isDisabled = false;
        }
        return !isDisabled;
    }

    /**
     * @param locator The locator
     * @return true if element is isReadonly, false otherwise
     */
    public boolean isReadonly(String locator) {
        boolean isReadonly = true;
        try {
            String attr = getRobot().getAttribute(locator, "readonly"); //$NON-NLS-1$
            if (attr == null || !"true".equalsIgnoreCase(attr)) {
                isReadonly = false;
            }
        } catch (WebDriverException se) {
            isReadonly = false;
        }
        return isReadonly;
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and selects an item at the given position in the popup menu
     *
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param indexPath path of item indices
     * @param button MouseButton
     * @throws StepExecutionException error
     */
    public void rcPopupSelectByIndexPath(int xPos, String xUnits, int yPos,
        String yUnits, String indexPath, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Select an item in the popup menu
     * 
     * @param indexPath path of item indices
     * @param button MouseButton
     * @throws StepExecutionException error
     */
    public void rcPopupSelectByIndexPath(String indexPath, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Selects an item in the popup menu
     *
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param textPath path of item texts
     * @param operator operator used for matching
     * @param button MouseButton
     * @throws StepExecutionException error
     */
    public void rcPopupSelectByTextPath(final int xPos, final String xUnits,
        final int yPos, final String yUnits, String textPath, String operator,
        int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Selects an item in the popup menu
     * 
     * @param textPath path of item texts
     * @param operator operator used for matching
     * @param button MouseButton
     * @throws StepExecutionException error
     */
    public void rcPopupSelectByTextPath(String textPath, String operator,
        int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is enabled.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param indexPath the menu item to verify
     * @param enabled for checking enabled or disabled
     * @param button MouseButton
     */
    public void rcPopupVerifyEnabledByIndexPath(int xPos, String xUnits,
        int yPos, String yUnits, String indexPath, boolean enabled, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified context menu entry is enabled.
     * 
     * @param indexPath the menu item to verify
     * @param enabled for checking enabled or disabled
     * @param button MouseButton
     */
    public void rcPopupVerifyEnabledByIndexPath(String indexPath,
        boolean enabled, int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is enabled.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param textPath the menu item to verify
     * @param operator operator used for matching
     * @param enabled for checking enabled or disabled
     * @param button MouseButton
     */
    public void rcPopupVerifyEnabledByTextPath(final int xPos,
        final String xUnits, final int yPos, final String yUnits,
        String textPath, String operator, boolean enabled, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified context menu entry is enabled.
     * 
     * @param textPath the menu item to verify
     * @param operator operator used for matching
     * @param enabled for checking enabled or disabled
     * @param button MouseButton
     */
    public void rcPopupVerifyEnabledByTextPath(String textPath,
        String operator, boolean enabled, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry exists.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param indexPath the menu item to verify
     * @param exists for checking if entry exists
     * @param button MouseButton
     */
    public void rcPopupVerifyExistsByIndexPath(int xPos, String xUnits,
        int yPos, String yUnits, String indexPath, boolean exists, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified context menu entry exists.
     * 
     * @param indexPath the menu item to verify
     * @param exists for checking if entry exists
     * @param button MouseButton
     */
    public void rcPopupVerifyExistsByIndexPath(String indexPath,
        boolean exists, int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry exists.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param textPath the menu item to verify
     * @param operator operator used for matching
     * @param exists for checking if entry exists
     * @param button MouseButton
     */
    public void rcPopupVerifyExistsByTextPath(final int xPos,
        final String xUnits, final int yPos, final String yUnits,
        String textPath, String operator, boolean exists, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified context menu entry exists.
     * 
     * @param textPath the menu item to verify
     * @param operator operator used for matching
     * @param exists for checking if entry exists
     * @param button MouseButton
     */
    public void rcPopupVerifyExistsByTextPath(String textPath, String operator,
        boolean exists, int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is selected.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param indexPath the menu item to verify
     * @param selected for checking if entry is selected
     * @param button MouseButton
     */
    public void rcPopupVerifySelectedByIndexPath(int xPos, String xUnits,
        int yPos, String yUnits, String indexPath, boolean selected, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified context menu entry is selected.
     * 
     * @param indexPath the menu item to verify
     * @param selected for checking if entry is selected
     * @param button MouseButton
     */
    public void rcPopupVerifySelectedByIndexPath(String indexPath,
        boolean selected, int button) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is selected.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param textPath the menu item to verify
     * @param operator operator used for matching
     * @param selected for checking if entry is selected
     * @param button MouseButton
     */
    public void rcPopupVerifySelectedByTextPath(final int xPos,
        final String xUnits, final int yPos, final String yUnits,
        String textPath, String operator, boolean selected, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified context menu entry is selected.
     * 
     * @param textPath the menu item to verify
     * @param operator operator used for matching
     * @param selected for checking if entry is selected
     * @param button MouseButton
     */
    public void rcPopupVerifySelectedByTextPath(String textPath,
        String operator, boolean selected, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Performs a Drag. Moves into the middle of the Component and presses and
     * holds the given modifier and the given mouse button.
     * 
     * @param mouseButton the mouse button.
     * @param modifier the modifier, e.g. shift, ctrl, etc.
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     */
    public void rcDrag(int mouseButton, String modifier, int xPos,
        String xUnits, int yPos, String yUnits) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Performs a Drop. Moves into the middle of the Component and releases the
     * modifier and mouse button pressed by gdDrag.
     * 
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *            between moving the mouse to the drop point and releasing the
     *            mouse button
     */
    public void rcDrop(int xPos, String xUnits, int yPos, String yUnits,
        int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Simulates a tooltip for demonstration purposes.
     *
     * @param text The text to show in the tooltip
     * @param textSize The size of the text in points
     * @param timePerWord The amount of time, in milliseconds, used to display a
     *            single word. A word is defined as a string surrounded by
     *            whitespace.
     * @param windowWidth The width of the tooltip window in pixels.
     */
    public void rcShowText(final String text, final int textSize,
        final int timePerWord, final int windowWidth) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * @param childElementType the child element type e.g. "//li"
     * @return the amount of child element or -1 if error occured during
     *         accessing children
     */
    protected int countChildElements(String childElementType) {
        int count = -1;
        try {
            String locator = getComponent().getLocator();

            String xpath = locator.replaceAll(Constants.XPATH, StringConstants.EMPTY);

            count = getRobot().getXpathCount(xpath + childElementType).intValue();
        } catch (WebDriverException e) {
            // ignore
        }
        return count;
    }
}