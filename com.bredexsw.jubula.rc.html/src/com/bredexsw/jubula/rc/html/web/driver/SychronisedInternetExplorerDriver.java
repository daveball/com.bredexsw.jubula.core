package com.bredexsw.jubula.rc.html.web.driver;

import java.util.Map;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.Response;

/**
 * A synchronised implementation of the InternetExplorerDriver. Workaround for
 * Selenium issue: <a
 * href="http://code.google.com/p/selenium/issues/detail?id=6592">IE Driver
 * returns 404: File not found</a>.
 * <p/>
 * <b>N.B.</b> synchronises the execute(...) methods.
 *
 * @author nickgrealy@gmail.com
 * @version 1.0
 * @since 27/08/2014
 */
public class SychronisedInternetExplorerDriver extends InternetExplorerDriver {

    private Object lock;

    public SychronisedInternetExplorerDriver() {
        super();
    }

    public SychronisedInternetExplorerDriver(Capabilities capabilities) {
        super(capabilities);
    }

    public SychronisedInternetExplorerDriver(int port) {
        super(port);
    }

    public SychronisedInternetExplorerDriver(
            InternetExplorerDriverService service) {
        super(service);
    }

    public SychronisedInternetExplorerDriver(
            InternetExplorerDriverService service, Capabilities capabilities) {
        super(service, capabilities);
    }

    public SychronisedInternetExplorerDriver(
            InternetExplorerDriverService service, Capabilities capabilities,
            int port) {
        super(service, capabilities, port);
    }

    @Override
    protected Response execute(String driverCommand, Map<String, ?> parameters) {
        synchronized (getLock()) {
            return super.execute(driverCommand, parameters);
        }
    }

    @Override
    protected Response execute(String command) {
        synchronized (getLock()) {
            return super.execute(command);
        }
    }

    public Object getLock() {
        if (lock == null) {
            // class level variable is not initialised if called from within
            // constructor.
            lock = new Object();
        }
        return lock;
    }
}
