/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms.select;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;
import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;

/**
 * @author BREDEX GmbH
 * @created Dec 1, 2009
 */
public class DefaultSelectFormHelper extends AbstractSelectFormHelper{

    /**
     * The web robot
     */
    private final RobotWebImpl m_robot;

    /**
     * The constructor.
     *
     * @param robot The web robot
     */
    public DefaultSelectFormHelper(RobotWebImpl robot) {
        m_robot = robot;
    }

    /**
     * selects item by index
     * 
     * @param component AbstractWebComponent
     * @param index index of item
     */
    @Override
    public void selectIndex(AbstractWebComponent component, String index) {
        List<WebElement> children = fetchChildren(component);
        int selOptSize = children.size();
        int idx = parseIndex(index);
        if (idx >= selOptSize || (idx < 0)) {
            throw new StepExecutionException("List index '" + index //$NON-NLS-1$
                    + "' is out of range", //$NON-NLS-1$
                    EventFactory
                            .createActionError(TestErrorEvent.INVALID_INDEX));
        }

        if (m_robot.isElementPresent(component.getLocator())
                && m_robot.isVisible(component.getLocator())) {
            m_robot.select(component, idx);
        }
    }

    /**
     * extend selection by index
     * 
     * @param comp AbstractWebComponent
     * @param index index of item
     */
    @Override
    public void addSelectionByIndex(AbstractWebComponent component, String index) {
        List<WebElement> children = fetchChildren(component);
        int selOptSize = children.size();
        int idx = parseIndex(index);
        if (idx >= selOptSize || (idx < 0)) {
            throw new StepExecutionException("List index '" + index //$NON-NLS-1$
                    + "' is out of range", //$NON-NLS-1$
                    EventFactory
                            .createActionError(TestErrorEvent.INVALID_INDEX));
        }

        if (m_robot.isElementPresent(component.getLocator())
                && m_robot.isVisible(component.getLocator())) {
            // Don't clear existing selection.
            m_robot.select(component, idx);
        }
    }

    @Override
    protected String[] getOptions(AbstractWebComponent comp) {
        return m_robot.getSelectOptions(comp.getLocator());
    }

    /**
     * @param comp AbstractWebComponent
     * @return The array of selected values as the renderer shows them
     */
    @Override
    public String[] getSelectedValues(AbstractWebComponent component) {
        List<String> selectedLabels = new ArrayList<String>();
        List<WebElement> children = fetchChildren(component);
        for (WebElement child : children) {
            if (isSelected(child)) {
                selectedLabels.add(child.getText());
            }
        }
        return selectedLabels.toArray(new String[selectedLabels.size()]);
    }

    /**
     * @param comp AbstractWebComponent
     * @return The array of indexes of selected items.
     */
    @Override
    public Integer[] getSelectedIndexes(AbstractWebComponent component) {
        List<Integer> selectedIndexes = new ArrayList<Integer>();
        List<WebElement> children = fetchChildren(component);
        for (int i = 0; i < children.size(); i++) {
            if (isSelected(children.get(i))) {
                selectedIndexes.add(i);
            }
        }
        return selectedIndexes.toArray(new Integer[selectedIndexes.size()]);
    }

    /**
     * @param comp AbstractWebComponent
     * @return Children of this component.
     */
    private List<WebElement> fetchChildren(AbstractWebComponent comp) {
        WebElement element = m_robot.getElement(comp.getLocator());
        return element.findElements(By.xpath(".//*"));
    }

    /**
     * @param child An option within a Select element.
     * @return true if this item is selected.
     */
    private boolean isSelected(WebElement child) {
        String attributeValue = child.getAttribute("selected");
        return "true".equalsIgnoreCase(attributeValue);
    }
}
