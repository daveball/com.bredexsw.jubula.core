/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms;

import org.eclipse.jubula.rc.common.util.Verifier;
import org.openqa.selenium.WebDriverException;

import com.bredexsw.jubula.rc.html.web.implclasses.forms.label.LabelImplClass;

/**
 * @author BREDEX GmbH
 * @created Nov 30, 2009
 */
public class AnchorImplClass extends LabelImplClass {

    /**
     * Verify Url
     * 
     * @param text url to verify
     * @param matchOp operator
     */
    public void rcVerifyURL(String text, String matchOp) {
        String actual = getUrl();

        Verifier.match(actual, text, matchOp);
    }

    /**
     * get url of anchor/link
     * 
     * @return url of link
     */
    private String getUrl() {
        String url;
        try {
            url = getRobot().getAttribute(getComponent().getLocator(), "href"); //$NON-NLS-1$
            if (url != null && url.contains("#")) {
                url = url.substring(url.indexOf('#'));
            }
        } catch (WebDriverException se) {
            url = null;
        }
        return url;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getTextArrayFromComponent() {
        return new String[] { getUrl() };
    }
}
