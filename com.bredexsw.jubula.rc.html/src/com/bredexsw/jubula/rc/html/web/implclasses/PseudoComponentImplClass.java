/*******************************************************************************
 * Copyright (c) 2015 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses;

import org.eclipse.jubula.rc.common.tester.interfaces.ITester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;

/** @author BREDEX GmbH */
public class PseudoComponentImplClass implements ITester {
    /** the logger */
    protected static final Logger LOG = LoggerFactory
                .getLogger(PseudoComponentImplClass.class);
    
    /** Constructor */
    public PseudoComponentImplClass() {
        super();
    }

    /** {@inheritDoc} */
    public void setComponent(Object graphicsComponent) {
        // empty
    }

    /** {@inheritDoc} */
    public String[] getTextArrayFromComponent() {
        // empty
        return null;
    }

    /**
     * @return the web robot
     */
    protected RobotWebImpl getWebRobot() {
        return WebAUTServer.getInstance().getWebRobot();
    }
}