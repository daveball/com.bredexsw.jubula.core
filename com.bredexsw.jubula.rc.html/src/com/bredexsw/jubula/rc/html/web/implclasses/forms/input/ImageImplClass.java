/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms.input;

import org.eclipse.jubula.rc.common.util.Verifier;
import org.openqa.selenium.WebDriverException;

/**
 * @author BREDEX GmbH
 * @created Nov 10, 2009
 */
public class ImageImplClass extends InputButtonImplClass {

    /**
     * Verifies the passed text.
     *
     * @param text The text to verify
     * @param operator The RegEx operator used to verify
     */
    @Override
    public void rcVerifyText(String text, String operator) {
        Verifier.match(getAltText(), text, operator);
    }

    /**
     * Action to read the value of a Button to store it in a variable in the
     * Client
     * 
     * @param variable the name of the variable
     * @return the text value.
     */
    @Override
    public String rcReadValue(String variable) {
        return getAltText();
    }

    /**
     * get alternative text of an image.
     * 
     * @return the alternative text of image
     */
    protected String getAltText() {
        String text;
        try {
            text = getRobot().getAttribute(getComponent().getLocator(), "alt"); //$NON-NLS-1$
        } catch (WebDriverException se) {
            text = null;
        }
        return text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getTextArrayFromComponent() {
        return new String[] { getAltText() };
    }
}
