package com.bredexsw.jubula.rc.html.web.implclasses.forms.select;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.toolkit.enums.ValueSets.SearchType;
import org.eclipse.jubula.tools.internal.constants.TestDataConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.StringParsing;

import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;

public abstract class AbstractSelectFormHelper {

    /** The default separator for enumerations of list values. */
    public static final char INDEX_LIST_SEP_CHAR = TestDataConstants.VALUE_CHAR_DEFAULT;
    /** The dafault separator of a list of values */
    public static final char VALUE_SEPARATOR = TestDataConstants.VALUE_CHAR_DEFAULT;

    /**
     * Parses the index and returns an integer.
     *
     * @param index The index to parse
     * @return The integer value
     * @throws StepExecutionException If the index cannot be parsed
     */
    public int parseIndex(String index) throws StepExecutionException {
        try {
            return Integer.parseInt(index);
        } catch (NumberFormatException e) {
            throw new StepExecutionException("Index '" + index //$NON-NLS-1$
                    + "' is not an integer", EventFactory.createActionError(//$NON-NLS-1$
                    TestErrorEvent.INVALID_INDEX));
        }
    }

    /**
     * Parses the enumeration of indices.
     * 
     * @param indexList The enumeration of indices
     * @return The array of parsed indices
     */
    public int[] parseIndices(String indexList) {
        String[] list = StringParsing.splitToArray(indexList,
                INDEX_LIST_SEP_CHAR, TestDataConstants.ESCAPE_CHAR_DEFAULT);
        int[] indices = new int[list.length];
        for (int i = 0; i < list.length; i++) {
            String index = list[i];
            try {
                indices[i] = Integer.parseInt(index);
            } catch (NumberFormatException e) {
                throw new StepExecutionException(
                        "Index '" + index + "' is not an integer", //$NON-NLS-1$ //$NON-NLS-2$
                        EventFactory
                                .createActionError(TestErrorEvent.INVALID_INDEX));
            }
        }
        return indices;
    }

    /**
     * Parses string array with indices.
     * 
     * @param indexArray The string array with indices
     * @return The array of parsed indices
     */
    public int[] parseIndices(String[] indexArray) {
        int[] indices = new int[indexArray.length];
        for (int i = 0; i < indexArray.length; i++) {
            String index = indexArray[i];
            try {
                indices[i] = Integer.parseInt(index);
            } catch (NumberFormatException e) {
                throw new StepExecutionException(
                        "Index '" + index + "' is not an integer", //$NON-NLS-1$ //$NON-NLS-2$
                        EventFactory
                                .createActionError(TestErrorEvent.INVALID_INDEX));
            }
        }
        return indices;
    }

    /**
     * Finds the indices of the list elements
     *
     * @param comp AbstractWebComponent
     * @param values The values
     * @param operator operator to use
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @return The array of indices. It's length is equal to the length of the
     *         values array, but may contains <code>null</code> elements for all
     *         values that are not found in the list
     */
    public Integer[] findIndicesOfValues(AbstractWebComponent comp,
        final String[] values, final String operator, final String searchType) {

        final Set<Integer> indexSet = new HashSet<Integer>();
        String[] items = getOptions(comp);
        int startIndex = getStartingIndex(comp, searchType);
        for (int i = startIndex; i < items.length; ++i) {
            if (MatchUtil.getInstance().match(items[i], values, operator)) {
                indexSet.add(new Integer(i));
            }
        }

        Integer[] indices = new Integer[indexSet.size()];
        indexSet.toArray(indices);
        return indices;
    }

    /**
     * Finds the indices of the list elements
     *
     * @param comp AbstractWebComponent
     * @param values The values
     * @param operator operator to use
     * @return The array of indices. It's length is equal to the length of the
     *         values array, but may contains <code>null</code> elements for all
     *         values that are not found in the list
     */
    public Integer[] findIndicesOfValues(AbstractWebComponent comp,
        final String[] values, final String operator) {

        return findIndicesOfValues(comp, values, operator,
                SearchType.absolute.rcValue());
    }

    /**
     * @param comp AbstractWebComponent
     * @param value The value
     * @return <code>true</code> if the list contains an element that is
     *         rendered with <code>value</code>
     */
    public boolean containsValue(AbstractWebComponent comp, String value) {
        Integer[] indices = findIndicesOfValues(comp, new String[] { value },
                MatchUtil.EQUALS);
        return indices.length > 0;
    }

    /**
     * @param comp AbstractWebComponent
     * @param value The value
     * @param operator The operator used to verify
     * @return <code>true</code> if the list contains an element that is
     *         rendered with <code>value</code>
     */
    public boolean containsValue(AbstractWebComponent comp, String value,
        String operator) {
        Integer[] indices = null;
        if (operator.equals(MatchUtil.NOT_EQUALS)) {
            indices = findIndicesOfValues(comp, new String[] { value },
                    MatchUtil.EQUALS);
            return indices.length == 0;
        }
        indices = findIndicesOfValues(comp, new String[] { value }, operator);
        return indices.length > 0;
    }

    /**
     * Splits the enumeration of values.
     *
     * @param values The values to split
     * @param separator The separator, may be <code>null</code>
     * @return The array of values
     */
    public String[] split(String values, String separator) {
        String[] list = StringParsing
                .splitToArray(
                        values,
                        ((separator == null) || (separator.length() == 0) ? INDEX_LIST_SEP_CHAR
                                : separator.charAt(0)),
                        TestDataConstants.ESCAPE_CHAR_DEFAULT);
        return list;
    }

    /**
     * @param comp AbstractWebComponent
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @return The index from which to begin a search, based on the search type
     *         and (if appropriate) the currently selected cell.
     */
    public int getStartingIndex(AbstractWebComponent comp,
        final String searchType) {
        int startingIndex = 0;
        if (searchType.equalsIgnoreCase(SearchType.relative.rcValue())) {
            String[] selectedValues = getSelectedValues(comp);
            Integer[] selectedIndices = findIndicesOfValues(comp,
                    selectedValues, MatchUtil.EQUALS);
            // Start from the last selected item
            startingIndex = selectedIndices[selectedIndices.length - 1] + 1;
        }
        return startingIndex;
    }
    

    /**
     * @param component AbstractWebComponent
     * @return the index of the selected item.
     */
    public Integer getSelectedIndex(AbstractWebComponent component) {
        Integer[] selected = getSelectedIndexes(component);
        if (selected == null || selected.length == 0)
            return null;

        return selected[0];
    }

    /**
     * @param component AbstractWebComponent
     * @return the selected value as the renderer shows it.
     */
    public String getSelectedLabel(AbstractWebComponent component) {
        String[] selected = getSelectedValues(component);
        if (selected == null || selected.length == 0)
            return null;

        return selected[0];
    }


    protected abstract String[] getSelectedValues(AbstractWebComponent comp);

    protected abstract Integer[] getSelectedIndexes(
        AbstractWebComponent component);

    protected abstract String[] getOptions(AbstractWebComponent comp);

    protected abstract void selectIndex(AbstractWebComponent component,
        String index);

    protected abstract void addSelectionByIndex(AbstractWebComponent component,
        String index);

}
