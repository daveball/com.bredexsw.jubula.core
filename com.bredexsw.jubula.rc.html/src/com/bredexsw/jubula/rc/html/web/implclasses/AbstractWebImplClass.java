/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses;


import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.interfaces.ITester;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.TestDataConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;
import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;

/**
 * The abstract base class for all web implementation classes
 * 
 * @author BREDEX GmbH
 * @created 15.09.2009
 *
 */
public abstract class AbstractWebImplClass implements ITester {
    /** The default separator for enumerations of list values. */
    public static final char INDEX_LIST_SEP_CHAR =
        TestDataConstants.VALUE_CHAR_DEFAULT;
    /** The dafault separator of a list of values */
    public static final char VALUE_SEPARATOR =
        TestDataConstants.VALUE_CHAR_DEFAULT;
    
    /** constants for communication */
    protected static final String POS_UNIT_PIXEL = "Pixel"; //$NON-NLS-1$
    /** constants for communication */
    protected static final String POS_UNI_PERCENT = "Percent"; //$NON-NLS-1$
    
    /** the component associated with this implementation class instance */
    private AbstractWebComponent m_component;
    
    /** {@inheritDoc} */
    public void setComponent(Object graphicsComponent) {
        Validate.isTrue(graphicsComponent instanceof AbstractWebComponent);
        m_component = (AbstractWebComponent)graphicsComponent;
    }

    /**
     * 
     * @return the component associated with this implementation class instance.
     */
    protected AbstractWebComponent getComponent() {
        return m_component;
    }
    
    /**
     * @param component
     *            the component identifier to get the locator string for
     * @return the locator string
     */
    private String getLocator(AbstractWebComponent component) {
        return component.getLocator();
    }

    /**
     * {@inheritDoc}
     */
    public void highLight(AbstractWebComponent component) {
        for (int i = 0; i < 3; i++) {
            getRobot().highlight(getLocator(component));
        }
    }

    /**
     * @return the robot
     */
    protected RobotWebImpl getRobot() {
        return WebAUTServer.getInstance().getWebRobot();
    }

    /**
     * dummy method for "wait for component"
     * @param timeout the maximum amount of time to wait for the component
     * @param delay the time to wait after the component is found
     */
    public void rcWaitForComponent (int timeout, int delay) {
        // do NOT delete this method!
        // do nothing, implementation is in class CAPTestCommand.getImplClass
        // because this action needs a special implementation!
    }
    
    /**
     * Verifies that the component exists and is visible.
     *
     * @param exists
     *            <code>True</code> if the component is expected to exist
     *            and be visible, otherwise <code>false</code>.
     */
    public void rcVerifyExists(boolean exists) {
        /*
         * The actual testing of the component's existence/non-existence is
         * implemented in CAPTestCommand.getImplClass. This method only checks
         * for visibility.
         */
        Verifier.equals(exists, getRobot().isVisible(
                getComponent().getLocator()));
    }

    /**
     * Verifies an attribute value
     *
     * @param name The name of the property
     * @param value The value of the property as a string
     * @param operator The operator used to verify
     */
    public void rcVerifyProperty(String name, String value, String operator) {
        try {
            final String prop = getRobot()
                    .getPropertyValue(getComponent(), name);
            final boolean matches = MatchUtil.getInstance().match(prop,
                    value, operator);
            if (!matches) {
                Verifier.throwVerifyFailed(value, prop);
            }
        } catch (RobotException e) {
            throw new StepExecutionException(e.getMessage(), EventFactory
                    .createActionError(TestErrorEvent.PROPERTY_NOT_ACCESSABLE));
        }
    }
    
    /**
     * Stores the value of the property with the name <code>name</code>.
     * The name of the property has be specified according to the JavaBean
     * specification. The value returned by the property is converted into a
     * string by calling <code>toString()</code> and is stored to the passed
     * variable.
     * 
     * @param variableName The name of the variable to store the property value in
     * @param propertyName The name of the property
     * @return the property value.
     */
    public String rcStorePropertyValue(String variableName, 
        String propertyName) {
        try {
            return getRobot().getPropertyValue(getComponent(), propertyName);
        } catch (RobotException e) {
            throw new StepExecutionException(e.getMessage(), EventFactory
                    .createActionError(TestErrorEvent.PROPERTY_NOT_ACCESSABLE));
        }
    }
}