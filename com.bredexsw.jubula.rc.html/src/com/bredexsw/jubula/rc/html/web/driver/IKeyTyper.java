/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver;

import org.eclipse.jubula.rc.common.exception.RobotException;

/**
 * @author BREDEX GmbH
 * @created Dec 7, 2009
 */
public interface IKeyTyper {
    
    /**
     * Types the string <code>text</code> at the specified cursor location.
     * @param graphicsComponent  The graphics component the string is typed in. Must not be <code>null</code>
     * @param text The string to type
     * @param index cursor position
     * @throws RobotException If the typing fails.
     */
    public void type(Object graphicsComponent, String text, int index)
        throws RobotException;
    
}
