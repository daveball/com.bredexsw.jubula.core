/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver;

import org.eclipse.jubula.rc.common.exception.RobotException;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;

/**
 * @author BREDEX GmbH
 * @created Dec 7, 2009
 */
public class KeyTyperReplaceImpl extends AbstractKeyTyper {
    /**
     * {@inheritDoc}
     */
    @Override
    public void type(Object graphicsComponent, String text, int index)
        throws RobotException {
        RobotWebImpl robot = WebAUTServer.getInstance().getWebRobot();
        AbstractWebComponent comp = (AbstractWebComponent) graphicsComponent;
        String oldText = robot.getValue(comp.getLocator());
        String new1 = oldText.substring(0, index);
        String new2 = oldText.substring(index);
        String newText = new1 + text + new2;
        robot.setText(comp, newText);
    }
}
