/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jubula.rc.common.driver.IEventThreadQueuer;
import org.eclipse.jubula.rc.common.driver.IMouseMotionTracker;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.driver.IRobotEventInterceptor;
import org.eclipse.jubula.rc.common.driver.IRobotFactory;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.logger.AutServerLogger;
import org.eclipse.jubula.toolkit.html.Browser;
import org.openqa.selenium.Dimension;


/**
 * Factory class for the web robot 
 * 
 * @author BREDEX GmbH
 * @created 13.08.2009
 */
public class RobotFactoryWebImpl implements IRobotFactory {

    /** the logger */
    private static final AutServerLogger LOG = new AutServerLogger(
            RobotFactoryWebImpl.class);
    
    /**
     * The Web Robot.
     */
    private IRobot m_robot = null;

    /**
     * The type of browser.
     */
    private Browser m_browser;
    
    /**
     * The path to the browser executable, or null if the default executable should be used.
     */
    private String m_browserExecutablePath;

    /** Size to use for the browser window, or null for fullscreen */
    private Dimension m_browserSize;

    /**
     * The initial URL when the browser is started.
     */
    private String m_initialURL;

    /**
     * The name of the attribute to regards as the technical id of the component
     */
    private String m_idAttributeName;
    
    /**
     * Constructor
     */
    public RobotFactoryWebImpl() {
        // empty
    }
    
    /**
     * {@inheritDoc}
     */
    public IEventThreadQueuer getEventThreadQueuer() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public IMouseMotionTracker getMouseMotionTracker() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public IRobot getRobot() throws RobotException {
        if (m_robot == null) {
            try {
                m_robot = new RobotWebImpl(m_browser,
                        m_browserExecutablePath, m_initialURL, m_idAttributeName,
                        m_browserSize);
            } catch (InvocationTargetException ite) {
                LOG.error("Error starting RobotWebImpl", ite);
                throw new RobotException(ite);
            }
        }
        return m_robot;
    }
    
    /**
     * {@inheritDoc}
     */
    public IRobotEventInterceptor getRobotEventInterceptor() {
        return null;
    }

    /**
     * 
     * @param initialURL The URL to set.
     */
    public void setInitialURL(String initialURL) {
        m_initialURL = initialURL;
    }
    
    /**
     * @return the initial URL when the browser is started.
     */
    public String getInitialURL() {
        return m_initialURL;
    }

    /**
     * @param browser The Type of browser Webdriver should use.
     */
    public void setBrowser(Browser browser) {
        m_browser = browser;
    }

    /**
     * @param browserExecutablePath The path to the browser executable, or null if the default executable should be used.
     */
    public void setBrowserExecutablePath(String browserExecutablePath) {
        this.m_browserExecutablePath = browserExecutablePath;
    }

    /**
     * @param idAttributeName the idAttributeName to set
     */
    public void setIdAttributeName(String idAttributeName) {
        m_idAttributeName = idAttributeName;
    }

    /**
     * @return the idAttributeName
     */
    public String getIdAttributeName() {
        return m_idAttributeName;
    }
    
    /**
     * @param browserSize the browserSize to set
     */
    public void setBrowserSize(Dimension browserSize) {
        this.m_browserSize = browserSize;
    }
}
