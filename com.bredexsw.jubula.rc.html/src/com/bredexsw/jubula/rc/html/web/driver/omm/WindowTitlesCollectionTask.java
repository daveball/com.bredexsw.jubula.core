/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver.omm;

import java.util.Arrays;
import java.util.TimerTask;

import org.eclipse.jubula.communication.internal.message.html.WindowTitlesMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.tools.internal.exception.CommunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.WebAUTServer;

/**
 * 
 * @author BREDEX GmbH
 *
 */
public class WindowTitlesCollectionTask extends TimerTask {
    /** <code>log</code>: the logger  */
    private static Logger log = LoggerFactory
            .getLogger(ComponentIdentifierCollectionTask.class);
    
    /** window titles of the last run */
    private String[] m_lastWindowTitles;
    /**
    * {@inheritDoc}
    */
    public void run() {
        String[] actualTitles = WebAUTServer.getInstance().getWebRobot()
                .getAllWindowTitles();
        if (m_lastWindowTitles == null) {
            m_lastWindowTitles = actualTitles;
            sendMessag();
        } else if (!Arrays.equals(m_lastWindowTitles, actualTitles)) {
            m_lastWindowTitles = actualTitles;
            sendMessag();
        }

    }
    /**
     * Send the message of the changed windows to the client
     */
    private void sendMessag() {
        try {
            // send a message with the window titles to the client
            WindowTitlesMessage message = 
                    new WindowTitlesMessage();
            message.setWindowTitles(WebAUTServer.getInstance()
                    .getWebRobot().getAllWindowTitles());
            AUTServer.getInstance().getCommunicator().send(message);
        } catch (CommunicationException ce) {
            log.error(ce.getLocalizedMessage(), ce);
            // do nothing here: a closed connection is handled by the
            // AUTServer
        }
    }

}
