/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms.select;

import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.ListSelectionVerifier;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.toolkit.enums.ValueSets.InteractionMode;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.openqa.selenium.WebDriverException;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * @author BREDEX GmbH
 * @created Dec 1, 2009
 */
public class ListImplClass extends AbstractHTMLImplClass {

    /**
     * The SelectForm helper
     */
    private AbstractSelectFormHelper m_selectFormHelper;

    /**
     * Gets the SelectFormHelper. The helper is created once per instance.
     *
     * @return The SelectForm helper
     */
    public AbstractSelectFormHelper getSelectFormHelper() {
        if (m_selectFormHelper == null) {
            m_selectFormHelper = new DefaultSelectFormHelper(getRobot());
        }
        return m_selectFormHelper;
    }

    /**
     * Selects the passed index or enumeration of indices. The enumeration must
     * be separated by <code>,</code>, e.g. <code>1, 3,6</code>.
     * 
     * @param indexList The index or indices to select
     * @param extendSelection Whether this selection extends a previous
     *            selection.
     * @param button what mouse button should be used
     * @param clickCount the click count
     */
    public void rcSelectIndex(String indexList, final String extendSelection,
        int button, int clickCount) {
        if (button != InteractionMode.primary.rcIntValue() || clickCount != 1) {
            throw new StepExecutionException(TestErrorEvent.INVALID_INPUT,
                    EventFactory
                            .createActionError(TestErrorEvent.INVALID_INPUT));
        }
        final boolean isExtendSelection = extendSelection
                .equals(ValueSets.BinaryChoice.yes.rcValue());
        selectIndices(
                IndexConverter.toImplementationIndices(getSelectFormHelper()
                        .parseIndices(indexList)), isExtendSelection);
    }

    /**
     * Selects the passed value or enumeration of values. By default, the
     * enumeration separator is <code>,</code>.
     * 
     * @param valueList The value or list of values to select
     * @param operator If regular expressions are used
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param isExtendSelection Whether this selection extends a previous
     *            selection. If <code>true</code>, the first element will be
     *            selected with CONTROL as a modifier.
     * @param button what mouse button should be used
     * @param clickCount the click count
     */

    public void rcSelectValue(String valueList, String operator,
        final String searchType, final String isExtendSelection, int button,
        int clickCount) {
        if (button != InteractionMode.primary.rcIntValue() || clickCount != 1) {
            throw new StepExecutionException(TestErrorEvent.INVALID_INPUT,
                    EventFactory
                            .createActionError(TestErrorEvent.INVALID_INPUT));
        }
        rcSelectValue(valueList, String.valueOf(VALUE_SEPARATOR), operator,
                searchType, 1, isExtendSelection);
    }

    /**
     * Selects the passed value or enumeration of values. By default, the
     * enumeration separator is <code>,</code>, but may be changed by
     * <code>separator</code>.
     * 
     * @param valueList The value or list of values to select
     * @param separator The separator, optional
     * @param operator If regular expressions are used
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param clickCount The number of times to click each given entry.
     * @param extendSelection Whether this selection extends a previous
     *            selection.
     */
    public void rcSelectValue(String valueList, String separator,
        String operator, final String searchType, int clickCount,
        final String extendSelection) {

        String[] values = null;
        final boolean isExtendSelection = extendSelection
                .equals(ValueSets.BinaryChoice.yes.rcValue());
        if (StringConstants.EMPTY.equals(valueList)) {
            values = new String[1];
            values[0] = StringConstants.EMPTY;
        } else {
            values = getSelectFormHelper().split(valueList, separator);
        }
        Integer[] indices = getSelectFormHelper().findIndicesOfValues(
                getComponent(), values, operator, searchType);
        Arrays.sort(indices);
        if (!operator.equals(MatchUtil.NOT_EQUALS)
                && (indices.length < values.length)) {
            throw new StepExecutionException(
                    "One or more values not found of set: " //$NON-NLS-1$
                            + Arrays.asList(values).toString(),
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        selectIndices(ArrayUtils.toPrimitive(indices), isExtendSelection);
    }

    /**
     * @param indices The indices to select
     * @param isExtendSelection Whether this selection extends a previous
     *            selection. If <code>true</code>, the first element will be
     *            selected with CONTROL as a modifier.
     */
    private void selectIndices(int[] indices, boolean isExtendSelection) {
        if (isEnabled(getComponent().getLocator())) {
            if (indices.length > 0) {
                if (isExtendSelection) {
                    // extend selection
                    getSelectFormHelper().addSelectionByIndex(getComponent(),
                            new Integer(indices[0]).toString());
                } else {
                    // first selection
                    getSelectFormHelper().selectIndex(getComponent(),
                            new Integer(indices[0]).toString());
                }
            }
            // folowing selections
            for (int i = 1; i < indices.length; i++) {
                getSelectFormHelper().addSelectionByIndex(getComponent(),
                        new Integer(indices[i]).toString());
            }
        }

    }

    /**
     * Verifies if the passed index is selected.
     * 
     * @param index The index to verify
     * @param expectSelected Whether the index should be selected.
     */
    public void rcVerifySelectedIndex(String index, boolean expectSelected) {
        try {
            Integer[] selected = getSelectFormHelper().getSelectedIndexes(
                    getComponent());
            int implIndex = IndexConverter.toImplementationIndex(Integer
                    .parseInt(index));

            boolean isSelected = ArrayUtils.contains(selected, implIndex);
            if (expectSelected != isSelected) {
                throw new StepExecutionException(
                        "Selection check failed for index: " + index, //$NON-NLS-1$
                        EventFactory.createVerifyFailed(
                                String.valueOf(expectSelected),
                                String.valueOf(isSelected)));
            }
        } catch (WebDriverException se) {
            throw new StepExecutionException("No list element selected", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NO_SELECTION));
        }

    }

    /**
     * Verifies if the passed value is selected.
     * 
     * @param value The value to verify
     * @param operator The operator to use when comparing the expected and
     *            actual values.
     * @param isSelected if the value should be selected or not.
     */
    public void rcVerifySelectedValue(String value, String operator,
        boolean isSelected) {

        final String[] current = getSelectFormHelper().getSelectedValues(
                getComponent());
        final ListSelectionVerifier listSelVerifier = new ListSelectionVerifier();
        for (int i = 0; i < current.length; i++) {
            listSelVerifier.addItem(i, current[i], true);
        }
        listSelVerifier.verifySelection(value, operator, isSelected);
    }

    /**
     * Verifies if the list contains an element that renderes <code>value</code>
     * .
     * 
     * @param value The text to verify
     * @param operator The operator used to verify
     * @param exists If the value should exist or not.
     */
    public void rcVerifyContainsValue(String value, String operator,
        boolean exists) {
        final boolean contains = getSelectFormHelper().containsValue(
                getComponent(), value, operator);
        Verifier.equals(exists, contains);
    }

    /**
     * Action to read the value of the current selected item of the JList to
     * store it in a variable in the Client
     * 
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        String[] selected = getSelectFormHelper().getSelectedValues(
                getComponent());
        if (selected.length > 0) {
            return selected[0];
        }
        throw new StepExecutionException("No list item selected", //$NON-NLS-1$
                EventFactory.createActionError(TestErrorEvent.NO_SELECTION));
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void rcDragIndex(int mouseButton, String modifierSpecification,
        int indexOnly) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDragValue(int mouseButton, String modifierSpecification,
        String text, String operator, String searchType) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDropIndex(int indexOnly, int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDropValue(String text, String operator, String searchType,
        int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcVerifyText(String text, String operator) {
        String txt = rcReadValue("dummy"); //$NON-NLS-1$
        Verifier.match(txt, text, operator);
    }

}
