/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.model;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.rc.common.Constants;
import org.eclipse.jubula.rc.common.components.AUTComponent;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;

/**
 * Base class for our representation of HTML DOM nodes.
 * 
 * @author BREDEX GmbH
 * @created Nov 17, 2009
 */
public class AbstractWebComponent extends AUTComponent {
    /** the component locator string */
    private String m_componentLocator;

    /** the frame index path */
    private int[] m_frameIdxPath = new int[0];
    
    /** the tagName */
    private String m_tagName;
    
    /** the tag's ID attribute */
    private String m_compId;

    /**
     * Constructor
     */
    public AbstractWebComponent() {
        super(new Object());
    }

    /**
     * @return the Selenium locator to use in order to find the DOM node
     *         represented by this component. This is always an XPATH locator
     */
    public String getLocator() {
        RobotWebImpl robot = WebAUTServer.getInstance().getWebRobot();
        for (int i = 0; i < m_frameIdxPath.length; i++) {
            int zeroBasedIndex = m_frameIdxPath[i] - 1;
           	robot.selectFrame("index=" + zeroBasedIndex);
        }
        return Constants.XPATH + m_componentLocator;
    }

    /**
     * @param locator
     *            the locator to set
     */
    private void setLocator(String locator) {
        m_componentLocator = locator;
    }

    /**
     * {@inheritDoc}
     */
    public void setComp(Object comp) {
        String framePrefix = "/uiframe[";
        String fqPath = comp.toString();
        int lastFrameIdx = StringUtils.lastIndexOf(fqPath, framePrefix);
        String xPath = fqPath;
        if (lastFrameIdx != -1) {
            int frameStringIdx = framePrefix.length() + lastFrameIdx + 2;
            xPath = fqPath.substring(frameStringIdx);
            String fPath = fqPath.substring(0, frameStringIdx);
            fPath = StringUtils.remove(fPath, framePrefix);
            String[] fIdxPath = StringUtils.split(fPath, ']');
            int[] idxPath = new int[fIdxPath.length];
            for (int i = 0; i < idxPath.length; i++) {
                idxPath[i] = Integer.valueOf(fIdxPath[i]).intValue();
            }
            m_frameIdxPath = idxPath;
        }
        setLocator(xPath);
        super.setComponent(comp);
    }

    /**
     * @param tagName
     *            the tagName to set
     */
    public void setTagName(String tagName) {
        m_tagName = tagName;
    }

    /**
     * @return the tagName
     */
    public String getTagName() {
        return m_tagName;
    }

    /**
     * @return The Component's ID attribute
     */
    public String getCompId() {
        return m_compId;
    }

    /**
     * @param compId The Component's ID attribute
     */
    public void setCompId(String compId) {
        this.m_compId = compId;
    }    
}
