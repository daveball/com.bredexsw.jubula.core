/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.KeyStroke;

import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.exception.OsNotSupportedException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.implclasses.AbstractApplicationImplClass;
import org.eclipse.jubula.rc.common.logger.AutServerLogger;
import org.eclipse.jubula.rc.common.util.KeyStrokeUtil;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;
import org.openqa.selenium.WebDriverException;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.utils.BrowserEnvironmentUtil;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;

/**
 * @author BREDEX GmbH
 * @created Nov 26, 2009
 */
public class HtmlApplicationImplClass extends AbstractApplicationImplClass {

    /**
     * The logging.
     */
    private static AutServerLogger log = new AutServerLogger(
            HtmlApplicationImplClass.class);

    /**
     * The default format to use when writing images to disk.
     */
    private static final String DEFAULT_IMAGE_FORMAT = "png"; //$NON-NLS-1$

    /**
     * The string used to separate filename and file extension.
     */
    private static final String EXTENSION_SEPARATOR = "."; //$NON-NLS-1$

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected IRobot getRobot() {
        return getWebRobot();
    }

    /**
     * get web robot
     * @return web robot
     */
    protected RobotWebImpl getWebRobot() {
        return WebAUTServer.getInstance().getWebRobot();
    }

    /**
     * Perform a native keystroke ignore "F5" for html and "ESCAPE" when browser
     * is firefox
     * 
     * @param modifierSpec the string representation of the modifiers
     * @param keySpec the string representation of the key
     */
    @Override
    public void rcNativeKeyStroke(String modifierSpec, String keySpec) {
        if (keySpec == null || keySpec.trim().length() == 0) {
            throw new StepExecutionException(
                    "The base key of the key stroke must not be null or empty", //$NON-NLS-1$
                    EventFactory.createActionError());
        }
        String key = keySpec.trim().toUpperCase();
        KeyStroke keyStroke = KeyStroke.getKeyStroke(key);
        if (keyStroke.getKeyCode() == KeyEvent.VK_F5
                || (keyStroke.getKeyCode() == KeyEvent.VK_ESCAPE && BrowserEnvironmentUtil
                        .isFirefox(getWebRobot()))) {
            return;
        }
        super.rcNativeKeyStroke(modifierSpec, keySpec);
    }

    /**
     * perform a keystroke
     * 
     * @param modifierSpec the string representation of the modifiers
     * @param keySpec the string representation of the key
     */
    public void rcKeyStroke(String modifierSpec, String keySpec) {

        if (keySpec == null || keySpec.trim().length() == 0) {
            throw new StepExecutionException(
                    "The base key of the key stroke must not be null or empty", //$NON-NLS-1$
                    EventFactory.createActionError());
        }

        String keyStrokeSpec = keySpec.trim().toUpperCase();
        String mod = KeyStrokeUtil.getModifierString(modifierSpec);
        if (mod.length() > 0) {
            keyStrokeSpec = mod.toString() + " " + keyStrokeSpec; //$NON-NLS-1$
        }

        getRobot().keyStroke(keyStrokeSpec);
    }

    /**
     * @param keyCodeName The name of a key code, e.g. <code>TAB</code> for a
     *            tabulator key code
     * @return The key code or <code>-1</code>, if the key code name doesn't
     *         exist in the <code>KeyEvent</code> class
     * @throws StepExecutionException If the key code name cannot be converted
     *             to a key code due to the reflection call
     */
    public int getKeyCode(String keyCodeName) throws StepExecutionException {
        int code = -1;
        String codeName = "VK_" + keyCodeName; //$NON-NLS-1$
        try {
            code = KeyEvent.class.getField(codeName).getInt(KeyEvent.class);
        } catch (IllegalArgumentException e) {
            throw new StepExecutionException(e.getMessage(),
                    EventFactory.createActionError());
        } catch (SecurityException e) {
            throw new StepExecutionException(e.getMessage(),
                    EventFactory.createActionError());
        } catch (IllegalAccessException e) {
            throw new StepExecutionException(e.getMessage(),
                    EventFactory.createActionError());
        } catch (NoSuchFieldException e) {
            if (log.isInfoEnabled()) {
                log.info("The key expression '" + keyCodeName //$NON-NLS-1$
                        + "' is not a key code, typed as key stroke instead"); //$NON-NLS-1$
            }
        }
        return code;
    }

    /**
     * toggle key
     * 
     * @param key to set: <br/>
     *            numlock Num Lock 1 <br/>
     *            caplock Caps Lock 2 <br/>
     *            scolllock Scroll 3
     * @param activated boolean
     */
    public void rcToggle(int key, boolean activated)
        throws OsNotSupportedException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * @param text text to type
     */
    public void rcInputText(String text) {
        if (text != null) {
            try {
                for (char c : text.toCharArray()) {
                    if (Character.isLetterOrDigit(c)) {
                        getWebRobot().keyType(null, c);
                    } else {
                        throw new StepExecutionException(
                                TestErrorEvent.INVALID_INPUT,
                                EventFactory
                                        .createActionError(TestErrorEvent.INVALID_INPUT));
                    }
                }
            } catch (WebDriverException se) {
                log.error(se.getLocalizedMessage());
                throw new StepExecutionException(TestErrorEvent.INPUT_FAILED,
                        EventFactory
                                .createActionError(TestErrorEvent.INPUT_FAILED));
            }
        }
    }

    /**
     * activate the AUT
     * 
     * @param method activation method
     */
    public void rcActivate(String method) {
        getRobot().activateApplication(method);
    }

    /**
     * clicks into the active window.
     *
     * @param count amount of clicks
     * @param button what mouse button should be used
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @throws StepExecutionException error
     */
    public void rcClickDirect(int count, int button, int xPos, String xUnits,
        int yPos, String yUnits) throws StepExecutionException {

        try {
            getWebRobot().selectFrame(null);
        } catch (WebDriverException se) {
            log.error(se.getLocalizedMessage());
            throw new StepExecutionException("No active window.", //$NON-NLS-1$
                    EventFactory
                            .createActionError(TestErrorEvent.NO_ACTIVE_WINDOW));
        }

    }

    /**
     * Checks for the existence of a window with the given title
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param exists <code>True</code> if the component is expected to exist and
     *            be visible, otherwise <code>false</code>.
     */
    public void rcCheckExistenceOfWindow(final String title, String operator,
        boolean exists) {
        Verifier.equals(exists, isWindowOpen(title, operator));
    }

    /**
     * Waits <code>timeMillSec</code> if the application opens a window with the
     * given title.
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param timeout the time in ms
     * @param delay delay after the window is shown
     */
    public void rcWaitForWindow(final String title, final String operator,
        int timeout, int delay) {

        int remainingTime = timeout;

        while (remainingTime > 0 && !isWindowOpen(title, operator)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                // do nothing
            }
            remainingTime -= 100;
        }

        if (!isWindowOpen(title, operator)) {
            throw new StepExecutionException("window did not open", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }

        TimeUtil.delay(delay);
    }

    /**
     * Waits <code>timeMillSec</code> if the application activates a window with
     * the given title.
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param timeout the time in ms
     * @param delay delay after the window is activated
     */
    public void rcWaitForWindowActivation(final String title,
        final String operator, final int timeout, int delay) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Waits <code>timeMillSec</code> if the application closes (or hides) a
     * window with the given title. If no window with the given title can be
     * found, then it is assumed that the window has already closed.
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param timeout the time in ms
     * @param delay delay after the window is activated
     */
    public void rcWaitForWindowToClose(final String title,
        final String operator, int timeout, int delay) {

        int remainingTime = timeout;

        while (remainingTime > 0 && isWindowOpen(title, operator)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                // do nothing
            }
            remainingTime -= 100;
        }

        if (isWindowOpen(title, operator)) {
            throw new StepExecutionException("window still open", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }

        TimeUtil.delay(delay);
    }

    /**
     * Returns <code>true</code> if a window with the given title is open and
     * visible
     * 
     * @param title the title
     * @param operator the matches/equals operator
     * @return if the window is open and visible
     */
    private boolean isWindowOpen(final String title, final String operator) {
        if (MatchUtil.getInstance().match(getWebRobot().getWindowTitle(),
                title, operator)) {
            return true;
        }
        String[] windows = getWebRobot().getAllWindowTitles();
        for (String window : windows) {
            if (MatchUtil.getInstance().match(window, title, operator)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Navigate the currently selected window to the supplied URL
     * 
     * @param url a valid URL
     */
    public void rcGotoUrl(String url) {
        try {
            getWebRobot().open(url);
        } catch (WebDriverException se) {
            log.error(se.getLocalizedMessage());
            throw new StepExecutionException("url not found", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }

    }

    /**
     * Performs a back navigation on the current browser window
     */
    public void rcBack() {
        try {
            getWebRobot().goBack();
        } catch (WebDriverException se) {
            log.error(se.getLocalizedMessage());
            throw new StepExecutionException("no active browser window", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
    }

    /**
     * reload page
     */
    public void rcReload() {
        try {
            getWebRobot().refresh();
        } catch (WebDriverException se) {
            log.error(se.getLocalizedMessage());
            throw new StepExecutionException("no active browser window", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Rectangle getActiveWindowBounds() {
        return getWebRobot().getWindowBounds();
    }

    /**
     * Selects the the window with the given windowName. This is for multi
     * window usage because only the selected window gets the Selenium commands.
     * 
     * @param window window title,name or id
     * @param searchtype type how the window should be selected. By name, title
     *            or any javascript variable. If searchtype is <code>any</code>
     *            the main Window can also be selected with <code>null</code>
     */
    public void rcSelectWindowByIdentifier(String window, String searchtype) {
        String selection;
        if (searchtype.equalsIgnoreCase("name")) { //$NON-NLS-1$
            selection = "name=".concat(window); //$NON-NLS-1$
        } else if (searchtype.equalsIgnoreCase("title")) { //$NON-NLS-1$
            selection = "title=".concat(window); //$NON-NLS-1$
        } else {
            selection = window;
        }
        try {
            getWebRobot().selectWindow(selection);
        } catch (WebDriverException se) {
            log.error(se.getLocalizedMessage());
            throw new StepExecutionException(
                    TestErrorEvent.WINDOW_ACTIVATION_FAILED,
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
    }

    /**
     * This is a title specific selection which could be used with an operator
     * 
     * @param title the title of the window
     * @param operator the matches/equals operator
     */
    public void rcSelectWindow(String title, String operator) {
        RobotWebImpl robot = getWebRobot();
        String[] windows = robot.getAllWindowTitles();
        boolean selected = false;
        for (String window : windows) {
            if (MatchUtil.getInstance().match(window, title, operator)) {
                try {
                    robot.selectWindow(window);
                    selected = true;
                } catch (WebDriverException se) {
                    log.error(se.getLocalizedMessage());
                    throw new StepExecutionException(
                            TestErrorEvent.WINDOW_ACTIVATION_FAILED,
                            EventFactory
                                    .createActionError(TestErrorEvent.NOT_FOUND));
                }
                break;
            }
        }
        if (!selected) {
            // this is the case if no window with the title was found
            throw new StepExecutionException(
                    TestErrorEvent.WINDOW_ACTIVATION_FAILED,
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rcTakeScreenshotOfActiveWindow(String destination, int delay,
        String fileAccess, int scaling, boolean createDirs, int marginTop,
        int marginRight, int marginBottom, int marginLeft) {

        String imageExtension = getExtension(destination);
        if (imageExtension.length() == 0) {
            // If not, then we simply append the default extension
            imageExtension = DEFAULT_IMAGE_FORMAT;
            destination += EXTENSION_SEPARATOR + imageExtension;
        }

        // Wait for a user-specified time
        if (delay > 0) {
            TimeUtil.delay(delay);
        }

        double scaleFactor = scaling * 0.01;
        RobotWebImpl robot = getWebRobot();
        try {
            BufferedImage image = robot.createWindowCapture();
            BufferedImage imageOut = scaleImage(image, scaleFactor);
            File out = fetchOutputFile(destination, fileAccess, imageExtension);

            // Save captured image using given format, if supported.
            if (imageExtension.length() == 0
                    || !ImageIO.getImageWritersBySuffix(imageExtension).hasNext()
                    || !ImageIO.write(imageOut, imageExtension, out)) {

                // Otherwise, save using default format
                out = new File(destination + EXTENSION_SEPARATOR
                        + DEFAULT_IMAGE_FORMAT);
                if (!ImageIO.write(imageOut, DEFAULT_IMAGE_FORMAT, out)) {

                    // This should never happen, so log as error if it does.
                    // In this situation, the screenshot will not be saved, but
                    // the test step will still be marked as successful.
                    log.error("Screenshot could not be saved. " + //$NON-NLS-1$
                            "Default image format (" + DEFAULT_IMAGE_FORMAT //$NON-NLS-1$
                            + ") is not supported."); //$NON-NLS-1$
                }
            }
        } catch (IOException e) {
            throw new StepExecutionException(e.getMessage(),
                    new TestErrorEvent(TestErrorEvent.FILE_IO_ERROR));
        }
    }

    /**
     * @param source The image to Scale
     * @param scaleFactor How much to Scale the image
     * @return A scaled copy of the Image.
     */
    private BufferedImage scaleImage(BufferedImage source, double scaleFactor) {
        // copied from AbstractApplicationImplClass.takeScreenshot()
        int scaledWidth = (int) Math.floor(source.getWidth() * scaleFactor);
        int scaledHeight = (int) Math.floor(source.getHeight() * scaleFactor);
        BufferedImage imageOut = new BufferedImage(scaledWidth, scaledHeight,
                BufferedImage.TYPE_INT_RGB);
        // Scale it to the new size on-the-fly
        Graphics2D graphics2D = imageOut.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.drawImage(source, 0, 0, scaledWidth, scaledHeight, null);
        return imageOut;
    }

    /**
     * @param destination Base path & filename for this file. We might use a
     *            different actual filename if the current file exists.
     * @param fileAccess Should an existing file be overwritten, or should the
     *            destination filename be incremented
     * @param imageExtension e.g. PNG
     * @return A valid File reference for saving the image.
     */
    private File fetchOutputFile(String destination, String fileAccess,
        String imageExtension) {
        File out = new File(destination);
        // Create path, if necessary
        if (fileAccess.equals(RENAME)) {
            String completeExtension = EXTENSION_SEPARATOR
                    + imageExtension.toLowerCase();
            int extensionIndex = out.getName().toLowerCase()
                    .lastIndexOf(completeExtension);
            String fileName = out.getName().substring(0, extensionIndex);
            for (int i = 1; out.exists(); i++) {
                out = new File(out.getParent(), fileName
                        + "_" + i + completeExtension); //$NON-NLS-1$
            }
        }
        return out;
    }

    /**
     * Copied from AbstractApplicationImplClass.getExtension
     * 
     * @param filename A filename for which to find the extension.
     * @return the file extension for the given filename. For example, "png" for
     *         "example.png". Returns an empty string if the given name has no
     *         extension. This is the case if the given name does not contain an
     *         instance of the extension separator or ends with the extension
     *         separator. For example, "example" or "example.".
     */
    private String getExtension(String filename) {
        File file = new File(filename);
        int extensionIndex = file.getName().lastIndexOf(EXTENSION_SEPARATOR);
        return extensionIndex == -1
                || extensionIndex == file.getName().length() - 1 ? StringConstants.EMPTY
                : file.getName().substring(extensionIndex + 1);
    }

}
