/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms;

import org.openqa.selenium.WebDriverException;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * @author BREDEX GmbH
 * @created Nov 30, 2009
 */
public class ImageImplClass extends AbstractHTMLImplClass {

    /**
     * get alternative text of an image.
     * @return the alternative text of image
     */
    protected String getAltText() {
        String text;
        try {
            text = getRobot().getAttribute(getComponent().getLocator(), "alt"); //$NON-NLS-1$
        } catch (WebDriverException se) {
            text = null;
        }
        return text;
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return new String[] { getAltText() };
    }
}
