/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms;

import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;


/**
 * @author BREDEX GmbH
 * @created Nov 10, 2009
 */
public class ButtonImplClass extends AbstractHTMLImplClass {
    /**
     * Verifies the selected property.
     *
     * @param selected The selected property value to verify.
     */
    public void rcVerifySelected(boolean selected) {
        Verifier.equals(selected, false);
    }
    /**
     * Verifies the passed text.
     *
     * @param text The text to verify
     * @param operator The RegEx operator used to verify
     */
    public void rcVerifyText(String text, String operator) {
        Verifier.match(getRobot().getText(getComponent().getLocator()),
                text, operator);
    }
    /**
     * Verifies the passed text.
     *
     * @param text The text to verify
     */
    public void rcVerifyText(String text) {
        rcVerifyText(text, MatchUtil.DEFAULT_OPERATOR);
    }
    
    /**
     * Action to read the value of a Button to store it in a variable
     * in the Client
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        return getRobot().getText(getComponent().getLocator());
    }
    
    
    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return new String[] {getRobot().getText(getComponent().getLocator())};
    }

}
