/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver;

import org.apache.commons.lang.NotImplementedException;
import org.eclipse.jubula.rc.common.exception.RobotException;

/**
 * @author BREDEX GmbH
 * @created Dec 7, 2009
 */
public class KeyTyperDefaultImpl extends AbstractKeyTyper {
    /**
     * {@inheritDoc}
     */
    @Override
    public void type(Object graphicsComponent, String text, int index)
        throws RobotException {

        // TODO unused?

        throw new NotImplementedException("KeyTyperDefault not implemented.");
    }
}
