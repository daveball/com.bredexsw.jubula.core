/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.model;

import java.util.List;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.components.AUTComponent;
import org.eclipse.jubula.rc.common.components.HierarchyContainer;


/**
 * specific Hierarchy for HTML
 * 
 * @author BREDEX GmbH
 * @created 23.11.2009
 *
 */
public class HtmlHierarchyContainer extends HierarchyContainer {
    /**
     * <code>XPATH_SEPARATOR</code>
     */
    public static final String XPATH_SEPARATOR = "/"; //$NON-NLS-1$
    
    /**
     * <code>INDEX_END</code>
     */
    private static final String INDEX_END = "]"; //$NON-NLS-1$
    
    /**
     * <code>INDEX_BEGIN</code>
     */
    private static final String INDEX_BEGIN = "["; //$NON-NLS-1$
    
    /** wrapped child tag name name */
    private String m_embeddedTagName = null;
    
    /** <code>m_fqpath</code> */
    private String m_fqpath = null;

    /**
     * @param component
     *            the AUT component
     */
    public HtmlHierarchyContainer(AUTComponent component) {
        super(component);
        AbstractWebComponent wc = (AbstractWebComponent)component; 
        setEmbeddedTagName(wc.getTagName());
        if (component.getName() != null) {
            String compName = wc.getName();
            if (!compName.equals(wc.getTagName())) {
                setName(compName, false);
            } else {
                setName(compName, true);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void add(HierarchyContainer component) {
        Validate.notNull(component);
        super.add(component);
        if (component.getPrnt() != this) {
            component.setPrnt(this);
        }
    }

    /**
     * @param fqPath
     *            the fqPath to set
     */
    private void setFQPath(String fqPath) {
        this.m_fqpath = fqPath;
    }

    /**
     * @param topLevelComponents  the top level components
     * @return the fully qualified path containing the xpath
     */
    public String getFQPath(List<HtmlHierarchyContainer> topLevelComponents) {
        if (m_fqpath == null) {
            computeFQPath(topLevelComponents);
        }
        return m_fqpath;
    }

    /**
     * @param topLevelComponents  the top level components
     * compute the fully qualify path 
     */
    private void computeFQPath(
            List<HtmlHierarchyContainer> topLevelComponents) {
        StringBuilder sb = new StringBuilder();
        HtmlHierarchyContainer ccParent = (HtmlHierarchyContainer)this
                .getPrnt();
        // must start counting by 1 as this is a W3C XPath specification
        int childPos = 1;
        
        List<HtmlHierarchyContainer> siblings;
        if (ccParent != null) {
            siblings = ccParent.getContainerList();
            sb.append(ccParent.getFQPath(topLevelComponents));
        } else {
            siblings = topLevelComponents;
        }
        for (HtmlHierarchyContainer hc : siblings) {
            if (hc.equals(this)) {
                break;
            }
            if (hc.getEmbeddedTagName()
                    .equals(this.getEmbeddedTagName())) {
                childPos++;
            }
        }
        
        sb.append(XPATH_SEPARATOR);
        sb.append(this.getEmbeddedTagName());
        sb.append(INDEX_BEGIN).append(childPos).append(INDEX_END);
        // ensure container name uniqueness
        if (isNameGenerated()) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.getName());
            sb2.append(INDEX_BEGIN).append(childPos).append(INDEX_END);
            this.setName(sb2.toString(), true);
        }
        setFQPath(sb.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    public AUTComponent getCompID() {
        return super.getCompID();
    }
    
    /**
     * @return HtmlHierarchyContainer[]
     */
    public HtmlHierarchyContainer[] getComponents() {
        if (super.getComps().length == 0) {
            return new HtmlHierarchyContainer[0];
        }
        HierarchyContainer[] containerArray = super.getComps();
        HtmlHierarchyContainer[] htmlContainerArray = 
            new HtmlHierarchyContainer[containerArray.length];
        for (int i = 0; i < containerArray.length; i++) {
            htmlContainerArray[i] = 
                (HtmlHierarchyContainer)containerArray[i]; 
        }
        return htmlContainerArray;
    }

    /**
     * @param embeddedCompName the embeddedCompName to set
     */
    private void setEmbeddedTagName(String embeddedCompName) {
        m_embeddedTagName = embeddedCompName;
    }

    /**
     * @return the embeddedCompName
     */
    public String getEmbeddedTagName() {
        return m_embeddedTagName;
    }
}
