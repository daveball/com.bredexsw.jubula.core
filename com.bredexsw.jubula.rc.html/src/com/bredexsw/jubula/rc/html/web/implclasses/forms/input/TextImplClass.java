/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms.input;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * 
 *
 * @author BREDEX GmbH
 * @created Nov 10, 2009
 *
 */
public class TextImplClass extends AbstractHTMLImplClass {

    /**
     * Types <code>text</code> into the component.
     *
     * @param text the text to type in
     */
    public void rcInputText(String text) {
        if (inputSupported()) {
            getRobot().sendText(getComponent(), text);
        }
    }

    /**
     * Types <code>text</code> into the component. This replaces the shown
     * content.
     *
     * @param text the text to type in
     */
    public void rcReplaceText(String text) {
        if (inputSupported()) {
            getRobot().setText(getComponent(), text);
        }
    }

    /**
     * Inserts <code>text</code> at the position <code>index</code>.
     *
     * @param text The text to insert
     * @param index The position for insertion
     */
    public void rcInsertText(String text, int index) {
        if (inputSupported()) {

            int idx = index;
            String locator = getComponent().getLocator();
            String oldText = getRobot().getValue(locator);
            if (oldText != null) {
                idx = idx > oldText.length() ? oldText.length() : idx;
            }
            if (idx < 0) {
                throw new StepExecutionException("Invalid position: " + idx, //$NON-NLS-1$
                        EventFactory
                                .createActionError(TestErrorEvent.INPUT_FAILED));
            }

            getRobot().focus(locator);

            getRobot().type(getComponent(), text, idx);
        }
    }

    /**
     * Inserts <code>text</code> before or after the first appearance of
     * <code>pattern</code>.
     *
     * @param text The text to insert
     * @param pattern The pattern to find the position for insertion
     * @param operator Operator to select Matching Algorithm
     * @param after If <code>true</code>, the text will be inserted after the
     *            pattern, otherwise before the pattern.
     * @throws StepExecutionException If the pattern is invalid or cannot be
     *             found
     */
    public void rcInsertText(String text, String pattern, String operator,
        boolean after) throws StepExecutionException {

        if (text == null) {
            throw new StepExecutionException(
                    "The text to be inserted must not be null", EventFactory //$NON-NLS-1$
                            .createActionError(TestErrorEvent.INVALID_PARAM_VALUE));
        }
        final MatchUtil.FindResult matchedText = MatchUtil.getInstance().find(
                getRobot().getValue(getComponent().getLocator()), pattern,
                operator);
        if ((matchedText == null) || (matchedText.getStr() == null)
                || (matchedText.getStr().length() == 0)) {
            throw new StepExecutionException("The pattern '" + pattern //$NON-NLS-1$
                    + "' could not be found", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        final int index = matchedText.getPos();
        int insertPos = after ? index + matchedText.getStr().length() : index;
        rcInsertText(text, insertPos);
    }

    /**
     * Verifies if the textfield shows the passed text.
     *
     * @param text The text to verify.
     * @param operator The operator used to verify
     */
    public void rcVerifyText(String text, String operator) {
        Verifier.match(getRobot().getValue(getComponent().getLocator()), text,
                operator);
    }

    /**
     * Verifies if the textfield shows the passed text.
     *
     * @param text The text to verify.
     */
    public void rcVerifyText(String text) {
        rcVerifyText(text, MatchUtil.DEFAULT_OPERATOR);
    }

    /**
     * Verifies the editable property.
     *
     * @param editable The editable property to verify.
     */
    public void rcVerifyEditable(boolean editable) {
        Verifier.equals(editable,
                getRobot().isEditable(getComponent().getLocator()));
    }

    /**
     * Action to read the value of a Textfield to store it in a variable in the
     * Client
     * 
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        return getRobot().getValue(getComponent().getLocator());
    }

    /**
     * select the whole text of the textfield by pressing mod1 + A; in case this
     * won't work the whole text is selected programmatically
     */
    public void rcSelect() {
        getRobot().focus(getComponent().getLocator());
        getRobot()
                .keyStroke(ValueSets.Modifier.control.toString() + " typed a");
    }

    /**
     * Selects the first (not)appearance of <code>pattern</code> in the text
     * component's content.
     *
     * @param pattern The pattern to select
     * @param operator operator
     */
    public void rcSelect(final String pattern, String operator) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * @return true, if input is supported
     */
    public boolean inputSupported() {
        String locator = getComponent().getLocator();
        return (getRobot().isElementPresent(locator)
                && getRobot().isVisible(locator) && isEnabled(locator)
                && !(isReadonly(locator)) && getRobot().isEditable(locator));
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

}
