/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver.omm;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.TimerTask;

import net.sf.json.JSONException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.communication.internal.message.ObjectMappedMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.exception.CommunicationException;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.constants.JSConstants;
import com.bredexsw.jubula.rc.html.listener.TechnicalComponentHandler;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;
import org.openqa.selenium.WebDriverException;

/**
 * @author BREDEX GmbH
 * @created 07.09.2009
 */
public class ComponentIdentifierCollectionTask extends TimerTask {
    /**
     * <code>REG_EX_BACKSLASH</code>
     */
    private static final String REG_EX_BACKSLASH = StringConstants.BACKSLASH
            + StringConstants.BACKSLASH;

    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory
            .getLogger(ComponentIdentifierCollectionTask.class);

    /**
     * {@inheritDoc}
     */
    public void run() {
        try {
            String jSONComponents = WebAUTServer.getInstance().getWebRobot().js(
                    JSConstants.getJSOMMGetCollectedTechnicalComponents());
            jSONComponents = checkForDoubleQuotingAndClean(jSONComponents);

            Collection<IComponentIdentifier> identifierList = 
                TechnicalComponentHandler
                    .getInstance().getIdentifier(jSONComponents);
            if (!identifierList.isEmpty()) {
                try {
                    // send a message with the identifier of the selected component
                    ObjectMappedMessage message = new ObjectMappedMessage();
                    message.setComponentIdentifiers(identifierList
                            .toArray(new IComponentIdentifier[identifierList
                                    .size()]));
                    AUTServer.getInstance().getCommunicator().send(message);
                } catch (CommunicationException ce) {
                    log.error(ce.getLocalizedMessage(), ce);
                    // do nothing here: a closed connection is handled by the AUTServer
                }
            }
        } catch (JSONException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (InvocationTargetException e) {
            log.error(e.getLocalizedMessage(), e);
            RobotWebImpl robot = WebAUTServer.getInstance().getWebRobot();
            try {  
                ObjectCollectionMode.getInstance().stop();

            } catch (InvocationTargetException ite) {
                // the old window is already closed
            } finally {
                robot.selectWindow(null);
                try {
                    ObjectCollectionMode.getInstance().start();
                } catch (InvocationTargetException ite) {
                    log.error(e.getLocalizedMessage(), ite);
                    
                } catch (WebDriverException se) {
                    // this happens if the selenium main window does not exist
                    log.error(e.getLocalizedMessage(), se);
                    WebAUTServer.getInstance().shutdown();               
                }
            }
        }
    }

    /**
     * @param jSONComponents
     *            the jSONComponents
     * @return the correct quoted string
     */
    private String checkForDoubleQuotingAndClean(String jSONComponents) {
        String stringToCheck = jSONComponents;
        if (stringToCheck.startsWith(StringConstants.QUOTE)) {
            stringToCheck = StringUtils.removeStart(stringToCheck,
                    StringConstants.QUOTE);
            stringToCheck = StringUtils.removeEnd(stringToCheck,
                    StringConstants.QUOTE);

            stringToCheck = stringToCheck.replaceAll(REG_EX_BACKSLASH,
                    StringConstants.EMPTY);
        }
        return stringToCheck;
    }
}
