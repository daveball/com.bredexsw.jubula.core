/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver.omm;

import java.lang.reflect.InvocationTargetException;
import java.util.Timer;

import net.sf.json.JSONSerializer;

import org.eclipse.jubula.rc.common.AUTServerConfiguration;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.logger.AutServerLogger;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.html.constants.JSConstants;
import com.bredexsw.jubula.rc.html.listener.TechnicalComponentHandler;
import com.bredexsw.jubula.rc.html.web.driver.AbstractRobotUser;

/**
 * @author BREDEX GmbH
 * @created 03.09.2009
 *
 */
public class ObjectCollectionMode extends AbstractRobotUser {
    private static final AutServerLogger LOG = new AutServerLogger(
            ObjectCollectionMode.class);
    /**
     * <code>instance</code> singleton
     */
    private static ObjectCollectionMode instance = null;

    /**
     * <code>javaScript</code>
     */
    private static String javaScript = null;

    /**
     * <code>isRunning</code> flag to indicate whether mode is active
     */
    private static boolean isRunning = false;

    /**
     * <code>m_technicalComponentCollector</code> the collection timer
     */
    private static Timer componentCollectorTimer = null;

    /**
     * <code>OBJECT_MAPPING_COLLECTION_INTERVAL</code>
     */
    private static final int OBJECT_MAPPING_COLLECTION_INTERVAL = 500;

    /**
     * <code>OBJECT_MAPPING_COLLECTION_DELAY</code>
     */
    private static final int OBJECT_MAPPING_COLLECTION_DELAY = 500;

    /**
     * constructor
     */
    private ObjectCollectionMode() {
        // empty
    }

    /**
     * @return the singleton instance
     */
    public static ObjectCollectionMode getInstance() {
        if (instance == null) {
            instance = new ObjectCollectionMode();
        }
        return instance;
    }

    /**
     * call this method before using the instance
     * 
     * @throws InvocationTargetException in case of initialization problems
     */
    public void init() throws InvocationTargetException {
        javaScript = JSConstants.getJSGetOMMScript();
    }

    /** start the web object mapping */
    public void start() throws InvocationTargetException {
        startOMM(JSONSerializer.toJSON(
                AUTServerConfiguration.getInstance().getSupportedTypes())
                .toString());
        TechnicalComponentHandler.getInstance().cleanHierarchy();
        componentCollectorTimer = new Timer();
        componentCollectorTimer.scheduleAtFixedRate(
                new ComponentIdentifierCollectionTask(),
                OBJECT_MAPPING_COLLECTION_DELAY,
                OBJECT_MAPPING_COLLECTION_INTERVAL);

        componentCollectorTimer.scheduleAtFixedRate(
                new WindowTitlesCollectionTask(),
                OBJECT_MAPPING_COLLECTION_DELAY,
                OBJECT_MAPPING_COLLECTION_INTERVAL);
        isRunning = true;
    }

    /**
     * ensure that the object mapping is started
     * 
     * @param jsonSupportedTypes a JSON serialized string of supported type
     *            identifier
     */
    private void startOMM(String jsonSupportedTypes)
        throws InvocationTargetException {
        try {
            // enable debug mode here for JIT debugger, after AUT has been
            // started
            // getRobot().js("window.omm.setDebugMode(true);");
            // if scripts are already available, e.g. OMM has been started /
            // stopped before
            getRobot().js(JSConstants.JS_OMM_START);
        } catch (InvocationTargetException e) {
            // add script for OMM
            getRobot().addScript(javaScript, "Jubula_OMM_Script");
            try {
                // create new OMM object with supported type information
                getRobot().js(JSConstants.getJSCreateOMM(jsonSupportedTypes));
                // getRobot().js("window.omm.setDebugMode(true);");
                // start OMM mode
                getRobot().js(JSConstants.JS_OMM_START);
            } catch (InvocationTargetException ite) {
                LOG.warn(ite); //$NON-NLS-1$
                throw new RobotException(
                        "Failed ininitialising ObjectMapping javascript: "
                                + ite.getMessage(), new TestErrorEvent(
                                TestErrorEvent.EXECUTION_ERROR));
            }
        }
    }

    /**
     * stop the web object mapping
     */
    public void stop() throws InvocationTargetException {
        if (isRunning) {
            try {
                getRobot().js(JSConstants.JS_OMM_STOP);
            } finally {
                componentCollectorTimer.cancel();
                componentCollectorTimer.purge();

                isRunning = false;
            }
        }
    }
}
