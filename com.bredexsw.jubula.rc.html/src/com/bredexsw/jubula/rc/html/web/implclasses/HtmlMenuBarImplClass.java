/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.interfaces.ITester;


/**
 * @author BREDEX GmbH
 * @created May 31, 2010
 */
public class HtmlMenuBarImplClass 
    implements ITester {
    /** {@inheritDoc} */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /** {@inheritDoc} */
    public void setComponent(Object graphicsComponent) {
        // empty stub
    }

    /**
     * implementation for "wait for component"
     * @param timeout the maximum amount of time to wait for the component
     * @param delay the time to wait after the component is found
     */
    public void waitForComponent(int timeout, int delay) {
        StepExecutionException.throwUnsupportedAction();
    }
    
    /**
     * Tries to select a menu item in a menu.
     * @param menuItem the menu item to select
     * @param operator operator used for matching
     */
    public void selectMenuItem(String menuItem, String operator) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Tries to select a menu item in a menu.
     * @param menuItem the menu item to select
     * @param operator operator used for matching
     */
    public void selectMenuItem(String[] menuItem, String operator) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Tries to select a menu item in a menu.
     * @param path path to the menu item
     */
    public void selectMenuItemByIndexpath(String path) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Tries to select a menu item in a menu.
     * @param path path to the menu item
     */

    public void selectMenuItemByIndexpath(int[] path) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies if the specified menu item exists
     * @param menuItem the menu item to verifiy against
     * @param operator operator used for matching
     * @param exists should the menu item exist?
     */
    public void verifyExists(String menuItem, String operator, boolean exists) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies if the specified menu item exists
     * @param menuItem the menu item to verify against
     * @param operator operator used for matching
     * @param exists should the menu item exist?
     */
    public void verifyExists(String[] menuItem,
                             String operator,
                             boolean exists) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies if the specified menu item exists
     * @param menuItem the menu item to verifiy against
     * @param exists should the menu item exist?
     */
    public void verifyExistsByIndexpath(String menuItem, boolean exists) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies if the specified menu item exists
     * @param menuItem the menu item to verify against
     * @param exists should the menu item exist?
     */
    public void verifyExistsByIndexpath(int[] menuItem, boolean exists) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is enabled.
     * @param menuItem the menu item as a text path to verify against
     * @param operator operator used for matching
     * @param enabled is the specified menu item enabled?
     */
    public void verifyEnabled(String menuItem,
                              String operator,
                              boolean enabled) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is enabled.
     * @param menuItem the menu item to verify against
     * @param operator operator used for matching
     * @param enabled is the specified menu item enabled?
     */
    public void verifyEnabled(String[] menuItem,
                              String operator,
                              boolean enabled) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is enabled.
     * @param menuItem the menu item as a text path to verify against
     * @param enabled is the specified menu item enabled?
     */
    public void verifyEnabledByIndexpath(String menuItem, boolean enabled) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is enabled.
     * @param menuItem the menu item to verify against
     * @param enabled is the specified menu item enabled?
     */
    public void verifyEnabledByIndexpath(int[] menuItem, boolean enabled) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is selected.
     * @param menuItem the menu item to verify against
     * @param operator operator used for matching
     * @param selected is the specified menu item selected?
     */
    public void verifySelected(String menuItem,
                               String operator,
                               boolean selected) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is selected.
     * @param menuItem the menu item to verify against
     * @param operator operator used for matching
     * @param selected is the specified menu item selected?
     */
    public void verifySelected(String[] menuItem,
                               String operator,
                               boolean selected) {
        StepExecutionException.throwUnsupportedAction();
    }
    /**
     * Checks if the specified menu item is selected.
     * @param menuItem the menu item to verify against
     * @param selected is the specified menu item selected?
     */
    public void verifySelectedByIndexpath(String menuItem, boolean selected) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Checks if the specified menu item is selected.
     * @param menuItem the menu item to verify against
     * @param selected is the specified menu item selected?
     */
    public void verifySelectedByIndexpath(int[] menuItem, boolean selected) {
        StepExecutionException.throwUnsupportedAction();
    }
}
