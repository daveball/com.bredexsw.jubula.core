/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms;

import java.util.List;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * @author BREDEX GmbH
 * @created Nov 30, 2009
 */
public class TableImplClass extends AbstractHTMLImplClass {

    /**
     * Drags the cell of the Table.<br>
     * With the xPos, yPos, xunits and yUnits the click position inside the cell
     * can be defined.
     *
     * @param mouseButton the mouseButton.
     * @param modifier the modifier.
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @throws StepExecutionException If the row or the column is invalid
     */
    public void rcDragCell(final int mouseButton, final String modifier,
        final String row, final String rowOperator, final String col,
        final String colOperator, final int xPos, final String xUnits,
        final int yPos, final String yUnits) throws StepExecutionException {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Finds the first column which contains the value <code>value</code> in the
     * given row and drags the cell.
     *
     * @param mouseButton the mouse button
     * @param modifier the modifiers
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param value the value
     * @param regex search using regex
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     */
    public void rcDragCellByColValue(int mouseButton, String modifier,
        String row, String rowOperator, final String value, final String regex,
        final String searchType) {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Finds the first row which contains the value <code>value</code> in column
     * <code>col</code> and drags this row.
     *
     * @param mouseButton the mouse button
     * @param modifier the modifier
     * @param col the column to select
     * @param colOperator the column header operator
     * @param value the value
     * @param regexOp the regex operator
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     */
    public void rcDragRowByValue(int mouseButton, String modifier, String col,
        String colOperator, final String value, final String regexOp,
        final String searchType) {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Drops on the cell of the Table.<br>
     * With the xPos, yPos, xunits and yUnits the click position inside the cell
     * can be defined.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *            between moving the mouse to the drop point and releasing the
     *            mouse button
     * @throws StepExecutionException If the row or the column is invalid
     */
    public void rcDropCell(final String row, final String rowOperator,
        final String col, final String colOperator, final int xPos,
        final String xUnits, final int yPos, final String yUnits,
        int delayBeforeDrop) throws StepExecutionException {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Finds the first column which contains the value <code>value</code> in the
     * given row and drops on the cell.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param value the value
     * @param regex search using regex
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *            between moving the mouse to the drop point and releasing the
     *            mouse button
     */
    public void rcDropCellByColValue(String row, String rowOperator,
        final String value, final String regex, final String searchType,
        int delayBeforeDrop) {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Finds the first row which contains the value <code>value</code> in column
     * <code>col</code> and drops on this row.
     *
     * @param col the column to select
     * @param colOperator the column header operator
     * @param value the value
     * @param regexOp the regex operator
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *            between moving the mouse to the drop point and releasing the
     *            mouse button
     */
    public void rcDropRowByValue(String col, String colOperator,
        final String value, final String regexOp, final String searchType,
        int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Writes the passed text into the currently selected cell.
     *
     * @param text The text.
     * @throws StepExecutionException If there is no selected cell, or if the
     *             cell is not editable, or if the table cell editor permits the
     *             text to be written.
     */
    public void rcInputText(final String text) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Types the text in the specified cell.
     *
     * @param text The text
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @throws StepExecutionException If the text input fails
     */
    public void rcInputText(String text, String row, String rowOperator,
        String col, String colOperator) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Selects a cell relative to the cell at the current mouse position. If the
     * mouse is not at any cell, the current selected cell is used.
     * 
     * @param direction the direction to move.
     * @param cellCount the amount of cells to move
     * @param clickCount the click count to select the new cell.
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param extendSelection Should this selection be part of a multiple
     *            selection
     * @throws StepExecutionException if any error occurs
     */
    public void rcMove(String direction, int cellCount, int clickCount,
        final int xPos, final String xUnits, final int yPos,
        final String yUnits, final String extendSelection)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Action to read the value of the current selected cell of the JTable to
     * store it in a variable in the Client
     * 
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /**
     * Action to read the value of the passed cell of the JTable to store it in
     * a variable in the Client
     * 
     * @param variable the name of the variable
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @return the text value.
     */
    public String rcReadValue(String variable, String row, String rowOperator,
        String col, String colOperator) {
        int implRow = getRowFromString(row, rowOperator);
        int implCol = getColumnFromString(col, colOperator);

        checkBounds(implCol, getColumnCount(implRow));

        return getCellText(implRow, implCol);
    }

    /**
     * Types <code>text</code> into the component. This replaces the shown
     * content in the current selected cell.
     *
     * @param text the text to type in
     * @throws StepExecutionException If there is no selected cell, or if the
     *             cell is not editable, or if the table cell editor permits the
     *             text to be written.
     */
    public void rcReplaceText(String text) throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Replaces the given text in the given cell coordinates
     * 
     * @param text the text to replace
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     */
    public void rcReplaceText(String text, String row, String rowOperator,
        String col, String colOperator) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Selects the cell of the JTable.<br>
     * With the xPos, yPos, xunits and yUnits the click position inside the cell
     * can be defined.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param clickCount The number of clicks with the right mouse button
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param extendSelection Should this selection be part of a multiple
     *            selection
     * @param button what mouse button should be used
     * @throws StepExecutionException If the row or the column is invalid
     */
    public void rcSelectCell(final String row, final String rowOperator,
        final String col, final String colOperator, final int clickCount,
        final int xPos, final String xUnits, final int yPos,
        final String yUnits, final String extendSelection, int button)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * check if table has header
     * 
     * @return true if table contains header, false otherwise
     */
    private boolean hasHeader() {
        return countChildElements("//th") > 0; //$NON-NLS-1$
    }

    /**
     * get row count
     * 
     * @return number of rows
     */
    private int getRowCount() {
        String locator = getComponent().getLocator();
        WebElement table = getRobot().getElement(locator);
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        return rows.size();
    }

    /**
     * get column count for row
     * 
     * @param row row
     * @return number of columns
     */
    private int getColumnCount(int row) {
        String locator = getComponent().getLocator();
        WebElement table = getRobot().getElement(locator);
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        List<WebElement> columns = rows.get(row).findElements(
                By.xpath(".//th | .//td"));
        return columns.size();
    }

    /**
     * get column count (longest row defines count)
     * 
     * @return number of columns
     */
    private int getColumnCount() {
        int maxCount = 0;
        int rowCount = getRowCount();
        for (int i = 0; i < rowCount; i++) {
            int rowLength = getColumnCount(i);
            maxCount = Math.max(maxCount, rowLength);
        }
        return maxCount;
    }

    /**
     * Finds the first column which contains the value <code>value</code> in the
     * given row and selects the cell.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param value the value
     * @param clickCount the number of clicks
     * @param regex search using regex
     * @param extendSelection Should this selection be part of a multiple
     *            selection
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param button what mouse button should be used
     */
    public void rcSelectCellByColValue(String row, String rowOperator,
        final String value, final String regex, int clickCount,
        final String extendSelection, final String searchType, int button) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Finds the first row which contains the value <code>value</code> in column
     * <code>col</code> and selects this row.
     *
     * @param col the column to select
     * @param colOperator the column header operator
     * @param value the value
     * @param clickCount the number of clicks
     * @param regexOp the regex operator
     * @param extendSelection Should this selection be part of a multiple
     *            selection
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param button what mouse button should be used
     */
    public void rcSelectRowByValue(String col, String colOperator,
        final String value, final String regexOp, int clickCount,
        final String extendSelection, final String searchType, int button) {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies the editable property of the current selected cell.
     *
     * @param editable The editable property to verify.
     */
    public void rcVerifyEditable(boolean editable) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies the editable property of the given indices.
     *
     * @param editable The editable property to verify.
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     */
    public void rcVerifyEditable(boolean editable, String row,
        String rowOperator, String col, String colOperator) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies the editable property of the cell under current mouse position.
     *
     * @param editable the editable property to verify.
     */
    public void rcVerifyEditableMousePosition(boolean editable) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies the editable property of the selected cell.
     *
     * @param editable the editable property to verify.
     */
    public void rcVerifyEditableSelected(boolean editable) {
        rcVerifyEditable(editable);
    }

    /**
     * Verifies the rendered text inside the currently selected cell.
     *
     * @param text The cell text to verify.
     * @throws StepExecutionException If there is no selected cell, or if the
     *             rendered text cannot be extracted.
     */
    public void rcVerifyText(String text) throws StepExecutionException {

        rcVerifyText(text, MatchUtil.DEFAULT_OPERATOR);
    }

    /**
     * Verifies the rendered text inside the currently selected cell.
     *
     * @param text The cell text to verify.
     * @param operator The operation used to verify
     * @throws StepExecutionException If there is no selected cell, or if the
     *             rendered text cannot be extracted.
     */
    public void rcVerifyText(String text, String operator)
        throws StepExecutionException {

        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies the rendered text inside the passed cell.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param text The cell text to verify.
     * @param operator The operation used to verify
     * @throws StepExecutionException If the row or the column is invalid, or if
     *             the rendered text cannot be extracted.
     */
    public void rcVerifyText(String text, String operator, final String row,
        final String rowOperator, final String col, final String colOperator)
        throws StepExecutionException {
        int implRow = getRowFromString(row, rowOperator);
        int implCol = getColumnFromString(col, colOperator);

        checkBounds(implCol, getColumnCount(implRow));

        String actual = getCellText(implRow, implCol);

        Verifier.match(actual, text, operator);
    }

    /**
     * Verifies the rendered text inside cell at the mouse position on screen.
     *
     * @param text The cell text to verify.
     * @param operator The operation used to verify
     * @throws StepExecutionException If there is no selected cell, or if the
     *             rendered text cannot be extracted.
     */
    public void rcVerifyTextAtMousePosition(String text, String operator)
        throws StepExecutionException {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Verifies, if value exists in row.
     *
     * @param row The row of the cell.
     * @param rowOperator the row header column
     * @param value The cell text to verify.
     * @param operator The operation used to verify
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param exists true if value exists, false otherwise
     * @throws StepExecutionException If the row or the column is invalid, or if
     *             the rendered text cannot be extracted.
     */
    public void rcVerifyValueInRow(final String row, final String rowOperator,
        final String value, final String operator, final String searchType,
        boolean exists) throws StepExecutionException {

        int implRow = getRowFromString(row, rowOperator);
        boolean valueExists = false;
        int colCount = getColumnCount(implRow);

        for (int i = 0; i < colCount; i++) {
            String cellText = getCellText(implRow, i);
            if (MatchUtil.getInstance().match(cellText, value, operator)) {
                valueExists = true;
                break;
            }
        }

        Verifier.equals(exists, valueExists);
    }

    /**
     * Verifies, if value exists in column.
     *
     * @param col The column of the cell.
     * @param colOperator the column header operator
     * @param value The cell text to verify.
     * @param operator The operation used to verify
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @param exists true if value exists, false otherwise
     * @throws StepExecutionException If the row or the column is invalid, or if
     *             the rendered text cannot be extracted.
     */
    public void rcVerifyValueInColumn(final String col,
        final String colOperator, final String value, final String operator,
        final String searchType, boolean exists) throws StepExecutionException {

        final int implCol = getColumnFromString(col, colOperator);
        checkBounds(implCol, getColumnCount());

        boolean valueExists = false;
        int rowCount = getRowCount();
        for (int i = 0; i < rowCount; i++) {
            if (implCol < getColumnCount(i)) {
                String cellText = getCellText(i, implCol);
                if (MatchUtil.getInstance().match(cellText, value, operator)) {
                    valueExists = true;
                    break;
                }
            }
        }

        Verifier.equals(exists, valueExists);
    }

    /**
     * Gets row index from string with index or text of first row
     *
     * @param row index or value in first col
     * @param operator the row header operator
     * @return integer of String of row
     */
    private int getRowFromString(String row, String operator) {
        int rowInt = -1;
        boolean hasHeader = hasHeader();
        try {
            rowInt = Integer.parseInt(row);
            if (!hasHeader) {
                rowInt = IndexConverter.toImplementationIndex(rowInt);
            }
            if (rowInt == -1) {
                if (!hasHeader) {
                    throw new StepExecutionException("No Header", //$NON-NLS-1$
                            EventFactory
                                    .createActionError(TestErrorEvent.NO_HEADER));
                }
            }
        } catch (NumberFormatException nfe) {
            for (int i = 0; i < getRowCount(); i++) {
                String cellTxt = getCellText(i, 0);
                if (MatchUtil.getInstance().match(cellTxt, row, operator)) {
                    return i;
                }
            }
            throw new StepExecutionException("no such row found", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        checkBounds(rowInt, getRowCount());
        return rowInt;
    }

    /**
     * get the value of a cell
     * 
     * @param row row
     * @param col column
     * @return value of cell
     */
    private String getCellText(int row, int col) {
        String locator = getComponent().getLocator();
        WebElement table = getRobot().getElement(locator);
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        List<WebElement> columns = rows.get(row).findElements(
                By.xpath(".//th | .//td"));
        WebElement cell = columns.get(col);

        return cell.getText();
    }

    /**
     * Gets column index from string with header name or index
     *
     * @param col Headername or index of column
     * @param operator the column header operator
     * @return column index
     */
    private int getColumnFromString(String col, String operator) {
        int column = -1;
        try {
            int usrIdxCol = Integer.parseInt(col);
            if (usrIdxCol == 0) {
                usrIdxCol = usrIdxCol + 1;
            }
            column = IndexConverter.toImplementationIndex(usrIdxCol);
        } catch (NumberFormatException nfe) {
            try {
                if (!hasHeader()) {
                    throw new StepExecutionException("No Header", //$NON-NLS-1$
                            EventFactory
                                    .createActionError(TestErrorEvent.NO_HEADER));
                }
                column = getIndexOfHeader(col, operator);
                if (column < 0) {
                    throw new StepExecutionException("no such column found", //$NON-NLS-1$
                            EventFactory
                                    .createActionError(TestErrorEvent.NOT_FOUND));
                }
            } catch (IllegalArgumentException iae) {
                // do nothing here
            }
        }
        return column;
    }

    /**
     * get index of column header
     * 
     * @param col column
     * @param operator the column header operator
     * @return index of table header
     */
    private int getIndexOfHeader(String col, String operator) {
        if (hasHeader()) {
            int colCount = getColumnCount();
            for (int i = 0; i < colCount; ++i) {
                String cellText = getCellText(0, i);
                if (MatchUtil.getInstance().match(cellText, col, operator)) {
                    return i;
                }
            }
        }
        return -1; // header not found
    }

    /**
     * Checks wether <code>0 <= value < count</code>.
     * 
     * @param value The value to check.
     * @param count The upper bound.
     */
    private void checkBounds(int value, int count) {
        if (value < 0 || value >= count) {
            throw new StepExecutionException("Invalid row/column: " + value, //$NON-NLS-1$
                    EventFactory
                            .createActionError(TestErrorEvent.INVALID_INDEX));
        }
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public String rcReadValueAtMousePosition(String variable) {
        StepExecutionException.throwUnsupportedAction();
        return StringConstants.EMPTY;
    }
}
