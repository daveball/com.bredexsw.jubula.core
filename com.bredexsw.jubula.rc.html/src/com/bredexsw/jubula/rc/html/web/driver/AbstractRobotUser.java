/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.driver;

import com.bredexsw.jubula.rc.html.WebAUTServer;


/**
 * @author BREDEX GmbH
 * @created 20.01.2010
 */
public class AbstractRobotUser {
    /**
     * @return the robot
     */
    protected RobotWebImpl getRobot() {
        return WebAUTServer.getInstance().getWebRobot();
    }
}
