/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms.select;

import java.util.Arrays;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * @author BREDEX GmbH
 * @created Dec 1, 2009
 */
public class ComboBoxImplClass extends AbstractHTMLImplClass {

    /**
     * The SelectForm helper
     */
    private AbstractSelectFormHelper m_selectFormHelper;

    /**
     * Gets the SelectFormHelper. The helper is created once per instance.
     *
     * @return The SelectForm helper
     */
    public AbstractSelectFormHelper getSelectFormHelper() {
        if (m_selectFormHelper == null) {
            m_selectFormHelper = new DefaultSelectFormHelper(getRobot());
        }
        return m_selectFormHelper;
    }

    /**
     * Selects <code>index</code> in the combobox.
     *
     * @param index The index to select
     */
    public void rcSelectIndex(String index) {
        if (isEnabled(getComponent().getLocator())) {
            int implIdx = IndexConverter
                    .toImplementationIndex(getSelectFormHelper().parseIndex(
                            index));
            getSelectFormHelper().selectIndex(getComponent(),
                    new Integer(implIdx).toString());
        }

    }

    /**
     * Selects a value from the list of the combobox
     * 
     * @param valueList The value or list of values to (not)select
     * @param operator if regular expressions are used
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     */
    public void rcSelectValue(String valueList, String operator,
        final String searchType) {

        selectValue(valueList, String.valueOf(VALUE_SEPARATOR), operator,
                searchType);
    }

    /**
     * Selects a value from the list of the combobox
     *
     * @param valueList the item(s) which should be (not)selected.
     * @param separator The seperator if <code>text</code> is an enumeration of
     *            values. Not supported by this implementation class
     * @param operator if regular expressions are used
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     */
    private void selectValue(String valueList, String separator,
        String operator, final String searchType) {
        String[] values = getSelectFormHelper().split(valueList, separator);
        select(values, operator, searchType);
    }

    /**
     * Verifies the editable property.
     *
     * @param editable The editable property to verify.
     */
    public void rcVerifyEditable(boolean editable) {
        Verifier.equals(editable,
                getRobot().isEditable(getComponent().getLocator()));
    }

    /**
     * Verifies if the combobox has <code>index</code> selected.
     *
     * @param index The index to verify
     * @param isSelected If the index should be selected or not.
     */
    public void rcVerifySelectedIndex(String index, boolean isSelected) {
        int implIdx = IndexConverter
                .toImplementationIndex(getSelectFormHelper().parseIndex(index));
        Integer actual = getSelectFormHelper().getSelectedIndex(getComponent());
        Verifier.equals(implIdx, actual.intValue(), isSelected);
    }

    /**
     * Verifies if the passed text is currently selected in the combobox.
     *
     * @param text The text to verify.
     * @param operator The operator used to verify
     */
    public void rcVerifyText(String text, String operator) {
        Verifier.match(getSelectFormHelper().getSelectedLabel(getComponent()),
                text, operator);
    }

    /**
     * Verifies if the passed text is currently selected in the combobox.
     *
     * @param text The text to verify.
     */
    public void rcVerifyText(String text) {
        rcVerifyText(text, MatchUtil.DEFAULT_OPERATOR);
    }

    /**
     * Verifies if the list contains an element that renderes <code>value</code>
     * 
     * @param value The text to verify
     * @param operator The operator used to verify
     * @param exists If the value should exist or not.
     */
    public void rcVerifyContainsValue(String value, String operator,
        boolean exists) {
        final boolean contains = getSelectFormHelper().containsValue(
                getComponent(), value, operator);
        Verifier.equals(exists, contains);
    }

    /**
     * Sets the text into the combobox, by typing it into the textfield
     *
     * @param text the text which should be typed in.
     */
    public void rcInputText(String text) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Types <code>text</code> into the component. This replaces the shown
     * content.
     *
     * @param text the text to type in
     */
    public void rcReplaceText(String text) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * Selects the specified item in the combobox.
     * 
     * @param values the values which should be (not) selected
     * @param operator if regular expressions are used
     * @param searchType Determines where the search begins ("relative" or
     *            "absolute")
     * @throws StepExecutionException if an error occurs during selecting the
     *             item
     * @throws IllegalArgumentException if <code>component</code> or
     *             <code>text</code> are null
     */
    public void select(final String[] values, String operator, String searchType)
        throws StepExecutionException, IllegalArgumentException {

        for (int i = 0; i < values.length; i++) {
            String text = values[i];
            Validate.notNull(text, "text must not be null"); //$NON-NLS-1$
        }

        Integer[] indices = getSelectFormHelper().findIndicesOfValues(
                getComponent(), values, operator, searchType);
        Arrays.sort(indices);
        if (indices.length == 0) {
            throw new StepExecutionException(
                    "Text '" + Arrays.asList(values).toString() //$NON-NLS-1$
                            + "' not found", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        if (isEnabled(getComponent().getLocator())) {
            getSelectFormHelper().selectIndex(getComponent(),
                    new Integer(indices[0]).toString());
        }

    }

    /**
     * Action to read the value of a JComboBox to store it in a variable in the
     * Client
     * 
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        final String selectedValue = getSelectFormHelper().getSelectedLabel(
                getComponent());
        return selectedValue != null ? selectedValue : StringConstants.EMPTY;
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }
}
