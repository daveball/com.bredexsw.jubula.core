/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.text;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * @author BREDEX GmbH
 * @created Jan 13, 2011
 */
public class TextualListImplClass extends AbstractHTMLImplClass {
    /**
     * <code>CHILD_ELEMENT_LIST_ITEM</code>
     */
    private static final String CHILD_ELEMENT_LIST_ITEM = "//li"; //$NON-NLS-1$

    /**
     * {@inheritDoc}
     */
    public void rcVerifySelectedValue(String text, String operator,
            boolean isSelected) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcVerifySelectedIndex(String index, boolean isSelected) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDragValue(int mouseButton, String modifierSpecification,
            String text, String operator, String searchType) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDragIndex(int mouseButton, String modifierSpecification,
            int indexOnly) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDropValue(String text, String operator, String searchType,
            int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public void rcDropIndex(int indexOnly, int delayBeforeDrop) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }
    
    /**
     * {@inheritDoc}
     */
    public void rcVerifyText(String text, String operator) {
        StepExecutionException.throwUnsupportedAction();
    }

    /**
     * {@inheritDoc}
     */
    public String rcReadValue(String variable) {
        StepExecutionException.throwUnsupportedAction();
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * as there is no selection concept for textual lists the
     * parameter extendSelection is ignored
     */
    public void rcSelectIndex(String index, String extendSelection,
            int mouseButton, int clickCount) {
        int numberOfListElements = getNumberOfListElements();
        int indexToSelect = IndexConverter.intValue(index);

        if (numberOfListElements < indexToSelect || indexToSelect < 1) {
            throw new StepExecutionException(
                    "No item with index '" + indexToSelect + "' found.", //$NON-NLS-1$ //$NON-NLS-2$
                    EventFactory
                            .createActionError(TestErrorEvent.INVALID_INDEX));
        }
        
        getRobot().click(getListItemLocator(indexToSelect), null,
                ClickOptions.create().setClickCount(clickCount)
                    .setMouseButton(mouseButton), 50,
                        false, 50, false);
    }

    /**
     * {@inheritDoc} 
     * 
     * as there is no selection concept for textual lists the
     * parameter extendSelection and searchType are ignored
     */
    public void rcSelectValue(String text, String operator, String searchType,
            String extendSelection, int mouseButton, int clickCount) {
        List<Integer> matchingIndices = findIndices(text, operator);
        if (matchingIndices.size() < 1) {
            throw new StepExecutionException(
                    "No item with value '" + text + "' found.", //$NON-NLS-1$ //$NON-NLS-2$
                    EventFactory
                            .createActionError(TestErrorEvent.NOT_FOUND));
        }
        for (Integer i : matchingIndices) {
            getRobot().click(getListItemLocator(i), null,
                    ClickOptions.create().setClickCount(clickCount)
                        .setMouseButton(mouseButton), 50,
                            false, 50, false);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public void rcVerifyContainsValue(String text, String operator,
            boolean exists) {
        int numberOfListElements = getNumberOfListElements();
        boolean contains = false;
        for (int i = 1; i <= numberOfListElements; i++) {
            String acutalText = getRobot().getText(getListItemLocator(i));
            contains = contains | MatchUtil.getInstance()
                    .match(acutalText, text, operator);
        }
        Verifier.equals(exists, contains);
    }
    
    /**
     * @param text
     *            the text to find
     * @param operator
     *            the operator to use
     * @return the indices of the matched list items
     */
    protected List<Integer> findIndices(String text, String operator) {
        int numberOfListElements = getNumberOfListElements();
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 1; i <= numberOfListElements; i++) {
            String acutalText = getRobot().getText(getListItemLocator(i));
            if (MatchUtil.getInstance().match(acutalText, text, operator)) {
                indices.add(i);
            }
        }
        return indices;
    }

    /**
     * @param index
     *            the index to get the locator for; starting with 1 for the
     *            first element
     * @return returns the locator for a given list item index
     */
    protected String getListItemLocator(int index) {
        return getComponent().getLocator() + "/li[" + index + "]"; //$NON-NLS-1$ //$NON-NLS-2$   
    }

    /**
     * @return the number of <li>-child elements for the current component
     */
    protected int getNumberOfListElements() {
        return countChildElements(CHILD_ELEMENT_LIST_ITEM);
    }
}
