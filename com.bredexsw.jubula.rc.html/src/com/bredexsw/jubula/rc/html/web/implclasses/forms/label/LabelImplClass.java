/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.web.implclasses.forms.label;

import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;

import com.bredexsw.jubula.rc.html.web.implclasses.AbstractHTMLImplClass;

/**
 * @author BREDEX GmbH
 * @created Nov 10, 2009
 */
public class LabelImplClass extends AbstractHTMLImplClass {
    
    /**
     * Verifies if the label shows the passed text.
     *
     * @param text The text to verify.
     * @param operator The operator used to verify
     */
    public void rcVerifyText(String text, String operator) {
        Verifier.match(getLabelText(), text, operator);
    }
    /**
     * Verifies if the label shows the passed text.
     *
     * @param text The text to verify.
     */
    public void rcVerifyText(String text) {
        rcVerifyText(text, MatchUtil.DEFAULT_OPERATOR);
    }

    /**
     * Action to read the value of a Label to store it in a variable
     * in the Client
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        return getLabelText();
    }
    
    /**
     * get text of label
     * @return text of label
     */
    protected String getLabelText() {
        return getRobot().getText(getComponent().getLocator());
    }

    /** {@inheritDoc} */
    public String[] getTextArrayFromComponent() {
        return new String[] {getLabelText()};
    }
}
