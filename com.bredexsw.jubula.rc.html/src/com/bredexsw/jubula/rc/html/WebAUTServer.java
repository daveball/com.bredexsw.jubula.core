/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.communication.internal.message.AUTServerStateMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.rc.common.Constants;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.driver.RobotTiming;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.rc.common.listener.BaseAUTListener;
import org.eclipse.jubula.rc.common.listener.DisabledCheckListener;
import org.eclipse.jubula.rc.common.listener.DisabledRecordListener;
import org.eclipse.jubula.rc.common.registration.IRegisterAut;
import org.eclipse.jubula.toolkit.html.Browser;
import org.eclipse.jubula.toolkit.html.BrowserSize;
import org.eclipse.jubula.tools.internal.constants.AUTServerExitConstants;
import org.eclipse.jubula.tools.internal.exception.CommunicationException;
import org.eclipse.jubula.tools.internal.exception.JBVersionException;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.listener.MappingListener;
import com.bredexsw.jubula.rc.html.listener.TechnicalComponentHandler;
import com.bredexsw.jubula.rc.html.web.driver.RobotFactoryWebImpl;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;
import com.bredexsw.jubula.rc.html.web.driver.omm.ObjectCollectionMode;

/**
 * @author BREDEX GmbH
 * @created 13.08.2009
 */
public class WebAUTServer extends AUTServer {
    /**
     * constant for top level frame selection
     */
    public static final String ROOT_WINDOW_PATH = "relative=top"; //$NON-NLS-1$

    /**
     * the maximum AUT response time
     */
    private static final int MAX_AUT_RESPONSE_INTERVAL = RobotTiming
            .getMaxAUTResponseInterval();

    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory.getLogger(WebAUTServer.class);

    /**
     * <code>ARG_URL</code> the position of the URL in the
     * StartHTMLAUTServerCommand to load at the beginning of AUT start
     */
    private static final int ARG_URL = 1;

    /**
     * <code>ARG_BROWSER</code> the position of the Browser type in the
     * StartHTMLAUTServerCommand to load at the beginning of AUT start
     */
    private static final int ARG_BROWSER = 2;


    /**
     * <code>ARG_BROWSER_SIZE</code> the position of the Browser size in the
     * StartHTMLAUTServerCommand to load at the beginning of AUT start
     */
    private static final int ARG_BROWSER_SIZE = 3;
    
    /**
     * <code>ARG_ID_ATTRIBUTE</code> the position of the id attribute in the
     * StartHTMLAUTServerCommand to load at the beginning of AUT start
     */
    private static final int ARG_ID_ATTRIBUTE = 7;

    /**
     * <code>MIN_WEB_ARGS_REQUIRED</code> The minimum amount of arguments that
     * are required to startup the Web AUT Server
     */
    private static final int MIN_WEB_ARGS_REQUIRED = 8;

    /**
     * <code>instance</code>: the singleton instance
     */
    private static WebAUTServer instance = null;

    /**
     * <code>m_robotFactory</code> the robot factory
     */
    private RobotFactoryWebImpl m_robotFactory = null;

    /**
     * No-argument constructor.
     */
    protected WebAUTServer() {
        super(new MappingListener(), new DisabledRecordListener(),
                new DisabledCheckListener());
        m_robotFactory = new RobotFactoryWebImpl();
    }

    /**
     * @param args the main arguments
     */
    public static void main(String[] args) {
        validateAndLogMainArgsCount(args, MIN_WEB_ARGS_REQUIRED);
        WebAUTServer.getInstance().setArgs(args);
        WebAUTServer.getInstance().start(false);
    }

    /**
     * Method to get the single instance of this class.
     * 
     * @return the instance of this Singleton
     */
    public static WebAUTServer getInstance() {
        if (instance == null) {
            instance = new WebAUTServer();
            setInstance(instance);
        }
        return instance;
    }

    /**
     * @param robot the robot to reset to the root window context
     */
    private static void resetToRootWindowContext(RobotWebImpl robot) {
        if (robot.isLaunched() && robot.isFrameSelected()) {
            robot.selectFrame(ROOT_WINDOW_PATH);
        }
    }

    /**
     * @param args the arguments to set
     */
    @Override
    public void setArgs(String args[]) {
        System.out.println("Setting args");
        // required arguments
        setAutAgentHost(args[Constants.ARG_REG_HOST]);
        setAutAgentPort(args[Constants.ARG_REG_PORT]);
        setAutID(args[Constants.ARG_AUT_NAME]);
        m_robotFactory.setInitialURL(args[ARG_URL]);

        String[] browserArgs = args[ARG_BROWSER].split(" ");
        if (browserArgs == null || browserArgs.length < 1) {
            log.error("Browser argument not supplied."); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.AUT_START_ERROR);
        }
        Browser browser = Browser.valueOf(browserArgs[0]);
        String executablePath = null;
        if (browserArgs.length > 1) {
            executablePath = browserArgs[1];
        }
        m_robotFactory.setBrowser(browser);
        m_robotFactory.setBrowserExecutablePath(executablePath);
        m_robotFactory.setIdAttributeName(args[ARG_ID_ATTRIBUTE]);
        m_robotFactory
                .setBrowserSize(getBrowserDimension(args[ARG_BROWSER_SIZE]));
    }

    private Dimension getBrowserDimension(String arg) {
        if (StringUtils.isEmpty(arg)) {
            return null; // Full screen.
        }
        try {
            BrowserSize size = BrowserSize.valueOf(arg);
            if (size != null && BrowserSize.FULLSCREEN == size) {
                return null;
            } else if (size != null) {
                return new Dimension(size.getWidth(), size.getHeight());
            }
        } catch (Exception e) {
        }

        String[] dimensions = arg.split("x");
        if (dimensions.length == 2) {
            int width = Integer.valueOf(dimensions[0]);
            int height = Integer.valueOf(dimensions[1]);
            return new Dimension(width, height);
        }
        
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(boolean isRcpAccessible) {
        try {
            final RobotWebImpl robot = getWebRobot();
            // TODO daveb - what are we trying to do here?
            // if (robot.getEval("navigator.userAgent").contains("IE")) { //$NON-NLS-1$
            // robot.useXpathLibrary("javascript-xpath"); //$NON-NLS-1$
            // }
            robot.setLaunched(true);
            IRegisterAut autReg = parseAutReg();
            if (autReg == null) {
                String errorMessage = "Unable to initialize connection to AUT Agent: No connection information provided."; //$NON-NLS-1$
                log.error(errorMessage);
                sendExitReason(errorMessage,
                        AUTServerExitConstants.EXIT_MISSING_AGENT_INFO);
            } else {
                try {
                    autReg.register();
                    setupBrowserHeartbeat(robot);
                } catch (IOException ioe) {
                    log.error("Exception during AUT registration", ioe); //$NON-NLS-1$
                    robot.stop();
                    System.exit(AUTServerExitConstants.AUT_START_ERROR);
                }
            }
        } catch (IllegalArgumentException iae) {
            log.error("Exception in start()", iae); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.EXIT_INVALID_ARGS);
        } catch (SecurityException se) {
            log.error("Exception in start()", se); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.EXIT_SECURITY_VIOLATION_REFLECTION);
        } catch (UnsupportedClassVersionError ucve) {
            log.error("Exception in start()", ucve); //$NON-NLS-1$
            try {
                getCommunicator()
                        .send(new AUTServerStateMessage(
                                AUTServerStateMessage.EXIT_AUT_WRONG_CLASS_VERSION,
                                ucve.getMessage()));
            } catch (CommunicationException ce) {
                log.error("Exception in start()", ce); //$NON-NLS-1$
            }
            System.exit(AUTServerExitConstants.EXIT_AUT_WRONG_CLASS_VERSION);
        } catch (JBVersionException ve) {
            log.error("Exception in start()", ve); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.EXIT_UNKNOWN_ITE_CLIENT);
        } catch (RuntimeException re) {
            log.error("Exception in start()", re); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.AUT_START_ERROR);
        }
    }

    /**
     * @param robot the robot to use to check the heartbeat
     */
    private void setupBrowserHeartbeat(final RobotWebImpl robot) {
        final int scheduleDelay = 1000;
        final Timer responseInterval = new Timer(true);
        TimerTask heartbeat = new TimerTask() {
            @Override
            public void run() {
                final TimerTask systemExit = new TimerTask() {
                    @Override
                    public void run() {
                        log.info("Browser did not respond within: " //$NON-NLS-1$
                                + MAX_AUT_RESPONSE_INTERVAL
                                + " ms - shutting down!"); //$NON-NLS-1$
                        robot.stop();
                        System.exit(AUTServerExitConstants.EXIT_COMMUNICATION_ERROR);
                    }
                };
                synchronized (robot) {
                    responseInterval.schedule(systemExit,
                            MAX_AUT_RESPONSE_INTERVAL);
                    try {
                        // invocation to check if browser instance is alive
                        robot.checkDriverStatus();
                    } catch (Throwable t) {
                        if (log.isDebugEnabled()) {
                            log.debug("Stopping due to heartbeat : " + t.getLocalizedMessage(), t);
                        } else {
                            log.info("Stopping due to heartbeat: " + t.getLocalizedMessage());
                        }
                        robot.stop();
                        System.exit(AUTServerExitConstants.EXIT_COMMUNICATION_ERROR);
                    } finally {
                        systemExit.cancel();
                        responseInterval.purge();
                    }
                }
            }
        };
        Timer keepAlive = new Timer(true);
        keepAlive.schedule(heartbeat, scheduleDelay, MAX_AUT_RESPONSE_INTERVAL
                + scheduleDelay);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addToolkitEventListener(BaseAUTListener listener) {
        if (log.isInfoEnabled()) {
            log.info("installing EventListener " //$NON-NLS-1$ 
                    + listener.toString());
        }
        if (listener instanceof MappingListener) {
            try {
                ObjectCollectionMode.getInstance().start();
            } catch (InvocationTargetException e) {
                log.error("error starting object mapping mode due to exception: " + e.toString()); //$NON-NLS-1$
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IRobot getRobot() {
        RobotWebImpl robot = (RobotWebImpl) m_robotFactory.getRobot();
        try {
            resetToRootWindowContext(robot);
        } catch (WebDriverException se) {
            // This might happen if the window is closed
            // necessary for multi window
        }
        return robot;
    }

    /**
     * get web robot
     * 
     * @return web robot
     */
    public RobotWebImpl getWebRobot() {
        return (RobotWebImpl) getRobot();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void removeToolkitEventListener(BaseAUTListener listener) {
        if (listener instanceof MappingListener) {
            try {
                ObjectCollectionMode.getInstance().stop();
            } catch (InvocationTargetException e) {
                log.error("error stopping object mapping mode due to exception: " + e.toString()); //$NON-NLS-1$
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startTasks() throws ExceptionInInitializerError {
        // empty
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void invokeAUT() throws ExceptionInInitializerError {
        // empty
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void shutdown() {
        super.shutdown();
        getWebRobot().stop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object findComponent(IComponentIdentifier ci, int timeout)
        throws ComponentNotFoundException, IllegalArgumentException {
        return TechnicalComponentHandler.getInstance().findComponent(ci,
                timeout);
    }
}