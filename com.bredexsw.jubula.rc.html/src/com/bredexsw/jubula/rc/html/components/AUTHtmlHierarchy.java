package com.bredexsw.jubula.rc.html.components;

import html.Element;
import html.forms.select.ComboBox;

import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.rc.common.AUTServerConfiguration;
import org.eclipse.jubula.tools.internal.xml.businessmodell.ComponentClass;
import org.eclipse.jubula.tools.internal.xml.businessmodell.Property;

import com.bredexsw.jubula.rc.html.components.json.JSONConstantsTraverseDOM;

/**
 * @author Markus Tiede
 * @since 18.11.2011
 */
public class AUTHtmlHierarchy extends AbstractAUTHtmlHierarchy {

    /** name of "selector" property in component configuration */
    private static final String PROP_NAME_SELECTOR = "selector"; //$NON-NLS-1$
    
    /** name of "selectorHtmlClass" property in component configuration */
    private static final String PROP_NAME_CLASS = "targetClassName"; //$NON-NLS-1$
    
    /** name of "extended type" attribute in component configuration */
    private static final String ATTR_NAME_TYPE = "type"; //$NON-NLS-1$

    /** name of "class" attribute in component configuration */
    private static final String ATTR_NAME_CLASS = "class"; //$NON-NLS-1$
    
    /**
     * Constructor
     */
    public AUTHtmlHierarchy() {
        super();
    }

    /** {@inheritDoc} */
    @Override
    protected String getComponentClass(JSONObject jo) {
        Set<ComponentClass> st = AUTServerConfiguration.getInstance()
                .getSupportedTypes();
        
        JSONObject attributes = jo
                .getJSONObject(JSONConstantsTraverseDOM.ATTRIBUTES_KEY);
        String technicalTagName = attributes
                .getString(JSONConstantsTraverseDOM.TAG_NAME_KEY);

        String technicalTagType = null;
        if (attributes.has(JSONConstantsTraverseDOM.TYPE_KEY)) {
            technicalTagType = attributes
                    .getString(JSONConstantsTraverseDOM.TYPE_KEY);
        }
        String technicalSize = null;
        if (attributes.has(JSONConstantsTraverseDOM.SIZE_KEY)) {
            technicalSize = attributes
                    .getString(JSONConstantsTraverseDOM.SIZE_KEY);
        }
        String technicalTagClass = null;
        if (attributes.has(JSONConstantsTraverseDOM.CLASSNAME_KEY)) {
            technicalTagClass = attributes
                    .getString(JSONConstantsTraverseDOM.CLASSNAME_KEY);
        }
        if (technicalTagName.equals("select")) { //$NON-NLS-1$
            if (technicalSize == null) {
                return ComboBox.class.getName();
            }
            int size = 1;
            try {
                size = Integer.parseInt(technicalSize);
            } catch (NumberFormatException e) {
                // ignore -> invalid value in html size attribute
            }
            if (size == 1) {
                return ComboBox.class.getName();
            }
            return html.forms.select.List.class.getName();
        }
        
        String matched = Element.class.getName();
        for (ComponentClass c : st) {
            List<Property> classProperties = c.getProperties();
            String selector = StringUtils.EMPTY;
            String selectorClassname = null;
            for (Property prop : classProperties) {
                if (PROP_NAME_SELECTOR.equals(prop.getName())) {
                    selector = prop.getValue();
                } else if (PROP_NAME_CLASS.equals(prop.getName())) {
                    selectorClassname = prop.getValue();
                }
            }

            // assuming that all selectors are of the format:
            // <tag>[<property_name>=<property_value>]
            // where the property tester is optional (0..n).
            String [] selectorSplit = StringUtils.split(selector, "[=]"); //$NON-NLS-1$
            String supportedTagName = selectorSplit[0];
            String extendedTypeValue = null;
            for (int i = 1; i < selectorSplit.length; i = i + 2) {
                if (ATTR_NAME_TYPE.equals(selectorSplit[i])) {
                    extendedTypeValue = selectorSplit[i + 1];
                    break;
                }
            }
            
            if (supportedTagName.equals(technicalTagName)) {
                if (extendedTypeValue == null 
                            || extendedTypeValue.equals(technicalTagType)) {
                        
                    if(selectorClassname==null){ 
                        matched = c.getName();
                        
                    } else if (technicalTagClass.contains(selectorClassname)) {
                        return c.getName();
                    }
                }
            }
        }
        return matched;
    }
}
