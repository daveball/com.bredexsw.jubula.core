/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.components;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.Constants;
import org.eclipse.jubula.rc.common.components.AUTHierarchy;
import org.eclipse.jubula.rc.common.components.HierarchyContainer;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.tools.internal.messagehandling.MessageIDs;
import org.eclipse.jubula.tools.internal.objects.ComponentIdentifier;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.html.WebAUTServer;
import com.bredexsw.jubula.rc.html.components.json.JSONConstantsTraverseDOM;
import com.bredexsw.jubula.rc.html.constants.JSConstants;
import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;
import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;
import com.bredexsw.jubula.rc.html.web.model.HtmlHierarchyContainer;

/**
 * the HTML AUT hierarchy
 *
 * @author BREDEX GmbH
 * @created 23.11.2009
 *
 */
public abstract class AbstractAUTHtmlHierarchy extends AUTHierarchy {
    /**
     * <code>DOM_TRAVERSAL_SCRIPT_ID</code>
     */
    private static final String DOM_TRAVERSAL_SCRIPT_ID = "UI_AUTOMATION.DOM_TRAVERSAL"; //$NON-NLS-1$

    /** the logger */
    private static final Logger LOG = LoggerFactory
            .getLogger(AbstractAUTHtmlHierarchy.class);

    /** the bp to find components */
    private static FindHtmlComponentsBP findBP = new FindHtmlComponentsBP();

    /**
     * Constructor
     */
    public AbstractAUTHtmlHierarchy() {
        // currently empty
    }

    /**
     * add the component to hierarchy
     * 
     * @param jsonObjects ALL serialized web components
     */
    private void add(String jsonObjects) {
        JSONArray webComponents = JSONArray.fromObject(jsonObjects);
        int numberOfComponents = webComponents.size();
        HtmlHierarchyContainer[] comp = new HtmlHierarchyContainer[numberOfComponents];

        // create synthetic model classes to put into hierarchy
        for (int i = 0; i < numberOfComponents; i++) {
            JSONObject jo = (JSONObject) webComponents.get(i);
            AbstractWebComponent component = getSyntheticModelWebComponet(jo);
            HtmlHierarchyContainer container = new HtmlHierarchyContainer(
                    component);

            comp[i] = container;
        }

        // define parent <-> child relationship
        for (int i = 0; i < numberOfComponents; i++) {
            JSONObject jo = (JSONObject) webComponents.get(i);
            if (jo.has(JSONConstantsTraverseDOM.CHILDREN_KEY)) {
                JSONArray ja = jo
                        .getJSONArray(JSONConstantsTraverseDOM.CHILDREN_KEY);
                HtmlHierarchyContainer parent = comp[i];
                for (int j = 0; j < ja.size(); j++) {
                    HtmlHierarchyContainer child = comp[ja.getInt(j)];
                    parent.add(child);
                }
            }
        }

        // search for top level components
        List<HtmlHierarchyContainer> topLevelComponents = new ArrayList<HtmlHierarchyContainer>();
        for (int i = 0; i < numberOfComponents; i++) {
            HtmlHierarchyContainer tlc = comp[i];
            if (tlc.getPrnt() == null) {
                topLevelComponents.add(tlc);
            }
        }

        // calculate all fully qualified PATHs -> take for technical component
        for (int i = 0; i < numberOfComponents; i++) {
            HtmlHierarchyContainer tcc = comp[i];
            AbstractWebComponent wc = (AbstractWebComponent) tcc.getCompID();
            String tccXPath = tcc.getFQPath(topLevelComponents);
            wc.setComp(tccXPath);
            addToHierachyMap(tcc);
        }
    }

    /** clean the complete hierarchy */
    public void clean() {
        getRealMap().clear();
        getHierarchyMap().clear();
    }

    /**
     * @param jo the serialized web component as a JSON object
     * @return a synthetic model web component instance
     */
    protected AbstractWebComponent getSyntheticModelWebComponet(JSONObject jo) {
        String componentClassName = getComponentClass(jo);
        try {
            Constructor<?> constructor = Class.forName(componentClassName)
                    .getConstructor();
            Object componentObj = constructor.newInstance();
            if (componentObj instanceof AbstractWebComponent) {
                AbstractWebComponent wc = (AbstractWebComponent) componentObj;
                JSONObject attr = jo
                        .getJSONObject(JSONConstantsTraverseDOM.ATTRIBUTES_KEY);
                wc.setTagName(attr
                        .getString(JSONConstantsTraverseDOM.TAG_NAME_KEY));
                String compName;
                if (attr.has(JSONConstantsTraverseDOM.ID_KEY)) {
                    compName = attr.getString(JSONConstantsTraverseDOM.ID_KEY);
                    wc.setCompId(compName);
                } else {
                    compName = wc.getTagName();
                }
                wc.setName(compName);
                return wc;
            }
        } catch (ClassNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (SecurityException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (NoSuchMethodException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (IllegalArgumentException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (InstantiationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (IllegalAccessException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (InvocationTargetException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    /**
     * this class determines the type of a serialized DOM object
     * 
     * @param jo the serialized web component (DOM component)
     * @return the fully qualified component class name for the given web
     *         component
     */
    protected abstract String getComponentClass(JSONObject jo);

    /**
     * @param fqPath the fully qualified PATH of the web component to get an
     *            identifier for
     * @return a component identifier for the given web component; may return
     *         null if no component identifier could be created
     */
    public IComponentIdentifier getComponentIdentifier(String fqPath) {
        AbstractWebComponent comp = findComponent(fqPath);
        if (comp != null) {
            IComponentIdentifier compID = new ComponentIdentifier();
            compID.setComponentClassName(comp.getClass().getName());
            compID.setSupportedClassName(compID.getComponentClassName());
            compID.setHierarchyNames(getPathToRoot(comp));
            compID.setNeighbours(getComponentContext(comp));
            // try to get some descriptive text
            String descriptiveText = comp.getCompId();
            try {
                if (descriptiveText.trim().length() == 0) {
                    descriptiveText = getRobot().getText(comp.getLocator());
                }
                if (descriptiveText.trim().length() == 0) {
                    descriptiveText = getRobot().getValue(comp.getLocator());
                }
                if (descriptiveText.trim().length() == 0) {
                    String[] options = getRobot().getSelectOptions(
                            comp.getLocator());
                    if (options.length > 0) {
                        descriptiveText = options[0];
                    }
                }
            } catch (WebDriverException e) {
                // ignore
            } finally {
                if (descriptiveText.trim().length() != 0) {
                    compID.setAlternativeDisplayName(descriptiveText);
                }
            }
            try {
                // initially run a find component to ensure hierarchy is
                // up-to-date
                findComponent(compID);
                // now search in up-to-date hierarchy
                if (fqPath.equals(findBP.findComponent(compID, this))) {
                    compID.setEqualOriginalFound(true);
                }
            } catch (ComponentNotFoundException e) {
                // ignore - is ok here
            }
            return compID;
        }
        return null;
    }

    /**
     * recollect all technical components from the web-page
     */
    private void rebuildHierarchy() {
        try {
            getRobot().removeScript(DOM_TRAVERSAL_SCRIPT_ID);
            clean();
            getRobot().addScript(JSConstants.getJSGetTraverseDOMScript(),
                    DOM_TRAVERSAL_SCRIPT_ID);
            String jsGetAllComponents = "return "
                    + JSConstants
                            .getJSAUTHierarchyGetAllTechnicalComponents(getRobot()
                                    .getIdAttribute());
            String technicalComponents = getRobot().js(jsGetAllComponents);
            add(technicalComponents);
        } catch (InvocationTargetException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } finally {
            // calling this method multiple times is ok
            getRobot().removeScript(DOM_TRAVERSAL_SCRIPT_ID);
        }
    }

    /**
     * @param component the component to search the path to the root
     * @return a string list
     */
    private List<String> getPathToRoot(AbstractWebComponent component) {
        List<String> pathToRoot = new ArrayList<String>();
        HierarchyContainer hc = getHierarchyContainer(component);
        do {
            pathToRoot.add(hc.getName());
            hc = hc.getPrnt();
        } while (hc != null);
        Collections.reverse(pathToRoot);
        return pathToRoot;
    }

    /**
     * Used during ObjectMapping.
     * @param xPath the XPATH to search for
     * @return the AbstractWebComponent from the AUT hierarchy
     */
    private AbstractWebComponent findComponent(String xPath) {
        Object o = getRealMap().get(xPath);
        if (o == null) {
            rebuildHierarchy();
            o = getRealMap().get(xPath);
        }
        if (o == null && xPath.startsWith("undefined/")) {
            xPath = xPath.replace("undefined/", "/");
            o = getRealMap().get(xPath);
        }
        if (o == null) {
            LOG.error("Even after rebuilding the hierarchy the xPath: \"" + xPath //$NON-NLS-1$
                    + "\" could not be found in the hierarchy."); //$NON-NLS-1$
        }
        return (AbstractWebComponent) o;
    }

    /**
     * Used during test playback.
     * @param compId the compId
     * @return the web component
     */
    public AbstractWebComponent findComponent(IComponentIdentifier compId)
        throws ComponentNotFoundException {
        // try to find component in hierarchy
        String compXPath = (String) findBP.findComponent(compId, this);
        if (compXPath == null) {
            // if component not in hierarchy --> rebuild
            rebuildHierarchy();
            // search again in hierarchy
            compXPath = (String) findBP.findComponent(compId, this);
        } else {
            // if matching component has been found in hierarchy
            try {
                // check whether it really still exists in AUT
                Number n = getRobot().getXpathCount(compXPath);
                // 0 --> no longer available in AUT
                if (n.intValue() == 0) {
                    // rebuild hierarchy and search again
                    rebuildHierarchy();
                    compXPath = (String) findBP.findComponent(compId, this);
                }
            } catch (WebDriverException e) {
                LOG.warn(e.getLocalizedMessage(), e);
            }
        }

        // still not found --> Component not found
        if (compXPath == null) {
            throw new ComponentNotFoundException(
                    "component not found: " + compId, //$NON-NLS-1$
                    MessageIDs.E_COMPONENT_NOT_FOUND);
        }
        return (AbstractWebComponent) getRealMap().get(compXPath);
    }

    /**
     * {@inheritDoc}
     */
    protected List<String> getComponentContext(AbstractWebComponent component,
        List<String> context) {
        HtmlHierarchyContainer hc = getHierarchyContainer(component);
        HtmlHierarchyContainer parent = (HtmlHierarchyContainer) hc.getPrnt();
        if (parent != null) {
            HtmlHierarchyContainer[] comps = parent.getComponents();
            for (int i = 0; i < comps.length; i++) {
                AbstractWebComponent child = (AbstractWebComponent) comps[i]
                        .getCompID();
                if (!child.equals(component)) {
                    String toAdd = child.getClass().getName()
                            + Constants.CLASS_NUMBER_SEPERATOR + 1;
                    while (context.contains(toAdd)) {
                        int lastCount = Integer
                                .valueOf(
                                        toAdd.substring(toAdd
                                                .lastIndexOf(Constants.CLASS_NUMBER_SEPERATOR) + 1))
                                .intValue();
                        toAdd = child.getClass().getName()
                                + Constants.CLASS_NUMBER_SEPERATOR
                                + (lastCount + 1);
                    }
                    context.add(toAdd);
                }
            }
        }
        return context;
    }

    /**
     * Returns the hierarchy container for <code>component</code>.
     * 
     * @param component the component from the AUT, must no be null
     * @throws IllegalArgumentException if component is null
     * @return the hierarchy container or null if the component is not yet
     *         managed
     */
    private HtmlHierarchyContainer getHierarchyContainer(
        AbstractWebComponent component) throws IllegalArgumentException {
        Validate.notNull(component, "The component must not be null"); //$NON-NLS-1$
        HtmlHierarchyContainer result = null;
        try {
            result = (HtmlHierarchyContainer) getHierarchyMap().get(component);
        } catch (ClassCastException cce) {
            LOG.error(cce.getLocalizedMessage(), cce);
        } catch (NullPointerException npe) {
            LOG.error(npe.getLocalizedMessage(), npe);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IComponentIdentifier[] getAllComponentId() {
        // no need to implement for HTML
        return new ComponentIdentifier[1];
    }

    /**
     * @return the robot
     */
    protected RobotWebImpl getRobot() {
        return WebAUTServer.getInstance().getWebRobot();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<String> getComponentContext(Object component) {
        List<String> context = new ArrayList<String>();
        if (component instanceof AbstractWebComponent) {
            getComponentContext((AbstractWebComponent) component, context);
        }
        return context;
    }
}
