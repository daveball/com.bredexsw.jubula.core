/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.components.json;

/**
 * Class to handle constants which are implied by serialization from
 * objectMapping.js
 * 
 * @author BREDEX GmbH
 * @created 05.01.2010
 *
 */
@SuppressWarnings("nls")
public abstract class JSONConstantsObjectMapping {
    /**
     * <code>TECH_COMP_X_PATH_KEY</code>
     */
    public static final String TECH_COMP_X_PATH_KEY = "techCompXPath";

    /**
     * hide
     */
    private JSONConstantsObjectMapping() {
    // hide
    }
}
