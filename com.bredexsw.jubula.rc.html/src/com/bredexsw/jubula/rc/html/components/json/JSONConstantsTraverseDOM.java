/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.components.json;

/**
 * Class to handle constants which are implied by serialization from
 * traverseDOM.js
 * 
 * @author BREDEX GmbH
 * @created 05.01.2010
 *
 */
@SuppressWarnings("nls")
public abstract class JSONConstantsTraverseDOM {
    /**
     * <code>ATTRIBUTES_KEY</code>
     */
    public static final String ATTRIBUTES_KEY = "attributes";

    /**
     * <code>TAG_NAME_KEY</code>
     */
    public static final String TAG_NAME_KEY = "nodeName";
    
    /**
     * <code>TYPE_KEY</code>
     */
    public static final String TYPE_KEY = "type";
    
    /**
     * <code>SIZE_KEY</code>
     */
    public static final String SIZE_KEY = "size";
    
    /**
     * <code>ID_KEY</code>
     */
    public static final String ID_KEY = "id";

    /**
     * <code>XPATH_KEY</code>
     */
    public static final String CLASSNAME_KEY = "className";
    
    /**
     * <code>CHILDREN_KEY</code>
     */
    public static final String CHILDREN_KEY = "children";
    
    /**
     * <code>ROLE_KEY</code>
     */
    public static final String ROLE_KEY = "role";
    
    /**
     * hide
     */
    private JSONConstantsTraverseDOM() {
    // hide
    }
}
