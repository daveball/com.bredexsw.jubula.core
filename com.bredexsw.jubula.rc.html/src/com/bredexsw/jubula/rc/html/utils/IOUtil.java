/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;

/**
 * Class contains io utility methods
 * 
 * @author BREDEX GmbH
 * @created 10.11.2009
 *
 */
public final class IOUtil {
    /** hide constructor */
    private IOUtil() {
    // nothing in here
    }

    /**
     * @param resourcePath
     *            the name of the resource to open.
     * @return the contents of the resource as a string
     */
    public static String readResourceAsString(String resourcePath)
        throws InvocationTargetException {
        
        return readResourceAsString(resourcePath, IOUtil.class);
    }

    /**
     * @param resourcePath
     *            the name of the resource to open.
     * @param loaderClass
     *            The class to use for locating the resource.
     * @return the contents of the resource as a string
     */
    public static String readResourceAsString(String resourcePath, 
            Class<?> loaderClass) throws InvocationTargetException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    loaderClass.getResourceAsStream(resourcePath)));
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                fileData.append(buf, 0, numRead);
            }
            reader.close();
        } catch (IOException e) {
            throw new InvocationTargetException(e);
        }
        return fileData.toString();
    }
}
