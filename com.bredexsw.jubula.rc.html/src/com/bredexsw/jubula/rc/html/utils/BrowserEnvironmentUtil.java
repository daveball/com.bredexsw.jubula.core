/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.jubula.rc.html.utils;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.KeyStroke;

import com.bredexsw.jubula.rc.html.web.driver.RobotWebImpl;

/**
 * @author BREDEX GmbH
 * @created Dec 7, 2009
 */
public class BrowserEnvironmentUtil {
    /**
     * <code>BROWSER_VERSION_IS_FIREFOX</code>
     */
    private static final String BROWSER_VERSION_IS_FIREFOX = "return browserVersion.isFirefox"; //$NON-NLS-1$

    /**
     * hide
     */
    private BrowserEnvironmentUtil() {
        // nothing in here
    }

    /**
     * @param robot the web robot
     * @return whether the currently running instance of the browser is a
     *         firefox instance
     */
    public static boolean isFirefox(RobotWebImpl robot) {
        boolean isFirefox = false;
        try {
            String returnValue = robot.js(BROWSER_VERSION_IS_FIREFOX);
            isFirefox = Boolean.valueOf(returnValue);
        } catch (InvocationTargetException e) {
            // nothing in here
        }
        return isFirefox;
    }

    /**
     * @param keyStroke KeyStroke whose modifiers are requested
     * @return a List of KeyCodes (hopefully) realising the ModifierMask
     *         contained in the KeyStroke
     */
    public static List<Integer> modifierKeyCodes(KeyStroke keyStroke) {
        List<Integer> l = new LinkedList<Integer>();
        int modifiers = keyStroke.getModifiers();

        if ((modifiers & InputEvent.ALT_MASK) != 0) {
            l.add(new Integer(KeyEvent.VK_ALT));
        }
        if ((modifiers & InputEvent.ALT_GRAPH_MASK) != 0) {
            l.add(new Integer(KeyEvent.VK_ALT_GRAPH));
        }
        if ((modifiers & InputEvent.CTRL_MASK) != 0) {
            l.add(new Integer(KeyEvent.VK_CONTROL));
        }
        if ((modifiers & InputEvent.SHIFT_MASK) != 0) {
            l.add(new Integer(KeyEvent.VK_SHIFT));
        }
        if ((modifiers & InputEvent.META_MASK) != 0) {
            l.add(new Integer(KeyEvent.VK_META));
        }
        return l;
    }

}
