/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.webautserver.web.implclasses;

import com.bredexsw.jubula.rc.html.web.model.AbstractWebComponent;


/**
 * This class is a dummy graphics component that represents the tested
 * application itself. It is used by implementation classes which perform
 * general, application wide actions without the need of a special graphics
 * component like a button or a textfield. Those actions are key strokes, for
 * example.
 * 
 * @author BREDEX GmbH
 * @created 26.11.2009
 */
public class GraphicApplication extends AbstractWebComponent {
    // empty model class
}