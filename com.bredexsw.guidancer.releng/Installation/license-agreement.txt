SOFTWARE LICENSE AGREEMENT Version 7.0 March 2015

End-user License agreement for BREDEX GmbH Software

For Jubula Standalone Version

IMPORTANT - READ CAREFULLY: 

This End-User License Agreement (EULA) is a legal agreement between you (either an individual or a single entity) and BREDEX GmbH, for the BREDEX GmbH software product identified above. This license is issued only upon the condition that you accept all of the terms contained in the following license agreement. 

By installing this software, you accept the terms of this EULA. Select "accept" if you are willing to be bound by these terms. If you are not willing to accept all these terms, select "do not accept", and the SOFTWARE PRODUCT will not be installed. 


1. DEFINITIONS

"SOFTWARE PRODUCT" means the products identified above in binary form, which includes, but is not restricted to, computer software and upgrades or error corrections, associated media and printed materials, and any "online" or electronic documentation. 

2. SOFTWARE PRODUCT LICENSE

The SOFTWARE PRODUCT is protected by copyright laws and international copyright treaties, as well as other intellectual property laws and treaties. 

The SOFTWARE PRODUCT is licensed, not sold. 


3. GRANT OF LICENSE

This EULA grants you a non-exclusive, non-transferable license. You may install unlimited copies of the SOFTWARE PRODUCT on multiple computers. 

4. WARRANTY AND SUPPORT

Should the SOFTWARE PRODUCT contain undocumented errors, you may report these to BREDEX GmbH via the public error repository for the SOFTWARE PRODUCT. 

Any support is conducted based on Support Packages which may be purchased from BREDEX GmbH. 
Further information regarding Support Packages including prices, duration and invoicing is available on the BREDEX Testing Resources Portal at:
http://testing.bredex.de

A Support Package must be purchased to deal with topics including but not limited to:

A: Installation and configuration support

B: Questions relating to usage of and best practices for the SOFTWARE PRODUCT

C: Support when writing or updating extensions to the SOFTWARE PRODUCT

D: Information and assistance with test creation, test process and environment


5. RESTRICTIONS

A: Limitations on Reverse Engineering, Decompilation and Disassembly - you may not reverse engineer, decompile or disassemble the SOFTWARE PRODUCT. 

B: Rental - you may not rent or lease the SOFTWARE PRODUCT. 

C: Software Transfer - you may not permanently transfer all of your rights under this EULA without prior written consent of BREDEX GmbH. 

D: Termination - without prejudice to other rights, BREDEX GmbH may terminate this EULA if you fail to comply with the terms and conditions set out in this EULA. In such event, you must destroy all copies of the SOFTWARE PRODUCT and all of its component parts. 

E: Design - you acknowledge that the SOFTWARE PRODUCT is not designed or intended for use in the design, construction, operation, or maintenance of any nuclear facility. 

F: Java Technologies -  you may not create, modify or change the behaviour of, or authorize your licensees to create, modify or change the behaviour of, classes, interfaces or subpackages that are in any way identified as "java", "javax", or similar convention as specialized by Oracle Corporation in any naming convention designation. 


All other uses that fall outside of the scope of this EULA must receive the prior written permission of BREDEX GmbH. 

6. COPYRIGHT

All title and copyrights in and to the SOFTWARE PRODUCT (including but not limited to any images, photographs, animations, video, audio, music, text and "applets" incorporated into the SOFTWARE PRODUCT), the accompanying printed materials, and any copies of the SOFTWARE PRODUCT, are owned by BREDEX GmbH and its suppliers. The SOFTWARE PRODUCT is protected by copyright laws and international treaty provisions. Therefore, you must treat the SOFTWARE PRODUCT like any other copyrighted material except that you may install the SOFTWARE PRODUCT on a computer. You may not copy the printed materials accompanying the SOFTWARE PRODUCT. Copyrights and trademarks may not be used on online auction sites without the express prior written permission of BREDEX GmbH. 

Java is a registered trademark of the Oracle Corporation. Portions of this SOFTWARE PRODUCT may use software owned by Oracle Corporation. 

Other product and brand names may be trademarks of their respective owners. 

7. NO WARRANTY

EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, THE PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Each Recipient is solely responsible for determining the appropriateness of using and distributing the Program and assumes all risks associated with its exercise of rights under this Agreement , including but not limited to the risks and costs of program errors, compliance with applicable laws, damage to or loss of data, programs or equipment, and unavailability or interruption of operations.

8. DISCLAIMER OF WARRANTY

To the maximum extent permitted by applicable law, BREDEX GmbH and its suppliers disclaim all warranties, either express or implied, including, but not limited to, implied warranties of merchantability and fitness for a particular purpose, with regard to the SOFTWARE PRODUCT and any accompanying software. 

9. NO LIABILITY FOR CONSEQUENTIAL DAMAGES

To the maximum extent permitted by applicable law, in no event shall BREDEX GmbH or its suppliers be liable for any special, incidental, indirect or consequential damages whatsoever (including, but not restricted to damages for loss of business profits, business interruption, loss of business information or any other pecuniary loss) arising out of the use or inability to use the SOFTWARE PRODUCT, even if BREDEX GmbH has been advised of the possibility of such damages. Because some states and jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you. 

10. DURATION

The license is issued for an unlimited period.  

11. EXPORT REGULATIONS

All Software and technical data delivered under this agreement are subject to applicable US and German export control laws and may be subject to import or export regulations in other countries. This SOFTWARE PRODUCT may not be downloaded, exported or otherwise made available to citizens, national residents, or those under the control of, the government of Cuba, Iran, Sudan, Libya, North Korea, Syria nor any other country to which the United States has prohibited export. This SOFTWARE PRODUCT may not be downloaded by, re-exported to or made available to persons listed on the 
United States Department of Treasury lists of Specially Designated Nationals, Specially Designated Terrorists, and Specially Designated Narcotic Traffickers, nor on the United States Department of Commerce Table of Denial Orders. 

This SOFTWARE PRODUCT may not be used for, and may not be allowed to be used for any purposes prohibited by United States law, including, without limitation, for the development, design, manufacture or production of  nuclear, chemical, or biological weapons of mass destruction. 

12. INTEGRATION

This EULA sets forth the entire agreement and understanding between the parties as to the subject matter of this agreement and supersedes all prior discussions, representations and amendments of understandings of every kind and nature between them. 

13. AMENDMENTS

Except as otherwise provided in this EULA, this agreement may not be amended, altered or any of its provisions waived on behalf of either party, except in writing executed by both parties' duly authorized agent. 

14. SURVIVAL

The restrictions and obligations as contained in this EULA that you have voluntarily agreed to shall survive any expiration, termination or cancellation of this EULA, and shall continue and remain in effect for one year, said period commencing on the day and year first written above to bind you, your employees, agents, successors, heirs and assigns. 

15. BENEFIT

This EULA shall be binding upon and inure to the benefit of BREDEX GmbH and its successors and permitted assigns. 

16. COUNTERPARTS

This EULA may be executed in one or more counterparts, each of which shall be enforceable against the party actually executing the counterparts, and all of which together shall constitute one instrument. 

17. CAPTIONS

Captions contained in this EULA are inserted for reference and  in no way define, limit, extend or describe the scope of the EULA or intent of any provision in the EULA. 

18. SEVERABILITY

If any provision of this EULA becomes or is declared by a court of competent justice to be illegal, unenforceable or void, this EULA shall continue in full force and effect without said provision, provided, however, that no such severability changes the economic benefit of the EULA to the other party. 

19. AUTHORITY TO EXECUTE

You warrant and represent to BREDEX GmbH that this EULA shall be binding upon you once executed, and that the individual executing this document is authorized or has been empowered to do so. 

20. THIRD PARTY CODE

Additional copyright notices and license terms applicable to portions of the SOFTWARE PRODUCT are set forth in the licenses for Oracle. In addition to any terms and conditions of any third party open source/freeware license identified in these licenses, the disclaimer of warranty and limitation of liability provisions in paragraphs eight (8), nine (9) and ten (10) shall apply to all software in this distribution. 

Parts of this SOFTWARE PRODUCT use software which is licensed under the MIT license, the Apache 2.0 license and the Eclipse Public License Version 1.0. Refer to the respective sites for information on these licenses. 

21. CHANGES

BREDEX GmbH reserves the right to change specifications without notice. Use of this SOFTWARE PRODUCT is subject to the acceptance of this EULA upon installation of the SOFTWARE PRODUCT.

Should you have any questions concerning this EULA, or if you wish to contact BREDEX GmbH for any reason, please contact:

BREDEX GmbH
Mauernstrasse 33
38100
Braunschweig
Germany



