= com.bredexsw.ite.releng =

== build.xml ==

== Generate Installer from local source == 

Invoke target "releaseITEsFromCheckedOutSrc" from build.xml.

General behavior:
 * remotely pulls Jubula from the jubula-nightly p2-Repository @ [http://download.eclipse.org/jubula/development]
  * pulls the rc.source directly from the latest successful build artifact from [http://hudson.eclipse.org/jubula-nightly]  
 * this pulls different artifacts from the latest hudson build of the following jobs
  * iOS native library build
  * Win native library build (this also get's wrapped into the rc-bundle):
   * com.bredexsw.guidancer.rc.win.nativ

== Building a maintenance release / installer == 

=== Pre-requisites ===

 * make sure that the following repositories are on the branch to build the maintenance installer for
  * jubula
  * jubula_lab
  * guidancer
 * make sure all local repositories are pulled and up-to-date
 * make sure you know exactly which components should be re-build for the maintenance installer
 * invoke a delReleaseDir, clean and make sure no cached / already pre-built artifacts are present
 
=== General steps to take ===
 * manually increase at least micro.version number on branch
  * modify version.properties 
  * commit version / qualifier increase
 * prepare "releaseITEsInstaller" to use / generate locally build artifacts
  * switch to local build artifact generation for Jubula dependencies
   * ... -P jubula-remote-fetch --> -P jubula-local-build
  * make sure remotely pulled artifacts are branch compatible
   * either adjust 
     * remote <get src="<URL>"/> URLs to local ones (e.g. from the latest released version)
      * recommended for the Win native library as this is included in a wrapper bundle (which makes post build modifications difficult)
     * re-run the jobs to generate artifacts for the correct branch (check for concurrency in job executions)
      * recommended if these artifacts require a re-generation due to maintenance issues
     * pause the installer generation before running install4j and manually replace those incorrectly pulled artifacts locally in the "<workspaceDir>/release" directory
      * recommended for artifacts located under "<workspaceDir>/release/development" e.g. 
       * the iOS native library aka. iOS-support.zip
       * eclipse remote fetched features aka. org.elipse.jubula.site.zip
 * disable the Jubula online help feature integration
 * generate the Installer from local source
 * check
  * version numbers
  * documentation content
  * pulled artifact inclusion

=== Known issues / pitfalls ===

 * Orbit p2-repository is no longer existing
  * check for a new one
  * check for an archived one
   * [http://download.eclipse.org/tools/orbit/downloads/<someRepo>]
   * --> 
   * [http://archive.eclipse.org/tools/orbit/downloads/<someRepo>]
 * not all documentation generation relevant documents are under source control
  * talk to AS / AL / MT
 * old tycho issues may be present
 * install4j JRE no longer present
  * talk to TK

== Installation/ite.install4j ==

=== Update JRE ===
 * Create the needed JREs for each OS (Windows/Linux with 32- and 64-bit):
  1. Start Install4J with {{{install4j5}}}
  1. Project -> "Create a JRE Bundle"
  1. Select a folder containing an already installed version of JRE (e.g. {{{/devapps/java/jdk1.7.0_17/jre}}}). This folder must contain a sub folder named {{{server/}}}, because only the server- and not the Client-JRE can be used for GUIdancer.
  1. Copy the created file into the folder {{{/devapps/install4j5.1.3/jres/}}} (or delegate it to administrator, if you do not have access rights).
 * Rename the old to the new JRE file names used in the {{{ite.install4j}}} file.
=== Test new JRE ===
 * Build the new installers locally with {{{ant releaseITEsFromCheckedOutSrc}}}
 * Test the Windows installer created in {{{release/installer/}}} before pushing the changes:
  * Install new GUIdancer on gandalf in incoming folder
  * Use VM gdwin7 with user {{{gduser}}} and start new autagent ({{{K:\Incoming\...\server\autagent.exe}}})
  * Use test {{{caa_swing_fulltest_win}}} in project {{{caa_swing}}} from database {{{dev_testweek}}}

== Increasing version numbers ==
 * as we DO NOT USE SEMANTIC versioning for our bundles / fragments and features we simply increase all version numbers of all artifacts in a consistent way
 * artifacts to increase the version number for are
  * all bundles / fragments in the MANIFEST.MF (the version of the artifact as well as the bundle dependencies version ranges)
  * all feature in their feature.xml and their dependencies
  * all pom.xml files have to be adjusted accordingly
  * all category.xml files (the p2-repository description files)
  * all products in their *.product file and their feature dependencies
  * all "Help --> About" information
   * GUIdancer: is automatically retrieved by a system property in com.bredexsw.guidancer.app which get's set to that application bundle version
   * Jubula: jubula_lab/com.bredexsw.jubula.app/plugin.properties
  * the documentation / installer version number located in "guidancer/Configurations/version.properties"
  * the wget URL in the "get.rc.sdk" target in "guidancer/Configurations/build.xml"
  * DON'T forget 
   * the dashboard and its specific bundles
   * all bundles that still target 3.x specific API
  