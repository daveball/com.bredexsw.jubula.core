/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.communication.commands;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jubula.communication.ICommand;
import org.eclipse.jubula.communication.message.FindMatchesMessage;
import org.eclipse.jubula.communication.message.FindMatchesResponseMessage;
import org.eclipse.jubula.communication.message.Message;
import org.eclipse.jubula.tools.serialisation.SerializedImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.ir.core.Finders;
import com.bredexsw.jubula.ir.core.ImageMatch;
import com.bredexsw.jubula.ir.core.SearchOptions;
import com.bredexsw.jubula.ir.core.exceptions.ImageTimeoutException;
import com.bredexsw.jubula.ir.core.exceptions.IncompatibleImageException;
import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;

/**
 * Command for finding an image within another image
 * @author BREDEX GmbH
 */
public class FindMatchesCommand implements ICommand {
    /** Logger */
    private static final Logger LOG = LoggerFactory
            .getLogger(FindMatchesCommand.class);

    /** message for requesting to find matches*/
    private FindMatchesMessage m_message;
    
    /** Default constructor */
    public FindMatchesCommand() {
        //empty
    }

    /** {@inheritDoc} */
    public Message execute() {
        FindMatchesResponseMessage response = 
                new FindMatchesResponseMessage();
        List<Rectangle> foundPlaces = new ArrayList<Rectangle>();
        IRegionFinder finder = Finders.getFinder("opencvFinder"); //$NON-NLS-1$
        
        BufferedImage template = SerializedImage.
                computeImage(m_message.getTemplate());
        BufferedImage searchImage = SerializedImage.
                computeImage(m_message.getAutScreenshot());
        int timeout = m_message.getTimeout();
        try {
            List<ImageMatch> matches = finder.findRegions(
                    template, searchImage, new SearchOptions(false, false, 0, timeout));
            int indexOfHighestSim;
            while (!matches.isEmpty()) {
                indexOfHighestSim = findHighestSim(matches);
                foundPlaces.add(matches.get(indexOfHighestSim).getRect());
                matches.remove(indexOfHighestSim);
            }
        } catch (IncompatibleImageException iie) {
            response.setException(iie.getMessage());
        } catch (ImageTimeoutException ite) {
            response.setException(ite.getMessage());
        }
        
        response.setMatches(foundPlaces);
        return response;
    }

    /**
     * method to find the index of the element with the highest
     * similarity value in a list of image matches
     * @param matches list of image matches to go through
     * @return highest similarity value in list
     */
    private int findHighestSim(List<ImageMatch> matches) {
        int bestIndex = 0;
        for (int i = 1; i < matches.size(); i++) {
            if (matches.get(i).getSim() > matches.get(bestIndex).getSim()) {
                bestIndex = i;
            }
        }
        return bestIndex;
    }

    /**
     * {@inheritDoc}
     */
    public void timeout() {
        LOG.error(this.getClass().getName() + ".timeout() called"); //$NON-NLS-1$
    }

    /**
     * sets the FindMatchesMessage
     * @param message the message 
     */
    public void setMessage(Message message) {
        m_message = (FindMatchesMessage)message;
    }

    /**
     * returns the FindMatchesMessage
     * @return the message
     */
    public Message getMessage() {
        return m_message;
    }

}
