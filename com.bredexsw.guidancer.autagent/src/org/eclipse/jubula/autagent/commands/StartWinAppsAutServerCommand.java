/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.jubula.autagent.commands;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jubula.autagent.AutStarter;
import org.eclipse.jubula.tools.internal.constants.AutConfigConstants;
import org.eclipse.jubula.tools.internal.constants.CommandConstants;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.Constants;

/**
 * @author BREDEX GmbH
 */
public class StartWinAppsAutServerCommand extends AbstractStartPseudoJavaAUT {
    /** the logger */
    private static final Logger LOG = LoggerFactory
            .getLogger(StartWinAppsAutServerCommand.class);

    /**
     * the .exe name from the win server
     */
    private static final String WIN_SERVER_NAME = 
            "/com.bredexsw.guidancer.rc.win.apps.nativ.impl.exe";

    @Override
    protected String[] createCmdArray(String baseCmd, Map parameters) {
        Vector<String> commands = new Vector<String>();
        commands.add(baseCmd);

        addDebugParams(commands, false);

        StringBuilder serverClasspath = new StringBuilder();
        String[] bundlesToAddToClasspath = getBundlesForClasspath();

        for (String bundleId : bundlesToAddToClasspath) {
            try {
                serverClasspath.append(AbstractStartToolkitAut
                        .getClasspathForBundleId(bundleId));
                serverClasspath.append(PATH_SEPARATOR);
            } catch (IndexOutOfBoundsException e) {
                // do nothing
            }

        }

        commands.add("-classpath"); //$NON-NLS-1$
        commands.add(serverClasspath.toString());

        commands.add("com.bredexsw.guidancer.rc.win.WinAUTServer"); //$NON-NLS-1$

        // communication port
        commands.add(String.valueOf(AutStarter.getInstance()
                .getAutCommunicator().getLocalPort()));

        // Win server execute file path
        commands.add(getWinServerPath());

        // registration parameters
        commands.add(String.valueOf(parameters
                .get(AutConfigConstants.AUT_AGENT_HOST)));
        commands.add(String.valueOf(parameters
                .get(AutConfigConstants.AUT_AGENT_PORT)));
        commands.add(String.valueOf(parameters.get(AutConfigConstants.AUT_ID)));

        // AUT type (0=Normal Application, 1=Modern UI App)
        String paramAutType = (String) parameters
                .get(AutConfigConstants.AUT_TYPE);
        if (paramAutType == null) {
            paramAutType = "1"; // $NON-NLS-1$
        }
        commands.add(paramAutType);
       
        // Modern UI App name
        commands.add(String.valueOf(parameters
                .get(AutConfigConstants.APP_NAME)));
    
        // aut arguments
        commands.add(createAutArguments(parameters));

        return commands.toArray(new String[commands.size()]);
    }

    /**
     * @return the bundles to add to the classpath
     */
    protected String[] getBundlesForClasspath() {
        return new String[] { CommandConstants.RC_WIN_BUNDLE_ID,
            CommandConstants.RC_WIN_NATIVE_BUNDLE_ID, 
            Constants.BUNDLE_ID,
            CommandConstants.TOOLS_BUNDLE_ID,
            CommandConstants.COMMUNICATION_BUNDLE_ID,
            CommandConstants.RC_COMMON_BUNDLE_ID,
            CommandConstants.TOOLKIT_API_BUNDLE_ID,
            CommandConstants.SLF4J_JCL_BUNDLE_ID,
            CommandConstants.SLF4J_API_BUNDLE_ID,
            CommandConstants.LOGBACK_CLASSIC_BUNDLE_ID,
            CommandConstants.LOGBACK_CORE_BUNDLE_ID,
            CommandConstants.LOGBACK_SLF4J_BUNDLE_ID,
            CommandConstants.COMMONS_LANG_BUNDLE_ID,
            CommandConstants.APACHE_ORO_BUNDLE_ID,
            CommandConstants.COMMONS_COLLECTIONS_BUNDLE_ID };
    }

    /**
     * @param parameters
     *            The parameters for starting the AUT.
     * @return The arguments for the AUT that were found in the given
     *         parameters.
     */
    private String createAutArguments(Map parameters) {

        String autArguments = (String) parameters
                .get(AutConfigConstants.AUT_ARGUMENTS);

        if (autArguments == null) {
            autArguments = StringConstants.EMPTY;
        }

        return autArguments;
    }

    /**
     * Builds the Win server path through the rc.win.native bundle.
     * 
     * @return the path for the Win server
     */
    private String getWinServerPath() {
        // -- BundleUtils find methode anschaun --

        Bundle bundle = Platform
                .getBundle(CommandConstants.RC_WIN_NATIVE_BUNDLE_ID);
        Path respath = new Path("resources" + WIN_SERVER_NAME);
        URL resurl = Platform.find(bundle, respath);

        try {
            URL resolvedurl = Platform.resolve(resurl);
            File resfile = new File(resolvedurl.getPath());
            return resfile.getAbsolutePath();
        } catch (IOException e) {
            LOG.error("Could not find the Win server path");
        }
        return "";
    }
}
