/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.jubula.autagent.commands;

import java.util.Map;
import java.util.Vector;

import org.eclipse.jubula.autagent.AutStarter;
import org.eclipse.jubula.tools.internal.constants.AutConfigConstants;
import org.eclipse.jubula.tools.internal.constants.CommandConstants;

import com.bredexsw.jubula.rc.common.Constants;

/**
 * @author BREDEX GmbH
 */
public class StartIOSAutServerCommand extends AbstractStartPseudoJavaAUT {
    /** ID of the iOS RC bundle */
    public static final String RC_IOS_BUNDLE_ID = "com.bredexsw.jubula.rc.mobile.ios"; //$NON-NLS-1$
    
    /** ID of the Mobile RC bundle */
    public static final String RC_MOBILE_BUNDLE_ID = "com.bredexsw.jubula.rc.mobile"; //$NON-NLS-1$
    
    @Override
    protected String[] createCmdArray(String baseCmd, 
        Map<String, String> parameters) {
        Vector<String> commands = new Vector<String>();
        commands.add(baseCmd);

        String [] bundlesToAddToClasspath = getBundlesForClasspath();
            
        StringBuilder serverClasspath = new StringBuilder();
        
        addDebugParams(commands, false);
        
        for (String bundleId : bundlesToAddToClasspath) {
            try {
                serverClasspath.append(AbstractStartToolkitAut
                        .getClasspathForBundleId(bundleId));
                serverClasspath.append(PATH_SEPARATOR);
            } catch (IndexOutOfBoundsException e) {
                //do nothing
            }
        }
        
        commands.add("-classpath"); //$NON-NLS-1$
        commands.add(serverClasspath.toString());
        
        commands.add("com.bredexsw.jubula.rc.mobile.ios.IOSAUTServer"); //$NON-NLS-1$
        
        // communication port
        commands.add(String.valueOf(
                AutStarter.getInstance().getAutCommunicator().getLocalPort()));

        // mobile device AUT host name
        commands.add(String.valueOf(
                parameters.get(AutConfigConstants.AUT_HOST)));
        // mobile device AUT port
        commands.add(String.valueOf(
                parameters.get(AutConfigConstants.AUT_HOST_PORT)));
        
        // registration parameters
        commands.add(String.valueOf(
                parameters.get(AutConfigConstants.AUT_AGENT_HOST)));
        commands.add(String.valueOf(
                parameters.get(AutConfigConstants.AUT_AGENT_PORT)));
        commands.add(String.valueOf(
                parameters.get(AutConfigConstants.AUT_ID)));
        
        return commands.toArray(new String[commands.size()]);
    }
    
    /**
     * @return the bundles to add to the classpath
     */
    protected String[] getBundlesForClasspath() {
        return new String[] { RC_IOS_BUNDLE_ID,
                              RC_MOBILE_BUNDLE_ID,
                              Constants.BUNDLE_ID,
                              CommandConstants.TOOLS_BUNDLE_ID,
                              CommandConstants.COMMUNICATION_BUNDLE_ID,
                              CommandConstants.RC_COMMON_BUNDLE_ID,
                              CommandConstants.TOOLKIT_API_BUNDLE_ID,
                              CommandConstants.SLF4J_JCL_BUNDLE_ID,
                              CommandConstants.SLF4J_API_BUNDLE_ID,
                              CommandConstants.LOGBACK_CLASSIC_BUNDLE_ID,
                              CommandConstants.LOGBACK_CORE_BUNDLE_ID,
                              CommandConstants.LOGBACK_SLF4J_BUNDLE_ID,
                              CommandConstants.COMMONS_LANG_BUNDLE_ID,
                              CommandConstants.COMMONS_CODEC_BUNDLE_ID,
                              CommandConstants.APACHE_ORO_BUNDLE_ID,
                              CommandConstants.COMMONS_COLLECTIONS_BUNDLE_ID };
       
    }
}