# jubula-webdriver
A holding ground for work updating Jubula's HTML implementation to latest webdriver (bypassing Selinium-RC) and other web AUT changes to improve Jubula behaviour when testing large Vaadin applications.

The goal is for these changes to be proposed for merging upstream, once they are sufficiently mature and reviewed!