package com.bredexsw.jubula.iot.mqtt.communication;


import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


/**
 * implements communication via MQTT
 * 
 * @author BREDEX GmbH
 * 
 */
public class MQTTCommunicator {
    /** Listens to the MQTT Broker */
    private SubscriptionListener m_subscriptionListener;
    /** MQTT Client for publishing */
    private MqttClient m_client;
    
    private static MQTTCommunicator MQTTCommunicator = new MQTTCommunicator();
    
    /**
     * Constructor
     */
    private MQTTCommunicator() {
        
    }
    
    public static MQTTCommunicator getInstance() {
        return MQTTCommunicator;
    }
    
    /** {@inheritDoc} */
    public String sendCommand(String uri) {
        String[] resolvedUri = resolveUri(uri);
        String host = "tcp://" + resolvedUri[0];
        String topic = resolvedUri[1].replaceFirst("/", "");
        String uuid = UUID.randomUUID().toString();
        String command = resolvedUri[2] + "~" + uuid;
        
        startClient(host, topic);
        
        sendMessage(command, topic, host);
        String response = waitForResponse(uuid);
        m_subscriptionListener.interrupt();
        try {
            m_subscriptionListener.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return response;
       
    }
    
    private void startClient(String host, String inputTopic) {
        m_subscriptionListener = new SubscriptionListener(host, this, inputTopic);
        m_subscriptionListener.start();
    }

    /**
     * Sends an MQTTmessage to the MQTT Broker
     * 
     * @param command
     *            the message which contains an command
     * @param topic
     *            the to publish the message in
     */
    private void sendMessage(String command, String topic, String host) {
        try {
            try {
                m_client = new MqttClient(host, "Pub" + MqttClient.generateClientId());
            } catch (MqttException mqExc) {
                System.out.println("Error creating MQTT Client");
                mqExc.printStackTrace();
            }
            m_client.connect();
            MqttMessage messageMQTT = new MqttMessage();
            messageMQTT.setQos(2);
            messageMQTT.setPayload(command.getBytes());
            m_client.publish(topic, messageMQTT);
            m_client.disconnect();
        } catch (MqttException e1) {
            System.out.println("Error: could not  publish on Broker");
            e1.printStackTrace();
        }
    }

    /**
     * Waits for an response, after an command has been send
     * 
     * @param uuid
     *            the uuid of the message
     * @return the response
     */
    private String waitForResponse(String uuid) {
        String answer = null;
        long before = System.currentTimeMillis();
        while (answer == null ) {
            List<MQTTMessage> receivedMessages = m_subscriptionListener
                    .getReceivedMessages();
            for (int i = 0; i < receivedMessages.size(); i++) {
                if (receivedMessages.get(i).getUuid().equals(uuid)) {
                    answer = receivedMessages.get(i).getMessage();
                    if (answer != null) {
                        receivedMessages.remove(i);
                    }
                }
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(answer);
        return answer;
    }

    private String[] resolveUri(String uri) {
        String[] resolvedUri = new String[3];
        URI uriFromString = null;
        try {
            uriFromString = new URI(uri);
            //todo: exception, if port is empty
            resolvedUri[0] = uriFromString.getAuthority();
            resolvedUri[1] = uriFromString.getPath();
            resolvedUri[2] = uriFromString.getQuery();
        } catch (URISyntaxException uriSyntayException) {
            System.out.println("The uri does not match the expected pattern!");
            System.out.println(uriSyntayException.getMessage());
            uriSyntayException.printStackTrace();
        }
        return resolvedUri;
    }
        
}