package com.bredexsw.jubula.iot.mqtt.communication;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * 
 * @author BREDEX GmbH
 * 
 */
public class SubscriptionListener extends Thread implements MqttCallback {
    
    private static final boolean debug = true;
    
    /** MQTT Broker URL */
    private String m_url;
    /** Topic for openHAB output */
    private String m_topic = "#";
    /** Instance of the Communicator which starts this thread */
    private MQTTCommunicator m_communicator;
    /** List of received MQTT Messages (cleaned after every 10th message) */
    private List<MQTTMessage> m_receivedMessages;
    /** Numeber of received messages */
    private int m_numberReceivedMessages;
    private String m_inputTopic;
    /**
     * Constructor
     * 
     * @param url
     *            MQTT Broker URL
     * @param model
     *            Instance of the communicator which starts this thread
     */
    public SubscriptionListener(String url, MQTTCommunicator model, String inputTopic) {
        m_inputTopic = inputTopic;
        m_url = url;
        m_communicator = model;
        m_receivedMessages = new ArrayList<MQTTMessage>();
        m_numberReceivedMessages = 0;
    }

    /**
     * Called if connection is Lost
     * 
     * 
     * @param cause
     *            the cause
     */
    public void connectionLost(Throwable cause) {
    }

    /**
     * Called after message delivery is complete
     * 
     * @param token
     *            the token
     */
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    /**
     * Called when message arrives the Broker
     * 
     * @param topic
     *            the topic of the arrived message
     * @param message
     *            the MQTT message
     * 
     */
    public void messageArrived(String topic, MqttMessage message)
        throws Exception {
        String messageContent = new String(message.getPayload());
        String topicName = topic.toString();
        
        if(debug) {
            String time = new Timestamp(System.currentTimeMillis()).toString();
            System.out.println("Time:\t" + time + "\tItem:\t" + topic + "\t\t"
                    + messageContent);    
        }
        
        if (!topic.equals(m_inputTopic)) {
     
            m_receivedMessages.add(new MQTTMessage(topicName, messageContent));
            m_numberReceivedMessages++;
            if (m_numberReceivedMessages > 9) {
                cleanReceivedMessages();
                m_numberReceivedMessages = 0;
           }
        }

    }

    /**
     * Removes all messages from m_receivedMessages which are older than 5
     * minutes
     */
    private void cleanReceivedMessages() {
        for (int i = 0; i < m_receivedMessages.size(); i++) {
            long now = System.currentTimeMillis();
            if (m_receivedMessages.get(i).getTimestamp() + 300000 < now) {
                m_receivedMessages.remove(i);
            }
        }
    }

    /**
     * Getter for receivedMessages
     * 
     * @return list of received messages
     */
    public List<MQTTMessage> getReceivedMessages() {
        return m_receivedMessages;
    }

    /**
     * 
     * @see java.lang.Thread#run()
     */
    public void run() {
        synchronized(m_communicator) {
            MqttClient client;
            try {
                client = new MqttClient(m_url, "Sub" + MqttClient.generateClientId());
                client.setCallback(this);
                client.connect();
                System.out.println("Connected");
                client.subscribe(m_topic, 2);
                System.out.println("Subscribed");
                try {
                    while (!Thread.currentThread().isInterrupted()) {
                            Thread.sleep(100);   
                    }
                } catch (InterruptedException ie) {
                    client.disconnect();
                    System.out.println(client.isConnected() + " Interrupted");
                    Thread.currentThread().interrupt();
                }
            } catch (MqttException e) {
                System.out.println(e.getMessage());
                System.out.println("ERROR: Could not subscribe to topic!");
            }  
        }
        
    }
}