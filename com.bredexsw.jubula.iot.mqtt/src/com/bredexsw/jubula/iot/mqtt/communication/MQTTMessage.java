package com.bredexsw.jubula.iot.mqtt.communication;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class MQTTMessage { 
    /** the topic the message was published in*/
    private String m_topic;
    /** the content of the message */
    private String m_message;
    /** the uuid of the message */
    private String m_uuid;
    /** system time when message was recieved*/
    private long m_timestamp;
    /**
     * Constructor
     * 
     * @param topic the topic
     * @param message the message;
     */
    public MQTTMessage(String topic, String message) {
        m_timestamp = System.currentTimeMillis();
        m_topic = topic;
        m_message = message.split("~")[0];
        m_uuid = message.split("~")[1];
    }
    /**
     * Getter for the topic
     * @return the topic
     */
    public String getTopic() {
        return m_topic;
    }
    /**
     * Getter for the message
     * @return the message
     */
    public String getMessage() {
        return m_message;
    }
    
    /**
     * Getter for the uuid
     * @return the uuid
     */
    public String getUuid() {
        return m_uuid;
    }
    /**
     * Getter for the timestamp
     * @return the timestamp
     */
    public long getTimestamp() {
        return m_timestamp;
    }
}
