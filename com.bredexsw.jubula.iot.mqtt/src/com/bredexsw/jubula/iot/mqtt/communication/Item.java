package com.bredexsw.jubula.iot.mqtt.communication;
/**
 * Provides a data structure to save items with their attributes
 *
 * @author felixki
 *
 */
public class Item {
    /** the state of the item */
    private String m_state;
    /** the topic of the item */
    private String m_topic;
    
    /**
     * Constructs an Item
     * @param topic the topic of the item
     * @param state the state of the item
     */
    public Item(String topic, String state) {
        m_topic = topic;
        m_state = state;
    }
    
    /**
     * gets the state of the item
     * @return the state of the item
     */
    public String getState() {
        return m_state;
    }
    
    /**
     * gets the topic of the item
     * @return the topic of the item
     */
    public String getTopic() {
        return m_topic;
    }
    
    /**
     * sets the state of the item
     * @param state the state of the item
     */
    public void setState(String state) {
        m_state = state;
    }
    
    /**
     * sets the topic of the item
     * @param topic the topic of the item
     */
    public void setTopic(String topic) {
        m_topic = topic;
    }
}
