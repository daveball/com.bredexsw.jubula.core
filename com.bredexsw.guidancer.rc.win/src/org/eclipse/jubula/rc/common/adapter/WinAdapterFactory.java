/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package org.eclipse.jubula.rc.common.adapter;

import org.eclipse.jubula.rc.common.adaptable.IAdapterFactory;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.AbstractButton;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBox;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Document;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Edit;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Menu;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Text;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Tree;
import com.bredexsw.guidancer.rc.win.tester.adapter.AbstractButtonAdapter;
import com.bredexsw.guidancer.rc.win.tester.adapter.ComboBoxAdapter;
import com.bredexsw.guidancer.rc.win.tester.adapter.MenuAdapter;
import com.bredexsw.guidancer.rc.win.tester.adapter.TextAdapter;
import com.bredexsw.guidancer.rc.win.tester.adapter.TextInputAdapter;
import com.bredexsw.guidancer.rc.win.tester.adapter.TreeAdapter;
/**
 * 
 * @author BREDEX GmbH
 *
 */

@SuppressWarnings({"rawtypes", "unchecked" })
public class WinAdapterFactory implements IAdapterFactory {
    /**
     * the supported classes
     */
    private static final Class[] SUPPORTEDCLASSES = new Class[] { Menu.class,
        AbstractButton.class, Text.class, Document.class, Edit.class,
        ComboBox.class , Tree.class};
 
    /**
     * {@inheritDoc}
     */
    public Class[] getSupportedClasses() {
        return SUPPORTEDCLASSES;
    }

    /**
     * {@inheritDoc}
     */
    public Object getAdapter(Class targetAdapterClass, Object objectToAdapt) {
        if (targetAdapterClass.isAssignableFrom(IComponent.class)) {
            if (objectToAdapt instanceof Menu) {
                return new MenuAdapter(objectToAdapt);
            }
            if (objectToAdapt instanceof AbstractButton) {
                return new AbstractButtonAdapter(objectToAdapt);
            }
            if (objectToAdapt instanceof Text) {
                return new TextAdapter(objectToAdapt);
            }
            if (objectToAdapt instanceof Document) {
                return new TextInputAdapter(objectToAdapt);
            }
            if (objectToAdapt instanceof Edit) {
                return new TextInputAdapter(objectToAdapt);
            }
            if (objectToAdapt instanceof ComboBox) {
                return new ComboBoxAdapter(objectToAdapt);
            }
            if (objectToAdapt instanceof Tree) {
                return new TreeAdapter(objectToAdapt);
            }
            
        }
        return null;
    }

}
