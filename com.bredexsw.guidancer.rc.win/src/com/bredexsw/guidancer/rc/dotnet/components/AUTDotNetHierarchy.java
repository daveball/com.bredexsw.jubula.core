/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.WordUtils;
import org.eclipse.jubula.rc.common.Constants;
import org.eclipse.jubula.rc.common.components.AUTHierarchy;
import org.eclipse.jubula.tools.internal.objects.ComponentIdentifier;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.win.listener.ComponentHandler;
/**
 *
 * AUTHierarchy class for the java-server part
 *
 * @author patrick
 *
 */
public class AUTDotNetHierarchy extends AUTHierarchy {

    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory.getLogger(
            AUTDotNetHierarchy.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public IComponentIdentifier[] getAllComponentId() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getComponentContext(Object component) {
        List<String> context = new ArrayList<String>();
        if (component instanceof AbstractDotNetComponent) {
            return getComponentContext((AbstractDotNetComponent)component,
                   context);
        } else if (component instanceof String) {
            return getComponentContext(getComponent(component.toString()),
                    context);
        }
        return context;
    }

    /**
     * Gets the context of the component
     *
     * @param component the component
     * @param context the context list
     * @return the context as a list
     */
    private List<String> getComponentContext(AbstractDotNetComponent component,
            List<String> context) {
        DotNetHierarchyContainer hc = getHierarchyContainer(component);
        DotNetHierarchyContainer parent =
                (DotNetHierarchyContainer)hc.getPrnt();

        if (parent != null) {
            DotNetHierarchyContainer[] comps = parent.getComponents();
            for (int i = 0; i < comps.length; i++) {
                AbstractDotNetComponent child = (AbstractDotNetComponent)
                    comps[i].getCompID();
                if (!child.equals(component)) {
                    String toAdd = child.getClass().getName()
                        + Constants.CLASS_NUMBER_SEPERATOR + 1;
                    while (context.contains(toAdd)) {
                        int lastCount = Integer.valueOf(
                            toAdd.substring(toAdd.lastIndexOf(
                                    Constants.CLASS_NUMBER_SEPERATOR) + 1)).
                                intValue();
                        toAdd = child.getClass().getName()
                            + Constants.CLASS_NUMBER_SEPERATOR
                            + (lastCount + 1);
                    }
                    context.add(toAdd);
                }
            }
        }
        return context;
    }

    /**
     * Returns the hierarchy container for <code>component</code>.
     * @param component the component from the AUT, must no be null
     * @throws IllegalArgumentException if component is null
     * @return the hierarchy container or null if the component is not yet managed
     */
    private DotNetHierarchyContainer getHierarchyContainer(
            AbstractDotNetComponent component)
        throws IllegalArgumentException {
        Validate.notNull(component, "The component must not be null"); //$NON-NLS-1$
        DotNetHierarchyContainer result = null;
        try {
            result = (DotNetHierarchyContainer)this.getHierarchyMap()
                    .get(component);
        } catch (ClassCastException cce) {
            log.error(cce.getLocalizedMessage(), cce);
        } catch (NullPointerException npe) {
            log.error(npe.getLocalizedMessage(), npe);
        }
        return result;
    }


    /**
     * Adds the Win hierarchy from his JSON string form to
     * HierarchyContainer and adds them to the AUTHierarchy
     *
     * @param jsonHierarchy the hierarchy as a JSON string list
     */
    public void add(List<String> jsonHierarchy) {
        ArrayList<AbstractDotNetComponent> hierarchyComponents = new
                ArrayList<AbstractDotNetComponent>();

        for (String s : jsonHierarchy) {
            JSONObject jsonString = (JSONObject) JSONSerializer.
                    toJSON(s);

            String name = jsonString.getString("m_name");
            String locator = jsonString.getString("m_locatorPath");
            String controltype = jsonString.getString("m_control");
            String frameworkID = jsonString.getString("m_frameworkID");

            AbstractDotNetComponent component = createConcreteComponent(locator,
                    name, controltype, frameworkID);
            hierarchyComponents.add(component);
        }

        DotNetHierarchyContainer rootContainer =
                new DotNetHierarchyContainer(
                        hierarchyComponents.get(0));
        hierarchyComponents.remove(0);

        for (AbstractDotNetComponent dnc : hierarchyComponents) {
            if (dnc.getParentLocator().equals(
                    rootContainer.getComponent().getLocator())) {
                DotNetHierarchyContainer childContainer =
                       new DotNetHierarchyContainer(dnc, rootContainer);
                rootContainer.add(childContainer);
                super.addToHierachyMap(childContainer);
            } else {
                DotNetHierarchyContainer parentContainer =
                        findContainer(dnc.getPrntIdxPath(), rootContainer);

                DotNetHierarchyContainer childContainer =
                        new DotNetHierarchyContainer(dnc, parentContainer);

                parentContainer.add(childContainer);
                super.addToHierachyMap(childContainer);
            }
        }

        super.addToHierachyMap(rootContainer);

    }

    /**
     * Creates a component.
     *
     * @param locator the locator
     * @param name the name
     * @param controltype the controltype
     * @param frameworkID the framework id of the component
     *
     *
     * @return the created component
     */
    private AbstractDotNetComponent createConcreteComponent(String locator,
            String name, String controltype, String frameworkID) {

        String componentClassName =
                "com.bredexsw.guidancer.rc.dotnet.components.controltype."
                        + StringUtils.deleteWhitespace(
                                WordUtils.capitalize(controltype)
                                + StringUtils.deleteWhitespace(frameworkID));
        try {
            Constructor<?> constructor = Class.forName(componentClassName)
                    .getConstructor();
            Object componentObj = constructor.newInstance();
            if (componentObj instanceof AbstractDotNetComponent) {
                AbstractDotNetComponent wc =
                        (AbstractDotNetComponent) componentObj;

                wc.setLocator(locator);
                wc.setName(name);
                wc.setControlTypeName(controltype);

                return wc;
            }
        } catch (ClassNotFoundException e) {
            log.debug(e.getLocalizedMessage(), e);
            return new AbstractDotNetComponent(locator, name,
                    controltype);
        } catch (SecurityException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (NoSuchMethodException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (IllegalArgumentException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (InstantiationException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (IllegalAccessException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (InvocationTargetException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    /**
     * Finds the component to the locator
     *
     * @param locatorPath the locator
     * @return the component
     */
    public AbstractDotNetComponent getComponent(String locatorPath) {
        Object o = getRealMap().get(locatorPath);
        return (AbstractDotNetComponent)o;
    }


    /**
     * Finds a container for the given index path
     * @param idxPath the index path
     * @param rootContainer the rootContainer
     * @return the win hierarchy container
     */
    private DotNetHierarchyContainer findContainer(int[] idxPath,
            DotNetHierarchyContainer rootContainer) {

        return rootContainer.getIndexComponent(idxPath, 0);
    }

    /** clean the complete hierarchy */
    public void clean () {
        getRealMap().clear();
        getHierarchyMap().clear();
    }

    /**
     * @param fqPath
     *            the fully qualified PATH of the web component to get an identifier for
     * @return a component identifier for the given web component; may return
     *         null if no component identifier could be created
     */
    public IComponentIdentifier getComponentIdentifier(String fqPath) {
        AbstractDotNetComponent comp = ComponentHandler.findComponent(fqPath);
        if (comp != null) {
            IComponentIdentifier compID = new ComponentIdentifier();
            compID.setComponentClassName(comp.getClass().getName());
            compID.setSupportedClassName(compID.getComponentClassName());
            compID.setHierarchyNames(ComponentHandler.getPathToRoot(comp));
            compID.setNeighbours(getComponentContext(comp));
            setAlternativeDisplayName(
                    getHierarchyContainer(comp), comp, compID);

            if (fqPath.equals(new FindDotNetComponentsBP().findComponent(
                    compID, this))) {
                compID.setEqualOriginalFound(true);
            }

            return compID;
        }
        return null;
    }
}
