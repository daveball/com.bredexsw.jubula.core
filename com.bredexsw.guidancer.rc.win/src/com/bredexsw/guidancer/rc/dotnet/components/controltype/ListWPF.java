package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.driver.RobotConstants;

/**
 */
public class ListWPF extends List {

    @Override
    public String getNativeClassName() {
        return ListWPF.class.getSimpleName() + RobotConstants.IMPL_CLASS_STRING;
    }
}
