package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;

/**
 * Abstract button class
 *
 */
public abstract class AbstractButton extends AbstractDotNetComponent {

    /**
     * @return the native component class name
     */
    public abstract String getNativeClassName();
}
