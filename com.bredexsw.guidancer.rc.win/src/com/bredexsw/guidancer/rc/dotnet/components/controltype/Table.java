/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Type class for Table components.
 */
public abstract class Table extends AbstractDotNetComponent {

    /** the text values contained in the table's cells */
    private String [][] m_contents = null;

    /** the text values contained in the table's column headers */
    private String [] m_columnHeaders = null;

    /** the text values contained in the table's row headers */
    private String [] m_rowHeaders = null;

    /**
     *
     * @return the text values contained in the table's column headers, or an
     *         empty array if the table does not have column headers. Never
     *         <code>null</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public String[] getColumnHeaders() throws RemoteServerException {
        if (m_columnHeaders == null) {
            Communicator communicator =
                    WinAUTServer.getWinCommunicator();
            NativeMessage msgObj = new NativeMessage(
                    getNativeClassName(), RobotConstants.GET_TABLE_COL_HEADERS,
                    getLocator());

            m_columnHeaders = communicator.sendMsg(msgObj).getStringArray();
        }

        return m_columnHeaders;
    }

    /**
     *
     * @return the text values contained in the table's row headers, or an
     *         empty array if the table does not have row headers. Never
     *         <code>null</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public String[] getRowHeaders() throws RemoteServerException {
        if (m_rowHeaders == null) {
            Communicator communicator =
                    WinAUTServer.getWinCommunicator();
            NativeMessage msgObj = new NativeMessage(
                    getNativeClassName(),
                    RobotConstants.GET_TABLE_ROW_HEADERS, getLocator());

            m_rowHeaders = communicator.sendMsg(msgObj).getStringArray();
        }

        return m_rowHeaders;
    }

    /**
     *
     * @return the text values contained in the table's content (non-header)
     *         cells, or an empty array if the table does not have any content
     *         cells. Never <code>null</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public String[][] getContents() throws RemoteServerException {
        if (m_contents == null) {
            Communicator communicator =
                    WinAUTServer.getWinCommunicator();
            NativeMessage msgObj = new NativeMessage(
                    getNativeClassName(),
                    RobotConstants.GET_TABLE_CONTENTS, getLocator());

            m_contents = communicator.sendMsg(msgObj).get2DStringArray();
        }

        return m_contents;
    }

    /**
     *
     * @return <code>true</code> if a cell editor in the table represented by
     *         the receiver currently has keyboard focus. Otherwise,
     *         <code>false</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public boolean isCellEditorFocused() throws RemoteServerException {
        Communicator communicator =
                WinAUTServer.getWinCommunicator();
        NativeMessage msgObj = new NativeMessage(
                getNativeClassName(),
                RobotConstants.IS_CELL_EDITOR_FOCUSED, getLocator());

        return communicator.sendMsg(msgObj).getBoolean();
    }

    /**
     * @return the native class name
     */
    public abstract String getNativeClassName();

}
