package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.driver.RobotConstants;

/**
 */
public class TableWPF extends Table {

    @Override
    public String getNativeClassName() {
        return TableWPF.class.getSimpleName()
                + RobotConstants.IMPL_CLASS_STRING;
    }

}
