/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components;

import org.eclipse.jubula.rc.common.components.AUTComponent;


/**
 *
 * @author patrick
 *
 */
public class AbstractDotNetComponent extends AUTComponent {

    /** the component locator string */
    private String m_locator;

    /** the component control type name */
    private String m_controlTypeName;

    /** index locator */
    private int[] m_idxPath = new int[0];

    /**
     * Constructor
     */
    public AbstractDotNetComponent() {
        super(new Object());
    }

    /**
     * Constructor
     *
     * @param locator the locator
     * @param name the name
     * @param type the ControlType name
     */
    public AbstractDotNetComponent(String locator, String name, String type) {
        super(new String(locator));

        this.m_locator = locator;
        this.setName(name);
        this.m_controlTypeName = type;
        getIndexArray(m_locator);
    }

    /**
     * Creates the IdxPath from the locator string
     *
     * @param locator the locator string
     */
    private void getIndexArray(String locator) {
        String[] pathArr = locator.split(
                DotNetHierarchyContainer.PATH_SEPARATOR);
        int[] indexes = new int[pathArr.length];

        for (int i = 0; i < pathArr.length; i++) {
            String tmpPath = pathArr[i];
            int pos1 = tmpPath.lastIndexOf(
                    DotNetHierarchyContainer.INDEX_BEGIN);
            int pos2 = tmpPath.lastIndexOf(DotNetHierarchyContainer.INDEX_END);
            String index = tmpPath.substring(pos1 + 1, pos2);
            indexes[i] = Integer.parseInt(index);
        }

        m_idxPath = indexes;
    }

    /**
     * @return the locator path
     */
    public String getLocator() {
        return m_locator;
    }

    /**
     * @return the ControlType name
     */
    public String getControlTypeName() {
        return m_controlTypeName;
    }

    /**
     * @return the index locator path
     */
    public int[] getIdxPath() {
        return m_idxPath;
    }

    /**
     *
     * @return the parent locator string
     */
    public String getParentLocator() {
        String parLoc = "";
        try {
            parLoc = m_locator.substring(0, m_locator.lastIndexOf("/"));
        } catch (StringIndexOutOfBoundsException e) {
            return parLoc;
        }
        return parLoc;
    }

    /**
     * Return the index path from the parent
     *
     * @return the indexpath from parent
     */
    public int[] getPrntIdxPath() {
        int[] parentIdxPath = new int[m_idxPath.length - 2];
        for (int i = 0; i < parentIdxPath.length; i++) {
            parentIdxPath[i] = m_idxPath[i + 1];
        }

        return parentIdxPath;
    }

    /**
     * Setter for the locator if a single string is only needed
     * and not the full locator path.
     *
     * @param locatorString the locator
     */
    public void setLocatorString(String locatorString) {
        this.m_locator = locatorString;
        this.setComponent(new String(m_locator));
    }

    /**
     * Sets the full JSON string locator
     * @param locator the locator
     */
    public void setLocator(String locator) {
        this.m_locator = locator;
        this.setComponent(new String(m_locator));
        getIndexArray(m_locator);
    }

    /**
     * set the control name
     * @param controlTypeName the control type name
     */
    public void setControlTypeName(String controlTypeName) {
        this.m_controlTypeName = controlTypeName;
    }
}