package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.driver.RobotConstants;

/**
 * Type class for multiline textinputs
 */
public class Document extends AbstractComponentWithTextInput {

    /**
     * @return the native class name
     */
    public String getNativeClassName() {
        return Document.class.getSimpleName()
                + RobotConstants.IMPL_CLASS_STRING;
    }
}
