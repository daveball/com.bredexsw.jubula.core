/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Type class for ComboBox components.
 */
public class ComboBox extends AbstractComponentWithTextInput {

    /** the text values of the combo box's items */
    private String [] m_itemLabels = null;

    /**
     *
     * @return the text values contained in the combo box's items, or an
     *         empty array if the combo box does not have any items. Never
     *         <code>null</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public String[] getItemLabels() throws RemoteServerException {
        if (m_itemLabels == null) {
            Communicator communicator =
                    WinAUTServer.getWinCommunicator();

            NativeMessage msgObj = new NativeMessage(
                    this.getNativeClassName(),
                    RobotConstants.GET_COMBO_ITEM_LABELS, getLocator());

            m_itemLabels = communicator.sendMsg(msgObj).getStringArray();
        }

        return m_itemLabels;
    }

    /**
     *
     * @return the text value contained in the combo box's text field.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public String getText() throws RemoteServerException {
        Communicator communicator =
                WinAUTServer.getWinCommunicator();

        NativeMessage msgObj = new NativeMessage(
                this.getNativeClassName(),
                RobotConstants.ROBOT_METHOD_GET_TEXT, getLocator());

        return communicator.sendMsg(msgObj).getString();
    }

    /**
     *
     * @return <code>true</code> if the Combo Box's text field is editable.
     *         Otherwise, <code>false</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public boolean isEditable() throws RemoteServerException {
        Communicator communicator =
                WinAUTServer.getWinCommunicator();

        NativeMessage msgObj = new NativeMessage(
                this.getNativeClassName(),
                RobotConstants.IS_COMBO_EDITABLE, getLocator());

        return communicator.sendMsg(msgObj).getBoolean();
    }
    /**
     * @return the native class name
     */
    public String getNativeClassName() {
        return ComboBox.class.getSimpleName()
                + RobotConstants.IMPL_CLASS_STRING;
    }
}
