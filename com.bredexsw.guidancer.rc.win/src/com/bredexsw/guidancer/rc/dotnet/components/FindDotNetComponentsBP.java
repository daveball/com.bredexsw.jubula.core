/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components;

import org.eclipse.jubula.rc.common.AUTServerConfiguration;
import org.eclipse.jubula.rc.common.components.FindComponentBP;

/**
 *
 * @author patrick
 *
 */
public class FindDotNetComponentsBP extends FindComponentBP {
    /**
     * {@inheritDoc}
     */
    protected boolean isAvailable(Object currComp) {
        // by default all objects are available due to the fact that the
        // currComp is just the model instance for the real web component
        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected boolean checkTestableClass(AUTServerConfiguration autServerConf,
            String suppClassName, Object currComp) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected boolean isSupportedComponent(Object component) {
        return true;
    }

    @Override
    protected String getCompName(Object currentComponent) {
        return ((AbstractDotNetComponent)currentComponent).getName();
    }

}
