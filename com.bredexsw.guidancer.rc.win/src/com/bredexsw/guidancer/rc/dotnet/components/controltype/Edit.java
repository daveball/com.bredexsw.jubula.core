/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.driver.RobotConstants;

/**
 * Type class for edit components
 */
public class Edit extends AbstractComponentWithTextInput {

    /**
     * @return the native class name
     */
    public String getNativeClassName() {
        return Edit.class.getSimpleName() + RobotConstants.IMPL_CLASS_STRING;
    }
}