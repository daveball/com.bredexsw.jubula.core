/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Type class for Tab components.
 */
public class Tab extends AbstractDotNetComponent {

    /** the text values of the tab's items */
    private String [] m_itemLabels = null;

    /**
     *
     * @return the text values contained in the Tab's tab items, or an
     *         empty array if the tab does not have tab items. Never
     *         <code>null</code>.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the
     *               communication with the Win server.
     */
    public String[] getItemLabels() throws RemoteServerException {
        if (m_itemLabels == null) {
            Communicator communicator =
                    WinAUTServer.getWinCommunicator();
            NativeMessage msgObj = new NativeMessage(getNativeClassName(),
                    RobotConstants.GET_TAB_ITEM_LABELS, getLocator());

            m_itemLabels = communicator.sendMsg(msgObj).getStringArray();
        }

        return m_itemLabels;
    }

    /**
     * @return the native class name
     */
    public String getNativeClassName() {
        return Tab.class.getSimpleName() + RobotConstants.IMPL_CLASS_STRING;
    }
}
