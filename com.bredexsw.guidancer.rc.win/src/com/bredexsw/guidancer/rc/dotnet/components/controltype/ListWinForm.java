package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.driver.RobotConstants;

/**
 */
public class ListWinForm extends List {

    @Override
    public String getNativeClassName() {
        return ListWinForm.class.getSimpleName()
                + RobotConstants.IMPL_CLASS_STRING;
    }
}
