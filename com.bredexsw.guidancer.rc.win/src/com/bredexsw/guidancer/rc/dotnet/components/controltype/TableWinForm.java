package com.bredexsw.guidancer.rc.dotnet.components.controltype;

import com.bredexsw.guidancer.rc.win.driver.RobotConstants;

/**
 */
public class TableWinForm extends Table {

    @Override
    public String getNativeClassName() {
        return TableWinForm.class.getSimpleName()
                + RobotConstants.IMPL_CLASS_STRING;
    }
}
