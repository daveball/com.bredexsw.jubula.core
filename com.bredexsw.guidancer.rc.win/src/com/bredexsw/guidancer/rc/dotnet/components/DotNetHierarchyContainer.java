/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.dotnet.components;

import java.util.ArrayList;

import org.eclipse.jubula.rc.common.components.AUTComponent;
import org.eclipse.jubula.rc.common.components.HierarchyContainer;


/**
 *
 * @author patrick
 *
 */
public class DotNetHierarchyContainer extends HierarchyContainer {

    /**
     * <code>PATH_SEPARATOR</code>
     */
    public static final String PATH_SEPARATOR = "/"; //$NON-NLS-1$

    /**
     * <code>INDEX_BEGIN</code>
     */
    public static final String INDEX_BEGIN = "["; //$NON-NLS-1$

    /**
     * <code>INDEX_END</code>
     */
    public static final String INDEX_END = "]"; //$NON-NLS-1$

    /**
     *
     * @param component the component
     */
    public DotNetHierarchyContainer(AUTComponent component) {
        super(component);
        setName(component.getName(), false);

    }

    /**
     *
     * @param component the component
     * @param parent the parent container
     */
    public DotNetHierarchyContainer(AUTComponent component,
            HierarchyContainer parent) {
        super(component, parent);
        setName(component.getName(), false);
    }

    /**
     *
     * @return the component
     */
    public AbstractDotNetComponent getComponent() {
        return (AbstractDotNetComponent) super.getCompID();
    }

    /**
     * Searches a child container for a given index path
     *
     * @param idxPath the index path
     * @param idx the current index
     * @return the child container
     */
    public DotNetHierarchyContainer getIndexComponent(int[] idxPath, int idx) {
        int i = idx;
        if (i == idxPath.length) {
            return this;
        }

        ArrayList<DotNetHierarchyContainer> containers =
                (ArrayList<DotNetHierarchyContainer>) this.getContainerList();

        return containers.get(idxPath[i]).getIndexComponent(idxPath, ++i);

    }

    /**
     * Adds a component to the container.
     * @param component The component to add.
     */
    @Override
    public void add(HierarchyContainer component) {
        super.add(component);
    }

    /**
     * @return HtmlHierarchyContainer[]
     */
    public DotNetHierarchyContainer[] getComponents() {
        if (super.getComps().length == 0) {
            return new DotNetHierarchyContainer[0];
        }
        HierarchyContainer[] containerArray = super.getComps();
        DotNetHierarchyContainer[] htmlContainerArray =
            new DotNetHierarchyContainer[containerArray.length];
        for (int i = 0; i < containerArray.length; i++) {
            htmlContainerArray[i] =
                (DotNetHierarchyContainer)containerArray[i];
        }
        return htmlContainerArray;
    }

    /**
     * {@inheritDoc}
     */
    public AUTComponent getCompID() {
        return super.getCompID();
    }

}
