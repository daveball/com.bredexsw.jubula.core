/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.rc.common.adaptable.AdapterFactoryRegistry;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.listener.BaseAUTListener;
import org.eclipse.jubula.rc.common.listener.DisabledCheckListener;
import org.eclipse.jubula.rc.common.listener.DisabledRecordListener;
import org.eclipse.jubula.rc.common.registration.IRegisterAut;
import org.eclipse.jubula.tools.internal.constants.AUTServerExitConstants;
import org.eclipse.jubula.tools.internal.exception.JBVersionException;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.NetUtil;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.guidancer.rc.win.listener.ComponentHandler;
import com.bredexsw.guidancer.rc.win.listener.MappingListener;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;
/**
 * 
 * @author patrick
 *
 */
public class WinAUTServer extends AUTServer {

    /** The minimum amount of arguments that are required to startup the win AUT Server. */
    private static final int MIN_ARGS_REQUIRED = 7;

    /** the path of the Win AUT server */
    private static final int ARG_WIN_SERVER_PATH = 1;

    /** <code>log</code>: the logger */
    private static Logger log = LoggerFactory.getLogger(WinAUTServer.class);

    /**
     * <code>instance</code>: the singleton instance
     */
    private static WinAUTServer instance = null;

    /**
     * the port for the socket
     */
    private int m_comPort;

    /**
     * Communication object
     */
    private Communicator m_com;

    /**
     * The WinRobotImpl Object.
     */
    private WinRobotImpl m_robot;

    /**
     * <code>winServerPath</code> the path of the win server.
     */
    private String m_winServerPath;

    /**
     * <code>AUT_ARGS</code> arguments for the win server
     */
    private int m_autType;

    /**
     * <code>AUT_PATH</code> the path of the AUT in the
     * {@link org.eclipse.jubula.autagent.commands.StartWinAutServerCommand} to start the AUT
     */
    private String m_autPath;
    
    /**
     * <code>AUT_WORKDIR</code> the working directory of the aut
     */
    private String m_autWorkDir;

    /**
     * <code>AUT_ARGS</code> arguments for the win server
     */
    private String m_appName;

    /**
     * <code>AUT_ARGS</code> arguments for the win server
     */
    private String m_autArgs;

    /**
     * <code>CONNECTED</code> The status if the Win Server is connected.
     * 
     */
    private final AtomicBoolean m_connected = new AtomicBoolean(false);  
    /**
     * Checks if the aut server & the aut are still active 
     */
    private AUTAliveThread m_aliveThread;
    
    /**
     * Constructor
     */
    protected WinAUTServer() {
        super(new MappingListener(), new DisabledRecordListener(),
                new DisabledCheckListener());
    }

    @Override
    protected MappingListener getMappingListener() {
        return (MappingListener) super.getMappingListener();
    }

    /**
     * @param args
     *            the main arguments
     */
    public static void main(String[] args) {
        validateAndLogMainArgsCount(args, MIN_ARGS_REQUIRED);
        WinAUTServer.getInstance().setArgs(args);
        WinAUTServer.getInstance().start(false);
    }
    
    /**
     * The sequence of the arguments are created by
     * {@link org.eclipse.jubula.autagent.commands.StartWinAutServerCommand}
     * @param args
     *            the arguments to set
     */
    public void setArgs(String[] args) {
        int index = ARG_WIN_SERVER_PATH;
        m_winServerPath = args[index++];
        setAutAgentHost(args[index++]);
        setAutAgentPort(args[index++]);
        setAutID(args[index++]);
        m_autType = Integer.valueOf(args[index++]);
        if (m_autType == 0) { // normal application
            m_autPath = args[index++];
            m_autWorkDir = args[index++];
        } else { // modern UI app
            m_appName = args[index++];
        }
        // AUT arguments
        StringBuffer sb = new StringBuffer();
        if (index < args.length) {
            sb.append(args[index++]);
            while (index < args.length) {
                sb.append(' '); //$NON-NLS-1$
                sb.append(args[index++].trim());
            }
        }
        m_autArgs = sb.toString();
        m_comPort = NetUtil.getFreePort();
    }
    
    /**
     * {@inheritDoc}
     */
    public void start(boolean isRcpAccessible) {
        initServerConnection();
        startWinServer();
        startAUT();
        startAUTchecking();
        
        try {
            IRegisterAut autReg = parseAutReg();
            if (autReg == null) {
                String errorMessage = "Unable to initialize connection to AUT Agent: No connection information provided."; //$NON-NLS-1$
                log.error(errorMessage);
                sendExitReason(errorMessage, 
                        AUTServerExitConstants.EXIT_MISSING_AGENT_INFO);
            } else if (m_connected.get()) {
                try {
                    autReg.register();
                } catch (IOException ioe) {
                    log.error("Exception during AUT registration", ioe); //$NON-NLS-1$
                    System.exit(AUTServerExitConstants.AUT_START_ERROR);
                }
            } else {
                log.error("AUT couldn't be started");
                System.exit(AUTServerExitConstants.AUT_START_ERROR);
            }
        } catch (JBVersionException ve) {
            log.error("Exception in start()", ve); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.EXIT_UNKNOWN_ITE_CLIENT);
        }
        
        
    }

    /**
     * Starts the "isAUTAlive" checking.
     */
    private void startAUTchecking() {
        m_aliveThread = new AUTAliveThread(m_robot);
        m_aliveThread.start();
    }

    @Override
    protected void addToolkitEventListener(BaseAUTListener listener) {
        if (log.isInfoEnabled()) {
            log.info("installing EventListener " //$NON-NLS-1$ 
                    + listener.toString());
        }
        if (listener instanceof MappingListener) {
            getMappingListener().initOMRobot();
        }
    }

    @Override
    protected void removeToolkitEventListener(BaseAUTListener listener) {
        if (listener instanceof MappingListener) {
            getMappingListener().deserializeOMRobot();
        }
    }

    @Override
    protected void startTasks() throws ExceptionInInitializerError,
            InvocationTargetException, NoSuchMethodException {
        
    }

    @Override
    public IRobot getRobot() {
        return m_robot;
    }
    
    /**
     * Initializes the WinRobotImpl Object which create the socket server on
     * our side. 
     */
    private void initServerConnection() {
        m_com = new Communicator(m_comPort);
        m_com.initConnection(m_connected);
        m_robot = new WinRobotImpl(m_com);
    }
    
    /**
     * Starts the Win Server with our socket port as an argument.
     * 
     */
    private void startWinServer() {
        try {
            List<String> commands = new ArrayList<String>();
            // "cmd /c [...]" is needed, if uiAccess=true in manifest file of AUT server
            commands.add("cmd");
            commands.add("/c");
            commands.add(m_winServerPath);
            commands.add(Integer.toString(m_comPort));
            ProcessBuilder procbuilder = new ProcessBuilder(commands);
            Process serverProcess = procbuilder.start();
            new Thread(new InStreamReader(
                    serverProcess.getInputStream())).start();
            
        } catch (IOException e) {
            log.error("Couldn't start the Win server.", e); //$NON-NLS-1$
            System.exit(AUTServerExitConstants.AUT_START_ERROR);
        }
    }
    
    /**
     * Starts the AUT through the Win Server
     */
    @Override
    public void startAUT() {
        //Send the startAUT-Command to the server if he is connected
        while (true) {
            try {
                if (m_connected.get()) {
                    if (m_autType == 0) { // normal application
                        m_robot.startAUT(
                                m_autPath, m_autWorkDir, m_autArgs);
                    } else { // modern UI app
                        m_robot.startAUT(m_appName, m_autArgs);
                    }
                    break;
                }
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.error("InterruptedException during AUT start", e);
            } catch (RemoteServerException e) {
                log.error("RemoteServerException during AUT start", e);
                System.exit(e.getExceptionID());
            }
        }
    }
    
    /**
     * InputStream Reader for the Win process
     * 
     */
    private static final class InStreamReader implements Runnable {

        /** the stream*/
        private InputStream m_stream;
        
        /**
         * Constructor
         * 
         * @param stream The stream
         */
        public InStreamReader(InputStream stream) {
            m_stream = stream;
        }
        
        /**
         * 
         */
        public void run() {
            try {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(m_stream));
                
                while ((br.readLine()) != null) {
                    TimeUtil.delay(10);
                }
            } catch (IOException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
        
    }
    
    /**
     * {@inheritDoc}
     */
    public synchronized void shutdown() {
        super.shutdown();
        m_aliveThread.setKeepRunning(false);
        getMappingListener().deserializeOMRobot();
        try {
            m_robot.stopAUT();
        } catch (RemoteServerException e) {
            System.exit(e.getExceptionID());
        }
        try {
            m_robot.stopServer();
        } catch (RemoteServerException e) {
            System.exit(e.getExceptionID());
        }
    }        
    
    /**
     * This Method is called if we notice that the AUT is not by us closed
     */
    public synchronized void autClosedShutdown() {
        try {
            m_robot.stopServer();
            System.exit(AUTServerExitConstants.EXIT_OK);
        } catch (RemoteServerException e) {
            System.exit(e.getExceptionID());
        }
    }        
    
    
    /**
     * Method to get the single instance of this class.
     * 
     * @return the instance of this Singleton
     */
    public static WinAUTServer getInstance() {
        if (instance == null) {
            instance = new WinAUTServer();
            setInstance(instance);
            AdapterFactoryRegistry.initRegistration();
        }
        return instance;
    }

    /**
     * Convenient method for accessing the communicator.
     * 
     * @return the communicator
     */
    public static Communicator getWinCommunicator() {
        return getInstance().m_com;
    }
    
    /**
     * @param message
     *            the message to send to native iOS side
     * @return the answer - may be null in case of remote server problems
     * @throws StepExecutionException
     *             in case of remote RC server problems
     */
    public static NativeAnswer sendMessage(NativeMessage message) 
        throws StepExecutionException {
        try {
            return getWinCommunicator().sendMsg(message);
        } catch (RemoteServerException e) {
            String errorMessage = e.getMessage();
            log.error(errorMessage, e);
            throw new StepExecutionException(errorMessage,
                    EventFactory.createActionError(TestErrorEvent
                        .EXECUTION_ERROR, new String[] {errorMessage}));
        }
    }
    
    /**
     * 
     * @return an integer value. 0 for normal application and 1 for windows app.
     */
    public int getAutType() {
        return m_autType;
    }

    /** {@inheritDoc} */
    public Object findComponent(IComponentIdentifier ci, int timeout) 
        throws ComponentNotFoundException, IllegalArgumentException {
        
        return ComponentHandler.findComponent(ci, timeout);
    }
}