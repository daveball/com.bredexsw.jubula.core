/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;


/**
 * Thread that checks every few seconds if the AUT is still alive.
 * 
 * @author patrick
 *
 */
public class AUTAliveThread extends Thread {

    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory.getLogger(AUTAliveThread.class);
    
    /**
     * Time in ms between the AUT checks.
     */
    private static final int WAITTIME = 5000;
    /**
     * Thread stops if the value is false
     */
    private boolean m_keepRunning = true;
    
    /**
     * The robot object for the communication.
     */
    private WinRobotImpl m_robot;
    
    /**
     * Constructor
     * 
     * @param winrobot WinRobotImpl object that is needed to check if the 
     * AUT is still alive or not.
     */
    public AUTAliveThread(WinRobotImpl winrobot) {
        this.m_robot = winrobot;
    }
    
    /**
     * run method
     */
    public void run() {
        while (m_keepRunning) {
            try {
                AUTAliveThread.sleep(WAITTIME);
                boolean isAlive = m_robot.isAUTactive();
                
                if (!isAlive) {
                    m_keepRunning = false;
                    WinAUTServer.getInstance().autClosedShutdown();
                }
                
            } catch (InterruptedException e) {
                log.error("InterruptedException during AUT alive check", e);
            } catch (RemoteServerException e) {
                System.exit(e.getExceptionID());
            }
        }
    }
    
    /**
     * 
     * @return if the thread is still running
     */
    public boolean isKeepRunning() {
        return m_keepRunning;
    }

    /**
     * 
     * @param keeprunning value to stop or run the thread
     */
    public void setKeepRunning(boolean keeprunning) {
        this.m_keepRunning = keeprunning;
    }
}
