/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.MatchUtil;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Tree;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;

/**
 * win toolkit tree adapter
 * @author BREDEX GmbH
 *
 */
public class TreeAdapter extends AbstractDotNetComponentAdapter {

    /** the tree component */
    private Tree m_component;
    /**
     * 
     * @param objectToAdapt the tree component
     */
    public TreeAdapter(Object objectToAdapt) {
        super(objectToAdapt);
        m_component = (Tree) objectToAdapt;
    }
    /**
    *
    * @return the text of the selected node in the receiver's tree. If multiple
    *         nodes are selected, then the text of one of those nodes
    *         (*which* node is undefined) is returned.
    */
    public String getTextOfSelectedNode() {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.GET_TREE_SELECTED_TEXT,
                m_component.getLocator());

        return WinAUTServer.sendMessage(msgObj).getString();
    }

   /**
    *
    * @return the text of the node (in the receiver's tree) at the given
    *         cursor position.
    */
    public String getTextOfNodeAtCursorPosition() {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.GET_TREE_CURSOR_POS_TEXT,
                m_component.getLocator());

        return WinAUTServer.sendMessage(msgObj).getString();
    }
    
    /**
    *
    * @return the text of the selected nodes in the receiver's tree. If multiple
    *         nodes are selected, then the text of one of those nodes
    *         (*which* node is undefined) is returned.
    */
    public String[] getTextOfSelectedNodes() {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.GET_TEXT_SELECTED_TREE_ITEMS,
                m_component.getLocator());
        return WinAUTServer.sendMessage(msgObj).getStringArray();
    }

    /**
     * Initializes the tree view as the first tree item on the Win side
     * 
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param isAbsolute
     *            whether the path is relative or absolute
     */
    public void initTreeMenu(boolean isAbsolute, int preAscend) {

        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.INIT_TREE_MENU, m_component.getLocator());
        msgObj.addParameters(isAbsolute, preAscend);

        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * Gets all child tree item names of the last expanded / involed tree item
     * 
     * @return the child names
     */
    public String[] getCurrentTreeItems() {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.GET_CURRENT_TREE_ITEMS);
        return WinAUTServer.sendMessage(msgObj).getStringArray();

    }

    /**
     * Selects a node relative to the currently selected node.
     * @param direction the direction to move.
     *                  directions:
     *                      UP - Navigates through parents
     *                      DOWN - Navigates through children
     *                      NEXT - Navigates to next sibling
     *                      PREVIOUS - Navigates to previous sibling
     * @param distance the distance to move
     * @param clickCount the click count to select the new cell.
     */
    public void moveInTree(String direction, int distance, int clickCount) {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.MOVE_IN_TREE, m_component.getLocator());
        msgObj.addParameters(direction, distance, clickCount);
        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * Searches the current opend menu items for the given name that matches
     * with the operator and selects the first hit.
     *
     * @param treeItem
     *            the name of the menuItem that shall be selected
     * @param operator
     *            the operator
     * @param clickCount
     *            click count
     * @param button
     *            the button to click
     * @param collapse
     *            Whether the node should be collapsed
     */
    public void selectTreeItemByName(String treeItem, String operator,
            int clickCount, int button, boolean collapse) {
        String[] menuItems = getCurrentTreeItems();
        boolean itemSelected = false;
        for (int i = 0; i < menuItems.length; i++) {
            if (MatchUtil.getInstance()
                    .match(menuItems[i], treeItem, operator)) {
                NativeMessage msgObj = new NativeMessage(
                        m_component.getNativeClassName(),
                        RobotConstants.SELECT_TREE_ITEM_BY_NAME);
                msgObj.addParameters(menuItems[i], clickCount,
                                     button, collapse);
                WinAUTServer.sendMessage(msgObj);
                itemSelected = true;
                break;
            }
        }
        // no select was performed so it means the item does not exits for our
        // match.
        if (!itemSelected) {
            throw new StepExecutionException(new Exception(
                    "TreeItem does not exist."));
        }
    }

   /**
     * Selects tree items by the given index path
     * 
     * @param idxPath
     *            the index path
     * @param absolute
     *            is the path absolute ?
     * @param preAscend
     *            preAscend
     * @param click
     *            click count
     * @param button
     *            mouse button
     * @param isMultiSelection
     *            multi selection
     * @param collapse
     *            if the last node should be collapsed
     */
    public void selectTreeItemByIndexPath(int[] idxPath, boolean absolute,
            int preAscend, int click, int button, boolean isMultiSelection,
            boolean collapse) {

        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.SELECT_CONTEXT_TREEITEM_BY_INDEXPATH,
                m_component.getLocator());
        msgObj.addParameters(idxPath, absolute, preAscend, click, button,
                collapse);
        if (isMultiSelection) {
            getRobot().keyPress(null, 17);
        }
        WinAUTServer.sendMessage(msgObj);
        if (isMultiSelection) {
            getRobot().keyRelease(null, 17);
        }

    }
}
