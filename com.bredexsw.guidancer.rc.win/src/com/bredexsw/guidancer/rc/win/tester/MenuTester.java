/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester;

import org.eclipse.jubula.rc.common.tester.AbstractMenuTester;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IComponent;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IMenuComponent;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IMenuItemComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Menu;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.guidancer.rc.win.tester.adapter.MenuAdapter;
import com.bredexsw.guidancer.rc.win.tester.adapter.MenuItemAdapter;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class MenuTester extends AbstractMenuTester {
   
    /** boolean for context menu */
    private boolean m_isContextMenu = false;

    /**
     * {@inheritDoc} 
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    @Override
    protected IMenuItemComponent newMenuItemAdapter(Object component) {
        return new MenuItemAdapter(null);
    }
    
    /**
     * {@inheritDoc}
     */
    public IComponent getComponent() {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName(),
                RobotConstants.EXISTS_MENUBAR);

        if (m_isContextMenu || WinAUTServer.sendMessage(msgObj).getBoolean()) {
            // creation of Menu due to the fact that Menus in Win have no own object
            return new MenuAdapter(new Menu());
        }
        return new MenuAdapter(null);
    }
    
    /**
     * @param isContextMenu is this a context menu?
     */
    public void setIsContextMenu(boolean isContextMenu) {
        m_isContextMenu = isContextMenu;
    }
    /**
     * {@inheritDoc}
     */
    public void verifyExists(String menuItem, String operator, boolean exists) {
        initWindowMenu();
        super.verifyExists(menuItem, operator, exists);
    }
    
    /**
     * {@inheritDoc}
     */
    public void verifyExistsByIndexpath(String menuItem, boolean exists) {
        initWindowMenu();
        super.verifyExistsByIndexpath(menuItem, exists);
    }
    
    /**
     * {@inheritDoc}
     */ 
    public void verifyEnabled(String menuItem, String operator, boolean enabled)
    {
        initWindowMenu();
        super.verifyEnabled(menuItem, operator, enabled);
        
    }
    /**
     * {@inheritDoc}
     */
    public void verifyEnabledByIndexpath(String menuItem, boolean enabled) {
        initWindowMenu();
        super.verifyEnabledByIndexpath(menuItem, enabled);
    }
    /**
     * {@inheritDoc}
     */
    public void selectMenuItem(String namePath, final String operator) {
        initWindowMenu();
        super.selectMenuItem(namePath, operator);
    }
    /**
     * {@inheritDoc}
     */
    public void selectMenuItemByIndexpath(String indexpath) {
        initWindowMenu();
        super.selectMenuItemByIndexpath(indexpath);
    }
    
   /**
    * {@inheritDoc}
    */
    public void verifySelected(String menuItem, String operator,
            boolean selected) {
        initWindowMenu();
        super.verifySelected(menuItem, operator, selected);
    }
    
    /**
     * {@inheritDoc}
     */
    public void verifySelectedByIndexpath(String menuItem, boolean selected) {
        initWindowMenu();
        super.verifySelectedByIndexpath(menuItem, selected);
    }
    
    /**
     * initializing the menu bar if one exists
     */
    public void initWindowMenu() {
        if (!m_isContextMenu) {
            NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName(),
                    RobotConstants.INIT_WINDOW_MENU);
            WinAUTServer.sendMessage(msgObj);
        }
    }
    
    /**
     * this methods closes the hole menu. It is clicking on the parent item in the menu bar.
     * 
     * If you need another implementation override this method.
     * @param menuBar the main menu
     * @param textPath the text path used for opening the menu
     * @param operator the operator which was used for opening the menu
     */
    protected void closeMenu(IMenuComponent menuBar, String[] textPath,
            String operator) {
        closeMenu(textPath.length);
    }
    
    /**
     * this methods closes the hole menu. It is clicking on the parent item in the menu bar.
     * 
     * If you need another implementation override this method. 
     * @param menuBar the main menu
     * @param path the integer based path used for opening the menu
     */
    protected void closeMenu(IMenuComponent menuBar, int[] path) {            
        closeMenu(path.length);
    }
    
    /**
     * closes the menu
     * @param count die number of submenus opened 
     */
    private void closeMenu(int count) {
        ((WinRobotImpl) getRobot()).closeMenu(null, count);
    }
}
