/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IComboComponent;
import org.eclipse.jubula.toolkit.enums.ValueSets;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBox;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class ComboBoxAdapter extends TextInputAdapter implements
        IComboComponent {

    /** component of type {@link ComboBox} */
    private ComboBox m_component;
    
    /**
     *
     * @param objectToAdapt component of type {@link ComboBox}
     */
    public ComboBoxAdapter(Object objectToAdapt) {
        super(objectToAdapt);
        m_component = (ComboBox) objectToAdapt;
    }

    /**
     * {@inheritDoc}
     */
    public int getSelectedIndex() {
        String text = getText();
        String[] values = getValues();
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(text)) {
                return i;
            }
        }
        return -1;
    }
    
    /**
     * {@inheritDoc}
     */
    public void select(int index) {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.SELECT_COMBO_ITEM,
                m_component.getLocator());
        msgObj.addParameter(index);
        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     */
    public void input(String text, boolean replace) {
        if (replace) {
            selectAll();
            getRobot().keyStroke(ValueSets.KeyStroke.delete.rcValue());
        }
        getRobot().scrollToVisible(m_component, null);
        getRobot().type(m_component, text);
    }

    /**
     * {@inheritDoc}
     */
    public String[] getValues() {

        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.GET_COMBO_ITEM_LABELS, m_component.getLocator());

        return WinAUTServer.sendMessage(msgObj).getStringArray();
    }

}
