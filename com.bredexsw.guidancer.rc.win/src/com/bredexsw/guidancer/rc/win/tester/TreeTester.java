/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester;

import org.eclipse.jubula.rc.common.driver.DragAndDropHelper;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.exception.StepVerifyFailedException;
import org.eclipse.jubula.rc.common.tester.WidgetTester;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.constants.TestDataConstants;
import org.eclipse.jubula.tools.internal.utils.StringParsing;

import com.bredexsw.guidancer.rc.win.tester.adapter.TreeAdapter;
/**
 * Tester class specific for windows toolkit trees
 * @author BREDEX GmbH
 *
 */
public class TreeTester extends WidgetTester {

    /**
     * {@inheritDoc}
     */
    public void rcVerifyTextAtMousePosition(String pattern, String operator) {
        String textAtMousePosition = getTreeAdapter()
                .getTextOfNodeAtCursorPosition();
        Verifier.match(textAtMousePosition, pattern, operator);
    }
    
    /**
     * Selects the last node of the path given by <code>indexPath</code>
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param indexPath
     *            the index path
     * @param clickCount
     *            the number of times to click
     * @param button
     *            what mouse button should be used
     * @param extendSelection
     *            Whether this selection extends a previous selection.
     */
    public void rcSelectByIndices(String pathType, int preAscend,
            String indexPath, int clickCount, int button,
            final String extendSelection) {
        indicesAction(pathType, preAscend, indexPath, clickCount, button,
                extendSelection, false);
    }

    /**
     * Selects the node at the end of the <code>treepath</code>.
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param treePath
     *            The tree path.
     * @param operator
     *            If regular expressions are used to match the tree path
     * @param clickCount
     *            the click count
     * @param button
     *            what mouse button should be used
     * @param extendSelection
     *            Whether this selection extends a previous selection.
     */
    public void rcSelect(String pathType, int preAscend, String treePath,
            String operator, int clickCount, int button,
            final String extendSelection) {

        String[] namePath = splitTextTreePath(treePath);
        final boolean isAbsolute = (pathType.equals("absolute"));
        final boolean isMultiSelection = (!extendSelection.equals(
                ValueSets.BinaryChoice.no.rcValue()));

        getTreeAdapter().initTreeMenu(isAbsolute,
                preAscend);

        if (isMultiSelection) {
            this.pressOrReleaseModifiers("control", true);
        }

        try {
            for (int i = 0; i < namePath.length; i++) {
                getTreeAdapter().selectTreeItemByName(namePath[i], operator,
                        clickCount, button, false);
            }
        } finally {
            // to be sure that the control button is really released
            if (isMultiSelection) {
                this.pressOrReleaseModifiers("control", false);
            }
        }
    }
    
    /**
     * Tests whether the given text treePath exists or not
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param treePath
     *            the path to check
     * @param operator
     *            the RegEx operator
     * @param exists
     *            if true, the verify succeeds if the path DOES exist. If false,
     *            the verify succeeds if the path DOES NOT exist.
     */
    public void rcVerifyPath(String pathType, int preAscend, String treePath,
            String operator, boolean exists) {

        String[] namePath = splitTextTreePath(treePath);
        final boolean isAbsolute = (pathType.equals("absolute"));
        boolean checkResult = true;

        try {
            getTreeAdapter().initTreeMenu(isAbsolute, preAscend);

            for (int i = 0; i < namePath.length; i++) {
                getTreeAdapter().selectTreeItemByName(namePath[i], operator, 0,
                        0, false);
            }
        } catch (StepExecutionException e) {
            // node not found
            checkResult = false;
        }
        Verifier.equals(exists, checkResult);
    }
    
    /**
     * Tests whether the given index treePath exists or not
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param treePath
     *            the path to check
     * @param exists
     *            if true, the verify succeeds if the path DOES exist. If false,
     *            the verify succeeds if the path DOES NOT exist.
     */
    public void rcVerifyPathByIndices(String pathType, int preAscend,
            String treePath, boolean exists) {
        boolean checkResult = true;
        try {
            indicesAction(pathType, preAscend, treePath, 0, 0,
                    ValueSets.BinaryChoice.no.rcValue(), false);
        } catch (StepExecutionException e) {
            // node not found
            checkResult = false;
        }
        Verifier.equals(exists, checkResult);
    }
    
    /**
     * Verifies whether the selection in the tree has a rendered text that
     * matches with a specific <code>operator</code> to <code>pattern</code>.
     * 
     * @param pattern
     *            The expected text
     * @param operator
     *            The operator to use when comparing the expected and actual
     *            values.
     * @throws StepExecutionException
     *             If no node is selected or the verification fails.
     */
    public void rcVerifySelectedValue(String pattern, String operator)
        throws StepExecutionException {
        String[] selection = getTreeAdapter().getTextOfSelectedNodes();
        int i = 0;
        boolean isMatched = false;
        while (i < selection.length && !isMatched) {
            try {
                Verifier.match(selection[i], pattern, operator);
                isMatched = true;
            } catch (StepVerifyFailedException svfe) {
                if (i == (selection.length - 1)) {
                    throw svfe;
                }
                // Otherwise just try the next element
            }
            i++;
        }
    }

    /**
     * Stores the text of the first selected node in the tree to the given
     * variable.
     * 
     * @param variable
     *            The variable name.
     * @return the text of the first selected node in the tree.
     */
    public String rcStoreSelectedNodeValue(String variable) {

        return getTreeAdapter().getTextOfSelectedNode();

    }

    /**
     * Stores the text of the node in the tree at the current mouse position to
     * the given variable.
     * 
     * @param variable
     *            The variable name.
     * @return the text of the node in the tree at the current mouse position.
     */
    public String rcStoreValueAtMousePosition(String variable) {

        return getTreeAdapter().getTextOfNodeAtCursorPosition();

    }

    /**
     * Expands the tree.
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param indexPath
     *            The index path
     */
    public void rcExpandByIndices(String pathType, int preAscend,
            String indexPath) {
        indicesAction(pathType, preAscend, indexPath, 0, 0,
                ValueSets.BinaryChoice.no.rcValue(), false);
    }

    /**
     * Expands the tree.
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param treePath
     *            the path which should be expanded
     * @param operator
     *            the operator
     */
    public void rcExpand(String pathType, int preAscend, String treePath,
            String operator) {
        String[] namePath = splitTextTreePath(treePath);
        final boolean isAbsolute = (pathType.equals("absolute"));

        getTreeAdapter().initTreeMenu(isAbsolute,
                preAscend);

        for (int i = 0; i < namePath.length; i++) {
            getTreeAdapter().selectTreeItemByName(namePath[i], operator, 0, 0,
                    false);
        }
    }
    
    /**
     * Collapses the tree. This method works like
     * {@link #rcCollapse(String, int, String, String)}, but expects an
     * enumeration of indices representing the top-down tree path. Any index is
     * the node's position at the current tree level.
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param indexPath
     *            The index path
     */
    public void rcCollapseByIndices(String pathType, int preAscend,
            String indexPath) {
        indicesAction(pathType, preAscend, indexPath, 0, 0,
                ValueSets.BinaryChoice.no.rcValue(), true);
    }

    /**
     * Collapses the JTree. The passed tree path is a slash-seperated list of
     * nodes that specifies a valid top-down path in the JTree. The last node of
     * the tree path is collapsed if it is currently expanded. Otherwise, the
     * JTree is left unchanged.
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param treePath
     *            The tree path.
     * @param operator
     *            Whether regular expressions are used to determine the tree
     *            path. <code>"matches"</code> for regex, <code>"equals"</code>
     *            for simple matching.
     */
    public void rcCollapse(String pathType, int preAscend, String treePath,
            String operator) {
        String[] namePath = splitTextTreePath(treePath);
        final boolean isAbsolute = (pathType.equals("absolute"));

        getTreeAdapter().initTreeMenu(isAbsolute,
                preAscend);

        for (int i = 0; i < namePath.length - 1; i++) {
            getTreeAdapter().selectTreeItemByName(namePath[i], operator, 0, 0,
                    false);
        }
        getTreeAdapter().selectTreeItemByName(namePath[namePath.length - 1],
                operator, 0, 0, true);
    }
    
    /**
     * Selects a node relative to the currently selected node.
     * @param direction the direction to move.
     *                  directions:
     *                      UP - Navigates through parents
     *                      DOWN - Navigates through children
     *                      NEXT - Navigates to next sibling
     *                      PREVIOUS - Navigates to previous sibling
     * @param distance the distance to move
     * @param clickCount the click count to select the new cell.
     */
    public void rcMove(String direction, int distance, int clickCount) {
        getTreeAdapter().moveInTree(direction, distance, clickCount);
    }

    /**
     * Drags the node of the given indexPath.
     * @param mouseButton the mouse button to press.
     * @param modifier the modifier to press.
     * @param pathType whether the path is relative or absolute
     * @param preAscend
     *      Relative traversals will start this many parent nodes
     *            above the current node. Absolute traversals ignore this
     *            parameter.
     * @param indexPath The index path.
     */
    public void rcDragByIndexPath(int mouseButton, String modifier,
            String pathType, int preAscend, String indexPath) {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setMouseButton(mouseButton);
        dndHelper.setModifier(modifier);
        
        indicesAction(pathType, preAscend, indexPath, 0, 0,
                ValueSets.BinaryChoice.no.rcValue(), false);
        
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);     
    }
    
    /**
     * Drags the node of the given treePath.
     * @param mouseButton the mouse button to press.
     * @param modifier the modifier to press.
     * @param pathType whether the path is relative or absolute
     * @param preAscend
     *      Relative traversals will start this many parent nodes
     *            above the current node. Absolute traversals ignore this
     *            parameter.
     * @param treePath The tree path.
     * @param operator If regular expressions are used to match the tree path
     */
    public void rcDragByTextPath(int mouseButton, String modifier,
            String pathType, int preAscend, String treePath, String operator) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setMouseButton(mouseButton);
        dndHelper.setModifier(modifier);

        rcSelect(pathType, preAscend, treePath, operator, 0, 0,
                ValueSets.BinaryChoice.no.rcValue());

        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);
    }
    
    /**
     * Drops the before dragged object on the given indexPath.
     * @param pathType whether the path is relative or absolute
     * @param preAscend
     *      Relative traversals will start this many parent nodes
     *            above the current node. Absolute traversals ignore this
     *            parameter.
     * @param indexPath The index path.
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    public void rcDropByIndexPath(String pathType, int preAscend,
            String indexPath, int delayBeforeDrop) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        final String modifier = dndHelper.getModifier();
        final int mouseButton = dndHelper.getMouseButton();

        indicesAction(pathType, preAscend, indexPath, 0, 0,
                ValueSets.BinaryChoice.no.rcValue(), false);

        waitBeforeDrop(delayBeforeDrop);
        pressOrReleaseModifiers(modifier, false);
        getRobot().mouseRelease(null, null, mouseButton);
    }
    
    /**
     * Drops the before dragged object on the given treePath.
     * @param pathType whether the path is relative or absolute
     * @param preAscend
     *      Relative traversals will start this many parent nodes
     *            above the current node. Absolute traversals ignore this
     *            parameter.
     * @param treePath The tree path.
     * @param operator If regular expressions are used to match the tree path
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    public void rcDropByTextPath(String pathType, int preAscend,
            String treePath, String operator, int delayBeforeDrop) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        final String modifier = dndHelper.getModifier();
        final int mouseButton = dndHelper.getMouseButton();

        rcSelect(pathType, preAscend, treePath, operator, 0, 0,
                ValueSets.BinaryChoice.no.rcValue());

        waitBeforeDrop(delayBeforeDrop);
        pressOrReleaseModifiers(modifier, false);
        getRobot().mouseRelease(null, null, mouseButton);
    }
        
    /**
     * 
     * @return TreeAdapter
     */
    private TreeAdapter getTreeAdapter() {
        return (TreeAdapter) getComponent();
    }
    /**
     * Abstract method for all index based actions on the tree. Select, Check,
     * Collapse and Expand Action are running through this method.
     * 
     * @param pathType
     *            whether the path is relative or absolute
     * @param preAscend
     *            Relative traversals will start this many parent nodes above
     *            the current node. Absolute traversals ignore this parameter.
     * @param indexPath
     *            the index path
     * @param clickCount
     *            the number of times to click
     * @param button
     *            what mouse button should be used
     * @param extendSelection
     *            Whether this selection extends a previous selection.
     * @param collapse
     *            wether the last node should be collapsed.
     */
    private void indicesAction(String pathType, int preAscend,
            String indexPath, int clickCount, int button,
            final String extendSelection, boolean collapse) {
        
        String[] stringIndexPath = indexPath.split(StringConstants.SLASH);
        int[] idxPath = new int[stringIndexPath.length];
        final boolean isAbsolute = (pathType.equals("absolute"));
        final boolean isMultiSelection = (!extendSelection.equals(
                ValueSets.BinaryChoice.no.rcValue()));

        for (int i = 0; i < stringIndexPath.length; i++) {
            idxPath[i] = Integer.parseInt(stringIndexPath[i]);
        }

        getTreeAdapter().selectTreeItemByIndexPath(idxPath, isAbsolute, 
                preAscend, clickCount, button, isMultiSelection, collapse);
    }
    
    /**
     * Splits the <code>treepath</code> string into an array, one entry for each level in the path
     *
     * @param treePath The tree path
     * @return An array of string representing the tree path
     */
    private String[] splitTextTreePath(String treePath) {
        return StringParsing.splitToArray(treePath,
                TestDataConstants.PATH_CHAR_DEFAULT,
                TestDataConstants.ESCAPE_CHAR_DEFAULT, 
                true);
    }
        

    
}
