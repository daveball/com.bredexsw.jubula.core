/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.ITextComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Text;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class TextAdapter extends AbstractDotNetComponentAdapter
        implements ITextComponent {

    /** Since Buttons and Text could have a text 
     * {@link AbstractDotNetComponent} is used 
     */
    private Text m_component;
    
    /**
     * @param objectToAdapt should be of type {@link Text}
     */
    public TextAdapter(Object objectToAdapt) {
        super(objectToAdapt);
        m_component = (Text) objectToAdapt;
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        //using getName because the automation controlType.text is using the name as the label text
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.ROBOT_METHOD_GET_NAME, 
                m_component.getLocator());

        String text = WinAUTServer.sendMessage(msgObj).getString();
        return text;
    }
    
}
