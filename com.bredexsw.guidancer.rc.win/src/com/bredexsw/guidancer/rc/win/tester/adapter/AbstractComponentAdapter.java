/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.driver.IRobotFactory;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IComponent;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public abstract class AbstractComponentAdapter implements IComponent {


    /**
     * {@inheritDoc}
     */
    public IRobotFactory getRobotFactory() {
        return null;
    }

}
