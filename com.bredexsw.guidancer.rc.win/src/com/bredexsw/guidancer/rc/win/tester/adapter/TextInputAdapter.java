/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.ITextInputComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.AbstractComponentWithTextInput;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;

/**
 * Adapter for Text Input components like document or edit
 * @author BREDEX GmbH
 *
 */
public class TextInputAdapter extends AbstractDotNetComponentAdapter implements
        ITextInputComponent {
    /** text component with input */
    private AbstractComponentWithTextInput m_component;
    
    /**
     * 
     * @param objectToAdapt should be of type {@link AbstractComponentWithTextInput}
     */
    public TextInputAdapter(Object objectToAdapt) {
        super(objectToAdapt);
        m_component = (AbstractComponentWithTextInput) objectToAdapt;
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.ROBOT_METHOD_GET_TEXT,
                m_component.getLocator());
        String answer = WinAUTServer.sendMessage(msgObj).getString();
        return answer;

    }

    /**
     * {@inheritDoc}
     */
    public void setSelection(int start) {
        NativeMessage msgObj = new NativeMessage(
                RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.SET_CARET_POSITION);
        msgObj.addParameters(start);
        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     */
    public void setSelection(int start, int end) {
        NativeMessage msgObj = new NativeMessage(
                RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.SELECT_PATTERN);
        msgObj.addParameters(start, end);
        WinAUTServer.sendMessage(msgObj);

    }

    /**
     * {@inheritDoc}
     */
    public String getSelectionText() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void selectAll() {
        NativeMessage msgObj = new NativeMessage(
                RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.SELECT_ALL);
        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEditable() {
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(), 
                RobotConstants.IS_EDITABLE,
                m_component.getLocator());
        return WinAUTServer.sendMessage(msgObj).getBoolean();
    }

}
