/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IButtonComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.AbstractButton;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.CheckBox;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.RadioButton;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
/**
 * Adapter for all button like components
 * @author BREDEX GmbH
 *
 */
public class AbstractButtonAdapter extends AbstractDotNetComponentAdapter 
    implements IButtonComponent {
    /** the button component*/
    private AbstractButton m_component;
    
    /** constructor to set a {@link AbstractButton} to the adapter 
     * @param objectToAdapt must be of type {@link AbstractButton}
     */
    public AbstractButtonAdapter(Object objectToAdapt) {
        super(objectToAdapt);
        m_component = (AbstractButton) objectToAdapt;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSelected() {
        if (m_component instanceof RadioButton 
                || m_component instanceof CheckBox) {
            NativeMessage msgObj = new NativeMessage(
                    m_component.getNativeClassName(),
                    RobotConstants.CHECK_SELECTION,
                    m_component.getLocator());
            return WinAUTServer.sendMessage(msgObj).getBoolean();
        }
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    public String getText() {
        //using getName because the automation controlType.text is using the name as the label text
        NativeMessage msgObj = new NativeMessage(
                m_component.getNativeClassName(),
                RobotConstants.ROBOT_METHOD_GET_NAME, 
                m_component.getLocator());

        String text = WinAUTServer.sendMessage(msgObj).getString();
        return text;
    }

}
