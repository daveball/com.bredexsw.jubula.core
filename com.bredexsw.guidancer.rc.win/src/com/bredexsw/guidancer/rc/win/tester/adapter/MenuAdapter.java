/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IMenuComponent;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IMenuItemComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Menu;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
/**
 * Adapter for menus, it has no real object because all information is requested from
 * the DotNet server
 * @author BREDEX GmbH
 *
 */
public class MenuAdapter extends AbstractComponentAdapter implements
        IMenuComponent {
    /** the menu */
    private Menu m_menu;

    /**
     * 
     * @param objectToAdapt
     *            the menu to adapt
     */
    public MenuAdapter(Object objectToAdapt) {
        m_menu = (Menu) objectToAdapt;
    }
    /**
     * {@inheritDoc}
     */
    public Object getRealComponent() {
        return m_menu;
    }
    
    /**
     * {@inheritDoc}
     */
    public IMenuItemComponent[] getItems() {
        NativeMessage msgObj = new NativeMessage(
                RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.GET_CURRENT_MENU_ITEMS);
        NativeAnswer answer = WinAUTServer.sendMessage(msgObj);
        String [] elements = answer.getStringArray();
        IMenuItemComponent[] menuItems = 
                new IMenuItemComponent[elements.length];
        for (int i = 0; i < elements.length; i++) {
            String name = elements[i];
            menuItems[i] = new MenuItemAdapter(name);
        }
        return menuItems;
    }
    
    /**
     * {@inheritDoc}
     */
    public int getItemCount() {
        return getItems().length;
    }

}
