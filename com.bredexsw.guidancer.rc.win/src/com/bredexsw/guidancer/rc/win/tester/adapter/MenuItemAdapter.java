/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IMenuComponent;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IMenuItemComponent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Menu;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;

/**
 * Since we have no real MenuItem component in Windows, we are only using its
 * name here as the component.
 * 
 * @author BREDEX GmbH
 * 
 */
public class MenuItemAdapter extends AbstractComponentAdapter implements
        IMenuItemComponent {

    /** Check selection at a menuItem ID */
    private final String m_checkSelectionID = "selection";
    /** Check existence at a menuItem ID */
    private final String m_checkExistenceID = "existence";
    /** Check enablement at a menuItem ID */
    private final String m_checkEnablementID = "enablement";
    /** Check expandable at a menuItem ID */
    private final String m_checkExpandableID = "expandable";
    
    /** Since there is no real MenuItem we are using its name*/
    private String m_name;
    
    /** The name identifies the menuItem 
     * @param name the text of the menuitem
     */
    public MenuItemAdapter(String name) {
        m_name = name;
    }
    /**
     * {@inheritDoc}
     */
    public Object getRealComponent() {
        return m_name;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSelected() {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName()
                , RobotConstants.CHECK_PROPERTY_OF_MENU);
        msgObj.addParameters(m_name, m_checkSelectionID);
        NativeAnswer answer = WinAUTServer.sendMessage(msgObj);
        return answer.getBoolean();
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        return m_name;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEnabled() {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName()
                , RobotConstants.CHECK_PROPERTY_OF_MENU);
        msgObj.addParameters(m_name, m_checkEnablementID);
        NativeAnswer answer = WinAUTServer.sendMessage(msgObj);
        return answer.getBoolean();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isExisting() {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName()
                , RobotConstants.CHECK_PROPERTY_OF_MENU);
        msgObj.addParameters(m_name, m_checkExistenceID);
        NativeAnswer answer = WinAUTServer.sendMessage(msgObj);
        return answer.getBoolean();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isShowing() {
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean hasSubMenu() {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName()
                , RobotConstants.CHECK_PROPERTY_OF_MENU);
        msgObj.addParameters(m_name, m_checkExpandableID);
        NativeAnswer answer = WinAUTServer.sendMessage(msgObj);
        return answer.getBoolean();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSeparator() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void selectMenuItem() {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName()
                , RobotConstants.SELECT_MENU_ITEM_BY_NAME);
        msgObj.addParameters(m_name);
        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     */
    public IMenuComponent openSubMenu() {
        selectMenuItem();
        return new MenuAdapter(new Menu());
    }

}
