/*******************************************************************************
 * Copyright (c) 2013 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.tester.adapter;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.StringTokenizer;

import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.DragAndDropHelper;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.AbstractMenuTester;
import org.eclipse.jubula.rc.common.tester.adapter.interfaces.IWidgetComponent;
import org.eclipse.jubula.rc.common.util.KeyStrokeUtil;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.tools.internal.i18n.I18n;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;

import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Menu;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.KeyCodeConverter;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.guidancer.rc.win.tester.MenuTester;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
/**
 * 
 * @author BREDEX GmbH
 *
 */
public class AbstractDotNetComponentAdapter extends AbstractComponentAdapter
        implements IWidgetComponent {
    /** constant for Win server abstract class name */
    public static final String ABSTRACT_CLASSNAME = "AbstractElementImplClass"; 
    /** */
    private AbstractDotNetComponent m_component;
    /**
     */
    private WinRobotImpl m_robot;
    
    /**
     * constructor setting the component
     * @param objectToAdapt should be an <code>AbstractDotNetComponent</code>
     */
    public AbstractDotNetComponentAdapter(Object objectToAdapt) {
        m_component = (AbstractDotNetComponent) objectToAdapt;
    }
    /**
     * {@inheritDoc}
     */
    public Object getRealComponent() {
        return m_component;
    }
    
    /**
     * @return the robot
     */
    protected WinRobotImpl getRobot() {
        if (m_robot == null) {
            m_robot = (WinRobotImpl) WinAUTServer.getInstance().getRobot();
        }
        return m_robot;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isShowing() {
        /*
         * The actual testing of the component's existence/non-existence is
         * implemented in CAPTestCommand.getImplClass.
         */
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isEnabled() {
        NativeMessage msgObj = new NativeMessage(ABSTRACT_CLASSNAME,
                RobotConstants.ROBOT_METHOD_CHECK_ENABLEMENT,
                m_component.getLocator());
        NativeAnswer message = WinAUTServer.sendMessage(msgObj);

        return message.getBoolean();
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasFocus() {
        NativeMessage msgObj = new NativeMessage(ABSTRACT_CLASSNAME,
                RobotConstants.HAS_FOCUS,
                m_component.getLocator());
        NativeAnswer message = WinAUTServer.sendMessage(msgObj);

        return message.getBoolean();
    }
    /**
     * {@inheritDoc}
     */
    public String getPropteryValue(String propertyname) {
        AbstractDotNetComponent comp = m_component;
        String locator = comp.getLocator();

        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = { RobotConstants.GET_PROPERTY, locator,
            propertyname };
        msgObj.addParameter(paramArray);

        NativeAnswer message = WinAUTServer.sendMessage(msgObj);
        return message.getString();
    }

    /**
     * {@inheritDoc}
     */
    public AbstractMenuTester showPopup(int xPos, String xUnits, int yPos,
            String yUnits, int button) throws StepExecutionException {
        String locator = m_component.getLocator();
        Rectangle bounds = getRobot().getBounds(ABSTRACT_CLASSNAME, locator);
        boolean xAbsolute = xUnits.equalsIgnoreCase(
                ValueSets.Unit.pixel.rcValue());
        boolean yAbsolute = yUnits.equalsIgnoreCase(
                ValueSets.Unit.pixel.rcValue());
        Point coords = getRobot()
                .getCoordinate(bounds, xPos, xAbsolute, yPos, yAbsolute);
        // Inits the context menu for proper
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = { RobotConstants.INIT_CONTEXT_WINDOW_MENU,
            locator, button, coords.x, coords.y, false };
        msgObj.addParameter(paramArray);
        WinAUTServer.sendMessage(msgObj);
        
        MenuTester menuTester = new MenuTester();
        menuTester.setComponent(new Menu());
        menuTester.setIsContextMenu(true);
        return menuTester;
    }

    /**
     * {@inheritDoc}
     */
    public AbstractMenuTester showPopup(int button) {
        return showPopup(50, ValueSets.Unit.percent.rcValue(),
                50, ValueSets.Unit.percent.rcValue(), button);
    }

    /**
     * {@inheritDoc}
     */
    public void showToolTip(String text, int textSize, int timePerWord,
            int windowWidth) {
        throw new StepExecutionException(
                I18n.getString(TestErrorEvent.UNSUPPORTED_OPERATION_ERROR),
                EventFactory.createActionError(
                    TestErrorEvent.UNSUPPORTED_OPERATION_ERROR));        
    }
    /**
     * {@inheritDoc}
     */
    public void rcDrag(int mouseButton, String modifier, int xPos,
            String xUnits, int yPos, String yUnits) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setMouseButton(mouseButton);
        dndHelper.setModifier(modifier);
        
        clickDirect(0, 0, xPos, xUnits, yPos, yUnits);
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);  
        
    }
    
    
    /**
     * {@inheritDoc}
     */
    public void rcDrop(int xPos, String xUnits, int yPos, String yUnits,
            int delayBeforeDrop) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        final String modifier = dndHelper.getModifier();
        final int mouseButton = dndHelper.getMouseButton();
        
        clickDirect(0, 0, xPos, xUnits, yPos, yUnits);
        TimeUtil.delay(delayBeforeDrop);
        pressOrReleaseModifiers(modifier, false);
        getRobot().mouseRelease(null, null, mouseButton); 
        
    }
    
    /**
     * clicks into a component.
     * 
     * @param count
     *            amount of clicks
     * @param button
     *            what mouse button should be used
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @throws StepExecutionException
     *             error
     */
    public void clickDirect(int count, int button, int xPos, String xUnits,
            int yPos, String yUnits) throws StepExecutionException {
        getRobot().click(
                getRealComponent(),
                null,
                ClickOptions.create().setClickCount(count)
                        .setMouseButton(button), xPos,
                xUnits.equalsIgnoreCase(ValueSets.Unit.pixel.rcValue()),
                yPos,
                yUnits.equalsIgnoreCase(ValueSets.Unit.pixel.rcValue()));
    }
    
    /**
     * {@inheritDoc}
     */
    public int getKeyCode(String mod) {
        return KeyCodeConverter.getKeyCode(mod);
    }

    /**
     * Presses or releases the given modifier.
     * 
     * @param modifier
     *            the modifier.
     * @param press
     *            if true, the modifier will be pressed. if false, the modifier
     *            will be released.
     */
    private void pressOrReleaseModifiers(String modifier, boolean press) {
        final StringTokenizer modTok = new StringTokenizer(
                KeyStrokeUtil.getModifierString(modifier), " "); //$NON-NLS-1$
        while (modTok.hasMoreTokens()) {
            final String mod = modTok.nextToken();
            final int keyCode = KeyCodeConverter.getKeyCode(mod);
            if (press) {
                getRobot().keyPress(null, keyCode);
            } else {
                getRobot().keyRelease(null, keyCode);
            }
        }
    }
}
