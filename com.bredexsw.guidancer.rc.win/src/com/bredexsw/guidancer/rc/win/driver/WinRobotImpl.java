/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.driver;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.exception.OsNotSupportedException;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.LocalScreenshotUtil;
import org.eclipse.jubula.rc.common.util.PointUtil;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.tools.internal.constants.AUTServerExitConstants;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Application;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBox;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.List;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Menu;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Tab;
import com.bredexsw.guidancer.rc.dotnet.components.controltype.Table;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.implclasses.AbstractWinImplClass;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerExceptionConstants;

/**
 * The Win toolkit robot implementation. Used to send all request and commands
 * to the Win server.
 */
public class WinRobotImpl implements IWinRobot {

    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory.getLogger(WinRobotImpl.class);

    /**
     * Communication object.
     */
    private static Communicator com;

    /**
     * Constructor with a communicator that owns a connection to the Win server
     * 
     * @param comm
     *            the Communicator
     */
    public WinRobotImpl(Communicator comm) {
        WinRobotImpl.com = comm;
    }

    /**
     * {@inheritDoc}
     */
    public void clickAtCurrentPosition(Object graphicsComponent,
            int clickCount, int button) throws RobotException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_CLICK);
        msgObj.addParameters(clickCount, button);
        try {
            com.sendMsg(msgObj);
        } catch (RemoteServerException e) {
            log.error(e.getExceptionMsg());
        }
    }

    /**
     * {@inheritDoc}
     */
    public void click(Object graphicsComponent, Object constraints)
        throws RobotException {

        click(graphicsComponent, constraints, new ClickOptions());
    }

    /**
     * Checks if the mouse curser is over the given component.
     * 
     * @param locator
     *            the locator to the component
     * @return if the mouse is over the element
     */
    public boolean isMouseInComponent(String locator) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = { RobotConstants.MOUSE_IN_COMPONENT, locator };
        msgObj.addParameter(paramArray);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void click(Object graphicsComponent, Object constraints,
            ClickOptions clickOptions, int xPos, boolean xAbsolute, int yPos,
            boolean yAbsolute) throws RobotException {
        try {
            Rectangle bounds = (Rectangle) constraints;

            // all concrete components not null, window etc. will be null
            if (graphicsComponent != null) {
                AbstractDotNetComponent component =
                        (AbstractDotNetComponent) graphicsComponent;
                String locator = component.getLocator();

                if (constraints == null) {
                    bounds = getBounds(
                            AbstractWinImplClass.ABSTRACT_CLASSNAME, 
                            locator);
                }
            }

            if (bounds != null) {
                Point coords = getCoordinate(bounds, xPos, xAbsolute, yPos,
                        yAbsolute);

                // move mouse
                moveMouseToPos(coords.x, coords.y);

                // send click command
                NativeMessage msgObj = new NativeMessage(
                        RobotConstants.WIN_ROBOT_CLASS,
                        RobotConstants.ROBOT_METHOD_CLICK);
                msgObj.addParameters(
                        clickOptions.getClickCount(),
                        clickOptions.getMouseButton());
                com.sendMsg(msgObj);
            }

        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                log.error(e.getExceptionMsg());
            }
        }
    }

    /**
     * Moves the mouse to a given position
     * 
     * @param x
     *            the x coordinate
     * @param y
     *            the y coordinate
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    public void moveMouseToPos(int x, int y) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.MOVE_MOUSE_TO);
        msgObj.addParameters(x, y);
        sendMessage(msgObj);
    }

    /**
     * Calculates the point where the mouse should be moved
     * 
     * @param bounds
     *            the bounds
     * @param xPos
     *            X-position within the constraints.
     * @param xAbsolute
     *            <code>True</code> if the x-position should be used as a pixel
     *            value. <code>False</code> if it should be considered as a
     *            percentage of the constraint's width.
     * @param yPos
     *            Y-position within the constraints.
     * @param yAbsolute
     *            <code>True</code> if the yx-position should be used as a pixel
     *            value. <code>False</code> if it should be considered as a
     *            percentage of the constraint's height.
     * @return the point coordinates
     */
    public Point getCoordinate(Rectangle bounds, int xPos, boolean xAbsolute,
            int yPos, boolean yAbsolute) {
        Point p = PointUtil.calculateAwtPointToGo(xPos, xAbsolute, yPos,
                yAbsolute, bounds);
        return p;
    }

    /**
     * {@inheritDoc}
     */
    public void click(Object graphicsComponent, Object constraints,
            ClickOptions clickOptions) throws RobotException {
        click(graphicsComponent, constraints, clickOptions, 50, false, 50,
                false);
    }

    /**
     * Select a text from pattern
     * 
     * @param begin
     *            the first index position
     * @param end
     *            the last index position
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    public void selectPattern(int begin, int end) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.SELECT_PATTERN);
        msgObj.addParameters(begin, end);
        sendMessage(msgObj);
    }

    /**
     * Check if the text input component is read only
     * 
     * @param locator the locator to the component
     * @param className the class name of the component
     * @return if readOnly
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public boolean isEditable(String className, String locator) 
        throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.IS_EDITABLE, locator);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * Get the size and position for a given component
     * 
     * @param locator
     *            the locator path
     * @param className
     *         the class name of the component
     * @return the bounds
     * @throws RemoteServerException
     * @throws RobotException
     */
    public Rectangle getBounds(String className, String locator) {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.GET_BOUNDS,
                locator);
        
        int[] bounds;
        WinAUTServer.getInstance();
        bounds = WinAUTServer.sendMessage(msgObj).getIntArray();
        Rectangle rec = new Rectangle(bounds[0], bounds[1], bounds[2],
                bounds[3]);
        return rec;

    }

    /**
     * Get the size and position for a cell in a Table component.
     * 
     * @param table
     *            The Table component.
     * @param row
     *            The row index (0-based) of the cell for which to retrieve the
     *            bounds. A value of {@link IWinRobot#HEADER_IDX} retrieves the
     *            bounds for a row header.
     * @param col
     *            The column index (0-based) of the cell for which to retrieve
     *            the bounds. A value of {@link IWinRobot#HEADER_IDX} retrieves
     *            the bounds for a column header.
     * @return the bounds for the cell at the intersection of the given indices
     *         on the Table found for the given locator.
     * @throws RemoteServerException
     * @throws RobotException
     */
    public Rectangle getCellBounds(Table table, int row, int col)
        throws RobotException, RemoteServerException {

        NativeMessage msgObj = new NativeMessage(table.getNativeClassName(),
                RobotConstants.GET_CELL_BOUNDS, table.getLocator());
        msgObj.addParameters(row, col);

        try {
            int[] bounds;
            bounds = com.sendMsg(msgObj).getIntArray();
            Rectangle rec = new Rectangle(bounds[0], bounds[1], bounds[2],
                    bounds[3]);
            return rec;
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return null;
    }

    /**
     * Get the size and position of a tab item in a Tab component.
     * 
     * @param tab
     *            The Tab component.
     * @param index
     *            The index (0-based) of the item for which to retrieve the
     *            bounds.
     * @return the bounds for the tab item at the given index on the Tab found
     *         for the given locator.
     * @throws RemoteServerException
     * @throws RobotException
     */
    public Rectangle getTabItemBounds(Tab tab, int index)
        throws RobotException, RemoteServerException {

        NativeMessage msgObj = new NativeMessage(tab.getNativeClassName(),
                RobotConstants.GET_TAB_ITEM_BOUNDS, tab.getLocator());
        
        msgObj.addParameter(index);

        try {
            int[] bounds;
            bounds = com.sendMsg(msgObj).getIntArray();
            Rectangle rec = new Rectangle(bounds[0], bounds[1], bounds[2],
                    bounds[3]);
            return rec;
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return null;
    }

    /**
     * Get the size and position of an item in a List component.
     * 
     * @param list
     *            The List component.
     * @param index
     *            The index (0-based) of the item for which to retrieve the
     *            bounds.
     * @return the bounds for the tab item at the given index on the List found
     *         for the given locator.
     * @throws RemoteServerException
     * @throws RobotException
     */
    public Rectangle getListItemBounds(List list, int index)
        throws RobotException, RemoteServerException {

        NativeMessage msgObj = new NativeMessage(list.getNativeClassName(),
                RobotConstants.GET_LIST_ITEM_BOUNDS, list.getLocator());
        
        msgObj.addParameter(index);
        try {
            int[] bounds;
            bounds = com.sendMsg(msgObj).getIntArray();
            Rectangle rec = new Rectangle(bounds[0], bounds[1], bounds[2],
                    bounds[3]);
            return rec;
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return null;
    }

    /**
     * Get the indices of all selected items in a List component.
     * 
     * @param list
     *            The List component.
     * @return the indices of all selected items in the List found for the given
     *         locator.
     * @throws RemoteServerException
     * @throws RobotException
     */
    public int[] getListSelectedIndices(List list) throws RobotException,
            RemoteServerException {

        NativeMessage msgObj = new NativeMessage(list.getNativeClassName(),
                RobotConstants.GET_LIST_SELECTED_INDICES, list.getLocator());

        try {
            return com.sendMsg(msgObj).getIntArray();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }

        return new int[0];
    }

    /**
     * Get the index of the selected tab item within a Tab component.
     * 
     * @param tab
     *            The Tab component.
     * @return the index of the selected tab item within the Tab found for the
     *         given locator, or <code>-1</code> if no tab item is selected.
     * @throws RemoteServerException
     * @throws RobotException
     */
    public int getSelectedTabItemIndex(Tab tab) throws RobotException,
            RemoteServerException {

        NativeMessage msgObj = new NativeMessage(tab.getNativeClassName(),
                RobotConstants.GET_SELECTED_TAB_ITEM_INDEX, tab.getLocator());

        try {
            return com.sendMsg(msgObj).getInt();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return -1;
    }

    /**
     * Gets the enablement state of the tab item at the given index within a Tab
     * component.
     * 
     * @param tab
     *            The Tab component.
     * @param index
     *            The index of the tab item within the Tab component.
     * @return <code>true</code> if no errors occur and the tab item is enabled.
     *         Otherwise, <code>false</code>.
     * @throws RemoteServerException
     * @throws RobotException
     */
    public boolean isTabItemEnabled(Tab tab, int index)
        throws RobotException, RemoteServerException {

        NativeMessage msgObj = new NativeMessage(tab.getNativeClassName(), 
                RobotConstants.IS_TAB_ITEM_ENABLED, tab.getLocator());
        
        msgObj.addParameter(index);

        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }

        return false;
    }

    /**
     * Get the size and position for a the active aut window
     * 
     * @param className
     *  the class name of the Win impl class
     * 
     * @return the bounds
     * @throws RemoteServerException
     * @throws RobotException
     */
    public Rectangle getActiveWindowBounds(String className) 
        throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(
                Application.getNativeClassName(),
                RobotConstants.GET_ACTIVE_WINDOW_BOUNDS);

        try {
            int[] bounds;
            bounds = com.sendMsg(msgObj).getIntArray();
            Rectangle rec = new Rectangle(bounds[0], bounds[1], bounds[2],
                    bounds[3]);
            return rec;
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return null;
    }

    /**
     * selects menu items by a index path
     * 
     * @param indexPath
     *            the index path
     * @param checkOnly
     *            if the last item should be checked or invoked
     * @param checkProperty
     *            the propertyname which will be checked
     * @return the check result
     * @throws RobotException
     *             robotException
     * @throws RemoteServerException
     *             remoteServerException
     * @deprecated
     */
    public boolean selectMenuItemByIndexpath(int[] indexPath,
            boolean checkOnly, String checkProperty) throws RobotException,
            RemoteServerException {

        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName(),
                RobotConstants.SELECT_MENUITEM_BY_INDEXPATH);
        msgObj.addParameters(indexPath, checkOnly, checkProperty);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * Check if the active window owns a menubar
     * 
     * @return if a menubar exist
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     * 
     */
    public boolean existsMenuBar() 
        throws RobotException, RemoteServerException {

        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName(),
                RobotConstants.EXISTS_MENUBAR);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * selects context menu items by a index path
     * 
     * @param locator
     *            the locator of the component that contains the context menu
     * @param intPath
     *            the index path
     * @param button
     *            the button
     * @param checkOnly
     *            last step check or invoke
     * @param checkProperty
     *            the property which will be checked
     * @param bounds
     *            the bounds of the element
     * @param xPos
     *            what x position
     * @param xAbsolute
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yAbsolute
     *            should y position be pixel or percent value
     * @param checkMouse
     *            if the mouse position should be checked
     * @return the check result
     * @throws RobotException
     *             robotException
     * @throws RemoteServerException
     *             remoteServerException
     * @deprecated
     */
    public boolean selectContextMenuItemByIndexpath(String locator,
            int[] intPath, int button, boolean checkOnly, String checkProperty,
            Rectangle bounds, int xPos, boolean xAbsolute, int yPos,
            boolean yAbsolute, boolean checkMouse) throws RobotException,
            RemoteServerException {

        Point coords = getCoordinate(bounds, xPos, xAbsolute, yPos, yAbsolute);

        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = {
            RobotConstants.SELECT_CONTEXT_MENUITEM_BY_INDEXPATH, locator,
            intPath, button, checkOnly, checkProperty, coords.x, coords.y,
            checkMouse };
        msgObj.addParameter(paramArray);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * Get the menuItems names from the current opend menu / selected item
     * 
     * @return the child names of the current selected / opened menu
     * @throws RobotException
     *             the RobotException
     * @throws RemoteServerException
     *             the RemoteServerException
     */
    public String[] getCurrentMenuItems() throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.GET_CURRENT_MENU_ITEMS);
        try {
            return com.sendMsg(msgObj).getStringArray();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return null;
    }

    /**
     * Perfroms a invoke or a expand action on the given menu item name.
     * 
     * @param menuItemName
     *            the name of the menu item
     * @param check
     *            should the item be checked
     * @param checkProperty
     *            the property name which will be checked
     * @return the check result
     * @throws RobotException
     *             the RobotException
     * @throws RemoteServerException
     *             the RemoteServerException
     * @deprecated
     */
    public boolean selectMenuItemByName(String menuItemName, boolean check,
            String checkProperty) throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName()
                , RobotConstants.SELECT_MENU_ITEM_BY_NAME);
        msgObj.addParameters(menuItemName, check, checkProperty);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /***
     * Searches for the main menu bar of the focus window on the Win Server
     * side. Only needed to refresh the saved old menu bar when using the menu
     * action containing a name path.
     * 
     * 
     * @throws RobotException
     * @throws RemoteServerException
     */
    public void initWindowMenu() throws RobotException,
        RemoteServerException {
        NativeMessage msgObj = new NativeMessage(Menu.getNativeClassName(),
                RobotConstants.INIT_WINDOW_MENU);
        sendMessage(msgObj);
    }

    /**
     * Opens the context menu of a component
     * 
     * @param locator
     *            the locator of the component
     * @param button
     *            the button id
     * @param bounds
     *            the bounds of the element
     * @param xPos
     *            what x position
     * @param xAbsolute
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yAbsolute
     *            should y position be pixel or percent value
     * @param checkMouse
     *            if the mouse position should be checked
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    public void initContextMenu(String locator, int button, Rectangle bounds,
            int xPos, boolean xAbsolute, int yPos, boolean yAbsolute,
            boolean checkMouse) throws RobotException, RemoteServerException {
        Point coords = getCoordinate(bounds, xPos, xAbsolute, yPos, yAbsolute);

        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = { RobotConstants.INIT_CONTEXT_WINDOW_MENU,
            locator, button, coords.x, coords.y, checkMouse };
        msgObj.addParameter(paramArray);
        sendMessage(msgObj);
    }

    /**
     * Check if the component has the focus currently
     * 
     * @param locator
     *            the locator
     * @param className 
     *            the component class name
     * @return the focus value
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    public boolean hasFocus(String className, String locator) 
        throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.HAS_FOCUS, locator);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void move(Object graphicsComponent, Object constraints)
        throws RobotException {

    }

    /**
     * {@inheritDoc}
     */
    public void type(Object graphicsComponent, char character)
        throws RobotException {
        

    }

    /**
     * {@inheritDoc}
     */
    public void type(Object graphicsComponent, String text)
        throws RobotException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_INPUT_TEXT);
        msgObj.addParameter(text);
        WinAUTServer.sendMessage(msgObj);

    }

    /**
     * {@inheritDoc}
     */
    public void keyType(Object graphicsComponent, int keycode)
        throws RobotException {

    }

    /**
     * {@inheritDoc}
     */
    public void keyPress(Object graphicsComponent, int keycode)
        throws RobotException {
        
        // ignoring the graphicsComponent
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRESS_BUTTON);
        msgObj.addParameter(keycode);
        try {
            com.sendMsg(msgObj);
        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                log.error(e1.getExceptionMsg());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void keyRelease(Object graphicsComponent, int keycode)
        throws RobotException {
        
        // ignoring the graphicsComponent
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.RELEASE_BUTTON);
        msgObj.addParameter(keycode);
        try {
            com.sendMsg(msgObj);
        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                log.error(e1.getExceptionMsg());
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    public void keyStroke(String keyStrokeSpecification) throws RobotException {

    }

    /**
     * {@inheritDoc}
     */
    public void scrollToVisible(Object graphicsComponent, Object constraints)
        throws RobotException {

    }

    /**
     * {@inheritDoc}
     */
    public void keyToggle(Object obj, int key, boolean activated)
        throws OsNotSupportedException {

    }

    /**
     * {@inheritDoc}
     */
    public void activateApplication(String method) throws RobotException {
        WinAUTServer server = WinAUTServer.getInstance();
        if (server.getAutType() == 1) {
            server.startAUT();
        }
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.SET_AUT_FOCUS);
        NativeMessage msgObjClick = new NativeMessage(
                RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ACTIVATION_CLICK);
        msgObjClick.addParameter(method);
        try {
            com.sendMsg(msgObj);
            com.sendMsg(msgObjClick);
        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Gets all open windows names.
     * 
     * @param className
     *  the class name of the Win impl class
     * 
     * @return the name of all open windows
     * @throws RobotException
     *             the {@link RobotException}
     */
    public String[] getAllWindowTitles(String className) throws RobotException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.GET_ALL_WINDOW_TITLES);
        try {
            return com.sendMsg(msgObj).getStringArray();
        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Get the title of the current active window
     * 
     * @param className
     *  the class name of the Win impl class
     * 
     * @return the current active window title
     * @throws RobotException
     *             the {@link RobotException}
     */
    public String getActiveWindowTitle(String className) throws RobotException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.GET_ACTIVE_WINDOW_TITLE);
        try {
            return com.sendMsg(msgObj).getString();
        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public Point getCurrentMousePosition() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void mousePress(Object graphicsComponent, Object constraints,
            int button) throws RobotException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRESS_MOUSE);
        msgObj.addParameter(button);
        try {
            com.sendMsg(msgObj);
        } catch (RemoteServerException e) {
            log.error(e.getExceptionMsg());
        }

    }

    /**
     * {@inheritDoc}
     */
    public void mouseRelease(Object graphicsComponent, Object constraints,
            int button) throws RobotException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.RELEASE_MOUSE);
        msgObj.addParameter(button);
        try {
            com.sendMsg(msgObj);
        } catch (RemoteServerException e) {
            log.error(e.getExceptionMsg());
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean isMouseInComponent(Object graphicsComponent) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public String getSystemModifierSpec() {
        String keyStrokeSpec = ValueSets.Modifier.control.rcValue();
        return keyStrokeSpec;
    }

    /**
     * {@inheritDoc}
     */
    public String getPropertyValue(Object graphicsComponent, 
            String propertyName) throws RobotException {

        AbstractDotNetComponent comp =
                (AbstractDotNetComponent) graphicsComponent;
        String locator = comp.getLocator();

        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = { RobotConstants.GET_PROPERTY, locator,
            propertyName };
        msgObj.addParameter(paramArray);

        try {
            String answer = com.sendMsg(msgObj).getString();
            return answer;

        } catch (RemoteServerException e) {
            try {
                checkExceptionType(e);
            } catch (RemoteServerException e1) {
                log.error(e.getExceptionMsg());
            }
        }

        return StringConstants.EMPTY;
    }

    /**
     * Close context menu or menu of menu bar by pressing ESC.
     * 
     * @param locator
     *            The locator of the parent widget for context menus or null for menu bar menus.
     * @param count
     *            The number how often the ESC button should be pressed.
     */
    public void closeMenu(String locator, int count) {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.PRE_ACTION);
        Object[] paramArray = {
            RobotConstants.CLOSE_MENU, locator, count};
        msgObj.addParameter(paramArray);
        WinAUTServer.sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     * 
     * @throws RemoteServerException
     */
    public void startAUT(String autPath, String autWorkDir, String args)
        throws RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_START_AUT);
        msgObj.addParameters(autPath, autWorkDir, args);
        WinAUTServer.getWinCommunicator().sendMsg(msgObj);
    }

    /**
     * {@inheritDoc}
     * @throws RemoteServerException
     */
    public void startAUT(String appName, String args)
        throws RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_START_AUT);
        msgObj.addParameters(appName, args);
        WinAUTServer.getWinCommunicator().sendMsg(msgObj);
    }

    /**
     * {@inheritDoc}
     * 
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void stopAUT() throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_STOP_AUT);
        sendMessage(msgObj);
    }

   /**
    * @param className the class name of the component
    * @param locator the locator to the component
    * @return the text of the component
    * @throws RemoteServerException the {@link RemoteServerException}
    */
    public String getText(String className, String locator) 
        throws RemoteServerException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.ROBOT_METHOD_GET_TEXT,
                locator);
        try {
            String answer = com.sendMsg(msgObj).getString();
            return answer;

        } catch (RemoteServerException e) {
            checkExceptionType(e);
            return null;
        }
    }

    /**
     * Returns the displayed text of a button which is the same like the
     * automation element name.
     * 
     * @param locator
     *            the locator
     * @param className
     *            the native class name of the component
     * @return the name / text of the button
     * @throws RemoteServerException
     */
    public String getName(String className, String locator) 
        throws RemoteServerException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.ROBOT_METHOD_GET_NAME,
                locator);
        try {
            String answer = com.sendMsg(msgObj).getString();
            return answer;
        } catch (RemoteServerException e) {
            checkExceptionType(e);
            return null;
        }
    }

    /**
     * Checks the radio button selection status
     * 
     * @param locator
     *            the locator
     * @param className
     *             the class name of the component
     * @return the check result
     */
    public boolean checkRadioButtonSelection(String className, String locator)
        throws RobotException, RemoteServerException {
        
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.CHECK_SELECTION, locator);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }

        return false;
    }

    /**
     * Checks the check box button selection status
     * 
     * @param locator
     *            the locator
     * @param className
     *            the component class name
     * @return the check result
     */
    public boolean checkCheckBoxSelection(String className, String locator)
        throws RobotException, RemoteServerException {
        
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.CHECK_CHECKBOX_SELECTION, locator);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }

        return false;
    }
    
    /**
     * Checks if the cell is editable at the current mouse position.
     * 
     * @param table the table
     * @return if the cell is editable
     * 
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public boolean checkCellEditabilityAtMousePos(Table table) throws
    RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(table.getNativeClassName(),
                RobotConstants.CHECK_CELL_EDITABILITY_AT_MOUSE_POS);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }
    
    /**
     * Checks if the current selected cell is editable.
     * 
     * 
     * @param table the table
     * @return if the cell is editable
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public boolean checkCurrentCellEditability(Table table) throws 
    RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(table.getNativeClassName(),
                RobotConstants.CHECK_SELECTED_CELL_EDITABILITIY,
                table.getLocator());
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }
    
    /**
     * Checks if the given cell is editable.
     * 
     * @param table the table
     * @param row the cell row
     * @param col the cell column
     * @return if the cell is editable
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public boolean checkCellEditability(Table table, int row, int col)
        throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(table.getNativeClassName(),
                RobotConstants.IS_CELL_EDITABLE, table.getLocator());
        msgObj.addParameters(row, col);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }
    
    /**
     * Send the checkEnablement command to the Win server
     * 
     * @param locator
     *            the locator for the automationElement
     * @param className
     *            the component class name           
     * @return if the component is enable
     * @throws RobotException
     *             exception
     * @throws RemoteServerException
     *             exception
     */
    public boolean checkEnablement(String className, String locator)
        throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(className,
                RobotConstants.ROBOT_METHOD_CHECK_ENABLEMENT, locator);
        try {
            return com.sendMsg(msgObj).getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * Send the inputText command to the Win server
     * 
     * @param insertText
     *            text to insert
     * @throws RobotException
     *             robotException
     * @throws RemoteServerException
     *             remoteServerException
     */
    public void inputText(String insertText) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_INPUT_TEXT);
        msgObj.addParameter(insertText);
        sendMessage(msgObj);
    }

    /**
     * Send the inputTextInCellEditor command to the Win server, which inputs
     * the given text without performing any additional operations (that may
     * cause the focused cell editor to change).
     * 
     * @param insertText
     *            text to insert
     * @throws RobotException
     *             robotException
     * @throws RemoteServerException
     *             remoteServerException
     */
    public void inputTextInCellEditor(String insertText) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.INPUT_TEXT_IN_CELL_EDITOR);
        msgObj.addParameter(insertText);
        sendMessage(msgObj);
    }

    /**
     * Send the replace command to the Win server
     * 
     * @param insertText
     *            the text that we will to insert as a replacement
     * @throws RobotException
     *             robotException
     * @throws RemoteServerException
     *             remoteServerException
     */
    public void replaceText(String insertText) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_REPLACE_TEXT);
        msgObj.addParameter(insertText);
        sendMessage(msgObj);
    }

    /**
     * Selects the full text of a component with text input
     * 
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    public void selectAll() throws RobotException, RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.SELECT_ALL);
        sendMessage(msgObj);
    }

    /**
     * @param msgObj
     *            the message to send
     * @throws RemoteServerException
     *             in case of an error in native code
     */
    private void sendMessage(NativeMessage msgObj) throws RemoteServerException
    {
        try {
            com.sendMsg(msgObj);
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
    }

    
    /**
     * Insert text after the index positon
     * 
     * @param text
     *            the text to insert
     * @param index
     *            the index position
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    public void insertTextAfterIndex(String text, int index)
        throws RobotException, RemoteServerException {
        
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.INSERT_TEXT_AFTER_INDEX);
        msgObj.addParameters(text, index);
        sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isAUTactive() throws RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_CHECK_AUT);
        try {
            NativeAnswer rmo = com.sendMsg(msgObj);
            return rmo.getBoolean();
        } catch (RemoteServerException e) {
            checkExceptionType(e);
        }
        return false;
    }

    /**
     * Toggle the given key
     * 
     * @param key
     *            to set numlock Num Lock 1 caplock Caps Lock 2 scrolllock
     *            Scroll 3
     * @param activated
     *            boolean
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     * @throws RobotException
     *             the {@link RobotException}
     */
    public void toggleKey(int key, boolean activated) throws RobotException,
            RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.TOGGLE_KEY);
        msgObj.addParameters(key, activated);
        sendMessage(msgObj);
    }

    /**
     * {@inheritDoc}
     */
    public void stopServer() throws RemoteServerException {
        NativeMessage msgObj = new NativeMessage(RobotConstants.WIN_ROBOT_CLASS,
                RobotConstants.ROBOT_METHOD_STOP_AUT);
        sendMessage(msgObj);
    }

    /**
     * Checks what kind of exception the Win server has thrown.
     * 
     * @param serverException
     *            the ServerException
     * @throws RemoteServerException
     */
    private void checkExceptionType(RemoteServerException serverException)
        throws RobotException, RemoteServerException {

        int id = serverException.getExceptionID();
        switch (id) {
            case AUTServerExitConstants.EXIT_INVALID_ARGS:
                throw serverException;
            case AUTServerExitConstants.EXIT_AUT_NOT_FOUND:
                throw serverException;
            case AUTServerExitConstants.AUT_START_ERROR:
                throw serverException;
            case AUTServerExitConstants.AUT_NOT_UIA_SUPPORTED:
                throw serverException;
            case AUTServerExitConstants.EXIT_AUT_STOP_FAILED:
                System.exit(0);
            case RemoteServerExceptionConstants.STEP_EXECUTION_EXCEPTION:
                throw new StepExecutionException(serverException);
            default:
                throw new RobotException(serverException);
        }
    }

    /**
     * Selects the item at the given index in the given combo box's dropdown
     * list.
     * 
     * @param comboBox
     *            The combo box.
     * @param index
     *            The index of the item to select.
     * @deprecated
     */
    public void selectComboBoxItem(ComboBox comboBox, int index)
        throws RobotException, RemoteServerException {
        
        NativeMessage msgObj = new NativeMessage(comboBox.getNativeClassName(),
                RobotConstants.SELECT_COMBO_ITEM, comboBox.getLocator());
        msgObj.addParameter(index);
        
        sendMessage(msgObj);
    }
    
    /** {@inheritDoc} */
    public BufferedImage createFullScreenCapture() {
        return LocalScreenshotUtil.createFullScreenCapture();
    }

}
