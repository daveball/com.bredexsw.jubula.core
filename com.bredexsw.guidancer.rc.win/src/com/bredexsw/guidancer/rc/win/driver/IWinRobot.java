/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.driver;

import org.eclipse.jubula.rc.common.driver.IRobot;

import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Interface for the Win-Toolkit Robot
 * FIXME RB: Is this interface necessary, because reflection works without interfaces?
 */
public interface IWinRobot extends IRobot {

    /** 
     * the index to use when referencing a column header or row header in 
     * a Table
     */
    public static final int HEADER_IDX = -1;

    /**
     * Starts the normal application AUT.
     * 
     * @param autPath The path to the AUT.
     * @param autWorkDir The working directory path used by AUT.
     * @param args Arguments for the AUT-Start.
     * @throws RemoteServerException
     */
    void startAUT(String autPath, String autWorkDir, String args)
        throws RemoteServerException;

    /**
     * Starts the modern UI AUT.
     * @param autPath The path to the AUT.
     * @param args Arguments for the AUT-Start.
     * @throws RemoteServerException
     */
    void startAUT(String autPath, String args) throws RemoteServerException;

    /**
     * Stops the AUT.
     * @throws RemoteServerException 
     */
    void stopAUT() throws RemoteServerException;
    
    /**
     * Checks if the AUT is still active.
     * 
     * @return true of false value
     * @throws RemoteServerException 
     */
    boolean isAUTactive() throws RemoteServerException;
    
    /**
     * Stops the Win server.
     */
    void stopServer() throws RemoteServerException;
    
}
