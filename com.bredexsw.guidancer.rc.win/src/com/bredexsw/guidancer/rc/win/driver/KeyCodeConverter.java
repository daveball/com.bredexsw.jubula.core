/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.driver;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;

/**
 * Utility Class to convert key descriptions into the virtual key codes.
 */
public class KeyCodeConverter {
    
    /**
     * ID code for windows ESC 
     */
    public static final String ESC = "ESC";
    /**
     * Virtual key code for windows shift
     */
    private static final int VK_SHIFT = 0x10;
    /**
     * Virtual key code for windows control 
     */
    private static final int VK_CONTROL = 0x11;
    /**
     * Virtual key code for windows alt 
     */
    private static final int VK_ALT = 0x12;
    /**
     * Virtual key code for windows ESC 
     */
    private static final int VK_ESC = 0x1B;
    
    /**
     * The Converter Map.
     */
    private static Map<String, Integer> converterTable = null;
    
    static {
        converterTable = new HashMap<String, Integer>();
        converterTable.put(ValueSets.Modifier.none.rcValue(), new Integer(-1));
        converterTable.put(ValueSets.Modifier.shift.rcValue(), new Integer(
            VK_SHIFT));
        converterTable.put(ValueSets.Modifier.control.rcValue(), new Integer(
            VK_CONTROL));
        converterTable.put(ValueSets.Modifier.alt.rcValue(),
            new Integer(VK_ALT));
        converterTable.put(ValueSets.Modifier.meta.rcValue(), new Integer(
            VK_ALT));
        converterTable.put(ValueSets.Modifier.mod.rcValue(), new Integer(
            VK_CONTROL));
        converterTable.put(ESC, new Integer(VK_ESC));
    }
    

    /**
     * Utility Constructor.
     */
    private KeyCodeConverter() {
        // nothing
    }
    
    /**
     * Gets the Virtual-Key-Code of the given key.
     * @param key a description of the key, e.g. "control", "alt", etc.
     * @return the Virtual-Key-Code of the given key or -1 if key is "none".
     */
    public static int getKeyCode(String key) {
        if (key == null) {
            throw new RobotException("Key is null!", //$NON-NLS-1$
                    EventFactory.createConfigErrorEvent());
        }
        final Integer keyCode = converterTable.get(key.toLowerCase());
        if (keyCode == null) {
            throw new RobotException("No KeyCode found for key '" + key + "'", //$NON-NLS-1$//$NON-NLS-2$
                    EventFactory.createConfigErrorEvent());
        }
        return keyCode.intValue();
    }
    
}
