/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.driver;

/**
 * Constants for all robot methods
 */
public class RobotConstants {
    
    /**
     * class name of the Win-Server class 
     */
    public static final String HIERARCHY_CLASS = "AUTHierarchyRobotImpl"; //$NON-NLS-1$
    
    /**
     * class name of the Win-Server class 
     */
    public static final String WIN_ROBOT_CLASS = "WinRobotImpl"; //$NON-NLS-1$
    
    /**
     * string which is appended to all "getNativeClassName" component methods 
     */
    public static final String IMPL_CLASS_STRING = "ImplClass";
    
    /**
     * Pre-Action method that needs to be called before a action is running
     */
    public static final String PRE_ACTION = "preAction"; //$NON-NLS-1$
    
    /**
     * Method for AUT start
     */
    public static final String ROBOT_METHOD_START_AUT = "startAUT"; //$NON-NLS-1$
    
    /**
     * Method for AUT stop
     */
    public static final String ROBOT_METHOD_STOP_AUT = "stopAUT"; //$NON-NLS-1$
    
    /**
     * Method for AUT active check
     */
    public static final String ROBOT_METHOD_CHECK_AUT = "isAUTactive"; //$NON-NLS-1$
    
    /**
     * Method for Win Server stop
     */
    public static final String ROBOT_METHOD_SERVER_STOP = "stopServer"; //$NON-NLS-1$
    
    /**
     * Method for OM start
     */
    public static final String ROBOT_METHOD_OM_START = "startMapping"; //$NON-NLS-1$
    
    /**
     * Method for OM stop
     */
    public static final String ROBOT_METHOD_OM_STOP = "stopMapping"; //$NON-NLS-1$
    
    /**
     * Method for invoking a click
     */
    public static final String ROBOT_METHOD_CLICK = "click"; //$NON-NLS-1$
    
    /**
     * Method for replacing a text
     */
    public static final String ROBOT_METHOD_REPLACE_TEXT = "replaceText"; //$NON-NLS-1$
    
    /**
     * Method for toggle a key
     */
    public static final String TOGGLE_KEY = "toggle"; //$NON-NLS-1$
    
    /**
     * Method for input a text
     */
    public static final String ROBOT_METHOD_INPUT_TEXT = "inputText"; //$NON-NLS-1$
    
    /**
     * Method for inputting text into a table cell editor
     */
    public static final String INPUT_TEXT_IN_CELL_EDITOR = "inputTextInCellEditor"; //$NON-NLS-1$

    /**
     * Method for get a text
     */
    public static final String ROBOT_METHOD_GET_TEXT = "getText"; //$NON-NLS-1$
    
    /**
     * Method for select all text
     */
    public static final String SELECT_ALL = "selectAll"; //$NON-NLS-1$
    
    /**
     * Method for closing a menu
     */
    public static final String CLOSE_MENU = "closeMenu"; //$NON-NLS-1$
    
    /**
     * Method to get component property
     */
    public static final String GET_PROPERTY = "getProperty"; //$NON-NLS-1$
    
    /**
     * Method to press a mouse button hold it
     */
    public static final String PRESS_MOUSE = "pressMouse"; //$NON-NLS-1$
    
    /**
     * Method to release a mouse button
     */
    public static final String RELEASE_MOUSE = "releaseMouse"; //$NON-NLS-1$
    
    /**
     * Method to get component property
     */
    public static final String SELECT_PATTERN = "selectPattern"; //$NON-NLS-1$
    
    /**
     * Method to set the caret position in a textfield
     */
    public static final String SET_CARET_POSITION = "setCaretPosition";
    
    /**
     * Method to check if the text input component is readonly
     */
    public static final String IS_EDITABLE = "isEditable"; //$NON-NLS-1$
    
    /**
     * Method to get active window bounds
     */
    public static final String GET_ACTIVE_WINDOW_BOUNDS = "getActiveWindowBounds"; //$NON-NLS-1$
    
    /**
     * Method to check if the mouse is in the component
     */
    public static final String MOUSE_IN_COMPONENT = "isMouseInComponent"; //$NON-NLS-1$
    
    /**
     * Method insert text after a index
     */
    public static final String INSERT_TEXT_AFTER_INDEX = "insertTextAfterIndex"; //$NON-NLS-1$
    
    /**
     * Method for get a name
     */
    public static final String ROBOT_METHOD_GET_NAME = "getName"; //$NON-NLS-1$
    
    /**
     * Method for check enablement
     */
    public static final String ROBOT_METHOD_CHECK_ENABLEMENT = "checkEnablement"; //$NON-NLS-1$
    
    /**
     * Method for setting supported components 
     */
    public static final String ROBOT_METHOD_OM_SUPPORTEDCOMPONENTS = "setAUTListOfSupportedComponents"; //$NON-NLS-1$
    
    /**
     * Method for setting the OM collect keyboard shortcuts
     */
    public static final String ROBOT_METHOD_OM_SHORTCUTS = "setShortCuts"; //$NON-NLS-1$
    
    /**
     * Method for getting the selected components
     */
    public static final String ROBOT_METHOD_OM_GET_ELEMENTS = "getSelectedElements"; //$NON-NLS-1$
    
    /**
     * Method for getting the AUTHierarchy from the Win Server
     */
    public static final String GET_HIERARCHY = "getHierarchy"; //$NON-NLS-1$
    
    /**
     * Method for getting the AUT to the front
     */
    public static final String SET_AUT_FOCUS = "setAUTFocus"; //$NON-NLS-1$
    
    
    /**
     * Method for clicking in the aut
     */
    public static final String ACTIVATION_CLICK = "appActivationClick"; //$NON-NLS-1$
    
    /**
     * Method for clicking in the aut
     */
    public static final String EXISTS_MENUBAR = "existsMenuBar"; //$NON-NLS-1$
    

    /**
     * Action method name for selecting a menu item by the index path
     */
    public static final String SELECT_MENUITEM_BY_INDEXPATH  = "selectMenuItemByIndexpath"; //$NON-NLS-1$
    
    /**
     * Action method name for selecting a menu item by the index path
     */
    public static final String SELECT_CONTEXT_MENUITEM_BY_INDEXPATH  = "selectContextMenuItemByIndexpath"; //$NON-NLS-1$
    
    /**
     * Action method name for getting the current menu items
     */
    public static final String GET_CURRENT_MENU_ITEMS = "getCurrentMenuItems"; //$NON-NLS-1$
    
    /**
     * Action method name for selecting menu items
     */
    public static final String SELECT_MENU_ITEM_BY_NAME = "selectMenuItemByName"; //$NON-NLS-1$
    
    /**
     * 
     */
    public static final String CHECK_PROPERTY_OF_MENU = "checkPropertys";
    
    /**
     * Action method name to init the menu bar
     */
    public static final String INIT_WINDOW_MENU = "initWindowMenu"; //$NON-NLS-1$
    
    /**
     * Action method name to init the context menu
     */
    public static final String INIT_CONTEXT_WINDOW_MENU = "initContextMenu"; //$NON-NLS-1$
    
    /**
     * Action method name to to get all open window names
     */
    public static final String GET_ALL_WINDOW_TITLES = "getAllWindowTitles"; //$NON-NLS-1$
    
    /**
     * Action method name to to get the current active window title
     */
    public static final String GET_ACTIVE_WINDOW_TITLE = "getActiveWindowTitle"; //$NON-NLS-1$
    
    /**
     * Action method name to select a tree item by name
     */
    public static final String SELECT_TREE_ITEM_BY_NAME = "selectTreeItemByName"; //$NON-NLS-1$
    
    /**
     * Action method name to init the tree menu
     */
    public static final String INIT_TREE_MENU = "initTreeMenu"; //$NON-NLS-1$
    
    /**
     * Action method name for getting the current tree items
     */
    public static final String GET_CURRENT_TREE_ITEMS = "getCurrentTreeItems"; //$NON-NLS-1$
    
    /**
     * Action method name for getting the text of the selected tree item
     */
    public static final String GET_TREE_SELECTED_TEXT = "getTextOfSelectedTreeItem"; //$NON-NLS-1$
    
    /**
     * Action method name for getting the text of the multiple selected tree items
     */
    public static final String GET_TEXT_SELECTED_TREE_ITEMS = "getTextOfSelectedTreeItems"; //$NON-NLS-1$
    
    /**
     * Action method name for getting the text of the tree item at the current
     * cursor position
     */
    public static final String GET_TREE_CURSOR_POS_TEXT = "getTextOfTreeItemAtCursorPosition"; //$NON-NLS-1$

    /**
     * Action method name for getting the component bounds
     */
    public static final String GET_BOUNDS = "getBounds"; //$NON-NLS-1$

    /**
     * Action method name for getting the itemLabels of a list after a selection check
     */
    public static final String GET_LIST_ITEMS_LABELS_AFTER_SELECTION_CHECK = "getListItemLabelsAfterSelectionCheck"; //$NON-NLS-1$
    
    /**
     * Action method name for getting the bounds of a cell inside a table
     * component.
     */
    public static final String GET_CELL_BOUNDS = "getCellBounds"; //$NON-NLS-1$
   
    /**
     * Action method name for getting the bounds of a tab item inside a Tab
     * component.
     */
    public static final String GET_TAB_ITEM_BOUNDS = "getTabItemBounds"; //$NON-NLS-1$

    /**
     * Action method name for getting the bounds of an item inside a List
     * component.
     */
    public static final String GET_LIST_ITEM_BOUNDS = "getListItemBounds"; //$NON-NLS-1$

    /**
     * Action method name for getting the bounds of a tab item inside a Tab
     * component.
     */
    public static final String GET_SELECTED_TAB_ITEM_INDEX = "getSelectedTabItemIndex"; //$NON-NLS-1$

    /**
     * Action method name for getting enablement status of a tab item inside a 
     * Tab component.
     */
    public static final String IS_TAB_ITEM_ENABLED = "isTabItemEnabled"; //$NON-NLS-1$

    /**
     * Action method name to check the focus
     */
    public static final String HAS_FOCUS = "hasFocus"; //$NON-NLS-1$
    
    /**
     * Action method name to check the selection of a radio button
     */
    public static final String CHECK_RADIOBUTTON_SELECTION = "checkRadioButtonSelection"; //$NON-NLS-1$
    
    /**
     * Action method name to check the selection of a radio button and checkboxes
     * since the implementation differs on the dotnet server it is importend to send the
     * correct native class in the messsage
     */
    public static final String CHECK_SELECTION = "checkButtonSelection"; //$NON-NLS-1$
    
    /**
     * Action method name to check the selection of a checkbox button
     */
    public static final String CHECK_CHECKBOX_SELECTION = "checkCheckBoxSelection"; //$NON-NLS-1$
    
    /**
     * Action method name to move the mouse
     */
    public static final String MOVE_MOUSE_TO = "moveMouseToPos";
    
    /**
     * Action method name to press a button
     */
    public static final String PRESS_BUTTON = "pressButton";
    
    /**
     * Action method name to release a button
     */
    public static final String RELEASE_BUTTON = "releaseButton";
    
    /**
     * Action method name for selecting a tree item by the index path
     */
    public static final String SELECT_CONTEXT_TREEITEM_BY_INDEXPATH = "selectTreeItemByIndexpath"; //$NON-NLS-1$

    /**
     * method name for retrieving the values for a table's column headers
     */
    public static final String GET_TABLE_COL_HEADERS = "getTableColumnHeaders"; //$NON-NLS-1$
    
    /**
     * method name for retrieving the values for a table's row headers
     */
    public static final String GET_TABLE_ROW_HEADERS = "getTableRowHeaders"; //$NON-NLS-1$

    /**
     * method name for retrieving the values for a table's content cells
     */
    public static final String GET_TABLE_CONTENTS = "getTableContents"; //$NON-NLS-1$

    /**
     * method name for checking whether a table has a cell editor with focus
     */
    public static final String IS_CELL_EDITOR_FOCUSED = "isCellEditorFocused"; //$NON-NLS-1$
    
    /**
     * method name for retrieving the labels of a tab's items
     */
    public static final String GET_TAB_ITEM_LABELS = "getTabItemLabels"; //$NON-NLS-1$

    /**
     * method name for retrieving the labels of a list's items
     */
    public static final String GET_LIST_ITEM_LABELS = "getListItemLabels"; //$NON-NLS-1$

    /**
     * method name for retrieving the labels of a combo box's items
     */
    public static final String GET_COMBO_ITEM_LABELS = "getComboBoxItemLabels"; //$NON-NLS-1$

    /**
     * method name for determining whether or not a Combo Box's text field
     * is editable
     */
    public static final String IS_COMBO_EDITABLE = "isComboBoxEditable"; //$NON-NLS-1$

    /**
     * method name for retrieving the indices of all selected items in a list
     */
    public static final String GET_LIST_SELECTED_INDICES = "getListSelectedIndices"; //$NON-NLS-1$

    /**
     * method name for retrieving the index coordinates of the cell at the 
     * current mouse cursor location
     */
    public static final String GET_CELL_AT_MOUSE_CURSOR = "getCellAtMouseCursor"; //$NON-NLS-1$
    
    /**
    * Action method name of checking if a cell is editable at the current mouse position
    */
    public static final String CHECK_CELL_EDITABILITY_AT_MOUSE_POS = "isCellAtCursorPositionEditable"; //$NON-NLS-1$
    
    /**
     * method name for retrieving the index coordinates of the currently 
     * selected cell at the
     */
    public static final String GET_SELECTED_CELL = "getSelectedCell"; //$NON-NLS-1$
    
    /**
     * Method name to check if the current selected cell is editable.
     */
    public static final String CHECK_SELECTED_CELL_EDITABILITIY = "isSelectedCellEditable"; //$NON-NLS-1$
    
    /**
     * Method name to check if the given cell is editable.
     */
    public static final String IS_CELL_EDITABLE = "isCellEditable"; //$NON-NLS-1$
    
    /**
     * method name for selecting a combo box entry from its dropdown list
     */
    public static final String SELECT_COMBO_ITEM = "selectComboBoxItem"; //$NON-NLS-1$
    
    /**
     * method name for changing the selection (relative to the current 
     * selection) within a tree
     */
    public static final String MOVE_IN_TREE = "moveInTree"; //$NON-NLS-1$
    
    /** constant for mode for hierarchy actions */
    public static final int AUT_HIERARCHY = 5;


    
    
    
    /**
     * Private Constructor.
     */
    private RobotConstants() {
        
    }
    
}
