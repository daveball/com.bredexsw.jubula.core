/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.driver;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jubula.communication.internal.message.ObjectMappedMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.rc.common.AUTServerConfiguration;
import org.eclipse.jubula.tools.internal.exception.CommunicationException;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.eclipse.jubula.tools.internal.xml.businessmodell.ComponentClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.listener.ComponentHandler;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Class that takes care of the communication and commands for the object
 * mapping between the Win Server and the Java Server
 */
public class WinOMRobotImpl {
    /** <code>log</code>: the logger */
    private static Logger log = LoggerFactory.getLogger(WinOMRobotImpl.class);
    /**
     * The name of the Win server class where the OM actions are performed 
     */
    private static final String OM_CLASSNAME = "ObjectMapping";
    /**
     * Communication object.
     */
    private static Communicator com = WinAUTServer.getWinCommunicator();
    /**
     * Thread for pulling the selected items from the Win Server
     */
    private static SelectedComponentsThread sct = null;
    
    /**
     * Says if the om is still running
     */
    private static boolean isRunning = false;
    
    /**
     * Constructor with the communicator that is used for the Win OM
     * communication.
     * 
     */
    public WinOMRobotImpl() {
        sendSupportedComponents();
        sct = new SelectedComponentsThread();
        sct.start();
    }
    
    /**
     * Starts the OM-Mode on the Win Server
     */
    public void startOM() {
        focusAUT();
        NativeMessage msgObj = new NativeMessage(OM_CLASSNAME,
                RobotConstants.ROBOT_METHOD_OM_START);
        int keyModifier = AUTServerConfiguration.getInstance().getKeyMod();
        int keyCode = AUTServerConfiguration.getInstance().getMouseButton();
        if (keyCode < 0) {
            keyCode = AUTServerConfiguration.getInstance().getKey();
        }
        msgObj.addParameter(String.valueOf(keyModifier));
        msgObj.addParameter(String.valueOf(keyCode));
        try {
            com.sendMsg(msgObj);
            isRunning = true;
            ComponentHandler.rebuildHierarchy();
        } catch (RemoteServerException e) {
            log.error(e.getExceptionMsg());
        }
    }

    /**
     * Sets the AUT to the focus while starting the OM.
     */
    private void focusAUT() {
        WinAUTServer.getInstance().getRobot().activateApplication("");
    }
    
    /**
     * Stops the OM-Mode on the Win Server
     */
    public void stopOM() {
        if (isRunning) {
            NativeMessage msgObj = new NativeMessage(OM_CLASSNAME,
                    RobotConstants.ROBOT_METHOD_OM_STOP);
            try {
                com.sendMsg(msgObj);
                isRunning = false;
                sct.interrupt();
            } catch (RemoteServerException e) {
                log.error(e.getExceptionMsg());
            }
        }
    }
    
    /**
     * Send the SupportedCompoents to the Win Server
     */
    private void sendSupportedComponents() {
        NativeMessage msgObj = new NativeMessage(OM_CLASSNAME,
                RobotConstants.ROBOT_METHOD_OM_SUPPORTEDCOMPONENTS);
        final Set<ComponentClass> supportedTypes = AUTServerConfiguration
                .getInstance().getSupportedTypes();
        List<String> classNameList = new ArrayList<String>();
        
        for (ComponentClass cc : supportedTypes) {
            if (!classNameList.contains(cc.getName())) {
                classNameList.add(cc.getName());
            }
        }
        
        msgObj.addParameter(classNameList.toArray());
            
        try {
            com.sendMsg(msgObj);
            isRunning = false;
        } catch (RemoteServerException e) {
            log.error(e.getLocalizedMessage());
        }
    }
    
    /**
     * Highlight command to the Win Server
     * 
     * @param compLocator locator of the components
     * @return if component could be highlighted
     */
    public boolean highlightComponent(String compLocator) {
        NativeMessage msgObj = new NativeMessage(OM_CLASSNAME,
                "highlightComponent");
        msgObj.addParameter(compLocator);
        try {
            NativeAnswer rmo = com.sendMsg(msgObj);
            return rmo.getBoolean();
        } catch (RemoteServerException e) {
            log.error(e.getExceptionMsg());
        }
        return false;
    }
    
    /**
     * Gets the selected elements from the Win Server
     */
    private static void getSelectedElements() {
        if (isRunning) {
            NativeMessage msgObj = new NativeMessage(OM_CLASSNAME,
                    RobotConstants.ROBOT_METHOD_OM_GET_ELEMENTS);
            try {
                NativeAnswer rmo = com.sendMsg(msgObj);   
                List<String> selectedElements = rmo.getStringList();
                if (selectedElements.size() > 0) {
                    ArrayList<IComponentIdentifier> identifierList = 
                            new ArrayList<IComponentIdentifier>();
                    
                    for (String s : selectedElements) {
                        identifierList.add(ComponentHandler.getHierarchy()
                                .getComponentIdentifier(s));
                    }

                    ObjectMappedMessage message = new ObjectMappedMessage();
                    message.setComponentIdentifiers(identifierList
                            .toArray(new IComponentIdentifier[identifierList
                                    .size()]));
                    try {
                        AUTServer.getInstance().getCommunicator().send(message);
                    } catch (CommunicationException e) {
                        log.error(e.getLocalizedMessage(), e);
                    }
                }
                
            } catch (RemoteServerException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    /**
     * Thread that pulls every two seconds the selected elements list
     * from the Win Server
     * 
     * @author patrick
     *
     */
    public class SelectedComponentsThread extends Thread {
        
        /**
         * Time in ms between the aut checks.
         */
        private static final int WAITTIME = 2000;
        
        /**
         * run method
         */
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    WinOMRobotImpl.getSelectedElements();
                    Thread.sleep(WAITTIME);
                } catch (InterruptedException e) {
                    // Expected if the user clicks on Stop ObjectMapping
                } catch (ThreadDeath e) {
                    e.printStackTrace();
                }
                
            }
        }
    }
    
    
    
    
    
    
    
}
