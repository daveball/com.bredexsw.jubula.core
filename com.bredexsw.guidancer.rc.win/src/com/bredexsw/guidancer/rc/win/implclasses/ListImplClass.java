package com.bredexsw.guidancer.rc.win.implclasses;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.DragAndDropHelper;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.ListSelectionVerifier;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.toolkit.enums.ValueSets.SearchType;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.constants.TestDataConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.StringParsing;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.List;
import com.bredexsw.guidancer.rc.win.driver.KeyCodeConverter;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * Implementation (tester) class for List.
 */
public abstract class ListImplClass extends AbstractWinImplClass {

    /**
     * 
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        // not yet implemented
        return null;
    }

    /**
     * @return the list component
     */
    public abstract List getList();
    
    /**
     * Verifies if the passed index is selected.
     * 
     * @param index The index to verify
     * @param expectSelected Whether the index should be selected.
     */
    public void rcVerifySelectedIndex(
            String index, boolean expectSelected) {
        
        try {
            int[] selected = getRobot().getListSelectedIndices(getList());
            int implIndex = IndexConverter.toImplementationIndex(
                    Integer.parseInt(index));

            boolean isSelected = ArrayUtils.contains(selected, implIndex);
            if (expectSelected != isSelected) {
                throw new StepExecutionException(
                        "Selection check failed for index: " + index,  //$NON-NLS-1$
                        EventFactory.createVerifyFailed(
                                String.valueOf(expectSelected), 
                                String.valueOf(isSelected)));
            }
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
        
    }

    /**
     * Verifies if the passed value is selected.
     * 
     * @param value The value to verify
     * @param operator The operator to use when comparing the expected and 
     *                 actual values.
     *  @param expectSelected Whether the value should be selected.
     */
    public void rcVerifySelectedValue(String value, String operator,
            boolean expectSelected) {

        try {
            final int[] selectedIndices = 
                    getRobot().getListSelectedIndices(getList());
            
            final ListSelectionVerifier listSelVerifier =
                    new ListSelectionVerifier();
            for (int i = 0; i < selectedIndices.length; i++) {
                listSelVerifier.addItem(i, 
                        getList().getItemLabelsforSelectionCheck()
                        [selectedIndices[i]], true);
            }
            listSelVerifier.verifySelection(
                    value, operator, expectSelected);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Checks whether the list contains an element that matches 
     * <code>value</code>.
     * @param value The text to verify
     * @param operator The operator used to verify
     * @param exists if the wanted value should exist or not.
     */
    public void rcVerifyContainsValue(String value, String operator,
            boolean exists) {

        try {
            Verifier.equals(exists, containsValue(value, operator));
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * @param value
     *            The value
    * @param operator
    *            The operator used to verify
     * @return <code>true</code> if the list contains an element that is
     *         rendered with <code>value</code>
     * @throws RemoteServerException 
     */
    private boolean containsValue(String value, String operator) 
        throws RemoteServerException {
        
        Integer[] indices = null;
        if (operator.equals(MatchUtil.NOT_EQUALS)) {
            indices = findIndicesOfValues(new String[] { value },
                MatchUtil.EQUALS);
            return indices.length == 0;
        }
        indices = findIndicesOfValues(new String[] { value },
            operator);
        return indices.length > 0;
    }

    /**
     * Finds the indices of the list elements that are rendered with the passed
     * values.
     *
     * @param values
     *            The values
     * @param operator
     *            operator to use
     * @return The array of indices. It's length is equal to the length of the
     *         values array, but may contains <code>null</code> elements for
     *         all values that are not found in the list
     * @throws RemoteServerException 
     */
    private Integer[] findIndicesOfValues(
            final String[] values, final String  operator) 
        throws RemoteServerException {

        return findIndicesOfValues(values, operator,
                SearchType.absolute.rcValue());
    }

    /**
     * Finds the indices of the list elements that are rendered with the passed
     * values.
     *
     * @param values
     *            The values
     * @param operator
     *            operator to use
     * @param searchType
     *            Determines where the search begins ("relative" or "absolute")
     * @return The array of indices. It's length is equal to the length of the
     *         values array, but may contains <code>null</code> elements for
     *         all values that are not found in the list
     * @throws RemoteServerException 
     */
    public Integer[] findIndicesOfValues(final String[] values, 
            final String operator, final String searchType) 
        throws RemoteServerException {

        final Set<Integer> indexSet = new HashSet<Integer>();
        String[] items = getList().getItemLabels();
        for (int i = getStartingIndex(searchType); i < items.length; ++i) {
            if (MatchUtil.getInstance().
                    match(items[i], values, operator)) {
                indexSet.add(new Integer(i));
            }
        }   

        Integer[] indices = new Integer[indexSet.size()];
        indexSet.toArray(indices);
        return indices;
    }

    /**
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @return The index from which to begin a search, based on the search type
     *         and (if appropriate) the currently selected cell.
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    private int getStartingIndex(final String searchType) 
        throws RobotException, RemoteServerException {
        
        int startingIndex = 0;
        if (searchType.equalsIgnoreCase(SearchType.relative.rcValue())) {
            int [] selectedIndices = 
                    getRobot().getListSelectedIndices(getList());
            // Start from the last selected item
            startingIndex = selectedIndices[selectedIndices.length - 1] + 1;
        }
        return startingIndex;
    }

    /**
     * Selects the passed value or enumeration of values. By default, the
     * enumeration separator is <code>,</code>, but may be changed by
     * <code>separator</code>.
     * @param valueList The value or list of values to select
     * @param operator If regular expressions are used
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param extendSelection Whether this selection extends a previous 
     *                        selection.
     * @param button what mouse button should be used
     * @param clickCount the click count
     */
    public void rcSelectValue(String valueList, String operator, 
            final String searchType, final String extendSelection, int button, 
            int clickCount) {
        String[] values = null;
        final boolean isExtendSelection = 
            extendSelection.equals(ValueSets.BinaryChoice.yes.rcValue());
        if (StringConstants.EMPTY.equals(valueList)) {
            values = new String[1];
            values[0] = StringConstants.EMPTY;
        } else {
            values = split(valueList, null);
        }
        try {
            Integer[] indices = 
                    findIndicesOfValues(values, operator, searchType);
            Arrays.sort(indices);
            if (!operator.equals(MatchUtil.NOT_EQUALS) 
                    && (indices.length < values.length)) {
                throw new StepExecutionException("One or more values not found of set: " //$NON-NLS-1$
                    + Arrays.asList(values).toString(),
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
            }
            selectIndices(
                    ArrayUtils.toPrimitive(indices), 
                    ClickOptions.create().setMouseButton(button)
                        .setClickCount(clickCount), 
                    isExtendSelection);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Selects the passed index or enumeration of indices. The enumeration must
     * be separated by <code>,</code>, e.g. <code>1, 3,6</code>.
     * @param indexList The index or indices to select
     * @param extendSelection Whether this selection extends a previous 
     *                        selection.
     * @param button what mouse button should be used
     * @param clickCount the click count
     */
    public void rcSelectIndex(String indexList, final String extendSelection,
            int button, int clickCount) {
        final boolean isExtendSelection = extendSelection
                .equals(ValueSets.BinaryChoice.yes.rcValue());
        selectIndices(IndexConverter
                .toImplementationIndices(parseIndices(indexList)), ClickOptions
                .create().setClickCount(clickCount).setMouseButton(button),
                isExtendSelection);
    }

    /**
     * @param indices The indices to select
     * @param co the click options to use
     * @param isExtendSelection Whether this selection extends a previous 
     *                          selection. If <code>true</code>, the first 
     *                          element will be selected with CONTROL as a 
     *                          modifier.
     */
    private void selectIndices(int[] indices, ClickOptions co, 
            boolean isExtendSelection) {
      
        int extendSelectionKeycode = KeyCodeConverter.getKeyCode(
                ValueSets.Modifier.control.rcValue());
        
        try {
            if (indices.length > 0) {
                try {
                    if (isExtendSelection) {
                        getRobot().keyPress(
                                getList(), extendSelectionKeycode);
                    }
    
                    // first selection
                    getRobot().click(getList(),
                            getRobot().getListItemBounds(
                                    getList(), indices[0]), 
                            co);
                } finally {
                    if (isExtendSelection) {
                        getRobot().keyRelease(
                                getList(), extendSelectionKeycode);
                    }
                }
            }
            // following selections
            for (int i = 1; i < indices.length; i++) {
                try {
                    getRobot().keyPress(getList(), extendSelectionKeycode);
                    getRobot().click(getList(),
                        getRobot().getListItemBounds(
                                getList(), indices[i]), 
                        co);
                } finally {
                    getRobot().keyRelease(getList(), extendSelectionKeycode);
                }
            }
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Parses the enumeration of indices.
     * @param indexList The enumeration of indices
     * @return The array of parsed indices
     */
    private int[] parseIndices(String indexList) {
        String[] list = StringParsing.splitToArray(indexList,
                TestDataConstants.VALUE_CHAR_DEFAULT, 
                TestDataConstants.ESCAPE_CHAR_DEFAULT);
        int[] indices = new int[list.length];
        for (int i = 0; i < list.length; i++) {
            indices[i] = IndexConverter.intValue(list[i]);
        }

        return indices;
    }

    /**
     * Splits the enumeration of values.
     * @param values The values to split
     * @param separator The separator, may be <code>null</code>
     * @return The array of values
     */
    private String[] split(String values, String separator) {
        String[] list = StringParsing.splitToArray(values, ((separator == null)
            || (separator.length() == 0) ? TestDataConstants.VALUE_CHAR_DEFAULT
                : separator.charAt(0)),
            TestDataConstants.ESCAPE_CHAR_DEFAULT);
        list = StringUtils.stripAll(list);
        return list;
    }

    /**
     * Action to read the value of the current selected item of the List
     * to store it in a variable in the Client
     * @param variable the name of the variable
     * @return the text value.
     * @throws StepExecutionException if no items in the List are selected.
     */
    public String rcReadValue(String variable) throws StepExecutionException {
        return getSelectedValue();
    }

    /**
     * Verifies if the (first) selected element of a list matches a text.
     * @param text The text to verify
     * @param operator The operator used to verify
     * @throws StepExecutionException if no items in the list are selected or
     *          if the first selected element does not match the given 
     *          arguments.
     */
    public void rcVerifyText(String text, String operator) 
        throws StepExecutionException {
        
        Verifier.match(getSelectedValue(), text, operator);
    }
    
    /**
     * 
     * @return the text of the (first) selected element in the List.
     * @throws StepExecutionException if no items in the List are selected.
     */
    private String getSelectedValue() throws StepExecutionException {
        try {
            final int[] selectedIndices = 
                    getRobot().getListSelectedIndices(getList());

            if (selectedIndices.length > 0) {
                return getList().getItemLabels()[selectedIndices[0]];
            }

            throw new StepExecutionException("No list item selected", //$NON-NLS-1$
                    EventFactory.createActionError(
                            TestErrorEvent.NO_SELECTION));

        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }
    
    /**
     * Drags the passed index.
     *
     * @param mouseButton the mouseButton.
     * @param modifier the modifier.
     * @param index The index to drag
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcDragIndex(final int mouseButton, final String modifier,
            int index) throws RobotException, RemoteServerException {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setMouseButton(mouseButton);
        dndHelper.setModifier(modifier);
        
        rcSelectIndex(Integer.toString(index), "no", 0, 1);
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);     
    }
    
    /**
     * Drags the passed value.
     *
     * @param mouseButton the mouseButton.
     * @param modifier the modifier.
     * @param value The value to drag
     * @param operator If regular expressions are used
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public void rcDragValue(int mouseButton, String modifier, String value,
            String operator, final String searchType) throws
            RemoteServerException {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setMouseButton(mouseButton);
        dndHelper.setModifier(modifier);
        
        String[] values = {value};
        Integer[] indices = findIndicesOfValues(values, operator, searchType);
        Arrays.sort(indices);
        
        rcSelectIndex(Integer.toString(indices[0] + 1), "no", 0, 1);
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);            
    }
    
    /**
     * Drops onto the passed index.
     *
     * @param index The index on which to drop
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    public void rcDropIndex(final int index, int delayBeforeDrop) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        final String modifier = dndHelper.getModifier();
        final int mouseButton = dndHelper.getMouseButton();
        
        rcSelectIndex(Integer.toString(index), "no", 0, 1);
        waitBeforeDrop(delayBeforeDrop);
        pressOrReleaseModifiers(modifier, false);
        getRobot().mouseRelease(null, null, mouseButton);
    }
    
    /**
     * Drops on the passed value.
     *
     * @param value The value on which to drop
     * @param operator If regular expressions are used
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     * @throws RemoteServerException the {@link RemoteServerException} 
     */
    public void rcDropValue(String value, String operator,
        final String searchType, int delayBeforeDrop) throws 
        RemoteServerException {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        final String modifier = dndHelper.getModifier();
        final int mouseButton = dndHelper.getMouseButton();
        
        String[] values = {value};
        Integer[] indices = findIndicesOfValues(values, operator, searchType);
        Arrays.sort(indices);
        
        rcSelectIndex(Integer.toString(indices[0] + 1), "no", 0, 1);
        waitBeforeDrop(delayBeforeDrop);
        pressOrReleaseModifiers(modifier, false);
        getRobot().mouseRelease(null, null, mouseButton);        
    }
}