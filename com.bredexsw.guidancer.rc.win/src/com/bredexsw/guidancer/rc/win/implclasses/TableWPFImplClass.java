package com.bredexsw.guidancer.rc.win.implclasses;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.TableWPF;

/**
 * ImplClass for WPF-Tables / data grids
 */
public class TableWPFImplClass extends TableImplClass {

    @Override
    TableWPF getTable() {
        return (TableWPF) getComponent();
    }

}
