/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;

import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Interface for all Win components which contains text.
 * No Impl class at the moment because all components need a different 
 * implication.
 */
public interface IComponentWithTextInputImpl extends IComponentWithTextImpl {

    /**
     * Types <code>text</code> into the component.
     *
     * @param text the text to type in
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcInputText(String text) throws RobotException, 
    RemoteServerException;
    
    /**
     * Types <code>text</code> into the component. This replaces the shown
     * content.
     *
     * @param text the text to type in
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcReplaceText(String text) throws RobotException, 
    RemoteServerException;
    
    /**
     * Insert text to a given index position
     * 
     * @param text the text to insert
     * @param index the index position
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public void rcInsertText(String text, int index) throws RobotException,
    RemoteServerException;
    
    /**
    * Selects the text of a component with text input
    *  
    * @throws RobotException the {@link RobotException}
    * @throws RemoteServerException the {@link RemoteServerException}
    */
    public void rcSelect() throws RobotException,
    RemoteServerException;  
    
    /**
     * Inserts <code>text</code> before or after the first appearance of
     * <code>pattern</code>.
     *
     * @param text The text to insert
     * @param pattern The pattern to find the position for insertion
     * @param operator Operator to select Matching Algorithm
     * @param after
     *            If <code>true</code>, the text will be inserted after the
     *            pattern, otherwise before the pattern.
     * @throws StepExecutionException
     *             If the pattern is invalid or cannot be found
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public void rcInsertText(String text, String pattern,
            String operator, boolean after) throws RobotException,
            RemoteServerException, StepExecutionException;  
    
    /**
     * Selects the first (not)appearance of <code>pattern</code> in the text
     * component's content.
     *
     * @param pattern The pattern to select
     * @param operator operator
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcSelect(final String pattern, String operator) throws 
    RobotException, RemoteServerException;
}
