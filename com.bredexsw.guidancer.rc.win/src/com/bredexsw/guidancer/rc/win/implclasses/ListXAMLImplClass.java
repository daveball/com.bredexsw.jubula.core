package com.bredexsw.guidancer.rc.win.implclasses;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.ListXAML;

/**
 * Impl class for winapps xaml implementations  
 */
public class ListXAMLImplClass extends ListImplClass {

    @Override
    public ListXAML getList() {
        return (ListXAML) getComponent();
    }
}
