package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.i18n.I18n;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Tab;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * Implementation (tester) class for Tab.
 */
public class TabImplClass extends AbstractWinImplClass {

    /** the logger */
    private static final Logger LOG = 
            LoggerFactory.getLogger(TabImplClass.class);
    
    /**
     * 
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        try {
            return getTab().getItemLabels();
        } catch (RemoteServerException rse) {
            LOG.error("Could not get descriptive text for component.", rse); //$NON-NLS-1$
            return null;
        }
    }

    /**
     * 
     * @return the component handled by the receiver, as a Table.
     */
    private Tab getTab() {
        return (Tab)getComponent();
    }
    
    /**
     * Verifies existence of tab by index/value
     *
     * @param tab index/value of tab
     * @param operator Operator to be executed
     * @param exists boolean, tab exists
     * @throws StepExecutionException if tab does not exist.
     */
    public void rcVerifyExistenceOfTab(
            String tab, String operator, boolean exists)
        throws StepExecutionException {
        
        try {
            final int tabIdx = parseTabIndex(tab, operator);
            verifyExistenceOfTabByImplIndex(tabIdx, exists);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies existence of tab by index.
     *
     * @param index Index of tab
     * @param exists Whether the tab is expected to exist. 
     * @throws StepExecutionException 
     *              if actual tab existence does not meet the expected value 
     *              defined in <code>exists</code>.
     */
    private void verifyExistenceOfTabByImplIndex(int index, boolean exists) 
        throws StepExecutionException {
        
        try {
            boolean tabExists = 
                    index >= 0 && index < getTab().getItemLabels().length;
            Verifier.equals(exists, tabExists);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }
    
    /**
     * 
     * @param tab index or title of tab
     * @param operator Operator to be executed
     * @return returns index of tab if exists, -1 otherwise
     * @throws RemoteServerException 
     */
    private int parseTabIndex(String tab, String operator) 
        throws RemoteServerException {
        
        try {
            return IndexConverter.toImplementationIndex(
                    Integer.parseInt(tab));
        } catch (NumberFormatException nfe) {
            return getTabIndex(tab, operator);
        }
    }
    
    /**
     * 
     * @param tab title of tab
     * @param operator Operator to be executed
     * @return returns index of tab if exists, -1 otherwise
     * @throws RemoteServerException 
     */
    private int getTabIndex(String tab, String operator)
        throws RemoteServerException {
        
        String[] itemLabels = getTab().getItemLabels();
        for (int i = 0; i < itemLabels.length; i++) {
            String text = itemLabels[i];
            if (MatchUtil.getInstance().match(text, tab, operator)) {
                return i;
            }
        }
        
        return -1;
    }
    
    /**
     * Selects the tab with the passed index.
     * The method doesn't care if the tab is enabled or not.
     *
     * @param index
     *            The tab index
     * @throws StepExecutionException
     *             If the tab index is invalid.
     */
    public void rcSelectTabByIndex(int index)
        throws StepExecutionException {

        int implIdx = IndexConverter.toImplementationIndex(index);

        try {
            selectTabByImplIndex(implIdx);
        } catch (RobotException re) {
            throw new StepExecutionException(re);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Selects the tab with the passed title. The method doesn't care if the tab
     * is enabled or not.
     *
     * @param title
     *            The tab title
     * @param operator
     *      using regex
     * @throws StepExecutionException
     *             If the tab title is invalid.
     */
    public void rcSelectTab(final String title, String operator)
        throws StepExecutionException {

        try {
            selectTabByImplIndex(getTabIndex(title, operator));
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }

    }
    
    /**
     * Selects the tab with the passed index. The method doesn't care if the tab is enabled or not.
     * @param index The tab index
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    private void selectTabByImplIndex(int index) 
        throws RobotException, RemoteServerException {
        
        verifyExistenceOfTabByImplIndex(index, true);

        // FIXME zeb: We currently ignore the possibility of needing to scroll
        //            or use a pulldown menu to find the tab item. This means
        //            that the user must know when this type of action is
        //            necessary and specify their tests accordingly. We may wish
        //            to change this later so that it is "smarter" (i.e. can
        //            scroll or use a pulldown menu to find tab items in a crowded
        //            tab folder).

        getRobot().click(getTab(), 
                getRobot().getTabItemBounds(getTab(), index), 
                ClickOptions.create().left(), 50, false, 50, false);
    }

    /**
     * Verifies the selection of the tab with the passed title.
     *
     * @param tabTitlePattern
     *            The tab title pattern to use for checking
     * @param operator
     *            Operator to be executed
     * @param selected
     *            Should the tab be selected?
     * @throws StepExecutionException
     *             If the tab title is invalid.
     */
    public void rcVerifySelectedTab(String tabTitlePattern, String operator,
            boolean selected)
        throws StepExecutionException {
        
        try {
            int selectedTabIndex = 
                    getRobot().getSelectedTabItemIndex(getTab());
            String selectedTabTitle = 
                    getTab().getItemLabels()[selectedTabIndex];
            if (selectedTabTitle == null) {
                if (!selected) {
                    return;
                }
                throw new StepExecutionException(
                        I18n.getString(TestErrorEvent.NO_SELECTION),
                        EventFactory.createActionError(
                                TestErrorEvent.NO_SELECTION));
            }
            Verifier.match(
                    selectedTabTitle, tabTitlePattern, operator, selected);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies the selection of the tab with the passed index.
     *
     * @param index
     *            The tab index
     * @param selected
     *            Should the tab be selected?
     * @throws StepExecutionException
     *             If the tab index is invalid.
     */
    public void rcVerifySelectedTabByIndex(int index, boolean selected)
        throws StepExecutionException {
        int implIdx = IndexConverter.toImplementationIndex(index);
        try {
            int selIndex = 
                    getRobot().getSelectedTabItemIndex(getTab());
            
            if (selIndex == -1) {
                if (!selected) {
                    return;
                }
                throw new StepExecutionException(
                        I18n.getString(TestErrorEvent.NO_SELECTION),
                        EventFactory.createActionError(
                                TestErrorEvent.NO_SELECTION));
            }
            
            Verifier.equals(selected, selIndex == implIdx);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies if the tab with the passed title is enabled.
     *
     * @param title The tab title
     * @param operator operation to be executed
     * @param enabled whether the tab item is expected to be enabled
     * @throws StepExecutionException
     *             If the tab title is invalid or the enablement of the tab
     *             with the given title does not match the expected enablement
     *             value.
     */
    public void rcVerifyEnabled(String title, String operator,
        final boolean enabled) throws StepExecutionException {
        
        try {
            verifyEnabledByImplIndex(getTabIndex(title, operator), enabled);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies if the tab with the passed index is enabled.
     *
     * @param index
     *            The tab index
     * @param enabled whether the tab item is expected to be enabled
     * @throws StepExecutionException
     *             If the tab index is invalid or the enablement of the tab
     *             at the given index does not match the expected enablement
     *             value.
     */
    public void rcVerifyEnabledByIndex(int index, boolean enabled)
        throws StepExecutionException {
        
        verifyEnabledByImplIndex(
                IndexConverter.toImplementationIndex(index), 
                enabled);
    }
    
    /**
     * Verifies if the tab item with the passed 0-based index is enabled.
     *
     * @param implIndex
     *            The 0-based tab item index
     * @param enabled whether the tab item is expected to be enabled
     * @throws StepExecutionException
     *             If the tab index is invalid or the enablement of the tab
     *             at the given index does not match the expected enablement
     *             value.
     */
    private void verifyEnabledByImplIndex(int implIndex, boolean enabled) 
        throws StepExecutionException {
        
        try {
            verifyExistenceOfTabByImplIndex(implIndex, true);
            Verifier.equals(enabled, 
                    getRobot().isTabItemEnabled(getTab(), implIndex));
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies the text of the tab by index
     *
     * @param index index of tab
     * @param text The tab title
     * @param operator Operator to be executed
     * @throws StepExecutionException
     *             If the tab index is invalid or the tab at the given index
     *             does not match the expected pattern.
     */
    public void rcVerifyTextOfTabByIndex(final int index, final String text,
            final String operator) throws StepExecutionException {        
        
        final int tabIndex = IndexConverter.toImplementationIndex(index);
        verifyExistenceOfTabByImplIndex(tabIndex, true);

        try {
            String tabTitle = getTab().getItemLabels()[tabIndex];
            Verifier.match(tabTitle, text, operator);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }
    
}
