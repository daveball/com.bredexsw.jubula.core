/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import java.awt.Rectangle;

import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.RobotConfiguration;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.implclasses.AbstractApplicationImplClass;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Application;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Impl class for applications
 */
public class WinApplicationImplClass extends AbstractApplicationImplClass {

    /**
     * Communication robot for the test steps.
     */
    private WinRobotImpl m_robot;
    
    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Rectangle getActiveWindowBounds() {
        try {
            return getRobot().getActiveWindowBounds(
                    Application.getNativeClassName());
        } catch (RobotException e) {
            // do nothing
        } catch (RemoteServerException e) {
            // do nothing
        }
        return null;
    }   
    
    /**
     * @param text text to type
     * @throws RemoteServerException  the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcInputText(String text) throws RobotException, 
    RemoteServerException {
        getRobot().inputText(text);
    }
    
    /**
    * Press the key combination 
    *  
    * @param modifierSpec the string representation of the modifiers
    * @param keySpec the string representation of the key
    */
    public void rcKeyStroke(String modifierSpec, String keySpec) {
       //no check mechanism for Win implemented, therefore we use the given
       // abstract application method
        rcNativeKeyStroke(modifierSpec, keySpec);
    }
    
   
    /**
     * @return the robot
     */
    protected WinRobotImpl getRobot() {
        if (m_robot == null) {
            m_robot = (WinRobotImpl) WinAUTServer.getInstance().getRobot(); 
        }
        return m_robot;
    }
    
    /**
     * activate the AUT
     * 
     * @param method activation method
     */
    public void rcActivate(String method) {
        String methodID = method;
        if (methodID.equals(("AUT_DEFAULT"))) {
            methodID = RobotConfiguration.getInstance().
                    getDefaultActivationMethod();
        }
        
        getRobot().activateApplication(method);
    }
    
    /**
     * Waits <code>timeMillSec</code> if the application opens a window
     * with the given title.
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param timeout the time in ms
     * @param delay delay after the window is shown
     */
    public void rcWaitForWindow(final String title, String operator, 
        int timeout, int delay) {
        int remainingTime = timeout;
        
        while (remainingTime > 0 && !isWindowOpen(title, operator)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                // do nothing
            }
            remainingTime -= 100;
        }
        
        if (!isWindowOpen(title, operator)) {
            throw new StepExecutionException("window did not open", //$NON-NLS-1$
                EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        try {
            Thread.sleep(delay);
        } catch (InterruptedException exc) {
            // do nothing
        }
    }
    
    /**
     * Waits <code>timeMillSec</code> if the application activates a window
     * with the given title.
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param pTimeout the time in ms
     * @param delay delay after the window is activated
     */
    public void rcWaitForWindowActivation(final String title, String operator, 
        int pTimeout, int delay) {
        int remainingTime = pTimeout;
        
        while (remainingTime > 0 && !isWindowActive(title, operator)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                // do nothing
            }
            remainingTime -= 100;
        }
        
        if (!isWindowActive(title, operator)) {
            throw new StepExecutionException("window was not activated", //$NON-NLS-1$
                    EventFactory.createActionError(
                            TestErrorEvent.TIMEOUT_EXPIRED));
        }
        try {
            Thread.sleep(delay);
        } catch (InterruptedException exc) {
            // do nothing
        }
    }
    
    /**
     * Waits <code>timeMillSec</code> if the application closes (or hides) 
     * a window with the given title. If no window with the given title can
     * be found, then it is assumed that the window has already closed.
     * 
     * @param title the title
     * @param operator the comparing operator
     * @param pTimeout the time in ms
     * @param delay delay after the window is closed
     */
    public void rcWaitForWindowToClose(final String title, String operator, 
        int pTimeout, int delay) {
        int remainingTime = pTimeout;
        
        while (remainingTime > 0 && isWindowOpen(title, operator)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                // do nothing
            }
            remainingTime -= 100;
        }
        
        if (isWindowOpen(title, operator)) {
            throw new StepExecutionException("window did not close", //$NON-NLS-1$
                    EventFactory.createActionError(
                            TestErrorEvent.TIMEOUT_EXPIRED));
        }
        try {
            Thread.sleep(delay);
        } catch (InterruptedException exc) {
            // do nothing
        }
    }
    
    /**
     * Toggles keys
     * 
     * @param key to set
     *      numlock Num Lock 1
     *      caplock Caps Lock 2 
     *      scrolllock Scroll 3
     * @param activated 
     *      boolean
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcToggle(int key, boolean activated) throws RobotException, 
    RemoteServerException {
        getRobot().toggleKey(key, activated);
    }
    
    /**
     * Checks for the existence of a window with the given title
     * 
     * @param title
     *            the title
     * @param operator
     *            the comparing operator
     * @param exists
     *            <code>True</code> if the window is expected to exist and be
     *            visible, otherwise <code>false</code>.
     */
    public void rcCheckExistenceOfWindow(final String title, String operator,
            boolean exists) {
        Verifier.equals(exists, isWindowOpen(title, operator));
    }
    
    /**
     * Returns <code>true</code> if a window with the given title is open and 
     * visible
     * 
     * @param title the title
     * @param operator the matches/equals operator
     * @return if the window is open and visible
     */
    private boolean isWindowOpen(final String title, final String operator) {
        String[] windows = getRobot().getAllWindowTitles(
                Application.getNativeClassName());
        
        for (String window : windows) {
            if (MatchUtil.getInstance().match(window, title, operator)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns <code>true</code> if a window with the given title has focus
     * 
     * @param title the title
     * @param operator the matches/equals operator
     * @return if the window has focus
     */
    private boolean isWindowActive(String title, String operator) {
        String activeWindowName = getRobot().getActiveWindowTitle(
                Application.getNativeClassName());
        if (MatchUtil.getInstance().match(activeWindowName, title, operator)) {
            return true;
        } 
        return false;
    }
    
    /**
     * clicks into the active window.
     * 
     * @param count amount of clicks
     * @param button what mouse button should be used
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @throws StepExecutionException error
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    public void rcClickDirect(int count, int button, 
        int xPos, String xUnits, int yPos, String yUnits) 
        throws StepExecutionException, RobotException, RemoteServerException {
        
        Rectangle bounds = getRobot().getActiveWindowBounds(
                Application.getNativeClassName());
        getRobot().click(null, bounds, 
                ClickOptions.create()
                    .setClickCount(count)
                    .setMouseButton(button),
                    xPos, 
                xUnits.equalsIgnoreCase(AbstractWinImplClass.POS_UNIT_PIXEL),
                yPos, 
                yUnits.equalsIgnoreCase(
                        AbstractWinImplClass.POS_UNIT_PIXEL));
    }
    
    /**
     * Does nothing! The prepare for shutdown is implemented in the client but
     * the server must have an action to execute.
     */
    public void rcPrepareForShutdown() {
        // nothing
    }
    
    /**
     * Does nothing! The sync shutdown and restart is implemented in the client
     * but the server must have an action to execute.
     * 
     * @param timeout
     *            the timeout to use
     */
    public void rcSyncShutdownAndRestart(int timeout) {
        // nothing
    }
}
