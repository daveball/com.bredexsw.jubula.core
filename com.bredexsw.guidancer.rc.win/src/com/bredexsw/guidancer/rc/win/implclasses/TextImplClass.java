/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.exception.StepVerifyFailedException;
import org.eclipse.jubula.rc.common.util.Verifier;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Text;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Class for testing text components like labels
 */
public class TextImplClass extends AbstractWinImplClass implements
    IComponentWithTextImpl {

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }
    
     
    /** 
     * {@inheritDoc} 
     * 
     * @throws RemoteServerException 
     * @throws StepExecutionException 
     * @throws StepVerifyFailedException */
    public void rcVerifyText(String text, String operator) throws
    //using getName because the automation controlType.text is using the name as the label text
    StepVerifyFailedException, StepExecutionException, RemoteServerException {
        Verifier.match(getRobot().getName(getText().getNativeClassName(),
                getText().getLocator()),
                text, operator);
    }
    
    /**
     * {@inheritDoc}
     */
    public String rcReadValue(String variable) throws RemoteServerException {
        return getRobot().getName(getText().getNativeClassName(),
                getText().getLocator());
    }
    
    /**
     * @return the native text class
     */
    public Text getText() {
        return (Text) getComponent();
    }

}
