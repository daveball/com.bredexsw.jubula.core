package com.bredexsw.guidancer.rc.win.implclasses;

import javax.naming.OperationNotSupportedException;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.toolkit.enums.ValueSets.SearchType;
import org.eclipse.jubula.tools.internal.i18n.I18n;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.ComboBox;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * Implementation (tester) class for Combo Box.
 */
public class ComboBoxImplClass extends ComponentWithTextInputImplClass {

    /**
     * 
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        // not yet implemented
        return null;
    }

    /**
     * 
     * @return the component handled by the receiver, as a Combo Box.
     */
    private ComboBox getComboBox() {
        return (ComboBox)getComponent();
    }
    
    /**
     * Verifies if the list contains an element that renderes <code>value</code>.
     * @param value The text to verify
     * @param operator The operator used to verify
     * @param exists If the value should exist or not.
     */
    public void rcVerifyContainsValue(String value, String operator,
            boolean exists) {

        try {
            final boolean contains = 
                    findFirstIndexOfValue(value, operator) != -1;
            Verifier.equals(exists, contains);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Selects <code>index</code> in the combobox.
     *
     * @param index
     *            The index to select
     */
    public void rcSelectIndex(String index) {
        int implIdx = IndexConverter.toImplementationIndex(
                IndexConverter.intValue(index));
        try {
            getRobot().selectComboBoxItem(getComboBox(), implIdx);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Finds the index of the first combo box element that is rendered with the 
     * passed value.
     *
     * @param value
     *            The value
     * @param operator
     *            operator to use
     * @return the index of the first combo box element that matches the search
     *         criteria, or <code>-1</code> if no such element could be found.
     * @throws RemoteServerException 
     */
    private int findFirstIndexOfValue(final String value, 
            final String operator) throws RemoteServerException {

        String[] items = getComboBox().getItemLabels();
        for (int i = 0; i < items.length; ++i) {
            if (MatchUtil.getInstance().
                    match(items[i], value, operator)) {
                return i;
            }
        }   

        return -1;
    }

    /**
     * Verifies if the combobox has <code>index</code> selected.
     *
     * @param index The index to verify
     * @param isSelected If the index should be selected or not.
     * @throws OperationNotSupportedException 
     *              always, because this operation is not supported.
     */
    public void rcVerifySelectedIndex(String index, boolean isSelected) 
        throws OperationNotSupportedException {
        
        throw new OperationNotSupportedException();
    }

    /**
     * Selects a value from the list of the combobox
     * @param value The value to select
     * @param operator if regular expressions are used
     * @param searchType Determines where the search begins ("relative" or 
     *                   "absolute"). Only "absolute" is supported.
     * @throws StepExecutionException
     *              if any value other than "absolute" is provided for 
     *              <code>searchType</code>.
     */
    public void rcSelectValue(String value, String operator,
        final String searchType) throws StepExecutionException {

        if (!SearchType.absolute.rcValue().equals(searchType)) {
            String[] i18nParameters = new String[] {searchType};
            throw new StepExecutionException(
                    I18n.getString(
                            TestErrorEvent.UNSUPPORTED_PARAMETER_VALUE, 
                            i18nParameters), 
                    EventFactory.createConfigErrorEvent(
                            TestErrorEvent.UNSUPPORTED_PARAMETER_VALUE, 
                            i18nParameters));
        }

        try {
            rcSelectIndex(String.valueOf(
                    IndexConverter.toUserIndex(
                            findFirstIndexOfValue(value, operator))));
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }

    }

    /**
     * Verifies if the passed text is currently selected in the combobox.
     *
     * @param text
     *            The text to verify.
     * @param operator
     *            The operator used to verify
     */
    public void rcVerifyText(String text, String operator) {
        try {
            Verifier.match(getComboBox().getText(), text, operator);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Action to read the value of a Combo Box to store in a variable
     * in the Client.
     * @param variable the name of the variable
     * @return the text value.
     */
    public String rcReadValue(String variable) {
        try {
            return getComboBox().getText();
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies the editable property.
     *
     * @param editable
     *            The editable property to verify.
     */
    public void rcVerifyEditable(boolean editable) {
        try {
            Verifier.equals(editable, getComboBox().isEditable());
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    @Override
    public ComboBox getTextInputComponent() {
        return (ComboBox)getComponent();
    }
}
