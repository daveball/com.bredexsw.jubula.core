/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.util.Verifier;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.CheckBox;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * Impl class for checkboxes
 */
public class CheckBoxImplClass extends AbstractButtonImplClass {
    
    /**
     *  {@inheritDoc}
     */
    @Override
    public void rcVerifySelected(boolean selected) throws RobotException, 
    RemoteServerException {
        
        boolean checkResult =  getRobot().checkCheckBoxSelection(
                getButton().getNativeClassName(),
                getButton().getLocator());
        
        Verifier.equals(selected, checkResult);
    }

    @Override
    public CheckBox getButton() {
        return (CheckBox) getComponent();
    }
}
