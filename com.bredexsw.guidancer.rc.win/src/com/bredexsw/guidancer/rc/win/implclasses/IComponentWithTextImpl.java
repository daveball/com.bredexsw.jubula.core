/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.exception.StepVerifyFailedException;

import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Interface for all Win components which contains text.
 * No Impl class at the moment because all components need a different 
 * implication.
 */
public interface IComponentWithTextImpl {

    /**
     * Verifies if the textfield shows the passed text.
     *
     * @param text The text to verify.
     * @param operator The operator used to verify
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws StepExecutionException the {@link StepExecutionException}
     * @throws StepVerifyFailedException the {@link StepVerifyFailedException}
     */
    public void rcVerifyText(String text, String operator) throws 
    StepVerifyFailedException, StepExecutionException, RemoteServerException;
    
    /**
     * Action to read the value of a JButton to store it in a variable
     * in the Client
     * @param variable the name of the variable
     * @return the text value.
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public String rcReadValue(String variable) throws RemoteServerException;
    
}
