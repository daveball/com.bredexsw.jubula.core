package com.bredexsw.guidancer.rc.win.implclasses;

import java.awt.Rectangle;

import org.apache.commons.lang.ArrayUtils;
import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.DragAndDropHelper;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.implclasses.table.Cell;
import org.eclipse.jubula.rc.common.util.IndexConverter;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.toolkit.enums.ValueSets;
import org.eclipse.jubula.toolkit.enums.ValueSets.InteractionMode;
import org.eclipse.jubula.toolkit.enums.ValueSets.SearchType;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Table;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.driver.IWinRobot;
import com.bredexsw.guidancer.rc.win.driver.KeyCodeConverter;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * Implementation (tester) class for Tables.
 */
public abstract class TableImplClass extends AbstractWinImplClass {

    /** the logger */
    private static final Logger LOG = 
            LoggerFactory.getLogger(TableImplClass.class);
    
    /** 
     * Constant to indicate that a value within a row or column could not be 
     * found 
     */
    private static final int INDEX_NOT_FOUND = -2;
    
    /**
     * 
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        try {
            return getTable().getColumnHeaders();
        } catch (RemoteServerException rse) {
            LOG.error("Could not get descriptive text for component.", rse); //$NON-NLS-1$
            return null;
        }
    }

    /**
     * 
     * @return the component handled by the receiver, as a Table.
     */
    abstract Table getTable();
    
    /**
     * Selects a cell relative to the cell at the current mouse position.
     * If the mouse is not at any cell, the current selected cell is used.
     * @param direction the direction to move.
     * @param cellCount the amount of cells to move
     * @param clickCount the click count to select the new cell.
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param extendSelection Should this selection be part of a multiple selection
     * @throws StepExecutionException if any error occurs
     */
    public void rcMove(String direction, int cellCount, int clickCount,
        final int xPos, final String xUnits,
        final int yPos, final String yUnits, final String extendSelection)
        throws StepExecutionException {

        Cell currCell = null;
        try {
            currCell = getCellAtMousePosition();
        } catch (StepExecutionException e) {
            currCell = getSelectedCell();
        }
        
        int newCol = currCell.getCol();
        int newRow = currCell.getRow();
        if (ValueSets.Direction.up.rcValue().equalsIgnoreCase(direction)) {
            newRow -= cellCount;
        } else if (ValueSets.Direction.down.rcValue()
                .equalsIgnoreCase(direction)) {
            newRow += cellCount;
        } else if (ValueSets.Direction.left.rcValue()
                .equalsIgnoreCase(direction)) {
            newCol -= cellCount;
        } else if (ValueSets.Direction.right.rcValue()
                .equalsIgnoreCase(direction)) {
            newCol += cellCount;
        }
        newRow = IndexConverter.toUserIndex(newRow);
        newCol = IndexConverter.toUserIndex(newCol);
        rcSelectCell(Integer.toString(newRow), MatchUtil.EQUALS, 
                Integer.toString(newCol), MatchUtil.EQUALS, clickCount,
                     xPos, xUnits, yPos, yUnits, extendSelection, 
                     InteractionMode.primary.rcIntValue());
    }

    /**
     * @return the cell under the current mouse position.
     * @throws StepExecutionException If no cell is found.
     */
    protected Cell getCellAtMousePosition() throws StepExecutionException {
        Communicator communicator =
                WinAUTServer.getWinCommunicator();
        NativeMessage msgObj = new NativeMessage(
                getTable().getNativeClassName(),
                RobotConstants.GET_CELL_AT_MOUSE_CURSOR,
                getTable().getLocator());
        
        try {
            int[] cellCoordinates = communicator.sendMsg(msgObj).getIntArray();
            verifyCellCoordinates(cellCoordinates);
            int row = cellCoordinates[0];
            int column = cellCoordinates[1];
            
            checkRowColBounds(row, column, true);
            return new Cell(row, column);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * @return The currently selected cell of the Table.
     *
     * @throws StepExecutionException
     *             If no cell is selected.
     */
    private Cell getSelectedCell() throws StepExecutionException {
        Communicator communicator =
                WinAUTServer.getWinCommunicator();
        NativeMessage msgObj = new NativeMessage(
                getTable().getNativeClassName(),
                RobotConstants.GET_SELECTED_CELL, getTable().getLocator());

        try {
            int[] cellCoordinates = communicator.sendMsg(msgObj).getIntArray();
            verifyCellCoordinates(cellCoordinates);
            int row = cellCoordinates[0];
            int column = cellCoordinates[1];
            
            checkRowColBounds(row, column, true);
            return new Cell(row, column);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Checks that the argument can be interpreted as cell coordinates.
     * Currently, the only criterium is that exactly two integers are present.
     * 
     * @param cellCoordinates The data structure to verify.
     * 
     * @throws StepExecutionException if the argument cannot be interpreted
     *                                as cell coordinates.
     */
    private void verifyCellCoordinates(int[] cellCoordinates) 
        throws StepExecutionException {
        
        if (cellCoordinates.length != 2) {
            throw new StepExecutionException("Received data structure that could not be interpreted as cell coordinates",  //$NON-NLS-1$
                    EventFactory.createConfigErrorEvent());
        }
    }
    
    /**
     * Selects a cell in the Table.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param clickCount The number of clicks with the right mouse button
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param extendSelection Should this selection be part of a multiple selection
     * @param button what mouse button should be used
     * @throws StepExecutionException
     *             If the row or the column is invalid
     */
    public void rcSelectCell(final String row, final String rowOperator,
        final String col, final String colOperator,
        final int clickCount, final int xPos, final String xUnits,
        final int yPos, final String yUnits, final String extendSelection, 
        int button) throws StepExecutionException {

        try {
            
            final int implRow = getRowFromString(row, rowOperator);
            final int implCol = getColumnFromString(col, colOperator);
            final boolean isExtendSelection = extendSelection.equals(
                    ValueSets.BinaryChoice.yes.rcValue());
            
            checkRowColBounds(implRow, implCol, false);
            Rectangle rectangle = getRobot().getCellBounds(
                    getTable(), implRow, implCol);
            
            ClickOptions clickOptions = ClickOptions.create();
            clickOptions.setClickCount(clickCount);
            clickOptions.setMouseButton(button);

            try {
                if (isExtendSelection) {
                    getRobot().keyPress(getTable(),
                            KeyCodeConverter.getKeyCode(
                                    ValueSets.Modifier.control.rcValue()));
                }
                getRobot().click(getTable(), rectangle, clickOptions,
                        xPos, xUnits.equalsIgnoreCase(POS_UNIT_PIXEL),
                        yPos, yUnits.equalsIgnoreCase(POS_UNIT_PIXEL));
            } finally {
                if (isExtendSelection) {
                    getRobot().keyRelease(getTable(),
                            KeyCodeConverter.getKeyCode(
                                    ValueSets.Modifier.control.rcValue()));
                }
            }
            
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }

    }

    /**
     * Gets row index from string with index or text of first row
     *
     * @param usrIdxRow index or value in first col
     * @param operator The operation used to verify
     * @return integer of String of row 
     * @throws RemoteServerException 
     */
    private int getRowFromString (String usrIdxRow, String operator) 
        throws RemoteServerException {
        
        try {
            
            int implIdxRow = IndexConverter.toImplementationIndex(
                    Integer.parseInt(usrIdxRow));
            if (implIdxRow == IWinRobot.HEADER_IDX 
                    && ArrayUtils.isEmpty(getTable().getColumnHeaders())) {
                throw new StepExecutionException("No Header", //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.NO_HEADER));
            }
            
            return implIdxRow;
            
        } catch (NumberFormatException nfe) {
            String[] i18nParameters = new String[] {usrIdxRow};
            throw new StepExecutionException(
                    "Table rows in this Toolkit can only be addressed by index. Invalid index: " //$NON-NLS-1$
                        + usrIdxRow, 
                    EventFactory.createConfigErrorEvent(
                            TestErrorEvent.UNSUPPORTED_PARAMETER_VALUE, 
                            i18nParameters));
        }
    }

    /**
     * Gets column index from string with header name or index
     *
     * @param usrIdxCol Headername or index of column 
     * @param operator The operation used to verify
     * @return column index 
     * @throws RemoteServerException 
     */
    private int getColumnFromString(String usrIdxCol, 
            String operator) throws RemoteServerException {
        
        try {
            int implIdxCol = IndexConverter.toImplementationIndex(
                    Integer.parseInt(usrIdxCol));
            if (implIdxCol == IWinRobot.HEADER_IDX 
                    && ArrayUtils.isEmpty(getTable().getRowHeaders())) {
                throw new StepExecutionException("No Header", //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.NO_HEADER));
            }
            return implIdxCol;
        } catch (NumberFormatException nfe) {
            String[] columnHeaders = getTable().getColumnHeaders();
            if (ArrayUtils.isEmpty(columnHeaders)) {
                throw new StepExecutionException("No Header", //$NON-NLS-1$
                        EventFactory.createActionError(
                            TestErrorEvent.NO_HEADER));
            }
            for (int i = 0; i < columnHeaders.length; i++) {
                if (MatchUtil.getInstance().match(
                        columnHeaders[i], usrIdxCol, operator)) {
                    return i;
                }
            }
            
            throw new StepExecutionException("Invalid column: " + usrIdxCol, //$NON-NLS-1$
                    EventFactory.createActionError(
                            TestErrorEvent.INVALID_INDEX_OR_HEADER));
        }
    }
    
    /**
     * Verifies that <code>value</code> is within the bounds of a table row
     * or column with length <code>count</code>.
     *
     * @param value
     *            The value to check.
     * @param count
     *            The upper bound.
     * @param ignoreHeader
     *            Whether the header should be ignored with respect to bounds
     *            verification. For example, a value of <code>true</code>
     *            will cause the verification to fail if <code>value</code>
     *            represents the index of a column header or row header.
     * 
     *  @throws StepExecutionException if verification fails.
     */
    private void checkBounds(int value, int count, boolean ignoreHeader) 
        throws StepExecutionException {
        
        if ((value < 0 || value >= count) 
                && (ignoreHeader || value != IWinRobot.HEADER_IDX)) {
            throw new StepExecutionException("Invalid row/column: " + value, //$NON-NLS-1$
                EventFactory.createActionError(
                        TestErrorEvent.INVALID_INDEX_OR_HEADER));
        }
    }

    /**
     * Checks if the passed row and column are inside the bounds of the Table.
     *
     * @param row
     *            The row
     * @param column
     *            The column
     * @param ignoreHeaders
     *            Whether the header should be ignored with respect to bounds
     *            verification. For example, a value of <code>true</code>
     *            will cause the verification to fail if <code>value</code>
     *            represents the index of a column header or row header.
     * @throws StepExecutionException
     *             If the row or the column is outside of the Table's bounds.
     */
    private void checkRowColBounds(int row, int column, boolean ignoreHeaders)
        throws StepExecutionException {
        
        try {
            String[][] tableContents = getTable().getContents();
            checkBounds(row, tableContents.length, ignoreHeaders);
            
            int columnCount = 0;
            if (tableContents.length > 0) {
                // assuming that all rows have the same number of columns
                columnCount = tableContents[0].length;
            }
            checkBounds(column, columnCount, ignoreHeaders);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Checks if the passed row is inside the bounds of the Table.
     *
     * @param row
     *            The row
     * @param ignoreHeaders
     *            Whether the header should be ignored with respect to bounds
     *            verification. For example, a value of <code>true</code>
     *            will cause the verification to fail if <code>value</code>
     *            represents the index of a column header.
     * @throws StepExecutionException
     *             If the row is outside of the Table's bounds.
     */
    private void checkRowBounds(int row, boolean ignoreHeaders) 
        throws StepExecutionException {
        
        try {
            String[][] tableContents = getTable().getContents();
            checkBounds(row, tableContents.length, ignoreHeaders);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }
    
    /**
     * Verifies, if value exists in column.
     *
     * @param col The column of the cell.
     * @param colOperator the column header operator
     * @param value The cell text to verify.
     * @param operator The operation used to verify
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param exists true if value exists, false otherwise
     * @throws StepExecutionException
     *             If the row or the column is invalid, or if the rendered text
     *             cannot be extracted.
     */
    public void rcVerifyValueInColumn(final String col,
            final String colOperator, final String value,
            final String operator, final String searchType, boolean exists)
        throws StepExecutionException {

        try {
            final int implCol = getColumnFromString(col, colOperator);
            boolean found = getFirstIndexOfValueInColumn(
                    value, operator, searchType, implCol) != INDEX_NOT_FOUND;
    
            Verifier.equals(exists, found);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
        
    }

    /**
     * Verifies, if value exists in row.
     *
     * @param row The row of the cell.
     * @param rowOperator the row header operator
     * @param value The cell text to verify.
     * @param operator The operation used to verify
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param exists true if value exists, false otherwise
     * @throws StepExecutionException
     *             If the row or the column is invalid, or if the rendered text
     *             cannot be extracted.
     */
    public void rcVerifyValueInRow(final String row, final String rowOperator,
            final String value, final String operator, final String searchType,
            boolean exists) throws StepExecutionException {

        try {
            final int implRow = getRowFromString(row, rowOperator);
            boolean found = getFirstIndexOfValueInRow(
                    value, operator, searchType, implRow) != INDEX_NOT_FOUND;
    
            Verifier.equals(exists, found);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }

    }

    /**
     * 
     * @param value The cell text to find.
     * @param operator The operation used to find
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param column The 0-based index of the column in which to search.
     * @return the 0-based index of the first row in which the desired value 
     *         was found, or {@link #INDEX_NOT_FOUND} if no such row could be
     *         found.
     * @throws RemoteServerException
     */
    private int getFirstIndexOfValueInColumn(String value, String operator,
            String searchType, int column) throws RemoteServerException {
        String [][] tableContents = getTable().getContents();
        for (int i = getStartingRowIndex(searchType);
                i < tableContents.length; ++i) {
            if (MatchUtil.getInstance().match(
                    getTable().getContents()[i][column], value, operator)) {
                return i;
            }
        }

        String [] headers = getTable().getColumnHeaders();
        if (headers.length > column) {
            if (MatchUtil.getInstance().match(
                    headers[column], value, operator)) {
                return WinRobotImpl.HEADER_IDX;
            }
        }
        
        return INDEX_NOT_FOUND;
    }

    /**
     * 
     * @param value The cell text to find.
     * @param operator The operation used to find
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param row The 0-based index of the row in which to search.
     * @return the 0-based index of the first column in which the desired value 
     *         was found, or {@link #INDEX_NOT_FOUND} if no such column could be
     *         found.
     * @throws RemoteServerException
     */
    private int getFirstIndexOfValueInRow(String value, String operator,
            String searchType, int row) throws RemoteServerException {

        checkRowBounds(row, false);
        boolean rowIsColumnHeader = row == IWinRobot.HEADER_IDX;
        String[] rowContents = rowIsColumnHeader ? getTable().getColumnHeaders()
                : getTable().getContents()[row];
        for (int i = getStartingColumnIndex(searchType);
                i < rowContents.length; ++i) {
            if (MatchUtil.getInstance().match(
                    rowContents[i], value, operator)) {
                return i;
            }
        }

        return INDEX_NOT_FOUND;
    }

    /**
     * @param searchType Determines the row where the search begins ("relative" or "absolute")
     * @return The index from which to begin a search, based on the search type
     *         and (if appropriate) the currently selected cell.
     */
    private int getStartingRowIndex(String searchType) {
        int startingIndex = 0;
        if (searchType.equalsIgnoreCase(SearchType.relative.rcValue())) {
            startingIndex = getSelectedCell().getRow() + 1;
        }
        return startingIndex;
    }

    /**
     * @param searchType Determines the row where the search begins ("relative" or "absolute")
     * @return The index from which to begin a search, based on the search type
     *         and (if appropriate) the currently selected cell.
     */
    private int getStartingColumnIndex(String searchType) {
        int startingIndex = 0;
        if (searchType.equalsIgnoreCase(SearchType.relative.rcValue())) {
            startingIndex = getSelectedCell().getCol() + 1;
        }
        return startingIndex;
    }

    /**
     * Finds the first row which contains the value <code>value</code>
     * in column <code>col</code> and selects this row.
     *
     * @param col the column to select
     * @param colOperator the column header operator
     * @param value the value
     * @param regexOp the regex operator
     * @param clickCount the number of clicks
     * @param extendSelection Should this selection be part of a multiple selection
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param button what mouse button should be used
     */
    public void rcSelectRowByValue(String col, String colOperator,
        final String value, final String regexOp, int clickCount,
        final String extendSelection, final String searchType, int button) {

        try {
            final int implCol = getColumnFromString(col, colOperator);
            int implRow = getFirstIndexOfValueInColumn(
                    value, regexOp, searchType, implCol);
            if (implRow == INDEX_NOT_FOUND) {
                throw new StepExecutionException("no such row found", //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.NOT_FOUND));
            }
            
            String  userIdxRow = 
                    String.valueOf(IndexConverter.toUserIndex(implRow));
            String  userIdxCol = 
                    String.valueOf(IndexConverter.toUserIndex(implCol));
            
            rcSelectCell(userIdxRow, MatchUtil.EQUALS, userIdxCol, colOperator, 
                    clickCount, 50, POS_UNI_PERCENT, 50, POS_UNI_PERCENT, 
                    extendSelection, button);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }
    
    /**
     * Finds the first column which contains the value <code>value</code>
     * in the given row and selects the cell.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param value the value
     * @param clickCount the number of clicks
     * @param regex search using regex
     * @param extendSelection Should this selection be part of a multiple selection
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param button what mouse button should be used
     */
    public void rcSelectCellByColValue(String row, String rowOperator,
        final String value, final String regex, int clickCount,
        final String extendSelection, final String searchType, int button) {

        try {
            final int implRow = getRowFromString(row, rowOperator);
            int implColumn = getFirstIndexOfValueInRow(
                    value, regex, searchType, implRow);
            if (implRow == INDEX_NOT_FOUND) {
                throw new StepExecutionException("no such column found", //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.NOT_FOUND));
            }
            
            String  userIdxRow = 
                    String.valueOf(IndexConverter.toUserIndex(implRow));
            String  userIdxCol = 
                    String.valueOf(IndexConverter.toUserIndex(implColumn));
            
            rcSelectCell(userIdxRow, rowOperator, userIdxCol, MatchUtil.EQUALS, 
                    clickCount, 50, POS_UNI_PERCENT, 50, POS_UNI_PERCENT, 
                    extendSelection, button);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies the rendered text inside cell at the mouse position on screen.
     *
     * @param text
     *            The cell text to verify.
     * @param operator
     *            The operation used to verify
     * @throws StepExecutionException
     *             If the mouse pointer is not currently positioned on a cell
     *             in the table.
     */
    public void rcVerifyTextAtMousePosition(String text, String operator)
        throws StepExecutionException {

        try {
            Cell cell = getCellAtMousePosition();
            final int implRow = cell.getRow();
            final int implCol = cell.getCol();
            Verifier.match(getText(implRow, implCol), text, operator);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies the rendered text inside the currently selected cell.
     *
     * @param text
     *            The cell text to verify.
     * @param operator
     *            The operation used to verify
     * @throws StepExecutionException
     *             If there is no selected cell.
     */
    public void rcVerifyText(String text, String operator)
        throws StepExecutionException {

        try {
            Cell cell = getSelectedCell();
            MatchUtil.getInstance().match(
                    getText(cell.getRow(), cell.getCol()), text, operator);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Verifies the rendered text inside the passed cell.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param text
     *            The cell text to verify.
     * @param operator
     *            The operation used to verify
     * @throws StepExecutionException
     *             If the row or the column is invalid, or if the rendered text
     *             cannot be extracted.
     */
    public void rcVerifyText(String text, String operator, final String row,
            final String rowOperator, final String col,
            final String colOperator) throws StepExecutionException {

        try {
            final int implRow = getRowFromString(row, rowOperator);
            final int implCol = getColumnFromString(col, colOperator);
            Verifier.match(getText(implRow, implCol), text, operator);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Action to read the value of the current selected cell of the Table
     * to store it in a variable in the Client.
     * @param variable the name of the variable
     * @return the text value of the selected cell in the Table.
     * @throws StepExecutionException if no cell is selected.
     */
    public String rcReadValue(String variable) throws StepExecutionException {
        try {
            Cell selectedCell = getSelectedCell();
            return getText(selectedCell.getRow(), selectedCell.getCol());
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Action to read the value of the passed cell of the JTable
     * to store it in a variable in the Client
     * @param variable the name of the variable
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @return the text value.
     */
    public String rcReadValue(String variable, String row, String rowOperator,
            String col, String colOperator) {

        try {
            final int implRow = getRowFromString(row, rowOperator);
            final int implCol = getColumnFromString(col, colOperator);

            return getText(implRow, implCol);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * 
     * @param row The 0-based index of the row with the desired cell. May be
     *            {@link IWinRobot#HEADER_IDX}, which indicates that the text 
     *            of a column header cell should be retrieved. 
     * @param column The 0-based index of the column with the desired cell.
     * @return the text in the cell at the given index coordinates.
     * @throws RemoteServerException if information needs to be retrieved
     *               from the Win server and an error occurs in the 
     *               communication with the Win server.
     * @throws StepExecutionException if the Table does not have a cell at the
     *               given index coordinates.
     */
    private String getText(int row, int column) 
        throws RemoteServerException, StepExecutionException {
        
        //if row is header and column is existing
        if (row == IWinRobot.HEADER_IDX && column > -1) {
            String[] columnHeaders = getTable().getColumnHeaders();
            if (column > columnHeaders.length) {
                throw new StepExecutionException("Invalid column: " + column, //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.INVALID_INDEX_OR_HEADER));
            }
            return columnHeaders[column];
        }
        checkRowColBounds(row, column, true);
        return getTable().getContents()[row][column];
    }
    
    /**
     * {@inheritDoc}
     */
    public String rcReadValueAtMousePosition(String variable) {

        try {
            Cell cellAtMousePosition = getCellAtMousePosition();
            int rowIndex = cellAtMousePosition.getRow();
            int columnIndex = cellAtMousePosition.getCol();
            return getText(rowIndex, columnIndex);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
        
    }

    /**
     * Types the text in the specified cell.
     *
     * @param text The text
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @throws StepExecutionException
     *             If the text input fails
     */
    public void rcInputText(String text, String row, String rowOperator,
            String col, String colOperator) throws StepExecutionException {

        try {
            //if row is header row
            if (getRowFromString(row, rowOperator) == -1) {
                throw new StepExecutionException("Unsupported Header Action", //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.UNSUPPORTED_HEADER_ACTION));
            }
            rcSelectCell(row, rowOperator, col, colOperator, 0, 50, 
                    POS_UNI_PERCENT, 50, POS_UNI_PERCENT, 
                    ValueSets.BinaryChoice.no.rcValue(), 1);
            rcInputText(text);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }
    
    /**
     * Writes the passed text into the currently selected cell.
     *
     * @param text
     *            The text.
     * @throws StepExecutionException
     *             If there is no selected cell, or if the cell is not editable,
     *             or if the table cell editor permits the text to be written.
     */
    public void rcInputText(final String text) throws StepExecutionException {
        inputText(text, false);
    }

    /**
     * Inputs/replaces the given text. Requires that the mouse pointer is
     * already positioned on the cell in which to input text. 
     * @param text the text to input
     * @param replace whether to replace or not
     * @throws StepExecutionException 
     *          If there is no selected cell, or if the cell is not editable, 
     *          or if the table cell editor does not permit the text to be 
     *          written.
     */
    private void inputText(final String text, boolean replace)
        throws StepExecutionException {

        try { 
            // click the cell once to activate it
            getRobot().clickAtCurrentPosition(getTable(), 1, 1);
            // sometimes the editor only appears after doubleclick!
            if (!getTable().isCellEditorFocused()) {
                getRobot().clickAtCurrentPosition(getTable(), 2, 1);
            }

            if (replace) {
                getRobot().replaceText(text);
            } else {
                getRobot().inputTextInCellEditor(text);
            }
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
    }

    /**
     * Types <code>text</code> into the component. This replaces the shown
     * content in the current selected cell.
     *
     * @param text the text to type in
     * @throws StepExecutionException
     *  If there is no selected cell, or if the cell is not editable,
     *  or if the table cell editor permits the text to be written.
     */
    public void rcReplaceText(String text) throws StepExecutionException {
        inputText(text, true);
    }

    /**
     * Replaces the given text in the given cell coordinates
     * @param text the text to replace
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     */
    public void rcReplaceText(String text, String row, String rowOperator,
            String col, String colOperator) {

        try {
            //if row is header row
            if (getRowFromString(row, rowOperator) == -1) {
                throw new StepExecutionException("Unsupported Header Action", //$NON-NLS-1$
                        EventFactory.createActionError(
                                TestErrorEvent.UNSUPPORTED_HEADER_ACTION));
            }
            rcSelectCell(row, rowOperator, col, colOperator, 0, 50, 
                    POS_UNI_PERCENT, 50, POS_UNI_PERCENT, 
                    ValueSets.BinaryChoice.no.rcValue(), 1);
            inputText(text, true);
        } catch (RemoteServerException rse) {
            throw new StepExecutionException(rse);
        }
        
    }
    
    /**
     * Drags the cell of the JTable.<br>
     * With the xPos, yPos, xunits and yUnits the click position inside the
     * cell can be defined.
     *
     * @param mouseButton the mouseButton.
     * @param modifier the modifier.
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     */
    public void rcDragCell(final int mouseButton, final String modifier,
            final String row, final String rowOperator,
            final String col, final String colOperator, final int xPos,
            final String xUnits, final int yPos, final String yUnits) {
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setModifier(modifier);
        dndHelper.setMouseButton(mouseButton);
        rcSelectCell(row, rowOperator, col, colOperator, 0, xPos, xUnits, yPos,
                yUnits, ValueSets.BinaryChoice.no.rcValue(), 1);
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);
    }
    
    /**
     * Drops on the cell of the JTable.<br>
     * With the xPos, yPos, xunits and yUnits the click position inside the
     * cell can be defined.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    public void rcDropCell(final String row, final String rowOperator,
            final String col, final String colOperator, final int xPos,
            final String xUnits, final int yPos, final String yUnits,
            int delayBeforeDrop) {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        try {
            rcSelectCell(row, rowOperator, col, colOperator, 0, xPos, xUnits,
                    yPos, yUnits, ValueSets.BinaryChoice.no.rcValue(), 1);
            waitBeforeDrop(delayBeforeDrop);
        } finally {
            getRobot().mouseRelease(null, null, dndHelper.getMouseButton());
            pressOrReleaseModifiers(dndHelper.getModifier(), false);
        }
    }
    
    /**
     * Finds the first column which contains the value <code>value</code>
     * in the given row and drags the cell.
     *
     * @param mouseButton the mouse button
     * @param modifier the modifiers
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param value the value
     * @param regex search using regex
     * @param searchType Determines where the search begins ("relative" or "absolute")
     */
    public void rcDragCellByColValue(int mouseButton, String modifier,
            String row, String rowOperator, final String value,
            final String regex, final String searchType) {

        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setModifier(modifier);
        dndHelper.setMouseButton(mouseButton);

        rcSelectCellByColValue(row, rowOperator, value, regex, 0, 
                ValueSets.BinaryChoice.no.rcValue(), searchType, 1);       
        
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);
    }
    
    /**
     * Finds the first row which contains the value <code>value</code>
     * in column <code>col</code> and drags this row.
     *
     * @param mouseButton the mouse button
     * @param modifier the modifier
     * @param col the column
     * @param colOperator the column header operator
     * @param value the value
     * @param regexOp the regex operator
     * @param searchType Determines where the search begins ("relative" or "absolute")
     */
    public void rcDragRowByValue(int mouseButton, String modifier, String col,
            String colOperator, final String value, final String regexOp,
            final String searchType) {

        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setModifier(modifier);
        dndHelper.setMouseButton(mouseButton);
        rcSelectRowByValue(col, colOperator, value, regexOp, 1,
                ValueSets.BinaryChoice.no.rcValue(), searchType, 1);
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);
    }
    
    /**
     * Finds the first column which contains the value <code>value</code>
     * in the given row and drops on the cell.
     *
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param value the value
     * @param regex search using regex
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    public void rcDropCellByColValue(String row, String rowOperator,
            final String value, final String regex, final String searchType,
            int delayBeforeDrop) {

        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        try {
            rcSelectCellByColValue(row, rowOperator, value, regex, 0, 
                    ValueSets.BinaryChoice.no.rcValue(), searchType, 1);
            waitBeforeDrop(delayBeforeDrop);
        } finally {
            getRobot().mouseRelease(null, null, dndHelper.getMouseButton());
            pressOrReleaseModifiers(dndHelper.getModifier(), false);
        }
    }
    
    /**
     * Finds the first row which contains the value <code>value</code>
     * in column <code>col</code> and drops on this row.
     *
     * @param col the column to select
     * @param colOperator the column header operator
     * @param value the value
     * @param regexOp the regex operator
     * @param searchType Determines where the search begins ("relative" or "absolute")
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    public void rcDropRowByValue(String col, String colOperator,
            final String value, final String regexOp, final String searchType,
            int delayBeforeDrop) {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        try {
            rcSelectRowByValue(col, colOperator, value, regexOp, 0,
                    ValueSets.BinaryChoice.no.rcValue(), searchType, 1);
            waitBeforeDrop(delayBeforeDrop);
        } finally {
            getRobot().mouseRelease(null, null, dndHelper.getMouseButton());
            pressOrReleaseModifiers(dndHelper.getModifier(), false);
        }
    }
    
    /**
     * Verifies the editable property of the cell under current mouse position.
     *
     * @param editable the editable property to verify.
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcVerifyEditableMousePosition(boolean editable) throws 
    RobotException, RemoteServerException {
        final boolean isEditable = getRobot().checkCellEditabilityAtMousePos(
                getTable());
        Verifier.equals(editable, isEditable);
    }
    
    /**
     * Verifies the editable property of the current selected cell.
     *
     * @param editable
     *            The editable property to verify.
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcVerifyEditableSelected(boolean editable) throws 
    RobotException, RemoteServerException {
        final boolean isEditable = getRobot().checkCurrentCellEditability(
                getTable());
        Verifier.equals(editable, isEditable);
    }
    
    /**
     * Verifies the editable property of the given indices.
     *
     * @param editable
     *            The editable property to verify.
     * @param row the row to select
     * @param rowOperator the row header operator
     * @param col the column to select
     * @param colOperator the column header operator
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public void rcVerifyEditable(boolean editable, String row,
            String rowOperator, String col, String colOperator) throws
            RemoteServerException {
        //if row is header row
        if (getRowFromString(row, rowOperator) == -1) {
            throw new StepExecutionException("Unsupported Header Action", //$NON-NLS-1$
                    EventFactory.createActionError(
                            TestErrorEvent.UNSUPPORTED_HEADER_ACTION));
        }        
        
        final int implRow = getRowFromString(row, rowOperator);
        final int implCol = getColumnFromString(col, colOperator);
        
        final boolean isEditable = getRobot().checkCellEditability(
                getTable(), implRow, implCol);
        Verifier.equals(editable, isEditable);
    }
    
}
