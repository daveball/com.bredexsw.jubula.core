/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.Document;

/**
 * Class for testing all ControlType.Edit components like multiline richboxtext
 */
public class DocumentImplClass extends ComponentWithTextInputImplClass {

    @Override
    public Document getTextInputComponent() {
        return (Document) getComponent();
    }
}
