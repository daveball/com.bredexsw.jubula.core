/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.exception.StepVerifyFailedException;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.AbstractButton;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Impl class for all buttons types
 */
public abstract class AbstractButtonImplClass extends AbstractWinImplClass {

    /** the logger */
    private static final Logger LOG = 
            LoggerFactory.getLogger(AbstractButtonImplClass.class);
    
    /** {@inheritDoc} */
    public String[] getTextArrayFromComponent() {
        try {
            return new String[] {
                getRobot().getName(getButton().getNativeClassName(),
                        getButton().getLocator())};
        } catch (RemoteServerException rse) {
            LOG.error("Could not get descriptive text for component.", rse); //$NON-NLS-1$
            return null;
        }
    }

    /** 
     * {@inheritDoc} 
     * 
     * @throws RemoteServerException 
     * @throws StepExecutionException 
     * @throws StepVerifyFailedException */
    public void rcVerifyText(String text, String operator) throws
    //using getName because the automation element buttons using the label text as name
    StepVerifyFailedException, StepExecutionException, RemoteServerException {
        Verifier.match(getRobot().getName(getButton().getNativeClassName(),
                getButton().getLocator()), text, operator);
    }
    
    /**
     * Action to read the value of a JButton to store it in a variable
     * in the Client
     * @param variable the name of the variable
     * @return the text value.
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    public String rcReadValue(String variable) throws RemoteServerException {
        return getRobot().getName(getButton().getNativeClassName(),
                getButton().getLocator());
    }
    
    /**
     * Verifies the selected property. 
     * Returns always false for non Radio- / Check-Buttons, they
     * possess a own implementation.
     *
     * @param selected The selected property value to verify.
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcVerifySelected(boolean selected) throws 
    RobotException, RemoteServerException {
        Verifier.equals(selected, false);
    }
    
    /**
     * @return the button component
     */
    public abstract AbstractButton getButton();

}
