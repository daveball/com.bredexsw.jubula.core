/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.exception.StepVerifyFailedException;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.AbstractComponentWithTextInput;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Impl class for all textinput types like textfields and textboxes
 */
public abstract class ComponentWithTextInputImplClass extends 
    AbstractWinImplClass implements IComponentWithTextInputImpl {

    /** 
     * {@inheritDoc} 
     * 
     * @throws RemoteServerException 
     * @throws StepExecutionException 
     * @throws StepVerifyFailedException */
    public void rcVerifyText(String text, String operator) throws
    StepVerifyFailedException, StepExecutionException, RemoteServerException {
        Verifier.match(getRobot().getText(
                getTextInputComponent().getNativeClassName(),
                getTextInputComponent().getLocator()),
                text, operator);
    }
    
    /**
     * Types <code>text</code> into the component.
     *
     * @param text the text to type in
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    public void rcInputText(String text) throws RobotException,
        RemoteServerException {
        //check if textfield is enabled to input text
        AbstractComponentWithTextInput component = getTextInputComponent();
        if (!getRobot().checkEnablement(component.getNativeClassName(),
                component.getLocator())) {
            throw new StepExecutionException("Component is disabled", 
                    new TestErrorEvent(TestErrorEvent.INPUT_FAILED));
        }
        if (!getRobot().hasFocus(component.getNativeClassName(),
                component.getLocator())) {
            rcClick(1, 1);
        }        
        getRobot().inputText(text);
    }
    
    /**
     * Types <code>text</code> into the component. This replaces the shown
     * content.
     *
     * @param text the text to type in
     */
    public void rcReplaceText(String text) throws RobotException,
    RemoteServerException {
        //check if textfield is enabled to replace a text
        if (!getRobot().checkEnablement(
                getTextInputComponent().getNativeClassName(),
                getComponent().getLocator())) {
            throw new StepExecutionException("Component is disabled", 
                    new TestErrorEvent(TestErrorEvent.INPUT_FAILED));
        }
        this.rcClick(1, 1);
        getRobot().replaceText(text);
    }
    
    /**
     * {@inheritDoc} 
     */
    public void rcSelect()  throws RobotException,
    RemoteServerException  {
        AbstractComponentWithTextInput component = getTextInputComponent();
        if (!getRobot().checkEnablement(component.getNativeClassName(),
                component.getLocator())) {
            throw new StepExecutionException("Component is disabled", 
                    new TestErrorEvent(TestErrorEvent.INPUT_FAILED));
        }
        this.rcClick(1, 1);
        getRobot().selectAll();
    }
    
    /**
     * {@inheritDoc} 
     */
    public void rcInsertText(String text, int index) throws RobotException,
    RemoteServerException {
        AbstractComponentWithTextInput component = getTextInputComponent();
        if (!getRobot().checkEnablement(component.getNativeClassName(),
                component.getLocator())) {
            throw new StepExecutionException("Component is disabled", 
                    new TestErrorEvent(TestErrorEvent.INPUT_FAILED));
        }
        if (!getRobot().hasFocus(component.getNativeClassName(),
                component.getLocator())) {
            rcClick(1, 1);
        }  
        getRobot().insertTextAfterIndex(text, index);
    }
    
    
    /**
     * {@inheritDoc}
     */
    public String rcReadValue(String variable) throws RemoteServerException {
        return getRobot().getText(getTextInputComponent().getNativeClassName(),
                getTextInputComponent().getLocator());
    }

    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void rcInsertText(String text, String pattern, String operator,
            boolean after) throws RobotException, RemoteServerException,
            StepExecutionException {
        
        if (text == null) {
            throw new StepExecutionException(
                "The text to be inserted must not be null", EventFactory //$NON-NLS-1$
                    .createActionError());
        }
        final MatchUtil.FindResult matchedText = MatchUtil.getInstance().
            find(getRobot().getText(
                    getTextInputComponent().getNativeClassName(),
                    getTextInputComponent().getLocator()), pattern, 
                    operator);
        if ((matchedText == null) || (matchedText.getStr() == null)
                || (matchedText.getStr().length() == 0)) {
            throw new StepExecutionException("The pattern '" + pattern //$NON-NLS-1$
                    + "' could not be found", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        final int index = matchedText.getPos();
        int insertPos = after ? index + matchedText.getStr().length() : index;
        rcInsertText(text, insertPos);
    }

    /**
     * {@inheritDoc}
     */
    public void rcSelect(String pattern, String operator) throws RobotException,
        RemoteServerException {
        
        final MatchUtil.FindResult matchedText = MatchUtil.getInstance().
                find(getRobot().getText(
                        getTextInputComponent().getNativeClassName(),
                        getTextInputComponent().getLocator()), 
                        pattern, operator);
        if ((matchedText == null) || (matchedText.getStr() == null)
                || (matchedText.getStr().length() == 0)) {
            throw new StepExecutionException("The pattern '" + pattern //$NON-NLS-1$
                    + "' could not be found", //$NON-NLS-1$
                    EventFactory.createActionError(TestErrorEvent.NOT_FOUND));
        }
        final int indexBegin = matchedText.getPos();
        int indexEnd = indexBegin + matchedText.getStr().length();
        
        this.rcClick(1, 1);
        getRobot().selectPattern(indexBegin, indexEnd);
    }
    
    /**
     * Verifies the editable property.
     *
     * @param editable The editable property to verify.
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcVerifyEditable(boolean editable) throws RobotException, 
    RemoteServerException {
        boolean isEditable = getRobot().isEditable(
                getTextInputComponent().getNativeClassName(),
                getTextInputComponent().getLocator());
        Verifier.equals(editable, isEditable);
    }
    
    /**
     * @return the button component
     */
    public abstract AbstractComponentWithTextInput getTextInputComponent();

}
