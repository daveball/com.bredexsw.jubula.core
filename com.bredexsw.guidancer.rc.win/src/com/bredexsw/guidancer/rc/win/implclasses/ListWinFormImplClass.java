package com.bredexsw.guidancer.rc.win.implclasses;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.ListWinForm;

/**
 * Impl class for winform list implementations  
 */
public class ListWinFormImplClass extends ListImplClass {

    @Override
    public ListWinForm getList() {
        return (ListWinForm) getComponent();
    }
}
