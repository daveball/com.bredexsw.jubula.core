package com.bredexsw.guidancer.rc.win.implclasses;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.ListWPF;

/**
 * Impl class for WPF list implementations  
 */
public class ListWPFImplClass extends ListImplClass {

    @Override
    public ListWPF getList() {
        return (ListWPF) getComponent();
    }
}
