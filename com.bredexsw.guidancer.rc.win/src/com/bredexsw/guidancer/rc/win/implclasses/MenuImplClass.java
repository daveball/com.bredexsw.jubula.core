/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import org.eclipse.jubula.rc.common.driver.RobotTiming;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;

import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Class for testing Menubars
 */
public class MenuImplClass extends AbstractWinImplClass {
    
    /**
     * {@inheritDoc}
     */
    public String[] getTextArrayFromComponent() {
        return null;
    }
    
    /**
     * implementation for "wait for component"
     * @param timeout the maximum amount of time to wait for the component
     * @param delay the time to wait after the component is found
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void waitForComponent(int timeout, int delay) throws RobotException,
    RemoteServerException {
        boolean existsMenuBar = getRobot().existsMenuBar();
        if (!existsMenuBar) {
            long start = System.currentTimeMillis();
            do {
                RobotTiming.sleepWaitForComponentPollingDelay();
                existsMenuBar = getRobot().existsMenuBar();
            } while (System.currentTimeMillis() - start < timeout 
                    && !existsMenuBar);
            if (!existsMenuBar) {
                throw new StepExecutionException("No Menubar found.", //$NON-NLS-1$
                        EventFactory.createComponentNotFoundErrorEvent());
            }
        }
        TimeUtil.delay(delay);
    }
    
    /**
     * Select a menu item by a name path
     * 
     * @param menuItems the name path as a string
     * @param operator the operator
     * @throws RobotException the RobotException
     * @throws RemoteServerException the RemoteServerException
     */
    public void selectMenuItem(String menuItems, String operator) throws 
    RobotException, RemoteServerException {
        useTextPath(menuItems, operator, false, null);
    }    
    
    /**
     * Checks if the specified menu item is enabled.
     * @param menuItem the menu item as a text path to verify against
     * @param operator operator used for matching
     * @param enabled is the specified menu item enabled?
     * @throws RemoteServerException the {@link RemoteServerException} 
     * @throws RobotException the {@link RobotException}
     */
    public void verifyEnabled(String menuItem, String operator, boolean enabled)
        throws RobotException, RemoteServerException {
        useTextPath(menuItem, operator, true, getCheckEnablementID());
        Verifier.equals(enabled, getCheckResult());         
    }
    
    /**
     * Verifies if the specified menu item exists
     * @param menuItem the menu item to verifiy against
     * @param operator operator used for matching
     * @param exists should the menu item exist?
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    public void verifyExists(String menuItem, String operator, boolean exists) 
        throws RobotException, RemoteServerException {
        try {
            useTextPath(menuItem, operator, true, getCheckExistenceID());
        } catch (StepExecutionException e) {
            // item not found
            setCheckResult(false);
        }
        
        Verifier.equals(exists, getCheckResult());         
    }
    
    /**
     * Checks if the specified menu item is selected.
     * @param menuItem the menu item to verify against
     * @param operator operator used for matching
     * @param selected is the specified menu item selected?
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    public void verifySelected(String menuItem, String operator, 
            boolean selected) throws RobotException, RemoteServerException {
        useTextPath(menuItem, operator, true, getCheckSelectionID());
        Verifier.equals(selected, getCheckResult()); 
    }
    
    /**
     * select a menu item by a index path
     * 
     * @param path the index path as a string
     * @throws RobotException the RobotException 
     * @throws RemoteServerException the RemoteServerException
     */
    public void selectMenuItemByIndexpath(String path) throws 
    RobotException, RemoteServerException {
        useIndexPath(path, false, null);
    }
    
    /**
     * Checks if the specified menu item is enabled.
     * @param menuItem the menu item as a text path to verify against
     * @param enabled is the specified menu item enabled?
     * @throws RemoteServerException 
     * @throws RobotException 
     */
    public void verifyEnabledByIndexpath(String menuItem, boolean enabled) 
        throws RobotException, RemoteServerException {
        useIndexPath(menuItem, true, getCheckEnablementID());
        Verifier.equals(enabled, getCheckResult());    
    }
    
    /**
     * Verifies if the specified menu item exists
     * @param menuItem the menu item to verifiy against
     * @param exists should the menu item exist?
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void verifyExistsByIndexpath(String menuItem, boolean exists) 
        throws RobotException, RemoteServerException {
        try {
            useIndexPath(menuItem, true, getCheckExistenceID());
        } catch (StepExecutionException e) {
            // item not found
            setCheckResult(false);
        }
        Verifier.equals(exists, getCheckResult());    
    }
    
    /**
     * Checks if the specified menu item is selected.
     * @param menuItem the menu item to verify against
     * @param selected is the specified menu item selected?
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void verifySelectedByIndexpath(String menuItem, boolean selected) 
        throws RobotException, RemoteServerException {
        useIndexPath(menuItem, true, getCheckSelectionID());
        Verifier.equals(selected, getCheckResult()); 
    }
    
    /**
     * Use the text path methods to perform menu actions
     * 
     * @param path the path
     * @param operator the operator
     * @param check check the last item 
     * @param checkOperator check operation
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    private void useTextPath(String path, String operator, boolean check, 
            String checkOperator) throws RobotException, RemoteServerException {
        getRobot().initWindowMenu();
        String[] namePath = path.split(StringConstants.SLASH);
        
        int lenght = namePath.length;
        if (check) {
            lenght = lenght - 1;
        }
        
        try {
            for (int i = 0; i < lenght; i++) {
                selectMenuItemByName(namePath[i], operator, false, 
                        null);
            }
            if (check) {
                selectMenuItemByName(namePath[lenght], operator, check,
                        checkOperator);
            }
        } catch (RobotException e) {
            throw e;
        } catch (RemoteServerException e) {
            throw e;
        } finally {
            getRobot().closeMenu(null, lenght);
        }
    }
    
    /**
     * Use the index methods to perform menu actions
     * 
     * @param path the path to go
     * @param check check the last item 
     * @param checkOperator check operation
     * @throws RobotException the {@link RobotException}
     * @throws RemoteServerException the {@link RemoteServerException}
     */
    private void useIndexPath(String path, boolean check, 
            String checkOperator) throws RobotException, RemoteServerException {
        String[] stringIndexPath = path.split(StringConstants.SLASH);
        int[] indexPath = new int[stringIndexPath.length];
        
        for (int i = 0; i < stringIndexPath.length; i++) {
            indexPath[i] = Integer.parseInt(stringIndexPath[i]);
        }
        // all the logic is done on the Win side in the "selectMenuItemByIndexpath" method
        boolean result = getRobot().selectMenuItemByIndexpath(
                indexPath, check, checkOperator);
        setCheckResult(result);
    }   
    
}
