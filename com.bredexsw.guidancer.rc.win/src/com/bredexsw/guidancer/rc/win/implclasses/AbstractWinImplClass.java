/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.implclasses;

import java.awt.Rectangle;
import java.util.StringTokenizer;

import org.apache.commons.lang.Validate;
import org.eclipse.jubula.rc.common.driver.ClickOptions;
import org.eclipse.jubula.rc.common.driver.DragAndDropHelper;
import org.eclipse.jubula.rc.common.exception.RobotException;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.exception.StepVerifyFailedException;
import org.eclipse.jubula.rc.common.tester.interfaces.ITester;
import org.eclipse.jubula.rc.common.util.KeyStrokeUtil;
import org.eclipse.jubula.rc.common.util.MatchUtil;
import org.eclipse.jubula.rc.common.util.Verifier;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.objects.event.EventFactory;
import org.eclipse.jubula.tools.internal.objects.event.TestErrorEvent;
import org.eclipse.jubula.tools.internal.utils.TimeUtil;

import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.driver.KeyCodeConverter;
import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * Abstract class for all component impl classes
 */
public abstract class AbstractWinImplClass implements
        ITester {

    /** constant for Win server abstract class name */
    public static final String ABSTRACT_CLASSNAME = "AbstractElementImplClass"; 
    /** constants for communication */
    protected static final String POS_UNIT_PIXEL = "Pixel"; //$NON-NLS-1$
    /** constants for communication */
    protected static final String POS_UNI_PERCENT = "Percent"; //$NON-NLS-1$
    /** the component associated with this implementation class instance */
    private AbstractDotNetComponent m_component; //$NON-NLS-1$
    /** the check result e.g for checking selection on a menuItem */
    private boolean m_checkResult;
    /** Check selection at a menuItem ID */
    private final String m_checkSelectionID = "selection";
    /** Check existence at a menuItem ID */
    private final String m_checkExistenceID = "existence";
    /** Check enablement at a menuItem ID */
    private final String m_checkEnablementID = "enablement";
    /**
     * Communication robot for the test steps.
     */
    private WinRobotImpl m_robot; //$NON-NLS-1$

    /**
     * Clicks the center of the component.
     * 
     * @param clickCount
     *            Number of mouse clicks
     * @param mouseButton
     *            Pressed button
     * @throws RemoteServerException
     * @throws RobotException
     * */
    public void rcClick(int clickCount, int mouseButton) throws RobotException,
            RemoteServerException {
        getRobot().click(
                getComponent(),
                null,
                ClickOptions.create().setClickCount(clickCount)
                        .setMouseButton(mouseButton));
    }

    /** {@inheritDoc} */
    public void setComponent(Object graphicsComponent) {
        Validate.isTrue(graphicsComponent instanceof AbstractDotNetComponent);
        m_component = (AbstractDotNetComponent) graphicsComponent;
    }

    /**
     * clicks into a component.
     * 
     * @param count
     *            amount of clicks
     * @param button
     *            what mouse button should be used
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @throws StepExecutionException
     *             error
     */
    public void rcClickDirect(int count, int button, int xPos, String xUnits,
            int yPos, String yUnits) throws StepExecutionException {

        getRobot().click(
                getComponent(),
                null,
                ClickOptions.create().setClickCount(count)
                        .setMouseButton(button), xPos,
                xUnits.equalsIgnoreCase(POS_UNIT_PIXEL), yPos,
                yUnits.equalsIgnoreCase(POS_UNIT_PIXEL));
    }

    /**
     * 
     * @return the component associated with this implementation class instance.
     */
    protected AbstractDotNetComponent getComponent() {
        return m_component;
    }

    /**
     * @return the robot
     */
    protected WinRobotImpl getRobot() {
        if (m_robot == null) {
            m_robot = (WinRobotImpl) WinAUTServer.getInstance().getRobot();
        }
        return m_robot;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcVerifyEnabled(boolean enabled) throws RobotException,
            RemoteServerException {
        boolean actual = getRobot()
                .checkEnablement(ABSTRACT_CLASSNAME,
                        getComponent().getLocator());
        Verifier.equals(enabled, actual);
    }

    /**
     * Verifies that the component exists and is visible.
     * 
     * @param exists
     *            <code>True</code> if the component is expected to exist and be
     *            visible, otherwise <code>false</code>.
     */
    public void rcVerifyExists(boolean exists) {
        /*
         * The actual testing of the component's existence/non-existence is
         * implemented in CAPTestCommand.getImplClass.
         */
    }

    /**
     * Verifies if the component has the focus.
     * 
     * @param hasFocus
     *            The hasFocus property to verify.
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     * @throws RobotException
     *             the {@link RobotException}
     */
    public void rcVerifyFocus(boolean hasFocus) throws RobotException,
            RemoteServerException {
        boolean focus = getRobot().hasFocus(ABSTRACT_CLASSNAME,
                this.getComponent().getLocator());
        Verifier.equals(hasFocus, focus);
    }

    /**
     * Verifies the value of the property with the name <code>name</code>. The
     * name of the property has be specified according to the JavaBean
     * specification. The value returned by the property is converted into a
     * string by calling <code>toString()</code> and is compared to the passed
     * <code>value</code>.
     * 
     * @param name
     *            The name of the property
     * @param value
     *            The value of the property as a string
     * @param operator
     *            The operator used to verify
     */
    public void rcVerifyProperty(String name, String value, String operator) {
        try {
            final String propToStr = getRobot().getPropertyValue(
                    getComponent(), name);
            Verifier.match(propToStr, value, operator);
        } catch (RobotException e) {
            throw new StepExecutionException(
                    e.getMessage(),
                    EventFactory.createActionError(
                            TestErrorEvent.PROPERTY_NOT_ACCESSABLE));
        }
    }

    /**
     * Stores the value of the property with the name <code>name</code>. The
     * name of the property has be specified according to the JavaBean
     * specification. The value returned by the property is converted into a
     * string by calling <code>toString()</code> and is stored to the passed
     * variable.
     * 
     * @param variableName
     *            The name of the variable to store the property value in
     * @param propertyName
     *            The name of the property
     * @return the property value.
     */
    public String rcStorePropertyValue(String variableName, String propertyName)
    {
        String propertyValue = StringConstants.EMPTY;
        try {
            propertyValue = getRobot().getPropertyValue(getComponent(),
                    propertyName);
        } catch (RobotException e) {
            throw new StepExecutionException(
                    e.getMessage(),
                    EventFactory.createActionError(
                            TestErrorEvent.PROPERTY_NOT_ACCESSABLE));
        }
        return propertyValue;
    }

    /**
     * dummy method for "wait for component"
     * 
     * @param timeout
     *            the maximum amount of time to wait for the component
     * @param delay
     *            the time to wait after the component is found
     */
    public void rcWaitForComponent(int timeout, int delay) {
        // do NOT delete this method!
        // do nothing, implementation is in class CAPTestCommand
        // because this action needs a special implementation!
    }
    
    /**
     * Performs a Drag. Moves into the middle of the Component and presses and
     * holds the given modifier and the given mouse button.
     * @param mouseButton the mouse button.
     * @param modifier the modifier, e.g. shift, ctrl, etc.
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcDrag(int mouseButton, String modifier, int xPos,
            String xUnits, int yPos, String yUnits) throws RobotException, 
            RemoteServerException {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        dndHelper.setMouseButton(mouseButton);
        dndHelper.setModifier(modifier);
        
        rcClickDirect(0, 0, xPos, xUnits, yPos, yUnits);
        pressOrReleaseModifiers(modifier, true);
        getRobot().mousePress(null, null, mouseButton);       
    }
    
    /**
     * Performs a Drop. Moves into the middle of the Component and releases
     * the modifier and mouse button pressed by rcDrag.
     * @param xPos what x position
     * @param xUnits should x position be pixel or percent values
     * @param yPos what y position
     * @param yUnits should y position be pixel or percent values
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     * @throws RemoteServerException the {@link RemoteServerException}
     * @throws RobotException the {@link RobotException}
     */
    public void rcDrop(int xPos, String xUnits, int yPos, String yUnits,
            int delayBeforeDrop) throws RobotException, RemoteServerException {
        
        final DragAndDropHelper dndHelper = DragAndDropHelper.getInstance();
        final String modifier = dndHelper.getModifier();
        final int mouseButton = dndHelper.getMouseButton();
        
        rcClickDirect(0, 0, xPos, xUnits, yPos, yUnits);
        waitBeforeDrop(delayBeforeDrop);
        pressOrReleaseModifiers(modifier, false);
        getRobot().mouseRelease(null, null, mouseButton); 
    }
    
    /**
     * Waits the given amount of time. Logs a drop-related error if interrupted.
     *
     * @param delayBeforeDrop the amount of time (in milliseconds) to wait
     *                        between moving the mouse to the drop point and
     *                        releasing the mouse button
     */
    static void waitBeforeDrop(int delayBeforeDrop) {
        TimeUtil.delay(delayBeforeDrop);
    }

    /**
     * Select an item in the popup menu
     * 
     * @param indexPath
     *            path of item indices
     * @param button
     *            MouseButton
     * @throws StepExecutionException
     *             error the StepExecutionException
     * @throws RemoteServerException
     *             the RemoteServerException
     * @throws RobotException
     *             the RobotException
     */
    public void rcPopupSelectByIndexPath(String indexPath, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        this.selectContextMenuItemByIndex(indexPath, 50, "", 50, "", button,
                false, null, true, false);
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and selects an item at the given position in the popup menu
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param indexPath
     *            path of item indices
     * @param button
     *            MouseButton
     * @throws StepExecutionException
     *             error
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupSelectByIndexPath(int xPos, String xUnits, int yPos,
            String yUnits, String indexPath, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        this.selectContextMenuItemByIndex(indexPath, xPos, xUnits, yPos,
                yUnits, button, false, null, false, false);
    }

    /**
     * Selects an item in the popup menu
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param textPath
     *            path of item texts
     * @param operator
     *            operator used for matching
     * @param button
     *            MouseButton
     * @throws StepExecutionException
     *             error
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupSelectByTextPath(final int xPos, final String xUnits,
            final int yPos, final String yUnits, String textPath,
            String operator, int button) throws StepExecutionException,
            RobotException, RemoteServerException {
        initContextMenu(xPos, xUnits, yPos, yUnits, textPath, operator, button, 
                false);
        String[] namePath = textPath.split("/");

        for (int i = 0; i < namePath.length; i++) {
            selectMenuItemByName(namePath[i], operator, false, null);
        }
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is enabled.
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param indexPath
     *            the menu item to verify
     * @param enabled
     *            for checking enabled or disabled
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifyEnabledByIndexPath(int xPos, String xUnits,
            int yPos, String yUnits, String indexPath, boolean enabled,
            int button) throws StepExecutionException, RobotException,
            RemoteServerException {
        this.selectContextMenuItemByIndex(indexPath, xPos, xUnits, yPos,
                yUnits, button, true, getCheckEnablementID(), false, true);

        Verifier.equals(enabled, m_checkResult);
    }

    /**
     * Selects a menu through the index path.
     * 
     * @param idxPath
     *            the index path
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent value
     * @param button
     *            the button to click
     * @param checkOnly
     *            if the menu should be checked
     * @param checkProperty
     *            the check property name
     * @param checkMouse
     *            if the mouse pos should be checked
     * @param closeMenu
     *            if the menu should be closed afterwards           
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    private void selectContextMenuItemByIndex(String idxPath, int xPos,
            String xUnits, int yPos, String yUnits, int button,
            boolean checkOnly, String checkProperty, boolean checkMouse, 
            boolean closeMenu)
        throws RobotException, RemoteServerException {

        boolean xAbsolute = xUnits.equalsIgnoreCase(POS_UNIT_PIXEL);
        boolean yAbsolute = yUnits.equalsIgnoreCase(POS_UNIT_PIXEL);
        String locator = this.getComponent().getLocator();
        int[] path = createPathIndexArray(idxPath);
        
        try {
            m_checkResult = getRobot().selectContextMenuItemByIndexpath(locator,
                    path, button, checkOnly,
                    checkProperty, getRobot().getBounds(ABSTRACT_CLASSNAME,
                            locator), xPos, xAbsolute, yPos, yAbsolute,
                            checkMouse);
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } finally {
            if (closeMenu) {
                getRobot().closeMenu(locator, path.length);
            }
        }
    }

    /**
     * Opens the ContextMenu for a component
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param textPath
     *            path of item indices
     * @param operator
     *            the operator
     * @param button
     *            the button to click
     * @param checkMouse
     *             if the mouse position should be checked
     * @throws StepExecutionException
     *             the {@link StepExecutionException}
     * @throws RobotException
     *             the {@link RobotException}
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     */
    private void initContextMenu(final int xPos, final String xUnits,
            final int yPos, final String yUnits, String textPath,
            String operator, int button, boolean checkMouse) 
        throws StepExecutionException, RobotException, RemoteServerException {

        String locator = this.getComponent().getLocator();
        Rectangle bounds = getRobot().getBounds(ABSTRACT_CLASSNAME, locator);
        boolean xAbsolute = xUnits.equalsIgnoreCase(POS_UNIT_PIXEL);
        boolean yAbsolute = yUnits.equalsIgnoreCase(POS_UNIT_PIXEL);
        getRobot().initContextMenu(locator, button, bounds, xPos, xAbsolute,
                yPos, yAbsolute, checkMouse);
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is enabled.
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param textPath
     *            the menu item to verify
     * @param operator
     *            operator used for matching
     * @param enabled
     *            for checking enabled or disabled
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifyEnabledByTextPath(final int xPos,
            final String xUnits, final int yPos, final String yUnits,
            String textPath, String operator, boolean enabled, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        initContextMenu(xPos, xUnits, yPos, yUnits, textPath, operator, button, 
                false);
        String[] namePath = textPath.split("/");
        
        try {
            for (int i = 0; i < namePath.length - 1; i++) {
                selectMenuItemByName(namePath[i], operator, false, null);
            }
            //last item to check
            selectMenuItemByName(namePath[namePath.length - 1], operator, true,
                    getCheckEnablementID());
            
            Verifier.equals(enabled, m_checkResult);
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } catch (StepVerifyFailedException svfe) {
            throw svfe;
        } catch (StepExecutionException see) {
            throw see;
        } finally {
            String locator = this.getComponent().getLocator();
            getRobot().closeMenu(locator, namePath.length);
        }
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry exists.
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param indexPath
     *            the menu item to verify
     * @param exists
     *            for checking if entry exists
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifyExistsByIndexPath(int xPos, String xUnits,
            int yPos, String yUnits, String indexPath, boolean exists,
            int button) throws StepExecutionException, RobotException,
            RemoteServerException {
        try {
            this.selectContextMenuItemByIndex(indexPath, xPos, xUnits, yPos,
                    yUnits, button, true, m_checkExistenceID, false, true);
        } catch (StepExecutionException e) {
            // item not found
            m_checkResult = false;
        }
        Verifier.equals(exists, m_checkResult);
    }

    /**
     * Creates the necessary int array for popup navigation through indexes
     * 
     * @param stringPath
     *            the index path as a string
     * @return the index path
     */
    private int[] createPathIndexArray(String stringPath) {
        String[] stringIndexPath = stringPath.split("/");
        int[] intPath = new int[stringIndexPath.length];

        for (int i = 0; i < stringIndexPath.length; i++) {
            intPath[i] = Integer.parseInt(stringIndexPath[i]);
        }

        return intPath;
    }

    /**
     * Checks if the specified context menu entry is selected.
     * 
     * @param indexPath
     *            the menu item to verify
     * @param selected
     *            for checking if entry is selected
     * @param button
     *            MouseButton
     */
    public void rcPopupVerifySelectedByIndexPath(String indexPath,
            boolean selected, int button) throws StepExecutionException,
            RobotException, RemoteServerException {
        this.selectContextMenuItemByIndex(indexPath, 50, "", 50, "", button,
                true, m_checkSelectionID, true, true);

        Verifier.equals(selected, m_checkResult);
    }

    /**
     * Presses or releases the given modifier.
     * 
     * @param modifier
     *            the modifier.
     * @param press
     *            if true, the modifier will be pressed. if false, the modifier
     *            will be released.
     */
    protected void pressOrReleaseModifiers(String modifier, boolean press) {
        final StringTokenizer modTok = new StringTokenizer(
                KeyStrokeUtil.getModifierString(modifier), " "); //$NON-NLS-1$
        while (modTok.hasMoreTokens()) {
            final String mod = modTok.nextToken();
            final int keyCode = KeyCodeConverter.getKeyCode(mod);
            if (press) {
                getRobot().keyPress(null, keyCode);
            } else {
                getRobot().keyRelease(null, keyCode);
            }
        }
    }

    /**
     * Selects an item in the popup menu
     * 
     * @param textPath
     *            path of item texts
     * @param operator
     *            operator used for matching
     * @param button
     *            MouseButton
     * @throws StepExecutionException
     *             error the StepExecutionException
     * @throws RemoteServerException
     *             the RemoteServerException
     * @throws RobotException
     *             the RobotException
     */
    public void rcPopupSelectByTextPath(String textPath, String operator,
            int button) throws StepExecutionException, RobotException,
            RemoteServerException {
        // search on the Win side for the context menu, need to be done as the
        // first step !
        initContextMenu(50, "", 50, "", textPath, operator, button, true);
        String[] namePath = textPath.split("/");

        for (int i = 0; i < namePath.length; i++) {
            selectMenuItemByName(namePath[i], operator, false, null);
        }
    }

    /**
     * Checks if the specified context menu entry is selected.
     * 
     * @param textPath
     *            the menu item to verify
     * @param operator
     *            operator used for matching
     * @param selected
     *            for checking if entry is selected
     * @param button
     *            MouseButton
     */
    public void rcPopupVerifySelectedByTextPath(String textPath,
            String operator, boolean selected, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        initContextMenu(50, "", 50, "", textPath, operator, button, true);
        String[] namePath = textPath.split("/");
        try {
            for (int i = 0; i < namePath.length - 1; i++) {
                selectMenuItemByName(namePath[i], operator, false, null);
            }

            selectMenuItemByName(namePath[namePath.length - 1], operator, true,
                    m_checkSelectionID);
            Verifier.equals(selected, m_checkResult);
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } catch (StepVerifyFailedException svfe) {
            throw svfe;
        } catch (StepExecutionException see) {
            throw see;
        } finally {
            String locator = this.getComponent().getLocator();
            getRobot().closeMenu(locator, namePath.length);
        }
    }

    /**
     * Checks if the specified context menu entry is enabled.
     * 
     * @param indexPath
     *            the menu item to verify
     * @param enabled
     *            for checking enabled or disabled
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifyEnabledByIndexPath(String indexPath,
            boolean enabled, int button) throws RobotException,
            RemoteServerException {
        this.selectContextMenuItemByIndex(indexPath, 50, "", 50, "", button,
                true, getCheckEnablementID(), true, true);

        Verifier.equals(enabled, m_checkResult);
    }

    /**
     * Checks if the specified context menu entry is enabled.
     * 
     * @param textPath
     *            the menu item to verify
     * @param operator
     *            operator used for matching
     * @param enabled
     *            for checking enabled or disabled
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifyEnabledByTextPath(String textPath,
            String operator, boolean enabled, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        initContextMenu(50, "", 50, "", textPath, operator, button, true);
        String[] namePath = textPath.split("/");

        try {
            for (int i = 0; i < namePath.length - 1; i++) {
                selectMenuItemByName(namePath[i], operator, false, null);
            }

            selectMenuItemByName(namePath[namePath.length - 1], operator, true,
                    getCheckEnablementID());
            Verifier.equals(enabled, m_checkResult); 
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } catch (StepVerifyFailedException svfe) {
            throw svfe;
        } catch (StepExecutionException see) {
            throw see;
        } finally {
            String locator = this.getComponent().getLocator();
            getRobot().closeMenu(locator, namePath.length);
        }
    }

    /**
     * Checks if the specified context menu entry exists.
     * 
     * @param indexPath
     *            the menu item to verify
     * @param exists
     *            for checking if entry exists
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     * @throws RobotException
     *             the {@link RobotException}
     */
    public void rcPopupVerifyExistsByIndexPath(String indexPath,
            boolean exists, int button) throws StepExecutionException,
            RobotException, RemoteServerException {
        try {
            this.selectContextMenuItemByIndex(indexPath, 50, "", 50, "",
                    button, true, m_checkExistenceID, true, true);
        } catch (StepExecutionException e) {
            // item not found
            m_checkResult = false;
        }
        Verifier.equals(exists, m_checkResult);
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry exists.
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param textPath
     *            the menu item to verify
     * @param operator
     *            operator used for matching
     * @param exists
     *            for checking if entry exists
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifyExistsByTextPath(final int xPos,
            final String xUnits, final int yPos, final String yUnits,
            String textPath, String operator, boolean exists, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        String[] namePath = textPath.split("/");
        try {
            initContextMenu(xPos, xUnits, yPos, yUnits, textPath, operator,
                    button, false);
            // open / select all items except the last one which will be checked
            for (int i = 0; i < namePath.length - 1; i++) {
                selectMenuItemByName(namePath[i], operator, false, null);
            }

            selectMenuItemByName(namePath[namePath.length - 1], operator, true,
                    m_checkExistenceID);
        } catch (StepExecutionException e) {
            // item not found
            m_checkResult = false;
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } finally {
            String locator = this.getComponent().getLocator();
            getRobot().closeMenu(locator, namePath.length);
        }
        Verifier.equals(exists, m_checkResult);
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is selected.
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param indexPath
     *            the menu item to verify
     * @param selected
     *            for checking if entry is selected
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifySelectedByIndexPath(int xPos, String xUnits,
            int yPos, String yUnits, String indexPath, boolean selected,
            int button) throws StepExecutionException, RobotException,
            RemoteServerException {
        this.selectContextMenuItemByIndex(indexPath, xPos, xUnits, yPos,
                yUnits, button, true, m_checkSelectionID, false, true);

        Verifier.equals(selected, m_checkResult);
    }

    /**
     * Opens the popup menu at the given position relative the current component
     * and checks if the specified context menu entry is selected.
     * 
     * @param xPos
     *            what x position
     * @param xUnits
     *            should x position be pixel or percent values
     * @param yPos
     *            what y position
     * @param yUnits
     *            should y position be pixel or percent values
     * @param textPath
     *            the menu item to verify
     * @param operator
     *            operator used for matching
     * @param selected
     *            for checking if entry is selected
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     * @throws RobotException
     */
    public void rcPopupVerifySelectedByTextPath(final int xPos,
            final String xUnits, final int yPos, final String yUnits,
            String textPath, String operator, boolean selected, int button)
        throws StepExecutionException, RobotException,
            RemoteServerException {
        initContextMenu(xPos, xUnits, yPos, yUnits, textPath, operator, button, 
                false);
        String[] namePath = textPath.split("/");
        try {
            for (int i = 0; i < namePath.length - 1; i++) {
                selectMenuItemByName(namePath[i], operator, false, null);
            }
            selectMenuItemByName(namePath[namePath.length - 1], operator, true,
                    m_checkSelectionID);
            Verifier.equals(selected, m_checkResult);
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } catch (StepVerifyFailedException svfe) {
            throw svfe;
        } catch (StepExecutionException see) {
            throw see;
        } finally {
            String locator = this.getComponent().getLocator();
            getRobot().closeMenu(locator, namePath.length);
        }
    }

    /**
     * Checks if the specified context menu entry exists.
     * 
     * @param textPath
     *            the menu item to verify
     * @param operator
     *            operator used for matching
     * @param exists
     *            for checking if entry exists
     * @param button
     *            MouseButton
     * @throws RemoteServerException
     *             the {@link RemoteServerException}
     * @throws RobotException
     *             the {@link RobotException}
     */
    public void rcPopupVerifyExistsByTextPath(String textPath, String operator,
            boolean exists, int button) throws StepExecutionException,
            RobotException, RemoteServerException {
        String[] namePath = textPath.split("/");
        try {
            initContextMenu(50, "", 50, "", textPath, operator, button, true);

            // open / select all items except the last one which will be checked
            for (int i = 0; i < namePath.length - 1; i++) {
                selectMenuItemByName(namePath[i], operator, false, null);
            }
            selectMenuItemByName(namePath[namePath.length - 1], operator, true,
                    m_checkExistenceID);
        } catch (StepExecutionException e) {
            // item not found
            m_checkResult = false;
        } catch (RobotException re) {
            throw re;
        } catch (RemoteServerException rse) {
            throw rse;
        } finally {
            String locator = this.getComponent().getLocator();
            getRobot().closeMenu(locator, namePath.length);
        }
        Verifier.equals(exists, m_checkResult);
    }

    /**
     * Searches the current opend menu items for the given name that matches
     * with the operator and selects the first hit.
     * 
     * @param menuItem
     *            the name of the menuItem that shall be selected
     * @param operator
     *            the operator
     * @param check
     *            check the menuItem
     * @param checkProperty
     *            the propertyname which will be checked
     * @throws RemoteServerException
     *             the RemoteServerException
     * @throws RobotException
     *             the RobotException
     */
    public void selectMenuItemByName(String menuItem, String operator,
            boolean check, String checkProperty) throws RobotException,
            RemoteServerException {
        String[] menuItems = getRobot().getCurrentMenuItems();
        boolean itemSelected = false;
        for (int i = 0; i < menuItems.length; i++) {
            if (MatchUtil.getInstance().match(menuItems[i], menuItem, operator))
            {
                m_checkResult = getRobot().selectMenuItemByName(menuItems[i],
                        check, checkProperty);
                itemSelected = true;
                break;
            }
        }
        // no select was performed so it means the item does not exits for our
        // match.
        if (!itemSelected) {
            throw new StepExecutionException(new Exception(
                    "MenuItem does not exist."));
        }
    }

    /**
     * Getter for check enablement constant
     * 
     *  @return the id
     */
    public String getCheckEnablementID() {
        return m_checkEnablementID;
    }
    
    /**
     * Getter for check existence constant
     * 
     *  @return the id
     */
    public String getCheckExistenceID() {
        return m_checkExistenceID;
    }
    
    /**
     * Getter for check Selection constant
     * 
     *  @return the id
     */
    public String getCheckSelectionID() {
        return m_checkSelectionID;
    }
    
    /**
     * Getter for check result
     * 
     * @return the check result
     */
    public boolean getCheckResult() {
        return m_checkResult;
    }    
    
    /**
     * Set the result
     * 
     * @param result the result
     */
    public void setCheckResult(boolean result) {
        m_checkResult = result;
    }
    
}
