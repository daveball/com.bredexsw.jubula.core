package com.bredexsw.guidancer.rc.win.implclasses;

import com.bredexsw.guidancer.rc.dotnet.components.controltype.TableWinForm;

/**
 * Impl class for WinForm-Tables
 */
public class TableWinFormImplClass extends TableImplClass {

    @Override
    TableWinForm getTable() {
        return (TableWinForm) getComponent();
    }

}
