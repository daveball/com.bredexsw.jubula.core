/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.jubula.tools.internal.constants.AUTServerExitConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * 
 * Communication class between the Java Win server and
 * the Win-Win server.
 * 
 * @author patrick
 *
 */
public class Communicator {
    
    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory.getLogger(Communicator.class);
    
    /** 
     * ID for timeout in communication with native Win RC.
     * This constant is also defined on the C# side
     * (c.b.g.rc.win.nativ.impl.ServerExceptionConstants)
     */
    private static final int TIMEOUT_ERROR_ID = 12;

    /** 
     * Name for timeout in communication with native Win RC.
     * This constant is also defined on the C# side
     * (c.b.g.rc.win.nativ.impl.ServerExceptionConstants)
     */
    private static final String TIMEOUT_ERROR_NAME = "TIMEOUT";

    /**
     * the port.
     */
    private int m_port;
    
    /**
     * the communication socket.
     */
    private Socket m_socket;
    
    /**
     * the server socket.
     */
    private ServerSocket m_serverSocket;
    
    /**
     * the printwriter.
     */
    private PrintWriter m_writer;
    
    /**
     * the bufferedreader.
     */
    private BufferedReader m_br;
    
    
    /**
     * Constructor 
     * 
     * @param port the tcp port.
     */
    public Communicator(int port) {
        this.m_port = port;
    }
    
    
    /**
     * Method to start the serversocket, contains timeout ( 30sec ) implementation.
     * 
     * @param connected boolean value for win server
     */
    public void initConnection(final AtomicBoolean connected) {
        //start server and wait in new thread
        final Thread initThread = new Thread(new Runnable() { //$NON-NLS-1$ 
            public void run() { 
                waitforconnection();
            }     
        }); 
        initThread.start();
        //timeout thread, checks every second if the Win server was connected
        Thread timeoutThread = new Thread(new Runnable() { //$NON-NLS-1$ 
            public void run() {
                int waittime = 0;                
                while (waittime < 60) {
                    //initThread is dead, that means the Win server is connected
                    if (!initThread.isAlive()) {
                        connected.set(true);
                        break;
                    }
                    waittime++;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        log.error("InterruptedException during Win server connection", e); //$NON-NLS-1$
                    }
                }
                if (!connected.get()) {
                    log.error("Timeout error during Win server connection");
                    System.exit(AUTServerExitConstants.AUT_START_ERROR);
                }
            }     
        }); 
        timeoutThread.start();
    }
    
    /**
     * Creates the ServerSocket with the given port and waits until the
     * Win server connected to the socket.
     */
    private void waitforconnection() {
        try {
            m_serverSocket = new ServerSocket(m_port);
            m_socket = m_serverSocket.accept();
            m_writer = new PrintWriter(
                    new OutputStreamWriter(m_socket.getOutputStream(), "UTF8"),
                    true);
//            m_writer = new PrintWriter(m_socket.getOutputStream(), true);
            m_br = new BufferedReader(new InputStreamReader(
                    m_socket.getInputStream()));
        } catch (IOException e) {
            log.error("IOException during Win server connection", e); //$NON-NLS-1$
        }        
    }
    
    /**
     * Sends a message to the Win server and returns the server answer.
     * Contains a 30sec. timeout implementation for the answer.
     * 
     * @param msgObj the message to send
     * @return return the server answer
     * @throws RemoteServerException 
     * 
     */
    public synchronized NativeAnswer sendMsg(NativeMessage msgObj)
        throws RemoteServerException {
        //Sending and serializing the MessageObj to JSON     
        JSONObject json = (JSONObject) JSONSerializer.toJSON(msgObj);
        m_writer.println(json.toString());
        m_writer.flush();

        final StringBuilder answer = new StringBuilder();
        //Thread waiting for the server answer
        final Thread answerThread = new Thread(new Runnable() { //$NON-NLS-1$ 
            public void run() {
                try {
                    answer.append(m_br.readLine());
                } catch (IOException e) {
                    log.error("IOException during waiting for server answer", e); //$NON-NLS-1$
                }
            }
        });
        answerThread.start();

        //timeout - Waiting 12 sec. for server answer
        int waittime = 0;
        while (waittime < 2400) {
            
            if (!answerThread.isAlive()) {
                if (answer.length() == 0) {
                    return new NativeAnswer("Server Closed");
                }
                JSONObject jsonAnswer = (JSONObject) JSONSerializer.
                        toJSON(answer.toString());
                boolean exceptionFlag = jsonAnswer.
                        getBoolean("exceptionFlag");
                
                if (!exceptionFlag) {
                    String responeString = jsonAnswer.getString("answer");
                    return new NativeAnswer(responeString);
                }
                int id = jsonAnswer.getInt("exceptionID");
                String name = jsonAnswer.getString("exceptionName");
                String msg = jsonAnswer.getString("exceptionMsg");
                String methodname = jsonAnswer.
                        getString("exceptionMethod");
                String classname = jsonAnswer.
                        getString("exceptionClass");
                
                NativeAnswer rmo = new NativeAnswer(id,
                        name, msg, methodname, classname);
                log.error("Server Exception occured: ", 
                        rmo.getServerException());
                throw rmo.getServerException();
            } 
            waittime++;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                log.error("InterruptedException during waiting for server answer", e); //$NON-NLS-1$
            }
        }
        //timeout occurred
        log.error("Timeout error during waiting for server answer"); //$NON-NLS-1$
        answerThread.interrupt();
        throw new RemoteServerException(TIMEOUT_ERROR_ID, TIMEOUT_ERROR_NAME, 
                "Timeout occurred during communication with native Win RC",  //$NON-NLS-1$
                "sendMsg", Communicator.class.getName()); //$NON-NLS-1$
    }
}
    
    
    