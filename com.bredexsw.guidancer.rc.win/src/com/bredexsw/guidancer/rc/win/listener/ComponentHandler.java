/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.listener;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jubula.rc.common.exception.ComponentNotFoundException;
import org.eclipse.jubula.tools.internal.messagehandling.MessageIDs;
import org.eclipse.jubula.tools.internal.objects.ComponentIdentifier;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.rc.dotnet.components.AUTDotNetHierarchy;
import com.bredexsw.guidancer.rc.dotnet.components.AbstractDotNetComponent;
import com.bredexsw.guidancer.rc.dotnet.components.FindDotNetComponentsBP;
import com.bredexsw.guidancer.rc.win.WinAUTServer;
import com.bredexsw.guidancer.rc.win.communication.Communicator;
import com.bredexsw.guidancer.rc.win.driver.RobotConstants;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeAnswer;
import com.bredexsw.jubula.rc.common.nativ.communication.NativeMessage;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;


/**
 * Handels all action for the components and the AUTHierarchy
 */
public class ComponentHandler {
    
    /** the bp to find components */
    private static FindDotNetComponentsBP findBP = new FindDotNetComponentsBP();
    
    /**
     * <code>log</code>: the logger
     */
    private static Logger log = LoggerFactory.getLogger(ComponentHandler.class);
    
    /**
     * Communication object.
     */
    private static Communicator com = WinAUTServer.getWinCommunicator();
    
    /**
     * The hierarchy that represents the AUT.
     */
    private static AUTDotNetHierarchy autHierarchy = new AUTDotNetHierarchy();
    
    /**
     * Private Constructor
     */
    private ComponentHandler() {
        
    }
    
    /**
     * Refreshes the autHierarchy by requesting a new Hierarchy from the Win server
     */
    public static void rebuildHierarchy() {
        autHierarchy.clean();
        NativeMessage msgObj = new NativeMessage(RobotConstants.HIERARCHY_CLASS,
                RobotConstants.GET_HIERARCHY);
        try {
            NativeAnswer rmo = com.sendMsg(msgObj);
            autHierarchy.add(rmo.getStringList());
        } catch (RemoteServerException e) {
            //empty
        }
    }
    
    /**
     * Finds a component to the given Identifier
     * 
     * @param compID the Identifier
     * @return the component
     * @throws ComponentNotFoundException exception if component not found
     */
    public static AbstractDotNetComponent findComponent(
            IComponentIdentifier compID) throws ComponentNotFoundException {

        rebuildHierarchy();
        String locator = (String) findBP.findComponent(compID, autHierarchy);

        if (locator == null) {
            rebuildHierarchy();
            locator = (String) findBP.findComponent(compID, autHierarchy);
        }

        if (locator == null) {
            throw new ComponentNotFoundException("component not found: "
                    + compID, MessageIDs.E_COMPONENT_NOT_FOUND);
        }

        return findComponent(locator);

    }
    
    /**
     * Searches for a {@link AbstractDotNetComponent} in the AUTHierarchy.
     * 
     * @param ci the identifier
     * @param timeout timeout value
     * @return the {@link AbstractDotNetComponent} object
     * @throws ComponentNotFoundException 
     */
    public static Object findComponent(IComponentIdentifier ci, int timeout) 
        throws ComponentNotFoundException {
        
        long start = System.currentTimeMillis();
        AbstractDotNetComponent comp = null;
        boolean timoutExpired = false;
        
        do {
            try {
                comp = findComponent(ci);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // ok
                }
                timoutExpired = System.currentTimeMillis() - start > timeout;
                if (timoutExpired && comp == null) {
                    comp = findComponent(ci);
                }
            } catch (ComponentNotFoundException e) {
                // ignore
            } finally {
                timoutExpired = System.currentTimeMillis() - start > timeout;
            }
        }        
        while (comp == null && !timoutExpired);
        
        if (comp == null) {
            throw new ComponentNotFoundException(
                    "component not found: " + ci, //$NON-NLS-1$
                    MessageIDs.E_COMPONENT_NOT_FOUND);
        }
        return comp;
    }
    
    /**
     * 
     * Finds a component to the given locator
     * 
     * @param locatorPath the locator
     * @return the component
     */
    public static AbstractDotNetComponent findComponent(String locatorPath) {
        AbstractDotNetComponent component =
                autHierarchy.getComponent(locatorPath);
        
        if (component == null) {
            rebuildHierarchy();
            component = 
                    autHierarchy.getComponent(locatorPath);
        }
        if (component == null) {
            log.error("Even after rebuilding the hierarchy the locatorPath: \"" + locatorPath //$NON-NLS-1$
                    + "\" could not be found in the hierarchy."); //$NON-NLS-1$
        }
        return component;
    }
    
    /**
     * Creates the hierarchy / path to the root container
     * 
     * @param comp the component
     * @return the hierarchy / path
     */
    public static List<String> getPathToRoot(AbstractDotNetComponent comp) {
        String[] pathArr = comp.getLocator().split("/");
        ArrayList<String> pathList = new ArrayList<String>();
        
        for (int i = 0; i < pathArr.length; i++) {
            String name = pathArr[i].substring(0, pathArr[i].lastIndexOf("["));
            pathList.add(name);
        }

        return pathList;     
    }
    
    /**
     * @param fqPath
     *            the fully qualified PATH of the web component to get an identifier for
     * @return a component identifier for the given web component; may return
     *         null if no component identifier could be created
     */
    public static IComponentIdentifier getComponentIdentifier(String fqPath) {
        AbstractDotNetComponent comp = findComponent(fqPath);
        if (comp != null) {
            IComponentIdentifier compID = new ComponentIdentifier();
            compID.setComponentClassName(comp.getClass().getName());
            compID.setSupportedClassName(compID.getComponentClassName());
            compID.setHierarchyNames(getPathToRoot(comp));
            compID.setNeighbours(autHierarchy.getComponentContext(comp));

            if (fqPath.equals(findBP.findComponent(compID, autHierarchy))) {
                compID.setEqualOriginalFound(true);
            }
            
            return compID;
        }
        return null;
    }
    
   /**
    * 
    * @return the hierarchy
    */
    public static AUTDotNetHierarchy getHierarchy() {
        return autHierarchy;
    }
    
}
