/*******************************************************************************
 * Copyright (c) 2004, 2012 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.rc.win.listener;

import org.eclipse.jubula.rc.common.listener.AUTEventListener;
import org.eclipse.jubula.tools.internal.objects.IComponentIdentifier;

import com.bredexsw.guidancer.rc.dotnet.components.FindDotNetComponentsBP;
import com.bredexsw.guidancer.rc.win.driver.WinOMRobotImpl;

/**
 * Listener for mapping commands
 */
public class MappingListener implements AUTEventListener {
    /**
     * Robot for OM Actions / Messages
     */
    private WinOMRobotImpl m_omRobot;

    /**
     * 
     * Initialise the OM Robot and starts the OM-Mode
     *
     */
    public void initOMRobot() {
        if (m_omRobot == null) {
            m_omRobot = new WinOMRobotImpl();
            m_omRobot.startOM();
        }
    }
    
    /**
     * Closes the Robot and OM Mode
     */
    public void deserializeOMRobot() {
        if (m_omRobot != null) {
            m_omRobot.stopOM();
            m_omRobot = null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public long[] getEventMask() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void cleanUp() {
        
    }

    /**
     * {@inheritDoc}
     */
    public void update() {
        
    }

    /**
     * {@inheritDoc}
     */
    public boolean highlightComponent(IComponentIdentifier compID) {
        FindDotNetComponentsBP bp = new FindDotNetComponentsBP();
        String locator = (String) bp.findComponent(compID, 
                ComponentHandler.getHierarchy());
        
        if (locator == null) {
            ComponentHandler.rebuildHierarchy();
            locator = (String) bp.findComponent(compID, 
                    ComponentHandler.getHierarchy());
            if (locator == null) {
                return false;
            }
        } 
        
        return m_omRobot.highlightComponent(locator);
    }

}
