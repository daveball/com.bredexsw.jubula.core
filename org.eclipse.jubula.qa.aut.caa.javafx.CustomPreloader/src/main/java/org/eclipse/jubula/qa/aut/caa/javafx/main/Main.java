package org.eclipse.jubula.qa.aut.caa.javafx.main;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Class for the main application with a button.
 * 
 * @author Bredex GmbH
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(new Button("Application")));
        primaryStage.show();
    }

    /**
     * Main Method
     * @param args arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
