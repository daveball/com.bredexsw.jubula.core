#!/bin/bash

umask 002

cd com.bredexsw.jubula.core/com.bredexsw.jubula.qa.aut.caa.mobile.ios/
ant cleanBuild
cd ../../
rm -rf /tmp/ios_caa
mkdir /tmp/ios_caa
cp -R com.bredexsw.jubula.core/com.bredexsw.jubula.qa.aut.caa.mobile.ios/target/i386 /tmp/ios_caa
