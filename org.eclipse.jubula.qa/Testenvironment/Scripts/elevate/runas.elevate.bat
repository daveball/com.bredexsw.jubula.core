@REM Usage: runas.elevate.bat USER_NAME PATH [ARGS]
@REM Runs a command line with given PATH, optional arguments, highest rights and given USER_NAME.
@ECHO off
SET user_name=%1
SET script_dir=%~dp0

SET args=%2
shift

:loop_args
IF [%2]==[] GOTO loop_end
SET args=%args% %2
SHIFT
GOTO loop_args
:loop_end

ECHO on

RunAs.exe /user:%user_name% /savecred "cscript.exe %script_dir%\elevate.js %args%"
