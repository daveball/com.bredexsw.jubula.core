// Runs a command line with given PATH, optional arguments and highest rights.
if (WScript.Arguments.Length >= 1) {
	var path = WScript.Arguments(0);
	var args = "";
	for (var i = 1; i < WScript.Arguments.Length; i++) {
		if (i > 1) {
			args += " ";
		}
		args += WScript.Arguments(i);
	}
	WScript.Echo("Elevate: " + path + " " + args);
	// @see http://msdn.microsoft.com/en-us/library/windows/desktop/gg537745%28v=vs.85%29.aspx
	// @see http://msdn.microsoft.com/en-us/library/windows/desktop/bb762153%28v=vs.85%29.aspx
	new ActiveXObject("Shell.Application").ShellExecute(path, args, "", "RunAs");
	WScript.Sleep(10000);
} else {
	WScript.Echo("Usage: cscript elevate.js PATH [ARGS]");
	WScript.Echo("Runs a command line with given PATH, optional arguments and highest rights.");
}
