<?xml version="1.0" encoding="UTF-8"?>
<!-- ======================================================================
     org.eclipse.jubula.ci.install.ite
     a script for CI to install the ITE
     ====================================================================== -->
<project name="org.eclipse.jubula.ci.install.ite" basedir=".">

	<import file="build.xml" />
	<import file="elevate/build.xml"/>

	<description>begin namespace install: Script for installing ITE on Windows.</description>

	<description>Initialize properties</description>

	<target name="init.pre" description="OS independent install properties">
		<property name="user.home.jubula.dir" value="${user.home}/.jubula" />
		<property name="hudson.url.folder" value="http://build.dev.bredex.local:8080/hudson/job/com.bredexsw.ite.releng.prod.installer/lastSuccessfulBuild/artifact/release/installer" />
		<property name="ite.uninstall.timeout.seconds" value="600"/>
		<property name="ite.installer.timeout.seconds" value="600"/>
	</target>

	<target name="init.env.win" description="provide environment variables and define program files 64 bit folder">
		<property environment="env" description="provide all environment variables with prefix env" />
		<property name="env.ProgramFiles64" value="${env.ProgramFiles}" />
	</target>

	<target name="init.env.win32" depends="init.env.win,init.env.win64" description="provide environment variables for 32 bit windows">
		<property name="env.ProgramFiles32" value="${env.ProgramFiles}" />
	</target>

	<target name="init.env.win64" depends="init.env.win" if="env.ProgramFiles(x86)" description="provide environment variables for 64 bit windows">
		<property name="env.ProgramFiles32" value="${env.ProgramFiles(x86)}" />
	</target>

	<target name="init.win" depends="init.pre" description="provide windows specific properties">
		<property name="response.var.file" value="response-win.varfile" />
		<property name="ite.uninstall.file.name" value="uninstall.exe" />
	</target>

	<target name="init.win8" depends="init.pre" description="provide windows specific properties">
		<property name="response.var.file" value="response-win8.varfile" />
		<property name="ite.uninstall.file.name" value="uninstall.exe" />
	</target>

	<target name="init.ite.win32.win8" depends="init.env.win32,init.win8">
		<property name="ite.installer.file.name" value="installer-jubula_win32-win32-x86.exe" />
	</target>

	<target name="init.ite.win32" depends="init.env.win32,init.win">
		<property name="ite.installer.file.name" value="installer-jubula_win32-win32-x86.exe" />
	</target>

	<target name="init.ite.win64" depends="init.env.win64,init.win">
		<property name="ite.installer.file.name" value="installer-jubula_win32-win32-x86_64.exe" />
	</target>

	<target name="init.ite.win.normal">
		<property name="ite.dir" value="C:\ite" />
	</target>

	<target name="init.ite.win.current">
		<property name="ite.dir" value="K:/guidancer/Workspace/hu_snapshot/current/platforms/win32.win32.x86" />
	</target>

	<target name="init.ite.win32.admin" depends="init.ite.win32.win8">
		<property name="ite.dir" value="${env.ProgramFiles32}\ite" />
	</target>

	<target name="init.ite.win64.admin" depends="init.ite.win64">
		<property name="ite.dir" value="${env.ProgramFiles64}\ite" />
	</target>

	<target name="init.win.admin">
		<property name="elevate.user.name" value="bxtest" />
	</target>

	<target name="init.env.lin" description="provide environment variables">
		<property environment="env" description="provide all environment variables with prefix env" />
	</target>

	<target name="init.lin" depends="init.pre" description="provide windows specific properties">
		<property name="response.var.file" value="response-lin.varfile" />
		<property name="ite.uninstall.file.name" value="uninstall" />
	</target>

	<target name="init.ite.lin32" depends="init.env.lin,init.lin">
		<property name="ite.installer.file.name" value="installer-jubula_linux-gtk-x86.sh" />
	</target>

	<target name="init.ite.lin64" depends="init.env.lin,init.lin">
		<property name="ite.installer.file.name" value="installer-jubula_linux-gtk-x86_64.sh" />
	</target>

	<target name="init.ite.lin.normal">
		<property name="ite.dir" value="/tmp/ite" />
	</target>

	<target name="init.post">
		<fail unless="ite.dir" />
		<fail unless="hudson.url.folder" />
		<fail unless="ite.uninstall.file.name" />
		<fail unless="ite.installer.file.name" />
		<property name="installer.hudson.url" value="${hudson.url.folder}/${ite.installer.file.name}" />
		<property name="ite.uninstall.path" value="${ite.dir}/${ite.uninstall.file.name}" />
		<property name="ite.uninstall.last.dir" value="${ite.dir}/.install4j" />
		<property name="ite.uninstall.jre" value="${ite.dir}/jre" />
		<property name="ite.installer.last.dir" value="${ite.dir}/.install4j" />
		<property name="installer.args" value='-q -dir "${ite.dir}" -varfile ${basedir}/${response.var.file}' />
	</target>

	<description>Uninstall ITE</description>

	<target name="check.isInstalled" description="Set the isInstalled variable, if the ITE has been previously installed.">
		<fail unless="ite.uninstall.path" />
		<condition property="isInstalled">
			<available file="${ite.uninstall.path}" type="file" />
		</condition>
	</target>

	<target name="ite.uninstall" depends="check.isInstalled" if="isInstalled" description="Delete the previously installed ITE with admin rights">
		<fail unless="ite.uninstall.path" />
		<echo message="Starting ITE uninstall..." level="info" />
		<exec executable="${ite.uninstall.path}" failonerror="false">
			<arg value="-q" />
		</exec>
	</target>

	<description>Wait until uninstall/install has been finished.</description>

	<target name="ite.uninstall.waitUntilHasFinished" description="wait until the installation directory has been deleted by uninstall process">
		<fail unless="ite.uninstall.last.dir" />
		<fail unless="ite.uninstall.jre" />
		<fail unless="ite.uninstall.timeout.seconds" />
		<echo message='Waiting until "${ite.uninstall.last.dir}" has been removed (timeout ${ite.uninstall.timeout.seconds}s)...' level="info" />
		<waitfor maxwait="${ite.uninstall.timeout.seconds}" maxwaitunit="second" timeoutproperty="ite.uninstall.waitUntilHasFinshed.failed">
			<and>
			<not>
				<available file="${ite.uninstall.last.dir}" type="dir" />
			</not>
			<not>
				<available file="${ite.uninstall.jre}" type="dir" />
			</not>
			</and>
		</waitfor>
		<fail if="ite.uninstall.waitUntilHasFinshed.failed" message="Timeout of ${ite.uninstall.timeout.seconds}s reached: ${ite.uninstall.last.dir} has not been deleted by uninstaller" />
		<!-- sleep here 3 seconds to prevent file locks on ${ite.uninstall.last.dir} that may be caused by ant checks -->
		<sleep seconds="3" />
	</target>

	<target name="ite.installer.waitUntilHasFinished" description="wait until ITE has been installed">
		<fail unless="ite.installer.last.dir" />
		<fail unless="ite.installer.timeout.seconds" />
		<echo message='Waiting until "${ite.installer.last.dir}" exists (timeout ${ite.installer.timeout.seconds}s)...' level="info" />
		<waitfor maxwait="${ite.installer.timeout.seconds}" maxwaitunit="second" timeoutproperty="ite.installer.waitUntilHasFinshed.failed">
			<available file="${ite.installer.last.dir}" type="dir" />
		</waitfor>
		<fail if="ite.installer.waitUntilHasFinshed.failed" message="Timeout of ${ite.installer.timeout.seconds}s reached: ${ite.installer.last.dir} has not been created by installer" />
	</target>

	<target name="ite.installer.download" description="Download last installer from Hudson server">
		<fail unless="ite.installer.file.name" />
		<fail unless="installer.hudson.url" />
		<get src="${installer.hudson.url}" dest="${basedir}/${ite.installer.file.name}" username="hudson" password="hudson" />
	</target>

	<description>Main target to install the ITE depending on the defined variables.</description>

	<target name="ite" depends="init.post" description="Install ITE depending on defined variables.">
		<fail unless="ite.dir" />
		<fail unless="response.var.file" />
		<fail unless="user.home.jubula.dir" />
		<fail unless="ite.installer.file.name" />
		<fail unless="installer.args" />
		<delete dir="${user.home.jubula.dir}" failonerror="false" />
		<antcall target="ite.uninstall" />
		<antcall target="ite.installer.download" />
		<!-- added listing all running processes directly before deleting ${ite.dir} -->
		<exec executable="tasklist" failonerror="false">
			<arg value="-V" />
		</exec>
		<!-- added verbose output here, because we had an unknown nighty problem deleting this folder -->
		<delete dir="${ite.dir}" verbose="yes" />
		<echo message="Starting ITE installer synchrone..." level="info" />
		<exec executable="${basedir}/${ite.installer.file.name}" failonerror="true">
			<arg line="${installer.args}" />
		</exec>
		<antcall target="modifyInstallation">
			<param name="installationDir" value="${ite.dir}" />
		</antcall>
	</target>

	<description>Targets for installing with normal user rights.</description>

	<description>public</description>
	<target name="ite.win32.normal" depends="init.ite.win32,init.ite.win.normal">
		<antcall target="ite" />
	</target>

	<description>public</description>
	<target name="ite.win64.normal" depends="init.ite.win64,init.ite.win.normal">
		<antcall target="ite" />
	</target>

	<description>public</description>
	<target name="ite.win32.current" depends="init.ite.win32,init.ite.win.current">
		<antcall target="ite" />
	</target>

	<description>public</description>
	<target name="ite.win64.current" depends="init.ite.win64,init.ite.win.current">
		<antcall target="ite" />
	</target>

	<description>Targets for installing with admin rights.</description>

	<description>public (recalled by target ite.win32.admin.runas)</description>
	<target name="ite.win32.admin" depends="init.ite.win32.admin">
		<antcall target="ite" />
	</target>

	<description>public (recalled by ite.win64.admin.runas)</description>
	<target name="ite.win64.admin" depends="init.ite.win64.admin">
		<antcall target="ite" />
	</target>

	<target name="ite.win.admin.runas" depends="init.post,init.win.admin" description="Starts this script with runas.elevate for installing ITE with admin rights.">
		<fail unless="elevate.user.name" />
		<fail unless="elevate.target" />
		<antcall target="elevate.executeTargetWithGivenUser">
			<param name="elevate.ant.file" value="${ant.file}" />
		</antcall>
		<antcall target="ite.uninstall.waitUntilHasFinished" />
		<antcall target="ite.installer.waitUntilHasFinished" />
		<antcall target="modifyInstallation.waitUntilHasFinished" />
		<concat description="print the output of the executed ant target">
			<path path="${user.home}/runas_log.txt" />
		</concat>
	</target>

	<description>public</description>
	<target name="ite.win32.admin.runas" depends="init.ite.win32.admin" description="public: Install 32-bit ITE with admin rights">
		<antcall target="ite.win.admin.runas">
			<param name="elevate.target" value="ite.win32.admin" />
		</antcall>
	</target>

	<description>public</description>
	<target name="ite.win64.admin.runas" depends="init.ite.win64.admin" description="public: Install 64-bit ITE with admin rights">
		<antcall target="ite.win.admin.runas">
			<param name="elevate.target" value="ite.win64.admin" />
		</antcall>
	</target>

	<description>public</description>
	<target name="ite.lin32.normal" depends="init.ite.lin32,init.ite.lin.normal">
		<antcall target="ite" />
	</target>

	<description>public</description>
	<target name="ite.lin64.normal" depends="init.ite.lin64,init.ite.lin.normal">
		<antcall target="ite" />
	</target>

</project>
