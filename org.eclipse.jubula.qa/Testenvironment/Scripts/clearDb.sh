#!/bin/bash
dir=`dirname $0`
sqlplus -L -S guidancertest/bxtest@bxutf @$dir/clearDb.sql > /dev/null
# since the sqlplus may return an error condition if the DB is already
# cleared which doesn't concern the caller, we just return an "all well"
# exit status
exit 0
