#!/bin/bash
#set -x
USER=testweek
PWD=bxtest

EXECDIR=/projects/guidancer/Workspace/hu_snapshot/current/platforms/lin.gtk.x86/jubula
DBTOOL=$EXECDIR/dbtool
DIR=`dirname $0`

#work around a problem with GUI dependencies
export DISPLAY=unixdev.bredex.de:1

# delete all projects but the test result summary
echo Start deleting all projects at `date`
$DBTOOL -debug -consolelog -deleteall -keepsummary -dburl jdbc:oracle:thin:@dbbxtest.dev.bredex.local:1521:bxutf -dbuser $USER -dbpw $PWD -data @none
echo Done deleting all projects at `date`

UNBOUND=`ls -1 $DIR/*.xml | grep -i unbound`
BOUND=`ls -1 $DIR/*.xml | grep -iv unbound`
echo Start importing at `date`
# import project 
for IMPORT in $UNBOUND $BOUND
do
echo Importing $IMPORT
$DBTOOL -import $IMPORT \
	-dburl jdbc:oracle:thin:@dbbxtest.dev.bredex.local:1521:bxutf -dbuser $USER -dbpw $PWD -data @none
done
echo Done importing at `date`