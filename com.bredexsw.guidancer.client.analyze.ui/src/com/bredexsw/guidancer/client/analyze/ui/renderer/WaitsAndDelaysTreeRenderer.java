package com.bredexsw.guidancer.client.analyze.ui.renderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jubula.client.analyze.internal.AnalyzeParameter;
import org.eclipse.jubula.client.analyze.internal.AnalyzeResult;
import org.eclipse.jubula.client.analyze.ui.definition.IResultRendererUI;
import org.eclipse.jubula.client.core.model.ICapPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.TestResultNode;
import org.eclipse.jubula.client.ui.constants.IconConstants;
import org.eclipse.jubula.client.ui.editors.TestResultViewer;
import org.eclipse.jubula.client.ui.rcp.businessprocess.UINodeBP;
import org.eclipse.jubula.client.ui.rcp.views.TestResultTreeView;
import org.eclipse.jubula.tools.internal.i18n.CompSystemI18n;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import com.bredexsw.guidancer.client.analyze.ui.i18n.Messages;

/**
 * @author volker
 *
 */
public class WaitsAndDelaysTreeRenderer implements IResultRendererUI {

    /**
     * The given AnalyzeResult
     */
    private AnalyzeResult m_analyzeResult;
    
    /**
     * The TopControl
     */
    private Control m_topControl;
    /**
     * 
     */
    public WaitsAndDelaysTreeRenderer() {
    }
    /**
     * @return The AnalyzeResult
     */
    public AnalyzeResult getAnalyzeResult() {
        return m_analyzeResult;
    }
    
    /**
     * @param result The given AnalyzeResult
     */
    public void setAnalyzeResult(AnalyzeResult result) {
        this.m_analyzeResult = result;
    }
    /**
     * {@inheritDoc}
     */
    public Control getTopControl() {
        return m_topControl;
    }
    /**
     * @param control The given Control
     */
    public void setTopControl(Control control) {
        this.m_topControl = control;
    }

    /**
     * {@inheritDoc}
     */
    public void renderResult(AnalyzeResult result, Composite composite) {
        
        setAnalyzeResult(result);
        createViewer(composite);
    }

    /**
     * creates the viewer
     * @param composite The given composite
     */
    private void createViewer(Composite composite) {
        TreeModel tm = new TreeModel(getAnalyzeResult());
        
        TreeViewer tv = new TreeViewer(composite, SWT.BORDER);
        tv.getTree().setHeaderVisible(true);
        tv.setComparator(new ViewerComparator() {
            
            @Override
            public int compare(Viewer viewer, Object e1, Object e2) {

                TreeElement t1 = (TreeElement) e1;
                TreeElement t2 = (TreeElement) e2;
                
                return Integer.valueOf(t2.getColumn2()).compareTo(
                        Integer.valueOf(t1.getColumn2()));
            }
        });
        tv.setContentProvider(new WaitAndDelayTreeViewerContentProvider(tm));
        TreeViewerColumn col1 = new TreeViewerColumn(tv, SWT.LEFT);
        col1.getColumn().setResizable(true);
        col1.getColumn().setText(Messages.Action);
        col1.getColumn().setWidth(300);

        col1.setLabelProvider(new WaitsAndDelaysTreeLabelProvider1(tm));
        
        TreeViewerColumn col2 = new TreeViewerColumn(tv, SWT.RIGHT);
        col2.getColumn().setResizable(true);
        col2.getColumn().setText(Messages.AmountOfWaits);
        col2.getColumn().setWidth(150);

        col2.setLabelProvider(new WaitsAndDelaysTreeLabelProvider2(tm));
        tv.addDoubleClickListener(new WaitsAndDelaysDoubleClickListener());
        tv.setInput(tm);
        
        if (tm.getParentElements().size() > 0) {
            setTopControl(tv.getControl());
        } else {
            Label label = new Label(composite, SWT.BORDER);
            label.setText(Messages.NoWaitOrDelayFound);
            setTopControl(label);
        }
    }

    /**
     * 
     * @author volker
     *
     */
    public class TreeModel {
     
        /**
         * A List that contains the childElements of this Tree
         */
        private List<TreeElement> m_childElements = 
                new ArrayList<TreeElement>();
        
        /**
         * A List that contains the childElements that will be shown in this Tree
         */
        private List<TreeElement> m_shownChildElements =
                new ArrayList<TreeElement>();
        /**
         * A List that contains the childElements of this Tree
         */
        private List<TreeElement> m_parentElements = 
                new ArrayList<TreeElement>();
        /**
         * The ResultMap
         */
        private Map<TestResultNode, String> m_result;
        
        /**
         * The Parameters of the Analyze. Can be null
         */
        private List<AnalyzeParameter> m_params;
        
        /**
         * @param anaResult The given result
         */        
        public TreeModel(AnalyzeResult anaResult) {
            
            if (anaResult.getResult() instanceof Map) {
                
                setResult((Map<TestResultNode, String>) anaResult.getResult());
                if (anaResult.getParameterList().size() != 0) {
                    setParams(anaResult.getParameterList());
                }
                createChildElements();
                createParents();
            }
        }
        
        /**
         * @return The List with the childElements
         */
        public List<TreeElement> getChildElements() {
            return m_childElements;
        }
        /**
         * @return The List which contains the ChildElements that will be shown in the Tree
         */
        public List<TreeElement> getShownChildElements() {
            return m_shownChildElements;
        }

        /**
         * @return The List with the parentElements
         */
        public List<TreeElement> getParentElements() {
            return m_parentElements;
        }
        
        /**
         * @return The resultMap
         */
        public Map<TestResultNode, String> getResult() {
            return m_result;
        }
   
        /**
         * @param result
         *            The given result
         */
        public void setResult(Map<TestResultNode, String> result) {
            this.m_result = result;
        }
        /**
         * 
         * @return The List of AnalyzeParameters. Can be null.
         */
        public List<AnalyzeParameter> getParams() {
            return m_params;
        }

        /**
         * 
         * @param params The List of AnalyzeParameters
         */
        public void setParams(List<AnalyzeParameter> params) {
            this.m_params = params;
        }
         
        /**
         * creates a List of TreeElements, that are used as the childElements of the Tree
         */
        public void createChildElements() {
            String parameter = null;
            for (Map.Entry<TestResultNode, String> e : getResult().entrySet()) {
                for (AnalyzeParameter p : getParams()) {
                    if (p.getID().equals("waitAndDelayMinWait")) {
                        parameter = p.getValue();
                    }
                }

                INodePO embeddedRealNode = e.getKey().getNode();
                if (embeddedRealNode instanceof ICapPO) {
                    ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;
                    
                    if (embeddedRealCap != null) {
                        String c1 = e.getKey().getParent().getName()
                                + "/" + embeddedRealCap.getName(); 
                        TreeElement elem = new TreeElement(e.getKey(), c1,
                                e.getValue(), e.getKey().getParent());

                        getChildElements().add(elem);
                        
                        if (parameter != null
                                && (e.getValue() != null) && (Integer.valueOf(e.getValue()) >= Integer
                                .valueOf(parameter))) {
                            getShownChildElements().add(elem);    
                        }
                    }
                }
            }
        }
        /**
         * creates a List of TreeElements, that are used as the parentElements of the Tree
         */
        public void createParents() {
            Map<String, Integer> parents = new HashMap<String, Integer>();
            for (int i = 0; i < getChildElements().size(); i++) {
                
                INodePO embeddedRealNode = getChildElements().get(i).
                        getTestResultNode().getNode();
                
                if (embeddedRealNode instanceof ICapPO) {
                    ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;

                    if (!parents.containsKey(CompSystemI18n
                            .getString(embeddedRealCap.getActionName()))) {

                        if (getChildElements().get(i).getColumn2() != null) {
                            
                            parents.put(CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()),
                                Integer.valueOf(getChildElements().get(i)
                                        .getColumn2()));
                            
                        }
                    } else {

                        if (getChildElements().get(i).getColumn2() != null) {

                            int currentValue = parents
                                    .get(CompSystemI18n.getString(embeddedRealCap
                                            .getActionName()));
                            int newValue = currentValue
                                    + Integer.valueOf(getChildElements().get(i)
                                            .getColumn2());
                            parents.put(CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()), newValue);
                        }
                    }
                }
            }
            
            if (parents.size() > 0) {
                for (Map.Entry<String, Integer> e : parents.entrySet()) {
                    getParentElements().add(
                            new TreeElement(null, e.getKey(), Integer
                                    .toString(e.getValue()), null));
                }
            }
        }
    }
    
    /**
     * Represents an element of the resultTree
     * @author volker
     */
    public class TreeElement {

        /**
         * 
         */
        private TestResultNode m_node;
        /**
         * 
         */
        private String m_column1;
        /**
         * 
         */
        private String m_column2;
        /**
         * 
         */
        private TestResultNode m_parent;
        
        /**
         * 
         */
        private int m_parameterizedValue;
        

        /**
         * @param node
         *            The given Node
         * @param column1
         *            The given Text
         * @param column2
         *            The given Text
         * @param parent
         *            The given parent
         */
        public TreeElement(TestResultNode node, String column1, 
                String column2, TestResultNode parent) {
            setTestResultNode(node);
            setColumn1(column1);
            setColumn2(column2);
            setParent(parent);
            setParameterizedValue(0);
            
        }
        
        /**
         * @return The matching Node. Can be null
         */
        public TestResultNode getTestResultNode() {
            return m_node;
        }
        
        /**
         * @param node The matching Node
         */
        public void setTestResultNode(TestResultNode node) {
            this.m_node = node;
        }
        /**
         * @return The Parameterized Value
         */
        public int getParameterizedValue() {
            return m_parameterizedValue;
        }
        /**
         * 
         * @param parameterizedValue The Parameterized Value
         */
        public void setParameterizedValue(int parameterizedValue) {
            this.m_parameterizedValue = parameterizedValue;
        }
        /**
         * @return The Label
         */
        public String getColumn1() {
            return m_column1;
        }
        
        /**
         * @param column1 The Label
         */
        public void setColumn1(String column1) {
            this.m_column1 = column1;
        }
        
        /**
         * @return The Label
         */
        public String getColumn2() {
            return m_column2;
        }
        
        /**
         * @param column2 The Label
         */
        public void setColumn2(String column2) {
            this.m_column2 = column2;
        }
        
        /**
         * @return The parent. Can be null
         */
        public TestResultNode getParent() {
            return m_parent;
        }
        
        /**
         * @param parent The parent
         */
        public void setParent(TestResultNode parent) {
            this.m_parent = parent;
        }
    }

    /**
     * 
     * @author volker
     *
     */
    public class WaitAndDelayTreeViewerContentProvider 
        implements ITreeContentProvider {

        /**
         * The given TreeModel
         */
        private TreeModel m_treeModel;
        
        /**
         * @param tm The given Tree Model
         */
        public WaitAndDelayTreeViewerContentProvider(TreeModel tm) {
            setTreeModel(tm);   
        }
        
        /**
         * @param tm The given TreeModel
         */
        public void setTreeModel(TreeModel tm) {
            this.m_treeModel = tm;
        }
        
        /**
         * @return The TreeModel
         */
        public TreeModel getTreeModel() {
            return m_treeModel;
        }
        
        /**
         * {@inheritDoc}
         */
        public void dispose() {
            
        }
        /**
         * {@inheritDoc}
         */
        public void inputChanged(Viewer viewer, Object oldInput, 
                Object newInput) {
            
        }
        /**
         * {@inheritDoc}
         */
        public Object[] getElements(Object inputElement) {
            
            if (inputElement instanceof TreeModel) {
                TreeModel tm = (TreeModel) inputElement;
                return tm.getParentElements().toArray();
            }
            
            return null;
        }
        /**
         * {@inheritDoc}
         */
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof TreeElement) {
                TreeElement elem = (TreeElement) parentElement;
                if (((TreeElement) parentElement).getParent() == null) {
                    
                    List<TreeElement> children = new ArrayList<TreeElement>();
                    
                    for (TreeElement e : getTreeModel().
                            getShownChildElements()) {
                        INodePO embeddedRealNode = e.getTestResultNode()
                                .getNode();
                        if (embeddedRealNode instanceof ICapPO) {
                            ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;
                        
                            if (CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()).equals(
                                    elem.getColumn1())) {
                                children.add(e);
                            }
                        }
                    }
                    return children.toArray();
                }
            }
            return null;
        }
        /**
         * {@inheritDoc}
         */
        public Object getParent(Object element) {
            if (element instanceof TreeElement) {
                TreeElement el = (TreeElement) element;
                if (el.getParent() != null) {
                    
                    
                    INodePO embeddedRealNode = el.getTestResultNode().getNode();
                    if (embeddedRealNode instanceof ICapPO) {
                        ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;

                        for (TreeElement par : getTreeModel()
                                .getParentElements()) {
                            if (CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()).equals(
                                    par.getColumn1())) {
                                return par;
                            }
                        }
                    }
                }
            }
            return null;
        }
        /**
         * {@inheritDoc}
         */
        public boolean hasChildren(Object element) {
            if (element instanceof TreeElement) {
                TreeElement te = (TreeElement) element;
                if (te.getParent() == null) {
                    return true;
                }
            }
            return false;
        }
    }
    
    /**
     * 
     * @author volker
     *
     */
    public class WaitsAndDelaysTreeLabelProvider1 
        extends StyledCellLabelProvider {
        /**
         * The TreeModel of this Tree
         */
        private TreeModel m_treeModel;
        
        /**
         * 
         * @param tm The given TreeModel
         */
        public WaitsAndDelaysTreeLabelProvider1(TreeModel tm) {
            setTreeModel(tm);
        }

        /**
         * @param tm
         *            The given TreeModel
         */
        public void setTreeModel(TreeModel tm) {
            this.m_treeModel = tm;
        }
        
        /**
         * @return The TreeModel
         */
        public TreeModel getTreeModel() {
            return m_treeModel;
        }
        @Override
        public void update(ViewerCell cell) {
            
            Object element = cell.getElement();
            StyledString text = new StyledString();
            
            if (element instanceof TreeElement) {
                TreeElement te = (TreeElement) element;
                
                if (te.getParent() != null) {
                    text.append(te.getColumn1());
                    cell.setImage(IconConstants.TC_REF_IMAGE);
                    cell.setText(text.toString());
                } else {
                    
                    int totalCounter = 0;
                    int parameterCounter = 0;

                    for (int i = 0; i < getTreeModel().getChildElements()
                            .size(); i++) {

                        INodePO embeddedRealNode = getTreeModel()
                                .getChildElements().get(i).getTestResultNode()
                                .getNode();
                        if (embeddedRealNode instanceof ICapPO) {
                            ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;

                            if (CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()).equals(
                                    te.getColumn1())) {
                                totalCounter++;
                            }
                        }
                    }
                    
                    for (int i = 0; i < getTreeModel().getShownChildElements().size(); i++) {

                        INodePO embeddedRealNode = getTreeModel().getShownChildElements().get(i)
                                .getTestResultNode().getNode();
                        if (embeddedRealNode instanceof ICapPO) {
                            ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;

                            if (CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()).equals(
                                    te.getColumn1())) {
                                parameterCounter++;
                            }
                        }
                    }
                    
                    text.append(te.getColumn1());
                    text.append(" ( " + parameterCounter + " / " + totalCounter + " ) ",
                            StyledString.COUNTER_STYLER);
                    cell.setImage(IconConstants.CAP_IMAGE);
                    cell.setText(text.toString());
                    cell.setStyleRanges(text.getStyleRanges());
                }
                super.update(cell);
            }
            super.update(cell);
        }
    }
    /**
     * 
     * @author volker
     *
     */
    public class WaitsAndDelaysTreeLabelProvider2 extends ColumnLabelProvider {
        
        /**
         * The TreeModel of this Tree
         */
        private TreeModel m_treeModel;
        
        /**
         * 
         * @param tm The given TreeModel
         */
        public WaitsAndDelaysTreeLabelProvider2(TreeModel tm) {
            setTreeModel(tm);
        }

        /**
         * @param tm
         *            The given TreeModel
         */
        public void setTreeModel(TreeModel tm) {
            this.m_treeModel = tm;
        }
        
        /**
         * @return The TreeModel
         */
        public TreeModel getTreeModel() {
            return m_treeModel;
        }
        @Override
        public String getText(Object element) {
            if (element instanceof TreeElement) {
                TreeElement te = (TreeElement) element;

                int value = 0;
                if (te.getParent() == null) {
                    List<TreeElement> shownChildElements = getTreeModel()
                            .getShownChildElements();
                    for (int i = 0; i < shownChildElements.size(); i++) {

                        INodePO embeddedRealNode = shownChildElements.get(i)
                                .getTestResultNode().getNode();
                        if (embeddedRealNode instanceof ICapPO) {
                            ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;

                            if (CompSystemI18n.getString(
                                    embeddedRealCap.getActionName()).equals(
                                    te.getColumn1())) {
                                value = te.getParameterizedValue()
                                        + Integer.valueOf(shownChildElements
                                                .get(i).getColumn2());
                                te.setParameterizedValue(value);
                            }
                        }
                    }

                    if (value == Integer.valueOf(te.getColumn2())) {
                        return te.getColumn2();
                    }
                    String text = te.getParameterizedValue() + " ("
                            + Messages.Total + ": " + te.getColumn2() + ")";
                    return text;

                }
                return te.getColumn2();
            }
            return null;
        }
    }
    
    /**
     * 
     * @author volker
     *
     */
    public class WaitsAndDelaysDoubleClickListener 
        implements IDoubleClickListener {

        /**
         * {@inheritDoc}
         */
        public void doubleClick(DoubleClickEvent event) {

            IWorkbenchPart part = (IWorkbenchPart) getAnalyzeResult().getAdditionalData();
            TreeSelection ts = (TreeSelection) event.getSelection();
            
            if (ts.getFirstElement() instanceof TreeElement) {
                TreeElement te = (TreeElement) ts.getFirstElement();
                
                if ((part != null)) {
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                    .getActivePage().activate(part);

                    if (part instanceof TestResultTreeView) {
                        
                        TestResultTreeView trtv = (TestResultTreeView) part;
                        
                        try {
                            UINodeBP.selectNodeInTree(te.getTestResultNode(),
                                    trtv.getTreeViewer());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    
                    
                    } else if (part instanceof TestResultViewer) {
                        
                        TestResultViewer trv = (TestResultViewer) part;

                        try {
                            UINodeBP.selectNodeInTree(te.getTestResultNode(),
                                trv.getTreeViewer());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
