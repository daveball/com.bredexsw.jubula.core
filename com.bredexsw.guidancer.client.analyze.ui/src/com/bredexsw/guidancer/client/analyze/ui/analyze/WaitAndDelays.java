package com.bredexsw.guidancer.client.analyze.ui.analyze;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jubula.client.analyze.definition.IAnalyze;
import org.eclipse.jubula.client.analyze.internal.AnalyzeParameter;
import org.eclipse.jubula.client.analyze.internal.AnalyzeResult;
import org.eclipse.jubula.client.core.model.ICapPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.TestResultNode;
import org.eclipse.jubula.client.core.model.TestResultParameter;
import org.eclipse.jubula.tools.internal.i18n.CompSystemI18n;
import org.eclipse.ui.IWorkbenchPart;

/**
 * @author volker
 */
public class WaitAndDelays implements IAnalyze {

    /**
     * the active part key
     */
    private static final String ACTIVE_PART = "activePart";  //$NON-NLS-1$
    /**
     * contains the TestResultNodes which are relevant for the calculation of
     * waits/delays and the wait/delay value
     */
    private Map<TestResultNode, String> m_result; 
    
    /**
     * constructor
     */
    public WaitAndDelays() {
        
    }

    /**
     * @return The resultMap
     */
    public Map<TestResultNode, String> getResultMap() {
        return m_result;
    }

    /**
     * @param result
     *            The resultMap
     */
    public void setResultMap(Map<TestResultNode, String> result) {
        this.m_result = result;
    }
    
    /**
     * {@inheritDoc}
     */
    public AnalyzeResult execute(Object obj2analyze, IProgressMonitor monitor,
            String resultType, List<AnalyzeParameter> param, 
            String analyzeName, ExecutionEvent event) {
        
        IWorkbenchPart workbenchPart = (IWorkbenchPart) event.getParameters()
                .get(ACTIVE_PART);
        
        setResultMap(new HashMap<TestResultNode, String>());
        
        TestResultNode root = (TestResultNode) obj2analyze;

        TestResultNodeTraverser traverser = new TestResultNodeTraverser(root);
        traverser.traverse();

        for (TestResultNode node : traverser.getWaitAndDelayNodes()) {
            if (node != null && node.getParameters() != null) {
                List<TestResultParameter> parameterList = node.getParameters();
                
                for (TestResultParameter p : parameterList) {
                    for (String name : traverser.getParameterI18nStrings()) {
                        if (p.getName().equals(CompSystemI18n.
                                getString(name))) {
                            getResultMap().put(node, p.getValue());
                        }
                    }
                }
            }
        }
        return new AnalyzeResult(resultType, getResultMap(), param, workbenchPart);
    }

    /**
     * The TestResultNodeTraverser is used to traverser over the TestResultTrees
     * @author volker
     */
    class TestResultNodeTraverser {
        /**
         * Contains the i18n Strings which are used for Waits and Delays in the CompSystemI18n
         */
        private String[] m_parameterNamesI18n = { "CompSystem.DelayAfterClose",  //$NON-NLS-1$
            "CompSystem.DelayAfterVisibility", "CompSystem.Delay",  //$NON-NLS-1$
            "CompSystem.DelayBeforeDrop", "CompSystem.TimeMillSec" };  //$NON-NLS-1$

        /**
         * Contains the i18n Strings of the Actions in the CompSystemI18n that are relevant for this metric 
         */
        private String[] m_actionNamesI18n = { "CompSystem.WaitForComponent",//$NON-NLS-1$
            "CompSystem.Wait", "CompSystem.WaitForWindow",  //$NON-NLS-1$
            "CompSystem.WaitForWindowActivation",  //$NON-NLS-1$
            "CompSystem.WaitForWindowToClose" };  //$NON-NLS-1$
        /**
         * The root node
         */
        private TestResultNode m_root;

        /**
         * The current node
         */
        private TestResultNode m_currentNode;
        
        /**
         * Contains the Nodes that have already been visited
         */
        private Set<TestResultNode> m_waitAndDelayNodes = 
                new HashSet<TestResultNode>();
        
        /**
         * Constructor
         * @param root The given root node
         */
        public TestResultNodeTraverser(TestResultNode root) {
            setRootNode(root);
        }

        /**
         * @param root The given root node
         */
        public void setRootNode(TestResultNode root) {
            this.m_root = root;
        }
        
        /**
         * @return the root node
         */
        public TestResultNode getRootNode() {
            return m_root;
        }
        /**
         * @param currentNode The given currentNode node
         */
        public void setCurrentNode(TestResultNode currentNode) {
            this.m_currentNode = currentNode;
        }
        
        /**
         * @return the currentNode node
         */
        public TestResultNode getCurrentNode() {
            return m_currentNode;
        }

        /**
         * @return the Array which contains the i18n Strings of the parameters from an relevant action
         */
        public String[] getParameterI18nStrings() {
            return m_parameterNamesI18n;
        }
        
        /**
         * @return the Array which contains the i18n Strings of the action names
         */
        public String[] getActionI18nStrings() {
            return m_actionNamesI18n;
        }
        
        /**
         * @return The list with the waitAndDelayNodes
         */
        public Set<TestResultNode> getWaitAndDelayNodes() {
            return m_waitAndDelayNodes;
        }
        /**
         * 
         */
        public void traverse() {
            
            checkForWaitAndDelayNodes(getRootNode());
        }
        
        /**
         * Traverses the TestResultTree and checks for TestSteps and EventHandlers
         * @param node the given Node
         */
        public void checkForWaitAndDelayNodes(TestResultNode node) {
            List<TestResultNode> resultNodeList = node.getResultNodeList();
            if (resultNodeList.size() != 0) {
                for (int i = 0; i < resultNodeList.size(); i++) {
                    checkForWaitAndDelayNodes(resultNodeList.get(i));
                }
            } else {
                INodePO embeddedRealNode = node.getNode();
                
                if (embeddedRealNode instanceof ICapPO) {
                    ICapPO embeddedRealCap = (ICapPO) embeddedRealNode;
                    for (int i = 0; i < getActionI18nStrings().length; i++) { 

                        String actionName = embeddedRealCap.getActionName();
                        if (actionName != null
                                && actionName.equals(
                                        getActionI18nStrings()[i])) {

                            getWaitAndDelayNodes().add(node);
                        }
                    }
                }
            }
        }
    }
}
