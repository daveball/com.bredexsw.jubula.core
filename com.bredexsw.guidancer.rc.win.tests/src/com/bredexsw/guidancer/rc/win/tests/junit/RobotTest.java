package com.bredexsw.guidancer.rc.win.tests.junit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.bredexsw.guidancer.rc.win.driver.WinRobotImpl;
import com.bredexsw.jubula.rc.common.nativ.exceptions.RemoteServerException;

/**
 * 
 * JUnit test class for the WinRobotImpl.class.
 * 
 * @author patrick
 *
 */
public class RobotTest {
    
    /**
     * Shows if the server connection was successful
     */
    private static final AtomicBoolean CONNECTED = new AtomicBoolean(false);
    
    /**
     * Robot Obj.
     */
    private static WinRobotImpl robot;
    
    /**
     * Win server path
     */
    private static final String NETPATH = "K:\\guidancer\\Workspace\\users"
            + "\\PatrickGutorski\\workspaces\\lin_37\\jubula_lab"
            + "\\com.bredexsw.guidancer.rc.win.nativ."
            + "src\\obj\\Release"
            + "\\com.bredexsw.guidancer.win.native.src.exe";
    
    /**
     * Win AUT path
     */
    private static final String AUTPATH = "K:\\guidancer\\Workspace\\users"
            + "\\PatrickGutorski\\workspaces\\lin_37\\jubula_qs"
            + "\\org.eclipse.jubula.qa.aut.caa.win\\obj\\Release"
            + "\\org.eclipse.jubula.qa.aut.caa.win.exe";
    
    /**
     * The Port.
     */
    private static int port;
    
    /**
     * The Win server process.
     */
    private static Process process;

    /**
     * Starts the Win server
     */
    @BeforeClass
    public static void startandconnect() {
        initServerConnection();
        startWinServer();
    }
    
    /**
     * 
     */
    private static void initServerConnection() {
//        robot = new WinRobotImpl();
//        port = 23234;
//        robot.createSocket(port, CONNECTED);
    }
    
    /**
     * Starts the Win server with our socket port as an argument.
     * 
     */
    private static void startWinServer() {
        try {
            List<String> commands = new ArrayList<String>();
            commands.add(NETPATH);
            commands.add(Integer.toString(port));
            ProcessBuilder procbuilder = new ProcessBuilder(commands);
            process = procbuilder.start();
            
        } catch (IOException e) {
//            System.exit(0);
        }
    }
    
    /**
     * Test if the Win server was started
     */
    @Test
    public void testServerStart() {
        boolean isServerRunning = (process != null ? true : false);
        assertTrue("Connection not made", isServerRunning);
    }
    
    /**
     * Test if the connection with the Win server is made
     */
    @Test
    public void testConnection() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue("Connection not made", CONNECTED.get());
    }
    
    /**
     * Checks if the aut is not running.
     */
    @Test
    public void testAUTnotrunning() {
        try {
            assertFalse("AUT is running", robot.isAUTactive());
        } catch (RemoteServerException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test if start method was found and void returned
     */
    @Test
    public void testStartAUT() {
        try {
            robot.startAUT(AUTPATH, "", "");
        } catch (RemoteServerException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Checks if the aut is running.
     */
    @Test
    public void testAUTrunning() {
        try {
            assertTrue("AUT is not running", robot.isAUTactive());
        } catch (RemoteServerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     */
    @Test
    @Ignore 
    public void testStopAUT() {
        fail("Not yet implemented");
    }
    
    /**
     * 
     */
    @Test
    @Ignore
    public void testgetText() {
        fail("Not yet implemented");
    }
    
    

}
