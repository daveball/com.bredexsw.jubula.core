//
//  StepperViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by marv on 2/5/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "StepperViewController.h"

@interface StepperViewController ()
@property (weak, nonatomic) IBOutlet UIStepper *m_stepper;
@property (weak, nonatomic) IBOutlet UIStepper *m_disabledStepper;
@property (weak, nonatomic) IBOutlet UIStepper *m_selectedStepper;
@property (weak, nonatomic) IBOutlet UILabel *m_reflectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *m_valueLabel;
@property (weak, nonatomic) IBOutlet UIStepper *m_noLongPressStepper;
@property (weak, nonatomic) IBOutlet UIStepper *m_unlimitedStepper;
@property (weak, nonatomic) IBOutlet UIButton *m_backbutton;

@end

@implementation StepperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_stepper.accessibilityIdentifier = @"enabledStepper";
    self.m_disabledStepper.accessibilityIdentifier =@"disabledStepper";
    self.m_selectedStepper.accessibilityIdentifier = @"selectedStepper";
    self.m_noLongPressStepper.accessibilityIdentifier = @"noLongPressStepper";
    self.m_unlimitedStepper.accessibilityIdentifier = @"continousStepper";
    
    self.m_valueLabel.accessibilityIdentifier = @"valueReflection";
    self.m_reflectionLabel.accessibilityIdentifier = @"switch_reflectionLabel";
    
    self.m_backbutton.accessibilityIdentifier = @"stepperBackButton";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)changeReflectionLabel:(id)sender {
    self.m_reflectionLabel.text = ((UIStepper *) sender).accessibilityIdentifier;
    self.m_valueLabel.text = [NSString stringWithFormat: @"%.0f", [((UIStepper *) sender) value] ];
}

- (void)viewDidUnload {
    [self setM_stepper:nil];
    [self setM_disabledStepper:nil];
    [self setM_selectedStepper:nil];
    [self setM_reflectionLabel:nil];
    [self setM_valueLabel:nil];
    [self setM_noLongPressStepper:nil];
    [self setM_unlimitedStepper:nil];
    [self setM_backbutton:nil];
    [super viewDidUnload];
}
@end
