//
//  AppDelegate.h
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Marvin Mueller on 1/7/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
