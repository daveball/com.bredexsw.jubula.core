//
//  CaAViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Marvin Mueller on 1/7/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "CaAViewController.h"

@interface CaAViewController ()
@property (weak, nonatomic) IBOutlet UIButton *m_buttons;
@property (weak, nonatomic) IBOutlet UIButton *m_switch;
@property (weak, nonatomic) IBOutlet UIButton *m_stepper;
@property (weak, nonatomic) IBOutlet UIButton *m_segmentedControl;
@property (weak, nonatomic) IBOutlet UIButton *m_textField;
@property (weak, nonatomic) IBOutlet UIButton *m_application;

@end

@implementation CaAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_buttons.accessibilityIdentifier =@"buttons";
    self.m_switch.accessibilityIdentifier = @"switches";
    self.m_stepper.accessibilityIdentifier = @"stepper";
    self.m_segmentedControl.accessibilityIdentifier = @"segmentedControl";
    self.m_textField.accessibilityIdentifier = @"textField";
    self.m_application.accessibilityIdentifier = @"application";
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setM_buttons:nil];
    [self setM_stepper:nil];
    [self setM_segmentedControl:nil];
    [self setM_switch:nil];
    [self setM_textField:nil];
    [self setM_application:nil];
    [super viewDidUnload];
}

- (IBAction)gotoUISwitch:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"UISwitchView" bundle:nil];
    UIViewController* uiSwitchController = [storyboard instantiateInitialViewController];
    
    uiSwitchController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:uiSwitchController animated:YES completion:nil];
}
@end
