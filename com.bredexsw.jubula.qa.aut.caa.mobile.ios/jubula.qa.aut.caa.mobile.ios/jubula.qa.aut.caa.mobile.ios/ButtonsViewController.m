//
//  ButtonsViewController.m
//  qa.aut.caa.mobile.ios
//
//  Created by Marvin Mueller on 1/4/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "ButtonsViewController.h"
#import "TapLocationButton.h"

@interface ButtonsViewController ()<TapDelegate>

@property (weak, nonatomic) IBOutlet UILabel *m_actionLabel;
@property (weak, nonatomic) IBOutlet UIButton *m_enabledButton;
@property (weak, nonatomic) IBOutlet UIButton *m_disabledButton;
@property (weak, nonatomic) IBOutlet UIButton *m_selectedButton;
@property (weak, nonatomic) IBOutlet UIButton *m_add;
@property (weak, nonatomic) IBOutlet UIButton *m_disabledAdd;
@property (weak, nonatomic) IBOutlet UIButton *m_selectedAdd;
@property (weak, nonatomic) IBOutlet UIButton *m_detail;
@property (weak, nonatomic) IBOutlet UIButton *m_disabledDetail;
@property (weak, nonatomic) IBOutlet UIButton *m_selectedDetail;
@property (weak, nonatomic) IBOutlet UIButton *m_infoLight;
@property (weak, nonatomic) IBOutlet UIButton *m_disabledInfoLight;
@property (weak, nonatomic) IBOutlet UIButton *m_selectedInfoLight;
@property (weak, nonatomic) IBOutlet UIButton *m_infoDark;
@property (weak, nonatomic) IBOutlet UIButton *m_disabledInfoDark;
@property (weak, nonatomic) IBOutlet UIButton *m_selectedInfoDark;

@property (weak, nonatomic) IBOutlet UIButton *m_backgroundPictureButton;
@property (weak, nonatomic) IBOutlet UIButton *m_pictureButton;

@property (weak, nonatomic) IBOutlet UIButton *m_swipeableButton;

@property (weak, nonatomic) IBOutlet UIButton *m_backButton;

@property (weak, nonatomic) IBOutlet TapLocationButton *m_button_position;
@end

@implementation ButtonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)changeLabel:(id)sender {
    self.m_actionLabel.text = ((UIButton *) sender).accessibilityIdentifier;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.m_add.accessibilityIdentifier =@"add";
    self.m_disabledAdd.accessibilityIdentifier =@"disabledAdd";
    self.m_selectedAdd.accessibilityIdentifier =@"selectedAdd";
    self.m_detail.accessibilityIdentifier =@"detailDisclosure";
    self.m_disabledDetail.accessibilityIdentifier =@"disabledDetailDisclosure";
    self.m_selectedDetail.accessibilityIdentifier =@"selectedDetailDisclosure";
    self.m_enabledButton.accessibilityIdentifier = @"button";
    self.m_disabledButton.accessibilityIdentifier = @"disabledButton";
    self.m_selectedButton.accessibilityIdentifier = @"selectedButton";
    self.m_infoLight.accessibilityIdentifier = @"infoLight";
    self.m_disabledInfoLight.accessibilityIdentifier = @"disabledInfoLight";
    self.m_selectedInfoLight.accessibilityIdentifier = @"selectedInfoLight";
    self.m_infoDark.accessibilityIdentifier = @"infoDark";
    self.m_disabledInfoDark.accessibilityIdentifier = @"disabledInfoDark";
    self.m_selectedInfoDark.accessibilityIdentifier = @"selectedInfoDark";
    self.m_pictureButton.accessibilityIdentifier = @"pictureButton";
    self.m_backgroundPictureButton.accessibilityIdentifier =@"backgroundPictureButton";
    
    self.m_swipeableButton.accessibilityIdentifier = @"swipeableButton";
    self.m_button_position.accessibilityIdentifier = @"positionButton";
    self.m_button_position.tapDelegate = self;
    self.m_backButton.accessibilityIdentifier =@"backButton";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)swipedRight:(id)sender {
    self.m_actionLabel.text = @"swiped Right";
}
- (IBAction)swipedLeft:(id)sender {
    self.m_actionLabel.text = @"swiped Left";
}
- (IBAction)swipedUp:(id)sender {
    self.m_actionLabel.text = @"swiped Up";
}
- (IBAction)swipedDown:(id)sender {
    self.m_actionLabel.text = @"swiped Down";
}

- (void) tapDidFinishDelegate:(CGPoint)location
{
    self.m_actionLabel.text = NSStringFromCGPoint(location);
}

- (void)viewDidUnload {
    [self setM_enabledButton:nil];
    [self setM_disabledButton:nil];
    [self setM_selectedButton:nil];
    [self setM_actionLabel:nil];
    [self setM_add:nil];
    [self setM_detail:nil];
    [self setM_infoLight:nil];
    [self setM_infoDark:nil];
    [self setM_disabledAdd:nil];
    [self setM_selectedAdd:nil];
    [self setM_disabledDetail:nil];
    [self setM_selectedDetail:nil];
    [self setM_disabledInfoLight:nil];
    [self setM_selectedInfoLight:nil];
    [self setM_disabledInfoDark:nil];
    [self setM_selectedInfoDark:nil];
    [self setM_backgroundPictureButton:nil];
    [self setM_pictureButton:nil];
    [self setM_backButton:nil];
    [super viewDidUnload];
}
@end

