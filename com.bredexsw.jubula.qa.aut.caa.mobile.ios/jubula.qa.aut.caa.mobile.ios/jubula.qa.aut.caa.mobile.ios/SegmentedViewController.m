//
//  SegmentedViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by marv on 2/5/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "SegmentedViewController.h"

@interface SegmentedViewController ()
@property (weak, nonatomic) IBOutlet UIButton *m_backButton;
@property (weak, nonatomic) IBOutlet UILabel *m_reflectionLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *m_segmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *m_disabledSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *m_selectedSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *m_segmentedControl_bounds;
@property (weak, nonatomic) IBOutlet UISegmentedControl *m_segmentedControl_bar;

@end

@implementation SegmentedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.m_backButton.accessibilityIdentifier = @"segmentedC_backButton";
    self.m_reflectionLabel.accessibilityIdentifier = @"segmentedC_Label";
    self.m_segmentedControl.accessibilityIdentifier = @"segmentedControl";
    self.m_disabledSegmentedControl.accessibilityIdentifier = @"disabledSegmentedControl";
    self.m_selectedSegmentedControl.accessibilityIdentifier = @"selectedSegmentedControl";
    
    self.m_segmentedControl_bounds.accessibilityIdentifier = @"segmentedControl_bordered";
    self.m_segmentedControl_bar.accessibilityIdentifier = @"segmentedControl_bar";
    
    [self.m_segmentedControl setEnabled:FALSE forSegmentAtIndex:2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setM_backButton:nil];
    [self setM_reflectionLabel:nil];
    [self setM_segmentedControl:nil];
    [self setM_disabledSegmentedControl:nil];
    [self setM_selectedSegmentedControl:nil];
    [self setM_segmentedControl_bounds:nil];
    [self setM_segmentedControl_bar:nil];
    [super viewDidUnload];
}
- (IBAction)changeLabel:(id)sender {
    UISegmentedControl* segmentedControl = ((UISegmentedControl*) sender);
    self.m_reflectionLabel.text = [segmentedControl titleForSegmentAtIndex:[segmentedControl selectedSegmentIndex]];
}
@end
