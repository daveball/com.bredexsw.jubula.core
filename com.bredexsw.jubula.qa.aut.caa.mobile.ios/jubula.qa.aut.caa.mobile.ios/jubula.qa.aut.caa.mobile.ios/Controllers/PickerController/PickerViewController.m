//
//  PickerViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 09/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "PickerViewController.h"

@interface PickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *pickerPickerView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *actionLbl;
@property (weak, nonatomic) IBOutlet UILabel *valueLbl;
@property (nonatomic,strong) NSArray* ABCList;
@end

@implementation PickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
    self.ABCList = @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"J",@"K"];
}

- (void) addAccessibilityIdentifier
{
    self.pickerPickerView.accessibilityIdentifier = @"pickerView";
    self.backBtn.accessibilityIdentifier = @"pickerBackButton";
    self.actionLbl.accessibilityIdentifier = @"pickerActionLabel";
    self.valueLbl.accessibilityIdentifier = @"pickerValueLabel";
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 10;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [@(row) stringValue];
            break;
        case 1:
            return self.ABCList[row];
            break;
        default:
            break;
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.actionLbl.text = [NSString stringWithFormat:@"Selected row: %@ in column: %@", @(row), @(component)];
    switch (component) {
        case 0:
            self.valueLbl.text = [@(row) stringValue];
            break;
        case 1:
            self.valueLbl.text = self.ABCList[row];
            break;
        default:
            break;
    }
    
}

@end
