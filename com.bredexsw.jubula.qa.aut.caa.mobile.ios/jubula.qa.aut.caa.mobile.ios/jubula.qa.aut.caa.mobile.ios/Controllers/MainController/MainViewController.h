//
//  MainViewController.h
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 06/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainViewController : UIViewController
- (IBAction)unwindToMainViewController:(UIStoryboardSegue *)unwindSegue;
@end
