//
//  MainViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 06/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController()

@property (weak, nonatomic) IBOutlet UIButton *buttonsBtn;
@property (weak, nonatomic) IBOutlet UIButton *switchBtn;
@property (weak, nonatomic) IBOutlet UIButton *stepperBtn;
@property (weak, nonatomic) IBOutlet UIButton *segmentCtrlBtn;
@property (weak, nonatomic) IBOutlet UIButton *textFieldBtn;
@property (weak, nonatomic) IBOutlet UIButton *applicationBtn;

@end

@implementation MainViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.buttonsBtn.accessibilityIdentifier =@"buttons";
    self.switchBtn.accessibilityIdentifier = @"switches";
    self.stepperBtn.accessibilityIdentifier = @"stepper";
    self.segmentCtrlBtn.accessibilityIdentifier = @"segmentedControl";
    self.textFieldBtn.accessibilityIdentifier = @"textField";
    self.applicationBtn.accessibilityIdentifier = @"application";
}

- (IBAction)unwindToMainViewController:(UIStoryboardSegue *)unwindSegue{}

@end
