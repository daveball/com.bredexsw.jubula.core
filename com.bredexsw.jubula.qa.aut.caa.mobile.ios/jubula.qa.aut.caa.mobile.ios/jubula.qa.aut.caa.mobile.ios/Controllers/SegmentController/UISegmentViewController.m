//
//  UISegmentViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "UISegmentViewController.h"

@interface UISegmentViewController ()
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *selectedSC;
@property (weak, nonatomic) IBOutlet UISegmentedControl *disabledSC;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlerBounds;
@property (weak, nonatomic) IBOutlet UISegmentedControl *barSegmentControl;
@property (weak, nonatomic) IBOutlet UILabel *actionLbl;

@end

@implementation UISegmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"segmentedC_backButton";
    self.actionLbl.accessibilityIdentifier = @"segmentedC_Label";
    self.segmentControl.accessibilityIdentifier = @"segmentedControl";
    self.disabledSC.accessibilityIdentifier = @"disabledSegmentedControl";
    self.selectedSC.accessibilityIdentifier = @"selectedSegmentedControl";
    
    self.segmentControlerBounds.accessibilityIdentifier = @"segmentedControl_bordered";
    self.barSegmentControl.accessibilityIdentifier = @"segmentedControl_bar";
    
    [self.segmentControl setEnabled:NO forSegmentAtIndex:2];
}

- (IBAction)changeLabel:(UISegmentedControl*) sender
{
    self.actionLbl.text = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
}

@end
