//
//  ApplicationsViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "ApplicationsViewController.h"

@interface ApplicationsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *tapBtn;
@property (weak, nonatomic) IBOutlet UILabel *actionLbl;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation ApplicationsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.textField.accessibilityIdentifier = @"application_textfield";
    self.tapBtn.accessibilityIdentifier =@"TapButton";
    self.actionLbl.accessibilityIdentifier = @"application_reflecting_label";
    self.backBtn.accessibilityIdentifier = @"application_back_button";
}

- (IBAction)buttonAction:(id)sender
{
    self.actionLbl.text = @"TapButton was pressed";
}


@end
