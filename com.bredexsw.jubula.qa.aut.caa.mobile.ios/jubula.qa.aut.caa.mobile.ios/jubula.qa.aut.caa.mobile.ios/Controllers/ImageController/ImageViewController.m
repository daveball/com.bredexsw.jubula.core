//
//  ImageViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 09/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *fitImage;
@property (weak, nonatomic) IBOutlet UIImageView *fillImage;

@end

@implementation ImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.fitImage.accessibilityIdentifier = @"fitImageView";
    self.fillImage.accessibilityIdentifier = @"fillImageView";
    self.backBtn.accessibilityIdentifier = @"imageBackButton";
}

@end
