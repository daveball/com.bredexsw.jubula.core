//
//  UITextfiledViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "UITextfiledViewController.h"

@interface UITextfiledViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textfieldTF;
@property (weak, nonatomic) IBOutlet UITextField *disabledTF;
@property (weak, nonatomic) IBOutlet UITextField *selecteTF;
@property (weak, nonatomic) IBOutlet UITextField *disabledWithText;
@property (weak, nonatomic) IBOutlet UITextField *placeHolderTF;
@property (weak, nonatomic) IBOutlet UITextField *backgroundTF;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation UITextfiledViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"textFields_backButton";
    self.textfieldTF.accessibilityIdentifier = @"enabledTextField";
    self.disabledTF.accessibilityIdentifier = @"disabledTextField";
    self.selecteTF.accessibilityIdentifier = @"selectedTextField";
    self.disabledWithText.accessibilityIdentifier = @"disabledWithText";
    self.placeHolderTF.accessibilityIdentifier = @"textFieldWithPlaceHolder";
    self.backgroundTF.accessibilityIdentifier = @"textFieldWithBackground";
}

- (IBAction)hideKeyBoard:(UITextField*)sender
{
    [sender resignFirstResponder];
}


@end
