//
//  LabelViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 08/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "LabelViewController.h"

@interface LabelViewController ()
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *attributedLbl;
@property (weak, nonatomic) IBOutlet UILabel *simpleLbl;
@property (weak, nonatomic) IBOutlet UILabel *middleTruncateLbl;

@end

@implementation LabelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"labelBackButton";
    self.simpleLbl.accessibilityIdentifier = @"simpleLabel";
    self.attributedLbl.accessibilityIdentifier = @"attributedLabel";
    self.middleTruncateLbl.accessibilityIdentifier = @"middleTruncateLabel";
}


@end
