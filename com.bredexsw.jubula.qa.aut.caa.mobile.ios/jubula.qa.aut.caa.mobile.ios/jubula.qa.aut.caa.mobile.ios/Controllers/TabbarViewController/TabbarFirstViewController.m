//
//  TabbarFirstViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 08/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "TabbarFirstViewController.h"

@interface TabbarFirstViewController ()

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@end

@implementation TabbarFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
    self.tabBarItem.badgeValue = @"Badge";
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"tabBackButton";
}

@end
