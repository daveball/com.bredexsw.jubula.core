//
//  TabbarSecondViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 08/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "TabbarSecondViewController.h"

@interface TabbarSecondViewController ()
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation TabbarSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"tabBackButton";
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSInteger badge = [self.tabBarItem.badgeValue integerValue] + 1;
    self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",badge];
}

@end
