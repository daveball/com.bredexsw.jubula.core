//
//  TableViewCellViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 08/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "TableViewCellViewController.h"

@interface TableViewCellViewController ()

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TableViewCellViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"labelBackButton";
    self.tableView.accessibilityIdentifier = @"simpleTableView";
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* UITableViewCellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UITableViewCellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%i. cell",indexPath.row];
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"navcon"]) {
        UIViewController* toVC = segue.destinationViewController;
        NSIndexPath* selectedPath = [self.tableView indexPathForSelectedRow];
        toVC.navigationItem.title = [NSString stringWithFormat:@"%i. cell",selectedPath.row];
    }
}

@end
