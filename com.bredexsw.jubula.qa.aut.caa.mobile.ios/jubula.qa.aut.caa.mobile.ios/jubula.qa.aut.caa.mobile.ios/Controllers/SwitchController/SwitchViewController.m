//
//  SwitchViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "SwitchViewController.h"

@interface SwitchViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *switchSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *disabledSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *selectedSwitch;
@property (weak, nonatomic) IBOutlet UILabel *actionLbl;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@end

@implementation SwitchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"switch_BackButton";
    self.switchSwitch.accessibilityIdentifier = @"enabledSwitch";
    self.disabledSwitch.accessibilityIdentifier = @"disabledSwitch";
    self.selectedSwitch.accessibilityIdentifier = @"selectedSwitch";
}

@end
