//
//  StepperViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "SteppersViewController.h"

@interface SteppersViewController ()
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UIStepper *unlimitedStepper;
@property (weak, nonatomic) IBOutlet UIStepper *noLongPressStepper;
@property (weak, nonatomic) IBOutlet UIStepper *disabledStepper;
@property (weak, nonatomic) IBOutlet UIStepper *selectedStepper;
@property (weak, nonatomic) IBOutlet UILabel *actionLbl;
@property (weak, nonatomic) IBOutlet UILabel *valueLbl;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation SteppersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.stepper.accessibilityIdentifier = @"enabledStepper";
    self.disabledStepper.accessibilityIdentifier =@"disabledStepper";
    self.selectedStepper.accessibilityIdentifier = @"selectedStepper";
    self.noLongPressStepper.accessibilityIdentifier = @"noLongPressStepper";
    self.unlimitedStepper.accessibilityIdentifier = @"continousStepper";
    
    self.valueLbl.accessibilityIdentifier = @"valueReflection";
    self.actionLbl.accessibilityIdentifier = @"switch_reflectionLabel";

    self.backBtn.accessibilityIdentifier = @"stepperBackButton";
}

- (IBAction)changeAction:(UIStepper*)sender
{
    self.actionLbl.text = sender.accessibilityIdentifier;
    self.valueLbl.text = [@(sender.value) stringValue];
}

@end
