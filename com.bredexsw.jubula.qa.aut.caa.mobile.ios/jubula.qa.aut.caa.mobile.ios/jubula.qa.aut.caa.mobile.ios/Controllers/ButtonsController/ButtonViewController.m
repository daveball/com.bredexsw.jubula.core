//
//  ButtonViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "ButtonViewController.h"
#import "TapLocationButton.h"

@interface ButtonViewController ()<TapDelegate>

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UIButton *enabledBtn;
@property (weak, nonatomic) IBOutlet UIButton *infoLight;
@property (weak, nonatomic) IBOutlet UIButton *infoDark;
@property (weak, nonatomic) IBOutlet UIButton *disabledAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *disabledDetailBtn;
@property (weak, nonatomic) IBOutlet UIButton *disabledBtn;
@property (weak, nonatomic) IBOutlet UIButton *disabledInfoLight;
@property (weak, nonatomic) IBOutlet UIButton *disabledInfoDarkBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectedAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectedDetailBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectedInfoLightBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectedInfoDarkBtn;
@property (weak, nonatomic) IBOutlet UIButton *backgroundPictureBtn;
@property (weak, nonatomic) IBOutlet UIButton *pictureBtn;
@property (weak, nonatomic) IBOutlet UIButton *swipeableBtn;
@property (weak, nonatomic) IBOutlet UILabel *actionLbl;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet TapLocationButton *positionBtn;

@end

@implementation ButtonViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibleIdentifier];
    self.positionBtn.tapDelegate = self;

}

- (void) addAccessibleIdentifier
{
    self.addBtn.accessibilityIdentifier =@"add";
    self.disabledAddBtn.accessibilityIdentifier =@"disabledAdd";
    self.selectedAddBtn.accessibilityIdentifier =@"selectedAdd";
    self.detailBtn.accessibilityIdentifier =@"detailDisclosure";
    self.disabledDetailBtn.accessibilityIdentifier =@"disabledDetailDisclosure";
    self.selectedDetailBtn.accessibilityIdentifier =@"selectedDetailDisclosure";
    self.enabledBtn.accessibilityIdentifier = @"button";
    self.disabledBtn.accessibilityIdentifier = @"disabledButton";
    self.selectedBtn.accessibilityIdentifier = @"selectedButton";
    self.infoLight.accessibilityIdentifier = @"infoLight";
    self.disabledInfoLight.accessibilityIdentifier = @"disabledInfoLight";
    self.selectedInfoLightBtn.accessibilityIdentifier = @"selectedInfoLight";
    self.infoDark.accessibilityIdentifier = @"infoDark";
    self.disabledInfoDarkBtn.accessibilityIdentifier = @"disabledInfoDark";
    self.selectedInfoDarkBtn.accessibilityIdentifier = @"selectedInfoDark";
    self.pictureBtn.accessibilityIdentifier = @"pictureButton";
    self.backgroundPictureBtn.accessibilityIdentifier =@"backgroundPictureButton";
    
    self.swipeableBtn.accessibilityIdentifier = @"swipeableButton";
    
    self.backBtn.accessibilityIdentifier =@"backButton";
}

- (IBAction)swipeRightAction:(id)sender
{
     self.actionLbl.text = @"swiped Right";
}

- (IBAction)swipeLeftAction:(id)sender
{
    self.actionLbl.text = @"swiped Left";
}

- (IBAction)swipeUpAction:(id)sender
{
    self.actionLbl.text = @"swiped Up";
}

- (IBAction)swipeDownAction:(id)sender
{
    self.actionLbl.text = @"swiped Down";
}

- (IBAction)changeLabelAction:(UIButton*)sender
{
    self.actionLbl.text = sender.accessibilityIdentifier;
}

- (void) tapDidFinishDelegate:(CGPoint)location
{
    self.actionLbl.text = NSStringFromCGPoint(location);
}

@end
