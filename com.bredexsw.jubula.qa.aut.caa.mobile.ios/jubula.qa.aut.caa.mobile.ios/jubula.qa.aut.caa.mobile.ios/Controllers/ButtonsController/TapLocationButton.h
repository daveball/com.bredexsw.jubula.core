//
//  TapLocationView.h
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/07/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TapDelegate <NSObject>

- (void) tapDidFinishDelegate:(CGPoint)location;

@end

@interface TapLocationButton : UIButton

@property (nonatomic,assign) CGPoint tappedLocation;
@property (nonatomic,weak) id<TapDelegate> tapDelegate;

@end
