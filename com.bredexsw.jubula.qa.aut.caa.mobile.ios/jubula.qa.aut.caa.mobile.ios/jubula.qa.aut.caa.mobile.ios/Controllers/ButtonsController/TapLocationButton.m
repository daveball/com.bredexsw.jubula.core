//
//  TapLocationView.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 07/07/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "TapLocationButton.h"

@implementation TapLocationButton

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    UIView* view = touch.view;
    CGPoint location = [touch locationInView:view];
    CGPoint roundedLoc = CGPointMake((NSInteger)location.x, (NSInteger)location.y);
    self.tappedLocation = location;
    [self.tapDelegate tapDidFinishDelegate:roundedLoc];
}


@end
