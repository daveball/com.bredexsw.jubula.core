//
//  TextViewViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by Kiss Tamás on 08/05/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "TextViewViewController.h"

@interface TextViewViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITextView *textViewTV;
@property (weak, nonatomic) IBOutlet UITextView *emptyTV;
@property (weak, nonatomic) IBOutlet UITextView *disabledTV;
@property (weak, nonatomic) IBOutlet UITextView *attributedTV;

@end

@implementation TextViewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addAccessibilityIdentifier];
}

- (void) addAccessibilityIdentifier
{
    self.backBtn.accessibilityIdentifier = @"textFields_backButton";
    self.textViewTV.accessibilityIdentifier = @"enabledTextView";
    self.disabledTV.accessibilityIdentifier = @"disabledTextView";
    self.emptyTV.accessibilityIdentifier = @"emptyTextView";
    self.attributedTV.accessibilityIdentifier = @"attributedTextView";
}

- (IBAction)tapAction:(id)sender
{
    [self.view endEditing:YES];
}

@end
