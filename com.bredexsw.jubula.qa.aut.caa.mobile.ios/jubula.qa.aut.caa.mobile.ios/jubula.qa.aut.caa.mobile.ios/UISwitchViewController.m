//
//  UISwitchViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by marv on 2/5/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UISwitchViewController.h"

@interface UISwitchViewController ()
@property (weak, nonatomic) IBOutlet UIButton *m_backButton;
@property (weak, nonatomic) IBOutlet UISwitch *m_switch;
@property (weak, nonatomic) IBOutlet UISwitch *m_switch_disabled;
@property (weak, nonatomic) IBOutlet UISwitch *m_switch_selected;
@property (weak, nonatomic) IBOutlet UILabel *m_reflectionLabel;

@end

@implementation UISwitchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.m_backButton.accessibilityIdentifier = @"switch_BackButton";
    self.m_switch.accessibilityIdentifier = @"enabledSwitch";
    self.m_switch_disabled.accessibilityIdentifier = @"disabledSwitch";
    self.m_switch_selected.accessibilityIdentifier = @"selectedSwitch";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setM_backButton:nil];
    [self setM_switch:nil];
    [self setM_switch_disabled:nil];
    [self setM_switch_selected:nil];
    [self setM_reflectionLabel:nil];
    [super viewDidUnload];
}
@end
