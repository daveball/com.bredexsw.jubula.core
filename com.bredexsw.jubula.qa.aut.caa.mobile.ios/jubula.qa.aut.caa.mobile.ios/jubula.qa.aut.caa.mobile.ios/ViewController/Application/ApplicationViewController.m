//
//  ApplicationViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by marv on 8/21/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "ApplicationViewController.h"

@interface ApplicationViewController ()
@property (weak, nonatomic) IBOutlet UITextField *m_textfield;
@property (weak, nonatomic) IBOutlet UIButton *m_buttonToTap;
@property (weak, nonatomic) IBOutlet UILabel *m_reflectingLabel;
@property (weak, nonatomic) IBOutlet UIButton *m_backButton;

@end

@implementation ApplicationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.m_textfield.accessibilityIdentifier = @"application_textfield";
    self.m_buttonToTap.accessibilityIdentifier =@"TapButton";
    self.m_reflectingLabel.accessibilityIdentifier = @"application_reflecting_label";
    self.m_backButton.accessibilityIdentifier = @"application_back_button";
}
- (IBAction)hideKeyBoard:(UITextField*)sender {
    [sender resignFirstResponder];
}

- (void) viewDidUnload
{
    [self setM_textfield:nil];
    [self setM_buttonToTap:nil];
    [self setM_reflectingLabel:nil];
    [self setM_backButton:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tapButtonAction:(id)sender {
    [self.m_reflectingLabel setText:@"TapButton was pressed"];
}

@end
