//
//  TextFieldViewController.m
//  jubula.qa.aut.caa.mobile.ios
//
//  Created by marv on 2/27/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "TextFieldViewController.h"

@interface TextFieldViewController ()
@property (weak, nonatomic) IBOutlet UIButton *m_backButton;
@property (weak, nonatomic) IBOutlet UITextField *m_textField;
@property (weak, nonatomic) IBOutlet UITextField *m_disabledTextField;
@property (weak, nonatomic) IBOutlet UITextField *m_selectedTextField;
@property (weak, nonatomic) IBOutlet UITextField *m_disabledWithText;
@property (weak, nonatomic) IBOutlet UITextField *m_placeholderTextField;
@property (weak, nonatomic) IBOutlet UITextField *m_backgroundTextField;

@end

@implementation TextFieldViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.m_backButton.accessibilityIdentifier = @"textFields_backButton";
    self.m_textField.accessibilityIdentifier = @"enabledTextField";
    self.m_disabledTextField.accessibilityIdentifier = @"disabledTextField";
    self.m_selectedTextField.accessibilityIdentifier = @"selectedTextField";
    self.m_disabledWithText.accessibilityIdentifier = @"disabledWithText";
    self.m_placeholderTextField.accessibilityIdentifier = @"textFieldWithPlaceHolder";
    self.m_backgroundTextField.accessibilityIdentifier = @"textFieldWithBackground";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setM_backButton:nil];
    [self setM_textField:nil];
    [self setM_disabledTextField:nil];
    [self setM_selectedTextField:nil];
    [self setM_disabledWithText:nil];
    [self setM_placeholderTextField:nil];
    [self setM_backgroundTextField:nil];
    [super viewDidUnload];
}
- (IBAction)tester:(id)sender {
     [self.view endEditing:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *) textField{
    [self.view endEditing:YES];
    return NO;
}
@end
