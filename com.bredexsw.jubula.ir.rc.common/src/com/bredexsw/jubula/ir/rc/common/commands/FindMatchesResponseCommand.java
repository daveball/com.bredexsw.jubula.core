/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.rc.common.commands;

import org.eclipse.jubula.communication.ICommand;
import org.eclipse.jubula.communication.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.eclipse.jubula.communication.message.FindMatchesResponseMessage;

/**
 * Response command for getting FindMatches
 * @author BREDEX GmbH
 */
public class FindMatchesResponseCommand implements ICommand {
    /** the logger */
    private static Logger log = LoggerFactory
            .getLogger(FindMatchesResponseCommand.class);

    /** the FindMatchesResponseMessage */
    private FindMatchesResponseMessage m_message;

    /** Default constructor */
    public FindMatchesResponseCommand() {
        //empty
    }

    /**
     * {@inheritDoc}
     */
    public Message execute() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public FindMatchesResponseMessage getMessage() {
        return m_message;
    }

    /**
     * {@inheritDoc}
     */
    public void setMessage(Message message) {
        m_message = (FindMatchesResponseMessage)message;
    }
    
    /**
     * {@inheritDoc}
     */
    public void timeout() {
        log.error(this.getClass().getName() + ".timeout() called"); //$NON-NLS-1$
    }

}
