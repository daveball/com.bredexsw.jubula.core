/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/
package com.bredexsw.jubula.ir.rc.common.tester;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.jubula.communication.Communicator;
import org.eclipse.jubula.communication.message.FindMatchesMessage;
import org.eclipse.jubula.communication.message.FindMatchesResponseMessage;
import org.eclipse.jubula.rc.common.AUTServer;
import org.eclipse.jubula.rc.common.driver.IRobot;
import org.eclipse.jubula.rc.common.exception.StepExecutionException;
import org.eclipse.jubula.rc.common.tester.AbstractUITester;
import org.eclipse.jubula.tools.constants.TimeoutConstants;
import org.eclipse.jubula.tools.exception.CommunicationException;
import org.eclipse.jubula.tools.i18n.I18n;
import org.eclipse.jubula.tools.objects.event.EventFactory;
import org.eclipse.jubula.tools.serialisation.SerializedImage;
import org.eclipse.jubula.tools.utils.TimeUtil;

import com.bredexsw.jubula.ir.rc.common.commands.FindMatchesResponseCommand;

/**
 * Implementation of Actions that work with image recognition
 * @author BREDEX GmbH
 * 
 */
public class ImageTester extends AbstractUITester {

    /** the robot used in the actions */
    private IRobot m_robot;
    
    /** default constructor that also gets the robot */
    public ImageTester() {
        m_robot = AUTServer.getInstance().getRobot();
    }
    
    /** {@inheritDoc} */
    public String[] getTextArrayFromComponent() {
        return null;
    }

    
    
    /**
     * CAP to click on all locations that look like the given image
     * @param templatePath
     *            path of image to find
     */
    public void rcClickImage(String templatePath) {        
        FindMatchesResponseMessage message = getMessage(templatePath, 0);
        List<Rectangle> regions = message.getMatches();                    

        if (regions.size() < 1) {
            throw new StepExecutionException(I18n.getString(
                    "TestErrorEvent.NoImageFound", true), 
                    EventFactory.createActionError(
                            "TestErrorEvent.NoImageFound"));
        }

        int xCenter = regions.get(0).x;
        xCenter += regions.get(0).width / 2;
        int yCenter = regions.get(0).y;
        yCenter += regions.get(0).height / 2;
        m_robot.clickAtCoordinates(xCenter, yCenter);
        TimeUtil.delay(50);

    }
    
    /**
     * CAP to click on all locations that look like the given image
     * @param bigTemplate
     *            path of image to find
     * @param smallTemplate
     *            path of image to find inside bigTemplate
     */
    public void rcClickInsideImage(String bigTemplate, String smallTemplate) {
            //if not a single small template is found we throw a StepExecutionException
        boolean foundOne = false;
        FindMatchesResponseMessage message = getMessage(bigTemplate, 0);
        List<Rectangle> regions = message.getMatches();                    

        if (regions.size() < 1) {
            throw new StepExecutionException(I18n.getString(
                    "TestErrorEvent.NoImageFound", true), 
                    EventFactory.createActionError(
                            "TestErrorEvent.NoImageFound"));
        }
//      for (int i = 0; i < regions.size(); i++) {
        FindMatchesResponseMessage innerMessage = getMessage(
                smallTemplate, bigTemplate, 0);
        List<Rectangle> innerRegions = innerMessage.getMatches();
        if (innerRegions.size() > 0) {
            foundOne = true;
        }
//      for (int j = 0; j < innerRegions.size(); j++) {
        int xCenter = regions.get(0).x;
        xCenter += innerRegions.get(0).x;
        xCenter += innerRegions.get(0).width / 2;
        int yCenter = regions.get(0).y;
        yCenter += innerRegions.get(0).y;
        yCenter += innerRegions.get(0).height / 2;
        
        m_robot.clickAtCoordinates(xCenter, yCenter);
        TimeUtil.delay(50);
//                }
//            }
        if (!foundOne) {
            throw new StepExecutionException(I18n.getString(
                    "TestErrorEvent.NoImageFound", true), 
                    EventFactory.createActionError(
                            "TestErrorEvent.NoImageFound"));
        }

    }
    
    /**
     * CAP to wait for given image
     * @param templatePath
     *            path of image to find
     * @param timeout maximum amount of time to wait
     */
    public void rcWaitForImage(String templatePath, int timeout) {
        long starttime = System.currentTimeMillis();
        int currenttime;
        List<Rectangle> regions = new ArrayList<Rectangle>();
        
        while (regions.size() < 1) {
            currenttime = (int) (System.currentTimeMillis() - starttime);
            FindMatchesResponseMessage message =
                    getMessage(templatePath, timeout - currenttime);
            regions = message.getMatches();  
            
        }
    }
    
    /**
     * CAP to wait for disappearance of given image
     * @param templatePath
     *            path of image to find
     * @param timeout maximum amount of time to wait
     */
    public void rcWaitForImageDisappearance(
            String templatePath, int timeout) {
        long starttime = System.currentTimeMillis();
        int currenttime;
        List<Rectangle> regions = new ArrayList<Rectangle>();
        FindMatchesResponseMessage message = getMessage(templatePath, timeout);
        regions = message.getMatches();   
        
        while (regions.size() > 0) {
            currenttime = (int) (System.currentTimeMillis() - starttime);
            message = getMessage(templatePath, timeout - currenttime);
            regions = message.getMatches();  
            
        }
    }
    
    /**
     * CAP to drag given image to an endpoint which is also given as an image
     * @param startImage image to drag
     * @param endImage place of image to drop
     */
    public void rcDragDropImage(String startImage, String endImage) {
        try {
            FindMatchesResponseMessage startMessage = getMessage(startImage, 0);
            List<Rectangle> startRegions = startMessage.getMatches();
            FindMatchesResponseMessage endMessage = getMessage(endImage, 0);
            List<Rectangle> endRegions = endMessage.getMatches(); 

            if (startRegions.size() > 1 || endRegions.size() > 1)  {
                throw new StepExecutionException(I18n.getString(
                        "TestErrorEvent.MoreThanOneImageFound", true), 
                        EventFactory.createActionError(
                                "TestErrorEvent.MoreThanOneImageFound"));
            }
            if (startRegions.size() < 1 || endRegions.size() < 1) {
                throw new StepExecutionException(I18n.getString(
                        "TestErrorEvent.NoImageFound", true), 
                        EventFactory.createActionError(
                                "TestErrorEvent.NoImageFound"));
            }
            Robot clicker = new Robot();

            int xCenterStart = startRegions.get(0).x;
            xCenterStart += startRegions.get(0).width / 2;
            int yCenterStart = startRegions.get(0).y;
            yCenterStart += startRegions.get(0).height / 2;

            clicker.mouseMove(xCenterStart, yCenterStart);
            clicker.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            
            int xCenterEnd = endRegions.get(0).x;
            xCenterEnd += endRegions.get(0).width / 2;
            int yCenterEnd = endRegions.get(0).y;
            yCenterEnd += endRegions.get(0).height / 2;
            
            clicker.mouseMove(xCenterEnd, yCenterEnd);
            clicker.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            
                       
        }  catch (AWTException e) {
            e.printStackTrace();
            throw new StepExecutionException(I18n.getString(
                    "TestErrorEvent.ClickNotPossible", true), 
                    EventFactory.createActionError(
                            "TestErrorEvent.ClickNotPossible"));
        }
    }
    /**
     * returns the found matches given the image, search image
     * is the screen of the AUT
     * @param templatePath image to search for
     * @param timeout timeout for findRegions
     * @return found matches in a list
     */
    private FindMatchesResponseMessage getMessage(
            String templatePath, int timeout) {
        try {
            Communicator autAgentCommunicator = AUTServer.getInstance()
                    .getServerCommunicator();
            String pathToTemplate = templatePath;
            
    
            pathToTemplate = pathToTemplate.replace("\\", File.separator);
            pathToTemplate = pathToTemplate.replace("/", File.separator);
            SerializedImage template = SerializedImage.
                    computeSerializeImage(ImageIO.read(
                            new File(pathToTemplate)));

            SerializedImage currentScreen = SerializedImage.
                    computeSerializeImage(m_robot.createFullScreenCapture());
                      
            FindMatchesResponseCommand imageMatchCommand = 
                    new FindMatchesResponseCommand();
            FindMatchesMessage imageMatchMessage = 
                    new FindMatchesMessage();
            imageMatchMessage.setAutScreenshot(currentScreen);
            imageMatchMessage.setTemplate(template);
            imageMatchMessage.setTimeout(timeout);
            autAgentCommunicator.request(
                    imageMatchMessage, imageMatchCommand, 
                    TimeoutConstants.CLIENT_SERVER_TIMEOUT_FIND_MATCHES);
            
            //since request() does not block we have to wait until the message gets filled
            while (imageMatchCommand.getMessage() == null) {
                TimeUtil.delay(70);
            }
            
            checkForExceptions(imageMatchCommand);
            
            return imageMatchCommand.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * returns the found matches given the images
     * @param templatePath path to image to search for
     * @param imagePath path to image to search in
     * @param timeout timeout for findRegions
     * @return found matches in a list
     */
    private FindMatchesResponseMessage getMessage(
            String templatePath, String imagePath, int timeout) {
        try {
            Communicator autAgentCommunicator = AUTServer.getInstance()
                    .getServerCommunicator();
            String pathToTemplate = templatePath;
    
            pathToTemplate = pathToTemplate.replace("\\", File.separator);
            pathToTemplate = pathToTemplate.replace("/", File.separator);
            SerializedImage template = SerializedImage.
                    computeSerializeImage(ImageIO.read(
                            new File(pathToTemplate)));

            String pathToImage = imagePath;
            
            pathToImage = pathToImage.replace("\\", File.separator);
            pathToImage = pathToImage.replace("/", File.separator);
            
            SerializedImage currentScreen = SerializedImage.
                    computeSerializeImage(ImageIO.read(
                            new File(pathToImage)));
                      
            FindMatchesResponseCommand imageMatchCommand = 
                    new FindMatchesResponseCommand();
            FindMatchesMessage imageMatchMessage = 
                    new FindMatchesMessage();
            imageMatchMessage.setAutScreenshot(currentScreen);
            imageMatchMessage.setTemplate(template);
            autAgentCommunicator.request(
                    imageMatchMessage, imageMatchCommand, 
                   TimeoutConstants.CLIENT_SERVER_TIMEOUT_FIND_MATCHES);
            
            //since request() does not block we have to wait until the message gets filled
            while (imageMatchCommand.getMessage() == null) {
                TimeUtil.delay(70);
            }
            checkForExceptions(imageMatchCommand);
            return imageMatchCommand.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * checks whether there are exceptions in the findmatches message,
     * if so it throws the appropriate StepExecutionException
     * @param imageMatchCommand command to check for exceptions
     */
    private void checkForExceptions(
            FindMatchesResponseCommand imageMatchCommand) {
        if (imageMatchCommand.getMessage().getException() != null) {
            if (imageMatchCommand.getMessage().
                    getException().contains("Timeout")) {
                throw new StepExecutionException(I18n.getString(
                        "TestErrorEvent.TimeoutExpired", true), 
                        EventFactory.createActionError(
                                "TestErrorEvent.TimeoutExpired"));
            }
            
            if (imageMatchCommand.getMessage().
                    getException().contains("incompatible")) {
                throw new StepExecutionException(I18n.getString(
                        "TestErrorEvent.IncompatibleImage", true), 
                        EventFactory.createActionError(
                                "TestErrorEvent.IncompatibleImage"));
            }
            
        }
    }
}
