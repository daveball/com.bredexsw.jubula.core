/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13473 $
 *
 * $Date: 2010-12-02 18:00:11 +0100 (Thu, 02 Dec 2010) $
 *
 * $Author: marc $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.jubula.client.monitoring.validator;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import com.bredexsw.jubula.client.monitoring.i18n.Messages;

/**
 * A validator to verify that the given pattern is valid
 * 
 * @author BREDEX GmbH
 */
public class PatternValidator implements IValidator {
    /**
     * default constructor
     */
    public PatternValidator() {
        // default
    }

    /**
     * {@inheritDoc}
     */
    public IStatus validate(Object value) {
        try {
            Pattern.compile(String.valueOf(value));
        } catch (PatternSyntaxException p) {

            return ValidationStatus.error(Messages.InvalidPattern);
        }
        return ValidationStatus.ok();
    }
}
