/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.trivialfinder;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.bredexsw.jubula.ir.core.ImageMatch;
import com.bredexsw.jubula.ir.core.SearchOptions;
import com.bredexsw.jubula.ir.core.exceptions.ImageTimeoutException;
import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;

/**
 * finds the Regions using a simple Algorithm
 * @author BREDEX GmbH
 */
public class TrivialFinder implements IRegionFinder {
      
    /** factor for saying into how many blocks an image should be separated */
    private int m_blockfactor = 4;
    
    /** 
     * threshold for the amount of blocks that can have different
     * classification values between subImage and img 
     */
    private int m_maxmismatch = 4;
    
    /** threshold for average similarity */
    private int m_averagethreshold = 5;

    /** threshold for cross correlation */
    private double m_threshold = 0.8;
    
    /** factor for jumpsize */
    private double m_factor = 0.1;
    
    /** width of searchImage */
    private int m_width;
    
    /** height of searchImage */
    private int m_height;
    
    /** width of subImage */
    private int m_subWidth;
    
    /** height of subImage */
    private int m_subHeight; 
    
    /** average color values of subImage */
    private double[] m_averageSub;
    
    /** pixel value of subImage */
    private double m_pixelValueSub;
    
    /**
     * matrix for internal color values of the searchImage
     * (first dimension is to differentiate between the colors,
     * 0 for red, 1 for green, 2 for blue)  
     */
    private int[][][] m_integralImage;
    
    /**
     * matrix for internal color values of the subImage
     * (first dimension is to differentiate between the colors,
     * 0 for red, 1 for green, 2 for blue) 
     */
    private int[][][] m_integralSubImage;
    
    /** matrix with average color values of the blocks of the subImage*/
    private double[][][] m_averageBlocksSub;   
    
    /** the search options for the action */
    private SearchOptions m_searchOptions;
    
    /** {@inheritDoc} 
     * @throws ImageTimeoutException if timeout occurs*/
    public List<ImageMatch> findRegions(BufferedImage subImage,
        BufferedImage img, SearchOptions so) throws ImageTimeoutException {
        List<ImageMatch> roughFinds = new ArrayList<ImageMatch>();
        setUpValues(subImage, img);

        m_searchOptions = so;

        final int jumpsizeVert = Math.max(1, (int) (m_factor * m_subHeight));
        final int jumpsizeHoriz = Math.max(1, (int) (m_factor * m_subWidth));
        
        final int xPosMax = m_width - m_subWidth;
        final int yPosMax = m_height - m_subHeight;

        /*
         *  first, go through the image with a certain step length, 
         *  calculate the difference between the average value of colors of the template
         *  and the searchImage in the given region
         *  if the difference between ALL colors is smaller than the average threshold
         *  write the coordinate in a list of rough finds
         */
        for (int xPos = 0; xPos < xPosMax; xPos += jumpsizeHoriz) {
            for (int yPos = 0; yPos < yPosMax; yPos += jumpsizeVert) {
                if (isWeakCheckSimilar(subImage, img, xPos, yPos)) {
                    roughFinds.add(new ImageMatch(new Rectangle(
                            xPos, yPos, subImage.getWidth(), 
                            subImage.getHeight()), 1));
                }
            }
        }
        /*
         * now, go through all the rough finds and compute the cross correlation
         * in an area around the coordinates of the found regions
         */
        return findPreciseMatches(roughFinds, subImage, img,
                jumpsizeVert, jumpsizeHoriz, xPosMax, yPosMax);
    }
 
    /** 
     * set up values later needed for finding regions (like width and average color values) 
     * @param subImage small image
     * @param img big image
     */
    private void setUpValues(BufferedImage subImage, BufferedImage img) {
        m_width = img.getWidth();
        m_height = img.getHeight();
        m_subWidth = subImage.getWidth();
        m_subHeight = subImage.getHeight();
        
        /*
         *  the integral images are needed to quickly calculate the average color value
         *  of an image
         *  They are calculated by adding to each pixel the sum of the pixels of their 
         *  column and the integral value of the pixel to their left
         */
        m_integralSubImage = calculateIntegralImage(subImage);
        m_integralImage = calculateIntegralImage(img);
        m_averageBlocksSub =
                calculateBlockSumsSub(subImage);
        
        /*
         * we can calculate the average values of the template this early
         * because the template does not change
         */
        m_averageSub = calcAverageSub();
    
        /*
         *  for the same reason we can already calculate the squared summed pixel value
         *  which is used for normalization later
         */
        m_pixelValueSub = calcNormValueSub(subImage);      
    }

    /** 
     * calculates the integral images of
     * the color channels in x-direction to a given image
     * @param image image to calculate
     * @return values of the integral image in a matrix
     */
    private int[][][] calculateIntegralImage(
            BufferedImage image) {
        int[][][] result = new int
                [image.getWidth() + 1][image.getHeight() + 1][3];
        int[][][] sumValues = calculateColumnSums(image);
        
        for (int colors = 0; colors < result[0][0].length; colors++) {
            for (int x = 0; x < image.getWidth() + 1; x++) {
                result[x][0][colors] = 0;
            }
            for (int y = 0; y < image.getHeight() + 1; y++) {
                result[0][y][colors] = 0;
            }
            for (int x = 1; x < image.getWidth() + 1; x++) {
                for (int y = 1; y < image.getHeight() + 1; y++) {
                    result[x][y][colors] = 
                            result[x - 1][y][colors]
                                    + sumValues[x][y][colors];
                }
            }
        }
        return result;
    }

    /** 
     * calculates the column sum for the integral image to a given image to all 
     * color channels
     * @param image image to calculate
     * @return values of the column sums in a matrix
     */
    private int[][][] calculateColumnSums(BufferedImage image) {
        int[][][] result = new int
                [image.getWidth() + 1][image.getHeight() + 1][3];

        for (int colors = 0; colors < result[0][0].length; colors++) {
            for (int x = 0; x < image.getWidth() + 1; x++) {
                result[x][0][colors] = 0;
            }
            for (int y = 0; y < image.getHeight() + 1; y++) {
                result[0][y][colors] = 0;
            }
            for (int x = 1; x < image.getWidth() + 1; x++) {
                for (int y = 1; y < image.getHeight() + 1; y++) {
                    int currentValue;
                    int bitshift;
                    if (colors == 0) {
                        bitshift = 16;
                    } else if (colors == 1) {
                        bitshift = 8;
                    } else {
                        bitshift = 0;
                    }
                    currentValue =
                            (image.getRGB(x - 1, y - 1) >> bitshift) & 0xff;
                    result[x][y][colors] =
                            result[x][y - 1][colors] + currentValue;
                }
            }
        }
        return result;
    }

    /**
     * checks if subImage is similar to img at the position using weak classifiers
     * @param subImage image to search for
     * @param img image to search in
     * @param xPos x-position to search at in img
     * @param yPos y-position to search at in img
     * @return true if similar, else false
     */
    private boolean isWeakCheckSimilar(BufferedImage subImage,
            BufferedImage img, int xPos, int yPos) {
        double[] averageImg = calcAverageImg(xPos, yPos);

        if (averageIsSimilar(subImage, img, xPos, yPos, averageImg)
                && blocksAreSimilar(subImage, img, xPos, yPos, averageImg)) {
            return true;
        }
        return false;
    }
    
    /**
     * weak classifier to check whether certain blocks from the subImage
     * and the region of the search image are smaller or bigger then their average
     * in that position
     * @param subImage image to search for
     * @param img image to search in
     * @param xPos x-position to search at in img
     * @param yPos y-position to search at in img
     * @param averageImg average color value of image in region
     * @return true if no more than a certain amount of blocks are different
     */
    private boolean blocksAreSimilar(BufferedImage subImage, BufferedImage img,
            int xPos, int yPos, double[] averageImg) {
        double[][][] blockValueImg =
                new double[m_blockfactor][m_blockfactor][3];
        int blockWidth = m_subWidth / m_blockfactor;
        int blockHeight = m_subHeight / m_blockfactor;
        int blockPixels = blockWidth * blockHeight;
        int mismatchCounter = 0;
        
        for (int blockX = 0; blockX < m_blockfactor; blockX++) {
            for (int blockY = 0; blockY < m_blockfactor; blockY++) {
                int x = xPos + (blockX * blockWidth);
                int y = yPos + (blockY * blockHeight);
//                System.out.println("x: " + x + " y: " + y);
                blockValueImg[blockX][blockY] = calculateSumForRegion(
                        m_integralImage, x, y, blockWidth, blockHeight); 
                for (int color = 0; color < 3; color++) {
                    //problem: should difference to average have to match in all color channels?
                    double diffImg = (blockValueImg
                            [blockX][blockY][color] / blockPixels)
                            - averageImg[color];
                    double diffSub = (m_averageBlocksSub
                            [blockX][blockY][color] / blockPixels)
                            - m_averageSub[color];
                    if (diffImg <= 0 && diffSub > 0 
                            || diffImg > 0 && diffSub <= 0) {
                        mismatchCounter++;
                    }
                    if (mismatchCounter >= m_maxmismatch - 1) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * true if the difference of the average colors
     * of the images at the offset are smaller than the threshold
     * @param subImage image to search for
     * @param img image to search in
     * @param xPos x-position to search at in img
     * @param yPos y-position to search at in img
     * @param averageImg average color value of image in region
     * @return true if smaller, else false
     */
    private boolean averageIsSimilar(BufferedImage subImage, BufferedImage img,
            int xPos, int yPos, double[] averageImg) {
        boolean allSimilar = true;
        
        for (int i = 0; i < 3; i++) {
            boolean colorIsSimilar = Math.abs(averageImg[i]
                    - m_averageSub[i]) < m_averagethreshold;
           
            if (!colorIsSimilar) {
                allSimilar = false;
            }
        }
        return allSimilar;
    }
    /**
     * use cross correlation to find the matches around the given list of image matches
     * @param roughFinds rough image matches to find real matches around
     * @param subImage template to search for
     * @param img image to search in
     * @param xPosMax maximum coordinate to go in x-direction
     * @param yPosMax maximum coordinate to go in y-direction
     * @param factorHoriz factor used to calculate left and right bound
     * @param factorVert factor used to calculate upper and lower bound
     * @return list of fine matches
     */
    private List<ImageMatch> findPreciseMatches(List<ImageMatch> roughFinds,
            BufferedImage subImage, BufferedImage img,
            int factorVert, int factorHoriz, int xPosMax, int yPosMax) 
        throws ImageTimeoutException {
        double starttime = System.currentTimeMillis();
        boolean timeoutIsSet = false;
        if (m_searchOptions != null) {
            if (m_searchOptions.getTimeout() > 0) {
                timeoutIsSet = true;
            }
        }
        double similarity;
        List<ImageMatch> result = new ArrayList<ImageMatch>();
        for (ImageMatch rf : roughFinds) {
    
            int leftBound = Math.max(rf.getRect().x - (factorHoriz / 2), 0);
            int rightBound = Math.min(rf.getRect().x + (factorHoriz / 2),
                    xPosMax);
            int upperBound = Math.max(rf.getRect().y - (factorVert / 2), 0);
            int lowerBound = Math.min(rf.getRect().y + (factorVert / 2),
                    yPosMax); 
            /*
             * we want to make sure that only one match is found in the area around
             * one roughFind because otherwise it would be possible that one image
             * can be found several times because the similarity doesn't change too
             * much when moving one pixel
             */
            ImageMatch bestResult = new ImageMatch(null, 0);
            for (int xPos = leftBound; xPos < rightBound; xPos++) {
                for (int yPos = upperBound; yPos < lowerBound; yPos++) {
                    similarity = calcImageDistance(subImage,
                          img, xPos, yPos);
                    if (similarity > m_threshold) {
                        if (bestResult.getRect() == null
                                || similarity > bestResult.getSim()) {
                            bestResult = new ImageMatch(new Rectangle(
                                xPos, yPos, subImage.getWidth(), 
                                subImage.getHeight()), similarity);
                        }
                    }
                    if (timeoutIsSet) {
                        double currentTime = 
                                System.currentTimeMillis() - starttime;
                        if (currentTime > m_searchOptions.getTimeout()) {
                            throw new ImageTimeoutException();
                        }
                    }
                }
            }           
            // only add a new match if one similarity was good enough
            if (bestResult.getRect() != null) {
                checkForCloseNeighbors(result, bestResult);
//                result.add(bestResult);
            }
        }
        return result;
    }

    /**
     * calculates the similarity between the two given images and the offset
     * @param subImage small image
     * @param img big image, limited through offset
     * @param xPos x-position to search at in img
     * @param yPos y-position to search at in img
     * @return double value of similarity
     */
    private double calcImageDistance(BufferedImage subImage, BufferedImage img,
            int xPos, int yPos) {

        double numerator = 0;
        int currentValue1;
        int currentValue2;        

        // must be calculated here for image because we have to find the average of only 
        // a part of the searchImage
        double[] averageImg = calcAverageImg(xPos, yPos);
        
        /*
         * we go through the template and the search image and
         * calculate the cross correlation in the region
         */

        for (int subImageX = 0; subImageX < m_subWidth 
                || xPos + subImageX >= m_width; subImageX++) {
            for (int subImageY = 0; subImageY < m_subHeight 
                    || yPos + subImageY >= m_height; subImageY++) {
                currentValue1 = subImage.getRGB(subImageX, subImageY);
                currentValue2 = img.getRGB(xPos 
                        + subImageX, yPos + subImageY);

                double[] colorsSub = new double[3];
                colorsSub[0] = (currentValue1 >> 16) & 0xff;
                colorsSub[1] = (currentValue1 >> 8) & 0xff;
                colorsSub[2] = (currentValue1) & 0xff;
                
                double[] colorsImg = new double[3];
                colorsImg[0] = (currentValue2 >> 16) & 0xff;
                colorsImg[1] = (currentValue2 >> 8) & 0xff;
                colorsImg[2] = (currentValue2) & 0xff;
                
                double currentDiff = 0;
                
                /* 
                 * for calculating the numerator we simply multiply the pixel
                 * values at the position minus their average
                 */
                for (int color = 0; color < 3; color++) {
//                    currentDiff += 
//                            Math.pow(colorsSub[color] - colorsImg[color], 2);
//                    currentDiff += colorsSub[color] * colorsImg[color];
                    currentDiff += (colorsSub[color] - m_averageSub[color])
                        * (colorsImg[color] - averageImg[color]);
                }
                numerator += currentDiff;
            }
        }
        
        double denominator = normalize(img, xPos, yPos,
                averageImg);
        return numerator / denominator;
    }

    /**
     * method that checks whether any other found regions are near the new one
     * if none are there the result gets added
     * if there is a close neighbor but the new result has higher similarity it gets added
     * and the old result gets removed
     * if the old result has higher similarity nothing happen
     * @param result list of previous results
     * @param newResult new result
     */
    private void checkForCloseNeighbors(List<ImageMatch> result,
            ImageMatch newResult) {
        final int denominator = 3;
        int minXDistance = 
                Math.max((int) ((m_factor * m_subWidth) / denominator), 3);
        int minYDistance = 
                Math.max((int) ((m_factor * m_subHeight) / denominator), 3);
        
        boolean isSet = false;
        for (ImageMatch res: result) {
            int xDifference =
                    Math.abs(newResult.getRect().x - res.getRect().x);
            int yDifference = 
                    Math.abs(newResult.getRect().y - res.getRect().y);
            if (xDifference <= Math.max(1,
                    minXDistance)
                    && yDifference <= Math.max(1,
                            minYDistance)) {
                if (newResult.getSim() > res.getSim()) {
                    result.remove(res);
                    result.add(newResult);
                    isSet = true;
                    break;
                }
            }
        }
        if (!isSet) {
            result.add(newResult);           
        }
        
    }  
    
    /**
     * normalizes the value of the cross correlation
     * distance for each color channel
     * @param img searchImage
     * @param xPos x-limiter for img
     * @param yPos y-limiter for img
     * @param averageImg average color values of image in limited area    
     * @return normalized value of distance
     */
    private double normalize(BufferedImage img,
            int xPos, int yPos, double[] averageImg) {
        double pixelValueImg = 0;
        
        /*
         * for normalization we first multiply the sum of
         * the pixel values of both images minus their averages
         */
        int currentValueImg;
        for (int subImageX = 0; subImageX < m_subWidth
                || xPos + subImageX >= m_width; subImageX++) {
            for (int subImageY = 0; subImageY < m_subHeight
                    || yPos + subImageY >= m_height; subImageY++) {
                currentValueImg = img.getRGB(xPos
                        + subImageX, yPos + subImageY);

                int[] colorsImg = new int[3];
                colorsImg[0] = (currentValueImg >> 16) & 0xff;
                colorsImg[1] = (currentValueImg >> 8) & 0xff;
                colorsImg[2] = (currentValueImg) & 0xff;

                double pixelValueCurrImg = 0;
                for (int color = 0; color < 3; color++) {
                    pixelValueCurrImg += 
                            Math.pow(colorsImg[color] - averageImg[color], 2);
//                    pixelValueCurrImg += Math.pow(colorsImg[color], 2);
                }                
                pixelValueImg += pixelValueCurrImg;                
            }
        }
        // after that we calculate the square root of the result 
        return Math.sqrt(m_pixelValueSub * pixelValueImg);
    }
    /**
         * calculates the pixel value of the cross correlation
         * for the sub image for normalization for each color channel
         * @param subImage first image
         * @return pixel value used for normalization
         */
    private double calcNormValueSub(BufferedImage subImage) {
        double pixelValueSub = 0;
        int currentValueSub;
        for (int subImageX = 0; subImageX < m_subWidth; subImageX++) {
            for (int subImageY = 0; subImageY < m_subHeight; subImageY++) {
                currentValueSub = subImage.getRGB(subImageX, subImageY);

                int[] colorsSub = new int[3];
                colorsSub[0] = (currentValueSub >> 16) & 0xff;
                colorsSub[1] = (currentValueSub >> 8) & 0xff;
                colorsSub[2] = (currentValueSub) & 0xff;
                                
                double pixelValueCurrSub = 0;
                for (int i = 0; i < 3; i++) {
                    pixelValueCurrSub +=
                        Math.pow(colorsSub[i] - m_averageSub[i], 2);
//                        pixelValueCurrSub += Math.pow(colorsSub[i], 2);
                }               
                pixelValueSub += pixelValueCurrSub;
            }
        }       
        return pixelValueSub;
    }

    /**
     * Utility method to alter the currentValues for the template
     * (meaning to subtract the average of the image from it)
     * @return altered Value
     */
    private double[] calcAverageSub() {
        double[] average = new double[3];

        average = calculateSumForRegion(
                m_integralSubImage, 0, 0, m_subWidth, m_subHeight);

        for (int color = 0; color < 3; color++) {
            average[color] = average[color]
                    / ((double) (m_subWidth * m_subHeight));
        }
        return average;
    }
    
    /**
     * Utility method  to alter the currentValues for the searchImage
     * (meaning to subtract the average of the image from it)
     * @param xPos x-limiter for img
     * @param yPos y-limiter for img
     * @return altered Value
     */
    private double[] calcAverageImg(int xPos, int yPos) {
        double[] average = new double[3];

        average = calculateSumForRegion(
                m_integralImage, xPos, yPos, m_subWidth, m_subHeight);

        for (int color = 0; color < 3; color++) {
            average[color] = 
                    average[color] / ((double) (m_subWidth * m_subHeight));
        }
        return average;
    }
    
    /**
     * calculates the sum of the region of the given integral images
     * @param integralImage integral image to calculate sum from
     * @param xPos left border
     * @param yPos upper border
     * @param width right border
     * @param height lower border
     * @return sum of pixel values of the region for each color channel
     */
    private double[] calculateSumForRegion(int[][][] integralImage, int xPos,
            int yPos, int width, int height) {
        double result[] = new double[3];
        for (int color = 0; color < 3; color++) {
            result[color] = 0;
            result[color] += integralImage
                    [xPos + width][yPos + height][color];
            result[color] += integralImage[xPos][yPos][color];
            result[color] -=  integralImage[xPos + width][yPos][color];
            result[color] -= integralImage[xPos][yPos + height][color];
        }
        return result;
    }

    /**
     * calculates the block values for the given image
     * @param image image to calculate blocks to
     * @return matrix with average values of the blocks for all three color channels
     */
    private double[][][] calculateBlockSumsSub(
            BufferedImage image) {
        double[][][] result = new double
                [m_blockfactor][m_blockfactor][3];
        int blockWidth = m_subWidth / m_blockfactor;
        int blockHeight = m_subHeight / m_blockfactor;
        
        // go through all blocks
        for (int blockX = 0; blockX < m_blockfactor; blockX++) {
            for (int blockY = 0; blockY < m_blockfactor; blockY++) {
               // inside a block get the sum of pixel values,
                int x = blockX * blockWidth;
                int y = blockY * blockHeight;
                result[blockX][blockY] = calculateSumForRegion(
                            m_integralSubImage, x, y, blockWidth, blockHeight); 
            }
        }
        return result;
    }
    
    /**
     * sets the averageThreshold
     * @param average the averageThreshold
     */
    public void setAverage(int average) {
        m_averagethreshold = average;
    }

    /**
     * sets the jumpsize factor
     * @param factor the factor
     */    
    public void setFactor(double factor) {
        m_factor = factor;
    }
    
    /**
     * sets the maximum mismatch
     * @param mismatch the mismatch
     */
    public void setMismatch(int mismatch) {
        m_maxmismatch = mismatch;
    }
    
    /**
     * sets the amount of blocks
     * @param block the amount of blocks
     */
    public void setBlock(int block) {
        m_blockfactor = block;
    }
    
    /**
     * sets the threshold
     * @param threshold the threshold
     */
    public void setThreshold(double threshold) {
        m_threshold = threshold;
    }
    
    /**
     * gets the threshold
     * @return the threshold
     */
    public double getThreshold() {
        return m_threshold;
    }
    
    /**
     * gets the amount of blocks
     * @return the amount of blocks
     */
    public int getBlock() {
        return m_blockfactor;
    }
    
    /**
     * gets the maximum mismatch
     * @return the maximum mismatch
     */
    public int getMismatch() {
        return m_maxmismatch;
    }
    
    /**
     * gets the average threshold
     * @return the average threshold
     */
    public int getAverage() {
        return m_averagethreshold;
    }

    /**
     * get the factor
     * @return the factor
     */
    public double getFactor() {
        return m_factor;
    }
}
