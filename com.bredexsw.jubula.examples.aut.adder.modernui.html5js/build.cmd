echo on

: Remove generated folders
rmdir /S /Q adder.modernui.html5js\bin
rmdir /S /Q adder.modernui.html5js\bld
: Remove folders from HTML-CaA
del /Q adder.modernui.html5js\SimpleAdder.html

: Create folders for generated files
mkdir adder.modernui.html5js\bin

: Copy files from HTML-CaA into this modern UI HTML5/JS project
xcopy ..\..\jubula\org.eclipse.jubula.examples.aut.adder.html\resources\aut\SimpleAdder.html adder.modernui.html5js\.

REM call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86

REM Remove the following line after the project has been created.
: exit 0

"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.exe" adder.modernui.html5js.sln /deploy "Release|x86" /project adder.modernui.html5js\adder.modernui.html5js.jsproj /out adder.modernui.html5js\bin\log.txt

type adder.modernui.html5js\bin\log.txt

: exit 0