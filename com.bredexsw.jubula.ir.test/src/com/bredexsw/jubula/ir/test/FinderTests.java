/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.test;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Test;

import com.bredexsw.jubula.ir.core.Finders;
import com.bredexsw.jubula.ir.core.ImageMatch;
import com.bredexsw.jubula.ir.core.SearchOptions;
import com.bredexsw.jubula.ir.core.exceptions.ImageTimeoutException;
import com.bredexsw.jubula.ir.core.exceptions.IncompatibleImageException;
import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;
import com.bredexsw.jubula.ir.trivialfinder.TrivialFinder;

/**
 * Class for tests
 * @author BREDEX GmbH
 *
 */
public class FinderTests {

    /** part of path that is system dependent */
    private static final String SYSTEMDEPENDENTPART = "K:/";
    
    /** path to directory of templates */
    private static final String DIRECTEMPL =
            SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                + "OliverHoffmann/images/Templates/";
    
    /** path to directory of images */
    private static final String DIRECIMG =
            SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                + "OliverHoffmann/images/Screenshots/";
    
    /** list of paths to all templates */
    private static List<String> templatePaths = new ArrayList<String>();
    
    /** list of paths to all search images */
    private static List<String> imagePaths = new ArrayList<String>();
    
    /** list of correct coordinates for the images */
    private static List<Point[]> correctPlace = new ArrayList<Point[]>();
    
    /** counter for the amount of passed tests*/
    private static int passedCount;
    
    /** StringBuilder for output */
    private StringBuilder m_output;
    
    /** counter for the filename */
    private int m_count = 0;
      
    /**
     * test different variables for the trivialFinder,
     * first variable to set is blockfactor
     */
    @Test
    public void testVariables() {
        for (int h = 0; h < 3; h++) {
            TrivialFinder finder = new TrivialFinder();
            addImages();
            double[] thresholdValues = 
                    new double[] {
                        0.7, 0.9};        
            for (int i = 0; i < thresholdValues.length; i++) {
                finder.setThreshold(thresholdValues[i]);
                testBlock(finder);
            }
        }
    }
    
    /**
     * sets different blockfactors for the finder
     * @param finder finder with threshold
     */
    private void testBlock(TrivialFinder finder) {
        int[] blockValues = 
                new int[] {4, 7, 10, 12};
        for (int i = 0; i < blockValues.length; i++) {
            finder.setBlock(blockValues[i]);
            testMisMatch(finder);
        }
    }
    /**
     * sets different maxMismatches for the finder
     * @param finder finder with blockfactor and threshold
     */
    private void testMisMatch(TrivialFinder finder) {
        int[] mismatchValues = 
                new int[] {finder.getBlock(), finder.getBlock() / 2,
                    (int) (Math.pow(finder.getBlock(), 2) * 0.1),
                    (int) (Math.pow(finder.getBlock(), 2) * 0.2), 
                    finder.getBlock() * 2};
        for (int i = 0; i < mismatchValues.length; i++) {
            finder.setMismatch(mismatchValues[i]);
            testAverage(finder);
        }
    }

    /**
     * sets different averageThresholds for the finder
     * @param finder finder with threshold, blockfactor and maxMismatch
     */
    private void testAverage(TrivialFinder finder) {
        int[] averageValues = 
                new int[] {6};
        for (int i = 0; i < averageValues.length; i++) {
            finder.setAverage(averageValues[i]);
            testFactors(finder);
        }
    }

    /**
     * sets different jumpsize factors for the finder
     * @param finder finder with threshold, blockfactor, maxMismatch and averageThreshold
     */
    private void testFactors(TrivialFinder finder) {
        double[] factorValues = 
                new double[] {0.005, 0.007, 0.0085,
                    0.01, 0.03, 0.07, 0.1};
        for (int i = 0; i < factorValues.length; i++) {
            finder.setFactor(factorValues[i]);
            testTrivialFinder(finder);
            m_count++;
        }
    }

    /** 
     * test the finder with the parameters given to it
     * @param finder finder to test with given parameters 
     */
    public void testTrivialFinder(TrivialFinder finder) {
    
//        addImages();
//        List<IRegionFinder> finder = Finders.getFinders();
//        for (int l = 0; l < 1; l++) {           

        passedCount = 0;
        m_output = new StringBuilder();
        
        m_output.append("Value of variables: Threshold: "
                + finder.getThreshold() + " Averagethreshold: "
                + finder.getAverage() + ", factor: " + finder.getFactor()
                + ", Mismatch: " + finder.getMismatch() + ", blockfactor: "
                + finder.getBlock() + System.lineSeparator());

        for (int i = 0; i < templatePaths.size(); i++) {
            m_output.append("For Template: " + templatePaths.get(i) 
                    + ". For Search Image: " + imagePaths.get(i) 
                    + System.lineSeparator());
            calculateResults(finder, i);
        }


        String subFolder;
        try {
            FileWriter out = null;
            if (passedCount > 0) {
                subFolder = "interesting";
            } else {
                subFolder = "garbage";
            }
            out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                    + "OliverHoffmann/testresults/variableTests/"
                    + subFolder + "/" + "testResultsRecognition"
                    + m_count + ".txt");
            out.write(m_output.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//        }

    /**
     * test all ImageFinders that are Extensions of the "finders" extension point
     * with the input given in addImages()
     */
    @Test
    public void testFinders() {
        addImages();
        List<IRegionFinder> finders = Finders.getFinders();
        m_output = new StringBuilder();
        
        m_output.append("Test started on: " + new Date().toString() 
                + System.lineSeparator() + System.lineSeparator());

        double starttime = System.currentTimeMillis();
        for (int i = 0; i < templatePaths.size(); i++) {
            m_output.append("For Template: " + templatePaths.get(i) 
                    + ". For Search Image: " + imagePaths.get(i) 
                    + System.lineSeparator() + "Amount of actual appearances: "
                    + correctPlace.get(i).length + System.lineSeparator()
                    + System.lineSeparator());
            for (IRegionFinder f: finders) {
                calculateResults(f, i);
            }     
        }
        m_output.append("Test ended on: " + new Date().toString());
        StringBuilder outputStart = new StringBuilder();

        double completionTime =
                (System.currentTimeMillis() - starttime) / 1000;
        if (completionTime < 60) {
            outputStart.append("Total time needed for all tests: "
                + completionTime + "s");
        } else {
            outputStart.append("Total time needed for all tests: "
                    + ((int)Math.floor(completionTime / 60)) + "min " 
                    + (Math.round(completionTime) % 60) + "s");
        }
        try {
            FileWriter out = null;

            out = new FileWriter(
                    SYSTEMDEPENDENTPART + "guidancer/Workspace/users/"
                    + "OliverHoffmann/testresults/testResultsRecognition.txt");
            out.write(outputStart.toString());
            out.write(m_output.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * use findRegions() on the finder and append the results to m_output
     * @param f finder
     * @param imagePos place in the image list
     */
    private void calculateResults(IRegionFinder f, int imagePos) {
        try {
            boolean wrongPlace = false;
            boolean closeEnough = false;
            BufferedImage template = 
                    ImageIO.read(new File(DIRECTEMPL 
                            + templatePaths.get(imagePos)));
            BufferedImage searchImage =
                    ImageIO.read(new File(DIRECIMG
                            + imagePaths.get(imagePos)));
            double starttime = System.currentTimeMillis();
            try {
                List<ImageMatch> result = 
                        f.findRegions(template, searchImage,
                                new SearchOptions(false, false, 0, 180000)); 
                double endtime = System.currentTimeMillis();

                for (int j = 0; j < result.size(); j++) {
                    
                    for (int c = 0; c 
                            < correctPlace.get(imagePos).length; c++) {
                        if (result.get(j).getRect().x 
                                - correctPlace.get(imagePos)[c].getX() < 10 
                                && result.get(j).getRect().y - correctPlace.
                                get(imagePos)[c].getY() < 10) {
                            closeEnough = true;
                        }
                    }
                         
                    closeEnough = false;                
                }
                
                if (!wrongPlace && result.size() 
                        == correctPlace.get(imagePos).length) {
                    passedCount++;
                    double totaltime = (endtime - starttime) / 1000;
                    m_output.append(totaltime + System.lineSeparator());  
                } else {
                    m_output.append("999999"
                            + System.lineSeparator());  
                }

            } catch (ImageTimeoutException ite) {
                m_output.append("999999"
                        + System.lineSeparator());
            }

            
  
        } catch (IOException  e) {
            e.printStackTrace();
            System.out.println("Could not read image pair number " 
                    + (imagePos + 1));
        } catch (IncompatibleImageException e) {
            e.printStackTrace();
        }
        
    }

    /** Add image paths and actual appearances to lists */
    private void addImages() {
        imagePaths.add("WIN7/multipleError_new.PNG");
        templatePaths.add("WIN7/error_Multi.PNG");
        correctPlace.add(new Point[]{new Point(479, 367), new Point(655, 534)});
        
        imagePaths.add("SWT/1.png");
        templatePaths.add("SWT/1_buttonMulti.PNG");
        correctPlace.add(new Point[]{ new Point(92, 236), new Point(357, 26)});
        
        imagePaths.add("macOS/desk_new.PNG");
        templatePaths.add("macOS/desk_windows.PNG");
        correctPlace.add(new Point[]{new Point(13, 37)});
               
        imagePaths.add("JavaFX/1.png");
        templatePaths.add("JavaFX/1_button.PNG");
        correctPlace.add(new Point[]{new Point(835, 425)});
        

        imagePaths.add("Linux/lin_desktop.png");
        templatePaths.add("Linux/equalsButton_lin.png");
        correctPlace.add(new Point[]{new Point(835, 425)});
        
//        imagePaths.add("iOS/portrait1.png");
//        templatePaths.add("iOS/portrait/1_plus.PNG");
//        correctPlace.add(new Point[]{new Point(395, 41), new Point(395,
//                102), new Point(395, 165), new Point(395,
//                        226), new Point(395, 288)});

        imagePaths.add("CENTOS/2_new.PNG");
        templatePaths.add("CENTOS/2_window.PNG");
        correctPlace.add(new Point[]{new Point(648, 456)});
        
        imagePaths.add("HTML/PNG/Application.png");
        templatePaths.add("HTML/PNG/input_Text.PNG");
        correctPlace.add(new Point[]{new Point(15, 645)});
        
//        imagePaths.add("macOS/Application_new.PNG");
//        templatePaths.add("macOS/desk_button.PNG"); 
//        correctPlace.add(new Point[]{new Point(807, 19), new Point(839, 375),
//            new Point(858, 395), new Point(876, 415), new Point(894, 435),
//            new Point(913, 455), new Point(931, 475)});
        
//        very long execution time
//        imagePaths.add("RCP/CENTOS/Tree_Full.png");
//        templatePaths.add("RCP/tf_full.PNG");
    }
}
