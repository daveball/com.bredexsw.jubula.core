package org.eclipse.jubula.qa.aut.caa.rap.app;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jubula.qa.aut.caa.rp.perspective.Perspective;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.WorkbenchAdvisor;

/**
 * @author Markus Tiede
 * @created 25.11.2011
 */
public class Application implements IApplication {
    /**
     * {@inheritDoc}
     */
    public Object start(IApplicationContext context) throws Exception {
        return PlatformUI.createAndRunWorkbench(PlatformUI.createDisplay(),
                new WorkbenchAdvisor() {
                    @Override
                    public String getInitialWindowPerspectiveId() {
                        return Perspective.ID;
                    }
                });
    }

    /**
     * {@inheritDoc}
     */
    public void stop() {
        // currently empty
    }
}
