package com.bredexsw.jubula.iot.rc.common.tester;

import org.eclipse.jubula.rc.common.tester.AbstractUITester;

import com.bredexsw.jubula.iot.mqtt.communication.MQTTCommunicator;

public class IotTester extends AbstractUITester {

    public void rcSendRequest(String url) {
         MQTTCommunicator communicator = MQTTCommunicator.getInstance();
         communicator.sendCommand(url);
    }

    public String[] getTextArrayFromComponent() {
        return null;
    }
}
