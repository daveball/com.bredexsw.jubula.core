echo on

: Remove generated folders
rmdir /S /Q modernui.html5js\bin
rmdir /S /Q modernui.html5js\bld
: Remove folders from HTML-CaA
rmdir /S /Q modernui.html5js\pages
rmdir /S /Q modernui.html5js\style

: Create folders for generated files
mkdir modernui.html5js\bin
mkdir modernui.html5js\pages
mkdir modernui.html5js\style

: Copy files from HTML-CaA into this modern UI HTML5/JS project
xcopy /S ..\org.eclipse.jubula.qa.aut.caa.html\WebContent\pages modernui.html5js\pages\.
xcopy /S ..\org.eclipse.jubula.qa.aut.caa.html\WebContent\style modernui.html5js\style\.

REM call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86

REM Remove the following line after the project has been created.
: exit 0

"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.exe" modernui.html5js.sln /deploy "Release|x86" /project modernui.html5js\modernui.html5js.jsproj /out modernui.html5js\bin\log.txt

type modernui.html5js\bin\log.txt

: exit 0