﻿(function() {
    "use strict";

    WinJS.UI.Pages.define("/pages/home/home.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function(element, options) {
            // TODO: Initialize the page here.
            WinJS.Utilities.query("button").listen(
                "click", this.onButtonClicked, false);

        },

        onButtonClicked: function(eventInfo) {
            /*
            var userName = document.getElementById("nameInput").value;
            var greetingString = "Hello, " + userName + "!";
            document.getElementById("greetingOutput").innerText = greetingString;
            // Save the session data. 
            WinJS.Application.sessionState.greetingOutput = greetingString;
            */
            var subDialogName = eventInfo.srcElement.innerText.toLowerCase();
            var subDialogNameLowerCase = subDialogName.toLowerCase();
            WinJS.Navigation.navigate("/pages/home/" + subDialogNameLowerCase + "/pagecontrol.html");
        }



    });
})();
