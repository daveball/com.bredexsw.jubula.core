package org.eclipse.jubula.qa.aut.caa.rp.jface.perspective;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * 
 *
 * @author tobias
 * @created 25.08.2011
 * @version $Revision: 1.1 $
 */
public class Perspective implements IPerspectiveFactory {

    /**
     * {@inheritDoc}
     */
    public void createInitialLayout(IPageLayout layout) {
        layout.setEditorAreaVisible(false);
        layout.setFixed(true);

    }

}
