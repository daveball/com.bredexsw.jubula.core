/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.jface;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.MockModels;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.MockModels.Node;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * 
 *
 * @author tobias
 * @created 02.09.2011
 * @version $Revision: 1.1 $
 */
public class TreeViewerOpener extends AbstractApplicationWindow {
    
    /**
     * @param parentShell mainShell
     */
    public TreeViewerOpener(Shell parentShell) {
        super(parentShell);
    }

    /**
     * {@inheritDoc}
     */
    protected Composite makeContent(Composite parent) {
        TreeViewer viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL
                | SWT.V_SCROLL);
        viewer.setContentProvider(new ContentProvider());
        viewer.setLabelProvider(new OwnLabelProvider());
        viewer.setInput(MockModels.DEFAULT_TREE_MODEL);

        return parent;
    }

    /**
     * {@inheritDoc}
     */
    protected String getWindowTitle() {
        return TableViewer.class.getCanonicalName();
    }

    /**
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    public class ContentProvider implements ITreeContentProvider {

        /** m_model */
        private Node m_model;

        /**
         * {@inheritDoc}
         */
        public void dispose() {
            //empty
        }

        /**
         * {@inheritDoc}
         */
        public void inputChanged(Viewer viewer, Object oldInput, 
                Object newInput) {
            this.m_model = (Node) newInput;
        }

        /**
         * {@inheritDoc}
         */
        public Object[] getElements(Object inputElement) {
            return m_model.getChildren().toArray();
        }

        /**
         * {@inheritDoc}
         */
        public Object[] getChildren(Object parentElement) {
            Node n = (Node) parentElement;
            if (n.getChildren().size() > 0) {
                return n.getChildren().toArray();
            }
            return null;
        }

        /**
         * {@inheritDoc}
         */
        public Object getParent(Object element) {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        public boolean hasChildren(Object element) {
            Node n = (Node) element;
            return (n.getChildren().size() > 0);
        }
    }
    
    /**
     * 
     *
     * @author tobias
     * @created 05.09.2011
     * @version $Revision: 1.1 $
     */
    public class OwnLabelProvider extends LabelProvider {
        /**
         * @param element element
         * @return text
         */
        public String getText(Object element) {
            return ((Node) element).getName();
        }

        @Override
        public Image getImage(Object element) {
            Node node = (Node) element;
            if (node.getChildren().size() > 0) {
                return PlatformUI.getWorkbench().getSharedImages()
                        .getImage(ISharedImages.IMG_OBJ_FOLDER);
            }
            return PlatformUI.getWorkbench().getSharedImages()
            .getImage(ISharedImages.IMG_OBJ_FILE);
        }
    }
}
