/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.utils;

import org.eclipse.swt.widgets.Widget;

/**
 * 
 *
 * @author tobias
 * @created 31.08.2011
 * @version $Revision: 1.1 $
 */
public final class CompUtils {

    /** gd key for comp names */
    public static final String TEST_COMP_NAME_KEY = "TEST_COMP_NAME"; //$NON-NLS-1$

    /**
     * Constructor
     */
    private CompUtils() {
    // empty
    }

    /**
     * set component name at given widget, using the default gd key
     * 
     * @param anyWidget
     *            the widget to set a comp name for
     * @param compName
     *            the used comp name
     */
    public static void setComponentName(Widget anyWidget, String compName) {
        anyWidget.setData(TEST_COMP_NAME_KEY, compName);
    }
    
}
