/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2005/06/30 11:34:59 $
 *
 * $Author: tobias $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2011 
 * 
 */
package org.eclipse.jubula.qa.aut.caa.rp.jface.jface;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.MockModels;
import org.eclipse.jubula.qa.aut.caa.rp.jface.utils.MockModels.ComboViewerEntry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 * 
 * @author tobias
 * @created 05.09.2011
 * @version $Revision: 1.1 $
 */
public class ComboViewerOpener extends AbstractApplicationWindow {

    /**
     * @param parentShell
     *            parent Shell
     */
    public ComboViewerOpener(Shell parentShell) {
        super(parentShell);
    }

    /**
     * {@inheritDoc}
     */
    protected Composite makeContent(Composite parent) {
        GridLayout layout = new GridLayout(2, false);
        parent.setLayout(layout);
        ComboViewer viewer = new ComboViewer(parent, SWT.READ_ONLY);
        viewer.setContentProvider(new ArrayContentProvider());
        viewer.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                if (element instanceof ComboViewerEntry) {
                    ComboViewerEntry person = (ComboViewerEntry)element;
                    return person.getName();
                }
                return super.getText(element);
            }
        });

        viewer.setInput(MockModels.DEFAULT_COMBO_LIST_MODEL);

        return parent;
    }

    /**
     * {@inheritDoc}
     */
    protected String getWindowTitle() {
        return ComboViewer.class.getCanonicalName();
    }

}
