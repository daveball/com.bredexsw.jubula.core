package org.eclipse.jubula.qa.aut.caa.javafx.osgi;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.fx.osgi.util.AbstractJFXApplication;
import org.eclipse.jubula.qa.aut.caa.javafx.ApplicationClass;
import org.eclipse.jubula.qa.aut.caa.javafx.StartScene;

/**
 * Adds CaA Content to the stage
 * 
 * @author Bredex GmbH
 *
 */
public class MainApplication extends AbstractJFXApplication implements
        ApplicationClass {

    /** The Stage **/
    private static Stage primaryStage;

    /**
     * Adds CaA Content to the stage
     * 
     * @param applicationContext
     *            the application context
     * @param jfxApplication
     *            the application
     * @param pStage
     *            the stage
     */
    protected void jfxStart(IApplicationContext applicationContext,
            Application jfxApplication, Stage pStage) {
        primaryStage = pStage;
        primaryStage.setMaximized(true);
        StartScene.createInstance(this);
        Platform.setImplicitExit(false);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            public void handle(WindowEvent event) {
                Platform.exit();
            }
        });
    }

    @Override
    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
