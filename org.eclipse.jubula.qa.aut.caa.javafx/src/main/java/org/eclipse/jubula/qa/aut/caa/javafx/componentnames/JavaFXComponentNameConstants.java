package org.eclipse.jubula.qa.aut.caa.javafx.componentnames;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

/**
 * Component constants specialized for the JavaFX-CAA-AUT
 * 
 */
public interface JavaFXComponentNameConstants extends ComponentNameConstants {
    /** The component id for the menuBar in the menu Testpage */
    public static final String TESTPAGE_MENUS_MENUBAR = 
            "TestPage.Menus.MenuBar";
    /** The component id for the menu in the menu Testpage */
    public static final String TESTPAGE_MENUS_MENU01 = "TestPage.Menus.Menu01";
    /** The component id for the second menu in the menu Testpage */
    public static final String TESTPAGE_MENUS_MENU02 = "TestPage.Menus.Menu02";

    /** The component id for the menuBar in the menu Testpage */
    public static final String TESTPAGE_APPLICATION_MENUBAR =
            "TestPage.Application.MenuBar";

    /** The component id for the ChoiceBoxes Testpage */
    public static final String MAINPAGE_CHOICEBOXES_TESTPAGE = 
            "MainPage.ChoiceBoxes.Page";
    /** The component id for title ChoiceBoxes */
    public static final String TESTPAGE_TITLE_CHOICEBOXES = "ChoiceBoxes";
    /** The component id for the ChoiceBoxes01 */
    public static final String TESTPAGE_CHOICEBOXES_CHOIBOX01 = 
            "TestPage.ChoiceBoxes.ChoiceBox01";
    /** The component id for the ChoiceBoxes02 */
    public static final String TESTPAGE_CHOICEBOXES_CHOIBOX02 = 
            "TestPage.ChoiceBoxes.ChoiceBox02";
    /** The component id for the ChoiceBoxes03 */
    public static final String TESTPAGE_CHOICEBOXES_CHOIBOX03 = 
            "TestPage.ChoiceBoxes.ChoiceBox03";
    /** The component id for the ChoiceBoxes04 */
    public static final String TESTPAGE_CHOICEBOXES_CHOIBOX04 = 
            "TestPage.ChoiceBoxes.ChoiceBox04";
    /** The component id for the ChoiceBoxes05 */
    public static final String TESTPAGE_CHOICEBOXES_CHOIBOX05 = 
            "TestPage.ChoiceBoxes.ChoiceBox05";
    
    /** The component id for the ComboBoxes Testpage */
    public static final String MAINPAGE_COMBOBOXES_TESTPAGE = 
            "MainPage.ComboBoxes.Page";
    /** The component id for title ComboBoxes */
    public static final String TESTPAGE_TITLE_COMBOBOXES = "ComboBoxes";
    /** The component id for the ComboBoxes01 */
    public static final String TESTPAGE_COMBOBOXES_CBOX01 = 
            "TestPage.ComboBoxes.ComboBoxes01";
    /** The component id for the ComboBoxes02 */
    public static final String TESTPAGE_COMBOBOXES_CBOX02 = 
            "TestPage.ComboBoxes.ComboBoxes02";
    /** The component id for the ComboBoxes03 */
    public static final String TESTPAGE_COMBOBOXES_CBOX03 = 
            "TestPage.ComboBoxes.ComboBoxes03";
    /** The component id for the ComboBoxes04 */
    public static final String TESTPAGE_COMBOBOXES_CBOX04 = 
            "TestPage.ComboBoxes.ComboBoxes04";
    /** The component id for the ComboBoxes05 */
    public static final String TESTPAGE_COMBOBOXES_CBOX05 = 
            "TestPage.ComboBoxes.ComboBoxes05";

    /** The component id for the MultipleMenuBars Testpage */
    public static final String MAINPAGE_MULTIPLEMENUBARS_TESTPAGE = 
            "MainPage.MultipleMenus.Page";
    /** The component id for title MultipleMenuBars */
    public static final String TESTPAGE_TITLE_MULTIPLEMENUS = "MultipleMenus";
    /** The component id for the MenuBar01 */
    public static final String TESTPAGE_MULTIPLEMENUS_MENUBAR01 = 
            "TestPage.MultipleMenus.MenuBar01";
    /** The component id for the Menu01 in MenuBar01 */
    public static final String TESTPAGE_MULTIPLEMENUS_MENUBAR01_MENU01 = 
            "TestPage.MultipleMenus.MenuBar01.Menu01";
    /** The component id for the Menu02 in MenuBar01 */
    public static final String TESTPAGE_MULTIPLEMENUS_MENUBAR01_MENU02 = 
            "TestPage.MultipleMenus.MenuBar01.Menu02";
    /** The component id for the MenuBar02 */
    public static final String TESTPAGE_MULTIPLEMENUS_MENUBAR02 = 
            "TestPage.MultipleMenus.MenuBar02";
    /** The component id for the Menu01 in MenuBar02 */
    public static final String TESTPAGE_MULTIPLEMENUS_MENUBAR02_MENU01 = 
            "TestPage.MultipleMenus.MenuBar02.Menu01";
    /** The component id for the Menu02 in MenuBar02 */
    public static final String TESTPAGE_MULTIPLEMENUS_MENUBAR02_MENU02 = 
            "TestPage.MultipleMenus.MenuBar02.Menu02";

    /** The component id for the ScrollBars Testpage */
    public static final String MAINPAGE_SCROLLBARS_TESTPAGE = 
            "MainPage.ScrollBars.Page";
    /** The component id for title ScrollBars*/
    public static final String TESTPAGE_TITLE_SCROLLBARS = "ScrollBars";
    /** The component id for the ScrollBar01 */
    public static final String TESTPAGE_SCROLLBARS_SCRBAR01 = 
            "TestPage.ScrollBars.ScrollBar01";
    /** The component id for the ScrollBar02 */
    public static final String TESTPAGE_SCROLLBARS_SCRBAR02 = 
            "TestPage.ScrollBars.ScrollBar02";
    /** The component id for the ScrollBar03 */
    public static final String TESTPAGE_SCROLLBARS_SCRBAR03 = 
            "TestPage.ScrollBars.ScrollBar03";
    /** The component id for the ScrollBar04 */
    public static final String TESTPAGE_SCROLLBARS_SCRBAR04 = 
            "TestPage.ScrollBars.ScrollBar04";
    /** The component id for the ScrollBar05 */
    public static final String TESTPAGE_SCROLLBARS_SCRBAR05 = 
            "TestPage.ScrollBars.ScrollBar05";

    /** The component id for the ScrollPanes Testpage */
    public static final String MAINPAGE_SCROLLPANES_TESTPAGE = 
            "MainPage.ScrollPanes.Page";
    /** The component id for title ScrollPanes */
    public static final String TESTPAGE_TITLE_SCROLLPANES = "ScrollPanes";
    /** The component id for the ScrollPane01 */
    public static final String TESTPAGE_SCROLLPANES_SCRP01 = 
            "TestPage.ScrollPanes.ScrollPane01";
    /** The component id for the ScrollPane02 */
    public static final String TESTPAGE_SCROLLPANES_SCRP02 = 
            "TestPage.ScrollPanes.ScrollPane02";
    /** The component id for the ScrollPane03 */
    public static final String TESTPAGE_SCROLLPANES_SCRP03 = 
            "TestPage.ScrollPanes.ScrollPane03";
    /** The component id for the ScrollPane04 */
    public static final String TESTPAGE_SCROLLPANES_SCRP04 = 
            "TestPage.ScrollPanes.ScrollPane04";

    /** The component id for the ToggleButtons Testpage */
    public static final String MAINPAGE_TOGGLEBUTTONS_TESTPAGE = 
            "MainPage.ToggleButtons.Page";
    /** The component id for title ToggleButtons */
    public static final String TESTPAGE_TITLE_TOGGLEBUTTONS = "ToggleButtons";
    /** The component id for the ToggleButton01 */
    public static final String TESTPAGE_TOGGLEBUTTONS_TOGLBUT01 = 
            "TestPage.ToggleButtons.ToggleButton01";
    /** The component id for the ToggleButton02 */
    public static final String TESTPAGE_TOGGLEBUTTONS_TOGLBUT02 = 
            "TestPage.ToggleButtons.ToggleButton02";
    /** The component id for the ToggleButton03 */
    public static final String TESTPAGE_TOGGLEBUTTONS_TOGLBUT03 = 
            "TestPage.ToggleButtons.ToggleButton03";
    /** The component id for the ToggleButton04 */
    public static final String TESTPAGE_TOGGLEBUTTONS_TOGLBUT04 = 
            "TestPage.ToggleButtons.ToggleButton04";
    
    /** The component id for the Texts Testpage */
    public static final String MAINPAGE_TEXTS_TESTPAGE = 
            "MainPage.Texts.Page";
    /** The component id for title Texts */
    public static final String TESTPAGE_TITLE_TEXTS = "Texts";
    /** The component id for Text01 */
    public static final String TESTPAGE_TEXT_TXT01 = "TestPage.Texts.Text01";
    /** The component id for Text02 */
    public static final String TESTPAGE_TEXT_TXT02 = "TestPage.Texts.Text02";
    /** The component id for Text03 */
    public static final String TESTPAGE_TEXT_TXT03 = "TestPage.Texts.Text03";
    
    /** The component id for TreeTable03 */
    public static final String TESTPAGE_TREETABLES_TRT03 = 
            "TestPage.TreeTables.TreeTable03";
    /** The component id for TreeTable04 */
    public static final String TESTPAGE_TREETABLES_TRT04 = 
            "TestPage.TreeTables.TreeTable04";
    
    /** The component id for Table 7 with nested columns */
    public static final String TESTPAGE_TABLES_TBL07 = 
            "TestPage.Tables.Table07";
    
    /** Testpage id **/
    public static final String TESTPAGE_BUTTONS_TRANSFORMED = "Transformed";
    /** component id **/
    public static final String TESTPAGE_BUTTONS_TRANSFORMED_BTN01 = 
            "TestPage.Transformed.Button01";
    /** component id **/
    public static final String TESTPAGE_BUTTONS_TRANSFORMED_BTN02 = 
            "TestPage.Transformed.Button02";
    /** component id **/
    public static final String TESTPAGE_BUTTONS_TRANSFORMED_BTN03 = 
            "TestPage.Transformed.Button03";
    /** component id **/
    public static final String TESTPAGE_BUTTONS_TRANSFORMED_BTN04 = 
            "TestPage.Transformed.Button04";
    /** main page button id **/
    public static final String MAINPAGE_TRANSFORMED_BUTTONS_TESTPAGE = 
            "MainPage.Transformed.Page";
    /** menu button 01 **/
    public static final String TESTPAGE_MENUBUTTONS_BTN01 = 
            "TestPage.MenuButtons.MenuButton01";
    /** menu button 02 **/
    public static final String TESTPAGE_MENUBUTTONS_BTN02 = 
            "TestPage.MenuButtons.MenuButton02";
    /** main page menu buttons**/
    public static final String MAINPAGE_MENUBUTTON_TESTPAGE = 
            "MainPage.MenuButton.Page";
    /** Testpage id**/
    public static final String TESTPAGE_TITLE_MENUBUTTONS = "MenuButtons";
    /** Testpage id**/
    public static final String TESTPAGE_TITLE_CUSTOMLISTVIEW = 
            "CustomLists";
    /** List id**/
    public static final String TESTPAGE_CUSTOMLISTS_LST01 = 
            "TestPage.CustomLists.List01";
    /** List id**/
    public static final String TESTPAGE_CUSTOMLISTS_LST02 = 
            "TestPage.CustomLists.List02";
    /** List id**/
    public static final String TESTPAGE_CUSTOMLISTS_LST03 = 
            "TestPage.CustomLists.List03";
    /** MainPage id**/
    public static final String MAINPAGE_CUSTOMLISTS_TESTPAGE = 
            "MainPage.CustomLists.Page";
    /** MainPage id**/
    public static final String MAINPAGE_STYLEDCONTENT_TESTPAGE = 
            "MainPage.StyledContent.Page";

    /** main page button id **/
    public static final String MAINPAGE_TRANSFORMED_WIDGETS_TESTPAGE = 
            "MainPage.TransformedWidgets.Page";
    /** List id**/
    public static final String TESTPAGE_TRANSFORMED_WIDGETS_LIST = 
            "TestPage.Transformed.Widgets.List01";
    /** List id**/
    public static final String TESTPAGE_TRANSFORMED_WIDGETS_TABLE = 
            "TestPage.Transformed.Widgets.TABLE01";

}