package org.eclipse.jubula.qa.aut.caa;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.eclipse.jubula.qa.aut.caa.javafx.ApplicationClass;
import org.eclipse.jubula.qa.aut.caa.javafx.StartScene;

/**
 * Mainclass to start the JavaFX AUT.
 * 
 */
public class StartJavaFXAUT extends Application implements ApplicationClass {

    /**
     * The Stage on which every scene will be displayed
     */
    private static Stage primaryStage;

    /**
     * 
     * @param pStage
     *            - the primary stage for this application, onto which the
     *            application scene can be set. The primary stage will be
     *            embedded in the browser if the application was launched as an
     *            applet. Applications may create other stages, if needed, but
     *            they will not be primary stages and will not be embedded in
     *            the browser.
     */
    @Override
    public void start(Stage pStage) throws Exception {
        primaryStage = pStage;
        primaryStage.setMaximized(true);
        StartScene.createInstance(this);
        Platform.setImplicitExit(false);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            
            public void handle(WindowEvent event) {
                Platform.exit();
            }
        });
    }

    /**
     * Main-method
     * 
     * @param args
     *            - arguments
     */
    public static void main(String[] args) {
        launch(args);       
    }

    @Override
    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
