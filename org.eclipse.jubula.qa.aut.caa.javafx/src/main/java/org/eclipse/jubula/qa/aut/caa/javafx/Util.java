package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.HashMap;

import javafx.event.EventHandler;
import javafx.scene.control.IndexedCell;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

/**
 * Util class for test panes
 */
public class Util {
    
    /**
     * Constructor
     */
    private Util() {
        // private
    }
    
    /**
     * Adds the D&D handler to a given Cell
     * 
     * @param cell
     *            the cell
     * @param tPane
     *            for convenience
     */
    public static void addDragHandlerToCell(final IndexedCell<?> cell,
            final TestPane tPane) {

        cell.setOnDragDetected(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                Dragboard db = cell.startDragAndDrop(
                        TransferMode.COPY_OR_MOVE);
                HashMap<DataFormat, Object> map =
                        new HashMap<DataFormat, Object>();
                map.put(DataFormat.PLAIN_TEXT, 
                        cell.getItem() + " [index=" + cell.getIndex() + "]\n");
                db.setContent(map);
            }

        });

        cell.setOnDragEntered(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        cell.setOnDragExited(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });

        cell.setOnDragOver(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                if (event.getDragboard().hasString()) {
                    event.acceptTransferModes(
                            TransferMode.COPY_OR_MOVE);
                }
            }
        });
        
        cell.setOnDragDropped(new EventHandler<DragEvent>() {

            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                String text = (String) db.getContent(DataFormat.PLAIN_TEXT);
                tPane.setActionText("Drag source: " + text + " Drop target: "
                        + cell.getItem() + " [index=" + cell.getIndex() + "]");
                event.setDropCompleted(true);
            }
        });
    }
}
