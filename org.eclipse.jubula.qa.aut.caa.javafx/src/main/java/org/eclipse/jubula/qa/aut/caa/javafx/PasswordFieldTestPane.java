package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.PasswordField;

import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;
/**
 * Initializes PasswordFields for testing.
 */
public class PasswordFieldTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_passwordField");
        
        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_PASSWORDFIELDS);
        //PasswordField1
        PasswordField pf1 = new PasswordField();
        pf1.setId(JavaFXComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        tPane.addContextMenuToNode(pf1);
        
      //PasswordField2
        PasswordField pf2 = new PasswordField();
        pf2.setId(JavaFXComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        pf2.setDisable(true);
        
      //PasswordField3
        PasswordField pf3 = new PasswordField();
        pf3.setId(JavaFXComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        pf3.setText("ababb");
        
      //PasswordField4
        PasswordField pf4 = new PasswordField();
        pf4.setId(JavaFXComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        pf4.setText("ababb");
        pf4.setDisable(true);
        
      //PasswordField5
        PasswordField pf5 = new PasswordField();
        pf5.setId(JavaFXComponentNameConstants.TESTPAGE_PASSWORDFIELDS_PWDF01);
        pf5.setEditable(false);
        
        tPane.addControls(pf1, pf2, pf3, pf4, pf5);
        tPane.addControlsToEventHandler(pf1, pf2, pf3, pf4, pf5);
  
        return tPane;
    }

}
