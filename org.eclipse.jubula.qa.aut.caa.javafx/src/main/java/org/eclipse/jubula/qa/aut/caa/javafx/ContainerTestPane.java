package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

/**
 * Initializes Buttons for testing.
 */
public class ContainerTestPane extends AbstractSceneSetter {

    /** The testPane */
    private TestPane m_testPane;

    @Override
    protected TestPane getTestScene() {
        m_testPane = new TestPane("title_containers");
        m_testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_CONTAINERS);

        GridPane grid = new GridPane();
        m_testPane.addNode(grid);

        SplitPane splitPane = new SplitPane();
        fillSplitPane(splitPane);
        TitledPane splitPanePane = new TitledPane(splitPane.getClass()
                .getName(), splitPane);
        setId(splitPanePane);
        grid.add(splitPanePane, 0, 0);

        TitledPane titledPanePane = new TitledPane(TitledPane.class.getName(),
                createTitledPane());
        setId(titledPanePane);
        grid.add(titledPanePane, 0, 1);

        ToolBar toolBar = createToolBar();
        TitledPane toolBarPane = new TitledPane(toolBar.getClass().getName(),
                toolBar);
        setId(toolBarPane);
        grid.add(toolBarPane, 0, 2);

        return m_testPane;
    }

    /**
     * generates an id for a pane
     * @param pane the pane
     */
    private void setId(TitledPane pane) {
        pane.setId(ComponentNameConstants.TESTPAGE_TITLE_CONTAINERS + "."
                + pane.getContent().getClass().getSimpleName() + ".Container");
    }

    /**
     * Creates a ToolBar
     * 
     * @return the ToolBar
     */
    private ToolBar createToolBar() {
        ToolBar toolBar = new ToolBar();
        for (int id = 0; id < 3; id++) {
            String name = "Button" + id;
            Button btn = new Button(name);
            String paneType = ToolBar.class.getSimpleName();
            btn.setId(ComponentNameConstants.TESTPAGE_CONTAINERS + "."
                    + paneType + "." + name);
            m_testPane.addControlsToEventHandler(btn);
            toolBar.getItems().add(btn);
        }
        return toolBar;
    }

    /**
     * Creates a TitledPane
     * 
     * @return the TitledPane
     */
    private TitledPane createTitledPane() {
        VBox vbox = new VBox();
        String paneType = TitledPane.class.getSimpleName();
        for (int id = 0; id < 3; id++) {
            String name = "Button" + id;
            Button btn = new Button(name);
            btn.setId(ComponentNameConstants.TESTPAGE_CONTAINERS + "."
                    + paneType + "." + name);
            vbox.setPrefWidth(200);
            vbox.setPrefHeight(100);
            vbox.getChildren().add(btn);
            m_testPane.addControlsToEventHandler(btn);
        }
        TitledPane titledPane = new TitledPane(
                "This is a TitledPane containing a VBox", vbox);
        titledPane.setId(ComponentNameConstants.TESTPAGE_CONTAINERS + "."
                + paneType);
        return titledPane;
    }

    /**
     * Fills a SplitPane with content
     * 
     * @param splitPane
     *            the SplitPane
     */
    private void fillSplitPane(SplitPane splitPane) {
        for (int id = 0; id < 3; id++) {
            String name = "Button" + id;
            Button btn = new Button(name);
            String paneType = splitPane.getClass().getSimpleName();
            btn.setId(ComponentNameConstants.TESTPAGE_CONTAINERS + "."
                    + paneType + "." + name);
            final StackPane stackPane = new StackPane();
            stackPane.setPrefWidth(200);
            stackPane.setPrefHeight(100);
            stackPane.getChildren().add(btn);
            splitPane.getItems().add(stackPane);
            m_testPane.addControlsToEventHandler(btn);
        }
        splitPane.setDividerPositions(0.33f, 0.66f);
    }

}
