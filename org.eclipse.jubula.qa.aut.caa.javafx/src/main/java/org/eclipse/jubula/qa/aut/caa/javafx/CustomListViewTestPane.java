package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes ListViews for testing.
 */
public class CustomListViewTestPane extends AbstractSceneSetter {
    /** The testPane */
    private TestPane m_testPane;

    @Override
    protected TestPane getTestScene() {
        m_testPane = new TestPane("title_customLists");
        m_testPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_CUSTOMLISTVIEW);

        ListView<String> listView = new ListView<String>(createListModel());
        listView.setId(JavaFXComponentNameConstants.TESTPAGE_CUSTOMLISTS_LST01);
        m_testPane.addControls(listView);
        m_testPane.addContextMenuToNode(listView);
        listView.setCellFactory(new Callback<ListView<String>,
                ListCell<String>>() {
            
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            Button b = new Button(item + " Button");
                            this.setText(item);
                            this.setGraphic(b);
                        }
                    }
                };
            }
        });
        
        ListView<String> disabledListView = new ListView<String>(
            createListModel());
        disabledListView.setDisable(true);
        disabledListView.setId(JavaFXComponentNameConstants.
                TESTPAGE_CUSTOMLISTS_LST02);
        m_testPane.addControls(disabledListView);
        
        ListView<String> largeListView = new ListView<String>(
            createLargeListModel());
        largeListView.setId(JavaFXComponentNameConstants.
                TESTPAGE_CUSTOMLISTS_LST03);
        largeListView.getSelectionModel().setSelectionMode(
            SelectionMode.MULTIPLE);
        largeListView.setPrefHeight(205);
        largeListView.setCellFactory(new Callback<ListView<String>,
                ListCell<String>>() {
            
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            Button b = new Button(item);
                            b.setId(b.getClass().getCanonicalName() + item);
                            this.setText(item);
                            this.setGraphic(new VBox(new Label("label"), b));
                        }
                    }
                };
            }
        });
        m_testPane.addControls(largeListView);
        
        m_testPane.addControlsToEventHandler(listView, largeListView);
        
        return m_testPane;
    }

    /**
     * @return the default list model
     */
    private ObservableList<String> createListModel() {
        return FXCollections.observableArrayList(
            I18NUtils.getString("text1"),
            I18NUtils.getString("text2"),
            I18NUtils.getString("text3"),
            I18NUtils.getString("text4"),
            I18NUtils.getString("text5"),
            I18NUtils.getString("text6"),
            I18NUtils.getString("text7"));
    }
    
    /**
     * @return the default list model
     */
    private ObservableList<String> createLargeListModel() {
        final int listLength = 200;
        List<String> longList = new ArrayList<String>(listLength);
        for (int i = 0; i <= listLength; i++) {
            longList.add(String.valueOf(i));
        }

        return FXCollections.observableArrayList(longList);
    }
}