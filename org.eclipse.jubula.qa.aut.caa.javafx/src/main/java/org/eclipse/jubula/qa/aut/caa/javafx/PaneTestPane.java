package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

/**
 * Initializes Buttons for testing.
 */
public class PaneTestPane extends AbstractSceneSetter {

    /** The testPane */
    private TestPane m_testPane;

    @Override
    protected TestPane getTestScene() {
        m_testPane = new TestPane("title_panes");
        m_testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_PANES);
        
        GridPane grid = new GridPane();

        AnchorPane anchorPane = new AnchorPane();
        generateContent(anchorPane, 3);
        TitledPane anchorPanePane = new TitledPane(
                anchorPane.getClass().getName(), anchorPane);
        setId(anchorPanePane);
        grid.add(anchorPanePane, 0, 0);

        BorderPane borderPane = new BorderPane();
        generateContent(borderPane, 3);
        TitledPane borderPanePane = new TitledPane(
                borderPane.getClass().getName(), borderPane);
        setId(borderPanePane);
        grid.add(borderPanePane, 1, 0);

        FlowPane flowPane = new FlowPane();
        generateContent(flowPane, 3);
        TitledPane flowPanePane = new TitledPane(
                flowPane.getClass().getName(), flowPane);
        setId(flowPanePane);
        grid.add(flowPanePane, 2, 0);

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10));
        generateContent(gridPane, 3);
        TitledPane gridPanePane = new TitledPane(
                gridPane.getClass().getName(), gridPane);
        setId(gridPanePane);
        grid.add(gridPanePane, 0, 1);

        HBox hbox = new HBox();
        generateContent(hbox, 3);
        TitledPane hboxPane = new TitledPane(
                hbox.getClass().getName(), hbox);
        setId(hboxPane);
        grid.add(hboxPane, 1, 1);
        
        Pane pane = new Pane();
        generateContent(pane, 3);
        TitledPane panePane = new TitledPane(
                pane.getClass().getName(), pane);
        setId(panePane);
        grid.add(panePane, 2, 1);

        StackPane stackPane = new StackPane();
        stackPane.setAlignment(Pos.BASELINE_LEFT);
        generateContent(stackPane, 3);
        TitledPane stackPanePane = new TitledPane(
                stackPane.getClass().getName(), stackPane);
        setId(stackPanePane);
        grid.add(stackPanePane, 0, 2);

        TilePane tilePane = new TilePane();
        generateContent(tilePane, 3);
        TitledPane tilePanePane = new TitledPane(
                tilePane.getClass().getName(), tilePane);
        setId(tilePanePane);
        grid.add(tilePanePane, 1, 2);

        VBox vbox = new VBox();
        generateContent(vbox, 3);
        TitledPane vboxPane = new TitledPane(
                vbox.getClass().getName(), vbox);
        setId(vboxPane);
        grid.add(vboxPane, 2, 2);
        
        m_testPane.addNode(grid);

        return m_testPane;
    }
    
    /**
     * generates an id for a pane
     * @param pane the pane
     */
    private void setId(TitledPane pane) {
        pane.setId(ComponentNameConstants.TESTPAGE_TITLE_CONTAINERS + "."
                + pane.getContent().getClass().getSimpleName() + ".Container");
    }

    /**
     * Adds buttons to a pane
     * 
     * @param pane
     *            the pane
     * @param buttonCount
     *            the amount of buttons to add
     */
    private void generateContent(Pane pane, int buttonCount) {
        for (int i = 0; i < buttonCount; i++) {
            addButton(pane, i);
        }
        pane.setStyle("-fx-border-color: black;");
        pane.setMinSize(300, 140);
    }

    /**
     * Adds a button to a pane
     * 
     * @param pane
     *            the pane
     * @param id
     *            the id of the button
     */
    private void addButton(Pane pane, int id) {
        String name = "Button" + id;
        Button btn = new Button(name);
        String paneType = pane.getClass().getSimpleName();
        btn.setId(ComponentNameConstants.TESTPAGE_PANES + "." + paneType + "."
                + name);

        if (m_testPane != null) {
            m_testPane.addControlsToEventHandler(btn);

            // do all the necessary layouting
            switch (paneType) {
                case "AnchorPane":
                    placeInAnchorPane(id, btn);
                    break;
                case "BorderPane":
                    placeInBorderPane((BorderPane)pane, id, btn);
                    return;
                case "GridPane":
                    placeInGridPane((GridPane)pane, id, btn);
                    return;
                case "Pane":
                    placeInPane(pane, id, btn);
                case "StackPane":
                    placeInStackPane(id, btn);
                    break;
                default:
                    break;
            }
            pane.getChildren().add(btn);
        }
    }

    /**
     * Places a button in a Pane
     * @param pane the pane
     * @param id the id of the button
     * @param btn the button
     */
    private void placeInPane(Pane pane, int id, Button btn) {
        int height = (3 - id) * 25;
        int width = (3 - id) * 80;
        btn.setMinHeight(height);
        btn.setMaxHeight(height);
        btn.setMaxWidth(width);
        btn.setMinWidth(width);
        btn.relocate(40 * id, 40 * id);
    }

    /**
     * Places a button in a StackPane
     * @param id the id of the button
     * @param btn the button
     */
    private void placeInStackPane(int id, Button btn) {
        btn.setPrefWidth(100 * (3 - id));
        btn.setPrefHeight(30 * (3 - id));
    }

    /**
     * Places a button in a BorderPane
     * @param borderPane the BorderPane
     * @param id the id of the button
     * @param btn the button
     */
    private void placeInBorderPane(BorderPane borderPane, int id, Button btn) {
        switch (id) {
            case 0:
                borderPane.setLeft(btn);
                break;
            case 1:
                borderPane.setCenter(btn);
                break;
            case 2:
                borderPane.setRight(btn);
                break;
            default:
                break;
        }
        
    }

    /**
     * Places a button in a GridPane
     * @param gridPane the GridPane
     * @param id the id of the button
     * @param btn the button
     */
    private void placeInGridPane(GridPane gridPane, int id, Button btn) {
        gridPane.add(btn, id % 2, (id / 2) % 2);
    }

    /**
     * Places a button in an AnchorPane
     * @param id the id of the button
     * @param btn the button
     */
    private void placeInAnchorPane(int id, Button btn) {
        switch (id) {
            case 0:
                AnchorPane.setTopAnchor(btn, 0.0);
                AnchorPane.setRightAnchor(btn, 0.0);
                break;
            case 1:
                AnchorPane.setBottomAnchor(btn, 0.0);
                AnchorPane.setLeftAnchor(btn, 0.0);
                break;
            case 2:
                AnchorPane.setBottomAnchor(btn, 0.0);
                AnchorPane.setRightAnchor(btn, 0.0);
                break;
            default:
                break;
        }
    }

}
