package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;

/**
 * Creates Charts for use in CaA tests.
 * 
 */
public class ChartTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane parentPane = new TestPane("title_charts");
        parentPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_CHARTS);

        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                new PieChart.Data("Red", 16),
                new PieChart.Data("Orange", 25),
                new PieChart.Data("Green", 22),
                new PieChart.Data("Blue", 20),
                new PieChart.Data("Violet", 17));
        final PieChart pieChart = new PieChart(pieChartData);
        pieChart.setTitle("Colors in a PieChart");
        
        parentPane.addNode(pieChart);

        return parentPane;
    }

}
