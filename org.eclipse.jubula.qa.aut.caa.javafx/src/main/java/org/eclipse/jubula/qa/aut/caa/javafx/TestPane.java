package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * This class initializes the base frame, which is equal for every component
 * test class. It also serves as a container for the component test class.
 */
public class TestPane {

    /** Title for the test scene */
    private String m_sceneTitle;

    /** Pane for the components that are going to be tested */
    private FlowPane m_flowPane;

    /** The root Pane */
    private BorderPane m_rootPane;

    /** Text for showing messages that EventHandlers throw */
    private Label m_actionText;

    /**
     * Constructor
     * 
     * @param title
     *            the title for the test scene
     */
    public TestPane(String title) {
        m_sceneTitle = I18NUtils.getString(title);
        init();
    }

    /**
     * Initializes the base frame for test scenes
     */
    private void init() {
        m_rootPane = new BorderPane();
        // m_rootPane.setPadding(new Insets(20, 0, 20, 0));

        m_flowPane = new FlowPane(Orientation.VERTICAL);
        m_flowPane.setPadding(new Insets(10, 10, 10, 10));
        m_flowPane.setAlignment(Pos.TOP_CENTER);
        m_flowPane.setVgap(10);
        m_flowPane.setHgap(10);
        m_flowPane.setId(m_sceneTitle + I18NUtils.getName("cp"));

        m_rootPane.setCenter(m_flowPane);
        BorderPane.setAlignment(m_flowPane, Pos.CENTER);

        Button closeButton = new Button(I18NUtils.getString("closeBtn"));
        closeButton.setId(ComponentNameConstants.TESTPAGES_CLOSEBUTTON);
        closeButton.setOnAction(new EventHandler<ActionEvent>() {

            
            public void handle(ActionEvent arg0) {
                StartScene.getInstance().showStartScene();

            }

        });

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20, 0, 20, 0));
        vBox.setAlignment(Pos.CENTER);

        m_actionText = new Label();
        m_actionText.setText(I18NUtils.getString("lblBeforeAction"));
        m_actionText.setId(ComponentNameConstants.TESTPAGES_CONTENTLABEL);

        vBox.getChildren().addAll(m_actionText, closeButton);

        m_rootPane.setBottom(vBox);
        BorderPane.setAlignment(vBox, Pos.BOTTOM_CENTER);
    }

    /**
     * Adds controls for testing
     * 
     * @param c
     *            controls for testing
     */
    public void addControls(Control... c) {
        m_flowPane.getChildren().addAll(c);
    }
    
    /**
     * Adds nodes for testing
     * 
     * @param n
     *            nodes for testing
     */
    public void addNode(Node... n) {
        m_flowPane.getChildren().addAll(n);
    }

    /**
     * Adds an EventHandler to the given controls, for displaying event messages
     * 
     * @param c
     *            controls for handler
     */
    public void addControlsToEventHandler(Control... c) {
        for (Control control : c) {
            control.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    m_actionText.setText(event.getSource() 
                            + " on X:" +  event.getScreenX() + ", Y:"
                            +  event.getScreenY());
                }
            });
        }
    }
    
    /**
     * Adds an EventHandler to the given nodes, for displaying event messages
     * 
     * @param n
     *            nodes for handler
     */
    public void addNodesToEventHandler(Node... n) {
        for (Node node : n) {

            node.setOnMouseClicked(
                    new EventHandler<Event>() {
                        
                        public void handle(Event event) {
                            EventTarget target = event.getTarget();
                            m_actionText.setText(target.toString());
                            if (target instanceof Node) {
                                ((Node)target).requestFocus();
                            }
                        }
                    });
        }
    }
    
    /**
     * Adds a context menu to a control.
     * @param c the control
     */
    public void addContextMenuToNode(Control c) {
        List<MenuItem> items = MenuBarTestPane.createContextMenu(this);
        MenuItem[] ar = new MenuItem[items.size()];
        items.toArray(ar);
        c.setContextMenu(new ContextMenu(ar));
    }

    /**
     * Special method for adding a MenuBar. Otherwise the MenuBar wouldn't be
     * displayed on top
     * 
     * @param b
     *            the MenuBar
     */
    public void addMenuBar(MenuBar b) {
        m_rootPane.setTop(b);
    }

    /**
     * Returns the scene title
     * 
     * @return the scene title
     */
    public String getSceneTitle() {
        return m_sceneTitle;
    }

    /**
     * Returns the root pane
     * 
     * @return the root pane
     */
    public Pane getRootPane() {
        return m_rootPane;
    }

    /**
     * Sets the text on the Label
     * @param text the Text
     */
    public void setActionText(String text) {
        m_actionText.setText(text);
    }

    /**
     * Sets the id of the root pane to the given id
     * 
     * @param id
     *            the new id for the root pane
     */
    public void setRootId(String id) {
        m_rootPane.setId(id);
    }

    /**
     * Special method for adding text. Because Text isn't a control
     * 
     * @param txt
     *            the text elements
     */
    public void addTexts(Text... txt) {
        m_flowPane.getChildren().addAll(txt);
    }

}
