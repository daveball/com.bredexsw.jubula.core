package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.stage.Stage;

/**
 * Interface to a allow the use of the caa code in different projects with
 * different mechanisms to start, e.g. the osgi caa project.
 *
 */
public interface ApplicationClass {

    /**
     * Returns the primary Stage
     * 
     * @return the primary Stage
     */
    public Stage getPrimaryStage();

}
