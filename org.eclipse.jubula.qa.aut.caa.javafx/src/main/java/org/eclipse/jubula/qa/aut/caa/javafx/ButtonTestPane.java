package org.eclipse.jubula.qa.aut.caa.javafx;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.constants.ComponentNameConstants;
import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Initializes Buttons for testing.
 */
public class ButtonTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane testPane = new TestPane("title_buttons");
        testPane.setRootId(ComponentNameConstants.TESTPAGE_TITLE_BUTTONS);
        // button 1
        Button btn1 = new Button(I18NUtils.getName("btn1"));
        btn1.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN01);
        btn1.setTooltip(new Tooltip((I18NUtils.getString("tooltip"))));
        btn1.setMaxWidth(Double.MAX_VALUE);
        testPane.addContextMenuToNode(btn1);
        Button graphicButton = new Button("Graphic Button");
        graphicButton.setId(" ");
        Button butception = new Button("Graphic Button of Graphic Button");
        graphicButton.setGraphic(butception);
        btn1.setGraphic(graphicButton);
        btn1.setAlignment(Pos.CENTER_LEFT);
        graphicButton.setMaxWidth(120);
        butception.setMaxWidth(60);
        butception.setId("GraphicNode.GraphicNode.Button");
        // button 2
        Button btn2 = new Button(I18NUtils.getName("btn2"));
        btn2.setId(ComponentNameConstants.TESTPAGE_BUTTONS_BTN02);
        btn2.setDisable(true);
        btn2.setMaxWidth(Double.MAX_VALUE);

        testPane.addControls(btn1, btn2);
        testPane.addControlsToEventHandler(btn1, btn2, graphicButton,
                butception);

        return testPane;
    }

}
