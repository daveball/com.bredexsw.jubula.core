package org.eclipse.jubula.qa.aut.caa.javafx;

import java.util.Arrays;

import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;

import org.eclipse.jubula.qa.aut.caa.base.utils.ComponentUtils;
import org.eclipse.jubula.qa.aut.caa.javafx.componentnames.JavaFXComponentNameConstants;

/**
 * Initializes ChoiceBoxes for testing.
 */
public class ChoiceBoxTestPane extends AbstractSceneSetter {

    @Override
    protected TestPane getTestScene() {
        TestPane tPane = new TestPane("title_choiceBoxes");
        tPane.setRootId(JavaFXComponentNameConstants.
                TESTPAGE_TITLE_CHOICEBOXES);
        
        // ChoiceBox 1
        ChoiceBox<String> cb1 = new ChoiceBox<String>();
        cb1.setId(JavaFXComponentNameConstants.TESTPAGE_CHOICEBOXES_CHOIBOX01);
        tPane.addContextMenuToNode(cb1);
        cb1.setPadding(new Insets(5));
        
        // ChoiceBox 2
        ChoiceBox<String> cb2 = new ChoiceBox<String>();
        cb2.setId(JavaFXComponentNameConstants.TESTPAGE_CHOICEBOXES_CHOIBOX02);
        cb2.getItems().addAll(ComponentUtils.getListItems());
        cb2.setPadding(new Insets(5));

        // ChoiceBox 3
        ChoiceBox<String> cb3 = new ChoiceBox<String>();
        cb3.setId(JavaFXComponentNameConstants.TESTPAGE_CHOICEBOXES_CHOIBOX03);
        cb3.setDisable(true);
        cb3.setPadding(new Insets(5));

        // ChoiceBox 4
        ChoiceBox<Object> cb4 = new ChoiceBox<Object>();
        cb4.setId(JavaFXComponentNameConstants.TESTPAGE_CHOICEBOXES_CHOIBOX04);
        cb4.getItems().addAll(Arrays.asList(ComponentUtils.getListItems()));
        cb4.getItems().add(1, new Separator());
        cb4.setPadding(new Insets(5));
        
        // ChoiceBox 5
        ChoiceBox<String> cb5 = new ChoiceBox<String>();
        cb5.setId(JavaFXComponentNameConstants.TESTPAGE_CHOICEBOXES_CHOIBOX05);
        cb5.setTooltip(new Tooltip("Tooltip"));
        cb5.setPadding(new Insets(5));
        tPane.addControls(cb1, cb2, cb3, cb4, cb5);

        return tPane;
    }
}
