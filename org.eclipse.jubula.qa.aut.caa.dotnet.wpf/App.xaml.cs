﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string contextText = "";

        static void click(object sender, RoutedEventArgs e)
        {
            var clickedItem = sender as MenuItem;
            contextText = clickedItem.Header.ToString();            
        }        

        public static ContextMenu getContextMenu()
        {
            contextText = "";
            ContextMenu menu = new ContextMenu();
            MenuItem m1, m2, m3, m4, m5, m6, m7, m8, m9, m10;

            m1 = new MenuItem();
            m1.Header = "FirstItem";
            m1.Click += new RoutedEventHandler(click);

            m2 = new MenuItem();
            m2.Header = "More";           

            m3 = new MenuItem();
            m3.Header = "SecondItem";
            m3.Click += new RoutedEventHandler(click);

            m4 = new MenuItem();
            m4.Header = "CheckboxItem";
            m4.Click += new RoutedEventHandler(click);
            m4.IsCheckable = true;

            m5 = new MenuItem();
            m5.Header = "RadioBoxItem";
            m5.Click += new RoutedEventHandler(click);
            m5.IsCheckable = true;

            m6 = new MenuItem();
            m6.Header = "ThirdItem";
            m6.IsEnabled = false;

            m7 = new MenuItem();
            m7.Header = "More";

            m8 = new MenuItem();
            m8.Header = "Selectable Level 3";
            m8.Click += new RoutedEventHandler(click);

            m9 = new MenuItem();
            m9.Header = "More";

            m10 = new MenuItem();
            m10.Header = "Selectable Level 4";
            m10.Click += new RoutedEventHandler(click);

            m2.Items.Add(m3);
            m2.Items.Add(m4);
            m2.Items.Add(m5);
            m2.Items.Add(m6);
            m2.Items.Add(m7);
            m7.Items.Add(m8);
            m7.Items.Add(m9);
            m9.Items.Add(m10);
            menu.Items.Add(m1);
            menu.Items.Add(m2);

            return menu;
        }        
    }
}
