﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for ListTestPage.xaml
    /// </summary>
    public partial class ListTestPage : Window
    {
        public ListTestPage()
        {
            InitializeComponent();
            this.List1.ContextMenu = App.getContextMenu();
            List1.AllowDrop = true;
            List1.MouseMove += new MouseEventHandler(list1_MouseMove);
            List1.Drop += new DragEventHandler(list1_Drop);


            List5.AddHandler(UIElement.MouseDownEvent,
                    new MouseButtonEventHandler(List5_MouseDown), true);
        }

        void list1_Drop(object sender, DragEventArgs e)
        {
            Point pos = e.GetPosition(List1);
            IInputElement dropTargetItem = List1.InputHitTest(pos);
            TextBlock itemAsText = dropTargetItem as TextBlock;
            ListtextBlock.Text = "From: " + (string)e.Data.GetData(DataFormats.StringFormat) + " to " + itemAsText.Text;
        }

        void list1_MouseMove(object sender, MouseEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list != null && (e.LeftButton == MouseButtonState.Pressed ||
                e.MiddleButton == MouseButtonState.Pressed))
            {
                ListBoxItem selectedItem = list.SelectedItem as ListBoxItem;
                DragDrop.DoDragDrop(list, selectedItem.Content, DragDropEffects.Move);
            }
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {
            ListtextBlock.Text = App.contextText;
        }

        private void list_Loaded(object sender, RoutedEventArgs e)
        {
            ListBox list = sender as ListBox;
            list.Items.Add("");
            list.Items.Add("abcde");
            list.Items.Add("aBcDe");
            list.Items.Add("a B c D e");
            list.Items.Add("a1b2c3");
            list.Items.Add("()+*\\./*+()");
            list.Items.Add("1234");
        }

        private void list5_Loaded(object sender, RoutedEventArgs e)
        {            
            for (int i = 1; i <= 200; i++)
            {
                List5.Items.Add(i.ToString());                
            }
            
        }

        private void List5_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ListtextBlock.Text = e.ClickCount.ToString();
        }
    }
}
