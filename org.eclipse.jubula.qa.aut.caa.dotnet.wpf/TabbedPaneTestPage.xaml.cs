﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for TabbedPaneTestPage.xaml
    /// </summary>
    public partial class TabbedPaneTestPage : Window
    {
        public TabbedPaneTestPage()
        {
            InitializeComponent();
            this.tab1.ContextMenu = App.getContextMenu();
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {
            textBlock.Text = App.contextText;
        }

        private void tab3_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 50; i++)
            {
                TabItem tab = new TabItem();
                tab.Header = i;
                tab.Content = "Carrot";
                tab3.Items.Add(tab);
            }
        }
        
    }
}
