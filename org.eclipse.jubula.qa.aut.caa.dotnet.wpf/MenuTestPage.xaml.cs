﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for MenuTestPage.xaml
    /// </summary>
    public partial class MenuTestPage : Window
    {
        public MenuTestPage()
        {
            InitializeComponent();
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void click(object sender, RoutedEventArgs e)
        {
            var clickedItem = sender as MenuItem;
            textBlock.Text = clickedItem.Header.ToString(); 
        }
    }
}
