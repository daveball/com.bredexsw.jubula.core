﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for TextFieldTestPage.xaml
    /// </summary>
    public partial class TextFieldTestPage : Window
    {
        public TextFieldTestPage()
        {
            InitializeComponent();
            textfield1.ContextMenu = App.getContextMenu();
            textfield2.ContextMenu = App.getContextMenu();
            textfield3.ContextMenu = App.getContextMenu();
            textfield4.ContextMenu = App.getContextMenu();
            textfield5.ContextMenu = App.getContextMenu();
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {            
            textBlock.Text = App.contextText;
        }
    }
}
