﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonClick(object sender, RoutedEventArgs e)
        {
            var testPage = new ButtonTestPage();
            testPage.Show();
            this.Close();
        }

        private void menuClick(object sender, RoutedEventArgs e)
        {
            var testPage = new MenuTestPage();
            testPage.Show();
            this.Close();
        }

        private void checkBoxClick(object sender, RoutedEventArgs e)
        {
            var testPage = new CheckBoxTestPage();
            testPage.Show();
            this.Close();
        }

        private void radioBoxClick(object sender, RoutedEventArgs e)
        {
            var testPage = new RadioBoxTestPage();
            testPage.Show();
            this.Close();
        }

        private void comboBoxClick(object sender, RoutedEventArgs e)
        {
            var testPage = new ComboBoxTestPage();
            testPage.Show();
            this.Close();
        }

        private void textfieldsClick(object sender, RoutedEventArgs e)
        {
            var testPage = new TextFieldTestPage();
            testPage.Show();
            this.Close();
        }

        private void textareaClick(object sender, RoutedEventArgs e)
        {
            var testPage = new TextAreaTestPage();
            testPage.Show();
            this.Close();
        }

        private void labelClick(object sender, RoutedEventArgs e)
        {
            var testPage = new LabelTestPage();
            testPage.Show();
            this.Close();
        }

        private void appClick(object sender, RoutedEventArgs e)
        {
            var testPage = new ApplicationTestPage();
            testPage.Show();
            this.Close();
        }

        private void tableClick(object sender, RoutedEventArgs e)
        {
            var testPage = new TableTestPage();
            testPage.Show();
            this.Close();
        }

        private void listClick(object sender, RoutedEventArgs e)
        {
            var testPage = new ListTestPage();
            testPage.Show();
            this.Close();
        }

        private void treeClick(object sender, RoutedEventArgs e)
        {
            var testPage = new TreeTestPage();
            testPage.Show();
            this.Close();
        }

        private void paneClick(object sender, RoutedEventArgs e)
        {
            var testPage = new TabbedPaneTestPage();
            testPage.Show();
            this.Close();
        }

        private void pwClick(object sender, RoutedEventArgs e)
        {
            var testPage = new PasswordFieldTestPage();
            testPage.Show();
            this.Close();
        } 

        
    }
}
