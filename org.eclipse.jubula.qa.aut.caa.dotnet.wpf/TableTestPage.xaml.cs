﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for TableTestPage.xaml
    /// </summary>
    public partial class TableTestPage : Window
    {
        public class grid1Data
        {
            public string Column1 { get; set; }
            public string Column2 { get; set; }
            public string Column3 { get; set; }
            public string Column4 { get; set; }
        }

        public class grid5Data
        {
            public bool Column1 { get; set; }
            public string Column2 { get; set; }
            public string Column3 { get; set; }
            public string Column4 { get; set; }
        }

        public class grid6Data
        {
            public bool Column1 { get; set; }            
        }

        public class grid4Data
        {
            public string A { get; set; }
            public string B { get; set; }
            public string C { get; set; }
            public string D { get; set; }
            public string E { get; set; }
            public string F { get; set; }
            public string G { get; set; }
            public string H { get; set; }
            public string I { get; set; }
            public string J { get; set; }
            public string K { get; set; }
            public string L { get; set; }
            public string M { get; set; }
            public string N { get; set; }
            public string O { get; set; }
            public string P { get; set; }
            public string Q { get; set; }
            public string R { get; set; }
            public string S { get; set; }
            public string T { get; set; }
            public string right_align { get; set; }

            public grid4Data(string a, string b, string c, string d, string e, string f, string g, string h
                , string i, string j, string k, string l, string m, string n, string o, string p, string q, string r
                , string s, string t, string right)
            {
                this.A = a;
                this.B = b;
                this.C = c;
                this.D = d;
                this.E = e;
                this.F = f;
                this.G = g;
                this.H = h;
                this.I = i;
                this.J = j;
                this.K = k;
                this.L = l;
                this.M = m;
                this.N = n;
                this.O = o;
                this.P = p;
                this.Q = q;
                this.R = r;
                this.S = s;
                this.T = t;
                this.right_align = right;
            }
        }

        private List<grid4Data> getgrid4Data()
        {
            List<grid4Data> gridList = new List<grid4Data>();

            string[] abc = {"A","B","C","D","E","F","G","H","I","J"
                ,"K","L","M","N","O","P","Q","R","S","T","rl"};

            for (int i = 1; i < abc.Length; i++)
            {
                gridList.Add(new grid4Data(abc[0] + i, abc[1] + i, abc[2] + i, abc[3] + i
                    , abc[4] + i, abc[5] + i, abc[6] + i, abc[7] + i, abc[8] + i, abc[9] + i
                    , abc[10] + i, abc[11] + i, abc[12] + i, abc[13] + i, abc[14] + i, abc[15] + i
                    , abc[16] + i, abc[17] + i, abc[18] + i, abc[19] + i, abc[20] + i));
            }

            return gridList;
        }

        private List<grid1Data> getgrid1Data()
        {
            List<grid1Data> grid1List = new List<grid1Data>();
            grid1List.Add(new grid1Data()
            {
                Column1 = "value1",
                Column2 = "value2",
                Column3 = "3",
                Column4 = "true"
            });

            grid1List.Add(new grid1Data()
            {
                Column1 = "value4",
                Column2 = "value5",
                Column3 = "6",
                Column4 = "false"
            });

            return grid1List;
        }

        private List<grid5Data> getgrid5Data()
        {
            List<grid5Data> grid5List = new List<grid5Data>();
            grid5List.Add(new grid5Data()
            {
                Column1 = false,
                Column2 = "value2",
                Column3 = "3",
                Column4 = "true"
            });

            grid5List.Add(new grid5Data()
            {
                Column1 = false,
                Column2 = "value5",
                Column3 = "6",
                Column4 = "false"
            });

            return grid5List;
        }

        private List<grid6Data> getgrid6Data()
        {
            List<grid6Data> grid6List = new List<grid6Data>();
            grid6List.Add(new grid6Data()
            {
                Column1 = false                
            });
            grid6List.Add(new grid6Data()
            {
                Column1 = false
            });
            grid6List.Add(new grid6Data()
            {
                Column1 = false
            });
            grid6List.Add(new grid6Data()
            {
                Column1 = false
            });
            
            return grid6List;
        }

        private List<grid1Data> getgrid2Data()
        {
            List<grid1Data> gridList = new List<grid1Data>();
            
            gridList.Add(new grid1Data()
            {
                Column1 = "",
                Column2 = "1234",
                Column3 = "aBcDe",
                Column4 = "a1b2c3"
            });

            gridList.Add(new grid1Data()
            {
                Column1 = "abcde",
                Column2 = "a B c D e",
                Column3 = "a1b2c3",
                Column4 = ""
            });

            gridList.Add(new grid1Data()
            {
                Column1 = "aBcDe",
                Column2 = "",
                Column3 = "a B c D e",
                Column4 = "1234"
            });

            return gridList;
        }

        private List<grid1Data> getgrid3Data()
        {
            List<grid1Data> grid1List = new List<grid1Data>();
            grid1List.Add(new grid1Data()
            {
                Column1 = "value4",
                Column2 = "value5",
                Column3 = "6",
                Column4 = "false"
            });            

            return grid1List;
        }
        
        public TableTestPage()
        {
            InitializeComponent();
            this.grid1.ContextMenu = App.getContextMenu();
            grid1.AllowDrop = true;
            grid1.MouseMove += new MouseEventHandler(grid1_MouseMove);
            grid1.Drop += new DragEventHandler(grid1_Drop);
        }

        void grid1_Drop(object sender, DragEventArgs e)
        {
            Point pos = e.GetPosition(grid1);
            IInputElement dropTargetCell = grid1.InputHitTest(pos);
            TextBlock cellAsText = dropTargetCell as TextBlock;
            textBlock.Text = "From: " + (string)e.Data.GetData(DataFormats.StringFormat) + " to " + cellAsText.Text;
        }

        void grid1_MouseMove(object sender, MouseEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (grid != null && (e.LeftButton == MouseButtonState.Pressed ||
                e.MiddleButton == MouseButtonState.Pressed))
            {
                Point pos = e.GetPosition(grid1);
                IInputElement dragCell = grid1.InputHitTest(pos);
                TextBlock cellAsText = dragCell as TextBlock;
                DragDrop.DoDragDrop(grid, cellAsText.Text, DragDropEffects.Move);
            }
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {
            textBlock.Text = App.contextText;
        }

        private void grid1_Loaded(object sender, RoutedEventArgs e)
        {
            grid1.ItemsSource = getgrid1Data();
        }

        private void grid2_Loaded(object sender, RoutedEventArgs e)
        {
            grid2.ItemsSource = getgrid2Data();
        }

        private void grid3_Loaded(object sender, RoutedEventArgs e)
        {
            grid3.ItemsSource = getgrid3Data();
        }

        private void grid4_Loaded(object sender, RoutedEventArgs e)
        {
            grid4.ItemsSource = getgrid4Data();
        }

        private void grid5_Loaded(object sender, RoutedEventArgs e)
        {
            grid5.ItemsSource = getgrid5Data();
        }

        private void grid6_Loaded(object sender, RoutedEventArgs e)
        {
            grid6.ItemsSource = getgrid6Data();
        }
       
    }
}
