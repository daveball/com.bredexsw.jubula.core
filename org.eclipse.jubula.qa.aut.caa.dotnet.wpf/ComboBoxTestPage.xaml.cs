﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for ComboBoxTestPage.xaml
    /// </summary>
    public partial class ComboBoxTestPage : Window
    {
        public ComboBoxTestPage()
        {
            InitializeComponent();
            this.combobox.ContextMenu = App.getContextMenu();
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {

            textBlock.Text = App.contextText;
        }

        private void combobox_Loaded(object sender, RoutedEventArgs e)
        {

            try
            {
                //win7
                var child1 = VisualTreeHelper.GetChild(combobox, 0);
                var child2 = VisualTreeHelper.GetChild(child1, 0);
                var child3 = VisualTreeHelper.GetChild(child2, 2);
                TextBox tb = VisualTreeHelper.GetChild(child3, 1) as TextBox;
                tb.ContextMenu = combobox.ContextMenu;
            }
            catch (Exception)
            {
            }

            try
            {
                ////textbox on windows 8
                var child1 = VisualTreeHelper.GetChild(combobox, 0);
                var child2 = VisualTreeHelper.GetChild(child1, 2);
                TextBox tb = VisualTreeHelper.GetChild(child2, 0) as TextBox;
                tb.ContextMenu = combobox.ContextMenu;
            }
            catch (Exception)
            {
            }

            try
            {
                ////textbox on windows vista
                var child1 = VisualTreeHelper.GetChild(combobox, 0);
                TextBox tb = VisualTreeHelper.GetChild(child1, 2) as TextBox;
                tb.ContextMenu = combobox.ContextMenu;        
            }
            catch (Exception)
            {
            }                     
        }  
    }
}
