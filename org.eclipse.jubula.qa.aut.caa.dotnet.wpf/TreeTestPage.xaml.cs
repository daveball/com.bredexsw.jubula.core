﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for TreeTestPage.xaml
    /// </summary>
    public partial class TreeTestPage : Window
    {
        private List<TreeViewItem> infiniteChildren = new List<TreeViewItem>();

        public TreeTestPage()
        {
            InitializeComponent();
            this.tree1.ContextMenu = App.getContextMenu();
            tree1.AllowDrop = true;
            tree1.MouseMove += new MouseEventHandler(tree1_MouseMove);
            tree1.Drop += new DragEventHandler(tree1_Drop);
            CreateInfiniteTree();
        }

        private void CreateInfiniteTree()
        {
            TreeViewItem root = new TreeViewItem();
            root.Header = "root";
            infiniteChildren.Add(root);
            tree4.Items.Add(root);
            root.Items.Add("staticChild");
            root.Expanded += new RoutedEventHandler(item_Expanded);
        }

        void item_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem child1 = new TreeViewItem();
            child1.Header = "dynamicChild";
            TreeViewItem child2 = new TreeViewItem();
            child2.Header = "dynamicChild";
            infiniteChildren.ElementAt(infiniteChildren.Count - 1).Items.Add(child1);
            child1.Items.Add(child2);
            infiniteChildren.Add(child1);
            infiniteChildren.Add(child2);
        }

        void tree1_Drop(object sender, DragEventArgs e)
        {
           Point pos = e.GetPosition(tree1);           
           IInputElement dropNode = tree1.InputHitTest(pos);
           TextBlock nodeAsText = dropNode as TextBlock;
           if (nodeAsText != null)
           {
               textBlock.Text = "From: " + (string)e.Data.GetData(DataFormats.StringFormat) + " to " + nodeAsText.Text;
           }
        }

        void tree1_MouseMove(object sender, MouseEventArgs e)
        {
            TreeView tree = sender as TreeView;
            if (tree != null && (e.LeftButton == MouseButtonState.Pressed || 
                e.MiddleButton == MouseButtonState.Pressed)) {
                    TreeViewItem dragItem = tree.SelectedItem as TreeViewItem;
                    if (dragItem != null)
                    {
                        DragDrop.DoDragDrop(tree, dragItem.Header, DragDropEffects.Move);      
                    }   

            }
        }       

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {
            textBlock.Text = App.contextText;
        }

        private void tree_Loaded(object sender, RoutedEventArgs e)
        {
            string longName = "Really long name";
            string shortName = "Short";

            TreeViewItem root = new TreeViewItem();
            root.Header = "";
            root.IsExpanded = true;

            TreeViewItem node1 = new TreeViewItem();
            node1.Header = longName;
            TreeViewItem node2 = new TreeViewItem();
            node2.Header = longName;
            TreeViewItem node3 = new TreeViewItem();
            node3.Header = shortName;
            TreeViewItem node4 = new TreeViewItem();
            node3.Header = shortName;

            TreeViewItem node11 = new TreeViewItem();
            node11.Header = shortName;
            TreeViewItem node12 = new TreeViewItem();
            node12.Header = shortName;
            TreeViewItem node21 = new TreeViewItem();
            node21.Header = shortName;
            TreeViewItem node22 = new TreeViewItem();
            node22.Header = longName;
            TreeViewItem node31 = new TreeViewItem();
            node31.Header = shortName;
            TreeViewItem node32 = new TreeViewItem();
            node32.Header = shortName;
            TreeViewItem node41 = new TreeViewItem();
            node41.Header = longName;
            TreeViewItem node42 = new TreeViewItem();
            node42.Header = longName;

            node2.Items.Add(node21);
            node2.Items.Add(node22);

            node3.Items.Add(node31);
            node3.Items.Add(node32);

            node4.Items.Add(node41);
            node4.Items.Add(node42);

            root.Items.Add(node1);
            root.Items.Add(node2);
            root.Items.Add(node3);
            root.Items.Add(node4);
            
            tree5.Items.Add(root);
        }
        
        private void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem expandedItem = sender as TreeViewItem;
            TreeViewItem item1 = new TreeViewItem();
            TreeViewItem item2 = new TreeViewItem();

            if(expandedItem.Header.Equals("aBcDe"))
            {                
                item1.Header = "a B c D e";
                item1.Expanded += TreeViewItem_Expanded;                
                item2.Header = "a1b2c3";

                TreeViewItem item1_1 = new TreeViewItem();
                item1_1.Header = "platzhalter";

                item1.Items.Add(item1_1);              
            }
            else if (expandedItem.Header.Equals("a B c D e"))
            {                
                item1.Header = "1234";
                item2.Header = "abcde";
            }

            expandedItem.Items.RemoveAt(0);
            expandedItem.Items.Add(item1);
            expandedItem.Items.Add(item2);
            expandedItem.Expanded -= TreeViewItem_Expanded;            
        }        
    }
}
