﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for TextAreaTestPage.xaml
    /// </summary>
    public partial class TextAreaTestPage : Window
    {
        public TextAreaTestPage()
        {
            InitializeComponent();
            this.textArea1.ContextMenu = App.getContextMenu();
            this.textArea2.ContextMenu = App.getContextMenu();
            this.textArea3.ContextMenu = App.getContextMenu();
            this.textArea4.ContextMenu = App.getContextMenu();
            this.textArea5.ContextMenu = App.getContextMenu();
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {
            textBlock.Text = App.contextText;
        }
    }
}
