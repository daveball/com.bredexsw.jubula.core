﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.wpfCaA
{
    /// <summary>
    /// Interaction logic for LabelTestPage.xaml
    /// </summary>
    public partial class LabelTestPage : Window
    {
        public LabelTestPage()
        {
            InitializeComponent();
            this.Label1.ContextMenu = App.getContextMenu();
        }

        private void closePage(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void contextClosing(object sender, ContextMenuEventArgs e)
        {
            textBlock.Text = App.contextText;
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                label3.Content = "OneClick";
            }
            else if (e.ClickCount == 2)
            {
                label3.Content = "TwoClick";
            }            
        }
    }
}
