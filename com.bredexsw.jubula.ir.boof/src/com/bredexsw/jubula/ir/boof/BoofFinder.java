/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.boof;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import boofcv.alg.feature.detect.template.TemplateMatching;
import boofcv.alg.feature.detect.template.TemplateMatchingIntensity;
import boofcv.alg.misc.ImageStatistics;
import boofcv.alg.misc.PixelMath;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.factory.feature.detect.template.FactoryTemplateMatching;
import boofcv.factory.feature.detect.template.TemplateScoreType;
import boofcv.gui.image.ShowImages;
import boofcv.gui.image.VisualizeImageData;
import boofcv.struct.feature.Match;
import boofcv.struct.image.ImageFloat32;

import com.bredexsw.jubula.ir.core.ImageMatch;
import com.bredexsw.jubula.ir.core.SearchOptions;
import com.bredexsw.jubula.ir.core.exceptions.IncompatibleImageException;
import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;

/**
 * Finds templates using boofCV
 * @author BREDEX GmbH
 */
public class BoofFinder implements IRegionFinder {

    /**
     * private constructor for utility class
     */
    private BoofFinder() {
        
    }
    /**
     * Demonstrates how to search for matches of a template inside an image
     *
     * @param image           Image being searched
     * @param template        Template being looked for
     * @param expectedMatches Number of expected matches it hopes to find
     * @return List of match location and scores
     */
    private static List<Match> findMatches(
            ImageFloat32 image, ImageFloat32 template,
                                           int expectedMatches) {
        // create template matcher.
        TemplateMatching<ImageFloat32> matcher =
                FactoryTemplateMatching.createMatcher(
                        TemplateScoreType.SUM_DIFF_SQ, ImageFloat32.class);
        // Find the points which match the template the best
        matcher.setTemplate(template, expectedMatches);
        long starttime = System.currentTimeMillis();
        System.out.println("findMatches: Starting process");
        matcher.process(image);

        System.out.println("findMatches: Image processed in " 
                + (System.currentTimeMillis() - starttime) + "ms.");
        
        return matcher.getResults().toList();
 
    }
 
    /**
     * Computes the template match intensity image and displays the results. Brighter intensity indicates
     * a better match to the template.
     * @param image image to search in
     * @param template image to search for
     */
    public static void showMatchIntensity(ImageFloat32 image,
            ImageFloat32 template) {
 
        // create algorithm for computing intensity image
        TemplateMatchingIntensity<ImageFloat32> matchIntensity =
                FactoryTemplateMatching.createIntensity(
                        TemplateScoreType.SUM_DIFF_SQ, ImageFloat32.class);
        System.out.println("Starting process");
        // apply the template to the image
        long starttime = System.currentTimeMillis();
        System.out.println("findIntensity: Starting process");
        matchIntensity.process(image, template);
        System.out.println("findIntensity: Image processed in "
                + (System.currentTimeMillis() - starttime) + "ms.");
        // get the results
        ImageFloat32 intensity = matchIntensity.getIntensity();

        for (int i = 1; i < intensity.getHeight() - template.height; i++) {
            System.out.println(i);
            for (int j = 1; j < intensity.getWidth() - 1000; j++) {
                System.out.println("Height: " + i + "\t Width: " + j);
                System.out.println("Intensity at point " + j + "/" 
                        + i + ": " + intensity.get(i - 1, j - 1));
            }
        }
        // adjust the intensity image so that white indicates a good match and black a poor match
        // the scale is kept linear to highlight how ambiguous the solution is
        float min = ImageStatistics.min(intensity);
        float max = ImageStatistics.max(intensity);
        float range = max - min;
        PixelMath.plus(intensity, -min, intensity);
        PixelMath.divide(intensity, range, intensity);
        PixelMath.multiply(intensity, 255.0f, intensity);
 
        BufferedImage output = new BufferedImage(image.width, 
                image.height, BufferedImage.TYPE_INT_BGR);
        VisualizeImageData.grayMagnitude(intensity, output, -1);
        ShowImages.showWindow(output, "Match Intensity");
    }
 
    /**
     * main class
     * @param args arguments
     */
    public static void main(String args[]) {
        String direc = "K:/guidancer/Workspace/users"
                + "/OliverHoffmann/workspaceWindows/ImageRec/";
//        start(direc + "Screenshots/HTML/PNG/Application.png", direc + "Templates/HTML/PNG/input_Text.PNG", 1);
//        start(direc + "Screenshots/iOS/portrait1.png", direc + "Templates/iOS/portrait/1_plus.PNG", 4);
//        start(direc + "Screenshots/JavaFX/1.png", direc + "Templates/JavaFX/1_button.PNG", 1);
//        start(direc + "Screenshots/CENTOS/2_new.PNG", direc + "Templates/CENTOS/2_window.PNG", 1);
//        start(direc + "Screenshots/RCP/CENTOS/Tree_full.png", direc + "Templates/RCP/tf_full.PNG", 1);
        BoofFinder f = new BoofFinder();
        try {
            f.findRegions(ImageIO.read(new File(direc 
                    + "Templates/macOS/desk_button.PNG")),
                    ImageIO.read(new File(direc 
                            + "Screenshots/macOS/desk_new.PNG")), null);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (IncompatibleImageException e) {
            e.printStackTrace();
        }
        
    }
 
    /** {@inheritDoc} */
    public List<ImageMatch> findRegions (BufferedImage templateImage, 
            BufferedImage searchImage, SearchOptions options)
        throws IncompatibleImageException {
        
        //used for drawRectangles, might need to find a way around that
        int expected = 1;
        ImageFloat32 image = new ImageFloat32(searchImage.getWidth(),
                searchImage.getHeight());
        ConvertBufferedImage.convertFrom(searchImage, image);
        ImageFloat32 template = new ImageFloat32(templateImage.getWidth(),
                templateImage.getHeight());        
        ConvertBufferedImage.convertFrom(templateImage, template);
        
//        showMatchIntensity(image, template);
//        ConvertBufferedImage.convertTo(image, output);
        return drawRectangles(image, template, expected);
//        return null;
    }
    /**
     * Helper function will find matches and displays the results as colored rectangles
     * @param image image to draw in
     * @param template template to find
     * @param expectedMatches the expected matches
     * @return foundRegions
     */
    private static List<ImageMatch> drawRectangles(
                                      ImageFloat32 image, ImageFloat32 template,
                                       int expectedMatches) {
        List<Match> found = findMatches(image, template, expectedMatches);
        System.out.println("Matches found: " + found.size());
        List<ImageMatch> result = new ArrayList<ImageMatch>();

        int w = template.width;
        int h = template.height;
 
        for (int i = 0; i < found.size(); i++) {
            // the return point is the template's top left corner
            int x0 = found.get(i).x;
            int y0 = found.get(i).y;           
            result.add(new ImageMatch(
                    new Rectangle(x0, y0, x0 + w, y0 + h), 1));
            
            System.out.println("Region found at " + x0 
                    + "/" + y0 + " with similarity "
                    + found.get(i).score + ".");
        }
        return result;
    }
}
