/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.SafeRunner;

import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;

/**
 * Class for getting a single image-finder or all of them
 * @author BREDEX GmbH
 *
 */
public class Finders {

    /** list with all finders */
    private static List<IRegionFinder> finders = new ArrayList<IRegionFinder>();
    
    /** hidden constructor */
    private Finders() {
        //empty
    }
    
    /** gets the finder that is specified by the name
     *  @param name name of the finder
     *  @return the finder  
     */
    public static IRegionFinder getFinder(String name) {
        IExtensionRegistry registry = Activator.getRegistryInstance();
        IConfigurationElement[] config =
                registry.getConfigurationElementsFor(
                        "com.bredexsw.jubula.ir.core.finders");
        IRegionFinder rf = null;
        
        try {
            for (IConfigurationElement e : config) {
                if (e.getAttribute("title").equals(name)) {
                    final Object o =
                            e.createExecutableExtension("class");

                    if (o instanceof IRegionFinder) {
                        rf = (IRegionFinder) o;
                    }
                }

            }
        } catch (CoreException ex) {
            System.out.println(ex.getMessage());
        }
        return rf;
    }
    
    /**
     * returns all finders that are extensions of core
     * @return Array list of finders
     */
    public static List<IRegionFinder> getFinders() {
        if (finders.isEmpty()) {
            setupFinders();
        }
        return finders;
    }
    /** set up the finders list */
    private static void setupFinders() {
        IExtensionRegistry registry = Activator.getRegistryInstance();
        IConfigurationElement[] config =
                registry.getConfigurationElementsFor(
                        "com.bredexsw.jubula.ir.core.finders");
        try {
            for (IConfigurationElement e : config) {
                final Object o =
                    e.createExecutableExtension("class");
                if (o instanceof IRegionFinder) {
                    executeExtension(o);
                }
            }
        } catch (CoreException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * execute the extension
     * @param o object to add to finders
     */
    private static void executeExtension(final Object o) {
        ISafeRunnable runnable = new ISafeRunnable() {
            public void handleException(Throwable e) {
                System.out.println("Exception in client");
            }
            public void run() throws Exception {
                finders.add((IRegionFinder) o);
            }
        };
        SafeRunner.run(runnable);
    }
}
