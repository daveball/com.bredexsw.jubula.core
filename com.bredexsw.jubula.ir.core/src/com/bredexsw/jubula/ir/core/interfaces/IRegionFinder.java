/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core.interfaces;

import java.awt.image.BufferedImage;
import java.util.List;

import com.bredexsw.jubula.ir.core.ImageMatch;
import com.bredexsw.jubula.ir.core.SearchOptions;
import com.bredexsw.jubula.ir.core.exceptions.ImageTimeoutException;
import com.bredexsw.jubula.ir.core.exceptions.IncompatibleImageException;

/**
 * Interface for all Image-Recognition Methods
 * @author BREDEX GmbH
 */

public interface IRegionFinder {

    /**
     * get the Regions of the template found on the search image
     * @param searchImage image to search in(e.g. desktop-screenshot)
     * @param templateImage template to search for
     * @param options options like threshold for the search
     * @return List of Regions of template on searchImage and similarity of match
     * @throws IncompatibleImageException if the template is wider/higher than 
     * the search image
     */
    public List<ImageMatch> findRegions(BufferedImage templateImage,
            BufferedImage searchImage, SearchOptions options) 
        throws IncompatibleImageException, ImageTimeoutException;
 
}
