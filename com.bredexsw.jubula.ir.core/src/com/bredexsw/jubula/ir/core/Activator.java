/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.RegistryFactory;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends Plugin {

    /** The plug-in ID */
    public static final String PLUGIN_ID = "com.bredexsw.jubula.ir.core"; //$NON-NLS-1$
    
    /** extension registry of the plugin */
    private static IExtensionRegistry registry;
    
    /** The shared instance */
    private static Activator plugin;

    /**
     * The constructor
     */
    public Activator() {
    }

    /**
     * 
     * @param context the context
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
        registry = RegistryFactory.getRegistry();
    }

    /**
     * @param context the context
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }

    /**
     * gets the registry instance of the ir.core bundle
     * @return the registry instance of the ir.core bundle
     */
    public static IExtensionRegistry getRegistryInstance() {
        registry = RegistryFactory.getRegistry();
        return registry;
    }
}
