/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.core;

/**
 * Data type for search options
 * @author BREDEX GmbH
 *
 */
public class SearchOptions {

    // Exception if several Options are true which are not to be used together
    // Exception if threshold smaller than 0 or bigger than 1
    
    /** is color relevant for search? */
    private boolean m_inColor;
    
    /** should the algorithm only look for corners? */
    private boolean m_cornerSearch;
    
    /** threshold for minimum similarity of matches */
    private double m_threshold;
    
    /** time in milliseconds after which findRegions should terminate (0 if no timeout is set)*/
    private int m_timeout;
    
    /**
     * Constructor for search options
     * @param inColor is color relevant for search?
     * @param cornerSearch should the algorithm only look for corners?(used for e.g. buttons)
     * @param threshold minimum amount of similarity of matches(between 0 and 1)
     * @param timeout time in milliseconds after which findRegions should terminate 
     * (0 if no timeout should be set)
     */
    public SearchOptions(boolean inColor, boolean cornerSearch,
            double threshold, int timeout) {
        this.m_inColor = inColor;
        this.m_cornerSearch = cornerSearch;
        this.m_threshold = threshold;
        this.m_timeout = timeout;
    }

    /**
     * 
     * @return true if color relevant else false
     */
    public boolean isInColor() {
        return m_inColor;
    }

    /**
     * 
     * @return true if algorithm should look for corners else false
     */
    public boolean isCornerSearch() {
        return m_cornerSearch;
    }

    /**
     * @return the Threshold(between 0 and 1)
     */
    public double getThreshold() {
        return m_threshold;
    }
    
    /**
     * @return time in milliseconds after which findRegions should terminate (0 if no timeout is set)
     */
    public int getTimeout() {
        return m_timeout;
    }

}
