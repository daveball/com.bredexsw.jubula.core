In this document you can find general hints for developing modern UI apps

== Known issues ==

=== What can I do to use longer file path? ===

* Problem: The file path length on Windows OS has a maximum of 260 chars and VS can not build.
* Solution: Map a folder to a drive letter with the subst command line like the following example:
{{{
  subst Z: K:\My\workspace\in\a\network\share
}}}

=== How can I deploy a modern UI app while the source code is on a network share? ===
* Add the following property to your {{{csproj}}} or {{{jsproj}}} file, which stores the generated files for deploying in your temporary folder:
{{{
  <PropertyGroup>
    <LayoutDir>$(TMP)\$(MSBuildProjectName)\$(Configuration)</LayoutDir>
  </PropertyGroup>
}}}
* These files in your temporary folder can be removed, after your program has been installed on the modern UI desktop, but they will not be removed automatically.
* See http://social.msdn.microsoft.com/Forums/windowsapps/en-US/3fc1f3cf-2d8b-4dee-a348-40d0bf2c3c66/rejecting-request-to-register-from-file-when-deploying

See also:
* [../org.eclipse.jubula.qa.aut.caa.dotnet.modernui.html5js/readme.tracwiki] Hints for developing HTML5 JavaScript modern UI apps