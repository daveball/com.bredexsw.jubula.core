﻿using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using System;

namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// public static backlink to the mainpage to navigate and 
        /// to create menus.
        /// </summary>
        public static MainPage Current;

        public MainPage()
        {
            Current = this;
            this.InitializeComponent();
        }

        private void buttonClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(ButtonPage));
            }
        }

        private void checkBoxClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(CheckBoxPage));
            }
        }

        private void radioBoxClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(RadioBoxPage));
            }
        }

        private void listViewClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(ListViewPage));
            }
        }

        private void textfieldsClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(TextFieldsPage));
            }
        }

        private void textblockClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(TextAreaPage));
            }
        }

        private void comboboxesClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(ComboBoxPage));
            }
        }

        private void listBoxClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(ListBoxPage));
            }
        }

        private void applicationClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(ApplicationPage));
            }
        }

        private void labelClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(LabelPage));
            }
        }

        private void pwFldClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(PasswordFields));
            }
        }

        private void appBarClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(AppBarPage));
            }
        }

        private void charmBarClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(CharmBarPage));
            }
        }

        private void datePickerClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(DatePickerPage));
            }
        }

        private void gestureClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(SwipePage));
            }
        }


        private void containerClick(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(ContainerPage));
            }
        }

        public void gotoMainPage(object sender, RoutedEventArgs e)
        {
            if (this.Frame != null)
            {
                this.Frame.Navigate(typeof(MainPage));
            }
        }

        public async void OpenContextMenu(object sender, RightTappedRoutedEventArgs e)
        {
            TextBlock textBlock = (sender as FrameworkElement).Tag as TextBlock;
            var menu = new PopupMenu();
            menu.Commands.Add(new UICommand("FirstItem", (command) =>
            {
                textBlock.Text = command.Label;
            }));
            menu.Commands.Add(new UICommand("More", (command) =>
            {
                textBlock.Text = command.Label;
            }));
            textBlock.Text = "Context menu shown";
            var chosenCommand = await menu.ShowForSelectionAsync(App.GetElementRect((FrameworkElement)sender));
            if (chosenCommand == null) // The command is null if no command was invoked.
            {
                textBlock.Text = "Context menu dismissed";
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // no configuration
        }

        public void setArgs(String args) {
            if (args.Equals(""))
            {
                this.ParameterText.Text = "The application has been started without any arguments.";
            }
            else
            {
                this.ParameterText.Text = "The application has been started with the following arguments: " + args;
            }
        }

    }
}
