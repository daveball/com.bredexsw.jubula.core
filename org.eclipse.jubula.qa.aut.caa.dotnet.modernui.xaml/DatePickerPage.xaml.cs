﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;


namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DatePickerPage : Page
    {

        private MainPage rootPage = MainPage.Current;

        public DatePickerPage()
        {
            InitializeComponent();
            datePicker1.Date = new System.DateTimeOffset(new System.DateTime(2013, 1, 1));
            datePicker1.DateChanged += datePicker_DateChanged;
            datePicker2.Date = new System.DateTimeOffset(new System.DateTime(2013, 1, 1));
            datePicker2.DateChanged += datePicker_DateChanged;
        }

        private void datePicker_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            textBlock.Text = datePicker1.Date.Month + "/" + datePicker1.Date.Day
                + "/" + datePicker1.Date.Year;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
    }
}
