﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SwipePage : Page
    {
        private GestureRecognizer m_gr = new GestureRecognizer();
        private double m_deltaX;
        private readonly MainPage rootPage = MainPage.Current;

        public SwipePage()
        {
            this.InitializeComponent();
            this.PointerPressed += swipePage_PointerPressed;
            this.PointerMoved += swipePage_PointerMoved;
            this.PointerReleased += swipePage_PointerReleased;
            m_gr.GestureSettings = GestureSettings.ManipulationTranslateX

                      | GestureSettings.ManipulationTranslateInertia;


            m_gr.ManipulationCompleted += gr_ManipulationCompleted;
            m_gr.ManipulationInertiaStarting += gr_ManipulationInertiaStarting;
            m_gr.ManipulationStarted += gr_ManipulationStarted;
            m_gr.ManipulationUpdated += gr_ManipulationUpdated;
        }

        private void gr_ManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            m_deltaX += args.Delta.Translation.X;
        }

        private void gr_ManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {
            m_deltaX = 0;
        }

        private void gr_ManipulationInertiaStarting(GestureRecognizer sender, ManipulationInertiaStartingEventArgs args)
        {
            textBlock.Text = m_deltaX > 0 ? "Swipe right" : "Swipe left";
        }

        private void gr_ManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {
            m_deltaX = 0;
        }

        private void swipePage_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var ps = e.GetIntermediatePoints(null);
            if (ps != null && ps.Count > 0)
            {
                m_gr.ProcessUpEvent(ps[0]);
                e.Handled = true;
                m_gr.CompleteGesture();
            }
        }

        private void swipePage_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            m_gr.ProcessMoveEvents(e.GetIntermediatePoints(null));
            e.Handled = true;
        }

        private void swipePage_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var ps = e.GetIntermediatePoints(null);
            if (ps != null && ps.Count > 0)
            {
                try
                {
                    m_gr.ProcessDownEvent(ps[0]);
                }
                catch (System.ArgumentException)
                {
                    e.Handled = true;
                    return;
                }
                finally
                {
                    e.Handled = true;
                }
            }
        }
    }
}
