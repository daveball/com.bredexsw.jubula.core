﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AppBarPage : Page
    {
        private MainPage rootPage = MainPage.Current;

        public AppBarPage()
        {
            this.InitializeComponent();
        }

              private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            textBlock.Text = (sender as AppBarButton).Label;
        }

        private async void Button_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            var menu = new PopupMenu();
            menu.Commands.Add(new UICommand("FirstItem", (command) =>
            {
                textBlock.Text = command.Label;
            }));
            menu.Commands.Add(new UICommand("More", (command) =>
            {
                textBlock.Text = command.Label;
            }));
            textBlock.Text = "Context menu shown";
            var chosenCommand = await menu.ShowForSelectionAsync(App.GetElementRect((FrameworkElement)sender));
            if (chosenCommand == null) // The command is null if no command was invoked.
            {
                textBlock.Text = "Context menu dismissed";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            textBlock.Text = (sender as Button).Content as string;
        }
    }
}
