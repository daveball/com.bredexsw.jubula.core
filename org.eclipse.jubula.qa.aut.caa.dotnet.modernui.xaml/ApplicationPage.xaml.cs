﻿using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

namespace caa.aut.modernui
{
    /// <summary>
    /// the ApplicationPage
    /// </summary>
    public sealed partial class ApplicationPage : Page
    {
        private MainPage rootPage = MainPage.Current;

        public ApplicationPage()
        {
            this.InitializeComponent();
            this.textBlock.Tag = textBox;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        
        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
    }
}
