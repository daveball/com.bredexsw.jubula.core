﻿using System;
using System.Diagnostics;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListViewPage : Page
    {

        private MainPage rootPage = MainPage.Current;
        
        public ListViewPage()
        {
            this.InitializeComponent();
            listview.RightTapped += rootPage.OpenContextMenu;
            listview.Items.VectorChanged += Items_VectorChanged;
            List2.DragItemsStarting += List2_DragItemsStarting;
        }

        void List2_DragItemsStarting(object sender, DragItemsStartingEventArgs e)
        {
            //var lv = sender as ListView;
            //ListViewItem item = lv.SelectedItem as ListViewItem;
            //textBlock.Text = item.Content.ToString();
        }

         
        
        void Items_VectorChanged(IObservableVector<object> sender, IVectorChangedEventArgs e)
        {
            Debug.WriteLine("MAIN:{0} {1}", e.CollectionChange, e.Index);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
      
        private void list_Loaded(object sender, RoutedEventArgs e)
        {
            ListView list = sender as ListView;
            list.Items.Add("");
            list.Items.Add("abcde");
            list.Items.Add("aBcDe");
            list.Items.Add("a B c D e");
            list.Items.Add("a1b2c3");
            list.Items.Add("()+*\\./*+()");
            list.Items.Add("1234");
        }

        private void list5_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 1; i <= 200; i++)
            {
                List5.Items.Add(i.ToString());
            }
        }
    }
}
