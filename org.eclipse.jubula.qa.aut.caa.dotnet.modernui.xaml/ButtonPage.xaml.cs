﻿using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ButtonPage : Page
    {
        private MainPage rootPage = MainPage.Current;

        public ButtonPage()
        {
            this.InitializeComponent();
            Button.Tag = textBlock;
            Button.RightTapped += rootPage.OpenContextMenu;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            textBlock.Text = "Button";
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            textBlock.Text = "Button1";
        }
    }
}
