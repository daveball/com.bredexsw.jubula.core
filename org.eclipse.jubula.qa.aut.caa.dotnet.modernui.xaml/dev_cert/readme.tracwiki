In this document you can find installing instructions for the developer certificates,
which are necessary for building (A) and installing (B) a modern UI app. Also other informations depending the certificates are documented here (C).

* (A) Install the pfx file on the build machine into '''Trusted Root Certification Authorities''' storage location:
 1. Login in with the user, who wants to build the Modern UI App.
 1. Double click on the {{{dev_cert.pfx}}} file.
 1. Choose '''Current User''' as '''Storage Location'''
 1. Click '''Next'''
 1. Click '''Next'''
 1. Click '''Next'''
 1. Select '''Place all certificates in the following store'''
 1. Click '''Browse...'''
 1. Choose '''Trusted Root Certification Authorities'''
 1. Click '''OK'''
 1. Click '''Next''' and '''Finish'''.
 1. The pfx file is correctly installed, if the '''bredex.de''' certificate is located in the '''Trusted Root Certification Authorities''' storage location and shows '''The certificate is OK.''' under the '''Certificate Path''' tab in the configuration manager, which can be opened with {{{certmgr.msc}}} after pressing <WIN>+R.

* (B) Install the crt file on the machine into '''Trusted People''' storage location, where the Modern UI App has to be installed (keep in mind, that MS Visual Studio has also be installed):
 1. Login in with a user, which has administrator rights.
 1. Double click on the {{{dev_cert.crt}}} file.
 1. Click on '''Install Certificate...'''
 1. Select '''Local Machine''' as '''Storage Location'''
 1. Click '''Next'''
 1. Click '''Yes''' on the '''User Account Control''' dialog
 1. Select '''Place all certificates in the following store'''
 1. Click '''Browse...'''
 1. Choose '''Trusted People'''
 1. Click '''OK'''
 1. Click '''Next''' and '''Finish'''
 1. The crt file is correctly installed, if the '''bredex.de''' certificate is located in the '''Trusted People''' storage location and shows '''The certificate is OK.''' under the '''Certificate Path''' tab in the configuration manager, which can be opened with {{{certmgr.msc}}} after pressing <WIN>+R.

* (C) Here are some useful informations you should know about certificates for Modern UI Apps.
 * The certificate used here is created originally by MS Visual Studio:
  1. Select Project -> Store -> Create App Packages...
  1. Select '''No''' for not uploading to the Windows Store
  1. Click Next
  1. Click Create
  1. Click OK
  1. You can verify the creation of the package:
   * A pfx file is created in the solution folder (a link is added to the solution and project file) and
   * a appx file is created in the sub folder {{{AppPackages}}}.
 * The certificate must contain the '''Extended Key Usage''' (EKU) for source code signing. This is defined by the number '''1.3.6.1.5.5.7.3.3''', which can be verified in the properties of the certificate.
 * After 30 days you must reactivate MS Visual Studio with your live account credentials. Otherwise installing of a Modern UI App is not possible.