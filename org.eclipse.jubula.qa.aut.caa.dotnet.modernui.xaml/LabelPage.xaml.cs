﻿using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace caa.aut.modernui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LabelPage : Page
    {

        private MainPage rootPage = MainPage.Current;

        public LabelPage()
        {
            this.InitializeComponent();
            this.lbl1.Tag = textBlock;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void lbl3_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            (sender as TextBlock).Text = "TwoClick";
        }

        private void lbl3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            (sender as TextBlock).Text = "OneClick";
        }
    }
}
