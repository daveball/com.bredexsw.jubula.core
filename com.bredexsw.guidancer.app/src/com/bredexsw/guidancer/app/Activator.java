package com.bredexsw.guidancer.app;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jubula.client.ui.utils.ImageUtils;
import org.eclipse.jubula.version.Vn;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author markus
 * @created Nov 22, 2010
 */
public class Activator extends AbstractUIPlugin {
    /**
     * <code>PLUGIN_ID</code>
     */
    public static final String PLUGIN_ID = "com.bredexsw.guidancer.app"; //$NON-NLS-1$

    /**
     * <code>IMAGE_PNG_GD_128_128_ID</code>
     */
    public static final String IMAGE_PNG_GD_128_128_ID = "IMAGE_PNG_GD_128_128_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_PNG_GD_64_64_ID</code>
     */
    public static final String IMAGE_PNG_GD_64_64_ID = "IMAGE_PNG_GD_64_64_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_PNG_GD_48_48_ID</code>
     */
    public static final String IMAGE_PNG_GD_48_48_ID = "IMAGE_PNG_GD_48_48_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_PNG_GD_32_32_ID</code>
     */
    public static final String IMAGE_PNG_GD_32_32_ID = "IMAGE_PNG_GD_32_32_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_PNG_GD_16_16_ID</code>
     */
    public static final String IMAGE_PNG_GD_16_16_ID = "IMAGE_PNG_GD_16_16_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_GIF_GD_128_128_ID</code>
     */
    public static final String IMAGE_GIF_GD_128_128_ID = "IMAGE_GIF_GD_128_128_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_GIF_GD_64_64_ID</code>
     */
    public static final String IMAGE_GIF_GD_64_64_ID = "IMAGE_GIF_GD_64_64_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_GIF_GD_48_48_ID</code>
     */
    public static final String IMAGE_GIF_GD_48_48_ID = "IMAGE_GIF_GD_48_48_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_GIF_GD_32_32_ID</code>
     */
    public static final String IMAGE_GIF_GD_32_32_ID = "IMAGE_GIF_GD_32_32_ID"; //$NON-NLS-1$

    /**
     * <code>IMAGE_GIF_GD_16_16_ID</code>
     */
    public static final String IMAGE_GIF_GD_16_16_ID = "IMAGE_PNG_GD_16_16_ID"; //$NON-NLS-1$

    /** 
     * Key for retrieving this bundle's version number from 
     * the JVM's Properties. The property is set during bundle activation. 
     */
    public static final String VERSION_PROPERTY_KEY = 
            "com.bredexsw.guidancer.app.version"; //$NON-NLS-1$
    
    /** The shared instance */
    private static Activator plugin;

    /**
     * The constructor
     */
    public Activator() {
    // empty
    }

    /**
     * {@inheritDoc}
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        System.setProperty(VERSION_PROPERTY_KEY, 
                Vn.getDefault().getVersion().toString());
        plugin = this;
    }

    /**
     * {@inheritDoc}
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }

    @Override
    protected void initializeImageRegistry(ImageRegistry registry) {
        super.initializeImageRegistry(registry);
        registry.put(IMAGE_PNG_GD_128_128_ID,
                getImageDescriptor("guidancer128x128.png")); //$NON-NLS-1$
        registry.put(IMAGE_PNG_GD_64_64_ID,
                getImageDescriptor("guidancer64x64.png")); //$NON-NLS-1$
        registry.put(IMAGE_PNG_GD_48_48_ID,
                getImageDescriptor("guidancer48x48.png")); //$NON-NLS-1$
        registry.put(IMAGE_PNG_GD_32_32_ID,
                getImageDescriptor("guidancer32x32.png")); //$NON-NLS-1$
        registry.put(IMAGE_PNG_GD_16_16_ID,
                getImageDescriptor("guidancer16x16.png")); //$NON-NLS-1$
        registry.put(IMAGE_GIF_GD_128_128_ID,
                getImageDescriptor("guidancer128x128.gif")); //$NON-NLS-1$
        registry.put(IMAGE_GIF_GD_64_64_ID,
                getImageDescriptor("guidancer64x64.gif")); //$NON-NLS-1$
        registry.put(IMAGE_GIF_GD_48_48_ID,
                getImageDescriptor("guidancer48x48.gif")); //$NON-NLS-1$
        registry.put(IMAGE_GIF_GD_32_32_ID,
                getImageDescriptor("guidancer32x32.gif")); //$NON-NLS-1$
        registry.put(IMAGE_GIF_GD_16_16_ID,
                getImageDescriptor("guidancer16x16.gif")); //$NON-NLS-1$
    }

    /**
     * @param name
     *            the file name URL
     * @return the image descriptor for the given file url
     */
    private ImageDescriptor getImageDescriptor(String name) {
        return ImageUtils.getImageDescriptor(Platform.getBundle(PLUGIN_ID),
                name);
    }
}
