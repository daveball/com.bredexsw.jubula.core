/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13240 $
 *
 * $Date: 2010-11-22 15:54:35 +0100 (Mon, 22 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2010 
 * 
 */
package com.bredexsw.guidancer.app.core;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jubula.app.core.JubulaWorkbenchWindowAdvisor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.bredexsw.guidancer.app.Activator;
import com.bredexsw.guidancer.app.i18n.Messages;

/**
 * @author markus
 * @created Nov 22, 2010
 */
public class GUIdancerWorkbenchWindowAdvisor extends
        JubulaWorkbenchWindowAdvisor {

    /**
     * Constructor
     * @param configurer the configurer
     */
    public GUIdancerWorkbenchWindowAdvisor(
            IWorkbenchWindowConfigurer configurer) {
        super(configurer);
    }
    
    /**
     * {@inheritDoc}
     */
    public void preWindowOpen() {
        getWindowConfigurer().setTitle(Messages.MainTitle);
        getWindowConfigurer().setShowMenuBar(true);
        getWindowConfigurer().setShowPerspectiveBar(true);
        getWindowConfigurer().setShowCoolBar(true);
        getWindowConfigurer().setShowStatusLine(true);
        getWindowConfigurer().setShowProgressIndicator(true);
        getWindowConfigurer().setShowFastViewBars(false);
    }

    /**
     * {@inheritDoc}
     */
    public void postWindowOpen() {
        super.postWindowOpen();
        AbstractUIPlugin plugin = Activator.getDefault();
        ImageRegistry imageRegistry = plugin.getImageRegistry();
        getWindowConfigurer().getWindow().getShell().setImages(
                new Image [] {
                        imageRegistry.get(Activator.IMAGE_GIF_GD_16_16_ID),
                        imageRegistry.get(Activator.IMAGE_GIF_GD_32_32_ID),
                        imageRegistry.get(Activator.IMAGE_GIF_GD_48_48_ID),
                        imageRegistry.get(Activator.IMAGE_GIF_GD_64_64_ID),
                        imageRegistry.get(Activator.IMAGE_GIF_GD_128_128_ID),
                        imageRegistry.get(Activator.IMAGE_PNG_GD_16_16_ID),
                        imageRegistry.get(Activator.IMAGE_PNG_GD_32_32_ID),
                        imageRegistry.get(Activator.IMAGE_PNG_GD_48_48_ID),
                        imageRegistry.get(Activator.IMAGE_PNG_GD_64_64_ID),
                        imageRegistry.get(Activator.IMAGE_PNG_GD_128_128_ID)
                });
    }
}
