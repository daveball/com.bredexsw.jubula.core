﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.dotnetCAA
{
    class ApplicationFormOpener
    {

        public ApplicationFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Application";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            TextBox textField = new TextBox();
            textField.Name = "TestPage.Application.TextField01";

            textField.ContextMenu = ContextMenuCreator.createContextMenu();

            panel.Controls.Add(textField);
            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

    }
}
