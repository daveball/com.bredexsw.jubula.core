﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class ComboBoxFormOpener
    {


        public ComboBoxFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "ComboBoxes";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();
            string[] values = {"", "abcde","aBcDe","a B c D e","a1b2c3","()+*\\./*+()","1234"};

            ComboBox bx1 = new ComboBox();
            bx1.Items.AddRange(values);
            bx1.Name = "TestPage.ComboBoxes.ComboBox01";
            bx1.ContextMenu = ContextMenuCreator.createContextMenu();

            ComboBox bx2 = new ComboBox();
            bx2.Name = "TestPage.ComboBoxes.ComboBox02";
            bx2.Enabled = false;

            ComboBox bx3 = new ComboBox();
            bx3.Name = "TestPage.ComboBoxes.ComboBox03";
            bx3.Items.AddRange(values);
            bx3.DropDownStyle = ComboBoxStyle.DropDownList;

            ComboBox bx4 = new ComboBox();
            bx4.Name = "TestPage.ComboBoxes.ComboBox04";
            bx4.Size = new Size(100,20);
            bx4.Items.AddRange(new string[] { "Size: 100px", "VeryVeryLongList", "VeryVeryVeryVeryLongList", "VeryVeryVeryVeryVeryVeryVeryLongList", "VeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongList", "VeryVeryVeryVeryVeryVeryVeryLongList", "VeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongList", "VeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongList" }); 
            bx4.DropDownStyle = ComboBoxStyle.DropDownList;
            bx4.SelectedIndex = 0;
                       
            panel.Controls.Add(bx1);
            panel.Controls.Add(bx2);
            panel.Controls.Add(bx3);
            panel.Controls.Add(bx4);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }
    }
}
