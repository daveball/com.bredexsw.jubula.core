﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using org.eclipse.jubula.qa.aut.caa.dotnet.dotnetCAA;
using org.eclipse.jubula.qa.aut.caa.dotnet.winFormsCaA;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
        }

        private void click_Listener(object sender, EventArgs e)
        {
            switch ((sender as Button).Text)
            {
                case "Buttons": new ButtonFormOpener(); break;
                case "CheckBoxes": new CheckBoxFormOpener(); break;
                case "RadioButtons": new RadioButtonFormOpener(); break;
                case "ComboBoxes": new ComboBoxFormOpener(); break;
                case "TextFields": new TextFieldFormOpener(); break;
                case "TextAreas": new TextAreaFormOpener(); break;
                case "Labels": new LabelFormOpener(); break;
                case "Menus": new MenuFormOpener(); break;
                case "Application": new ApplicationFormOpener(); break;
                case "Lists": new ListFormOpener(); break;
                case "Tables": new TableFormOpener(); break;
                case "Trees": new TreeFormOpener(); break;
                case "TabbedPanes": new TabbedPaneFormOpener(); break;
                case "PasswordFields": new PasswordFieldFormOpener(); break;
                case "ScrolledComposite": new ScrolledCompositeFormOpener(); break;
            }

            StartdotnetAUT.autClose();
        }

     }
}
