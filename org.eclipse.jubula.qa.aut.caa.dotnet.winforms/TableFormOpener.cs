﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class TableFormOpener
    {
        DataGridView table1; 

        /// <summary>
        /// Array of standard table value entries.
        /// </summary>
        static string[][] _valueEntries = new string[][] {
            new string[] {
                "value 1", "value 2", "3", "true"
            },
            new string[] {
                "value 4", "value 5", "6", "false"
            }
        };

        /// <summary>
        /// Array of standard table alphanumeric entries.
        /// </summary>
        static string[][] _alphaEntries = new string[][] {
            new string[] {
                "", "1234", "aBcDe", "a1b2c3"
            },
            new string[] {
                "abcde", "a B c D e", "a1b2c3", ""
            },
            new string[] {
                "aBcDe", "", "a B c D e", "1234"
            }
        };

        /// <summary>
        /// Array of standard table column names.
        /// </summary>
        static string[] _columnNames = new string[] {
            "Column 1", "Column 2", "Column 3", "Column 4"
        };

        public TableFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Tables";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            table1 = new DataGridView();
            table1.Name = "TestPage.Tables.Table01";
            table1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table1.RowHeadersVisible = false;
            table1.AllowUserToAddRows = false;
            table1.AllowUserToDeleteRows = false;
            table1.EditMode = DataGridViewEditMode.EditProgrammatically;
            table1.ColumnCount = 4;
            table1.ContextMenu = ContextMenuCreator.createContextMenu();
            table1.MouseClick += new MouseEventHandler(table1_MouseClick);
       

            for (int i = 0; i < table1.ColumnCount; i++ )
            {
                table1.Columns[i].Name = _columnNames[i];
            }
            
            foreach (string[] row in _valueEntries)
            {
                int newIndex = table1.Rows.Add(row);
                table1.Rows[newIndex].HeaderCell.Value = row[0];
            }

            table1.AutoSize = true;
            table1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            table1.RowHeadersWidthSizeMode = 
                DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            table1.CellMouseDown += new DataGridViewCellMouseEventHandler(table1_CellMouseDown);
            table1.AllowDrop = true;
            table1.DragDrop += new DragEventHandler(table1_DragDrop);
            table1.DragOver += new DragEventHandler(table1_DragOver);

            ToolTip tt = new ToolTip();
            tt.SetToolTip(table1, "Tooltip");

            DataGridView table2 = new DataGridView();
            table2.Name = "TestPage.Tables.Table02";
            table2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table2.ColumnHeadersVisible = false;
            table2.RowHeadersVisible = false;
            table2.AllowUserToAddRows = false;
            table2.AllowUserToDeleteRows = false;
            table2.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
            table2.ColumnCount = 4;
            

            foreach (string[] row in _alphaEntries)
            {
                table2.Rows.Add(row);
            }

            table2.AutoSize = true;
            table2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;


            DataGridView table3 = new DataGridView();
            table3.Name = "TestPage.Tables.Table03";
            table3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table3.RowHeadersVisible = false;
            table3.AllowUserToAddRows = false;
            table3.AllowUserToDeleteRows = false;
            table3.EditMode = DataGridViewEditMode.EditProgrammatically;
            table3.ColumnCount = 4;

            for (int i = 0; i < table3.ColumnCount; i++)
            {
                table3.Columns[i].Name = _columnNames[i];
            }

            foreach (string[] row in _valueEntries)
            {
                table3.Rows.Add(row);
            }

            table3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            table3.Size = new Size(90, 60);

            DataGridView table4 = new DataGridView();
            table4.Name = "TestPage.Tables.Table04";
            table4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table4.RowHeadersVisible = false;
            table4.AllowUserToAddRows = false;
            table4.AllowUserToDeleteRows = false;
            table4.EditMode = DataGridViewEditMode.EditProgrammatically;
            table4.ColumnCount = 21;
            table4.RowCount = 20;

            char letter = 'A';
            for (int i = 0; i < table4.ColumnCount - 1; i++)
            {
                for (int j = 0; j < table4.RowCount; j++)
                {
                    table4.Rows[j].Cells[i].Value = 
                        Convert.ToString(letter) + Convert.ToString(j + 1);
                }

                table4.Columns[i].Name = Convert.ToString(letter);
                letter++;
            }

            DataGridViewColumn rightAlignedColumn = 
                table4.Columns[table4.ColumnCount - 1];
            rightAlignedColumn.Name = "right align";

            for (int i = 0; i < table4.RowCount; i++ )
            {
                DataGridViewCell cell = 
                    table4.Rows[i].Cells[table4.ColumnCount - 1];
                cell.Value = "rl" + Convert.ToString(i + 1);
                cell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }


            DataGridView table5 = new DataGridView();
            table5.Name = "TestPage.Tables.Table05";
            table5.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table5.RowHeadersVisible = false;
            table5.AllowUserToAddRows = false;
            table5.AllowUserToDeleteRows = false;

            DataGridViewCheckBoxColumn colCB = new DataGridViewCheckBoxColumn();
            colCB.Name = "Column1";
            table5.Columns.Add(colCB);

            table5.ColumnCount = 4;
            table5.RowCount = 2;

            //value fill for table 5
            DataGridViewCell check1 = table5.Rows[0].Cells[0];
            check1.Value = true;
            

            DataGridViewCell check2 = table5.Rows[1].Cells[0];
            check2.Value = true;

            DataGridViewCell valueCell = table5.Rows[0].Cells[1];
            valueCell.Value = "value2";
            valueCell = table5.Rows[0].Cells[2];
            valueCell.Value = "3";
            valueCell = table5.Rows[0].Cells[3];
            valueCell.Value = "true";
            valueCell = table5.Rows[1].Cells[1];
            valueCell.Value = "value5";
            valueCell = table5.Rows[1].Cells[2];
            valueCell.Value = "6";
            valueCell = table5.Rows[1].Cells[3];
            valueCell.Value = "false";
            
            



            

                table5.AutoSize = true;
            table5.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            table5.RowHeadersWidthSizeMode =
                DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            table5.Columns[1].HeaderText = "Column2";
            table5.Columns[2].HeaderText = "Column3";
            table5.Columns[3].HeaderText = "Column4";

            DataGridView table6 = new DataGridView();
            table6.Name = "TestPage.Tables.Table05";
            table6.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table6.RowHeadersVisible = false;
            table6.AllowUserToAddRows = false;
            table6.AllowUserToDeleteRows = false;

            DataGridViewCheckBoxColumn checkboxcolumn = new DataGridViewCheckBoxColumn();
            table6.Columns.Add(checkboxcolumn);

            table6.ColumnCount = 1;
            table6.RowCount = 4;

            //value fill for table 5
                        
            table6.AutoSize = true;
            table6.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            table6.RowHeadersWidthSizeMode =
                DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            
            panel.Controls.Add(table1);
            panel.Controls.Add(table2);
            panel.Controls.Add(table3);
            panel.Controls.Add(table4);

            panel.Controls.Add(table5);
            panel.Controls.Add(table6);

            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }        

        void table1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move; 
        }

        void table1_DragDrop(object sender, DragEventArgs e)
        {
            DataGridView table = (DataGridView)sender;

            Point dscreen = new Point(e.X, e.Y);
            Point dclient = table.PointToClient(dscreen);
            DataGridView.HitTestInfo hitTest = table.HitTest(dclient.X, dclient.Y);
            DataGridViewCell dropCell = table.Rows[hitTest.RowIndex].Cells[hitTest.ColumnIndex];

            AbstractTestForm.setText("From " +
                e.Data.GetData(DataFormats.Text).ToString() + " to " + dropCell.Value);            
        }

        void table1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                try
                {
                    DataGridViewCell dragCell = table1.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    table1.DoDragDrop(dragCell.Value, DragDropEffects.Move);
                }
                catch (Exception execption)
                {
                    //do nothing
                }                
            }    
        }

        void table1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Right))
            {
                table1.ContextMenu.Show(table1, new Point(e.X, e.Y)); 
            }            
        }
    }
}
