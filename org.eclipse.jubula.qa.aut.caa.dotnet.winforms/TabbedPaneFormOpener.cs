﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class TabbedPaneFormOpener
    {

        public TabbedPaneFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "TabbedPanes";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            TabControl tabControl1 = new TabControl();
            
            // use fixed-size tabs in order to make this component more testable 
            // with regards to opening a context menu at specific coordinates
            // in the tab control (see http://bugzilla.bredex.de/469)
            tabControl1.SizeMode = TabSizeMode.Fixed;
            tabControl1.ItemSize = new Size(65, tabControl1.ItemSize.Height);

            tabControl1.Name = "TestPages.TabbedPanes.TabbedPane01";
            tabControl1.TabPages.AddRange(createTabPages());
            tabControl1.ContextMenu = ContextMenuCreator.createContextMenu();

            TabControl tabControl2 = new TabControl();
            tabControl2.Name = "TestPages.TabbedPanes.TabbedPane02";
            tabControl2.Alignment = TabAlignment.Left;
            tabControl2.TabPages.AddRange(createTabPages());

            TabControl tabControl3 = new TabControl();
            tabControl3.Name = "TestPages.TabbedPanes.TabbedPane03";
            TabPage[] tabPages3 = new TabPage[50];
            for (int i = 0; i < tabPages3.Length; i++)
            {
                Label label = new Label();
                label.Text = "Carrot";
                tabPages3[i] = new TabPage();
                tabPages3[i].Text = Convert.ToString(i);
                tabPages3[i].Controls.Add(label);
            }
            tabControl3.TabPages.AddRange(tabPages3);


            panel.Controls.Add(tabControl1);
            panel.Controls.Add(tabControl2);
            panel.Controls.Add(tabControl3);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        private TabPage[] createTabPages()
        {
            string titleTab1 = "123";
            string titleTab2 = "abcde";
            string titleTab3 = "aBcDe";

            Label contentTab1 = new Label();
            contentTab1.Text = "Carrot";

            Label contentTab2 = new Label();
            contentTab2.Text = "Banana";
            
            Label contentTab3 = new Label();
            contentTab3.Text = "Kiwi";

            TabPage[] pages = new TabPage[3];

            pages[0] = new TabPage();
            pages[0].Text = titleTab1;
            pages[0].Controls.Add(contentTab1);

            pages[1] = new TabPage();
            pages[1].Text = titleTab2;
            pages[1].Controls.Add(contentTab2);

            pages[2] = new TabPage();
            pages[2].Text = titleTab3;
            pages[2].Controls.Add(contentTab3);

            return pages;
        }

    }
}
