﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class CheckBoxFormOpener
    {


        public CheckBoxFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "CheckBoxes";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            CheckBox cb1 = new CheckBox();
            cb1.Text = "CheckBox";
            cb1.Name = "TestPage.CheckBoxes.CheckBox00";
            cb1.UseVisualStyleBackColor = true;
            cb1.ContextMenu = ContextMenuCreator.createContextMenu();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(cb1, "Tooltip");

            CheckBox cb2 = new CheckBox();
            cb2.Text = "CheckBox1";
            cb2.Name = "TestPage.CheckBoxes.CheckBox01";
            cb2.UseVisualStyleBackColor = true;
            cb2.Enabled = false;

            panel.Controls.Add(cb1);
            panel.Controls.Add(cb2);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }
    }
}
