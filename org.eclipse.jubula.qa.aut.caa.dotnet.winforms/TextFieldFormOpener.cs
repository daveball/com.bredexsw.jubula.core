﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class TextFieldFormOpener
    {
        private TextBox tb3;

        public TextFieldFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "TextFields";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();
            
            TextBox tb1 = new TextBox();
            tb1.Name = "TestPage.TextField.TextField01";
            tb1.ContextMenu = ContextMenuCreator.createContextMenu();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(tb1, "Tooltip");

            TextBox tb2 = new TextBox();
            tb2.Name = "TestPage.TextField.TextField02";
            tb2.Enabled = false;

            tb3 = new TextBox();
            tb3.Name = "TestPage.TextField.TextField03";
            tb3.MouseDown += new MouseEventHandler(tb3_MouseDown);
            
            TextBox tb4 = new TextBox();
            tb4.Name = "TestPage.TextField.TextField04";
            tb4.Text = "abcde";
            tb4.Enabled = false;

            TextBox tb5 = new TextBox();
            tb5.Name = "TestPage.TextField.TextField05";
            tb5.Text = "abcde";
            tb5.ReadOnly = true;
            
            panel.Controls.Add(tb1);
            panel.Controls.Add(tb2);
            panel.Controls.Add(tb3);
            panel.Controls.Add(tb4);
            panel.Controls.Add(tb5);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        void tb3_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (Control.ModifierKeys == Keys.Shift)
                tb.DoDragDrop("SHIFT + " + e.Button + " " + tb.Text, DragDropEffects.Copy);
            else if (Control.ModifierKeys == Keys.Alt)
                tb.DoDragDrop("ALT + " + e.Button + " " + tb.Text, DragDropEffects.Copy);
            else if (Control.ModifierKeys == Keys.Control)
                tb.DoDragDrop("CTRL + " + e.Button + " " + tb.Text, DragDropEffects.Copy);
            else
                tb.DoDragDrop(e.Button + " " + tb.Text, DragDropEffects.Copy);

            
        }

    }
}
