﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class LabelFormOpener
    {
        public Label l3;

        public LabelFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Labels";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            Label l1 = new Label();
            l1.Name = "TestPage.Labels.Label01";
            l1.Text = "Label1";
            l1.ContextMenu = ContextMenuCreator.createContextMenu();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(l1, "Tooltip");

            Label l2 = new Label();
            l2.Name = "TestPage.Labels.Label02";
            l2.Text = "Label2";
            l2.Enabled = false;

            l3 = new Label();
            l3.Name = "TestPage.Labels.Label03";
            l3.Text = "Label3";
            l3.MouseClick += new MouseEventHandler(mous);
            l3.MouseDoubleClick += new MouseEventHandler(MouseDoubleClick);

            panel.Controls.Add(l1);
            panel.Controls.Add(l2);
            panel.Controls.Add(l3);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        void MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (sender as Label).Text = "TwoClick";
        }

        void mous(object sender, MouseEventArgs e)
        {
            (sender as Label).Text = "OneClick";        
        }
               
    }
}
