﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class TextAreaFormOpener
    {


        public TextAreaFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "TextAreas";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            RichTextBox rtb1 = new RichTextBox();
            rtb1.Name = "TestPage.TextArea.TextArea01";
            rtb1.ContextMenu = ContextMenuCreator.createContextMenu();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(rtb1, "Tooltip");

            RichTextBox rtb2 = new RichTextBox();
            rtb2.Name = "TestPage.TextArea.TextArea02";
            rtb2.Enabled = false;

            RichTextBox rtb3 = new RichTextBox();
            rtb3.Name = "TestPage.TextArea.TextArea03";

            RichTextBox rtb4 = new RichTextBox();
            rtb4.Name = "TestPage.TextArea.TextArea04";
            rtb4.Text = "abcde";
            rtb4.Enabled = false;

            RichTextBox rtb5 = new RichTextBox();
            rtb5.Name = "TestPage.TextArea.TextArea05";
            rtb5.Text = "abcde";
            rtb5.ReadOnly = true;

            panel.Controls.Add(rtb1);
            panel.Controls.Add(rtb2);
            panel.Controls.Add(rtb3);
            panel.Controls.Add(rtb4);
            panel.Controls.Add(rtb5);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }
    }
}
