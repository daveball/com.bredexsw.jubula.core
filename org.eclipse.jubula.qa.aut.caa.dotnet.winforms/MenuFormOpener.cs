﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class MenuFormOpener
    {
        private MenuItem checkItem;
        private MenuItem radioItem;

        public MenuFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Menus";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            MainMenu mainMenu = new MainMenu();
            MenuItem firstMenuItem = new MenuItem("Menu");
            firstMenuItem.Click += new EventHandler(menu_click);          

            MenuItem firstItem = new MenuItem("FirstItem");
            firstItem.Click += new EventHandler(menu_click);
            MenuItem moreItem = new MenuItem("More");
            moreItem.Click += new EventHandler(menu_click);
            MenuItem secondItem = new MenuItem("SecondItem");
            secondItem.Click += new EventHandler(menu_click);
            checkItem = new MenuItem("CheckboxItem");
            checkItem.Click+=new EventHandler(menu_click);
            radioItem = new MenuItem("RadioBoxItem");
            radioItem.Click += new EventHandler(menu_click);
            radioItem.RadioCheck = true;
            MenuItem thirdItem = new MenuItem("ThirdItem");
            thirdItem.Enabled = false;
            thirdItem.Click += new EventHandler(menu_click);
            MenuItem fourthItem = new MenuItem("FourthItem");
            fourthItem.Enabled = false;
            fourthItem.Click += new EventHandler(menu_click);

            MenuItem moreLevel2 = new MenuItem("More");
            MenuItem selectableLevel3 = new MenuItem("Selectable Level 3");
            selectableLevel3.Click += new EventHandler(menu_click);
            moreLevel2.MenuItems.Add(selectableLevel3);

            MenuItem moreLevel3 = new MenuItem("More");
            MenuItem selectableLevel4 = new MenuItem("Selectable Level 4");
            selectableLevel4.Click += new EventHandler(menu_click);
            moreLevel3.MenuItems.Add(selectableLevel4);

            moreLevel2.MenuItems.Add(moreLevel3);

            firstMenuItem.MenuItems.Add(firstItem);
            firstMenuItem.MenuItems.Add(moreItem);
            moreItem.MenuItems.Add(secondItem);
            moreItem.MenuItems.Add(checkItem);
            moreItem.MenuItems.Add(radioItem);
            moreItem.MenuItems.Add(thirdItem);
            moreItem.MenuItems.Add(moreLevel2);
            mainMenu.MenuItems.Add(firstMenuItem);

            form.Menu = mainMenu;
            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();

        }

        void menu_click(object sender, EventArgs e)
        {
            var clickedItem = sender as MenuItem;
            AbstractTestForm.setText(clickedItem.Text);

            if(clickedItem.Text.Equals("CheckItem"))
            {
                setCheck(checkItem);
            }
            else if (clickedItem.Text.Equals("RadioItem"))
            {
                setCheck(radioItem);
            }
        }

        private void setCheck(MenuItem item)
        {
            if (item.Checked)
                item.Checked = false;
            else
                item.Checked = true;
        }
    }
}
