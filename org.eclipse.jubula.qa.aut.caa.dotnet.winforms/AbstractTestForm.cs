﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    public partial class AbstractTestForm : Form
    {
        public static bool buttonClose = false;

        public AbstractTestForm()
        {
           
        }

        private void close_Click(object sender, EventArgs e)
        {
            AbstractTestForm.buttonClose = true;
            StartdotnetAUT.autStart();
            Close();
        }

        private void AbstractTestForm_Load(object sender, EventArgs e)
        {

        }
        
        public static void form_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!buttonClose)
            StartdotnetAUT.realClose();
        }
    }
}
