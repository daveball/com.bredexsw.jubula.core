﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class RadioButtonFormOpener
    {


        public RadioButtonFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "RadioButtons";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            RadioButton rb1 = new RadioButton();
            rb1.Size = new Size(90, 23);
            rb1.Text = "RadioButton";
            rb1.Name = "TestPage.RadioButtons.RadioButton00";
            rb1.UseVisualStyleBackColor = true;
            rb1.ContextMenu = ContextMenuCreator.createContextMenu();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(rb1, "Tooltip");

            RadioButton rb2 = new RadioButton();
            rb2.Size = new Size(90, 23);
            rb2.Text = "RadioButton1";
            rb2.Name = "TestPage.RadioButtons.RadioButton01";
            rb2.UseVisualStyleBackColor = true;
            rb2.Checked = true;

            RadioButton rb3 = new RadioButton();
            rb3.Size = new Size(90, 23);
            rb3.Text = "RadioButton2";
            rb3.Name = "TestPage.RadioButtons.RadioButton02";
            rb3.UseVisualStyleBackColor = true;
            rb3.Enabled = false;

            panel.Controls.Add(rb1);
            panel.Controls.Add(rb2);
            panel.Controls.Add(rb3);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }
    }
}
