﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class ContextMenuCreator
    {

        public static ContextMenu createContextMenu()
        {

            ContextMenu contextMenu = new ContextMenu();
            
            MenuItem firstItem = new MenuItem("FirstItem");
            firstItem.Click += new EventHandler(menu_click);
            
            MenuItem moreItem = new MenuItem("More");

            MenuItem secondItem = new MenuItem("SecondItem");
            secondItem.Click += new EventHandler(menu_click);

            MenuItem checkItem = new MenuItem("CheckboxItem");
            checkItem.Click += new EventHandler(toggle_checkmark);
            
            MenuItem radioItem = new MenuItem("RadioBoxItem");
            radioItem.Click += new EventHandler(toggle_checkmark);
            radioItem.RadioCheck = true;
            
            MenuItem thirdItem = new MenuItem("ThirdItem");
            thirdItem.Enabled = false;

            MenuItem moreLevel2 = new MenuItem("More");
            MenuItem selectableLevel3 = new MenuItem("Selectable Level 3");
            selectableLevel3.Click += new EventHandler(menu_click);
            moreLevel2.MenuItems.Add(selectableLevel3);

            MenuItem moreLevel3 = new MenuItem("More");
            MenuItem selectableLevel4 = new MenuItem("Selectable Level 4");
            selectableLevel4.Click += new EventHandler(menu_click);
            moreLevel3.MenuItems.Add(selectableLevel4);

            moreLevel2.MenuItems.Add(moreLevel3);

            moreItem.MenuItems.Add(secondItem);
            moreItem.MenuItems.Add(checkItem);
            moreItem.MenuItems.Add(radioItem);
            moreItem.MenuItems.Add("-");
            moreItem.MenuItems.Add(thirdItem);
            moreItem.MenuItems.Add(moreLevel2);
            contextMenu.MenuItems.Add(firstItem);
            contextMenu.MenuItems.Add(moreItem);

            return contextMenu;
        }        

        private static void toggle_checkmark(object sender, EventArgs e)
        {
            var clickedItem = sender as MenuItem;
            AbstractTestForm.setText(clickedItem.Text);
            clickedItem.Checked = !clickedItem.Checked;
        }

        private static void menu_click(object sender, EventArgs e)
        {
            var clickedItem = sender as MenuItem;
            AbstractTestForm.setText(clickedItem.Text);
        }

    }
}
