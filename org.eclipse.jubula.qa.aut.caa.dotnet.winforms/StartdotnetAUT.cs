﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    static class StartdotnetAUT
    {

        private static StartForm startForm;

        public static void autStart()
        {
            startForm.Show();
        }

        public static void autClose()
        {
            startForm.Hide();
        }

        public static void realClose()
        {
            startForm.Close();
        }
                
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            startForm = new StartForm();
            Application.Run(startForm);
        }
    }
}
