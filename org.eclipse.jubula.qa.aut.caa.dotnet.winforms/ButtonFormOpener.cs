﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class ButtonFormOpener
    {
        

        public ButtonFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Buttons";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();            

            Button button1 = new Button();
            button1.Text = "Button";
            button1.Name = "TestPage.Buttons.Button01";
            button1.UseVisualStyleBackColor = true;
            button1.ContextMenu = ContextMenuCreator.createContextMenu();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(button1,"Tooltip");

            Button button2 = new Button();
            button2.Text = "Button1";
            button2.Name = "TestPage.Buttons.Button02";
            button2.UseVisualStyleBackColor = true;
            button2.Enabled = false;

            panel.Controls.Add(button1);
            panel.Controls.Add(button2);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }        
    }
}
