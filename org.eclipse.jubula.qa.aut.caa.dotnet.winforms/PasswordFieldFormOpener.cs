﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class PasswordFieldFormOpener
    {

        public PasswordFieldFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "PasswordField";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            char passwordChar = '*';

            TextBox passwordField1 = new TextBox();
            passwordField1.Name = "TestPage.PasswordFields.PasswordField01";
            passwordField1.PasswordChar = passwordChar;
            passwordField1.ContextMenu = ContextMenuCreator.createContextMenu();

            TextBox passwordField2 = new TextBox();
            passwordField2.Name = "TestPage.PasswordFields.PasswordField02";
            passwordField2.PasswordChar = passwordChar;
            passwordField2.Enabled = false;

            TextBox passwordField3 = new TextBox();
            passwordField3.Name = "TestPage.PasswordFields.PasswordField03";
            passwordField3.PasswordChar = passwordChar;
            passwordField3.Text = "text1";

            TextBox passwordField4 = new TextBox();
            passwordField4.Name = "TestPage.PasswordFields.PasswordField04";
            passwordField4.PasswordChar = passwordChar;
            passwordField4.Enabled = false;
            passwordField4.Text = "text2";

            TextBox passwordField5 = new TextBox();
            passwordField5.Name = "TestPage.PasswordFields.PasswordField05";
            passwordField5.PasswordChar = passwordChar;
            passwordField5.ReadOnly = true;
            passwordField5.Text = "text3";

            panel.Controls.Add(passwordField1);
            panel.Controls.Add(passwordField2);
            panel.Controls.Add(passwordField3);
            panel.Controls.Add(passwordField4);
            panel.Controls.Add(passwordField5);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        private TabPage[] createTabPages()
        {
            string titleTab1 = "123";
            string titleTab2 = "abcde";
            string titleTab3 = "aBcDe";

            Label contentTab1 = new Label();
            contentTab1.Text = "Carrot";

            Label contentTab2 = new Label();
            contentTab2.Text = "Banana";
            
            Label contentTab3 = new Label();
            contentTab3.Text = "Kiwi";

            TabPage[] pages = new TabPage[3];

            pages[0] = new TabPage();
            pages[0].Text = titleTab1;
            pages[0].Controls.Add(contentTab1);

            pages[1] = new TabPage();
            pages[1].Text = titleTab2;
            pages[1].Controls.Add(contentTab2);

            pages[2] = new TabPage();
            pages[2].Text = titleTab3;
            pages[2].Controls.Add(contentTab3);

            return pages;
        }

    }
}
