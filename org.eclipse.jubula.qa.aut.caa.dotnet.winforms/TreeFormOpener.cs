﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class TreeFormOpener
    {
        private List<TreeNode> infiniteChildren = new List<TreeNode>();
        private TreeView tree1;
        private bool aBcDeNodeExpanded = false;
        private bool a_B_c_D_eNodeExpanded = false;

        public TreeFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Trees";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            tree1 = new TreeView();
            tree1.Name = "TestPage.Trees.Tree01";
            tree1.Nodes.Add(createTreeModel());
            tree1.Nodes[0].Expand();
            tree1.ContextMenu = ContextMenuCreator.createContextMenu();
            //d&d code
            tree1.AllowDrop = true;
            tree1.ItemDrag += new ItemDragEventHandler(tree1_ItemDrag);
            tree1.DragDrop +=new DragEventHandler(tree1_DragDrop);
            tree1.DragOver +=new DragEventHandler(tree1_DragOver);

            TreeView dynamicTree = new TreeView();
            dynamicTree.Name = "TestPage.Trees.DynamicTree";
            dynamicTree.Nodes.Add(createDynamicTreeModel());
            dynamicTree.Nodes[0].Expand();

            dynamicTree.BeforeExpand += new TreeViewCancelEventHandler(dynamicTree_BeforeExpand);            
            dynamicTree.ContextMenu = ContextMenuCreator.createContextMenu();


            TreeView tree2 = new TreeView();
            tree2.Name = "TestPage.Trees.Tree02";
            tree2.Nodes.Add(createTreeModel().Nodes[1]);
            tree2.Enabled = false;
            tree2.Nodes[0].Expand();

            TreeView tree3 = new TreeView();
            tree3.Name = "TestPage.Trees.Tree03";
            tree3.Nodes.Add(createTreeModel());
            tree3.Nodes[0].Expand();
            tree3.Size = new Size(80, 80);

            TreeView tree4 = new TreeView();
            tree4.Name = "TestPage.Trees.Tree04";
            InitInfiniteTree(tree4);

            TreeView tree5 = new TreeView();
            tree5.Name = "TestPage.Trees.Tree05";
            tree5.Nodes.Add(createLongTreeModel());
            tree5.Nodes[0].Expand();
            tree5.Size = new Size(80, 80);

            panel.Controls.Add(tree1);
            panel.Controls.Add(dynamicTree);
            panel.Controls.Add(tree2);
            panel.Controls.Add(tree3);
            panel.Controls.Add(tree4);
            panel.Controls.Add(tree5);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        private void InitInfiniteTree(TreeView tree4)
        {
            TreeNode root = new TreeNode("root");
            infiniteChildren.Add(root);
            tree4.Nodes.Add(root);
            root.Nodes.Add("staticChild");
            tree4.BeforeExpand += new TreeViewCancelEventHandler(tree_BeforeExpand);
        }


        void tree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode child1 = new TreeNode("dynamicChild");
            TreeNode child2 = new TreeNode("dynamicChild");
            infiniteChildren.ElementAt(infiniteChildren.Count - 1).Nodes.Add(child1);
            child1.Nodes.Add(child2);
            infiniteChildren.Add(child1);
            infiniteChildren.Add(child2);
        }

        void dynamicTree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode expandedNode = e.Node;
            
            if (expandedNode.Text.Equals("aBcDe") && !aBcDeNodeExpanded)
            {
                expandedNode.Nodes.RemoveAt(0);
                TreeNode node1 = new TreeNode("a B c D e");
                TreeNode node11 = new TreeNode("platzhalter");
                TreeNode node2 = new TreeNode("a1b2c3");

                node1.Nodes.Add(node11);
                expandedNode.Nodes.Add(node1);
                expandedNode.Nodes.Add(node2);
                aBcDeNodeExpanded = true;
            }
            else if (expandedNode.Text.Equals("a B c D e") && !a_B_c_D_eNodeExpanded)
            {
                expandedNode.Nodes.RemoveAt(0);
                TreeNode node1 = new TreeNode("1234");                
                TreeNode node2 = new TreeNode("abcde");
                
                expandedNode.Nodes.Add(node1);
                expandedNode.Nodes.Add(node2);
                a_B_c_D_eNodeExpanded = true;
            }

        }

        void tree1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move; 
        }

        void tree1_DragDrop(object sender, DragEventArgs e)
        {
            Point mousePos = tree1.PointToClient(new Point(e.X, e.Y));
            TreeNode targetNode = tree1.GetNodeAt(mousePos);

            AbstractTestForm.setText("From " + e.Data.GetData(DataFormats.Text).ToString() +
                " to " + targetNode.Text);
        }

        void tree1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode startNode = (TreeNode) e.Item;

            tree1.DoDragDrop(startNode.Text, DragDropEffects.Move); 
        }

        private TreeNode createDynamicTreeModel()
        {
            TreeNode rootNode = new TreeNode();
            TreeNode node1 = new TreeNode("abcde");
            TreeNode node2 = new TreeNode("aBcDe");
            TreeNode node21 = new TreeNode("platzhalter");            
            TreeNode node3 = new TreeNode("()+*\\./*+()");
            TreeNode node31 = new TreeNode("");
            TreeNode node4 = new TreeNode("1234");

            rootNode.Nodes.Add(node1);
            rootNode.Nodes.Add(node2);
            rootNode.Nodes.Add(node3);
            rootNode.Nodes.Add(node4);

            node2.Nodes.Add(node21);
            node3.Nodes.Add(node31);

            return rootNode;
        }
 
        private TreeNode createTreeModel()
        {
            TreeNode rootNode = new TreeNode();
            TreeNode node1 = new TreeNode("abcde");
            TreeNode node2 = new TreeNode("aBcDe");
            TreeNode node21 = new TreeNode("a B c D e");
            TreeNode node211 = new TreeNode("1234");
            TreeNode node212 = new TreeNode("abcde");
            TreeNode node22 = new TreeNode("a1b2c3");
            TreeNode node3 = new TreeNode("()+*\\./*+()");
            TreeNode node31 = new TreeNode("");
            TreeNode node4 = new TreeNode("1234");

            rootNode.Nodes.Add(node1);
            rootNode.Nodes.Add(node2);
            rootNode.Nodes.Add(node3);
            rootNode.Nodes.Add(node4);

            node2.Nodes.Add(node21);
            node2.Nodes.Add(node22);

            node3.Nodes.Add(node31);

            node21.Nodes.Add(node211);
            node21.Nodes.Add(node212);

            return rootNode;
        }

        private TreeNode createLongTreeModel()
        {
            string longName = "Really long name";
            string shortName = "Short";

            TreeNode rootNode = new TreeNode();
            TreeNode node1 = new TreeNode(longName);
            TreeNode node2 = new TreeNode(longName);
            TreeNode node3 = new TreeNode(shortName);
            TreeNode node4 = new TreeNode(shortName);

            TreeNode node11 = new TreeNode(shortName);
            TreeNode node12 = new TreeNode(shortName);
            TreeNode node21 = new TreeNode(shortName);
            TreeNode node22 = new TreeNode(longName);
            TreeNode node31 = new TreeNode(shortName);
            TreeNode node32 = new TreeNode(shortName);
            TreeNode node41 = new TreeNode(longName);
            TreeNode node42 = new TreeNode(longName);

            rootNode.Nodes.Add(node1);
            rootNode.Nodes.Add(node2);
            rootNode.Nodes.Add(node3);
            rootNode.Nodes.Add(node4);

            node2.Nodes.Add(node21);
            node2.Nodes.Add(node22);

            node3.Nodes.Add(node31);
            node3.Nodes.Add(node32);

            node4.Nodes.Add(node41);
            node4.Nodes.Add(node42);

            return rootNode;
        }

    }
}
