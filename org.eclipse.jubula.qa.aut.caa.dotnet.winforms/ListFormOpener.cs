﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using org.eclipse.jubula.qa.aut.caa.dotnet.winFormsCaA;

namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    class ListFormOpener
    {
        /// <summary>
        /// Array of standard list entries.
        /// </summary>
        static string[] _entries = new string[]
        {
	        "",
	        "abcde",
	        "aBcDe",
	        "a B c D e",
            "a1b2c3",
            "()+*\\./*+()",
            "1234"
        };

        public ListFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "Lists";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();

            ListBox list1 = new ListBox();
            list1.Name = "TestPage.Lists.List01";
            list1.SelectionMode = SelectionMode.One;
            list1.Items.AddRange(_entries);
            list1.ContextMenu = ContextMenuCreator.createContextMenu();
            list1.AllowDrop = true;
            list1.MouseDown += new MouseEventHandler(list3_MouseDown);
            list1.DragDrop += new DragEventHandler(list3_DragDrop);
            list1.DragOver += new DragEventHandler(list3_DragOver);
            
            ToolTip tt = new ToolTip();
            tt.SetToolTip(list1, "Tooltip");

            ListBox list2 = new ListBox();
            list2.Name = "TestPage.Lists.List02";
            list2.Enabled = false;
            list2.Items.AddRange(_entries);

            ListBox list3 = new ListBox();
            list3.Name = "TestPage.Lists.List03";
            list3.SelectionMode = SelectionMode.MultiExtended;
            list3.Items.AddRange(_entries);
            list3.ContextMenu = ContextMenuCreator.createContextMenu();
            list3.AllowDrop = true;
            list3.MouseDown += new MouseEventHandler(list3_MouseDown);
            list3.DragDrop += new DragEventHandler(list3_DragDrop);
            list3.DragOver += new DragEventHandler(list3_DragOver);

            ListBox list4 = new ListBox();
            list4.Name = "TestPage.Lists.List04";
            list4.SelectionMode = SelectionMode.MultiExtended;
            list4.Items.AddRange(_entries);

            ListBox list5 = new ListBox();
            list5.Name = "TestPage.Lists.List05";
            list5.SelectionMode = SelectionMode.MultiExtended;
            for (int i = 1; i < 201; i++) {
                list5.Items.Add(Convert.ToString(i));
            }          
            list5.MouseDown += new MouseEventHandler(list5_MouseDown);

            panel.Controls.Add(list1);
            panel.Controls.Add(list2);
            panel.Controls.Add(list3);
            panel.Controls.Add(list4);
            panel.Controls.Add(list5);
            panel.AutoSize = true;
            panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel.FlowDirection = FlowDirection.TopDown;

            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        void list5_MouseDown(object sender, MouseEventArgs e)
        {
            AbstractTestForm.setText(e.Clicks.ToString() + " Click");
        }       

        void list3_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move; 
        }

        void list3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                ListBox lb = (ListBox)sender;

                if (lb.SelectedItem == null)
                    return;

                lb.DoDragDrop(lb.SelectedItem.ToString(), DragDropEffects.Move);             
            }            
        }

        void list3_DragDrop(object sender, DragEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            Point point = lb.PointToClient(new Point(e.X, e.Y));
            int index = lb.IndexFromPoint(point);

            AbstractTestForm.setText("From " + 
                e.Data.GetData(DataFormats.Text).ToString() + " to " + lb.Items[index].ToString());            
        }

        ListViewItem[] createListViewItems()
        {
            ListViewItem[] items = new ListViewItem[_entries.Length];
            for (int i = 0; i < _entries.Length; i++) {
                items[i] = new ListViewItem(_entries[i]);
            }
            
            return items;
        }        
    }
}
