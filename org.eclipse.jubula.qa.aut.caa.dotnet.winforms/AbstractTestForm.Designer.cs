﻿
using System.Windows.Forms;
namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    partial class AbstractTestForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
                       
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.close = new System.Windows.Forms.Button();

            // 
            // close
            // 
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 0;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            ///
            /// action text
            /// 
            actionText = new System.Windows.Forms.Label();
            actionText.Text = "Label reflecting invoked UI action.";
            actionText.AutoSize = true;
            actionText.Name = "TestPage.AbstractTestForm.ActionLabel01";
            actionText.AllowDrop = true;
            actionText.DragEnter += new DragEventHandler(actionText_DragEnter);
            actionText.DragDrop += new DragEventHandler(actionText_DragDrop);
            // 
            // AbstractTestForm
            // 
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.AbstractTestForm_Load);      
      
            formpanel = new System.Windows.Forms.FlowLayoutPanel();
            formpanel.Controls.Add(this.contentPanel);
            formpanel.Controls.Add(actionText);
            formpanel.Controls.Add(this.close);
            formpanel.AutoSize = true;
            formpanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            formpanel.FlowDirection = FlowDirection.TopDown;
            formpanel.Name = "close_panel";
            this.AutoScroll = true;

            this.Controls.Add(this.formpanel);
        }

        void actionText_DragDrop(object sender, DragEventArgs e)
        {
            actionText.Text = e.Data.GetData(DataFormats.Text).ToString();
        }

        void actionText_DragEnter(object droper, DragEventArgs drop)
        {
            if (drop.Data.GetDataPresent(DataFormats.Text)) {
                drop.Effect = DragDropEffects.Copy;
            }
            else
            {
                drop.Effect = DragDropEffects.None;
            }
        }

        public void setPanel(System.Windows.Forms.FlowLayoutPanel panel)
        {
            this.contentPanel = panel;
            this.InitializeComponent();
        }

        public static void setText(string text)
        {
            actionText.Text = text;
        }

        #endregion

        public static  System.Windows.Forms.Label actionText;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.FlowLayoutPanel contentPanel;
        private System.Windows.Forms.FlowLayoutPanel formpanel;
    }
}