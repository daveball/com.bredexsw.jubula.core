﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace org.eclipse.jubula.qa.aut.caa.dotnet.winFormsCaA
{
    class ScrolledCompositeFormOpener
    {
        private RichTextBox m_textArea;
        
        public ScrolledCompositeFormOpener()
        {
            AbstractTestForm form = new AbstractTestForm();
            form.Name = "ScrollComposite";
            form.Text = form.Name;
            FlowLayoutPanel panel = new FlowLayoutPanel();
            panel.AutoSize = true;

            m_textArea = new RichTextBox();
            m_textArea.MinimumSize = new Size(300, 300);
            m_textArea.Multiline = true;
            m_textArea.Name = "ActionLog";

            Panel scrolledPanel1 = createPanel(1);
            scrolledPanel1.MaximumSize = new Size(100, 100);

            Panel scrolledPanel2 = createPanel(2);
            scrolledPanel2.MaximumSize = new Size(100, 100);
            scrolledPanel2.VerticalScroll.Value = scrolledPanel2.VerticalScroll.Maximum;
            scrolledPanel2.HorizontalScroll.Value = scrolledPanel2.HorizontalScroll.Maximum;

            Panel scrolledPanel31 = createPanel(3);
            scrolledPanel31.MaximumSize = new Size(100, 100);
            scrolledPanel31.VerticalScroll.Value = scrolledPanel31.VerticalScroll.Maximum;
            scrolledPanel31.HorizontalScroll.Value = scrolledPanel31.HorizontalScroll.Maximum;
            FlowLayoutPanel scrolledPanel3 = new FlowLayoutPanel();
            scrolledPanel3.AutoScroll = true;
            scrolledPanel3.MaximumSize = new Size(100, 100);
            scrolledPanel3.Controls.Add(scrolledPanel31);

            panel.Controls.Add(scrolledPanel1);
            panel.Controls.Add(scrolledPanel2);
            panel.Controls.Add(scrolledPanel3);
            panel.Controls.Add(m_textArea);
            form.FormClosed += new FormClosedEventHandler(AbstractTestForm.form_FormClosed);
            AbstractTestForm.buttonClose = false;
            form.setPanel(panel);
            form.Show();
        }

        private Panel createPanel(int number)
        {
            FlowLayoutPanel panel = new FlowLayoutPanel();

            String buttonName = "Button" + number;
            Button button = new Button();
            button.Name = buttonName;
            button.Text = buttonName;
            button.MinimumSize = new Size(300, 25);
            button.Click += new EventHandler(control_Click);

            String textFieldName = "Textfield" + number;
            TextBox textField = new TextBox();
            textField.Name = textFieldName;
            textField.Text = textFieldName;
            textField.MinimumSize = new Size(300, 25);
            textField.Click += new EventHandler(control_Click);

            Panel emptyPanel = new Panel();
            emptyPanel.MinimumSize = new Size(300, 200);

            panel.Controls.Add(button);
            panel.Controls.Add(textField);
            panel.Controls.Add(emptyPanel);

            panel.AutoScroll = true;

            return panel;
        }

        private void control_Click(object sender, EventArgs e)
        {
            Control source = sender as Control;
            if (source != null)
            {
                m_textArea.AppendText(DateTime.Now.Ticks + " " + source.Name + Environment.NewLine);
            }
        }
    }
}
