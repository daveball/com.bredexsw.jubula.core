﻿namespace org.eclipse.jubula.qa.aut.caa.dotnet
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttons = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.menu = new System.Windows.Forms.Button();
            this.checkboxes = new System.Windows.Forms.Button();
            this.radioButtons = new System.Windows.Forms.Button();
            this.tables = new System.Windows.Forms.Button();
            this.comboBoxes = new System.Windows.Forms.Button();
            this.textFields = new System.Windows.Forms.Button();
            this.textAreas = new System.Windows.Forms.Button();
            this.labels = new System.Windows.Forms.Button();
            this.application = new System.Windows.Forms.Button();
            this.lists = new System.Windows.Forms.Button();
            this.trees = new System.Windows.Forms.Button();
            this.tabbedPanes = new System.Windows.Forms.Button();
            this.passwordFields = new System.Windows.Forms.Button();
            this.scrolledComposite = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttons
            // 
            this.buttons.Location = new System.Drawing.Point(3, 3);
            this.buttons.Name = "buttons";
            this.buttons.Size = new System.Drawing.Size(110, 23);
            this.buttons.TabIndex = 0;
            this.buttons.Text = "Buttons";
            this.buttons.UseVisualStyleBackColor = true;
            this.buttons.Click += new System.EventHandler(this.click_Listener);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.buttons);
            this.flowLayoutPanel1.Controls.Add(this.menu);
            this.flowLayoutPanel1.Controls.Add(this.checkboxes);
            this.flowLayoutPanel1.Controls.Add(this.radioButtons);
            this.flowLayoutPanel1.Controls.Add(this.tables);
            this.flowLayoutPanel1.Controls.Add(this.comboBoxes);
            this.flowLayoutPanel1.Controls.Add(this.textFields);
            this.flowLayoutPanel1.Controls.Add(this.textAreas);
            this.flowLayoutPanel1.Controls.Add(this.labels);
            this.flowLayoutPanel1.Controls.Add(this.application);
            this.flowLayoutPanel1.Controls.Add(this.lists);
            this.flowLayoutPanel1.Controls.Add(this.trees);
            this.flowLayoutPanel1.Controls.Add(this.tabbedPanes);
            this.flowLayoutPanel1.Controls.Add(this.passwordFields);
            this.flowLayoutPanel1.Controls.Add(this.scrolledComposite);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(232, 349);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // menu
            // 
            this.menu.Location = new System.Drawing.Point(3, 32);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(110, 23);
            this.menu.TabIndex = 7;
            this.menu.Text = "Menus";
            this.menu.UseVisualStyleBackColor = true;
            this.menu.Click += new System.EventHandler(this.click_Listener);
            // 
            // checkboxes
            // 
            this.checkboxes.Location = new System.Drawing.Point(3, 61);
            this.checkboxes.Name = "checkboxes";
            this.checkboxes.Size = new System.Drawing.Size(110, 23);
            this.checkboxes.TabIndex = 1;
            this.checkboxes.Text = "CheckBoxes";
            this.checkboxes.UseVisualStyleBackColor = true;
            this.checkboxes.Click += new System.EventHandler(this.click_Listener);
            // 
            // radioButtons
            // 
            this.radioButtons.Location = new System.Drawing.Point(3, 90);
            this.radioButtons.Name = "radioButtons";
            this.radioButtons.Size = new System.Drawing.Size(110, 23);
            this.radioButtons.TabIndex = 2;
            this.radioButtons.Text = "RadioButtons";
            this.radioButtons.UseVisualStyleBackColor = true;
            this.radioButtons.Click += new System.EventHandler(this.click_Listener);
            // 
            // tables
            // 
            this.tables.Location = new System.Drawing.Point(3, 119);
            this.tables.Name = "tables";
            this.tables.Size = new System.Drawing.Size(110, 23);
            this.tables.TabIndex = 9;
            this.tables.Text = "Tables";
            this.tables.UseVisualStyleBackColor = true;
            this.tables.Click += new System.EventHandler(this.click_Listener);
            // 
            // comboBoxes
            // 
            this.comboBoxes.AutoSize = true;
            this.comboBoxes.Location = new System.Drawing.Point(3, 148);
            this.comboBoxes.Name = "comboBoxes";
            this.comboBoxes.Size = new System.Drawing.Size(110, 23);
            this.comboBoxes.TabIndex = 3;
            this.comboBoxes.Text = "ComboBoxes";
            this.comboBoxes.UseVisualStyleBackColor = true;
            this.comboBoxes.Click += new System.EventHandler(this.click_Listener);
            // 
            // textFields
            // 
            this.textFields.Location = new System.Drawing.Point(3, 177);
            this.textFields.Name = "textFields";
            this.textFields.Size = new System.Drawing.Size(110, 23);
            this.textFields.TabIndex = 4;
            this.textFields.Text = "TextFields";
            this.textFields.UseVisualStyleBackColor = true;
            this.textFields.Click += new System.EventHandler(this.click_Listener);
            // 
            // textAreas
            // 
            this.textAreas.Location = new System.Drawing.Point(3, 206);
            this.textAreas.Name = "textAreas";
            this.textAreas.Size = new System.Drawing.Size(110, 23);
            this.textAreas.TabIndex = 5;
            this.textAreas.Text = "TextAreas";
            this.textAreas.UseVisualStyleBackColor = true;
            this.textAreas.Click += new System.EventHandler(this.click_Listener);
            // 
            // labels
            // 
            this.labels.Location = new System.Drawing.Point(3, 235);
            this.labels.Name = "labels";
            this.labels.Size = new System.Drawing.Size(110, 23);
            this.labels.TabIndex = 6;
            this.labels.Text = "Labels";
            this.labels.UseVisualStyleBackColor = true;
            this.labels.Click += new System.EventHandler(this.click_Listener);
            // 
            // application
            // 
            this.application.Location = new System.Drawing.Point(3, 264);
            this.application.Name = "application";
            this.application.Size = new System.Drawing.Size(110, 23);
            this.application.TabIndex = 7;
            this.application.Text = "Application";
            this.application.UseVisualStyleBackColor = true;
            this.application.Click += new System.EventHandler(this.click_Listener);
            // 
            // lists
            // 
            this.lists.Location = new System.Drawing.Point(3, 293);
            this.lists.Name = "lists";
            this.lists.Size = new System.Drawing.Size(110, 23);
            this.lists.TabIndex = 12;
            this.lists.Text = "Lists";
            this.lists.UseVisualStyleBackColor = true;
            this.lists.Click += new System.EventHandler(this.click_Listener);
            // 
            // trees
            // 
            this.trees.Location = new System.Drawing.Point(3, 322);
            this.trees.Name = "trees";
            this.trees.Size = new System.Drawing.Size(110, 23);
            this.trees.TabIndex = 13;
            this.trees.Text = "Trees";
            this.trees.UseVisualStyleBackColor = true;
            this.trees.Click += new System.EventHandler(this.click_Listener);
            // 
            // tabbedPanes
            // 
            this.tabbedPanes.Location = new System.Drawing.Point(119, 3);
            this.tabbedPanes.Name = "tabbedPanes";
            this.tabbedPanes.Size = new System.Drawing.Size(110, 23);
            this.tabbedPanes.TabIndex = 14;
            this.tabbedPanes.Text = "TabbedPanes";
            this.tabbedPanes.UseVisualStyleBackColor = true;
            this.tabbedPanes.Click += new System.EventHandler(this.click_Listener);
            // 
            // passwordFields
            // 
            this.passwordFields.Location = new System.Drawing.Point(119, 32);
            this.passwordFields.Name = "passwordFields";
            this.passwordFields.Size = new System.Drawing.Size(110, 23);
            this.passwordFields.TabIndex = 15;
            this.passwordFields.Text = "PasswordFields";
            this.passwordFields.UseVisualStyleBackColor = true;
            this.passwordFields.Click += new System.EventHandler(this.click_Listener);
            // 
            // scrolledComposite
            // 
            this.scrolledComposite.Location = new System.Drawing.Point(119, 61);
            this.scrolledComposite.Name = "scrolledComposite";
            this.scrolledComposite.Size = new System.Drawing.Size(110, 23);
            this.scrolledComposite.TabIndex = 16;
            this.scrolledComposite.Text = "ScrolledComposite";
            this.scrolledComposite.UseVisualStyleBackColor = true;
            this.scrolledComposite.Click += new System.EventHandler(this.click_Listener);

            // 
            // StartForm
            // 
            this.ClientSize = new System.Drawing.Size(232, 349);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "StartForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "winFormsCaA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttons;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button checkboxes;
        private System.Windows.Forms.Button radioButtons;
        private System.Windows.Forms.Button comboBoxes;
        private System.Windows.Forms.Button textFields;
        private System.Windows.Forms.Button textAreas;
        private System.Windows.Forms.Button labels;
        private System.Windows.Forms.Button menu;
        private System.Windows.Forms.Button application;
        private System.Windows.Forms.Button tables;
        private System.Windows.Forms.Button lists;
        private System.Windows.Forms.Button trees;
        private System.Windows.Forms.Button tabbedPanes;
        private System.Windows.Forms.Button passwordFields;
        private System.Windows.Forms.Button scrolledComposite;
    }
}

