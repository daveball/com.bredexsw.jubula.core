/*******************************************************************************
 * Copyright (c) 2014 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation 
 *******************************************************************************/

package com.bredexsw.jubula.ir.testing;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.SafeRunner;

import com.bredexsw.jubula.ir.core.Extensions;
import com.bredexsw.jubula.ir.core.exceptions.IncompatibleImageException;
import com.bredexsw.jubula.ir.core.interfaces.IRegionFinder;


/**
 * Class for tests
 * @author BREDEX GmbH
 *
 */
public class FinderTests {

    /** path to directory of templates */
    private static final String DIRECTEMPL = "K:/guidancer/Workspace/users/"
                + "OliverHoffmann/workspaceWindows/ImageRec/Templates/";
    
    /** path to directory of images */
    private static final String DIRECIMG = "K:/guidancer/Workspace/users/"
                + "OliverHoffmann/workspaceWindows/ImageRec/Screenshots/";
    
    /** list of paths to all templates */
    private static List<String> templatePaths = new ArrayList<String>();
    
    /** list of paths to all search images */
    private static List<String> imagePaths = new ArrayList<String>();
    
    /** list of correct coordinates for the images */
    private static List<Point[]> correctPlace = new ArrayList<Point[]>();
    
    /** list with all finders */
    private static List<IRegionFinder> finders;
    
    /** StringBuilder for output */
    private StringBuilder m_output;
    

    /**
     * execute the extension
     * @param o the extension
     */
    private void executeExtension(final Object o) {
        ISafeRunnable runnable = new ISafeRunnable() {
          //@Override
            public void handleException(Throwable e) {
                System.out.println("Exception in client");
            }

//         @Override
            public void run() throws Exception {
                finders = new ArrayList<IRegionFinder>();
                finders.add((IRegionFinder) o);
            }
        };
        SafeRunner.run(runnable);
    }
    
    public static void main(String[] args) {
        FinderTests ft = new FinderTests();
        ft.testFinders();
    }
    /** 
     * test all ImageFinders that are Extensions of the core 
     * with the input given in addImages() 
     */
    public void testFinders() {
        FileWriter out = null;
        double begin = System.currentTimeMillis();
        addImages();
        setupFinders();
            
        for (int l = 0; l < 3; l++) {           
            try {
                out = new FileWriter(
                        "K:/guidancer/Workspace/users/OliverHoffmann/"
                        + "testResultsRecognition" + (l + 1) + ".txt");
            } catch (IOException e1) {
                e1.printStackTrace();
                
            }
            m_output = new StringBuilder();

            
            m_output.append("Test started on: " + new Date().toString() 
                    + "\r\n" + "\r\n");
            
            for (int i = 0; i < templatePaths.size(); i++) {
                m_output.append("For Template: " 
                        + templatePaths.get(i) 
                        + ". For Search Image: " + imagePaths.get(i) 
                        + "\r\n"
                        + "Amount of actual appearances: "
                        + correctPlace.get(i).length
                        + "\r\n"
                        + "\r\n");
                for (IRegionFinder f: finders) {
                    checkFinder(f, i);
                }     
            }
            m_output.append("Test ended on: " + new Date().toString());
    //        Collection<FunctionDefinition> functions = 
    //                FunctionRegistry.getInstance().getAllFunctions();
//            System.out.println(m_output.toString());
            try {
                out.write(m_output.toString());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        }
        double completionTime = (System.currentTimeMillis() - begin) / 1000;
        if (completionTime < 60) {
            System.out.println("Total time needed for all tests: "
                + completionTime + "s");
        } else {
            System.out.println("Total time needed for all tests: "
                    + ((int)Math.floor(completionTime / 60)) + "min " 
                    + (Math.round(completionTime) % 60) + "s");
        }
    }

    /** setup the finders through extension points */
    private void setupFinders() {
        IExtensionRegistry reg = Extensions.getRegistryInstance();
        //for some reason reg is always null
        IConfigurationElement[] extensions = 
                reg.getConfigurationElementsFor(
                        "com.bredexsw.jubula.ir.core.finders");
        for (int i = 0; i < extensions.length; i++) {
            IConfigurationElement element = extensions[i];
            IConfigurationElement[] children = element.getChildren("finders");
            for (int j = 0; j < children.length; j++) {
                Object o;
                try {
                    o = children[j].createExecutableExtension("class");
                    if (o instanceof IRegionFinder) {
                        executeExtension(o);
                    }
                } catch (CoreException e) {
                    e.printStackTrace();
                } 
            }
        }            
    }


    /**
     * use findRegions() on the finder and append the results to output
     * @param f finder
     * @param imagePos place in the image list
     */
    private void checkFinder(IRegionFinder f, int imagePos) {
        try {
            boolean closeEnough = false;
            boolean allCorrect = true;
            BufferedImage template = 
                    ImageIO.read(new File(DIRECTEMPL 
                            + templatePaths.get(imagePos)));
            BufferedImage searchImage =
                    ImageIO.read(new File(DIRECIMG
                            + imagePaths.get(imagePos)));
            double starttime = System.currentTimeMillis();
            Rectangle[] result = 
                    f.findRegions(template, searchImage, null);
            
            double endtime = System.currentTimeMillis();
            m_output.append("Result of "  
                    + f.getClass().getSimpleName() + ":" 
                    + "\r\n");
            for (int j = 0; j < result.length; j++) {
                m_output.append(j + 1 + ". region found at " 
                        + result[j].x + "/" + result[j].y);
                for (int c = 0; c < correctPlace.get(imagePos).length; c++) {
                    if (result[j].x - correctPlace.get(imagePos)[c].getX() < 10 
                            && result[j].y - correctPlace.
                            get(imagePos)[c].getY() < 10) {
                        closeEnough = true;
                    }
                }
                if (closeEnough) {
                    m_output.append("\t Correct"
                            + "\r\n");
                } else {
                    m_output.append("\t False"
                            + "\r\n");
                    allCorrect = false;
                }               
                closeEnough = false;                
            }                        
            double totaltime = (endtime - starttime) / 1000;
            if (allCorrect && correctPlace.get(imagePos).length
                    == result.length) {
                m_output.append("PASSED"
                        + "\r\n"); 
            } else {
                m_output.append("FAILED"
                        + "\r\n"); 
            }
            m_output.append("Required Time: "
                    + totaltime + "s." + "\r\n"
                    + "\r\n"); 
            allCorrect = true;
        } catch (IOException  e) {
            e.printStackTrace();
            System.out.println("Could not read image pair number " 
                    + (imagePos + 1));
        } catch (IncompatibleImageException e) {
            e.printStackTrace();
        }
        
    }

    /** Add image paths and actual appearances to lists */
    private void addImages() {
        imagePaths.add("WIN7/multipleError_new.PNG");
        templatePaths.add("WIN7/error_Multi.PNG");
        correctPlace.add(new Point[]{new Point(479, 367), new Point(655, 534)});
        
        imagePaths.add("SWT/1.png");
        templatePaths.add("SWT/1_buttonMulti.PNG");
        correctPlace.add(new Point[]{ new Point(92, 236), new Point(357, 26)});
        
        imagePaths.add("macOS/desk_new.PNG");
        templatePaths.add("macOS/desk_windows.PNG");
        correctPlace.add(new Point[]{new Point(13, 37)});
               
        imagePaths.add("JavaFX/1.png");
        templatePaths.add("JavaFX/1_button.PNG");
        correctPlace.add(new Point[]{new Point(835, 425)});
        
//        imagePaths.add("iOS/portrait1.png");
//        templatePaths.add("iOS/portrait/1_plus.PNG");
//        correctPlace.add(new Point[]{new Point(0,0)});
        
        imagePaths.add("CENTOS/2_new.PNG");
        templatePaths.add("CENTOS/2_window.PNG");
        correctPlace.add(new Point[]{new Point(648, 456)});
        
        imagePaths.add("HTML/PNG/Application.png");
        templatePaths.add("HTML/PNG/input_Text.PNG");
        correctPlace.add(new Point[]{new Point(15, 645)});
        
        imagePaths.add("macOS/Application_new.PNG");
        templatePaths.add("macOS/desk_button.PNG"); 
        correctPlace.add(new Point[]{new Point(807, 19), new Point(839, 375),
            new Point(858, 395), new Point(876, 415), new Point(894, 435),
            new Point(913, 455), new Point(931, 475)});
        
//        very long execution time
//        imagePaths.add("RCP/CENTOS/Tree_Full.png");
//        templatePaths.add("RCP/tf_full.PNG");
    }
   
}
