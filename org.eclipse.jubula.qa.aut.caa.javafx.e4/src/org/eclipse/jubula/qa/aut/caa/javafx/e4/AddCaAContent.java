package org.eclipse.jubula.qa.aut.caa.javafx.e4;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.annotation.PostConstruct;

import org.eclipse.jubula.qa.aut.caa.javafx.ApplicationClass;
import org.eclipse.jubula.qa.aut.caa.javafx.StartScene;
/**
 * Adds CaA Content to the stage
 * @author Bredex GmbH
 *
 */
public class AddCaAContent implements ApplicationClass {
    
    /** The Stage **/
    private Stage m_primaryStage;

    
    /**
     * Adds CaA Content to the stage
     * @param stage the stage
     */
    @PostConstruct
    public void doAdd(Stage stage) {
        m_primaryStage = stage;
        m_primaryStage.setMaximized(true);
        StartScene.createInstance(this);
        Platform.setImplicitExit(false);
        m_primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            
            public void handle(WindowEvent event) {
                Platform.exit();
            }
        });
    }

    @Override
    public Stage getPrimaryStage() {
        return m_primaryStage;
    }

}
