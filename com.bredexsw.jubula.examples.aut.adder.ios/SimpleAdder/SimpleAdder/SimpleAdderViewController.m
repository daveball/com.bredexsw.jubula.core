//
//  SimpleAdderViewController.m
//  SimpleAdder
//
//  Created by markus on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SimpleAdderViewController.h"

@interface SimpleAdderViewController ()

@end

@implementation SimpleAdderViewController
@synthesize resultValue = _resultValue;
@synthesize value1 = _value1;
@synthesize value2 = _value2;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setValue1:nil];
    [self setValue2:nil];
    [self setResultValue:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (IBAction)actionPerformed:(UIButton *)sender {
    double result = 0;
    double value1 = [self.value1.text doubleValue];
    double value2 = [self.value2.text doubleValue];
    if ([sender.currentTitle isEqualToString:@"="]) {
        
        result = value1 + value2;
    } else {
        // perform reset
        self.value1.text = @"";
        self.value2.text = @"";
        
        [self.value1 becomeFirstResponder];
    }
    // on purpose implemented "bug"
    NSString *text2Display = [NSString stringWithFormat:@"%g", result];
    if (value1 == 17 && value2 == 4) {
        text2Display = @"jackpot";
    }
    self.resultValue.text = text2Display;
}

- (IBAction)performInfo:(id)sender {
    // open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"About SimpleAdder" message:@"2012 ©BREDEX GmbH" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
}
@end
