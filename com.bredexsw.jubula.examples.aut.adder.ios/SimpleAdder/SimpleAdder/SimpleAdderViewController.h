//
//  SimpleAdderViewController.h
//  SimpleAdder
//
//  Created by markus on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleAdderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *value1;
@property (weak, nonatomic) IBOutlet UITextField *value2;
- (IBAction)actionPerformed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *resultValue;
- (IBAction)performInfo:(id)sender;

@end
