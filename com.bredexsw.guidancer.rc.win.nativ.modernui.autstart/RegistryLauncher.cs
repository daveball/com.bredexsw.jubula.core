﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace com.bredexsw.guidancer.rc.win.nativ.modernui.autstart
{
    /// <summary>
    /// Command line tool for starting and reactivating a modern UI app.
    /// </summary>
    class RegistryLauncher
    {
        /// <summary>
        /// Starts or reactivate the modern UI app with the AppUserModelId given by a command line parameter.
        /// </summary>
        /// <param name="args">
        ///     The AppUserModelId of the modern UI app to start.
        ///     -q as first parameter prevents opening an error message dialog while starting.
        ///     All further parameters are given directly as parameters to the modern UI app.
        /// </param>
        /// <returns>The process ID of the started modern UI app, otherwise the exit code 0 is returned.</returns>
        public static int Main(string[] args)
        {
            ActivateOptions activateOption = ActivateOptions.None;
            int indexAppUserModelID = 0;
            if (args.Length == 0)
            {
                return printHelp(); // missing AppUserModelId, because there are no command line parameters
            }
            else
            {
                if (args[0] == "-q") // quite option prevents opening an error message dialog while starting
                {
                    activateOption = activateOption | ActivateOptions.NoErrorUI;
                    indexAppUserModelID++;
					if (args.Length == 1)
					{
                        return printHelp(); // missing AppUserModelId, because only -q is given as command line parameter
					}
                }
            }
            // parse modern UI app parameters
            String modernUIParameters = null;
            for (int i = indexAppUserModelID + 1; i < args.Length; i++) {
                if (i == indexAppUserModelID + 1)
                {
                    modernUIParameters = args[i]; // first parameter without leading space
                }
                else
                {
                    modernUIParameters += " " + args[i]; // other than first parameter with leading space
                }
            }
            // start modern UI app
            return startModernUIApp(args[indexAppUserModelID], activateOption, modernUIParameters);
        }

        /// <summary>
        /// Starts or reactivate the modern UI app.
        /// </summary>
        /// <param name="appUserModeID">
        ///     The AppUserModelId of the modern UI app to start,
        ///     e.g. c160db5f-1b90-4fab-aed7-3aa5d877dbb4_t4ggbkbvwn4ag!App is for the XAML/C# Simple Adder.
        /// </param>
        /// <param name="activateOption">Options for activating the modern UI app (combined flags with OR).</param>
        /// <param name="modernUIParameters">A string containing all parameters, which are directly given to the modern UI app.</param>
        /// <returns>The process ID of the started modern UI app, otherwise the exit code 0 is returned.</returns>
        private static int startModernUIApp(string appUserModelID, ActivateOptions activateOption, String modernUIParameters)
        {
            ApplicationActivationManager appActiveManager = new ApplicationActivationManager(); //Class not registered
            IntPtr result = IntPtr.Zero;
            IntPtr pid = IntPtr.Zero;
            try
            {
                result = appActiveManager.ActivateApplication(appUserModelID, modernUIParameters, activateOption, out pid);
            }
            catch (Exception)
            {
            }
            if (result != IntPtr.Zero)
            {
                pid = IntPtr.Zero; // unknown error
            }
            if (pid == IntPtr.Zero)
            {
                Console.WriteLine("Error: App not installed or Id is wrong:\n{0}", appUserModelID);
            }
            // The process ID is converted from unsigned int 32 to signed int 32. This only leads
            // to a problem, if there are more then 2^31=2147483648 processes on one computer.
            // If the exit code is negative, we only have to add 2^32 to fix this problem.
            return (int) pid;
        }

        /// <summary>
        /// Prints a description of the needed and optional command line parameters.
        /// </summary>
        /// <returns>0</returns>
        private static int printHelp()
        {
            Console.WriteLine(
                "Optionally use -q to prevent opening an error message dialog while starting.\n" +
                "Please add the AppUserModelId as a parameter to start a modern UI app.\n" +
                "All further parameters are given directly as parameters to the modern UI app.");
            return 0;
        }

    }

    /// <summary>
    /// Enumeration defining the different activation methods of modern UI apps.
    /// </summary>
    enum ActivateOptions
    {
        None = 0x00000000,  // No flags set
        DesignMode = 0x00000001,
        NoErrorUI = 0x00000002,  // Do not show an error dialog if the app fails to activate.
        NoSplashScreen = 0x00000004,  // Do not show the splash screen when activating the app.
    }

    /// <summary>
    /// <seealso cref="http://msdn.microsoft.com/en-us/library/windows/desktop/hh706902(v=vs.85).aspx"/>
    /// </summary>
    [ComImport, Guid("2e941141-7f97-4756-ba1d-9decde894a3d"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    interface IApplicationActivationManager
    {
        // Activates the specified immersive application for the "Launch" contract, passing the provided arguments
        // string into the application. Callers can obtain the process Id of the application instance fulfilling this contract.
        IntPtr ActivateApplication([In] String appUserModelId, [In] String arguments, [In] ActivateOptions options, [Out] out IntPtr processId);
    }

    /// <summary>
    /// <seealso cref="http://msdn.microsoft.com/en-us/library/windows/desktop/Hh706903(v=vs.85).aspx"/>
    /// </summary>
    [ComImport, Guid("45BA127D-10A8-46EA-8AB7-56EA9078943C")]//Application Activation Manager
    class ApplicationActivationManager : IApplicationActivationManager
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)/*, PreserveSig*/]
        public extern IntPtr ActivateApplication([In] String appUserModelId, [In] String arguments, [In] ActivateOptions options, [Out] out IntPtr processId);
    }
}

