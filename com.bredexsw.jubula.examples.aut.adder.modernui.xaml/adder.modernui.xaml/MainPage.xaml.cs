﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace adder.modernui.xaml
{
    /// <summary>
    /// The main page for the Simple Adder.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// Show the modern UI parameter given to this page in the first input text field.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter == null)
            {
                result.Text = "NULL"; // currently in every case not null is passed
            }
            String parameters = e.Parameter.ToString().Trim();
            if (parameters.Length > 0)
            {
                int index = parameters.IndexOf(" ");
                if (index >= 0)
                {
                    value1.Text = parameters.Substring(0, index);
                    value2.Text = parameters.Substring(index + 1);
                }
                else
                {
                    value1.Text = parameters;
                }
            }
        }

        /// <summary>
        /// Event handler after the = button has been pressed.
        /// The sum of the first and second input text field is shown in the result text field.
        /// If it can not be calculated, #error is shown in the result text field.
        /// If 17 and 4 is given, the result is jockpot.
        /// </summary>
        /// <param name="sender">unused</param>
        /// <param name="e">unused</param>
        private void btnResult_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int intValue1 = Convert.ToInt32(value1.Text);
                int intValue2 = Convert.ToInt32(value2.Text);
                if (intValue1 == 17 && intValue2 == 4)
                {
                    result.Text = "jackpot";
                }
                else
                {
                    result.Text = "" + (intValue1 + intValue2);
                }
            }
            catch(Exception)
            {
                result.Text = "#error";
            }
        }

        /// <summary>
        /// Event hanlder after the C button has been pressed. It clears all three text fields.
        /// </summary>
        /// <param name="sender">unused</param>
        /// <param name="e">unused</param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            value1.Text = "";
            value2.Text = "";
            result.Text = "";
        }

        private void value_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                btnResult_Click(sender, e);
            }
        }

    }
}
