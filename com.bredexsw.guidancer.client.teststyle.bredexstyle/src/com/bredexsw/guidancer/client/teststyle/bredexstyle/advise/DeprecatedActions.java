package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import java.util.Iterator;

import org.eclipse.jubula.client.core.model.IEventExecTestCasePO;
import org.eclipse.jubula.client.core.model.IExecTestCasePO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.ISpecTestCasePO;
import org.eclipse.jubula.client.core.model.ITestCasePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 * @created Nov 9, 2010
 * @version $Revision: 1.1 $
 */
public class DeprecatedActions extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = 
        Messages.CheckMessageDeprecatedActions;

    /** sign for a deprecated action */
    public static final String DEPRECATED = "DEPRECATED"; //$NON-NLS-1$

    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof ITestCasePO)) {
            return false;
        }

        ITestCasePO po = (ITestCasePO) obj;
        Iterator<INodePO> iter = po.getNodeListIterator();
        while (iter.hasNext()) {
            Object next = iter.next();
            if (next instanceof IExecTestCasePO) {
                IExecTestCasePO execPo = (IExecTestCasePO) next;
                ISpecTestCasePO specTestCase = execPo.getSpecTestCase();
                // specTestCase will be null if its reused project is missing
                if ((specTestCase != null)
                        && specTestCase.getName().startsWith(DEPRECATED)) {
                    return true;
                }
            }
        }
        for (IEventExecTestCasePO eh : po.getAllEventEventExecTC()) {
            ISpecTestCasePO specTestCase = eh.getSpecTestCase();
            // specTestCase will be null if its reused project is missing
            if ((specTestCase != null)
                    && specTestCase.getName().startsWith(DEPRECATED)) {
                return true;
            }
        }
        return false;
    }

}
