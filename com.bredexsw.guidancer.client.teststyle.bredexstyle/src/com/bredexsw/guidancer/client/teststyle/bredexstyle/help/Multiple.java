package com.bredexsw.guidancer.client.teststyle.bredexstyle.help;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.eclipse.jubula.client.core.businessprocess.ExternalTestDataBP;
import org.eclipse.jubula.client.core.model.IDataSetPO;
import org.eclipse.jubula.client.core.model.IExecTestCasePO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.IParamNodePO;
import org.eclipse.jubula.client.core.model.ITDManager;
import org.eclipse.jubula.client.core.model.ITestDataPO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;
/**
 * @author marcell
 */
public class Multiple extends BaseCheck {

    /** Error message */
    private static final String ERROR_MSG = 
        Messages.CheckMessageMultiple;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IParamNodePO)) {
            return false;
        }
        
        IParamNodePO node = (IParamNodePO) obj;
        
        Iterator<INodePO> children = node.getNodeListIterator();
        while (children.hasNext()) {
            INodePO childNode = children.next();
            if (!(childNode instanceof IExecTestCasePO)) {
                continue;
            }
            IExecTestCasePO child = (IExecTestCasePO) childNode;
            ITDManager man = null;
            try {
                man = new ExternalTestDataBP()
                    .getExternalCheckedTDManager(
                            child, new Locale(""), true);
            } catch (Exception e) {
                // ignore and break
                return false;
            }
            Map<String, Set<String>> map = 
                new HashMap<String, Set<String>>();
            for (IDataSetPO lst : man.getDataSets()) {
                if (node.getParameterList().size() < lst.getList().size()) {
                    break;
                }
                for (int i = 0; i < lst.getList().size(); i++) {
                    ITestDataPO data = lst.getList().get(i);
                    String name = node.getParameterList().get(i).getName();
                    for (Locale locale : data.getLanguages()) {
                        if (!map.containsKey(name)) {
                            map.put(name, new HashSet<String>());
                        }
                        String lValue = data.getValue(locale);
                        if (lValue.startsWith("=") 
                                && (!map.get(name).isEmpty()
                                && map.get(name).add(lValue))) {
                            return true;
                        }
                    }
                    
                }
            }
        }
        
        
        
        return false;
    }

}
