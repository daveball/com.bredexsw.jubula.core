package com.bredexsw.guidancer.client.teststyle.bredexstyle.help;

import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class ReusedProjects extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = Messages.CheckMessageReusedProjects;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IProjectPO)) {
            return false;
        }
        
        IProjectPO project = (IProjectPO) obj;
        return project.getUsedProjects().size() == 0;
    }

}
