package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jubula.client.core.events.DataEventDispatcher;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.DataState;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.UpdateState;
import org.eclipse.jubula.client.core.model.ICategoryPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.core.model.ISpecObjContPO;
import org.eclipse.jubula.client.core.model.NodeMaker;
import org.eclipse.jubula.client.core.persistence.ISpecPersistable;
import org.eclipse.jubula.client.core.persistence.NodePM;
import org.eclipse.jubula.client.core.persistence.PMException;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.client.teststyle.quickfix.Quickfix;
import org.eclipse.jubula.client.ui.rcp.controllers.PMExceptionHandler;
import org.eclipse.osgi.util.NLS;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class EventHandlerDirectory extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = 
        Messages.CheckMessageEventHandlerDirectory;
    
    /** name */
    public static final String EH_DIR_NAME = "name"; //$NON-NLS-1$
    
    @Override
    public String getDescription() {
        return NLS.bind(ERROR_MSG, getAttributeValue(EH_DIR_NAME));
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IProjectPO)) {
            return false;
        }
        
        boolean exists = false;
        
        IProjectPO project = (IProjectPO)obj;
        for (ISpecPersistable p : project.getSpecObjCont().getSpecObjList()) {
            if (p instanceof ICategoryPO) {
                if (getAttributeValue(EH_DIR_NAME).equals(p.getName())) { 
                    exists = true;
                    break;
                }
            }
        }
        
        return !exists;
    }
    
    @Override
    public Quickfix[] getQuickfix(Object obj) {
        return new Quickfix[] { new EventHandlerTestSuiteQuickfix() };
    }
    
    /** {@inheritDoc} */
    private class EventHandlerTestSuiteQuickfix extends Quickfix {

        /** {@inheritDoc} */
        public String getLabel() {
            return NLS.bind(Messages.QuickfixEventHandler,  
                    getAttributeValue(EH_DIR_NAME));
        }

        /** {@inheritDoc} */
        public void run(IMarker marker) {
            String categoryName = getAttributeValue(EH_DIR_NAME);
            ICategoryPO category = NodeMaker.createCategoryPO(categoryName);
            
            INodePO parent = ISpecObjContPO.TCB_ROOT_NODE;
            
            try {
                NodePM.addAndPersistChildNode(parent, category, null);
            } catch (PMException e) {
                PMExceptionHandler.handlePMExceptionForMasterSession(e);
            } catch (Exception e) {
                PMExceptionHandler.handleProjectDeletedException();
            }
            DataEventDispatcher.getInstance().fireDataChangedListener(category, 
                DataState.Added, UpdateState.all);
        }
        
    }

}
