package com.bredexsw.guidancer.client.teststyle.bredexstyle.help;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.PersistenceException;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jubula.client.core.events.DataEventDispatcher;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.DataState;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.UpdateState;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.IPersistentObject;
import org.eclipse.jubula.client.core.persistence.PMException;
import org.eclipse.jubula.client.core.persistence.PersistenceManager;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.client.teststyle.quickfix.Quickfix;
import org.eclipse.jubula.client.ui.rcp.controllers.PMExceptionHandler;
import org.eclipse.jubula.client.ui.rcp.editors.AbstractJBEditor;
import org.eclipse.jubula.client.ui.rcp.editors.JBEditorHelper;
import org.eclipse.jubula.client.ui.rcp.handlers.open.AbstractOpenHandler;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.Activator;
import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class Commented extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = 
        Messages.CheckMessageCommented;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof INodePO)) {
            return false;
        }
        
        boolean hasError = false;
        INodePO parent = (INodePO) obj;
        Iterator<INodePO> iter = parent.getNodeListIterator();
        while (iter.hasNext()) {
            INodePO child = iter.next();
            if (!child.isActive()) {
                hasError = true;
                break;
            }
        }
        return hasError;
    }
    
    @Override
    public Quickfix[] getQuickfix(Object obj) {
        Quickfix[] defaults = super.getQuickfix(obj);
        Quickfix[] additions = new Quickfix[defaults.length + 2];
        for (int i = 0; i < defaults.length; i++) {
            additions[i] = defaults[i];
        }
        additions[defaults.length] = new RemoveInactiveQuickfix();
        additions[defaults.length + 1] = new MakeActiveQuickfix();
        return additions;
    }
    
    /** {@inheritDoc} */
    private static class RemoveInactiveQuickfix extends Quickfix {

        /** {@inheritDoc} */
        public String getLabel() {
            return Messages.QuickfixRemoveInactive;
        }

        /** {@inheritDoc} */
        public void run(IMarker marker) {
            AbstractOpenHandler.openEditor(
                    (IPersistentObject)getObject(marker));
            IWorkbenchPage page =
                Activator.getDefault().getWorkbench()
                        .getActiveWorkbenchWindow().getActivePage();
            
            IWorkbenchPart activePart = page.getActivePart();
            ISelection currentSelection = makeSelection(page);
            
            if (activePart instanceof AbstractJBEditor) {
                AbstractJBEditor tce = (AbstractJBEditor)activePart;
                IStructuredSelection structuredSelection = 
                    (IStructuredSelection)currentSelection;
                if (tce.getEditorHelper().requestEditableState() 
                        != JBEditorHelper.EditableState.OK) {
                    return;
                }
                for (@SuppressWarnings("unchecked")
                    Iterator<INodePO> it = structuredSelection.iterator(); 
                        it.hasNext();) {
                    
                    INodePO node = it.next();
                    try {
                        node.getParentNode().removeNode(node);
                        if (node.getId() != null) {
                            tce.getEditorHelper().getEditSupport()
                            .getSession().remove(node);
                        }
                        tce.getEditorHelper().setDirty(true);
                        DataEventDispatcher.getInstance()
                            .fireDataChangedListener(node, DataState.Deleted, 
                                UpdateState.onlyInEditor);
                    } catch (PersistenceException e) {
                        try {
                            PersistenceManager.handleDBExceptionForEditor(
                                node, e, 
                                tce.getEditorHelper().getEditSupport());
                        } catch (PMException pme) {
                            PMExceptionHandler
                                .handlePMExceptionForMasterSession(pme);
                            break;
                        }
                        break;
                    }
                    
                }
            }
            
        }
        
        /**
         * @param page page where the selection should be
         * @return the selection object
         */
        private StructuredSelection makeSelection(IWorkbenchPage page) {
            StructuredSelection currentSelection = new StructuredSelection();
            if (page.getActiveEditor() instanceof AbstractJBEditor) {
                AbstractJBEditor tcEditor = 
                    (AbstractJBEditor) page.getActiveEditor();
                TreeViewer tree = tcEditor.getTreeViewer();
                INodePO root =
                        (INodePO)tree.getTree().getItems()[0].getData();
                List<INodePO> toSelect = new ArrayList<INodePO>(); 
                for (INodePO child : root.getUnmodifiableNodeList()) {
                    if (!child.isActive()) {
                        toSelect.add(child);
                    }
                }
                currentSelection = 
                    new StructuredSelection(toSelect);
                tree.setSelection(currentSelection, true);
            } 
            return currentSelection;
        } 
        
    }
    
    /** {@inheritDoc} */
    private static class MakeActiveQuickfix extends Quickfix {

        /** {@inheritDoc} */
        public String getLabel() {
            return Messages.QuickfixMakeActive;
        }

        /** {@inheritDoc} */
        public void run(IMarker marker) {
            AbstractOpenHandler.openEditor(
                    (IPersistentObject)getObject(marker));
            IWorkbenchPage page =
                Activator.getDefault().getWorkbench()
                        .getActiveWorkbenchWindow().getActivePage();
            IWorkbenchPart activePart = page.getActivePart();
            ISelection currentSelection = makeSelection(page);
            
            if (activePart instanceof AbstractJBEditor) {
                AbstractJBEditor tce = (AbstractJBEditor)activePart;
                if (tce.getEditorHelper().requestEditableState() 
                        != JBEditorHelper.EditableState.OK) {
                    return;
                }
                IStructuredSelection structuredSelection = 
                    (IStructuredSelection)currentSelection;
                for (@SuppressWarnings("unchecked")
                    Iterator<INodePO> it = structuredSelection.iterator(); it
                        .hasNext();) {
                    INodePO node = it.next();
                    node.setActive(!node.isActive());
                    tce.getEditorHelper().setDirty(true);
                }
                DataEventDispatcher.getInstance().firePropertyChanged(false);
            }
        }
        
        
        /**
         * @param page page where the selection should be
         * @return the selection object
         */
        private StructuredSelection makeSelection(IWorkbenchPage page) {
            StructuredSelection currentSelection = new StructuredSelection();
            if (page.getActiveEditor() instanceof AbstractJBEditor) {
                AbstractJBEditor tcEditor = 
                    (AbstractJBEditor) page.getActiveEditor();
                TreeViewer tree = tcEditor.getTreeViewer();
                INodePO root =
                        (INodePO)tree.getTree().getItems()[0].getData();
                List<INodePO> toSelect = new ArrayList<INodePO>(); 
                for (INodePO child : root.getUnmodifiableNodeList()) {
                    if (!child.isActive()) {
                        toSelect.add(child);
                    }
                }
                currentSelection = 
                    new StructuredSelection(toSelect);
                tree.setSelection(currentSelection, true);
            } 
            return currentSelection;
        } 
        
        
    
    }

}
