package com.bredexsw.guidancer.client.teststyle.bredexstyle.naming;

import org.eclipse.jubula.client.core.businessprocess.db.TestCaseBP;
import org.eclipse.jubula.client.core.model.IExecTestCasePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.tools.internal.constants.StringConstants;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class MissingNameTestCase extends BaseCheck {

    /** Error message */
    private static final String ERROR_MSG = 
        Messages.CheckMessageMissingNameTestCase;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IExecTestCasePO)) {
            return false;
        }
        IExecTestCasePO execTc = (IExecTestCasePO)obj;
        if (TestCaseBP.belongsToCurrentProject(execTc.getSpecTestCase())) {
            return false;
        }
        
        if (execTc.getRealName() == null
                || StringConstants.EMPTY.equals(execTc.getRealName())) {
            return true;
        }
        return false;
    }

}
