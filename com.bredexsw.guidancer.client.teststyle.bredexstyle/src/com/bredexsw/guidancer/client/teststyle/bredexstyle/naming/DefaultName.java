package com.bredexsw.guidancer.client.teststyle.bredexstyle.naming;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jubula.client.core.constants.InitialValueConstants;
import org.eclipse.jubula.client.core.model.ICapPO;
import org.eclipse.jubula.client.core.model.ICategoryPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.ISpecTestCasePO;
import org.eclipse.jubula.client.core.model.ITestJobPO;
import org.eclipse.jubula.client.core.model.ITestSuitePO;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.tools.internal.constants.StringConstants;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class DefaultName extends BaseCheck {

    /** Error message */
    private static final String ERROR_MSG = Messages.CheckMessageDefaultName;

    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        String elementName = StringConstants.EMPTY;
        List<String> defaultNames = new ArrayList<String>(1);
        if (obj instanceof INodePO) {
            elementName = ((INodePO)obj).getName();
        }

        if (obj instanceof ISpecTestCasePO) {
            defaultNames.add(
                    InitialValueConstants.DEFAULT_TEST_CASE_NAME);
            defaultNames.add(
                    InitialValueConstants.DEFAULT_TEST_CASE_NAME_OBSERVED);
        } else if (obj instanceof ITestSuitePO) {
            defaultNames.add(InitialValueConstants.DEFAULT_TEST_SUITE_NAME);
        } else if (obj instanceof ITestJobPO) {
            defaultNames.add(InitialValueConstants.DEFAULT_TEST_JOB_NAME);
        } else if (obj instanceof ICapPO) {
            defaultNames.add(InitialValueConstants.DEFAULT_CAP_NAME);
        } else if (obj instanceof ICategoryPO) {
            defaultNames.add(InitialValueConstants.DEFAULT_CATEGORY_NAME);
        }

        return isDefaultName(elementName, defaultNames);
    }

    /**
     * @param toCheck
     *            the name to check
     * @param defaultNames
     *            the list of possible default names
     * @return true if name to check is default name
     */
    private boolean isDefaultName(String toCheck, List<String> defaultNames) {
        boolean hasDefault = false;
        for (String name : defaultNames) {
            hasDefault = toCheck.startsWith(name);
            if (hasDefault) {
                break;
            }
        }
        return hasDefault;
    }
}
