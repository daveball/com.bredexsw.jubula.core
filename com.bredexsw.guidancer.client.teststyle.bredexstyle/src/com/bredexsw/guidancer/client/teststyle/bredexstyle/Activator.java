package com.bredexsw.guidancer.client.teststyle.bredexstyle;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 */
public class Activator extends AbstractUIPlugin {

    /** The plug-in ID */
    public static final String PLUGIN_ID = "com.bredexsw.guidancer.client.teststyle.bredexstyle"; //$NON-NLS-1$

    /** The shared instance */
    private static Activator plugin;

    /**
     * The private constructor because of singleton.
     */
    public Activator() {
    // Yes, it's empty. Is there any problem with that?
    }

    /**
     * {@inheritDoc}
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    /**
     * {@inheritDoc}
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }

}
