package com.bredexsw.guidancer.client.teststyle.bredexstyle.advise;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jubula.client.core.businessprocess.db.TestSuiteBP;
import org.eclipse.jubula.client.core.events.DataEventDispatcher;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.DataState;
import org.eclipse.jubula.client.core.events.DataEventDispatcher.UpdateState;
import org.eclipse.jubula.client.core.model.IAUTMainPO;
import org.eclipse.jubula.client.core.model.IExecObjContPO;
import org.eclipse.jubula.client.core.model.INodePO;
import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.core.model.ITestSuitePO;
import org.eclipse.jubula.client.core.model.NodeMaker;
import org.eclipse.jubula.client.core.persistence.GeneralStorage;
import org.eclipse.jubula.client.core.persistence.NodePM;
import org.eclipse.jubula.client.core.persistence.PMException;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;
import org.eclipse.jubula.client.teststyle.quickfix.Quickfix;
import org.eclipse.jubula.client.ui.rcp.controllers.PMExceptionHandler;
import org.eclipse.osgi.util.NLS;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class BrokenTestSuite extends BaseCheck {

    /** Error message */
    public static final String ERROR_MSG = Messages.CheckMessageBrokenTestSuite;
    
    /** name */
    public static final String TEST_SUITE_CONTAIN = "name contains"; //$NON-NLS-1$
    
    @Override
    public String getDescription() {
        return NLS.bind(ERROR_MSG, getAttributeValue(TEST_SUITE_CONTAIN));
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IProjectPO)) {
            return false;
        }
        
        boolean exists = false;
        
        IProjectPO project = (IProjectPO)obj;
        for (ITestSuitePO p : TestSuiteBP.getListOfTestSuites(project)) {
            if (p.getName().contains(getAttributeValue(TEST_SUITE_CONTAIN))) {
                exists = true;
                break;
            }
        }
        
        return !exists;
    }
    
    @Override
    public Quickfix[] getQuickfix(Object obj) {
        return new Quickfix[] { new BrokenTestSuiteQuickfix() };
    }
    
    /**
     * Sets a default AUT and a default AUTConfig to the new TestSuite (when
     * only one AUT / AUTConfig is defined in the Project.
     * 
     * @param testSuite
     *            the new test suite.
     */
    public static void setDefaultValuesToTestSuite(ITestSuitePO testSuite) {
        IProjectPO project = GeneralStorage.getInstance().getProject();
        // set default AUTMainPO to testSuite
        int autListSize = project.getAutMainList().size();
        if (autListSize == 0 || autListSize > 1) {
            return;
        }
        IAUTMainPO aut = 
            (IAUTMainPO)(project.getAutMainList().toArray())[0];
        testSuite.setAut(aut);

        // set default AUTConfigPO to testSuite
        int autConfigListLength = aut.getAutConfigSet().size();
        if (autConfigListLength == 0 || autConfigListLength > 1) {
            return;
        }
    }
    
    /** {@inheritDoc} */
    private class BrokenTestSuiteQuickfix extends Quickfix {

        /** {@inheritDoc} */
        public String getLabel() {
            return NLS.bind(Messages.QuickfixBrokenTestSuite,
                    getAttributeValue(TEST_SUITE_CONTAIN));
        }

        /** {@inheritDoc} */
        public void run(IMarker marker) {
            INodePO parent = IExecObjContPO.TSB_ROOT_NODE;
            try {
                ITestSuitePO testSuite = NodeMaker.createTestSuitePO(
                        getAttributeValue(TEST_SUITE_CONTAIN));
                setDefaultValuesToTestSuite(testSuite);
                NodePM.addAndPersistChildNode(parent, testSuite, null);
                DataEventDispatcher.getInstance().fireDataChangedListener(
                        testSuite, DataState.Added, UpdateState.all);
            } catch (PMException e) {
                PMExceptionHandler.handlePMExceptionForMasterSession(e);
            } catch (Exception e) {
                PMExceptionHandler.handleProjectDeletedException();
            }
        }
    }
}
