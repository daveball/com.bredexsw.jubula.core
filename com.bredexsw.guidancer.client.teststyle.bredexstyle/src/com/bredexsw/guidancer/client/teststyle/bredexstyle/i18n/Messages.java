/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n;

import org.eclipse.osgi.util.NLS;

/**
 * @author BREDEX GmbH
 * @created 10.12.2010
 */
public class Messages extends NLS {
    private static final String BUNDLE_NAME = "com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.messages"; //$NON-NLS-1$

    public static String CheckMessageBrokenTestSuite;
    public static String CheckMessageDeprecatedActions;
    public static String CheckMessageEventHandlerDirectory;
    public static String CheckMessageExcelData;
    public static String CheckMessageFulltestTestSuite;
    public static String CheckMessageNumberOfChildren;
    public static String CheckMessageParametersQuantity;
    public static String CheckMessageWorkingDirectory;
    public static String CheckMessageCommented;
    public static String CheckMessageMultiple;
    public static String CheckMessageNoCentraTestData;
    public static String CheckMessageReusedProjects;
    public static String CheckMessageDefaultName;
    public static String CheckMessageMissingNameTestCase;
    public static String CheckMessageOSRelevantCharacters;
    public static String CheckMessageParameter;
    public static String CheckMessageSingleSpaces;
    public static String QuickfixBrokenTestSuite;
    public static String QuickfixReplaces;
    public static String QuickfixEventHandler;
    public static String QuickfixFulltest;
    public static String QuickfixWorkingDirectory;
    public static String QuickfixRemoveInactive;
    public static String QuickfixMakeActive;
    public static String QuickfixOpenCTDEditor;
    public static String QuickfixRemoveSpaces;
    
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }
    
    /**
     * Constructor
     */
    private Messages() {
        // hide
    }
}
