package com.bredexsw.guidancer.client.teststyle.bredexstyle.naming;

import org.eclipse.jubula.client.core.model.IExecTestCasePO;
import org.eclipse.jubula.client.core.model.IPersistentObject;
import org.eclipse.jubula.client.teststyle.checks.BaseCheck;

import com.bredexsw.guidancer.client.teststyle.bredexstyle.i18n.Messages;

/**
 * @author marcell
 */
public class SingleSpaces extends BaseCheck {
    
    /** Error message */
    private static final String ERROR_MSG = 
        Messages.CheckMessageSingleSpaces;
    
    @Override
    public String getDescription() {
        return ERROR_MSG;
    }

    @Override
    public boolean hasError(Object obj) {
        if (!(obj instanceof IPersistentObject)) {
            return false;
        }
        if (obj instanceof IExecTestCasePO) {
            IExecTestCasePO execTestCase = (IExecTestCasePO) obj;
            if (execTestCase.getRealName() == null) {
                return false;
            }
            return execTestCase.getRealName().contains("  ");
        }
        IPersistentObject persObj = (IPersistentObject) obj;        
        return persObj.getName().contains("  ");
    }
    
}
