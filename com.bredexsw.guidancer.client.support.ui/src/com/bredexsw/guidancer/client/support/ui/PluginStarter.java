/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 12891 $
 *
 * $Date: 2010-11-09 17:02:25 +0100 (Tue, 09 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2008 
 * 
 */
package com.bredexsw.guidancer.client.support.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class PluginStarter extends AbstractUIPlugin {

    /** The shared instance */
    private static PluginStarter plugin;
    
    /**
     * The constructor
     */
    public PluginStarter() {
        // no action
    }

    /**
     * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
     * {@inheritDoc}
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    /**
      * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
     * {@inheritDoc}
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static PluginStarter getDefault() {
        return plugin;
    }

}
