/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.client.support.ui.dialogs;

import java.io.File;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jubula.client.core.persistence.GeneralStorage;
import org.eclipse.jubula.client.ui.constants.ContextHelpIds;
import org.eclipse.jubula.client.ui.rcp.Plugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.bredexsw.guidancer.client.support.ui.i18n.Messages;


/**
 * Allows the user to choose which files to include in a Support Information 
 * Package and where the Package should be saved.
 *
 * @author BREDEX GmbH
 * @created 18.04.2008
 */
public final class SupportInfoPackageDialog extends TitleAreaDialog {
    /** number of columns in area parent */
    private static final int NUM_COLUMNS_AREA_PARENT = 1;
    /** number of columns in area */
    private static final int NUM_COLUMNS_AREA = 2;
    /** vertical spacing = 2 */
    private static final int VERTICAL_SPACING = 2;
    /** margin width = 0 */
    private static final int MARGIN_WIDTH = 10;
    /** margin height = 2 */
    private static final int MARGIN_HEIGHT = 10;
    /** width hint = 300 */
    private static final int WIDTH_HINT = 300;
    
    /** the 'add configuration information' checkbox */
    private Button m_addConfigInfoCheckbox;
    /** the 'add aut starter log' label */
    private Label m_addAutStarterLogLabel;
    /** the 'add aut starter log' checkbox */
    private Button m_addAutStarterLogCheckbox;
    /** the 'add client log' label */
    private Label m_addClientLogLabel;
    /** the 'add client log' checkbox */
    private Button m_addClientLogCheckbox;
    /** the 'add projects' label */
    private Label m_addProjectsLabel;
    /** the 'add projects' checkbox */
    private Button m_addProjectsCheckbox;

    /** the 'package destination' textfield */
    private Text m_packageDestinationTextField;

    /** Is 'add aut starter log' checkbox to enable? */
    private final boolean m_enableAutStarterLogCheckbox;
    /** Is 'add client log' checkbox to enable? */
    private final boolean m_enableClientLogCheckbox;
    
    // ---------- default data ------------
    /** Is 'add aut starter log' checkbox selected? */
    private boolean m_addAutStarterLogCheckboxSelected = true;
    /** Is 'add client log' checkbox selected? */
    private boolean m_addClientLogCheckboxSelected = true;
    /** Is 'add projects' checkbox selected? */
    private boolean m_addProjectsCheckboxSelected = false;
    /** Is 'add configuration information' checkbox selected? */
    private boolean m_addConfigInfoCheckboxSelected = true;
    /** the destination directory for the generated Support Information Package */
    private File m_packageDestination = new File(System.getProperty("user.home")); //$NON-NLS-1$
    /** the filename for the generated Support Information Package */
    private String m_packageFileName = "support_" + System.currentTimeMillis() + ".zip";  //$NON-NLS-1$ //$NON-NLS-2$
    
    /**
     * @param parentShell The parent shell
     * @param enableAutStarterLogCheckbox Indicates whether aut starter log
     *                                    checkbox has to be enabled
     * @param enableClientLogCheckbox Indicates whether client log checkbox 
     *                                has to be enabled
     */
    public SupportInfoPackageDialog(Shell parentShell,
            boolean enableAutStarterLogCheckbox,
            boolean enableClientLogCheckbox) {
        
        super(parentShell);
        
        m_enableAutStarterLogCheckbox = enableAutStarterLogCheckbox;
        m_enableClientLogCheckbox = enableClientLogCheckbox;
    }

    /**
     * {@inheritDoc}
     */
    protected Control createDialogArea(Composite parent) {
        setTitle(Messages.SupportRequestMailDialogTitle);
        setMessage(Messages.SupportRequestMailDialogMessage);
        getShell().setText(Messages.SupportRequestMailDialogShellTitle);
        final GridLayout gridLayoutParent = new GridLayout();
        gridLayoutParent.numColumns = NUM_COLUMNS_AREA_PARENT;
        gridLayoutParent.verticalSpacing = VERTICAL_SPACING;
        gridLayoutParent.marginWidth = MARGIN_WIDTH;
        gridLayoutParent.marginHeight = MARGIN_HEIGHT;
        parent.setLayout(gridLayoutParent);
        
        GridDataFactory groupGridDataFactory = 
            GridDataFactory.swtDefaults().grab(true, true)
                .align(SWT.FILL, SWT.FILL).hint(WIDTH_HINT, SWT.DEFAULT);
        
        Group infoGroup = new Group(parent, SWT.NONE);
        infoGroup.setText(Messages.SupportInfoPackageContentsGroupTitle);
        
        final GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = NUM_COLUMNS_AREA;
        infoGroup.setLayout(gridLayout);

        groupGridDataFactory.applyTo(infoGroup);
        createFields(infoGroup);

        Group destinationGroup = new Group(parent, SWT.NONE);
        groupGridDataFactory.applyTo(destinationGroup);
        destinationGroup.setText(
                Messages.SupportInfoPackageDestinationGroupTitle);
        destinationGroup.setLayout(new GridLayout(2, false));
        new Label(destinationGroup, SWT.NONE).setText(
                Messages.SupportInfoPackageFileLabel);
        new Label(destinationGroup, SWT.NONE).setText(m_packageFileName);
        new Label(destinationGroup, SWT.NONE).setText(
                Messages.SupportInfoPackageDestinationLabel);
        Composite fileFieldComposite = 
            new Composite(destinationGroup, SWT.NONE);
        fileFieldComposite.setLayout(new GridLayout(2, false));
        fileFieldComposite.setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false));
        m_packageDestinationTextField = 
            new Text(fileFieldComposite, SWT.BORDER);
        m_packageDestinationTextField.setEnabled(false);
        m_packageDestinationTextField.setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false));
        m_packageDestinationTextField.setText(
                m_packageDestination.getAbsolutePath());
        Button browseButton = new Button(fileFieldComposite, SWT.NONE);
        browseButton.setText(
                Messages.SupportInfoPackageDialogDestBrowseButtonText);
        browseButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                DirectoryDialog fileChooser = new DirectoryDialog(getShell());
                fileChooser.setFilterPath(
                        m_packageDestinationTextField.getText());
                String selectedPath = fileChooser.open();
                if (selectedPath != null) {
                    m_packageDestinationTextField.setText(selectedPath);
                }
            }
        });
        
        Plugin.getHelpSystem().setHelp(parent, ContextHelpIds
                .SUPPORT_REQUEST_DIALOG);
        setHelpAvailable(true);
        return parent;
    }
    
    /**
     * @param area The composite.
     * creates the editor widgets
     */
    private void createFields(Composite area) {
        // for 'add configuration information' 
        new Label(area, SWT.NONE).setText(
            Messages.SupportRequestMailDialogAddConfigInfoLabel);
        m_addConfigInfoCheckbox = new Button(area, SWT.CHECK);
        m_addConfigInfoCheckbox.setSelection(m_addConfigInfoCheckboxSelected);

        // for 'add aut starter log' 
        m_addAutStarterLogLabel = new Label(area, SWT.NONE);
        m_addAutStarterLogLabel.setText(
            Messages.SupportRequestMailDialogAddAutStarterLogLabel);
        m_addAutStarterLogCheckbox = new Button(area, SWT.CHECK);
        if (m_enableAutStarterLogCheckbox) { 
            m_addAutStarterLogCheckbox
                .setSelection(m_addAutStarterLogCheckboxSelected);
        } else {
            m_addAutStarterLogLabel.setEnabled(false);
            m_addAutStarterLogCheckbox.setEnabled(false);
        }
        
        // for 'add client log' 
        m_addClientLogLabel = new Label(area, SWT.NONE);
        m_addClientLogLabel.setText(
            Messages.SupportRequestMailDialogAddClientLogLabel);
        m_addClientLogCheckbox = new Button(area, SWT.CHECK);
        if (m_enableClientLogCheckbox) { 
            m_addClientLogCheckbox.setSelection(m_addClientLogCheckboxSelected);
        } else {
            m_addClientLogLabel.setEnabled(false);
            m_addClientLogCheckbox.setEnabled(false);
        }
        
        // for 'add projects' 
        m_addProjectsLabel = new Label(area, SWT.NONE);
        m_addProjectsLabel.setText(
            Messages.SupportRequestMailDialogAddProjectsLabel);
        m_addProjectsCheckbox = new Button(area, SWT.CHECK);
        if (GeneralStorage.getInstance().getProject() != null) { 
            m_addProjectsCheckbox.setSelection(m_addProjectsCheckboxSelected);
        } else {
            m_addProjectsLabel.setEnabled(false);
            m_addProjectsCheckbox.setEnabled(false);
        }

        
    }
    
    /**
     * {@inheritDoc}
     */
    protected void okPressed() {
        m_addConfigInfoCheckboxSelected = 
            m_addConfigInfoCheckbox.getSelection();
        m_addAutStarterLogCheckboxSelected = 
            m_addAutStarterLogCheckbox.getSelection();
        m_addClientLogCheckboxSelected = m_addClientLogCheckbox.getSelection();
        m_addProjectsCheckboxSelected = m_addProjectsCheckbox.getSelection();
        m_packageDestination = new File(
                new File(m_packageDestinationTextField.getText()), 
                m_packageFileName);
        super.okPressed();
    }

    /**
     * @return the destination for the generated Support Information Package.
     */
    public File getPackageDestination() {
        return m_packageDestination;
    }

    /**
     * @return A boolean indicating whether the 'add aut starter log' checkbox
     *         was selected while leaving the dialog by pressing 'Ok'.
     */
    public boolean isAutStarterLogCheckboxSelected() {
        return m_addAutStarterLogCheckboxSelected;
    }

    /**
     * @return A boolean indicating whether the 'add client log' checkbox
     *         was selected while leaving the dialog by pressing 'Ok'.
     */
    public boolean isClientLogCheckboxSelected() {
        return m_addClientLogCheckboxSelected;
    }

    /**
     * @return A boolean indicating whether the 'add project' checkbox
     *         was selected while leaving the dialog by pressing 'Ok'.
     */
    public boolean isProjectsCheckboxSelected() {
        return m_addProjectsCheckboxSelected;
    }

    /**
     * @return A boolean indicating whether the 'configuration information' checkbox
     *         was selected while leaving the dialog by pressing 'Ok'.
     */
    public boolean isConfigInfoCheckboxSelected() {
        return m_addConfigInfoCheckboxSelected;
    }
}
    

