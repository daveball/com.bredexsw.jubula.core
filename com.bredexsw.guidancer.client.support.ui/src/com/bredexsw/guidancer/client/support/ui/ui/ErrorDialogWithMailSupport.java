/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13224 $
 *
 * $Date: 2010-11-22 13:52:11 +0100 (Mon, 22 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package com.bredexsw.guidancer.client.support.ui.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jubula.client.ui.constants.IconConstants;
import org.eclipse.jubula.client.ui.rcp.Plugin;
import org.eclipse.jubula.client.ui.utils.CommandHelper;
import org.eclipse.jubula.tools.internal.i18n.I18n;
import org.eclipse.jubula.tools.internal.utils.EnvironmentUtils;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


/**
 * @author markus
 * @created 20.02.2009
 */
public class ErrorDialogWithMailSupport extends ErrorDialog {
    /** the ID of the "request support mail" command */
    private static final String REQUEST_SUPPORT_MAIL_COMMAND_ID = "com.bredexsw.guidancer.client.gui.commands.supportRequestMail"; //$NON-NLS-1$
    
    /**
     * @param parentShell
     *            the shell under which to create this dialog
     * @param dialogTitle
     *            the title to use for this dialog, or <code>null</code> to
     *            indicate that the default title should be used
     * @param message
     *            the message to show in this dialog, or <code>null</code> to
     *            indicate that the error's message should be shown as the
     *            primary message
     * @param status
     *            the error to show to the user
     * @param displayMask
     *            the mask to use to filter the displaying of child items, as
     *            per <code>IStatus.matches</code>
     * @see org.eclipse.core.runtime.IStatus#matches(int)
     */
    public ErrorDialogWithMailSupport(Shell parentShell, String dialogTitle,
            String message, IStatus status, int displayMask) {
        super(parentShell, dialogTitle, message, status, displayMask);
    }

    /**
     * {@inheritDoc}
     */
    protected void createButtonsForButtonBar(Composite parent) {
        super.createButtonsForButtonBar(parent);
        
        // currently our jdic library does not support MacOS
        if (EnvironmentUtils.isMacOS()) {
            return;
        }
        
        Button sendMail = createButton(parent, 4711, 
                I18n.getString("ErrorDialog.email"), false); //$NON-NLS-1$
        final Image mailImage = new Image(Display.getCurrent(), 
                IconConstants.MAIL.createImage()
                    .getImageData().scaledTo(14, 14));
        sendMail.setImage(mailImage);
        sendMail.addSelectionListener(new SelectionListener() {
            public void widgetDefaultSelected(SelectionEvent e) {
                Plugin.getDisplay().asyncExec(new Runnable() {
                    public void run() {
                        CommandHelper.executeCommand(
                                REQUEST_SUPPORT_MAIL_COMMAND_ID);
                    }
                });
                setReturnCode(OK);
                close();
                mailImage.dispose();
            }

            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }
        });
    }
}
