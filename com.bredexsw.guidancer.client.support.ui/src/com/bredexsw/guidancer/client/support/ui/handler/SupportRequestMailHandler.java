/*
 * $RCSfile: eclipse_3_codetemplates.xml,v $
 *
 * $Revision: 13224 $
 *
 * $Date: 2010-11-22 13:52:11 +0100 (Mon, 22 Nov 2010) $
 *
 * $Author: markus $ BREDEX GmbH (http://www.bredex.de) Copyright(c) 2009 
 * 
 */
package com.bredexsw.guidancer.client.support.ui.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.Collator;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.window.Window;
import org.eclipse.jubula.client.archive.XmlStorage;
import org.eclipse.jubula.client.core.model.IProjectPO;
import org.eclipse.jubula.client.core.model.IReusedProjectPO;
import org.eclipse.jubula.client.core.persistence.GeneralStorage;
import org.eclipse.jubula.client.core.persistence.Persistor;
import org.eclipse.jubula.client.core.persistence.ProjectPM;
import org.eclipse.jubula.client.internal.AutAgentConnection;
import org.eclipse.jubula.client.internal.exceptions.ConnectionException;
import org.eclipse.jubula.client.ui.handlers.AbstractHandler;
import org.eclipse.jubula.client.ui.rcp.Plugin;
import org.eclipse.jubula.client.ui.rcp.businessprocess.ExportAllBP;
import org.eclipse.jubula.client.ui.rcp.businessprocess.ShowClientLogBP;
import org.eclipse.jubula.client.ui.rcp.businessprocess.ShowServerLogBP;
import org.eclipse.jubula.client.ui.utils.DialogUtils;
import org.eclipse.jubula.client.ui.utils.ErrorHandlingUtil;
import org.eclipse.jubula.communication.internal.message.ServerLogResponseMessage;
import org.eclipse.jubula.tools.internal.constants.EnvConstants;
import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.eclipse.jubula.tools.internal.exception.JBException;
import org.eclipse.jubula.tools.internal.i18n.I18n;
import org.eclipse.jubula.tools.internal.messagehandling.MessageIDs;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.about.ISystemSummarySection;
import org.eclipse.ui.internal.IWorkbenchConstants;
import org.eclipse.ui.internal.WorkbenchMessages;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.registry.IWorkbenchRegistryConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bredexsw.guidancer.client.support.ui.dialogs.SupportInfoPackageDialog;
import com.bredexsw.guidancer.client.support.ui.i18n.Messages;

/**
 * This handler shows a configuration dialog to choose mail attachments and 
 * opens afterwards the system mail client with preconfigurations (including 
 * the chosen attachments and a message template), so that the user only has to 
 * complete the message for the GUIdancer support team and press 'send' button.
 * 
 * The three possible attachment are:
 * - AUT-Agent log file (only if connection to AUT-Agent exists)
 * - Client log file
 * - Current active project and all depending projects (only if there is a 
 *   currently active project)
 *   
 * NOTE: Each attachment is attached as ZIP file.
 * 
 * @author BREDEX GmbH
 * @created 20.02.2009
 */
public class SupportRequestMailHandler extends AbstractHandler {

    /**
     * Name of the <code>sectionTitle</code> attribute.
     */
    private static final String SECTION_TITLE_ATTR_NAME = "sectionTitle"; //$NON-NLS-1$
    /**
     * Name of the <code>id</code> attribute.
     */
    private static final String ID_ATTR_NAME = "id"; //$NON-NLS-1$
    
    /** the logger */
    private static Logger log = 
        LoggerFactory.getLogger(SupportRequestMailHandler.class);
    
    /**
     * Inner class for exporting the current project and all depending projects.
     * 
     * @author BREDEX GmbH
     * @created Apr 8, 2008
     */
    private static final class ExportOperation 
        implements IRunnableWithProgress {
        
        /** list of written project files */
        private List<File> m_listOfProjectFiles;

        /**
         * Constructor
         * 
         * @param listOfProjectFiles The written project files are added to 
         *                           this list.
         */
        ExportOperation(List<File> listOfProjectFiles) {
            m_listOfProjectFiles = listOfProjectFiles;
        }
        
        /**
         * {@inheritDoc}
         */
        public void run(IProgressMonitor monitor) {        
            ExportAllBP.getInstance().showStartingExport();
            Persistor hib = Persistor.instance();
            EntityManager exportSession = null;
            try {
                exportSession = hib.openSession();
                
                List<IProjectPO> projectList = 
                    buildDependingProjectList(exportSession);
                
                // export current project and all projects it depends on
                ExportAllBP.getInstance().exportProjectList(projectList,
                        StringConstants.EMPTY, exportSession, monitor, true,
                        m_listOfProjectFiles);

                ExportAllBP.getInstance().showFinishedExport();
            } catch (JBException gde) {
                log.error("Export aborted.", gde); //$NON-NLS-1$
                ExportAllBP.getInstance().showAbortExport(gde);
                m_listOfProjectFiles.clear();
            } catch (InterruptedException ie) {
                ExportAllBP.getInstance().showCancelExport();
                m_listOfProjectFiles.clear();
            } finally {
                hib.dropSession(exportSession);
                Plugin.stopLongRunning();
                monitor.done();
            }
        }
        
        /**
         * Builds a list of projects consisting of the current project and all 
         * projects it depends on recursive.
         * The current project will be the first in the list.
         * @param exportSession The session to be used for Persistence (JPA / EclipseLink)
         * @return A list containing current project and all depending projects
         */
        private List<IProjectPO> buildDependingProjectList(
                EntityManager exportSession) 
            throws JBException {
            
            IProjectPO currentProject = 
                GeneralStorage.getInstance().getProject();
            List<IProjectPO> projectList = new ArrayList<IProjectPO>();
            Map<Long, Boolean> projectMap = new Hashtable<Long, Boolean>();
            
            buildDependingProjectListSub(
                    projectList, currentProject, exportSession, projectMap);
            
            return projectList;
        }
        
        /**
         * Builds a list of projects consisting of all projects from the given 
         * list, the given project and all projects it depends on recursive.
         * @param projectList The interim content of the list of projects 
         *                    consisting of the current project and all 
         *                    projects it depends on.
         * @param project The project for that all depending projects have to 
         *                be added to projectList.
         * @param exportSession The session to be used for Persistence (JPA / EclipseLink)
         * @param projectMap Consists of all projects that already have been 
         *                   exported. (NOTE: Needed to avoid duplicates.)
         */
        private void buildDependingProjectListSub(List<IProjectPO> projectList,
                IProjectPO project, EntityManager exportSession, 
                Map<Long, Boolean> projectMap) throws JBException {
            
            if (!projectMap.containsKey(project.getId())) {
                projectMap.put(project.getId(), Boolean.TRUE);
                projectList.add(project);
                Set<IReusedProjectPO> usedProjectList = 
                    project.getUsedProjects();
                
                for (IReusedProjectPO usedProject : usedProjectList) {
                    IProjectPO subProject = 
                        ProjectPM.loadReusedProject(usedProject, exportSession);
                    buildDependingProjectListSub(
                            projectList, subProject, exportSession, projectMap);
                }
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public Object executeImpl(ExecutionEvent event) {
        // NOTE: ExportFileBP is used here, although the exporting is done with
        //       ExportAllBP, because it is enabled only if a project is opened. 
        //       Otherwise it is disabled.
        SupportInfoPackageDialog dialog = 
            new SupportInfoPackageDialog(getActiveShell(),
                    ShowServerLogBP.getInstance().isEnabled(),
                    ShowClientLogBP.getInstance().isEnabled());
        dialog.create();
        DialogUtils.setWidgetNameForModalDialog(dialog);
        dialog.open();
        
        if (dialog.getReturnCode() == Window.OK) {
            writeSupportInfoPackage(dialog);
        }
        return null;
    }

    /**
     * Creates a Support Information Package configured by the given dialog. 
     * The Package is then written to the file system at a location determined
     * by the given dialog.
     * 
     * @param dialog The configuration dialog that was used to configure the
     *               generation of the Support Information Package. 
     */
    private void writeSupportInfoPackage(SupportInfoPackageDialog dialog) {
        boolean addAutStarterLog = dialog.isAutStarterLogCheckboxSelected();
        boolean addClientLog = dialog.isClientLogCheckboxSelected();
        boolean addProjects = dialog.isProjectsCheckboxSelected();
        boolean addConfigInformation = dialog.isConfigInfoCheckboxSelected();
        File packageDestination = dialog.getPackageDestination();

        if (packageDestination.exists()) {
            MessageBox mb = new MessageBox(getActiveShell(),
                    SWT.ICON_WARNING | SWT.YES | SWT.NO);
            mb.setText(Messages.SupportInfoPackageOverwriteDialogTitle);
            mb.setMessage(NLS.bind(
                    Messages.SupportInfoPackageOverwriteDialogMsg,
                    packageDestination));
            if (mb.open() == SWT.NO) {
                return;
            }
        }
        
        try (ZipOutputStream zipOutput = new ZipOutputStream(
                new FileOutputStream(packageDestination))) {
            // create all files that have to be attached
            if (addAutStarterLog) {
                addAutStarterLog("agent/", zipOutput); //$NON-NLS-1$
            }        
            if (addClientLog) {
                addClientLog("client/", zipOutput); //$NON-NLS-1$
            }
            if (addProjects) {
                addProjects("projects/", zipOutput); //$NON-NLS-1$
            }
            if (addConfigInformation) {
                addConfigInfo("config/", zipOutput); //$NON-NLS-1$
            }
            
        } catch (SecurityException ex) {
            log.error("Error occurred while writing Support Information Package.", ex); //$NON-NLS-1$
            ErrorHandlingUtil.createMessageDialog(
                MessageIDs.E_ADD_ATTACHMENTS_TO_MAIL_FAILED);
        } catch (IOException ex) {
            log.error("Error occurred while writing Support Information Package.", ex); //$NON-NLS-1$
            ErrorHandlingUtil.createMessageDialog(
                MessageIDs.E_ADD_ATTACHMENTS_TO_MAIL_FAILED);
        }
    }
    
    /**
     * 
     * @param entryPrefix The prefix to use for the created ZIP entries.
     * @param zipOutput The output stream to which to write the ZIP entries.
     * @throws IOException if an error occurs while writing to the stream.
     */
    private void addAutStarterLog(String entryPrefix, ZipOutputStream zipOutput)
        throws IOException {
        
        // add AUT-Agent log file to attachment list, if it is possible
        ServerLogResponseMessage response = 
            ShowServerLogBP.getInstance().requestServerLog();
        if ((response == null)
            || (response.getStatus() != ServerLogResponseMessage.OK)) {
            
            log.error("Read AUT Agent log failed."); //$NON-NLS-1$
            ErrorHandlingUtil.createMessageDialog(
                MessageIDs.E_ADD_ATTACHMENTS_TO_MAIL_FAILED);
        } else {
            String serverLog = response.getServerLog();
            ZipEntry zipEntry = new ZipEntry(entryPrefix + "AUTAgent.log"); //$NON-NLS-1$
            zipOutput.putNextEntry(zipEntry);
            zipOutput.write(serverLog.getBytes());
        }
        
    }

    /**
     * 
     * @param entryPrefix The prefix to use for the created ZIP entries.
     * @param zipOutput The output stream to which to write the ZIP entries.
     * @throws IOException if an error occurs while writing to the stream.
     */
    private void addClientLog(String entryPrefix, ZipOutputStream zipOutput) 
        throws IOException {
        
        // add client log file to attachment list, if it is possible
        File clientLogFile = 
            ShowClientLogBP.getInstance().getClientLogFile();
        if (clientLogFile == null) {
            log.error("Read client log failed."); //$NON-NLS-1$
            ErrorHandlingUtil.createMessageDialog(
                MessageIDs.E_ADD_ATTACHMENTS_TO_MAIL_FAILED);
        } else {
            try (Scanner scanner = new Scanner(clientLogFile)) {
                StringBuilder sb = new StringBuilder();
                while (scanner.hasNextLine()) {
                    sb.append(scanner.nextLine()).append('\n');
                }
                
                zipFile(zipOutput, sb.toString().getBytes(), 
                        entryPrefix + clientLogFile.getName());
            }
            
        }
    }

    /**
     * 
     * @param entryPrefix The prefix to use for the created ZIP entries.
     * @param zipOutput The output stream to which to write the ZIP entries.
     * @throws IOException if an error occurs while writing to the stream.
     */
    private void addConfigInfo(String entryPrefix, ZipOutputStream zipOutput) 
        throws IOException {
        
        ZipEntry zipEntry = new ZipEntry(entryPrefix + "configuration.txt"); //$NON-NLS-1$
        zipOutput.putNextEntry(zipEntry);
        zipOutput.write(getSystemSummary().getBytes());
    }

    /**
     * 
     * @param entryPrefix The prefix to use for the created ZIP entries.
     * @param zipOutput The output stream to which to write the ZIP entries.
     * @throws IOException if an error occurs while writing to the stream.
     */
    private void addProjects(String entryPrefix, ZipOutputStream zipOutput) 
        throws IOException {
        
        try {
            List<File> listOfProjectFiles = new ArrayList<File>();
            
            if (Plugin.getDefault().showSaveEditorDialog(getActiveShell())) {
                performExportOperation(listOfProjectFiles);
            } else {
                throw new InterruptedException("Interrupted by dirty editors."); //$NON-NLS-1$
            }
            
            zipListOfProjectFiles(
                    listOfProjectFiles, entryPrefix,
                    entryPrefix + "dependingProjects/", zipOutput); //$NON-NLS-1$
        } catch (InterruptedException ex) {
            log.error("Zip projects was interrupted.", ex); //$NON-NLS-1$
            ErrorHandlingUtil.createMessageDialog(
                MessageIDs.E_ADD_ATTACHMENTS_TO_MAIL_FAILED);
        } finally {
            Plugin.stopLongRunning();
        }
        
    }
    
    /**
     * @param listOfProjectFiles The written project files are added to 
     *                           this list.
     */
    private void performExportOperation(List<File> listOfProjectFiles)
        throws InterruptedException {
        
        IRunnableWithProgress exportOperation = 
            new ExportOperation(listOfProjectFiles);
        
        try {
            PlatformUI.getWorkbench().getProgressService()
                .busyCursorWhile(exportOperation);
        } catch (InvocationTargetException ite) {
            log.error(ite.getLocalizedMessage(), ite);
        } catch (InterruptedException ie) {
            throw ie;
        }
    }
    
    /**
     * 
     * @param listOfProjectFiles The list of project files to be zipped
     * @param pathPrefixForFirst The path prefix to be added inside ZIP file
     *                           to the first file name of the list
     * @param pathPrefixForOthers The path prefix to be added inside ZIP file
     *                            to all other file names of the list
     * @param zipOutput The output stream to which the projects will be written
     * @throws IOException if an error occurs while writing to the stream.
     */
    private void zipListOfProjectFiles(List<File> listOfProjectFiles, 
            String pathPrefixForFirst, String pathPrefixForOthers,
            ZipOutputStream zipOutput) throws IOException {
        
        for (File file : listOfProjectFiles) {
            URL fileURL = file.toURI().toURL();
            XmlStorage.checkCharacterEncoding(fileURL);
            final String fileNameToUse;
            final String fileName = file.getName();
            if (file == listOfProjectFiles.get(0)) {
                fileNameToUse = pathPrefixForFirst + fileName;
            } else {
                fileNameToUse = pathPrefixForOthers + fileName;
            }
            InputStream is = null;
            try {
                is = fileURL.openStream();
                zipFile(zipOutput, IOUtils.toByteArray(is), fileNameToUse);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }

    /**
     * Writes the given file to the given output stream.
     * 
     * @param zipOutput The output stream to write to.
     * @param fileContent The contents to write to the stream as a 
     *                    compressed file.
     * @param fileNameToUse The name of the ZIP entry containing the file 
     *                      contents.
     */
    private void zipFile(ZipOutputStream zipOutput, byte [] fileContent,
            final String fileNameToUse) throws IOException {

        zipOutput.putNextEntry(new ZipEntry(fileNameToUse));
        zipOutput.write(fileContent);
        zipOutput.closeEntry();
    
    }
    
    /**
     * Appends the contents of all extensions to the configurationLogSections
     * extension point to the given writer. Basically, this produces the same
     * text found in Help -> About -> Configuration details.
     * 
     * @param writer The writer to which the text will be appended.
     */
    private void appendExtensions(PrintWriter writer) {
        IConfigurationElement[] configElements = getSortedExtensions();
        for (int i = 0; i < configElements.length; ++i) {
            IConfigurationElement element = configElements[i];

            Object obj = null;
            try {
                obj = WorkbenchPlugin.createExtension(element,
                        IWorkbenchConstants.TAG_CLASS);
            } catch (CoreException e) {
                log.error("could not create class attribute for extension", //$NON-NLS-1$
                        e);
            }

            writer.println();
            writer.println(NLS.bind(
                    WorkbenchMessages.SystemSummary_sectionTitle, 
                    element.getAttribute(SECTION_TITLE_ATTR_NAME)));

            if (obj instanceof ISystemSummarySection) {
                ISystemSummarySection logSection = (ISystemSummarySection) obj;
                logSection.write(writer);
            } else {
                writer.println(WorkbenchMessages.SystemSummary_sectionError);
            }
        }
    }

    /**
     * 
     * @return a group of system summary elements.
     */
    private IConfigurationElement[] getSortedExtensions() {
        IConfigurationElement[] configElements = Platform
                .getExtensionRegistry().getConfigurationElementsFor(
                        PlatformUI.PLUGIN_ID,
                        IWorkbenchRegistryConstants.PL_SYSTEM_SUMMARY_SECTIONS);

        Arrays.sort(configElements, new Comparator<IConfigurationElement>() {
            private Collator m_collator = 
                Collator.getInstance(Locale.getDefault());

            public int compare(IConfigurationElement element1, 
                    IConfigurationElement element2) {

                String id1 = element1.getAttribute(ID_ATTR_NAME);
                String id2 = element2.getAttribute(ID_ATTR_NAME);

                if (id1 != null && id2 != null && !id1.equals(id2)) {
                    return m_collator.compare(id1, id2);
                }

                String title1 = StringUtils.defaultString(
                        element1.getAttribute(SECTION_TITLE_ATTR_NAME));
                String title2 = StringUtils.defaultString(
                        element2.getAttribute(SECTION_TITLE_ATTR_NAME));

                return m_collator.compare(title1, title2);
            }
        });

        return configElements;
    }

    /**
     * 
     * @return a large amount of platform configuration information. This text
     *         is about the same as that found in 
     *         Help -> About -> Configuration details. 
     */
    private String getSystemSummary() {
        StringWriter out = new StringWriter();
        PrintWriter writer = new PrintWriter(out);
        writer.println(NLS.bind(WorkbenchMessages.SystemSummary_timeStamp,
                DateFormat
                        .getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)
                        .format(new Date())));  

        appendExtensions(writer);

        // AutStarter connection? (localhost/not localhost/no connection)
        writer.println();
        String connectionType = StringConstants.EMPTY;
        try {
            if (AutAgentConnection.getInstance().isConnected()) {
                InetAddress connectedServer = InetAddress.getByName(
                        AutAgentConnection.getInstance()
                        .getCommunicator().getHostName());
                if (EnvConstants.LOCALHOST_FQDN.equals(connectedServer)) {
                    connectionType = I18n.getString(
                            "SupportRequestMail.LocalhostConnection"); //$NON-NLS-1$
                } else {
                    connectionType = I18n.getString(
                            "SupportRequestMail.NotLocalhostConnection"); //$NON-NLS-1$
                }
            } else {
                connectionType = I18n.getString(
                        "SupportRequestMail.NoConnection"); //$NON-NLS-1$
            }
        } catch (ConnectionException ce) {
            // Not connected
            connectionType = I18n.getString(
                    "SupportRequestMail.NoConnection"); //$NON-NLS-1$
        } catch (UnknownHostException e) {
            // Host does not exist for connected server
            // This should NOT happen, but if it does, make a note of it
            connectionType = I18n.getString(
                    "SupportRequestMail.UnknownHost"); //$NON-NLS-1$
        }        
        writer.print(I18n.getString(
                "SupportRequestMail.AutStarterConnection")); //$NON-NLS-1$
        writer.println(connectionType);
        writer.close();
        return out.toString();
    }
}
