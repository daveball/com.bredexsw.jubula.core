#!/bin/bash -eu
# Retrieves the latest Jubula build and publishes it for use
# in EPP package aggregation.

# make sure exactly the right number of  args are supplied
# and print usage if not
if [ $# -ne 1 ]
then
    echo "Error in $0 - Invalid Argument Count"
    echo "Usage: $0 committer_id"
    exit 1
fi

ARCHIVE="site-packed.zip"
RELEASE="indigo"
COMMITTER_ID=$1
STAGING="staging"
JUBULA_PATH="/home/data/users/${COMMITTER_ID}/downloads/jubula/release/"
RELEASE_PATH="${JUBULA_PATH}${RELEASE}"
STAGING_PATH="${JUBULA_PATH}${RELEASE}"
WORKING_DIRECTORY=`pwd`

if [ -e ${ARCHIVE} ]; then
  echo "Archive exists: ${WORKING_DIRECTORY}/${ARCHIVE}"
  echo "Backup this archive (if necessary) and delete it before running this script again."
  exit 1
fi

# retrieve
wget --no-clobber https://hudson.eclipse.org/hudson/view/Tycho%20+%20Maven/job/jubula-${RELEASE}/lastSuccessfulBuild/artifact/org.eclipse.jubula.site/target/${ARCHIVE}

# copy
scp -P 2405 ${ARCHIVE} ${COMMITTER_ID}@gatekeeper.bredex.de:${STAGING_PATH}

#local cleanup
rm ${ARCHIVE}

# publish
ssh -p 2405 ${COMMITTER_ID}@gatekeeper.bredex.de 'bash -s' < publish.sh ${COMMITTER_ID} ${RELEASE_PATH} ${ARCHIVE} ${STAGING_PATH}
