/**
 * 
 */
package org.eclipse.jubula.qa.agentcheck;

import java.util.Date;

import org.eclipse.jubula.qa.agentcheck.util.AvailablePortFinder;

/**
 * @author BREDEX Gmbh
 */
public class App {
    /**
     * the start time
     */
    private static final Date START_DATE = new Date();

    /**
     * @param args
     */
    public static void main(String[] args) {
        if ((args.length != 3) || (!args[2].equals("start") && !args[2].equals("stop"))) {
            System.err
                    .println("Usage: java -jar agentcheck.jar <portnumber> <timeout in seconds> start|stop");
            System.exit(-1);
        }
        int portNum = 0;
        try {
            portNum = Integer.parseInt(args[0]);
            int timeout = Integer.parseInt(args[1]);
            long end = START_DATE.getTime() + timeout * 1000;
            boolean start = (args[2].equals("start"));
            do {
                if (start) {
                    probeAgent4Startup(portNum);
                } else {
                    probeAgent4Shutdown(portNum);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // just continue
                }
            } while (new Date().getTime() <= end);
        } catch (NumberFormatException e) {
            System.err
                    .println("Data format error; usage: java -jar agentcheck.jar <portnumber> <timeout in seconds>");
            System.exit(-1);
        }
        System.err.println("Can't connect to AUTAgent, check port (" + portNum
                + ") and state.");
        System.exit(-1);
    }

    /**
     * If all is well this method will not return but exit the application with
     * state 0 (success).
     * 
     * @param portNum
     *            The network port number to connect to.
     */
    private static void probeAgent4Startup(int portNum) {
        if (!AvailablePortFinder.available(portNum)) {
            available(true);
            System.exit(0);
        }
    }
    
    /**
     * If all is well this method will not return but exit the application with
     * state 0 (success).
     * 
     * @param portNum
     *            The network port number to connect to.
     */
    private static void probeAgent4Shutdown(int portNum) {
        if (AvailablePortFinder.available(portNum)) {
            available(false);
            System.exit(0);
        }
    }
    
    /**
     * Communicate the status
     * @param availableStatus the status
     */
    private static void available(boolean availableStatus) {
        Date now = new Date();
        long delay = (now.getTime() - START_DATE.getTime()) / 1000l;
        String status = availableStatus ? "available" : "not available";
        System.out.println("AUT-Agent " + status + " after " + delay
                + " seconds.");
    }
}
