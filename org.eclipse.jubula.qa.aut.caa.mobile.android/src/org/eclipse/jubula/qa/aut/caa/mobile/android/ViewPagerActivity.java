package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * 
 * Fragment activity with multiple pages. Swipe changes to other sites.
 * 
 * @author soeren
 * 
 */
public class ViewPagerActivity extends FragmentActivity {

    /** the number of pages */
    private static final int PAGE_COUNT = 5;
    /** the pager widget handles animation swiping */
    private ViewPager m_pager;
    /** the pager adapter provides the pages to the view pager widget */
    private PagerAdapter m_pagerAdapter;
    /** the back button */
    private Button m_btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        // init
        m_pager = (ViewPager) findViewById(R.id.act_viewpager_pager);
        m_pagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        m_pager.setAdapter(m_pagerAdapter);
        m_btnBack = (Button) findViewById(R.id.act_viewpager_btnBack);
        m_btnBack.setOnClickListener(new OnClickListener() {
            
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * A pager adapter that represents 5 ViewPagerFragment objects.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        /**
         * Constructor for the adapter.
         * 
         * @param fm
         *            - the fragment manager
         */
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @SuppressWarnings("static-access")
        @Override
        public Fragment getItem(int position) {
            return new ViewPagerFragment().create(position);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }
}
