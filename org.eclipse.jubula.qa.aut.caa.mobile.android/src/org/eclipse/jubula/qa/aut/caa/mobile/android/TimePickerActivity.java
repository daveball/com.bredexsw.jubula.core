package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

/**
 * Activiy contains all variations of android.widget.TimePicker.
 * 
 * @author soeren
 * 
 */
public class TimePickerActivity extends Activity {

    /** the back button */
    private Button m_btnBack;
    /** the selected item text */
    private TextView m_resultTxt;
    /** timepicker 1 */
    private TimePicker m_timePicker1;
    /** timepicker 2 */
    private TimePicker m_timePicker2;
    /** time changed listener */
    private OnTimeChangedListener m_onTimeChangedListener;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timepicker);
        initComponents();
        initActions();
    }

    /**
     * Show the time picker dialog.
     * 
     * @param v
     *            - the view
     */
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment(m_timePicker1);
        newFragment.show(getFragmentManager(), "timePicker");
    }
    
    /**
     * Init all components of the activity.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_timepicker_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_timepicker_txtView);
        m_timePicker1 = (TimePicker) findViewById(R.id.
                act_timepicker_timePicker1);
        m_timePicker2 = (TimePicker) findViewById(R.id.
                act_timepicker_timePicker2);
        m_onTimeChangedListener = new OnTimeChangedListener() {
            
            public void onTimeChanged(TimePicker view, int hourOfDay,
                    int minute) {
                m_resultTxt.setText(hourOfDay + " hour, " + minute 
                        + " minutes");
            }
        };
    }

    /**
     * Register listeners on all components of the activity.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_timePicker2.setEnabled(false);
        m_timePicker1.setCurrentHour(1);
        m_timePicker1.setCurrentMinute(23);
        m_timePicker2.setCurrentHour(4);
        m_timePicker2.setCurrentMinute(56);
        
        m_timePicker1.setOnTimeChangedListener(m_onTimeChangedListener);
        m_timePicker2.setOnTimeChangedListener(m_onTimeChangedListener);
    }

    /**
     * The timepicker dialog fragment.
     * 
     * @author soeren
     *
     */
    @SuppressLint("ValidFragment")
    static class TimePickerFragment extends DialogFragment implements
            OnTimeSetListener {

        /** the time picker */
        private TimePicker m_timePicker;

        /**
         * Create a new TimePickerDialogFragment.
         * 
         * @param timePicker
         *            - the timepicker to set
         */
        public TimePickerFragment(TimePicker timePicker) {
            this.m_timePicker = timePicker;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, m_timePicker
                    .getCurrentHour(), m_timePicker.getCurrentMinute(),
                    DateFormat.is24HourFormat(getActivity()));
        }

        /**
         * Called when the time is set.
         * 
         * @param view
         *            - the view
         * @param hourOfDay
         *            - the chosen hour
         * @param minute
         *            - the chosen minute
         */
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            m_timePicker.setCurrentHour(hourOfDay);
            m_timePicker.setCurrentMinute(minute);
        }
    }
}
