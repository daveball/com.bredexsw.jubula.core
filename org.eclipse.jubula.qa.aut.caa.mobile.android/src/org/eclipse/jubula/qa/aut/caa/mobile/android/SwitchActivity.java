package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Activity contains all android.widget.switch variations.
 * 
 * @author soeren
 * 
 */
public class SwitchActivity extends Activity {

    /** back button */
    private Button m_btnBack;
    /** the resulttext */
    private TextView m_resultTxt;
    // switch 1 is completely init in activity_switch.xml
    /** switch 2 */
    private Switch m_switch2;
    /** switch 3 */
    private Switch m_switch3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch);

        initComponents();
        initActions();

    }

    /**
     * Callback for the switch selection.
     * 
     * @param v
     *            - the selected view
     */
    public void onSwitchSelected(View v) {
        // Is the switch now checked?
        boolean checked = ((Switch) v).isChecked();
        switch (v.getId()) {
            case R.id.act_switch_switch1:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_switch_switch1On));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_switch_switch1Off));
                }
                break;
            case R.id.act_switch_switch2:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_switch_switch2On));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_switch_switch2Off));
                }
                break;
            case R.id.act_switch_switch3:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_switch_switch3On));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_switch_switch3Off));
                }
                break;

            default:
                break;
        }
    }

    /**
     * Init actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_resultTxt.setEnabled(false);
        m_switch2.setEnabled(false);
        m_switch3.setClickable(false);

    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_switch_txtView);
        m_btnBack = (Button) findViewById(R.id.act_switch_btnBack);
        m_switch2 = (Switch) findViewById(R.id.act_switch_switch2);
        m_switch3 = (Switch) findViewById(R.id.act_switch_switch3);
    }

}
