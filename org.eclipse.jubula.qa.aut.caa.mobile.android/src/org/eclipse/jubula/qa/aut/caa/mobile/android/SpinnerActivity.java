package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * 
 * Activity contains all android.widget.Spinner variations.
 * 
 * @author soeren
 * 
 */
public class SpinnerActivity extends Activity implements
    OnItemSelectedListener {

    /** spinner res id identifier */
    private static final String SPINNER_IDENTIFIER = "act_spinner_spinner";
    /** the result text */
    private TextView m_resultTxt;
    /** array of all spinners */
    private Spinner[] m_spinners = new Spinner[5];
    /** back button */
    private Button m_btnBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        initComponents();
        initActions();

    }

    /**
     * {@inheritDoc}
     */
    public void onItemSelected(AdapterView<?> parent, View view, int position,
            long id) {
        m_resultTxt.setText(parent.getItemAtPosition(position).toString());
    }

    /**
     * {@inheritDoc}
     */
    public void onNothingSelected(AdapterView<?> parent) {
        // empty

    }

    /**
     * Init all EditTexts and control widgets. Add text change listener to all
     * EditTexts.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_spinner_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_spinner_txtView);
        for (int i = 0; i < m_spinners.length; i++) {
            // init edittexts
            int spinnerId = i + 1;
            int resId = getResources().getIdentifier(
                    SPINNER_IDENTIFIER + spinnerId, "id", getPackageName());
            m_spinners[i] = (Spinner) findViewById(resId);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter
                    .createFromResource(this, R.array.act_spinner_planets,
                            android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.
                    simple_spinner_dropdown_item);
            m_spinners[i].setAdapter(adapter);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.act_spinner_planets,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        m_spinners[1].setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
                this, R.array.act_spinner_planets,
                android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        m_spinners[2].setAdapter(adapter2);

    }

    /**
     * Init actions / listeners.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_resultTxt.setEnabled(false);
        m_spinners[m_spinners.length - 1].setEnabled(false);
        for (int i = 0; i < m_spinners.length; i++) {
            m_spinners[i].setOnItemSelectedListener(this);
        }
    }

}
