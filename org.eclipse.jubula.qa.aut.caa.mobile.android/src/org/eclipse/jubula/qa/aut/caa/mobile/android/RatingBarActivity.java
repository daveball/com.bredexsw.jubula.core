package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

/**
 * 
 * Activity contains all android.widget.RatingBar variations.
 * 
 * @author soeren
 * 
 */
public class RatingBarActivity extends Activity {
    /** ratingbar res id identifier */
    private static final String RB_IDENTIFIER = "act_ratingbar_ratingbar";
    /** ratingbar count */
    private static final int RB_COUNTER = 4;
    /** array of all ratingbars */
    private RatingBar m_rb[] = new RatingBar[RB_COUNTER];
    /** back button */
    private Button m_btnBack;
    /** text of selected button */
    private TextView m_resultTxt;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratingbar);

        initComponents();
        initButtonActions();

    }

    /**
     * Init button actions.
     */
    private void initButtonActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_resultTxt.setEnabled(false);
        m_rb[1].setEnabled(false);
        m_rb[2].setClickable(false);
       
    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_ratingbar_txtView);
        m_btnBack = (Button) findViewById(R.id.act_ratingbar_btnBack);
        
        for (int i = 0; i < m_rb.length; i++) {
            // init ratingbars
            int rbId = i + 1;
            int resId = getResources().getIdentifier(RB_IDENTIFIER + rbId,
                    "id", getPackageName());
            m_rb[i] = (RatingBar) findViewById(resId);
            m_rb[i].setOnRatingBarChangeListener(new 
                    OnRatingBarChangeListener() {

                public void onRatingChanged(RatingBar ratingBar, float rating,
                        boolean fromUser) {
                    m_resultTxt.setText(Float.toString(rating));
                }
            });
        }
    }

}
