package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;

/**
 * Activiy contains all variations of android.widget.DatePicker.
 * 
 * @author soeren
 * 
 */
public class DatePickerActivity extends Activity {

    /** the back button */
    private Button m_btnBack;
    /** the selected item text */
    private TextView m_resultTxt;
    /** DatePicker 1 */
    private DatePicker m_datePicker1;
    /** DatePicker 2 */
    /** time changed listener */

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datepicker);
        initComponents();
        initActions();
    }

    /**
     * Init all components of the activity.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_datepicker_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_datepicker_txtView);
        m_datePicker1 = (DatePicker) findViewById(R.id.
                act_datepicker_datepicker1);
    }

    /**
     * Register listeners on all components of the activity.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_datePicker1.init(2000, 0, 1, new OnDateChangedListener() {

            public void onDateChanged(DatePicker view, int year,
                    int monthOfYear, int dayOfMonth) {
                m_resultTxt.setText(dayOfMonth + "." + ((int) monthOfYear + 1)
                        + "." + year);
            }
        });
    }
}
