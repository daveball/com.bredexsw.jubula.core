package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity contains all android.widget.EditText variations.
 * 
 * @author soeren
 * 
 */
public class EditTextActivity extends Activity {

    /** edittext count */
    private static final int TXT_FIELD_COUNT = 11;
    /** edittext res id identifier */
    private static final String TXT_IDENTIFIER = "act_txt_editTxt";
    /** array of all edittexts */
    private EditText[] m_editTxts = new EditText[TXT_FIELD_COUNT];
    /** the result text */
    private TextView m_resultTxt;
    /** back button */
    private Button m_btnBack;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edittext);
        initComponents();
        initActions();

    }

    /**
     * Init all EditTexts and control widgets. Add text change listener to all
     * EditTexts.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_txt_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_txt_txtView);
        for (int i = 0; i < m_editTxts.length; i++) {
            // init edittexts
            int resId = getResources().getIdentifier(TXT_IDENTIFIER + i, "id",
                    getPackageName());
            m_editTxts[i] = (EditText) findViewById(resId);
            m_editTxts[i].addTextChangedListener(new TextWatcher() {

                public void onTextChanged(CharSequence s, int start,
                        int before, int count) {
                    // empty
                }

                public void beforeTextChanged(CharSequence s, int start,
                        int count, int after) {
                    // empty
                }

                public void afterTextChanged(Editable s) {
                    m_resultTxt.setText(s);
                }
            });
        }
    }

    /**
     * Init actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_resultTxt.setEnabled(false);
        m_editTxts[10].setEnabled(false);
    }

}
