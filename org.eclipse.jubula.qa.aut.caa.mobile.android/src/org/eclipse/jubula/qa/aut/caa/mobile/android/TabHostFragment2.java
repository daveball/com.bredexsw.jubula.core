package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment2 for the TabHostActivity. Has nested fragments.
 * 
 * @author soeren
 * 
 */
public class TabHostFragment2 extends TabHostFragment1 {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        FragmentTabHost tabHost = new FragmentTabHost(getActivity());
        tabHost.setup(getActivity(), getChildFragmentManager(),
                R.id.menu_settings);
        // infos for nested fragments
        Bundle[] bundle = new Bundle[2];
        bundle[0] = new Bundle();
        bundle[1] = new Bundle();
        bundle[0].putString("key",
                getResources().getString(R.string.act_tabhost_fragment4));
        bundle[1].putString("key",
                getResources().getString(R.string.act_tabhost_fragment5));
        tabHost.addTab(
                tabHost.newTabSpec("4").setIndicator(
                        getResources()
                                .getString(R.string.act_tabhost_fragment4)),
                TabHostFragment1.class, bundle[0]);

        tabHost.addTab(
                tabHost.newTabSpec("5").setIndicator(
                        getResources()
                                .getString(R.string.act_tabhost_fragment5)),
                TabHostFragment1.class, bundle[1]);
        return tabHost;
    }
}
