package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TextView;

/**
 * 
 * Activity contains all android.widget.Button variations.
 * 
 * @author soeren
 * 
 */
public class NumberPickerActivity extends Activity {
    /** text of selected value */
    private TextView m_resultTxt;
    /** the back button */
    private Button m_btnBack;
    /** the numberpicker 1 */
    private NumberPicker m_numberPicker1;
    /** the numberpicker 2 */
    private NumberPicker m_numberPicker2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numpicker);

        initComponents();
        initActions();

    }

    /**
     * Init actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_numberPicker1.setOnValueChangedListener(new OnValueChangeListener() {

            public void onValueChange(NumberPicker picker, int oldVal,
                    int newVal) {
                m_resultTxt.setText("Picker1: " + newVal);
            }
        });
        
        m_numberPicker2.setOnValueChangedListener(new OnValueChangeListener() {

            public void onValueChange(NumberPicker picker, int oldVal,
                    int newVal) {
                m_resultTxt.setText("Picker2: " + newVal);
            }
        });
        m_numberPicker1.setMinValue(0);
        m_numberPicker1.setMaxValue(100);
        m_numberPicker2.setMinValue(0);
        m_numberPicker2.setMaxValue(100);
        m_numberPicker2.setEnabled(false);
    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_numpicker_resultTxt);
        m_btnBack = (Button) findViewById(R.id.act_numpicker_btnBack);
        m_numberPicker1 = (NumberPicker) findViewById(R.id.
                act_numpicker_picker1);
        m_numberPicker2 = (NumberPicker) findViewById(R.id.
                act_numpicker_picker2);
    }
}
