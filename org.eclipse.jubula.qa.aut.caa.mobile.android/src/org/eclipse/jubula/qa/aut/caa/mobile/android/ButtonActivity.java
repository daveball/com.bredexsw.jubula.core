package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ZoomControls;

/**
 * 
 * Activity contains all android.widget.Button variations.
 * 
 * @author soeren
 * 
 */
public class ButtonActivity extends Activity {
    /** button res id identifier */
    private static final String BTN_IDENTIFIER = "act_button_btn";
    /** how many buttons are in this activity */
    private static final int BTN_COUNTER = 5;
    /** array of all buttons */
    private Button m_buttons[] = new Button[BTN_COUNTER];
    /** zoom control 1 */
    private ZoomControls m_zoomCtrl1;
    /** zoom control 2 */
    private ZoomControls m_zoomCtrl2;
    /** zoom control 3 */
    private ZoomControls m_zoomCtrl3;
    /** back button */
    private Button m_btnBack;
    /** text of selected button */
    private TextView m_resultTxt;
    /** listener for each button */
    private OnClickListener m_onClickListener;
    /** listener for each zoom button */
    private OnClickListener m_onZoomListener;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        initComponents();
        initButtonActions();

    }

    /**
     * Init button actions.
     */
    private void initButtonActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_resultTxt.setEnabled(false);
        m_buttons[1].setEnabled(false);
        m_buttons[2].setClickable(false);
        m_zoomCtrl1.setOnZoomInClickListener(m_onZoomListener);
        m_zoomCtrl1.setOnZoomOutClickListener(m_onZoomListener);
        m_zoomCtrl1.getChildAt(0).setTag(
                getResources().getString(R.string.act_button_zoom1m));
        m_zoomCtrl1.getChildAt(1).setTag(
                getResources().getString(R.string.act_button_zoom1p));
        m_zoomCtrl2.setOnZoomInClickListener(m_onZoomListener);
        m_zoomCtrl2.setOnZoomOutClickListener(m_onZoomListener);
        m_zoomCtrl2.setEnabled(false);
        m_zoomCtrl2.getChildAt(0).setEnabled(false);
        m_zoomCtrl2.getChildAt(1).setEnabled(false);
        m_zoomCtrl2.getChildAt(0).setTag(
                getResources().getString(R.string.act_button_zoom2m));
        m_zoomCtrl2.getChildAt(1).setTag(
                getResources().getString(R.string.act_button_zoom2p));
        m_zoomCtrl3.setOnZoomInClickListener(m_onZoomListener);
        m_zoomCtrl3.setOnZoomOutClickListener(m_onZoomListener);
        m_zoomCtrl3.setClickable(false);
        m_zoomCtrl3.getChildAt(0).setClickable(false);
        m_zoomCtrl3.getChildAt(1).setClickable(false);
        m_zoomCtrl3.getChildAt(0).setTag(
                getResources().getString(R.string.act_button_zoom3m));
        m_zoomCtrl3.getChildAt(1).setTag(
                getResources().getString(R.string.act_button_zoom3p));
    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_button_txtView);
        m_btnBack = (Button) findViewById(R.id.act_button_btnBack);
        m_zoomCtrl1 = (ZoomControls) findViewById(R.id.
                act_button_zoomControls1);
        m_zoomCtrl2 = (ZoomControls) findViewById(R.id.
                act_button_zoomControls2);
        m_zoomCtrl3 = (ZoomControls) findViewById(R.id.
                act_button_zoomControls3);
        m_onClickListener = new OnClickListener() {

            public void onClick(View v) {
                m_resultTxt.setText(((Button) v).getText());
            }
        };
        m_onZoomListener = new OnClickListener() {

            public void onClick(View v) {
                m_resultTxt.setText(v.getTag().toString());
            }
        };
        for (int i = 0; i < m_buttons.length; i++) {
            // init buttons
            int btnId = i + 1;
            int resId = getResources().getIdentifier(BTN_IDENTIFIER + btnId,
                    "id", getPackageName());
            m_buttons[i] = (Button) findViewById(resId);
            m_buttons[i].setOnClickListener(m_onClickListener);
        }
    }

}
