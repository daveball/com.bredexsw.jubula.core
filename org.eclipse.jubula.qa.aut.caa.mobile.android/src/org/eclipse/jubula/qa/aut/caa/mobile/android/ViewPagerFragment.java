package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 
 * One Fragment for the ViewPagerActivity that displays text. Is created with
 * the Adapter in ViewPagerActivity.
 * 
 * @author soeren
 * 
 */
public class ViewPagerFragment extends Fragment {

    /** the argument key for the page number this fragment represents */
    public static final String ARG_PAGE = "page";
    /** the page number is set to the value for {@link #ARG_PAGE} */
    private int m_pageNumber;

    /**
     * Factory method for the fragment. Constructs a new fragment for the page
     * number.
     * 
     * @param pageNumber
     *            - the page number
     * @return the fragment
     */
    public static ViewPagerFragment create(int pageNumber) {
        ViewPagerFragment fragment = new ViewPagerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_pageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // inflate layout containing a title and body text
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_viewpager_page, container, false);

        // set the title view to show the page number
        ((TextView) rootView.findViewById(android.R.id.text1))
                .setText(getString(R.string.title_template_step,
                        m_pageNumber + 1));
        return rootView;
    }

    /**
     * Get the page number.
     * 
     * @return the page number
     */
    public int getPageNumber() {
        return m_pageNumber;
    }
}
