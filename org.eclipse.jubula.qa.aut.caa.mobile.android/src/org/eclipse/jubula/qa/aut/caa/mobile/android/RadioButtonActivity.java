package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * 
 * Activity contains all android.widget.Button variations.
 * 
 * @author soeren
 * 
 */
public class RadioButtonActivity extends Activity {

    /** radio button res id identifier */
    private static final String RADIO_IDENTIFIER = "act_radio_radioButton";
    /** radio button count */
    private static final int RADIO_COUNTER = 9;
    /** all radio buttons */
    private RadioButton m_radios[] = new RadioButton[RADIO_COUNTER];
    /** back button */
    private Button m_btnBack;
    /** the result text */
    private TextView m_resultTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radiobutton);

        initComponents();
        initActions();
    }

    /**
     * Check which radioButton has been selected and display the text.
     * 
     * @param view
     *            - the view
     */
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.act_radio_radioButton1:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn1)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton2:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn2)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton3:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn3)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton4:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn1)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton5:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn2)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton6:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn3)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton7:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn1)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton8:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn2)
                            + " win");
                }
                break;
            case R.id.act_radio_radioButton9:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_radio_radioBtn3)
                            + " win");
                }
                break;
            default:
                break;
        }
    }

    /**
     * Init actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_resultTxt.setEnabled(false);
        m_radios[3].setEnabled(false);
        m_radios[4].setEnabled(false);
        m_radios[5].setEnabled(false);
        m_radios[6].setClickable(false);
        m_radios[7].setClickable(false);
        m_radios[8].setClickable(false);
    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_radio_txtView);
        m_btnBack = (Button) findViewById(R.id.act_radio_btnBack);
        for (int i = 0; i < m_radios.length; i++) {
            // init radiobuttons
            int btnId = i + 1;
            int resId = getResources().getIdentifier(RADIO_IDENTIFIER + btnId,
                    "id", getPackageName());
            m_radios[i] = (RadioButton) findViewById(resId);
        }
    }

}
