package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Activity contains a multi select android.widget.ListView.
 * 
 * @author soeren
 * 
 */
public class MultiSelectListViewActivity extends ListActivity {

    /** back button */
    private Button m_btnBack;
    /** the result text */
    private TextView m_resultTxt;
    /** true on the positions, where the listview is selected */
    private boolean[] m_listSelection;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiselectlistview);
        initComponents();
        initActions();

    }

    /**
     * Init all components of the activity.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_multilistview_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_multilistview_txtView);

        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice,
                android.R.id.text1, getResources().getStringArray(
                        R.array.act_listview_items));
        setListAdapter(listAdapter);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        m_listSelection = new boolean[getResources().getStringArray(
                R.array.act_listview_items).length];
    }

    /**
     * Register listeners on all components of the activity.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        l.setItemChecked(position, !m_listSelection[position]);
        m_listSelection[position] = !m_listSelection[position];
        String selectedItems = "";
        for (int i = 0; i < m_listSelection.length; i++) {
            if (m_listSelection[i]) {
                selectedItems += getListAdapter().getItem(i) + ", ";
            }
        }
        m_resultTxt.setText(selectedItems.substring(0,
                selectedItems.length() - 2));
    }
}
