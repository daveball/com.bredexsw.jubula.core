package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;

/**
 * 
 * Activity works as a FragmentTabHost with different fragments.
 * 
 * @author soeren
 * 
 */
public class TabHostActivity extends FragmentActivity {

    /** tab count */
    private static final int TAB_COUNT = 3;
    /** tab string identifier for texts */
    private static final String TAB_IDENTIFIER = "act_tabhost_fragment";
    /** the tabhost */
    private FragmentTabHost m_tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_tabhost_bottomtabs);
        m_tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        m_tabHost.setup(this, getSupportFragmentManager(),
                R.id.act_tabhost_realtabcontent);

        // has info for each tab - fill bundles with names
        Bundle[] tabBundles = new Bundle[TAB_COUNT];
        for (int i = 0; i < tabBundles.length; i++) {
            tabBundles[i] = new Bundle();
            int tabId = i + 1;
            int resId = getResources().getIdentifier(TAB_IDENTIFIER + tabId,
                    "string", getPackageName());
            tabBundles[i].putString("key", getResources().getString(resId));
        }

        m_tabHost.addTab(
                m_tabHost.newTabSpec("1").setIndicator(
                        getResources()
                                .getString(R.string.act_tabhost_fragment1)),
                TabHostFragment1.class, tabBundles[0]);

        m_tabHost.addTab(
                m_tabHost.newTabSpec("2").setIndicator(
                        getResources()
                                .getString(R.string.act_tabhost_fragment2)),
                TabHostFragment1.class, tabBundles[1]);

        m_tabHost.addTab(
                m_tabHost.newTabSpec("3").setIndicator(
                        getResources()
                                .getString(R.string.act_tabhost_fragment3)),
                TabHostFragment2.class, tabBundles[2]);
    }

}
