package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * 
 * Activity contains all android.widget.Button variations.
 * 
 * @author soeren
 * 
 */
public class ToggleButtonActivity extends Activity {

    /** */
    private static final String TOGGLE_IDENTIFIER = "act_toggle_togglebutton";
    /** */
    private static final int TOGGLE_COUNTER = 3;
    /** */
    private ToggleButton m_toggles[] = new ToggleButton[TOGGLE_COUNTER];
    /** */
    private Button m_btnBack;
    /** */
    private TextView m_resultTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_togglebutton);

        initComponents();
        initActions();
    }

    /**
     * Check which togglebutton has been selected and display the text.
     * 
     * @param view
     *            - the selected view
     */
    public void onToggleButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((ToggleButton) view).isChecked();

        // Check which toggle button was clicked
        switch (view.getId()) {
            case R.id.act_toggle_togglebutton1:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_toggle_toggleBtn1On));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_toggle_toggleBtn1Off));
                }
                break;
            case R.id.act_toggle_togglebutton2:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_toggle_toggleBtn2On));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_toggle_toggleBtn2Off));
                }
                break;
            case R.id.act_toggle_togglebutton3:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_toggle_toggleBtn3On));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_toggle_toggleBtn3Off));
                }
                break;
            default:
                break;
        }
    }

    /**
     * Init actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_toggles[1].setEnabled(false);
        m_toggles[2].setClickable(false);
    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_toggle_txtView);
        m_btnBack = (Button) findViewById(R.id.act_toggle_btnBack);
        for (int i = 0; i < m_toggles.length; i++) {
            // init togglebuttons
            int btnId = i + 1;
            int resId = getResources().getIdentifier(TOGGLE_IDENTIFIER + btnId,
                    "id", getPackageName());
            m_toggles[i] = (ToggleButton) findViewById(resId);
        }
    }

}
