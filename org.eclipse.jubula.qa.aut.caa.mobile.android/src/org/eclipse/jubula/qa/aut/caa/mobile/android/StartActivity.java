package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Main menu activity that is shown first. Has navigation to all other
 * activities.
 * 
 * @author soeren
 * 
 */
public class StartActivity extends Activity {

    /** listener starts new activities */
    private OnClickListener m_onClickListener;
    /** button to the button activity */
    private Button m_btnButtonAct;
    /** button to the edit text activity */
    private Button m_btnEditTextAct;
    /** button to the spinner activity */
    private Button m_btnSpinnerAct;
    /** button to the radio button activity */
    private Button m_btnRadioButtonAct;
    /** button to the toggle button activity */
    private Button m_btnToggleButtonAct;
    /** button to the checkbox activity */
    private Button m_btnCheckBoxAct;
    /** button to the switch activity */
    private Button m_btnSwitchAct;
    /** button to the tab activity */
    private Button m_btnTabhostAct;
    /** button to the listview activity */
    private Button m_btnListViewAct;
    /** button to the multi select listview activity */
    private Button m_btnMultiSelectListViewAct;
    /** button to the ratingbar activity */
    private Button m_btnRatingBar;
    /** button to the timepicker activity */
    private Button m_btnTimePicker;
    /** button to the datepicker activity */
    private Button m_btnDatePicker;
    /** button to the seekbar activity */
    private Button m_btnSeekBar;
    /** button to the vierpager activity */
    private Button m_btnViewPager;
    /** button to the numberpicker activity */
    private Button m_btnNumPicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        initWidgets();
        setTags();
        registerListeners();

    }

    /**
     * Set the launch intent as a tag.
     */
    private void setTags() {
        m_btnButtonAct.setTag(new Intent(this, ButtonActivity.class));
        m_btnEditTextAct.setTag(new Intent(this, EditTextActivity.class));
        m_btnSpinnerAct.setTag(new Intent(this, SpinnerActivity.class));
        m_btnRadioButtonAct.setTag(new Intent(this, RadioButtonActivity.class));
        m_btnToggleButtonAct
                .setTag(new Intent(this, ToggleButtonActivity.class));
        m_btnCheckBoxAct.setTag(new Intent(this, CheckBoxActivity.class));
        m_btnSwitchAct.setTag(new Intent(this, SwitchActivity.class));
        m_btnTabhostAct.setTag(new Intent(this, TabHostActivity.class));
        m_btnListViewAct.setTag(new Intent(this, ListViewActivity.class));
        m_btnMultiSelectListViewAct.setTag(new Intent(this,
                MultiSelectListViewActivity.class));
        m_btnRatingBar.setTag(new Intent(this, RatingBarActivity.class));
        m_btnTimePicker.setTag(new Intent(this, TimePickerActivity.class));
        m_btnDatePicker.setTag(new Intent(this, DatePickerActivity.class));
        m_btnSeekBar.setTag(new Intent(this, SeekBarActivity.class));
        m_btnViewPager.setTag(new Intent(this, ViewPagerActivity.class));
        m_btnNumPicker.setTag(new Intent(this, NumberPickerActivity.class));
    }

    /**
     * Init all buttons of the activity.
     */
    private void initWidgets() {
        m_onClickListener = new OnClickListener() {
            
            public void onClick(View v) {
                startActivity((Intent) v.getTag());
            }
        };
        m_btnButtonAct = (Button) findViewById(R.id.act_start_buton);
        m_btnEditTextAct = (Button) findViewById(R.id.act_start_edittext);
        m_btnSpinnerAct = (Button) findViewById(R.id.act_start_spinner);
        m_btnRadioButtonAct = (Button) findViewById(R.id.act_start_radiobutton);
        m_btnToggleButtonAct = (Button) findViewById(
                R.id.act_start_togglebutton);
        m_btnCheckBoxAct = (Button) findViewById(R.id.act_start_checkbox);
        m_btnSwitchAct = (Button) findViewById(R.id.act_start_switch);
        m_btnTabhostAct = (Button) findViewById(R.id.act_start_tabhost);
        m_btnListViewAct = (Button) findViewById(R.id.act_start_listview);
        m_btnMultiSelectListViewAct = (Button) findViewById(
                R.id.act_start_multilistview);
        m_btnRatingBar = (Button) findViewById(R.id.act_start_ratingbar);
        m_btnTimePicker = (Button) findViewById(R.id.act_start_timepicker);
        m_btnDatePicker = (Button) findViewById(R.id.act_start_datepicker);
        m_btnSeekBar = (Button) findViewById(R.id.act_start_seekbar);
        m_btnViewPager = (Button) findViewById(R.id.act_start_viewpager);
        m_btnNumPicker = (Button) findViewById(R.id.act_start_numpicker);
    }

    /**
     * Register listeners on all buttons of the activity.
     */
    private void registerListeners() {
       
        m_btnButtonAct.setOnClickListener(m_onClickListener);
        m_btnEditTextAct.setOnClickListener(m_onClickListener);
        m_btnSpinnerAct.setOnClickListener(m_onClickListener);
        m_btnRadioButtonAct.setOnClickListener(m_onClickListener);
        m_btnToggleButtonAct.setOnClickListener(m_onClickListener);
        m_btnCheckBoxAct.setOnClickListener(m_onClickListener);
        m_btnSwitchAct.setOnClickListener(m_onClickListener);
        m_btnTabhostAct.setOnClickListener(m_onClickListener);
        m_btnListViewAct.setOnClickListener(m_onClickListener);
        m_btnMultiSelectListViewAct.setOnClickListener(m_onClickListener);
        m_btnRatingBar.setOnClickListener(m_onClickListener);
        m_btnTimePicker.setOnClickListener(m_onClickListener);
        m_btnDatePicker.setOnClickListener(m_onClickListener);
        m_btnSeekBar.setOnClickListener(m_onClickListener);
        m_btnViewPager.setOnClickListener(m_onClickListener);
        m_btnNumPicker.setOnClickListener(m_onClickListener);
    }
}
