package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * 
 * Activity contains all android.widget.Button variations.
 * 
 * @author soeren
 * 
 */
public class CheckBoxActivity extends Activity {

    /** checkbox res id identifier */
    private static final String CHECK_IDENTIFIER = "act_checkbox_checkbox";
    /** check box count */
    private static final int CHECK_COUNTER = 3;
    /** array contains all checkboxes */
    private CheckBox m_checkBoxes[] = new CheckBox[CHECK_COUNTER];
    /** back button */
    private Button m_btnBack;
    /** the result text */
    private TextView m_resultTxt;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkbox);

        initComponents();
        initActions();
    }

    /**
     * Check which checkbox has been selected and display the text.
     * 
     * @param view
     *            - the view
     */
    public void onCheckBoxSelected(View view) {
        // Is the button now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox button was clicked
        switch (view.getId()) {
            case R.id.act_checkbox_checkbox1:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_checkbox_checkbox1checked));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_checkbox_checkbox1unchecked));
                }
                break;
            case R.id.act_checkbox_checkbox2:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_checkbox_checkbox2checked));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_checkbox_checkbox2unchecked));
                }
                break;
            case R.id.act_checkbox_checkbox3:
                if (checked) {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_checkbox_checkbox3checked));
                } else {
                    m_resultTxt.setText(getResources().getString(
                            R.string.act_checkbox_checkbox3unchecked));
                }
                break;
            default:
                break;
        }
    }

    /**
     * Init actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
        m_checkBoxes[1].setEnabled(false);
        m_checkBoxes[2].setClickable(false);
    }

    /**
     * Init all widgets by their res id.
     */
    private void initComponents() {
        m_resultTxt = (TextView) findViewById(R.id.act_checkbox_txtView);
        m_btnBack = (Button) findViewById(R.id.act_checkbox_btnBack);
        for (int i = 0; i < m_checkBoxes.length; i++) {
            // init checkButtons
            int btnId = i + 1;
            int resId = getResources().getIdentifier(CHECK_IDENTIFIER + btnId,
                    "id", getPackageName());
            m_checkBoxes[i] = (CheckBox) findViewById(resId);
        }
    }

}
