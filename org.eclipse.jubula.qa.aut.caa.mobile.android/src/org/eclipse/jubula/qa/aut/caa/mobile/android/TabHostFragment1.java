package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Fragment1 for the TabHostActivity. Shows a text.
 * 
 * @author soeren
 * 
 */
public class TabHostFragment1 extends Fragment {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(
                R.layout.fragment_tabhost_content, null);
        // get text from given bundle arg
        TextView tv = (TextView) v.findViewById(R.id.text);
        if (getArguments() != null) {
            try {
                String value = getArguments().getString("key");
                tv.setText(value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
