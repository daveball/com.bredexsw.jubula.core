package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * Activity contains android.widget.SeekBar.
 * 
 * @author soeren
 * 
 */
public class SeekBarActivity extends Activity {

    /** back button */
    private Button m_btnBack;
    /** the result text */
    private TextView m_resultTxt;
    /** the seekbar 1 */
    private SeekBar m_seekBar1;
    /** the seekbar 2 */
    private SeekBar m_seekBar2;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seekbar);
        
        initComponents();
        initActions();
    }

    /**
     * Init all widgets by their id.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_seekbar_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_seekbar_txtView);
        m_seekBar1 = (SeekBar) findViewById(R.id.act_seekbar_seekbar1);
        m_seekBar2 = (SeekBar) findViewById(R.id.act_seekbar_seekbar2);
    }

    /**
     * Init the actions.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        
        OnSeekBarChangeListener onSeekBarChangeListener = new 
                OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
            
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                if (fromUser) {
                    m_resultTxt.setText(String.valueOf(progress));
                }
            }
        };
        
        m_seekBar1.setOnSeekBarChangeListener(onSeekBarChangeListener);
        m_seekBar2.setOnSeekBarChangeListener(onSeekBarChangeListener);
        m_seekBar2.setEnabled(false);

    }
}
