package org.eclipse.jubula.qa.aut.caa.mobile.android;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Activiy contains a single select android.widget.ListView.
 * @author soeren
 *
 */
public class ListViewActivity extends ListActivity {

    /** the back button */
    private Button m_btnBack;
    /** the selected item text */
    private TextView m_resultTxt;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        initComponents();
        initActions();
    }

    /**
     * Init all components of the activity.
     */
    private void initComponents() {
        m_btnBack = (Button) findViewById(R.id.act_listview_btnBack);
        m_resultTxt = (TextView) findViewById(R.id.act_listview_txtView);

        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1,
                getResources().getStringArray(R.array.act_listview_items));
        setListAdapter(listAdapter);
    }

    /**
     * Register listeners on all components of the activity.
     */
    private void initActions() {
        m_btnBack.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        m_resultTxt.setText(getListAdapter().getItem(position).toString());
    }
}
