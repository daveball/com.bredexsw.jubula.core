package org.eclipse.jubula.qa.aut.caa.base.utils;

import org.eclipse.jubula.qa.aut.caa.base.utils.i18n.I18NUtils;

/**
 * Helper class for components.
 * 
 */
public class ComponentUtils {
    /**
     * ComponentUtils
     */
    private ComponentUtils() {
    // empty
    }

    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static String[] getListItems() {
        return new String[] { I18NUtils.getString("text1"),
                I18NUtils.getString("text2"), I18NUtils.getString("text3"),
                I18NUtils.getString("text4"), I18NUtils.getString("text5"),
                I18NUtils.getString("text6"), I18NUtils.getString("text7") };
    }
    
    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static Integer[] getNumberItems() {
        return new Integer[] { 
            new Integer(100000000), 
            new Integer(200000000), 
            new Integer(300000000),
            new Integer(400000000) };
    }
    
    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static String[] getListLongItems() {
        return new String[] { "Size: 100px", "VeryLongListItem", //$NON-NLS-1$ //$NON-NLS-2$
                              "VeryVeryLongListItem", "VeryVeryVeryLongListItem",  //$NON-NLS-1$//$NON-NLS-2$
                              "VeryVeryVeryVeryLongListItem", //$NON-NLS-1$
                              "VeryVeryVeryVeryVeryLongListItem", //$NON-NLS-1$
                              "VeryVeryVeryVeryVeryVeryLongListItem", //$NON-NLS-1$
                              "VeryVeryVeryVeryVeryVeryVeryLongListItem" }; //$NON-NLS-1$
    }

    /**
     * Returns a array of items for comboBoxes or lists.
     * 
     * @return array of items
     */
    public static String[] getList() {

        String[] index = new String[200];
        for (int i = 0; i < index.length; i++) {
            index[i] = String.valueOf(i + 1);
        }
        return index;
    }
    

}
