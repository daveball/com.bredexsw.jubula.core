/*******************************************************************************
 * Copyright (c) 2004, 2010 BREDEX GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BREDEX GmbH - initial API and implementation and/or initial documentation
 *******************************************************************************/
package com.bredexsw.guidancer.autagent.monitoring.jacoco;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jubula.tools.internal.constants.StringConstants;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICoverageVisitor;
import org.jacoco.core.analysis.ISourceFileCoverage;
import org.jacoco.core.internal.analysis.BundleCoverageImpl;
import org.jacoco.core.internal.analysis.SourceFileCoverageImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Bredex CoverageBuilder which ignores duplicate class file names 
 * even if their are different.  
 *
 * @author BREDEX GmbH
 * @created Feb 15, 2011
 */
public class CoverageBuilder implements ICoverageVisitor {
    /** the logger */
    private static Logger log = LoggerFactory.getLogger(CoverageBuilder.class);
    /** class coverage **/
    private final Map<String, IClassCoverage> m_classes;
    /** sourcefile coverage */
    private final Map<String, ISourceFileCoverage> m_sourcefiles;    
    /** pattern matcher */
    private Pattern m_pattern; 
    
    /**
     * Create a new builder.
     * 
     */
    public CoverageBuilder() {
        this.m_classes = new HashMap<String, IClassCoverage>();
        this.m_sourcefiles = new HashMap<String, ISourceFileCoverage>();
    }
    /**
     * Create a new builder with pattern to filter classes
     * @param pattern A pattern to filter classes
     */
    public CoverageBuilder(String pattern) {
        this.m_classes = new HashMap<String, IClassCoverage>();
        this.m_sourcefiles = new HashMap<String, ISourceFileCoverage>(); 
        this.m_pattern = Pattern.compile(pattern);
    }

    /**
     * Returns all class nodes currently contained in this builder.
     * 
     * @return all class nodes
     */
    public Collection<IClassCoverage> getClasses() {
        return Collections.unmodifiableCollection(m_classes.values());
    }

    /**
     * Returns all source file nodes currently contained in this builder.
     * 
     * @return all source file nodes
     */
    public Collection<ISourceFileCoverage> getSourceFiles() {
        return Collections.unmodifiableCollection(m_sourcefiles.values());
    }

    /**
     * Creates a bundle from all nodes currently contained in this bundle.
     * 
     * @param name
     *            Name of the bundle
     * @return bundle containing all classes and source files
     */
    public BundleCoverageImpl getBundle(final String name) {
        return new BundleCoverageImpl(name, m_classes.values(),
                m_sourcefiles.values());
    }

   
    /**
     * Check classes against package pattern and 
     * put them into the classes to analyze map
     * @param coverage The Class coverage
     */
    public void visitCoverage(final IClassCoverage coverage) {
        
        // Only consider classes that actually contain code: 
        if (coverage.getInstructionCounter().getTotalCount() > 0) {  
            if (!m_pattern.pattern().equals(StringConstants.EMPTY)) {
                Matcher matcher = m_pattern.matcher(
                        coverage.getName().replace('/' , '.'));                
                if (matcher.find()) {                    
                    final IClassCoverage dup = m_classes.put(
                            coverage.getName(), coverage);
                    if (dup != null && dup.getId() != coverage.getId()) {
                        log.warn("CODE-COVERAGE: different class with same name found: " + coverage.getName()); //$NON-NLS-1$                        
                    }
                    final String source = coverage.getSourceFileName();
                    if (source != null) {
                        final SourceFileCoverageImpl sourceFile = 
                            getSourceFile(source, coverage.getPackageName());
                        sourceFile.increment(coverage);
                    }
                }                 
            } else {               
                final IClassCoverage dup = m_classes.put(
                        coverage.getName(), coverage);
                if (dup != null && dup.getId() != coverage.getId()) {
                    log.warn("CODE-COVERAGE: different class with same name found: " + coverage.getName()); //$NON-NLS-1$                        
                }
                final String source = coverage.getSourceFileName();
                if (source != null) {
                    final SourceFileCoverageImpl sourceFile = 
                        getSourceFile(source, coverage.getPackageName());
                    sourceFile.increment(coverage);
                }
            }
           
        }
    }
    /**
     * 
     * @param filename The filename
     * @param packagename The packagename
     * @return An {@link SourceFileCoverageImpl} instance
     */
    private SourceFileCoverageImpl getSourceFile(final String filename,
            final String packagename) {
        final String key = packagename + '/' + filename;
        SourceFileCoverageImpl sourcefile = 
            (SourceFileCoverageImpl) m_sourcefiles
                .get(key);
        if (sourcefile == null) {
            sourcefile = new SourceFileCoverageImpl(filename, packagename);
            m_sourcefiles.put(key, sourcefile);
        }
        return sourcefile;
    }
    
}
