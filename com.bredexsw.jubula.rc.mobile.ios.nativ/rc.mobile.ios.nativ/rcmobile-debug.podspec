Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name         = "rcmobile-debug"
  s.version      = "0.0.1"
  s.summary      = "Professionally developed functional testing tools with no license costs."

  s.description  = "GUIdancer and Jubula are powerful and professional 
  					Eclipse-based tools for automated functional testing 
  					through the Graphical User Interface (GUI)."

  s.homepage     = "http://www.bredex.de/guidancer_jubula_en.html"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license      = "MIT"

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.author       = { "Tamas Kiss" => "tamas.kiss@z-consulting.eu" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.platform     = :ios

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source_files  = "rc.mobile.ios.nativ/**/*.{h,m}"
  
  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.requires_arc = true
  s.frameworks = 'XCTest'
  s.dependency 'CocoaAsyncSocket', '7.3.4'
  s.dependency 'KIF', '3.0.2'

  # ――― Sub spec --------――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  	
  s.subspec 'GoogleToolboxForMax' do |ss|
    ss.source_files = 'Frameworks/**/*.{h,m}'
	ss.requires_arc = false
  end

end
