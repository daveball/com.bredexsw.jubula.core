//
//  KIFCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/3/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KIFCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) tap:(UIAccessibilityElement*) aElement;
+(NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX y:(NSString *)sY width:(NSString *)sWidth height:(NSString *)sHeight;
+(NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX xUnit:(NSString *)xUnit y:(NSString *)sY yUnit:(NSString *)yUnit;
+(NSString*) tapScreenAtPointX:(NSString *)sX Y:(NSString *)sY;
+(NSString*) type:(NSString*) text;
+(NSString*) tapViewWithAccessibilityLabel:(NSString*) label;
+(NSString*) pressDelete;
+(NSString*) swipe:(UIAccessibilityElement*) view inDirection:(NSString*) sDirection;
+ (NSString*) keyInputPostDelay:(NSString*) delay;
@end
