//
//  KIFCapTestCase.h
//  rc.mobile.ios.nativ
//
//  Created by Kiss Tamás on 10/06/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <KIF/KIF.h>

@interface KIFCapTestCase : KIFTestCase

+ (instancetype) sharedKIFCapTesCase;

- (NSString*) tap:(UIAccessibilityElement*) aElement;
- (NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX y:(NSString *)sY width:(NSString *)sWidth height:(NSString *)sHeight;
- (NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX xUnit:(NSString *)xUnit y:(NSString *)sY yUnit:(NSString *)yUnit;
- (NSString*) tapScreenAtPointX:(NSString *)sX Y:(NSString *)sY;
- (NSString*) tapViewWithAccessibilityLabel:(NSString*) label;

- (NSString*) swipe:(UIAccessibilityElement*) view inDirection:(NSString*) sDirection;

- (NSString*) type:(NSString*) text;
- (NSString*) keyInputPostDelay:(NSString*) delay;

@end
