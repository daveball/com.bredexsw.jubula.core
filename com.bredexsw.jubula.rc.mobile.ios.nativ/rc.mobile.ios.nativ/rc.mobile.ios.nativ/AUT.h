//
//  AUT.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 11/6/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AUT : NSObject

+(UIView*) findUIView:(NSArray*) indexPath;

// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) isAlive:(NSString*) timestamp;
+(NSMutableDictionary*) getHierarchy;
+(NSString*) getScreenshot;
+(NSString*) getMainScreenBounds;

@end
