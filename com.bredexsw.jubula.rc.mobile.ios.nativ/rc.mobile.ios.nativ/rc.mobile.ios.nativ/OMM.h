//
//  OMM.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 11/9/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

@interface OMM : NSObject

// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) start:(NSArray*) supportedUITypes;
+(NSArray*) getTappedUIWidgetIndices;
+(NSString*) stop;
+(NSString*) highlight:(NSArray*) indexPath;

@end
