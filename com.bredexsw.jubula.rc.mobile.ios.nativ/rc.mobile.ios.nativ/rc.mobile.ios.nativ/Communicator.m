//
//  Communicator.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 7/16/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import "Communicator.h"
#import "AUT.h"
#import "AsyncSocket.h"
#import "ReflectionUtil.h"

#define GENERIC_MSG 0 

@interface Communicator()

@property (nonatomic, strong) AsyncSocket *communicator;
@property (nonatomic, strong) NSMutableArray *connectedClients;

@end

@implementation Communicator

@synthesize running;

- (id) init; {
    self = [super init];
    if (self != nil)
    {
        _communicator = [[AsyncSocket alloc] initWithDelegate:self];
        _connectedClients = [[NSMutableArray alloc] initWithCapacity:1];
        running = false;
    }
    return self;
}

- (void) dealloc;
{
    [self stop];
}

- (void) startOnPort:(int)port;
{
    if (running) return;
    
    if (port < 0 || port > 65535)
        port = 0;
    
    NSError *error = nil;
    if (![self.communicator acceptOnPort:port error:&error])
        return;
        
    running = true;
}

- (void) stop;
{
    if (!running) return;
    
    [self.communicator disconnect];
    for (AsyncSocket* socket in self.connectedClients) {
        [socket disconnect]; 
    }
    running = false;
}

- (void)onSocket:(AsyncSocket *)socket didAcceptNewSocket:(AsyncSocket *)newSocket;
{
    [self.connectedClients addObject:newSocket];
}

- (void)onSocketDidDisconnect:(AsyncSocket *)socket;
{
    [self.connectedClients removeObject:socket];
}

- (void)onSocket:(AsyncSocket *)socket didConnectToHost:(NSString *)host port:(UInt16)port;
{
    [socket readDataWithTimeout:-1 tag:GENERIC_MSG];
}

- (void)onSocket:(AsyncSocket *)socket didReadData:(NSData *)data withTag:(long)tag;
{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

    NSString* className  = json[@"className"];
    NSString* methodName = json[@"methodName"];
    
    if (className && methodName) {
        NSMutableDictionary* dict = [NSMutableDictionary dictionary];
        
        NSMutableArray* parameter = [NSMutableArray arrayWithArray:json[@"parameters"]];
        @try {
            // UI component lookup
            NSString* locator = json[@"locator"];
            if ([locator length] > 0) {
                NSMutableArray* indexPath = [NSMutableArray arrayWithArray:[locator componentsSeparatedByString:@"/"]];
                [indexPath removeObjectAtIndex:0];
                UIView* view = [AUT findUIView:indexPath];
                if (view) {
                    if (sizeof(parameter) == 0) {
                        [parameter addObject:view];
                    } else {
                        [parameter insertObject:view atIndex:0];
                    }
                } else {
                    [NSException raise:@"Component with locator %@ not found" format:@"Locator %@ is invalid", locator];
                }
            }
            
            // method invocation
            id returnValue = [ReflectionUtil invoke:methodName with:parameter on:className];
            dict[@"exceptionFlag"]      = @"FALSE";
            dict[@"methodReturnString"] = returnValue;
        }
        @catch (NSException *exception) {
            dict[@"exceptionFlag"]   = @"TRUE";
            dict[@"exceptionID"]     = @"0";
            dict[@"exceptionName"]   = @"na";
            dict[@"exceptionMsg"]    = exception.reason;
            dict[@"exceptionMethod"] = exception.debugDescription;
            dict[@"exceptionClass"]  = [exception.class description];
        }
        
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0UL error:&error];
        NSMutableData* answerData = [NSMutableData dataWithData:jsonData];
        // send line break for readLine(); on Java RC iOS side
        [answerData appendData:[AsyncSocket CRLFData]];
        [socket writeData:answerData withTimeout:-1 tag:GENERIC_MSG];
    }

    [socket readDataWithTimeout:-1 tag:GENERIC_MSG];
}

-(void) writeMessage:(NSString*) message toSocket:(AsyncSocket *)socket;
{
    NSData *messageData = [message dataUsingEncoding:NSUTF8StringEncoding];
    [socket writeData:messageData withTimeout:-1 tag:GENERIC_MSG];
}

@end
