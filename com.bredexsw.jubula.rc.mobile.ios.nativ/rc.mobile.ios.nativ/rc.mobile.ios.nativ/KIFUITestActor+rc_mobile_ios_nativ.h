//
//  KIFUITestActor+rc_mobile_ios_nativ.h
//  rc.mobile.ios.nativ
//
//  Created by  on 1/15/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "KIFUITestActor.h"
#import <UIKit/UIKit.h>

@interface KIFUITestActor (rc_mobile_ios_nativ)

- (void)rcTapAccessibilityElement:(UIAccessibilityElement *)element;
- (void)tapView:(UIAccessibilityElement *)element withConstraints:(CGRect) innerConstraints;
- (void)swipeView:(UIAccessibilityElement *)element inDirection:(KIFSwipeDirection) direction;
- (void)tapSwitch:(UIAccessibilityElement *)element;
- (void)tapSwitch:(UIAccessibilityElement *)element withConstraints:(CGRect) innerConstraints;

@end