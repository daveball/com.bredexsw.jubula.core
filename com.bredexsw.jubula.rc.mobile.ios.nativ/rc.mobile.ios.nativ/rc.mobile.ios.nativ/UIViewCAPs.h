//
//  UIViewCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) isShowing:(UIView*) view;
+(NSString*) hasFocus:(UIView*) view;
+(NSString*) getText:(UIView*) view;

@end
