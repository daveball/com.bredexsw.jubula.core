//
//  UITextInputCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/8/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractUITextInputCAPs : NSObject

// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) selectAll:(UIView <UITextInput>*) textInputView;

@end
