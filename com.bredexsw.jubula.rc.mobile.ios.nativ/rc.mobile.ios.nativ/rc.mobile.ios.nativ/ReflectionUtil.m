//
//  ReflectionUtil.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 7/16/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import "ReflectionUtil.h"
#import <objc/runtime.h>
#import <objc/message.h>

// The first two arguments are the hidden arguments self and _cmd
#define NO_OF_DEFAULT_ARGS 2 

@implementation ReflectionUtil

+(id) invoke:(NSString *) methodName with:(NSArray*) arguments on:(NSString*) className;
{
    Class class = NSClassFromString(className);
    SEL selector = NSSelectorFromString(methodName);
    
    Method method = class_getClassMethod(class, selector);
    
    NSMethodSignature *signature = [class methodSignatureForSelector:selector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    [invocation setTarget:class];
    [invocation setSelector:selector];
    
    int amountOfRequiredArguments = method_getNumberOfArguments(method);
    int amountOfGivenArguments = (int)[arguments count] + NO_OF_DEFAULT_ARGS;
    if (amountOfRequiredArguments > 0) {
        if(amountOfRequiredArguments > amountOfGivenArguments)
        {
            return nil; // Not enough arguments in the array
        }
        
        for(int i=0; i<[arguments count]; i++)
        {
            id arg = [arguments objectAtIndex:i];
            [invocation setArgument:&arg atIndex:i + NO_OF_DEFAULT_ARGS];
        }
    }
    [invocation invoke]; // Invoke the selector
    //Do not release the object after it is out of scope.
    __unsafe_unretained id anObject;
    [invocation getReturnValue:&anObject];

    return anObject;
}

@end
