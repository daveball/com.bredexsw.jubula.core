//
//  UIView+Automation.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/24/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Automation)

- (BOOL) isVisibleOnScreen;
- (id) findFirstResponder;
- (NSMutableArray*) indexPathOfView;

@end
