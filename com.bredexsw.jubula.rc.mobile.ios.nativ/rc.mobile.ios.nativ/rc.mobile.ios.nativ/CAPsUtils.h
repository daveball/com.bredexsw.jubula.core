//
//  CAPsUtils.h
//  rc.mobile.ios.nativ
//
//  Created by markus on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAPsUtils : NSObject
+(NSString*) getRenderedText:(UIView*) view;
+(NSString*) getFallback:(NSObject*) obj forText:(NSString*)text;
+(NSString*) boolAsString:(BOOL) value;
+(NSString*) intAsString:(NSInteger) value;
+(NSString*) boundsAsStringX:(NSInteger) x y:(NSInteger) y width:(NSInteger) width height:(NSInteger) height;
+(void) waitForUItoSettle;

@end
