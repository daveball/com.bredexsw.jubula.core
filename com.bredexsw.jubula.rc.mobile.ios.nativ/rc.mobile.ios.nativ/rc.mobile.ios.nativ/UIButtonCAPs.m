//
//  UIButtonCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by markus on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UIButtonCAPs.h"
#import <UIKit/UIKit.h>
#import "CAPsUtils.h"

@implementation UIButtonCAPs

+(NSString*) getText:(UIButton*) button;
{
    return [CAPsUtils getFallback:button forText:[button currentTitle]];
}

+(NSString*) isSelected:(UIButton*) button;
{
    return [CAPsUtils boolAsString:[button isSelected]];
}

@end
