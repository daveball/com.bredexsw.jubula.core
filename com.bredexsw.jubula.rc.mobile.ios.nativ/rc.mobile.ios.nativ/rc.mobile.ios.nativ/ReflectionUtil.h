//
//  ReflectionUtil.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 7/16/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

@interface ReflectionUtil : NSObject

+(NSData*) invoke:(NSString *) methodName with:(NSArray*) arguments on:(NSString*) className;

@end
