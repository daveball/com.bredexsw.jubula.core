//
//  UITabBarCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/28/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) getTabCount:(UITabBar*) sControl;
+(NSString*) getTitleOfTab:(UITabBar*) sControl atIndex:(NSString*) sIdx;
+(NSString*) isTabEnabled:(UITabBar*) sControl atIndex:(NSString*) sIdx;
+(NSString*) getSelectedIndex:(UITabBar*) sControl;
+(NSString*) getBoundsOfTab:(UITabBar*) sControl atIndex:(NSString*) sIdx;
+(NSString*) badgeValue:(UIView*) view atIndex:(NSNumber*) tabIndex;
@end
