//
//  UIPickerViewCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/14/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPickerViewCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) getText:(UIPickerView*) view;
+(NSString*) getNoOfColumns:(UIPickerView*) view;
+(NSString*) getSelectedIndex:(UIPickerView*) view inColumn:(NSString*) sColIdx;
+(NSString*) select:(UIPickerView*) view index:(NSString*) sRowIdx inColumn:(NSString*) sColIdx;
+(NSArray*) getValues:(UIPickerView*) view inColumn:(NSString*) sColIdx;

@end
