//
//  UISwitchCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/7/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UISwitchCAPs.h"
#import "CAPsUtils.h"

@implementation UISwitchCAPs

+(NSString*) isSelected:(UISwitch*) checkbox;
{
    return [CAPsUtils boolAsString:[checkbox isOn]];
}

@end
