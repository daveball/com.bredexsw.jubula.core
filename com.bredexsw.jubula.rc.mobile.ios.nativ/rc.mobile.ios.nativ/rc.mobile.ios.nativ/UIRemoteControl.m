//
//  rc_mobile_ios_nativ.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 6/29/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import "UIRemoteControl.h"
#import "Communicator.h"

#define DEFAULT_PORT 11022 

@implementation UIRemoteControl

+(void)attach {
    [self attach:DEFAULT_PORT];
}

+(void) attach:(int)port {
    [[Communicator new] startOnPort:port];
}

@end
