//
//  UISegmentedControlCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/7/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UISegmentedControlCAPs.h"
#import "CAPsUtils.h"

@interface UISegmentedControlCAPs()

+(NSInteger) getWidthOfTab: (UISegmentedControl*) sControl atIndex:(NSInteger) idx;

@end

@implementation UISegmentedControlCAPs

+(NSString*) getTabCount:(UISegmentedControl*) sControl;
{
    return [CAPsUtils intAsString:[sControl numberOfSegments]];
}

+(NSString*) getTitleOfTab:(UISegmentedControl*) sControl atIndex:(NSString*) sIdx;
{
    NSInteger idx = [sIdx integerValue];
    NSString* titleOfTab = [sControl titleForSegmentAtIndex:idx];
    if (!titleOfTab) {
        titleOfTab = [[sControl imageForSegmentAtIndex:idx] accessibilityLabel];
    }

    return [CAPsUtils getFallback:sControl forText:titleOfTab];
}

+(NSString*) isTabEnabled:(UISegmentedControl*) sControl atIndex:(NSString*) sIdx;
{
    return [CAPsUtils boolAsString:[sControl isEnabledForSegmentAtIndex:[sIdx integerValue]]];
}

+(NSString*) getSelectedIndex:(UISegmentedControl*) sControl;
{
    return [CAPsUtils intAsString:[sControl selectedSegmentIndex]];
}


+(NSString*) getBoundsOfTab:(UISegmentedControl*) sControl atIndex:(NSString*) sIdx;
{  
    NSInteger idx = [sIdx integerValue];
    
    NSInteger tabX = 0;
    NSInteger tabY = 0;
    NSInteger tabWidth = [self getWidthOfTab:sControl atIndex:idx];
    NSInteger tabHeight = [sControl bounds].size.height;
    
    for (NSUInteger i = 0; i < idx; i++) {
        tabX += [self getWidthOfTab:sControl atIndex:idx];
    }

    return [CAPsUtils boundsAsStringX:tabX y:tabY width:tabWidth height:tabHeight];
}

+(NSInteger) getWidthOfTab: (UISegmentedControl*) sControl atIndex:(NSInteger) idx;
{
    NSInteger widthOfTab = [sControl widthForSegmentAtIndex:idx];
    if (widthOfTab == 0.0f) {
        widthOfTab = [sControl bounds].size.width / [sControl numberOfSegments];
    }
    return widthOfTab;
}

@end
