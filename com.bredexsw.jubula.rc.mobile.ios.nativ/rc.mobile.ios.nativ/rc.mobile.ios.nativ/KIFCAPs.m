//
//  KIFCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/3/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "KIFCAPs.h"
#import "KIFCapTestCase.h"

@implementation KIFCAPs

+ (NSString*) tap:(UIAccessibilityElement*) aElement;
{
    [[KIFCapTestCase sharedKIFCapTesCase] tap:aElement];
    return @"OK";
}

+ (NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX y:(NSString *)sY width:(NSString *)sWidth height:(NSString *)sHeight;
{
   [[KIFCapTestCase sharedKIFCapTesCase] tap:aElement x:sX y:sY width:sWidth height:sHeight];
    return @"OK";
}

+ (NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX xUnit:(NSString *)xUnit y:(NSString *)sY yUnit:(NSString *)yUnit
{
    [[KIFCapTestCase sharedKIFCapTesCase] tap:aElement x:sX xUnit:xUnit y:sY yUnit:yUnit];
    return @"OK";
}

+ (NSString*) tapScreenAtPointX:(NSString *)sX Y:(NSString *)sY
{
   [[KIFCapTestCase sharedKIFCapTesCase] tapScreenAtPointX:sX Y:sY];
    return @"OK";
}

+ (NSString*) type:(NSString*) text;
{
   [[KIFCapTestCase sharedKIFCapTesCase] type:text];
    return @"OK";
}


+ (NSString*) tapViewWithAccessibilityLabel:(NSString*) label
{
   [[KIFCapTestCase sharedKIFCapTesCase] tapViewWithAccessibilityLabel:label];
    return @"OK";
}

+ (NSString*) pressDelete;
{
    return [self type:@"\b"];
}

+(NSString*) swipe:(UIAccessibilityElement*) view inDirection:(NSString*) sDirection
{
    [[KIFCapTestCase sharedKIFCapTesCase] swipe:view inDirection:sDirection];
    return @"OK";
}

+ (NSString*) keyInputPostDelay:(NSString*) delay
{
    [[KIFCapTestCase sharedKIFCapTesCase] keyInputPostDelay:delay];
    return @"OK";
}

@end
