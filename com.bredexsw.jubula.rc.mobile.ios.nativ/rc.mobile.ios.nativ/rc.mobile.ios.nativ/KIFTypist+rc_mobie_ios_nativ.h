//
//  KIFTypist+rc_mobie_ios_nativ.h
//  rc.mobile.ios.nativ
//
//  Created by  on 3/31/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "KIFTypist.h"

@interface KIFTypist (rc_mobie_ios_nativ)
+ (BOOL)rc_enterCharacter:(NSString *)characterString;
@end
