//
//  UILabelCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/3/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UILabelCAPs.h"
#import "CAPsUtils.h"

@implementation UILabelCAPs

+(NSString*) getText:(UILabel*) label;
{
    return [CAPsUtils getFallback:label forText:[label text]];
}


+(NSString*) isEnabled:(UILabel*) label;
{
    return [CAPsUtils boolAsString:[label isEnabled]];

}

@end
