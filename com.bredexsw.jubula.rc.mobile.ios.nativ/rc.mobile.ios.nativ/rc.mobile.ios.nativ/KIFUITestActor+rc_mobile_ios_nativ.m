//
//  KIFUITestActor+rc_mobile_ios_nativ.m
//  rc.mobile.ios.nativ
//
//  Created by  on 1/15/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "KIFUITestActor+rc_mobile_ios_nativ.h"
#import "UIApplication-KIFAdditions.h"
#import "UIAccessibilityElement-KIFAdditions.h"
#import "UIWindow-KIFAdditions.h"
#import "CGGeometry-KIFAdditions.h"
#import "NSError-KIFAdditions.h"
#import "KIFTypist+rc_mobie_ios_nativ.h"

@implementation KIFUITestActor (rc_mobile_ios_nativ)

- (void)rcTapAccessibilityElement:(UIAccessibilityElement *)element
{
    [self tapView:element withConstraints:CGRectNull];
}

/**
 * We need this because the change of the error message,
 * when view is not enabled for user interaction
 */
- (void)tapView:(UIAccessibilityElement *)element withConstraints:(CGRect) innerConstraints
{
    UIView *view = [UIAccessibilityElement viewContainingAccessibilityElement:element];
    [self runBlock:^KIFTestStepResult(NSError **error) {
        
        //KIFTestWaitCondition(view.isUserInteractionActuallyEnabled, error, @"View is not enabled for interaction");
        if (!view.isUserInteractionActuallyEnabled) {
            return KIFTestStepResultSuccess;
        }
        
        // If the accessibilityFrame is not set, fallback to the view frame.
        CGRect elementFrame;
        if (CGRectEqualToRect(CGRectZero, element.accessibilityFrame)) {
            elementFrame.origin = CGPointZero;
            elementFrame.size = view.frame.size;
        } else {
            elementFrame = [view.window convertRect:element.accessibilityFrame toView:view];
        }
        
        if (!CGRectIsNull(innerConstraints)) {
            elementFrame.origin.x += innerConstraints.origin.x;
            elementFrame.origin.y += innerConstraints.origin.y;
            elementFrame.size.width = innerConstraints.size.width;
            elementFrame.size.height = innerConstraints.size.height;
        }
        
        CGPoint tappablePointInElement = [view tappablePointInRect:elementFrame];
        
        // This is mostly redundant of the test in _accessibilityElementWithLabel:
        KIFTestWaitCondition(!isnan(tappablePointInElement.x), error, @"View is not tappable");
        [view tapAtPoint:tappablePointInElement];
        
        KIFTestCondition(![view canBecomeFirstResponder] || [view isDescendantOfFirstResponder], error, @"Failed to make the view into the first responder");
        
        return KIFTestStepResultSuccess;
    }];
    
    // Wait for the view to stabilize.
    [self waitForTimeInterval:0.5];
}

- (void)swipeView:(UIAccessibilityElement *)element inDirection:(KIFSwipeDirection) direction
{
    const NSUInteger kNumberOfPointsInSwipePath = 20;
    // The original version of this came from http://groups.google.com/group/kif-framework/browse_thread/thread/df3f47eff9f5ac8c
    
    UIView *viewToSwipe = [UIAccessibilityElement viewContainingAccessibilityElement:element];
   
    // Within this method, all geometry is done in the coordinate system of the view to swipe.
    CGRect elementFrame = [viewToSwipe.window convertRect:element.accessibilityFrame toView:viewToSwipe];
    CGPoint swipeStart = CGPointCenteredInRect(elementFrame);
    KIFDisplacement swipeDisplacement = KIFDisplacementForSwipingInDirection(direction);
    
    [viewToSwipe dragFromPoint:swipeStart displacement:swipeDisplacement steps:kNumberOfPointsInSwipePath];
}

- (void)tapSwitch:(UIAccessibilityElement *)element
{
    NSLog(@"Constrains is null");
    [self tapSwitch:element withConstraints:CGRectNull];
}

- (void)tapSwitch:(UIAccessibilityElement *)element withConstraints:(CGRect) innerConstraints
{
    NSLog(@"withConstraints");
    UIView *view = [UIAccessibilityElement viewContainingAccessibilityElement:element];
    
    if (![view isKindOfClass:[UISwitch class]]) {
        [self failWithError:[NSError KIFErrorWithFormat:@"View with accessibility label \"%@\" is a %@, not a UISwitch", view.accessibilityLabel, NSStringFromClass([view class])] stopTest:YES];
    }
    
    UISwitch *switchView = (UISwitch *)view;
    Boolean switchIsOn = !switchView.isOn; // we want to switch it so save the initial state
    
    [self tapView:element withConstraints:innerConstraints];
    
    // If we succeeded, stop the test.
    if (switchView.isOn == switchIsOn) {
        return;
    }
    
    NSLog(@"Faking turning switch %@", switchIsOn ? @"ON" : @"OFF");
    [switchView setOn:switchIsOn animated:YES];
    [switchView sendActionsForControlEvents:UIControlEventValueChanged];
    [self waitForTimeInterval:0.5];
    
    // We gave it our best shot.  Fail the test.
    if (switchView.isOn != switchIsOn) {
        [self failWithError:[NSError KIFErrorWithFormat:@"Failed to toggle switch to \"%@\"; instead, it was \"%@\"", switchIsOn ? @"ON" : @"OFF", switchView.on ? @"ON" : @"OFF"] stopTest:YES];
    }
}

@end
