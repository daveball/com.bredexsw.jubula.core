//
//  UIView+Automation.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/24/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UIView+Automation.h"

@implementation UIView (Automation)

- (BOOL) isVisibleOnScreen
{
    CGRect absoluteBounds = [self convertRect:self.bounds toView:self.window];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    return ![self isHidden] && CGRectIntersectsRect(screenBounds, absoluteBounds);
}

- (id) findFirstResponder
{

    if (self.isFirstResponder) { return self; }
    for (UIView *subView in self.subviews) {
        id responder = [subView findFirstResponder];
        if (responder) return responder;
    }
    return nil;
}

- (NSMutableArray*) indexPathOfView
{
    UIView* parent = [self superview];
    UIView* child = self;
    NSMutableArray* indexPath = [NSMutableArray array];
    // gather local indexpath
    while (parent != nil) {
        [indexPath addObject:@([parent.subviews indexOfObject:child])];
        child = parent;
        parent = [parent superview];
    }
    
    NSInteger indexOfObject = [[[UIApplication sharedApplication] windows] indexOfObject:child];
    [indexPath addObject:@(indexOfObject)];
    return indexPath;
}


@end
