//
//  AUT.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 11/6/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import "AUT.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GTMStringEncoding.h"
#import "UIView+Automation.h"
#import "CAPsUtils.h"

@interface AUT()

@end

@implementation AUT

+(NSString*) isAlive:(NSString*) timestamp;
{
    return timestamp;
}

+(NSString*) getMainScreenBounds {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    return [CAPsUtils boundsAsStringX:screenBounds.origin.x y:screenBounds.origin.y width:screenBounds.size.width height:screenBounds.size.height];
}

+(NSMutableArray*) getHierarchy;
{
    NSMutableArray* topLevelWindows = [NSMutableArray array];
    for (UIWindow *window in [[UIApplication sharedApplication] windows]) {
        [topLevelWindows addObject:[self traverseViews:window]];
    }
    return topLevelWindows;
}

+(NSMutableDictionary*)traverseViews:(UIView *)view
{
    NSMutableArray* children = [NSMutableArray array];
    for (UIView *subView in [[view subviews] objectEnumerator]) {
        if ([view isVisibleOnScreen]) {
            [children addObject:[self traverseViews:subView]];
        }
    }
    return [self getDescriptiveDictionary:view withChildren:children];
}

+ (NSMutableDictionary*) getDescriptiveDictionary:(UIView *) view withChildren:(NSMutableArray*) children
{
    NSMutableDictionary* serializedView = [NSMutableDictionary dictionary];
    serializedView[@"className"] = NSStringFromClass([view class]);
    NSMutableArray* superClasses = [NSMutableArray array];
    Class superClass = [view superclass];
    while (superClass != nil) {
        [superClasses addObject:NSStringFromClass(superClass)];
        superClass = [superClass superclass];
    }
    serializedView[@"superClasses"] = superClasses;
    NSString* identifier = [view accessibilityIdentifier];
    if ([identifier length] != 0) {
        serializedView[@"accessibilityIdentifier"] = identifier;
    }
    if ([children count] > 0) {
        serializedView[@"children"] = children;
    }
    return serializedView;
}

+(UIView*) findUIView:(NSArray*) indexPath {
    NSInteger windowIdx = [indexPath[0] integerValue];
    NSArray* windows = [[UIApplication sharedApplication] windows];
    if ((windowIdx+1) > [windows count]) {
        return nil;
    }
    
    UIView* cView = windows[windowIdx];
    for (NSUInteger i = 1; i < [indexPath count]; i++) {
        NSInteger viewIdx = [[indexPath objectAtIndex:i] integerValue];
        if ((viewIdx+1) > [cView.subviews count]) {
            return nil;
        }
        
        cView = cView.subviews[viewIdx];
    }
    return cView;
}


+(NSString*) getScreenshot;
{
    BOOL isIOS7 = [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0;
    UIImage* image = isIOS7 ? [AUT screenshotIOS7] : [AUT screenshot];
    NSData *imageData = UIImagePNGRepresentation(image);
    GTMStringEncoding *coder = [GTMStringEncoding rfc4648Base64StringEncoding];
    
    return [coder encode:imageData];
}

// taken from http://developer.apple.com/library/ios/#qa/qa1703/_index.html

+ (UIImage*)screenshot
{
    // Create a graphics context with the target size
    // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
    // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
    CGSize imageSize = [[UIScreen mainScreen] bounds].size;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    else
        UIGraphicsBeginImageContext(imageSize);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Iterate over every window from back to front
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen])
        {
            // -renderInContext: renders in the coordinate space of the layer,
            // so we must first apply the layer's geometry to the graphics context
            CGContextSaveGState(context);
            // Center the context around the window's anchor point
            CGContextTranslateCTM(context, [window center].x, [window center].y);
            // Apply the window's transform about the anchor point
            CGContextConcatCTM(context, [window transform]);
            // Offset by the portion of the bounds left of and above the anchor point
            CGContextTranslateCTM(context,
                                  -[window bounds].size.width * [[window layer] anchorPoint].x,
                                  -[window bounds].size.height * [[window layer] anchorPoint].y);
            
            // Render the layer hierarchy to the current context
            [[window layer] renderInContext:context];
            
            // Restore the context
            CGContextRestoreGState(context);
        }
    }
    
    // Retrieve the screenshot image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage*) screenshotIOS7
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    UIGraphicsBeginImageContextWithOptions(screenBound.size, NO, [UIScreen mainScreen].scale);
    
    UIWindow* window = [UIApplication sharedApplication].windows[0];
    [window drawViewHierarchyInRect:screenBound afterScreenUpdates:YES];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
