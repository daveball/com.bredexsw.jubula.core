//
//  UISegmentedControlCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/7/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControlCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) getTabCount:(UISegmentedControl*) sControl;
+(NSString*) getTitleOfTab:(UISegmentedControl*) sControl atIndex:(NSString*) sIdx;
+(NSString*) isTabEnabled:(UISegmentedControl*) sControl atIndex:(NSString*) sIdx;
+(NSString*) getSelectedIndex:(UISegmentedControl*) sControl;
+(NSString*) getBoundsOfTab:(UISegmentedControl*) sControl atIndex:(NSString*) sIdx;
@end
