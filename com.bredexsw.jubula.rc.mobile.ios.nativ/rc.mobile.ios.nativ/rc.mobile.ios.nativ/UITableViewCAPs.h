//
//  UITableViewCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/4/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSArray*) getValues:(UITableView*) table;
+(NSArray*) getSelectedValues:(UITableView*) table;
+(NSArray*) getSelectedIndices:(UITableView*) table;
+(NSString*) tap:(UITableView*) table count:(NSString*) sNoOfTaps atIndex:(NSString*) sIdx;
@end
