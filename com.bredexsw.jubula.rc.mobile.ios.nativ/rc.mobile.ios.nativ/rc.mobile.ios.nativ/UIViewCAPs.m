//
//  UIViewCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UIViewCAPs.h"
#import "CAPsUtils.h"

@implementation UIViewCAPs

+(NSString*) isShowing:(UIView*) view;
{
    return [CAPsUtils boolAsString:![view isHidden]];
}

+(NSString*) hasFocus:(UIView*) view;
{
    return [CAPsUtils boolAsString:[view isFirstResponder]];
}


+(NSString*) getText:(UIView*) view;
{
    return [CAPsUtils getFallback:view forText:nil];
}

@end
