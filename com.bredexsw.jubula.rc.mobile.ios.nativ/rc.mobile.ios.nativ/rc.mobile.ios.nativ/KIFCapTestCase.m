//
//  KIFCapTestCase.m
//  rc.mobile.ios.nativ
//
//  Created by Kiss Tamás on 10/06/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "KIFCapTestCase.h"
#import "UIAccessibilityElement-KIFAdditions.h"
#import "KIFUITestActor+rc_mobile_ios_nativ.h"
#import "KIFTypist.h"

@implementation KIFCapTestCase

+ (instancetype) sharedKIFCapTesCase
{
    static dispatch_once_t onceToken;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [self new];
    });
    return _sharedObject;
}

- (NSString*) tap:(UIAccessibilityElement*) aElement
{
    UIView *view = [UIAccessibilityElement viewContainingAccessibilityElement:aElement];
    if ([view isKindOfClass:[UISwitch class]]) {
        [tester tapSwitch:aElement];
    } else {
        [tester rcTapAccessibilityElement:aElement];
    }
    return @"OK";
}

- (NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX y:(NSString *)sY width:(NSString *)sWidth height:(NSString *)sHeight
{
    NSInteger x = [sX integerValue];
    NSInteger y = [sY integerValue];
    NSInteger width = [sWidth integerValue];
    NSInteger height = [sHeight integerValue];
    UIView *view = [UIAccessibilityElement viewContainingAccessibilityElement:aElement];
    if ([view isKindOfClass:[UISwitch class]]) {
        [tester tapSwitch:aElement withConstraints:CGRectMake(x, y, width, height)];
    } else {
        [tester tapView:aElement withConstraints:CGRectMake(x, y, width, height)];
    }
    return @"OK";
}

- (NSString*) tap:(UIAccessibilityElement*) aElement x:(NSString *)sX xUnit:(NSString *)xUnit y:(NSString *)sY yUnit:(NSString *)yUnit
{
    double x = [sX doubleValue];
    double y = [sY doubleValue];
    UIView *view = [UIAccessibilityElement viewContainingAccessibilityElement:aElement];
    CGRect frame = view.frame;
    if ([xUnit isEqualToString:@"PERCENT"]) {
        x = frame.size.width * x/100;
    }
    if ([yUnit isEqualToString:@"PERCENT"]) {
        y = frame.size.height * y/100;
    }
    if (x > frame.size.width || y > frame.size.height) {
        NSString* exceptionMsg = [NSString stringWithFormat:@"Position is bigger than component (x: %@, y: %@, component width: %@, component height: %@)",@(x),@(y),@(frame.size.width),@(frame.size.height)];
        NSException *exception = [NSException exceptionWithName: @"Position error!"
                                                         reason: exceptionMsg
                                                       userInfo: nil];
        @throw exception;
        return @"OK";
    }
    if ([view isKindOfClass:[UISwitch class]]) {
        [tester tapSwitch:aElement withConstraints:CGRectMake(x, y, x+2, y+2)];
    } else {
        [tester tapView:aElement withConstraints:CGRectMake(x, y, 1, 1)];
    }
    return @"OK";
}

- (NSString*) tapScreenAtPointX:(NSString *)sX Y:(NSString *)sY
{
    NSInteger x = [sX integerValue];
    NSInteger y = [sY integerValue];
    [tester tapScreenAtPoint:CGPointMake(x, y)];
    return @"OK";
}

- (NSString*) tapViewWithAccessibilityLabel:(NSString*) label
{
    [tester tapViewWithAccessibilityLabel:label];
    return @"OK";
}

- (NSString*) swipe:(UIAccessibilityElement*) view inDirection:(NSString*) sDirection
{
    KIFSwipeDirection direction = KIFSwipeDirectionRight;
    if ([@"up" isEqualToString:sDirection]) {
        direction = KIFSwipeDirectionUp;
    } else if ([@"down" isEqualToString:sDirection]) {
        direction = KIFSwipeDirectionDown;
    } else if ([@"left" isEqualToString:sDirection]) {
        direction = KIFSwipeDirectionLeft;
    } else if ([@"right" isEqualToString:sDirection]) {
        direction = KIFSwipeDirectionRight;
    }
    [tester swipeView:view inDirection:direction];
    return @"OK";
}

- (NSString*) type:(NSString*) text;
{
    [tester enterTextIntoCurrentFirstResponder:text];
    return @"OK";
}

- (NSString*) keyInputPostDelay:(NSString*) delay
{
    double keyStrokeDelay = [delay doubleValue];
    keyStrokeDelay = keyStrokeDelay / 1000;
    [KIFTypist setKeystrokeDelay:keyStrokeDelay];
    return @"OK";
}

@end
