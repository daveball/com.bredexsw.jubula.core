//
//  UITextFieldCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/3/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UITextFieldCAPs.h"
#import "CAPsUtils.h"

@implementation UITextFieldCAPs

+(NSString*) getText:(UITextField*) textfield;
{
    return [CAPsUtils getFallback:textfield forText:[textfield text]];
}

@end
