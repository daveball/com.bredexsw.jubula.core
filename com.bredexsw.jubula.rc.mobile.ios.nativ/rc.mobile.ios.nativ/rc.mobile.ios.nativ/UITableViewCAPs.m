//
//  UITableViewCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/4/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UITableViewCAPs.h"
#import "UITableViewCellCAPs.h"
#import "KIFCAPs.h"
#import "CAPsUtils.h"

@implementation UITableViewCAPs

+(NSArray*) getValues:(UITableView*) table;
{   
    NSMutableArray* values = [NSMutableArray array];
    
    for (NSUInteger i = 0; i < [table numberOfSections]; i++) {
        for (NSUInteger j = 0; j < [table numberOfRowsInSection:i]; j++) {
            NSIndexPath* cIdx = [NSIndexPath indexPathForRow:j inSection:i];
            [table scrollToRowAtIndexPath:cIdx atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            UITableViewCell* cell = [table cellForRowAtIndexPath:cIdx];            
            [values addObject:[UITableViewCellCAPs getText:cell]];
        }
    }
    
    return values;
}


+(NSString*) tap:(UITableView*) table count:(NSString*) sNoOfTaps atIndex:(NSString*) sIdx;
{
    NSInteger noOfTaps = [sNoOfTaps integerValue];
    NSInteger idx = [sIdx integerValue];
    bool found = false;
    
    NSInteger count = 0;
    for (NSUInteger i = 0; (i < [table numberOfSections]) && !found; i++) {
        for (NSUInteger j = 0; (j < [table numberOfRowsInSection:i]) && !found; j++) {
            if (count == idx) {
                found = true;
                NSIndexPath* cIdx = [NSIndexPath indexPathForRow:j inSection:i];
                [table scrollToRowAtIndexPath:cIdx atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                [CAPsUtils waitForUItoSettle];
                UITableViewCell* cell = [table cellForRowAtIndexPath:cIdx];
                for (NSUInteger k = 0; k < noOfTaps; k++) {
                    [KIFCAPs tap:((UIAccessibilityElement*)cell)];
                }
            }
            count++;
        }
    }
    
    return @"OK";
}


+(NSArray*) getSelectedValues:(UITableView*) table;
{
    NSMutableArray* values = [NSMutableArray array];
    
    for (NSIndexPath *idx in [[table indexPathsForSelectedRows] objectEnumerator]) {
        [table scrollToRowAtIndexPath:idx atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        UITableViewCell* cell = [table cellForRowAtIndexPath:idx];
        [values addObject:[UITableViewCellCAPs getText:cell]];
    }
    
    return values;
}

+(NSArray*) getSelectedIndices:(UITableView*) table;
{
    NSMutableArray* values = [NSMutableArray array];
    
    for (NSIndexPath *idx in [[table indexPathsForSelectedRows] objectEnumerator]) {
        [values addObject:[CAPsUtils intAsString:[idx indexAtPosition:0]]];
    }
    
    return values;
}

@end
