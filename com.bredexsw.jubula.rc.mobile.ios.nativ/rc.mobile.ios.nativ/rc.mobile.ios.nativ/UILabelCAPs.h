//
//  UILabelCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/3/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabelCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) getText:(UILabel*) label;
+(NSString*) isEnabled:(UILabel*) label;
@end
