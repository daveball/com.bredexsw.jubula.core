//
//  UIApplicationCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by Kiss Tamás on 19/06/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIApplicationCAPs : NSObject

+ (NSString*) rcInputText:(NSString*) text;

@end
