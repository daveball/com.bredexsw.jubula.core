//
//  OMM.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 11/9/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import "OMM.h"
#import "AUT.h"
#import <UIKit/UIKit.h>
#import "UIView+Automation.h"

@interface OMM()

@end

@implementation OMM

static NSMutableArray *tappedUIWidgetIndices;

+ (NSMutableArray *)tappedUIWidgetIndices
{
    if (!tappedUIWidgetIndices) {
        tappedUIWidgetIndices = [[NSMutableArray alloc] init];
    }
    return tappedUIWidgetIndices;
}

+(NSArray*) getTappedUIWidgetIndices
{
    NSArray* tappedUIWidgets = [NSArray arrayWithArray:[self tappedUIWidgetIndices]];
    [[self tappedUIWidgetIndices] removeAllObjects];
    return tappedUIWidgets;
}

#pragma mark - Start/Stop

+(NSString*) start:(NSArray*) supportedUITypes
{
    for (UIWindow *window in [[[UIApplication sharedApplication] windows] objectEnumerator]) {
        for (UIView *view in [[window subviews] objectEnumerator]) {
            if ([view isVisibleOnScreen]) {
                [self attachToView:view with:supportedUITypes];
            }
        }
    }

    return @"OK";
}

+(NSString*) stop;
{
    for (UIWindow *window in [[[UIApplication sharedApplication] windows] objectEnumerator]) {
        for (UIView *view in [[window subviews] objectEnumerator]) {
            [self detachFromView:view];
        }
    }
    [[self tappedUIWidgetIndices] removeAllObjects];
    return @"OK";
}

#pragma mark - Attach/Detach

+(void)attachToView:(UIView *)view with:(NSArray*) supportedUITypes {
    BOOL isSupportedType = NO;
    
    for (NSUInteger i = 0; i < [supportedUITypes count]; i++) {
        NSString* className = [supportedUITypes objectAtIndex:i];
        if ([view isKindOfClass:NSClassFromString(className)]) {
            isSupportedType = YES;
            break;
        }
    }
    
    if (isSupportedType) {
        UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        [singleTap setNumberOfTapsRequired:1];
        
        UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        [doubleTap setNumberOfTapsRequired:2];
        
        UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [longPress setMinimumPressDuration:2.0f];

        singleTap.accessibilityHint = view.userInteractionEnabled ? @"rc_mobile_ios_nativ_omm:true" : @"rc_mobile_ios_nativ_omm:false";
        longPress.accessibilityHint = view.userInteractionEnabled ? @"rc_mobile_ios_nativ_omm:true" : @"rc_mobile_ios_nativ_omm:false";
        doubleTap.accessibilityHint = view.userInteractionEnabled ? @"rc_mobile_ios_nativ_omm:true" : @"rc_mobile_ios_nativ_omm:false";
        NSLog(@"access hint: %@",singleTap.accessibilityHint);
        [view addGestureRecognizer:singleTap];
        [view addGestureRecognizer:longPress];
        [view addGestureRecognizer:doubleTap];
        view.userInteractionEnabled = YES;
    }
    for (UIView *subView in [[view subviews] objectEnumerator]) {
        if ([view isVisibleOnScreen]) {
            [self attachToView:subView with:supportedUITypes];
        }
    }
}

+(void)detachFromView:(UIView *)view {
    NSArray *gestureRecognizers = [view gestureRecognizers];
    for (NSUInteger i = 0; i < [gestureRecognizers count]; i++) {
        UIGestureRecognizer* gr = [gestureRecognizers objectAtIndex:i];
        NSString * accessibilityHint = [gr accessibilityHint];
        if ([@"rc_mobile_ios_nativ_omm:false" isEqualToString:accessibilityHint]) {
            view.userInteractionEnabled = NO;
            [view removeGestureRecognizer:gr];
        } else if ([@"rc_mobile_ios_nativ_omm:true" isEqualToString:accessibilityHint]) {
            [view removeGestureRecognizer:gr];
        }
    }
    
    for (UIView *subView in [[view subviews] objectEnumerator]) {
        [self detachFromView:subView];
    }
}

#pragma mark - Gesture recognizer actions

// single tap for "normal" collecting
+(void) singleTap:(UIGestureRecognizer* )sender {
    UIView* senderView = sender.view;
    [self collectView:senderView];
}

// double tap or "normal" + "parental" collecting
+(void) doubleTap:(UIGestureRecognizer* )sender {
    UIView* senderView = sender.view;
    [self collectView:senderView];
    UIView* parent = [senderView superview];
    
    while (parent != nil) {
        [self collectViewIfSupported:parent];
        parent = [parent superview];
    }
}

// long tap for "complete" collecting 
+(void) longPress:(UIGestureRecognizer* )sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        for (UIWindow *window in [[[UIApplication sharedApplication] windows] objectEnumerator]) {
            for (UIView *view in [[window subviews] objectEnumerator]) {
                if ([view isVisibleOnScreen]) {
                    [self collectAllSubViews:view];
                }
            }
        }
    }
}

#pragma mark - Collect views

+(void) collectViewIfSupported: (UIView *) view {
    NSArray *gestureRecognizers = [view gestureRecognizers];
    for (NSUInteger i = 0; i < [gestureRecognizers count]; i++) {
        UIGestureRecognizer* gr = [gestureRecognizers objectAtIndex:i];
        NSString * accessibilityHint = [gr accessibilityHint];
        if ([@"rc_mobile_ios_nativ_omm:true" isEqualToString:accessibilityHint]) {
            [self collectView:view];
            break;
        }
    }
}

+(void) collectAllSubViews: (UIView *) view {
    [self collectViewIfSupported:view];
    for (UIView *subView in [[view subviews] objectEnumerator]) {
        if ([view isVisibleOnScreen]) {
            [self collectAllSubViews:subView];
        }
    }
}

+(void) collectView: (UIView *) view;
{
    NSMutableArray* indexPath = [view indexPathOfView];
    [[self tappedUIWidgetIndices] addObject:indexPath];
    [self highlightControl:view];
}

#pragma mark - Highlight view

+(void) highlightControl: (UIView*) view {
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.75];
    int oldAlpha = view.alpha;
    view.alpha = 0.1;
    [UIView commitAnimations];
    
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.75];
    view.alpha = oldAlpha;
    [UIView commitAnimations];
}


+(NSString*) highlight:(NSArray*) indexPath
{
    UIView* toHighlight = [AUT findUIView:indexPath];
    if (toHighlight) {
        [self highlightControl:toHighlight];
    }
    return @"OK";
}

@end