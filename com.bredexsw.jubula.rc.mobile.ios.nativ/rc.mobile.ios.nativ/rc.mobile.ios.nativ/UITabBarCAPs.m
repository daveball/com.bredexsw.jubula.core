//
//  UITabBarCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/28/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UITabBarCAPs.h"
#import "CAPsUtils.h"

@implementation UITabBarCAPs

+(NSString*) getTabCount:(UITabBar*) sControl;
{
    return [CAPsUtils intAsString:[[sControl items] count]];
}

+(NSString*) getTitleOfTab:(UITabBar*) sControl atIndex:(NSString*) sIdx;
{
    UITabBarItem* item = [[sControl items] objectAtIndex:[sIdx intValue]];

    return [CAPsUtils getFallback:item forText:[item title]];
}

+(NSString*) isTabEnabled:(UITabBar*) sControl atIndex:(NSString*) sIdx;
{
    UITabBarItem* item = [[sControl items] objectAtIndex:[sIdx intValue]];
    
    return [CAPsUtils boolAsString:[item isEnabled]];
}

+(NSString*) getSelectedIndex:(UITabBar*) sControl;
{
    UITabBarItem* selectedItem = [sControl selectedItem];
    NSInteger index = -1;
    if (selectedItem) {
        NSInteger pIndex = [[sControl items] indexOfObject:selectedItem];
        if (pIndex != NSNotFound) {
            index = pIndex;
        }
    }
    return [CAPsUtils intAsString:index];
}

+(NSString*) getBoundsOfTab:(UITabBar*) sControl atIndex:(NSString*) sIdx;
{
    NSInteger idx = [sIdx integerValue];
    
    NSInteger tabX = 0;
    NSInteger tabY = 0;
    NSInteger fixSingleTabWidth = [sControl bounds].size.width / [[sControl items] count];
    NSInteger tabHeight = [sControl bounds].size.height;
    
    for (NSUInteger i = 0; i < idx; i++) {
        tabX += fixSingleTabWidth;
    }
    
    return [CAPsUtils boundsAsStringX:tabX y:tabY width:fixSingleTabWidth height:tabHeight];
}

+(NSString*) badgeValue:(UIView*) view atIndex:(NSNumber*) tabIndex
{
    int _tabIndex = [tabIndex  intValue];
    UITabBar *tabbar = (UITabBar*) view;
    NSString* badgeValue = @"-1";
    int itemsCount = tabbar.items.count;
    if (itemsCount > 0 && _tabIndex < itemsCount) {
        UITabBarItem * item = tabbar.items[_tabIndex];
        badgeValue = item.badgeValue ? item.badgeValue : @"-1";
    }

    return badgeValue;
}

@end
