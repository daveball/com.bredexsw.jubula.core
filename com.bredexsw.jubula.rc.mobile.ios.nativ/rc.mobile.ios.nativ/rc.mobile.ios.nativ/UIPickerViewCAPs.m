//
//  UIPickerViewCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/14/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UIPickerViewCAPs.h"
#import "CAPsUtils.h"
#import "KIFCAPs.h"

@interface UIPickerViewCAPs()

+(NSString*) getText:(UIPickerView*) view fromRow:(NSInteger)row inComponent:(NSInteger) component;

@end

@implementation UIPickerViewCAPs

+(NSString*) getText:(UIPickerView*) view;
{    
    NSMutableString* string = [NSMutableString string];
    for (NSUInteger i = 0; i < [view numberOfComponents]; i++) {
        NSString* string2append = [self getText:view fromRow:[view selectedRowInComponent:i] inComponent:i];
        if (string2append) {
            [string appendString:string2append];
            [string appendString:@" "];
        }
    }
    
    return [NSString stringWithString:string];
}

+(NSString*) getText:(UIPickerView*) view fromRow:(NSInteger)row inComponent:(NSInteger) component;
{
    NSString* text = nil;
    id<UIPickerViewDelegate> delegate = [view delegate];
    if ([delegate respondsToSelector:@selector(pickerView:attributedTitleForRow:forComponent:)]) {
        text = [[delegate pickerView:view attributedTitleForRow:row forComponent:component] string];
    } else if ([delegate respondsToSelector:@selector(pickerView:titleForRow:forComponent:)]) {
        text = [delegate pickerView:view titleForRow:row forComponent:component];
    } else if ([delegate respondsToSelector:@selector(pickerView:viewForRow:forComponent:reusingView:)]) {
        text = [CAPsUtils getRenderedText:[delegate pickerView:view viewForRow:row forComponent:component reusingView:nil]];
    }
    return text;
}


+(NSString*) getNoOfColumns:(UIPickerView*) view;
{
    return [CAPsUtils intAsString:[view numberOfComponents]];
}

+(NSString*) getSelectedIndex:(UIPickerView*) view inColumn:(NSString*) sColIdx;
{
    return [CAPsUtils intAsString:[view selectedRowInComponent:[sColIdx integerValue]]];
}

+(NSString*) select:(UIPickerView*) view index:(NSString*) sRowIdx inColumn:(NSString*) sColIdx;
{
    NSInteger rowIndex = [sRowIdx integerValue];
    NSInteger componentIndex = [sColIdx integerValue];
    [view selectRow:rowIndex inComponent:componentIndex animated:YES];
    
    [CAPsUtils waitForUItoSettle];
    
    [KIFCAPs tap:((UIAccessibilityElement*) view)];
    id<UIPickerViewDelegate> delegate = [view delegate];
    if ([delegate respondsToSelector:@selector(pickerView:didSelectRow:inComponent:)]) {
        [delegate pickerView:view didSelectRow:rowIndex inComponent:componentIndex];
    }
    
    return @"OK";
}


+(NSArray*) getValues:(UIPickerView*) view inColumn:(NSString*) sColIdx;
{
    NSMutableArray* values = [NSMutableArray array];
    NSInteger component = [sColIdx integerValue];
    NSInteger numberOfRows = [[view dataSource] pickerView:view numberOfRowsInComponent:component];
    BOOL skipAsSoonAsDuplicate = FALSE;
    if ([view isKindOfClass:NSClassFromString(@"_UIDatePickerView")]) {
        // UIDatePicker* datePicker = (UIDatePicker*) [view superview];
        if (numberOfRows > 3000) {
            numberOfRows = 3000;
        }
        skipAsSoonAsDuplicate = TRUE;
    }
    for (NSUInteger i = 0; i < numberOfRows; i++) {
        NSString* value = [self getText:view fromRow:i inComponent:component];
        if (value) {
            if (skipAsSoonAsDuplicate) {
                if ([values containsObject:value]) {
                    break;
                }
            }
            [values addObject:value];
        }
    }
    
    return values;
}

@end
