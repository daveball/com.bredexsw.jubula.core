//
//  rc_mobile_ios_nativ.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 6/29/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

@protocol UITestable <NSObject>

-(NSString*)getTestableText;

@end


@interface UIRemoteControl : NSObject

+(void) attach;
+(void) attach:(int)port;

@end
