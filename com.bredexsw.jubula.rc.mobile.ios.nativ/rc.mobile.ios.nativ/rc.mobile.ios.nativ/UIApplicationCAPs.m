//
//  UIApplicationCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Kiss Tamás on 19/06/14.
//  Copyright (c) 2014 BREDEX GmbH. All rights reserved.
//

#import "UIApplicationCAPs.h"
#import "CAPsUtils.h"
#import "KIFCAPs.h"
#import "UIView+Automation.h"

@implementation UIApplicationCAPs

+ (NSString*) rcInputText:(NSString*) text
{
    UIView* view = [self rcFindFirstResponder];
    BOOL isSuccess = NO;
    if (view && [view conformsToProtocol:@protocol(UITextInput)]) {
        [KIFCAPs type:text];
        isSuccess = YES;
    }
    return [CAPsUtils boolAsString:isSuccess];
}

+ (UIView*) rcFindFirstResponder
{
    NSArray* windows = [[UIApplication sharedApplication] windows];
    for (UIWindow* window in windows) {
        UIView* view = [window findFirstResponder];
        return view;
    }
    return nil;
}


@end
