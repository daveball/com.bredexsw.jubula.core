//
//  UITextViewCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/8/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UITextViewCAPs.h"
#import "CAPsUtils.h"

@implementation UITextViewCAPs

+(NSString*) getText:(UITextView*) textview;
{
    return [CAPsUtils getFallback:textview forText:[textview text]];
}


+(NSString*) isEditable:(UITextView*) textview;
{
    return [CAPsUtils boolAsString:[textview isEditable]];
}

@end
