//
//  UITableViewCellCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/4/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UITableViewCellCAPs.h"
#import "CAPsUtils.h"

@implementation UITableViewCellCAPs

+(NSString*) getText:(UITableViewCell*) cell;
{
    return [CAPsUtils getFallback:cell forText:[[cell textLabel] text]];
}

@end
