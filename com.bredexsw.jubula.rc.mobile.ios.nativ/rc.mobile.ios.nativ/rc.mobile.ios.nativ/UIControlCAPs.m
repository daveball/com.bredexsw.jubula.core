//
//  UIControlCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by markus on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "UIControlCAPs.h"
#import "CAPsUtils.h"

@implementation UIControlCAPs

+(NSString*) isEnabled:(UIControl*) control;
{
    return [CAPsUtils boolAsString:[control isEnabled]];
}
@end
