//
//  Communicator.h
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 7/16/12.
//  Copyright (c) 2012 BREDEX GmbH. All rights reserved.
//

#import "AsyncSocket.h"

@interface Communicator : NSObject

@property (readonly,getter=isRunning) BOOL running;

- (void) startOnPort:(int)port;
- (void) stop;

@end
