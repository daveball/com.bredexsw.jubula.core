//
//  CAPsUtils.m
//  rc.mobile.ios.nativ
//
//  Created by markus on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import "CAPsUtils.h"
#import "UILabelCAPs.h"
#import "UIButtonCAPs.h"
#import "UITableViewCellCAPs.h"
#import "UITextFieldCAPs.h"
#import "UIViewCAPs.h"
#import "UIRemoteControl.h"
#import "UIPickerViewCAPs.h"
#import "UITextViewCAPs.h"

@implementation CAPsUtils

+(NSString*) getRenderedText:(UIView*) view;
{
    NSString* renderedText = nil;
    if ([view isKindOfClass:[UILabel class]]) {
        renderedText = [UILabelCAPs getText:((UILabel*)view)];
    } else if ([view isKindOfClass:[UIButton class]]) {
        renderedText = [UIButtonCAPs getText:((UIButton*)view)];
    } else if ([view isKindOfClass:[UIPickerView class]]) {
        renderedText = [UIPickerViewCAPs getText:((UIPickerView*)view)];
    } else if ([view isKindOfClass:[UITableViewCell class]]) {
        renderedText = [UITableViewCellCAPs getText:((UITableViewCell*)view)];
    } else if ([view isKindOfClass:[UITextField class]]) {
       renderedText = [UITextFieldCAPs getText:((UITextField*)view)];
    } else if ([view isKindOfClass:[UITextView class]]) {
        renderedText = [UITextViewCAPs getText:((UITextView*)view)];
    } else if ([view isKindOfClass:[UIView class]]) {
       renderedText = [UIViewCAPs getText:((UIView*)view)];
    }
    return renderedText;
}


+(NSString*) getFallback:(NSObject*) obj forText:(NSString*)text
{
    // if the provided text is not nil no fallback necessary
    if (text) {
        return text;
    }
    
    // if the UIAutomation protocol is implemented use it
    if ([[obj class] conformsToProtocol:@protocol(UITestable)]) {
        NSObject<UITestable>* tObj = (NSObject<UITestable>*)obj;
        NSString* fallback = [tObj getTestableText];
        if (fallback) {
            return fallback;
        }
    }
    
    // otherwise use the accessibility label
    if ([obj respondsToSelector:@selector(accessibilityLabel)]) {
        NSString* fallback = [obj accessibilityLabel];
        if (fallback) {
            return fallback;
        }
    }
    // otherwise return an empty string
    return @"";
}

+(NSString*) boolAsString:(BOOL) value; {
    return value ? @"true" : @"false";
}

+(NSString*) intAsString:(NSInteger) value; {
    return [NSString stringWithFormat:@"%ld", (long)value];
}

+(NSString*) boundsAsStringX:(NSInteger) x y:(NSInteger) y width:(NSInteger) width height:(NSInteger) height;
{
    NSMutableString* bounds = [NSMutableString stringWithFormat:@"%ld", (long)x];
    [bounds appendFormat:@"%@", @":"];
    [bounds appendFormat:@"%ld", (long)y];
    [bounds appendFormat:@"%@", @":"];
    [bounds appendFormat:@"%ld", (long)width];
    [bounds appendFormat:@"%@", @":"];
    [bounds appendFormat:@"%ld", (long)height];
    return bounds;
}

+(void) waitForUItoSettle;
{
    CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0.5, false);
}

@end
