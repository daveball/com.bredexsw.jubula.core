//
//  UIButtonCAPs.h
//  rc.mobile.ios.nativ
//
//  Created by markus on 1/2/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButtonCAPs : NSObject
// methods invoked from Java iOS RC side - they all have to have a JSON serializable return value
+(NSString*) getText:(UIButton*) button;
+(NSString*) isSelected:(UIButton*) button;
@end
