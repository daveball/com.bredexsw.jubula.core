//
//  UITextInputCAPs.m
//  rc.mobile.ios.nativ
//
//  Created by Markus Tiede on 1/8/13.
//  Copyright (c) 2013 BREDEX GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractUITextInputCAPs.h"

@implementation AbstractUITextInputCAPs

+(NSString*) selectAll:(UIView <UITextInput>*) textInputView;
{
    [textInputView setSelectedTextRange:[textInputView textRangeFromPosition:textInputView.beginningOfDocument toPosition:textInputView.endOfDocument]];
    return @"OK";
}

@end
