//
//  ViewController.m
//  RotaionBredex
//
//  Created by Kiss Tamás on 03/06/14.
//  Copyright (c) 2014 defko@me.com. All rights reserved.
//

#import "ViewController.h"
#import <CoreMotion/CoreMotion.h>

static NSInteger const kDegree404BottomBorder = 100;
static NSInteger const kDegree404TopBorder = 120;

@interface ViewController ()

@property (nonatomic,strong) CMMotionManager *motionManager;
@property (weak, nonatomic) IBOutlet UILabel *angleLbl;
@property (weak, nonatomic) IBOutlet UILabel *orientationLbl;

@end

static double RADIANS_TO_DEGREES (double radians)
{
    return radians * 180/M_PI;
}

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initRotation];
    [self changeTextAtOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self startDeviceMotion];
}

- (void) initRotation
{
    self.motionManager = [CMMotionManager new];
    self.motionManager.deviceMotionUpdateInterval = 0.1;
}

- (void)startDeviceMotion
{
    if ([self.motionManager isDeviceMotionAvailable]) {
        [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMDeviceMotion *motion, NSError *error){
            if (motion) {
                double pitch = round(RADIANS_TO_DEGREES(motion.attitude.pitch));
                double roll  = round(RADIANS_TO_DEGREES(motion.attitude.roll));
                double angle = roll > 0 ? 90 - pitch : 270 + pitch;
                [self backgroundAtAngle:angle];
                [self showCurrentAngle:angle];
            }
        }];
    }
}

- (void) showCurrentAngle:(NSInteger) angle
{
    if (angle >= kDegree404BottomBorder && angle <= kDegree404TopBorder) {
        self.angleLbl.text = @"404";
    } else {
        self.angleLbl.text = [@(angle) stringValue];
    }
}

- (void) currentOrientation:(NSInteger) angle
{
    if ((angle > 75 && angle < 105) || (angle > 255 && angle < 285)) {
        self.orientationLbl.text = @"Landscape";
    } else {
        self.orientationLbl.text = @"Portrait";
    }
}

- (void) backgroundAtAngle:(NSInteger) angle
{
    if (angle>0 && angle<90) {
        self.view.backgroundColor = [UIColor greenColor];
    } else if (angle>90 && angle<180) {
        self.view.backgroundColor = [UIColor redColor];
    } else if (angle>180 && angle<270) {
        self.view.backgroundColor = [UIColor blueColor];
    } else if (angle>270 && angle<360) {
        self.view.backgroundColor = [UIColor yellowColor];
    }
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self changeTextAtOrientation:toInterfaceOrientation];
}

- (void) changeTextAtOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        self.orientationLbl.text = @"Landscape";
    } else {
        self.orientationLbl.text = @"Portrait";
    }
}

@end
